#! /usr/bin/env python3

from csv import reader, writer
from pathlib import Path


def get_data(file_name):
    with open(file_name) as file:
        # Get data
        data_raw = reader(file, delimiter=" ")

        # Process
        data = []
        for row_raw in data_raw:
            row = []
            for item in row_raw:
                if item:
                    row.append(float(item))
            data.append(row)

    return data


def strip_accuracy(data):
    return [row[0] for row in data]


def stringify(number):
    return "{:1.15E}".format(number)


def write_data(file_name, data_):
    with open(file_name, "w") as file:
        write = writer(file, delimiter=" ")
        write.writerows([[stringify(e) for e in r] for r in data_])


def write_out(output_file, data):
    if output_file:
        print(f"Writing data to file {output_file}")
        if Path(output_file).is_file():
            if input("File already exists. Do you want to overwrite? [y/n] ") != "y":
                exit("Aborting.")

        write_data(output_file, data)
    else:
        for line in data:
            print(*[stringify(e) + " " for e in line])


if __name__ == "__main__":
    exit()
