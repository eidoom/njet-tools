template <typename T>
const std::vector<MOM<T>>
ps::getMasslessPhaseSpacePoint(const int mul, const bool verbose, const int rseed, const double sqrtS)
{
    Flavour<double> flavours[mul];
    for (auto flavour : flavours) {
        flavour = com::g;
    }

    PhaseSpace<T> psp(mul, flavours, rseed, sqrtS);
    const std::vector<MOM<T>> momenta { psp.getPSpoint() };

    std::cout << std::endl;
    if (verbose) {
        psp.showPSpoint();
    }

    return momenta;
}

// BROKEN - needs complex momenta
// hard coded single collinear phase-space point for 2->2
// collinear in 0||1 (s12->0) as theta -> 0
template <typename T>
std::vector<MOM<T>> ps::getCollinearPhaseSpacePoint4(const T theta, const bool verbose)
{
    const T sqrtS { 1. };

    const T ct { cos(theta) };
    const T st { sin(theta) };

    const T cp { -1 };
    com::print("cos(phi)", cp, verbose);
    assert(-1. <= cp);
    assert(cp <= 1.);
    const T sp { std::sqrt(T(1.) - cp * cp) };
    com::print("sin(phi)", sp, verbose);
    assert(-1. <= sp);
    assert(sp <= 1.);

    std::vector<MOM<T>> momenta(4);
    momenta[0] = sqrtS / T(2.) * MOM<T>(1., 0., 0., 1.);
    momenta[1] = sqrtS / T(2.) * MOM<T>(1, 0, -st, ct);
    momenta[2] = sqrtS / T(2.) * MOM<T>(-1, sp, -(cp * st), cp * ct);
    momenta[3] = -(momenta[0] + momenta[1] + momenta[2]);

    com::checkPhaseSpacePoint(momenta, verbose);
    com::printMomenta(momenta, verbose);

    return momenta;
}

// hard coded single collinear phase-space point for 2->3
// collinear in 3||4 (s34->0)
template <typename T>
std::vector<MOM<T>> ps::getCollinearPhaseSpacePoint5(const T lambda, const bool verbose)
{
    const T sqrtS { 1. };
    std::vector<MOM<T>> momenta(5);

    const T y1 { lambda };
    assert((0. <= y1) && (y1 <= 1.));
    const T y2 { T(0.2) };
    assert((0. <= y2) && (y2 <= 1.));
    assert((0. <= y1 + y2) && (y1 + y2 <= 1.));

    const T x1 { T(1.) - y1 };
    assert((0. <= x1) && (x1 <= 1.));
    const T x2 { T(1.) - y2 };
    assert((0. <= x2) && (x2 <= 1.));
    assert((0. <= 2 - x1 - x2) && (2 - x1 - x2 <= 1.));

    const T cosBeta { T(1.) + T(2.) * (T(1.) - x1 - x2) / x1 / x2 };
    assert((-1. <= cosBeta) && (cosBeta <= 1.));
    const T sinBeta { std::sqrt(T(1.) - pow(cosBeta, 2.)) };
    assert((-1. <= sinBeta) && (sinBeta <= 1.));

    const T theta { M_PI / T(2.) };
    assert((0. <= theta) && (theta <= M_PI));
    const T cosTheta { cos(theta) };
    assert((-1. <= cosTheta) && (cosTheta <= 1.));
    const T sinTheta { sin(theta) };
    assert((-1. <= sinTheta) && (sinTheta <= 1.));

    const T alpha { M_PI / T(2.) };
    assert((0. <= alpha) && (alpha <= M_PI));
    const T cosAlpha { cos(alpha) };
    assert((-1. <= cosAlpha) && (cosAlpha <= 1.));
    const T sinAlpha { sin(alpha) };
    assert((-1. <= sinAlpha) && (sinAlpha <= 1.));

    momenta[0] = sqrtS / T(2.) * MOM<T>(-1., 0., 0., -1.);
    momenta[1] = sqrtS / T(2.) * MOM<T>(-1., 0., 0., 1.);
    momenta[2] = x1 * sqrtS / T(2.) * MOM<T>(1., sinTheta, 0., cosTheta);
    momenta[3] = x2 * sqrtS / T(2.) * MOM<T>(1., cosAlpha * cosTheta * sinBeta + cosBeta * sinTheta, sinAlpha * sinBeta, cosBeta * cosTheta - cosAlpha * sinBeta * sinTheta);
    momenta[4] = -(momenta[0] + momenta[1] + momenta[2] + momenta[3]);

    com::checkPhaseSpacePoint(momenta, verbose);
    com::printMomenta(momenta, verbose);

    return momenta;
}

// To check that sij scales correctly with lambda (and other sij do not)
template <typename T>
void ps::testCollinearPhaseSpacePoint(const std::vector<MOM<T>>& mom, const int mul)
{
    for (int i { 0 }; i < mul - 1; ++i) {
        for (int j { i + 1 }; j < mul; ++j) {
            const T s { 2 * dot(mom[i], mom[j]) };
            std::cout << "s" << i << j << "=" << ((s >= 0) ? " " : "") << s << "   ";
        }
        if (i < mul - 2) {
            std::cout << std::endl;
        }
    }
    std::cout << std::endl;
}

// To check that collinear sij scales correctly with lambda (and other sij do not)
template <typename T>
void ps::testPhaseSpacePoint5(const T lambda)
{
    std::vector<MOM<T>> mom = getCollinearPhaseSpacePoint5<T>(lambda);
    std::cout << std::endl
              << "lambda = " << lambda << ":" << std::endl;
    ps::testCollinearPhaseSpacePoint(mom, 5);
}

// hard coded single collinear phase-space point for 2->4, 0||1
template <typename T>
std::vector<MOM<T>> ps::getCollinear01PhaseSpacePoint6(const T lambda, const bool verbose, const T sqrtS)
{
    const int mul { 6 };
    std::vector<MOM<T>> momenta(mul);

    const T E2 { T(0.12) * sqrtS };
    const T E3 { T(0.37) * sqrtS };

    const T theta1 { M_PI / T(3.) };
    const T theta2 { lambda + theta1 };
    const T ct1 { cos(theta1) };
    const T ct2 { cos(theta2) };
    const T st1 { sin(theta1) };
    const T st2 { sin(theta2) };

    const T phi1 { M_PI / T(2.) };
    const T phi2 { M_PI / T(2.) };
    const T cp1 { cos(phi1) };
    const T cp2 { cos(phi2) };
    const T sp1 { sin(phi1) };
    const T sp2 { sin(phi2) };

    const T E4 {
        -T(0.5) * (-sqrtS * sqrtS + T(2.) * sqrtS * E2 + T(2.) * E2 * st1 * sp1 * E3 * sp2 * st2 + 2 * E2 * st1 * cp1 * E3 * cp2 * st2 + T(2.) * E2 * ct1 * E3 * ct2 + T(2.) * sqrtS * E3 - T(2.) * E2 * E3) / (T(2.) * E2 * st1 * sp1 + T(2.) * E3 * sp2 * st2 + T(2.) * E2 * st1 * cp1 + T(2.) * E3 * cp2 * st2 + E2 * ct1 + E3 * ct2 + T(3.) * sqrtS - T(3.) * E2 - T(3.) * E3)
    };

    momenta[4] = MOM<T>(sqrtS / 2, 0., 0., -sqrtS / 2);
    momenta[5] = MOM<T>(sqrtS / 2, 0., 0., sqrtS / 2);
    momenta[0] = -E2 * MOM<T>(1., sp1 * st1, ct1, cp1 * st1);
    momenta[1] = -E3 * MOM<T>(1., sp2 * st2, ct2, cp2 * st2);
    momenta[2] = -E4 * MOM<T>(3., 2., 1., 2.);
    momenta[3] = -momenta[0] - momenta[1] - momenta[2] - momenta[4] - momenta[5];

    com::checkPhaseSpacePoint(momenta, verbose);

    if (verbose) {
        std::cout << std::endl;
        for (int i { 0 }; i < mul; ++i) {
            std::cout << 'p' << i << '=' << momenta[i] << std::endl;
        }
    }

    return momenta;
}

// template for hard coded single collinear phase-space point for 2->4
template <typename T>
std::vector<MOM<T>>
ps::getCollinearPhaseSpacePoint6(const T alpha, const T beta, const T gamma, const bool verbose,
    const T sqrtS)
{
    const int mul { 6 };
    std::vector<MOM<T>> momenta(mul);

    const T x1 { 0.12 };
    const T x2 { 0.37 };

    const T ca { cos(alpha) };
    const T cb { cos(beta) };
    const T cg { cos(gamma) };

    const T sa { sin(alpha) };
    const T sb { sin(beta) };
    const T sg { sin(gamma) };

    const T x3 {
        (4 - 4 * x1 + x1 * x1 - ca * ca * x1 * x1 - sa * sa * x1 * x1 - 4 * x2 + 2 * x1 * x2 - 2 * ca * ca * cb * x1 * x2 - 2 * sa * sa * x1 * x2 + x2 * x2 - ca * ca * cb * cb * x2 * x2 - sa * sa * x2 * x2 - ca * ca * sb * sb * x2 * x2) / (2 * (2 - x1 + ca * ca * cb * x1 + cg * sa * sa * x1 - ca * sa * sb * sg * x1 - x2 + ca * ca * cb * cb * x2 + cg * sa * sa * x2 + ca * ca * cg * sb * sb * x2))
    };

    momenta[0] = -sqrtS / T(2.) * MOM<T>(1., 0., 0., 1.);
    momenta[1] = -sqrtS / T(2.) * MOM<T>(1., 0., 0., -1.);
    momenta[2] = x1 * sqrtS / T(2.) * MOM<T>(1., 0., -sa, ca);
    momenta[3] = x2 * sqrtS / T(2.) * MOM<T>(1., ca * sb, -sa, ca * cb);
    momenta[4] = x3 * sqrtS / T(2.) * MOM<T>(1., ca * cg * sb + sa * sg, -(cg * sa) + ca * sb * sg, ca * cb);
    momenta[5] = -(momenta[0] + momenta[1] + momenta[2] + momenta[3] + momenta[4]);

    com::checkPhaseSpacePoint(momenta, verbose);

    if (verbose) {
        for (int i { 0 }; i < mul; ++i) {
            std::cout << 'p' << i << '=' << momenta[i] << std::endl;
        }
    }

    return momenta;
}

// hard coded single collinear phase-space point for 2->4, 2||3
template <typename T>
std::vector<MOM<T>> ps::getCollinear23PhaseSpacePoint6(const T lambda, const bool verbose, const T sqrtS)
{
    const T alpha { M_PI / T(3.) };
    const T beta { lambda };
    const T gamma { M_PI / T(5.) };
    return ps::getCollinearPhaseSpacePoint6(alpha, beta, gamma, verbose, sqrtS);
}

// hard coded single collinear phase-space point for 2->4, 3||4
template <typename T>
std::vector<MOM<T>> ps::getCollinear34PhaseSpacePoint6(const T lambda, const bool verbose, const T sqrtS)
{
    const T alpha { M_PI / T(3.) };
    const T beta { M_PI / T(5.) };
    const T gamma { lambda };
    return ps::getCollinearPhaseSpacePoint6(alpha, beta, gamma, verbose, sqrtS);
}

// To check that s01 scales correctly with lambda (and other sij do not)
template <typename T>
void ps::testCollinear01PhaseSpacePoint6(const T lambda, const bool verbose)
{
    const int mul { 6 };
    std::vector<MOM<T>> mom = getCollinear01PhaseSpacePoint6<T>(lambda, verbose);
    std::cout << std::endl
              << "lambda=" << lambda << ":" << std::endl;
    ps::testCollinearPhaseSpacePoint(mom, mul);
}

// To check that s34 scales correctly with lambda (and other sij do not)
template <typename T>
void ps::testCollinear34PhaseSpacePoint6(const T lambda, const bool verbose)
{
    const int mul { 6 };
    std::vector<MOM<T>> mom = getCollinear34PhaseSpacePoint6<T>(lambda, verbose);
    std::cout << std::endl
              << "lambda=" << lambda << ":" << std::endl;
    ps::testCollinearPhaseSpacePoint(mom, mul);
}

// To check that s23 scales correctly with lambda (and other sij do not)
template <typename T>
void ps::testCollinear23PhaseSpacePoint6(const T lambda, const bool verbose)
{
    const int mul { 6 };
    std::vector<MOM<T>> mom = getCollinear23PhaseSpacePoint6<T>(lambda, verbose);
    std::cout << std::endl
              << "lambda=" << lambda << ":" << std::endl;
    ps::testCollinearPhaseSpacePoint(mom, mul);
}

// To check that s12 scales correctly with lambda (and other sij do not)
template <typename T>
void ps::testCollinearPhaseSpacePoint4(const T lambda, const bool verbose)
{
    const int mul { 4 };
    std::vector<MOM<T>> mom = getCollinearPhaseSpacePoint4<T>(lambda, verbose);
    std::cout << std::endl
              << "lambda=" << lambda << ":" << std::endl;
    ps::testCollinearPhaseSpacePoint(mom, mul);
}

// test that phase space point has massless momenta and that momentum conservation is obeyed
template <typename T, int mul>
void ps::checkPhaseSpacePoint(const std::array<std::array<T, 4>, mul>& momenta, const bool verbose)
{
    /* T zero { std::pow(10, -10) }; */
    std::array<T, 4> sum { 0 };
    for (int i { 0 }; i < mul; ++i) {
        for (int j { 0 }; j < 4; ++j) {
            sum[j] += (i < 2 ? -1 : 1) * momenta[i][j];
        }
        T mass { std::pow(momenta[i][0], 2) };
        for (int j { 1 }; j < 4; ++j) {
            mass -= std::pow(momenta[i][j], 2);
        }
        com::print("m" + std::to_string(i), mass, verbose);
        /* assert(std::abs(mass) <= zero); */
    }
    std::cout << std::endl;
    for (int j { 0 }; j < 4; ++j) {
        com::print("p-sum_" + std::to_string(j), sum[j], verbose);
    }
    /* assert(sum[0] <= zero); */
    /* assert(sum[1] <= zero); */
    /* assert(sum[2] <= zero); */
    /* assert(sum[3] <= zero); */
}

template <typename T, int mul>
void ps::printPhaseSpacePoint(const std::array<std::array<T, 4>, mul>& momenta, const bool verbose)
{
    const int d { 4 };
    if (verbose) {
        for (int p { 0 }; p < mul; ++p) {
            std::cout << "p" << p << "=(";
            for (int mu { 0 }; mu < d; ++mu) {
                std::cout << momenta[p][mu] << (mu == d - 1 ? "" : ",");
            }
            std::cout << ")" << std::endl;
        }
    }
}
