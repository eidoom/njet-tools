template <typename T>
T com::mod_sq(std::complex<T> n)
{
    return real(conj(n) * n);
}

template <typename T>
void com::print(const std::string& name, const T& datum, const bool verbose)
{
    if (verbose) {
        std::cout << name << "=" << datum << std::endl;
    }
}

template <typename AT, int len>
std::string com::genArrStr(const AT (&array)[len])
{
    std::string arrStr;
    for (auto el : array) {
        arrStr.append(std::to_string(el));
    }
    return arrStr;
}

template <int mul>
std::string com::genHelStr(const int (&helInt)[mul])
{
    std::string helStr;
    for (auto hel : helInt) {
        helStr.append(hel == 1 ? "+" : "-");
    }
    return helStr;
}

template <int mul>
void com::printHelicity(const std::string& name, const int (&helInt)[mul], const bool verbose)
{
    print(name, genHelStr(helInt), verbose);
}

template <typename T>
void com::printMomenta(const std::vector<MOM<T>> momenta, const bool verbose)
{
    if (verbose) {
        int i { 0 };
        for (auto mom : momenta) {
            std::cout << 'p' << i << '=' << mom << std::endl;
            ++i;
        }
    }
}

// test that phase space point has massless momenta and that momentum conservation is obeyed
template <typename T>
void com::checkPhaseSpacePoint(const std::vector<MOM<T>>& momenta, const bool verbose)
{
    T zero { pow(10, -10) };
    MOM<T> sum;
    int i { 0 };
    for (auto momentum : momenta) {
        sum += momentum;
        print("m" + std::to_string(i), momentum.mass(), verbose);
        assert(std::abs(momentum.mass()) <= zero);
        ++i;
    }
    print("momentum sum", sum, verbose);
    assert(sum.x0 <= zero);
    assert(sum.x1 <= zero);
    assert(sum.x2 <= zero);
    assert(sum.x3 <= zero);
}
