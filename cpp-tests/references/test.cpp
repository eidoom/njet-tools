#include <iostream>

int main()
{
    // create a value a
    int a { 1 };
    std::cout << "a: " << a << '\n';
    // a: 1

    // create a reference b to value a
    int& b { a };

    // we can change the value of a (and therefore its reference b) directly
    a = 2;
    std::cout << "a: " << a << " b: " << b << '\n';
    // a: 2 b: 2

    // we can change the value of a (and therefore its reference b) through its reference b
    b = 3;
    std::cout << "a: " << a << " b: " << b << '\n';
    // a: 3 b: 3

    // let's introduce a new value aa and reference bb to aa
    int aa { 4 };
    int& bb { aa };
    std::cout << "a: " << a << " b: " << b << " aa: " << aa << " bb: " << bb << '\n';
    // a: 3 b: 3 aa: 4 bb: 4

    // this will set the value of a (through reference b) to be the value of aa (through reference bb)
    // through the reference, we can only set the value of the referenced object; we can never change the reference to be a reference to another object
    b = bb;
    std::cout << "a: " << a << " b: " << b << " aa: " << aa << " bb: " << bb << '\n';
    // a: 4 b: 4 aa: 4 bb: 4

    // we can change aa and see everything works as expected
    // in the previous step, a took only the value of aa, it did NOT become the reference bb to aa
    aa = 5;
    std::cout << "a: " << a << " b: " << b << " aa: " << aa << " bb: " << bb << '\n';
    // a: 4 b: 4 aa: 5 bb: 5

    // we make a new const reference c to a
    const int& c { a };
    std::cout << "a: " << a << " b: " << b << " aa: " << aa << " bb: " << bb << " c: " << c << '\n';
    // a: 4 b: 4 aa: 5 bb: 5 c: 4

    // c is read-only!
    // c = 6;
    // error: assignment of read-only reference ‘c’

    // obviously, the value returned by of const reference c to a changes if the value of a does
    // that's why const is such an appropriate name here
    a = 6;
    std::cout << "a: " << a << " b: " << b << " aa: " << aa << " bb: " << bb << " c: " << c << '\n';
    // a: 6 b: 6 aa: 5 bb: 5 c: 6

    // we can also indirectly make a new reference d to a through reference b
    int& d { b };
    std::cout << "a: " << a << " b: " << b << " aa: " << aa << " bb: " << bb << " c: " << c << " d: " << d << '\n';
    // a: 6 b: 6 aa: 5 bb: 5 c: 6 d: 6

    // or make a new const reference e to a through reference b
    const int& e { b };
    std::cout << "a: " << a << " b: " << b << " aa: " << aa << " bb: " << bb << " c: " << c << " d: " << d << " e: " << e << '\n';
    // a: 6 b: 6 aa: 5 bb: 5 c: 6 d: 6 e: 6

    // or similarly through const reference c
    const int& f { c };
    std::cout << "a: " << a << " b: " << b << " aa: " << aa << " bb: " << bb << " c: " << c << " d: " << d << " e: " << e << " f: " << f  << '\n';
    // a: 6 b: 6 aa: 5 bb: 5 c: 6 d: 6 e: 6 f: 6

    // however, we cannot make a new (mutable) reference g through the const reference c (or e or f)
    // int& g { c };
    // error: binding reference of type ‘int&’ to ‘const int’ discards qualifiers
}
