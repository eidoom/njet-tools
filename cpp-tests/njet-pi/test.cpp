#include <iostream>
#include <numbers>

#include <qd/dd_real.h>
#include <qd/qd_real.h>

#include "ngluon2/utility.h"

void run()
{
    std::cout << std::numbers::pi_v<double> << '\n';
    std::cout << constant_traits<double>::pi() << '\n';
    std::cout << M_PI << '\n';
    std::cout << '\n';

    std::cout << constant_traits<dd_real>::pi() << '\n';
    std::cout << dd_real::_pi << '\n';
    std::cout << '\n';

    std::cout << constant_traits<qd_real>::pi() << '\n';
    std::cout << qd_real::_pi << '\n';
    std::cout << '\n';
}

int main()
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(64);
    std::cout << '\n';

    run();

    std::cout << '\n';
}
