# eigen-test
## Install Eigen3
### New
```shell
sudo dnf install eigen3-devel
```
### Old
```shell
echo "export EIGEN_LOCAL=$LOCAL/eigen" >> ~/.zshrc && source ~/.zshrc
echo "export PKG_CONFIG_PATH=$EIGEN_LOCAL/share/pkgconfig:$PKG_CONFIG_PATH" >> ~/.zshrc
```
and
```shell
cd ~/git
git clone https://gitlab.com/libeigen/eigen.git
mkdir -p ~/scratch/eigen
cd ~/scratch/eigen
cmake ~/git/eigen -DCMAKE_INSTALL_PREFIX=$EIGEN_LOCAL
```
or
```shell
cd ~/scratch
wget https://gitlab.com/libeigen/eigen/-/archive/3.4.0/eigen-3.4.0.tar.bz2
tar xf eigen-3.4.0.tar.bz2
cd eigen-3.4.0
mkdir build_dir
cd build_dir
cmake .. -DCMAKE_INSTALL_PREFIX=$EIGEN_LOCAL
```
then
```shell
make install
```
## Usage
See [`Makefile`](./Makefile) and [`test.cpp`](./test.cpp)
## SparseMatrix
* [Tutorial](https://eigen.tuxfamily.org/dox/group__TutorialSparse.html)
* [Reference](https://eigen.tuxfamily.org/dox/classEigen_1_1SparseMatrix.html#a54adf6aa526045f37e67e352da8fd105)
