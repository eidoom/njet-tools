// https://en.cppreference.com/w/cpp/header/cstdint
// https://en.cppreference.com/w/cpp/language/types
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// #include "qd/dd_real.h"

template <typename T> bool read_byte(std::ifstream &infile, T &out) {
  return infile.read(reinterpret_cast<char *>(&out), sizeof out) ? true : false;
}

int main() {
  std::string filename{"data.bin"};
  std::ifstream infile(filename, std::ios::binary);

  if (!infile.is_open()) {
    std::cerr << "Error: can't open file " << filename << '\n';
    std::exit(EXIT_FAILURE);
  }

  int32_t nz, nrows, ncols, nnnz;

  read_byte(infile, nrows);
  std::cout << "nrows: " << nrows << '\n';

  read_byte(infile, ncols);
  std::cout << "ncols: " << ncols << '\n';

  read_byte(infile, nnnz);
  std::vector<int> nnz(nnnz);

  for (int i{0}; i < nnnz; ++i) {
    read_byte(infile, nz);
    nnz[i] = nz;
  }

  std::cout << "nnz[" << nnz.size() << "]: ";
  for (int i : nnz) {
    std::cout << i << ' ';
  }
  std::cout << '\n';

  int32_t i, j;
  double n, d;
  while (read_byte(infile, i)) {
    read_byte(infile, j);
    read_byte(infile, n);
    read_byte(infile, d);
    std::cout << "(" << i << "," << j << "): " << n << "/" << d << '\n';
  }
}
