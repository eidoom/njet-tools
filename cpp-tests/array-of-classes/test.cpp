#include <array>
#include <iostream>

struct Class {
  Class(int num) { this->member = num; }

  int member;
};

int main() {
  std::array<Class, 2> arr({Class(1), Class(2)});

  arr[0] = Class(3);

  std::cout << arr[0].member << '\n';
}
