#include <iostream>
#include <memory>

class A {
public:
  virtual void func() { std::cout << "A\n"; }
};

class B : public A {
public:
  virtual void func() override { std::cout << "B\n"; }
};

int main() {
  std::unique_ptr<A> a{std::make_unique<B>()};
  a->func();

  B b;
  A *bb{&b};
  bb->func();
}
