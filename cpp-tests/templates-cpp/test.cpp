#include <iostream>

template <typename T>
struct Object {
    void member_function();
};

template <typename T>
void Object<T>::member_function()
{
    std::cout << "member_function<T>()\n";
}

template <>
void Object<int>::member_function()
{
    std::cout << "member_function<int>()\n";
}

template <>
struct Object<float> {
    void special_function()
    {
        std::cout << "special_function<float>()\n";
    };
};

// int main(int argc, char* argv[])
int main()
{
    std::cout << '\n';

    Object<double> object_double;
    object_double.member_function();

    Object<int> object_int;
    object_int.member_function();

    Object<float> object_float;
    object_float.special_function();
    // object_float.member_function(); // doesn't exist

    std::cout << '\n';
}
