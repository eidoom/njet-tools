#include <array>
#include <cassert>
#include <chrono>
#include <cmath>
#include <complex>
#include <iostream>
#include <vector>

#include <qd/qd_real.h>

template <typename T>
T pow_njet(T x, const int m)
{
    unsigned int n = m < 0 ? -m : m;
    T y = n % 2 ? x : T(1.);
    while (n >>= 1) {
        x = x * x;
        if (n % 2) {
            y = y * x;
        }
    }
    return m < 0 ? T(1.) / y : y;
}

// supports power > 0
template <typename T>
T pow_pi(T base, int power)
{
    T ans { base };
    for (int x { 1 }; x < power; ++x) {
        ans *= base;
    }
    return ans;
}

// supports power < 0
template <typename T>
T pow_ni(T base, int power)
{
    T ans { base };
    for (int x { 1 }; x < -power; ++x) {
        ans *= base;
    }
    return T(1.) / ans;
}

// supports power >= 0
template <typename T>
T pow_zpi(T base, int power)
{
    T ans { 1. };
    for (int x { 0 }; x < power; ++x) {
        ans *= base;
    }

    return ans;
}

const int N { 1000 };

template <typename T>
void test_pow_njet(T e, int x)
{
    auto t1 { std::chrono::high_resolution_clock::now() };
    T ex { 0 };
    for (int i { 0 }; i < N; ++i)
        ex += pow_njet(e + i, x);
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout << "njet " << ex
              << '\n'
              << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_pow(T e, T x)
{
    auto t1 { std::chrono::high_resolution_clock::now() };
    T ex { 0 };
    for (int i { 0 }; i < N; ++i)
        ex += std::pow(e + i, x);
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout << "std::pow(f,f) " << ex
              << '\n'
              << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_pow_overload(T e, int x)
{
    auto t1 { std::chrono::high_resolution_clock::now() };
    T ex { 0 };
    for (int i { 0 }; i < N; ++i)
        ex += std::pow(e + i, x);
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout << "std::pow(f,i) " << ex
              << '\n'
              << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_powd(T e, T x)
{
    auto t1 { std::chrono::high_resolution_clock::now() };
    T ex { pow(e, x) };
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout << "pow(f,f) " //<< ex
              << '\n'
              << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_powi(T e, int x)
{
    auto t1 { std::chrono::high_resolution_clock::now() };
    T ex { pow(e, x) };
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout << "pow(f,i) " //<< ex
              << '\n'
              << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_pow_pi(T e, int x)
{
    assert(x > 0);
    auto t1 { std::chrono::high_resolution_clock::now() };
    T ex { pow_pi(e, x) };
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout //<< "pow_pi " << ex << '\n'
        << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_pow_zpi(T e, int x)
{
    assert(x >= 0);
    auto t1 { std::chrono::high_resolution_clock::now() };
    T ex { pow_zpi(e, x) };
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout //<< "pow_zpi " << ex << '\n'
        << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_pow_ni(T e, int x)
{
    assert(x < 0);
    auto t1 { std::chrono::high_resolution_clock::now() };
    T ex { pow_ni(e, x) };
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout //<< "pow_ni " << ex << '\n'
        << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_div(T e)
{
    auto t1 { std::chrono::high_resolution_clock::now() };
    T ex { T(1.) / e };
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout << "div " << ex << '\n'
              << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_mul(T e, T x)
{
    auto t1 { std::chrono::high_resolution_clock::now() };
    T ex { x * e };
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout << "mul " << ex << '\n'
              << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_imul(T e, T x)
{
    auto t1 { std::chrono::high_resolution_clock::now() };
    e *= x;
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout << "imul " << e << '\n'
              << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_conj(T a, T b)
{
    std::complex<T> ex(a, b);
    auto t1 { std::chrono::high_resolution_clock::now() };
    std::complex<T> cex { conj(ex) };
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout << "conj " << cex << '\n'
              << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

template <typename T>
void test_stdconj(T a, T b)
{
    std::complex<T> ex(a, b);
    auto t1 { std::chrono::high_resolution_clock::now() };
    std::complex<T> cex { std::conj(ex) };
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::cout << "std::conj " << cex << '\n'
              << "Time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "ns\n";
}

// int main(int argc, char* argv[])
int main()
{
    std::cout.precision(17);
    std::cout.setf(std::ios_base::scientific);

    // double n { 1.1111111111111111 };
    double e { 2. };
    double x { 2. };

    std::cout << "\ndoubles\nfirst is warmup" << '\n';
    test_pow<double>(e, x);
    test_pow<double>(e, x);
    test_pow<double>(e, x);
    test_pow_overload<double>(e, static_cast<int>(x));
    test_pow_overload<double>(e, static_cast<int>(x));
    //test_powd<double>(e, x);
    //test_powi<double>(e, static_cast<int>(x));
    //test_pow_pi<double>(e, static_cast<int>(x));
    //test_pow_zpi<double>(e, static_cast<int>(x));
    test_pow_njet<double>(e, static_cast<int>(x));
    test_pow_njet<double>(e, static_cast<int>(x));

    // std::cout << "\ndouble doubles" << '\n';
    // test_powd<dd_real>(static_cast<dd_real>(e), static_cast<dd_real>(x));
    // test_powd<dd_real>(static_cast<dd_real>(e), static_cast<dd_real>(x));
    // test_powi<dd_real>(static_cast<dd_real>(e), static_cast<int>(x));
    // test_pow_pi<dd_real>(static_cast<dd_real>(e), static_cast<int>(x));
    // test_pow_njet<dd_real>(static_cast<dd_real>(e), static_cast<int>(x));

    // std::cout << "\nnegative doubles" << '\n';
    // test_powd<double>(e, -x);
    // test_powi<double>(e, static_cast<int>(-x));
    // test_pow_ni<double>(e, static_cast<int>(-x));
    // test_pow_njet<double>(e, static_cast<int>(-x));

    // std::cout << "\ndivision" << '\n';
    // test_pow<double>(e, -1);
    // test_powd<double>(e, -1);
    // test_pow_njet<double>(M_PI, -1);
    // test_pow_ni<double>(M_PI, -1);
    // test_div<double>(M_PI);

    // std::cout << "\nmultiplication" << '\n';
    // test_mul<double>(M_PI, e);
    // test_imul<double>(M_PI, e);

    // std::cout << "\nconj" << '\n';
    // test_conj<double>(3., 4.);
    // test_stdconj<double>(3., 4.);
    // test_conj<dd_real>(3., 4.);
    // test_stdconj<dd_real>(3., 4.);
    // test_conj<qd_real>(3., 4.);
    // test_stdconj<qd_real>(3., 4.);
    // test_stdconj<double>(6., 8.);
    // test_conj<double>(6., 8.);
    // test_stdconj<dd_real>(6., 8.);
    // test_conj<dd_real>(6., 8.);
    // test_stdconj<qd_real>(6., 8.);
    // test_conj<qd_real>(6., 8.);

    // std::cout << "\narrays" << '\n';
    // const std::array<double, 2> one { { 1., 2. } };
    // const std::array<double, 2>& two { one };
    // const double* zero { one.data() };
    // std::vector<dd_real> three(2);
    // for (std::size_t i { 0 }; i < three.size(); i++) {
    //     // three[i] = static_cast<dd_real>(one[i]);
    //     three[i] = zero[i];
    // }
    // std::cout << two[0] << '\n';
    // std::cout << three[0] << '\n';

    // std::cout
    //     << '\n'
    //     << "1=" << (1 << 0) << '\n'
    //     << "10=" << (1 << 1) << '\n'
    //     << "100=" << (1 << 2) << '\n'
    //     << "1000=" << (1 << 3) << '\n'
    //     << "10000=" << (1 << 4) << '\n'
    //     << "10=" << (2 << 0) << '\n'
    //     << "100=" << (2 << 1) << '\n'
    //     << "1000=" << (2 << 2) << '\n'
    //     << '\n';

    double a { pow_njet(5., 2) };
    std::cout << a << '\n';
    // function returns can be ignored in C++
    pow_njet(5., 2);

    double b { 1 };
    std::cout << b << '\n';
}
