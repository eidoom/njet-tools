#include <array>
#include <iostream>
#include <vector>

void func1(int arr[]) { std::cout << arr[1] << '\n'; }

void func2(int *arr) { std::cout << arr[1] << '\n'; }

template <int N> void func3(std::array<int, N> arr) {
  std::cout << arr[1] << '\n';
}

int main() {
  std::vector<int> vec({0, 1, 2, 3});
  func1(vec.data());
  func2(vec.data());

  func1(&vec[0]);
  func2(&vec[0]);

  // fails
  // func3(vec.data());
}
