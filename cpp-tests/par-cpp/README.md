# par

How to parallelise a `for` loop in C++?

* OpenMP
* Threads
* jthread? https://en.cppreference.com/w/cpp/thread/jthread
* async/futures? https://en.cppreference.com/w/cpp/header/future
* libraries
    * [TaskFlow](https://taskflow.github.io/)
