#include <iostream>

int main()
{
    // can't const enum
    enum Number {
        Zero,
        One,
        size
    };

    std::cout << Number::size << '\n';
}
