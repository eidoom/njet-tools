#include <array>
#include <functional>
#include <iostream>

struct Test2 {
    void ret3(std::function<float(int)> obj);
};

struct Test : Test2 {
    Test();

    using ReturnType = float;
    using ParameterType = int;

    typedef ReturnType (Test::*FnPtrType)(ParameterType p);

    ReturnType func(ParameterType p);

    void ret();
    void ret2();

    std::array<FnPtrType, 1> arr;
    std::array<std::function<ReturnType(ParameterType)>, 1> arr2;
};

Test::Test()
    : arr()
{
    arr[0] = &Test::func;
    arr2[0] = [this](ParameterType i) { return func(i); };
}

Test::ReturnType Test::func(ParameterType p)
{
    return static_cast<ReturnType>(p);
}

void Test::ret()
{
    std::cout << (this->*arr[0])(2) << '\n';
}

void Test::ret2()
{
    std::cout << arr2[0](4) << '\n';
    ret3(arr2[0]);
}

void Test2::ret3(std::function<float(int)> obj)
{
    std::cout << obj(4) << '\n';
}

void efunc(int i)
{
    std::cout << "Hi " << i << "!\n";
}

int main()
{
    {
        typedef void (*FunctionPointer)(int);

        std::array<FunctionPointer, 2> func_ptrs;

        func_ptrs[0] = &efunc;
        func_ptrs[1] = &efunc;

        func_ptrs[0](1);
        func_ptrs[1](2);
    }

    {
        Test test;

        Test::FnPtrType fn = test.arr[0];
        Test::ReturnType ans = (test.*fn)(1);
        std::cout << ans << '\n';

        test.ret();

        test.ret2();
    }

    std::array<std::function<void()>, 1> f1;
    f1[0] = std::bind(efunc, 0);
    f1[0]();

    std::array<std::function<void()>, 1> f2;
    f2[0] = []() { return efunc(3); };
    f2[0]();
}
