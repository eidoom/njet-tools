#include <complex>
#include <iostream>

template <typename T>
struct Class {
    using C = std::complex<T>;

    C func(C in);
};

template <typename T>
typename Class<T>::C Class<T>::func(C in)
{
    return 2. * in;
}

int main()
{
    Class<double> test;
    std::cout << test.func(std::complex<double>(2., 1.)) << '\n';
}
