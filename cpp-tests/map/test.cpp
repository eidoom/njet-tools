#include <array>
#include <iostream>
#include <string>
#include <unordered_map>

int func(const std::unordered_map<std::string, std::array<int, 2>> &ass,
         const std::string &key) {
    // const needs at
  return ass.at(key)[0];
}

int main() {
  const std::unordered_map<std::string, std::array<int, 2>> ass({
      {"one", {1, 1}},
      {"two", {2, 2}},
  });

  std::cout << func(ass, "one") << '\n';

  std::unordered_map<std::string, std::array<int, 2>> ass2({
      {"one", {1, 1}},
      {"two", {2, 2}},
  });

  // [] requires mutable
  std::cout << ass2["two"][1] << '\n';
}
