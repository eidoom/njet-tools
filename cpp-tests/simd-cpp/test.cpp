#include <algorithm>
#include <cmath>
#include <execution>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>

#include <Eigen/Dense>

#include "common.hpp"

template <typename T>
void run(const int n)
{
    std::vector<T> a(n);
    std::vector<T> b(n);

    std::iota(a.begin(), a.end(), T(1));
    std::ranges::generate(b, [n = 1]() mutable { return std::log(++n); });

    Eigen::VectorX<double> ea(n);
    for (int i { 0 }; i < n; ++i) {
        ea[i] = a[i];
    }
    Eigen::VectorXd eb(n);
    for (int i { 0 }; i < n; ++i) {
        eb[i] = b[i];
    }

    const int N { 100 };
    Timer timer;

    // seq
    timer.start();
    T val_seq { 0 };
    for (int i { 0 }; i < N; ++i) {
        val_seq += std::inner_product(a.cbegin(), a.cend(), b.cbegin(), T());
    }
    val_seq /= N;
    long dur_seq { timer.split_us() / N };

    timer.start();
    T val_unseq { 0 };
    for (int i { 0 }; i < N; ++i) {
        val_unseq += std::transform_reduce(std::execution::unseq, a.cbegin(), a.cend(), b.cbegin(), T());
    }
    val_unseq /= N;
    long dur_unseq { timer.split_us() / N };

    timer.start();
    T val_par { 0 };
    for (int i { 0 }; i < N; ++i) {
        val_par += std::transform_reduce(std::execution::par, a.cbegin(), a.cend(), b.cbegin(), T());
    }
    val_par /= N;
    long dur_par { timer.split_us() / N };

    timer.start();
    T val_par_unseq { 0 };
    for (int i { 0 }; i < N; ++i) {
        val_par_unseq += std::transform_reduce(std::execution::par_unseq, a.cbegin(), a.cend(), b.cbegin(), T());
    }
    val_par_unseq /= N;
    long dur_par_unseq { timer.split_us() / N };

    timer.start();
    T val_eigen { 0 };
    for (int i { 0 }; i < N; ++i) {
        val_eigen += ea.dot(eb);
    }
    val_eigen /= N;
    long dur_eigen { timer.split_us() / N };

    const double fastest = std::min({ dur_seq, dur_unseq, dur_par, dur_par_unseq, dur_eigen });

    std::cout << "      seq:  v=" << val_seq << "  t=" << std::setw(6) << dur_seq << "us"
              << "  r=" << dur_seq / fastest << '\n'
              << "    unseq:  v=" << val_unseq << "  t=" << std::setw(6) << dur_unseq << "us"
              << "  r=" << dur_unseq / fastest << '\n'
              << "      par:  v=" << val_par << "  t=" << std::setw(6) << dur_par << "us"
              << "  r=" << dur_par / fastest << '\n'
              << "par_unseq:  v=" << val_par_unseq << "  t=" << std::setw(6) << dur_par_unseq << "us"
              << "  r=" << dur_par_unseq / fastest << '\n'
              << "    eigen:  v=" << val_eigen << "  t=" << std::setw(6) << dur_eigen << "us"
              << "  r=" << dur_eigen / fastest << '\n';
}

int main(int argc, char* argv[])
{
    std::cout << '\n';

    if (argc != 2) {
        std::cerr << "Error: wrong number of arguments!\nProvide the size of the vectors.\n";
    }

    const int n { std::atoi(argv[1]) };

    run<double>(n);

    std::cout << '\n';
}
