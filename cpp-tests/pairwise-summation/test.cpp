#include <algorithm>
#include <cmath>
#include <execution>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>

#include <Eigen/Dense>
#include <qd/dd_real.h>
#include <qd/qd_real.h>

#include "common.hpp"

template <typename T>
T sequential_summation(const T* v, const int n)
{
    T r {};
    for (int i { 0 }; i < n; ++i) {
        r += v[i];
    }
    return r;
}

template <typename T>
T sequential_summation(const std::vector<T>& v)
{
    return sequential_summation(v.data(), v.size());
}

// N=128 is copied from Julia & NumPy
static constexpr int N { 128 };

template <typename T>
T pairwise_summation_recursive(const T* v, const int n)
{
    if (n <= N) {
        return sequential_summation(v, n);
    } else {
        int m { n / 2 };
        return pairwise_summation_recursive(v, m) + pairwise_summation_recursive(v + m, n - m);
    }
}

template <typename T>
T pairwise_summation_recursive(const std::vector<T>& v)
{
    return pairwise_summation_recursive(v.data(), int(v.size()));
}

// TODO implement base case?
// ie. switch to sequential sum for small sub-blocks
// iterative is slower, is it because there's no base case?
template <typename T>
T pairwise_summation_iterative(const T* v, const int n)
{
    // array of partial sums s[i] holds sums of 2^i values
    // eg 11 = b1101 values accumulated: s[3]=first 8, s[1]=next 2, s[0]=last one
    // size array for largest int (sizeof(int) = 4 bytes = 32 bits)
    std::array<T, 8 * sizeof(int)> s;

    // position/place in v
    int p;

    for (int i { 0 }; i < n; ++i) {
        T a { v[i] };
        p = 0;
        while (i & (1 << p)) {
            a += s[p];
            ++p;
        }
        s[p] = a;
    }

    T r {};
    p = 0;

    // don't look at entries in s beyond the size required for n
    while ((1 << p) <= n) {
        // use bitmask to only add non-zero entries
        // (we didn't initialise the others)
        if (n & (1 << p)) {
            r += s[p];
        }
        ++p;
    }

    return r;
}

template <typename T>
T pairwise_summation_iterative(const std::vector<T>& v)
{
    return pairwise_summation_iterative(v.data(), v.size());
}

template <typename T>
T sequential_dot(const T* a, const T* b, const int n)
{
    T r {};
    for (int i { 0 }; i < n; ++i) {
        r += a[i] * b[i];
    }
    return r;
}

template <typename T>
T sequential_dot(const std::vector<T>& a, const std::vector<T>& b)
{
    return sequential_dot(a.data(), b.data(), a.size());
}

template <typename T>
T pairwise_dot(const T* a, const T* b, const int n)
{
    if (n <= N) {
        return sequential_dot(a, b, n);
    } else {
        int m { n / 2 };
        return pairwise_dot(a, b, m) + pairwise_dot(a + m, b + m, n - m);
    }
}

template <typename T>
T pairwise_dot(const std::vector<T>& a, const std::vector<T>& b)
{
    return pairwise_dot(a.data(), b.data(), a.size());
}

template <typename T>
void row(const std::string& name, const T val, const long dur, const double best)
{
    const int num_dig { 64 };
    const int wid_dig { num_dig + 7 };

    std::cout << std::setw(13) << name << ":  v=" << std::scientific << std::setprecision(num_dig) << std::setw(wid_dig) << val << "  t=" << std::fixed << std::setprecision(1) << std::setw(6) << dur << "us"
              << "  r=" << dur / best << '\n';
}

template <typename T>
void run(const int n)
{
    const int N { 100 };
    Timer timer;

    std::vector<T> z(n);
    std::ranges::generate(z, [n = 1]() mutable { return std::sqrt(++n); });

    // warm up
    T sum_seq;
    for (int i { 0 }; i < N; ++i) {
        sum_seq = sequential_summation(z);
    }

    timer.start();
    for (int i { 0 }; i < N; ++i) {
        sum_seq = sequential_summation(z);
    }
    long dur_sum_seq { timer.split_us() / N };

    timer.start();
    T sum_pair_rec;
    for (int i { 0 }; i < N; ++i) {
        sum_pair_rec = pairwise_summation_recursive(z);
    }
    long dur_sum_pair_rec { timer.split_us() / N };

    timer.start();
    T sum_pair_it;
    for (int i { 0 }; i < N; ++i) {
        sum_pair_it = pairwise_summation_iterative(z);
    }
    long dur_sum_pair_it { timer.split_us() / N };

    const double fastest_sum = std::min({ dur_sum_seq, dur_sum_pair_rec, dur_sum_pair_it });

    std::cout << "Sum" << '\n';
    row("sequential", sum_seq, dur_sum_seq, fastest_sum);
    row("rec pairwise", sum_pair_rec, dur_sum_pair_rec, fastest_sum);
    row("it pairwise", sum_pair_it, dur_sum_pair_it, fastest_sum);
    std::cout << "Dot (scalar product)" << '\n';

    std::vector<T> a(n);
    std::vector<T> b(n);

    // std::iota(a.begin(), a.end(), T(1));// qd doesn't support
    std::ranges::generate(a, [n = 1]() mutable { return std::sin(++n); });
    std::ranges::generate(b, [n = 1]() mutable { return std::log(++n); });

    Eigen::VectorX<T> ea(n);
    for (int i { 0 }; i < n; ++i) {
        ea[i] = a[i];
    }

    Eigen::VectorX<T> eb(n);
    for (int i { 0 }; i < n; ++i) {
        eb[i] = b[i];
    }

    // std dot product
    timer.start();
    T val_naive;
    for (int i { 0 }; i < N; ++i) {
        val_naive = std::inner_product(a.cbegin(), a.cend(), b.cbegin(), T());
    }
    long dur_naive { timer.split_us() / N };

    // sequential dot product
    timer.start();
    T val_seq;
    for (int i { 0 }; i < N; ++i) {
        val_seq = sequential_dot(a, b);
    }
    long dur_seq { timer.split_us() / N };

    // pairwise summation
    timer.start();
    T val_pair;
    for (int i { 0 }; i < N; ++i) {
        val_pair = pairwise_dot(a, b);
    }
    long dur_pair { timer.split_us() / N };

    // eigen dot product
    timer.start();
    T val_eigen;
    for (int i { 0 }; i < N; ++i) {
        val_eigen = ea.dot(eb);
    }
    long dur_eigen { timer.split_us() / N };

    const double fastest = std::min({ dur_naive, dur_pair, dur_eigen });

    row("std", val_naive, dur_naive, fastest);
    row("sequential", val_seq, dur_seq, fastest);
    row("pairwise", val_pair, dur_pair, fastest);
    row("eigen", val_eigen, dur_eigen, fastest);
}

int main(int argc, char* argv[])
{
    std::cout << '\n';

    if (argc != 2) {
        std::cerr << "Error: wrong number of arguments!\nProvide the size of the vectors.\n";
    }

    const int n { std::atoi(argv[1]) };

    std::cout << "Summation algorithms" << '\n'
              << '\n';

    std::cout << "float = f32 (8 digits)" << '\n';
    run<float>(n);

    std::cout << '\n'
              << "double = f64 (16 digits)" << '\n';
    run<double>(n);

    std::cout << '\n'
              << "long double = f80 (18 digits)" << '\n';
    run<long double>(n);

    std::cout << '\n'
              << "double double = f128 (32 digits)" << '\n';
    run<dd_real>(n);

    std::cout << '\n'
              << "quad double = f256 (64 digits)" << '\n';
    run<qd_real>(n);

    std::cout << '\n';
}
