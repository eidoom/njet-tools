#include <algorithm>
#include <chrono>
// #include <cmath>
#include <complex>
#include <execution>
#include <iostream>
#include <numeric>
#include <valarray>
#include <vector>

#include <qd/qd_real.h>

#include "common.hpp"

template <typename T>
void run()
{
    Timer timer;
    const int sze { 10000000 };

    timer.start();
    std::vector<std::complex<T>> vec0(sze);
    // std::iota(vec0.begin(), vec0.end(), T());
    std::generate(vec0.begin(), vec0.end(), [n = std::complex<T>()]() mutable { return n += std::complex<T>(T(2.), T(1.)); });
    std::cout << timer.split_ms() << " ms" << '\n';

    timer.start();
    std::vector<std::complex<T>> vec1(sze);
    // std::iota(vec1.begin(), vec1.end(), T());
    std::generate(vec1.begin(), vec1.end(), [n = std::complex<T>()]() mutable { return n += std::complex<T>(T(2.), T(1.)); });
    std::cout << timer.split_ms() << " ms" << '\n';

    timer.start();
    std::vector<std::complex<T>> vec2(sze);
    // std::iota(vec2.begin(), vec2.end(), T());
    std::generate(vec2.begin(), vec2.end(), [n = std::complex<T>()]() mutable { return n += std::complex<T>(T(2.), T(1.)); });
    std::cout << timer.split_ms() << " ms" << '\n';

    timer.start();
    std::valarray<std::complex<T>> valarr(sze);
    // std::iota(std::begin(valarr), std::end(valarr), T());
    std::generate(std::begin(valarr), std::end(valarr), [n = std::complex<T>()]() mutable { return n += std::complex<T>(T(2.), T(1.)); });
    std::cout << timer.split_ms() << " ms" << '\n';

    std::cout << '\n';

    // serial
    timer.start();
    for (std::complex<T>& e : vec0) {
        e *= T(2.);
        // e *= std::complex<T>(2.); // slower
    }
    std::cout << timer.split_ms() << " ms" << '\n';

    // vectorised
    timer.start();
    std::for_each(
        std::execution::unseq,
        vec1.begin(),
        vec1.end(),
        [](std::complex<T>& e) {
            e *= T(2.);
        });
    std::cout << timer.split_ms() << " ms" << '\n';

    // parallel and vectorised
    timer.start();
    std::for_each(
        std::execution::par_unseq,
        vec2.begin(),
        vec2.end(),
        [](std::complex<T>& e) {
            e *= T(2.);
        });
    std::cout << timer.split_ms() << " ms" << '\n';

    // valarray
    timer.start();
    valarr *= T(2.);
    std::cout << timer.split_ms() << " ms" << '\n';
}

// int main(int argc, char* argv[])
int main()
{
    std::cout << '\n';
    // std::cout.precision(16);
    // std::cout.setf(std::ios_base::scientific);

    run<qd_real>();
    std::cout << '\n';
}
