#include <iostream>

int main()
{
    // create a value a
    int a { 1 };
    std::cout << "a: " << a << '\n';
    // a: 1

    // create a pointer b to value a
    int* b { &a };

    // notice that we have to dereference b to access the value
    std::cout << "b: " << b << '\n';
    // b: 0x7ffeaf232ebc
    std::cout << "a: " << a << " b: " << *b << '\n';
    // a: 1 b: 1

    // we can change the value of a (and therefore its pointer b) directly
    a = 2;
    std::cout << "a: " << a << " b: " << *b << '\n';
    // a: 2 b: 2

    // we can change the value of a (and therefore its pointer b) through its pointer b
    *b = 3;
    std::cout << "a: " << a << " b: " << *b << '\n';
    // a: 3 b: 3

    // let's introduce a new value aa and pointer bb to aa
    int aa { 4 };
    int* bb { &aa };
    std::cout << "a: " << a << " b: " << *b << " aa: " << aa << " bb: " << *bb << '\n';
    // a: 3 b: 3 aa: 4 bb: 4

    // this will set the value of a (through pointer b) to be the value of aa (through pointer bb)
    *b = *bb;
    std::cout << "a: " << a << " b: " << *b << " aa: " << aa << " bb: " << *bb << '\n';
    // a: 4 b: 4 aa: 4 bb: 4

    // this will set pointer b to point at aa (same as pointer bb)
    a = 5;
    b = bb;
    std::cout << "a: " << a << " b: " << *b << " aa: " << aa << " bb: " << *bb << '\n';
    // a: 5 b: 4 aa: 5 bb: 5

    // we make a new const-valued pointer c to a
    const int* c { &a };
    // int const * c { &a }; // is also valid
    std::cout << "a: " << a << " b: " << *b << " aa: " << aa << " bb: " << *bb << " c: " << *c << '\n';
    // a: 5 b: 4 aa: 4 bb: 4 c: 5

    // *c is read-only
    // *c = 6;
    // error: assignment of read-only location ‘* c’

    // but we can point c at something else
    c = &aa;
    std::cout << "a: " << a << " b: " << *b << " aa: " << aa << " bb: " << *bb << " c: " << *c << '\n';
    // a: 5 b: 4 aa: 4 bb: 4 c: 4

    // let us make a const-pointer d to a
    int *const d { &a };
    std::cout << "a: " << a << " b: " << *b << " aa: " << aa << " bb: " << *bb << " c: " << *c << " d: " << *d << '\n';
    // a: 5 b: 4 aa: 4 bb: 4 c: 4 d: 5

    // we can write to the value
    *d = 6;
    std::cout << "a: " << a << " b: " << *b << " aa: " << aa << " bb: " << *bb << " c: " << *c << " d: " << *d << '\n';
    // a: 6 b: 4 aa: 4 bb: 4 c: 4 d: 6

    // but we can't move the pointer
    // d = &aa;
    // error: assignment of read-only variable ‘d’

    // we can also make a const-valued const-pointer
    const int *const e { &a };
    // int const * const e { &a }; // is also valid
    std::cout << "a: " << a << " b: " << *b << " aa: " << aa << " bb: " << *bb << " c: " << *c << " d: " << *d << " e: " << *e << '\n';
    // a: 6 b: 4 aa: 4 bb: 4 c: 4 d: 6 e: 6

    // *e is read-only
    // *e = 6;
    // error: assignment of read-only location ‘* e’

    // and we can't move the pointer
    // e = &aa;
    // error: assignment of read-only variable ‘e’

    // if we have a reference (to value a through dereferenced b)
    int& f { *b };
    std::cout << "a: " << a << " b: " << *b << " aa: " << aa << " bb: " << *bb << " c: " << *c << " d: " << *d << " e: " << *e << " f: " << f << '\n';
    // a: 6 b: 4 aa: 4 bb: 4 c: 4 d: 6 e: 6 f: 6

    // we can make a pointer to the reference
    int* g { &f };
    std::cout << "a: " << a << " b: " << *b << " aa: " << aa << " bb: " << *bb << " c: " << *c << " d: " << *d << " e: " << *e << " f: " << f << " g: " << *g << '\n';
    // a: 6 b: 4 aa: 4 bb: 4 c: 4 d: 6 e: 6 f: 6 g: 6

    // we cannot reference a pointer (that's a type error)
    // int& h { b };
    // error: invalid conversion from ‘int*’ to ‘int’
    // error: cannot bind rvalue ‘(int)b’ to ‘int&’

    // I'll need a const reference for the following
    const int& h { a };
    std::cout << "a: " << a << " b: " << *b << " aa: " << aa << " bb: " << *bb << " c: " << *c << " d: " << *d << " e: " << *e << " f: " << f << " g: " << *g << " h: " << h << '\n';
    // a: 6 b: 4 aa: 4 bb: 4 c: 4 d: 6 e: 6 f: 4 g: 4 h: 6

    // const type errors:

    // can't make a (mutable) reference to a dereferenced const-valued pointer
    // int& h { *c };
    // error: binding reference of type ‘int&’ to ‘const int’ discards qualifiers

    // can't make a (mutable-valued) pointer to a const reference
    // int* i { &h };
    // error: invalid conversion from ‘const int*’ to ‘int*’
}
