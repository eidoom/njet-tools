#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <qd/dd_real.h>

#include "chsums/NJetAccuracy.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

#include "finrem/0q5g/ggggg.h"
#include "finrem/2q3g/ggqbqg.h"
#include "finrem/2q3g/qbgqgg.h"
#include "finrem/2q3g/qbqggg.h"
#include "finrem/4q1g/uDdUg.h"
#include "finrem/4q1g/uUdDg.h"
#include "finrem/4q1g/uUuUg.h"
#include "finrem/4q1g/udUDg.h"
#include "finrem/4q1g/ugUdD.h"
#include "finrem/4q1g/ugUuU.h"
#include "finrem/4q1g/uuUUg.h"

template <template <typename, typename> class AMP> dd_real run() {
  const std::vector<double> scales2{{0}};
  const double sqrtS{1.}, mur{91.188}, mur2{std::pow(mur, 2)};
  const int legs{5}, Nc{3}, Nf{5}, rseed{1};

  NJetAccuracy<double> *const ampDD{
      NJetAccuracy<double>::template create<AMP<double, double>>()};
  ampDD->setMuR2(mur2);
  ampDD->setNf(Nf);
  ampDD->setNc(Nc);

  NJetAccuracy<dd_real> *const ampQD{
      NJetAccuracy<dd_real>::template create<AMP<dd_real, double>>()};
  ampQD->setMuR2(mur2);
  ampQD->setNf(Nf);
  ampQD->setNc(Nc);

  PhaseSpace<double> psD(legs, rseed, sqrtS);
  std::vector<MOM<double>> momD{psD.getPSpoint()};
  refineM(momD, momD, scales2);

  ampDD->setMomenta(momD);

  ampDD->setSpecFuncs();
  ampDD->initFinRem();

  ampDD->c2lx0l_fin();

  const double res{ampDD->c2lx0l_fin_value()};

  PhaseSpace<dd_real> psQ(legs, rseed, sqrtS);
  std::vector<MOM<dd_real>> momQ{psQ.getPSpoint()};
  refineM(momQ, momQ, scales2);

  ampQD->setMomenta(momQ);

  ampQD->copySpecFuncs(ampDD);
  ampQD->initFinRem();

  ampQD->c2lx0l_fin();

  return ampDD->c2lx0l_fin_value() / ampQD->c2lx0l_fin_value();
}

int main() {
  std::cout << "uUdDg\n" << run<Amp4q1g_uUdDg_a2l>() << '\n' << '\n';
  std::cout << "uDdUg\n" << run<Amp4q1g_uDdUg_a2l>() << '\n' << '\n';
  std::cout << "uUuUg\n" << run<Amp4q1g_uUuUg_a2l>() << '\n' << '\n';
}
