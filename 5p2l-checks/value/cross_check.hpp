#pragma once

#include <array>
#include <string>
#include <unordered_map>

namespace xc {
extern const std::unordered_map<std::string, std::array<double, 5>> values;
}
