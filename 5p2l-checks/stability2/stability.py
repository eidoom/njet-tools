#!/usr/bin/env python3

import argparse, math

import numpy

import reader, plotter


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "analyse stability of 5p 2L run over NJet phase space"
    )
    parser.add_argument(
        "-r", "--refresh-data", action="store_true", help="Don't use cached data"
    )
    parser.add_argument("-d", "--data", default="data_100k", help="Set data directory")
    args = parser.parse_args()

    print("Using", args.data)

    all_5p = reader.read_proc(args.data, "result", use_cache=not args.refresh_data)

    print("Plotting...")

    plotter.init_plots()

    channels = all_5p["f64f64"].keys()

    maxs = []
    mins = []
    for channel in channels:
        maxs.append(max(all_5p["f64f64"][channel][:, 8]))
        if (
            isinstance(all_5p["f128f128"][channel], numpy.ndarray)
            and all_5p["f128f128"][channel].size
        ):
            mins.append(min(all_5p["f128f128"][channel][:, 8]))

    max_pow = math.ceil(math.log10(max(maxs)))
    min_pow = math.floor(math.log10(min(mins)))

    for channel in channels:
        plotter.histo_single(
            data=all_5p,
            channel=channel,
            xlabel="Correct digits",
            x_min_pow=min_pow,
            x_max_pow=max_pow,
            label="prelim_stability",
            xexps=True,
        )
