# Python library

import csv, pickle, pathlib, collections

import numpy


def read(filename):
    if not pathlib.Path(filename).is_file():
        print(f"File {filename} not found")
        exit()

    data = collections.defaultdict(list)

    with open(filename, "r") as filecont:
        for row in csv.reader(filecont, delimiter=" "):
            # TODO 0 <-> 1 and strip trailing ' ' in cpp
            data[row[1]].append(
                numpy.array([row[0], *[a for a in row[2:] if a != ""]], dtype="float64")
            )

    for channel, results in data.items():
        results = numpy.array(results)
        data[channel] = results[results[:, 0].argsort()]

    return data


def write(filename, data):
    picklefile = filename + ".pickle"
    if pathlib.Path(picklefile).is_file():
        print(f"File {picklefile} already exists.")
        while True:
            decision = input("Overwrite? [y/n] ")
            if decision == "n":
                return None
            elif decision == "y":
                break
    with open(picklefile, "wb") as f:
        pickle.dump(data, f)


def read_pickle(filename):
    picklefile = filename + ".pickle"
    if pathlib.Path(picklefile).is_file():
        with open(picklefile, "rb") as f:
            return pickle.load(f)


def read_proc(folder, name="result", use_cache=True, old=False):
    precs = ["f64f64", "f128f64", "f128f128"]

    data = {prec: read_memo(f"{folder}/{name}.{prec}.csv", use_cache) for prec in precs}

    print(f"Length of dataset: {len(data['f64f64']['ggggg'])}")

    return data


def read_memo(filename, use_cache):
    if use_cache:
        data = read_pickle(filename)
        if data is None:
            data = read(filename)
            write(filename, data)
    else:
        data = read(filename)
        write(filename, data)
    return data
