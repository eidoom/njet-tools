#!/usr/bin/env python3

import argparse

import numpy

import reader, plotter


def print_results(channel, t_sf, t_c, latex=False):
    d_sf = numpy.average(t_sf) / 1e6
    d = numpy.average(t_sf + t_c) / 1e6

    if latex:
        print(
            plotter.CHAN[channel],
            "&",
            round(d, 2),
            "&",
            int(round(100 * d_sf / d, 0)),
            "\\\\",
        )
    else:
        print(channel, f"{round(d, 2)}s", f"{int(round(100 * d_sf / d, 0))}%")


def timing_rescue(
    data,
    i_err,
    i_sf,
    i_c,
    cutoff=1e-3,
    latex=False,
):
    channels = data["f64f64"].keys()

    for channel in channels:
        t_sf = []
        t_c = []

        for i, errDD, tDD_sf, tDD_c in data["f64f64"][channel][
            :, [0, i_err, i_sf, i_c]
        ]:
            tt_sf = tDD_sf
            tt_c = tDD_c

            if errDD > cutoff:
                matchQD = data["f128f64"][channel][data["f128f64"][channel][:, 0] == i]
                # this checks if there exists a QD result
                # this won't be true if one was running when I killed the data generation jobs
                if matchQD.size > 0:
                    rQD = matchQD[0, [i_sf, i_c, i_err]]

                    tt_sf += rQD[0]
                    tt_c += rQD[1]

                    if rQD[2] > cutoff:
                        matchQQ = data["f128f128"][channel][
                            data["f128f128"][channel][:, 0] == i
                        ]
                        # this checks if there exists a QQ result
                        if matchQQ.size > 0:
                            rQQ = matchQQ[0, [i_sf, i_c, i_err]]

                            tt_sf += rQQ[0]
                            tt_c += rQQ[1]

                            if rQQ[2] > cutoff:
                                print(
                                    f"Warning: rseed {int(i)} fails to achieve accuracy {cutoff} at f128/f128"
                                )
                        else:
                            print(f"Error: rseed {int(i)} is missing f128/f128 result!")
                else:
                    print(f"Error: rseed {int(i)} is missing f128/f64 result!")

            t_sf.append(tt_sf)
            t_c.append(tt_c)

        t_sf = numpy.array(t_sf)
        t_c = numpy.array(t_c)

        print_results(channel, t_sf, t_c, latex)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "analyse timing of 5p 2L run over NJet phase space"
    )
    parser.add_argument(
        "-r", "--refresh-data", action="store_true", help="Don't use cached data"
    )
    parser.add_argument("-l", "--latex", action="store_true", help="Print out in LaTeX")
    parser.add_argument("-d", "--data", default="data_100k", help="Set data directory")
    args = parser.parse_args()

    print("Using", args.data)

    all_5p = reader.read_proc(args.data, "result", use_cache=not args.refresh_data)

    print("f64f64")
    for channel, data in all_5p["f64f64"].items():
        t_sf, t_c = numpy.transpose(data[:, [9, 10]])
        print_results(channel, t_sf, t_c, args.latex)

    print("rescue")
    timing_rescue(all_5p, 8, 9, 10, 1e-3, args.latex)
