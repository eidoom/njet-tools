#!/usr/bin/env bash

strt=$1
points=$2
raw_cores=$3
channels=11
cores=$((raw_cores / channels))
quotient=$((points / cores))
remainder=$((points % cores))

first_cores=$((cores - remainder))
first_each=$quotient
first_total=$((first_cores * first_each))
first_start=$strt
first_end=$((strt + first_total - 1))

second_cores=$remainder
second_each=$((quotient + 1))
second_total=$((second_cores * second_each))
second_start=$((first_end + 1))
second_end=$((second_start + second_total - 1))

last_point=$(($strt + $points - 1))

log="log.main.$1-$2-$3"
echo "Requested $points points over $raw_cores cores, starting from random number seed $strt" >${log}
echo "There are $channels channels, so do $cores runs of process" >>${log}
echo "So get rseeds $strt to $last_point" >>${log}
echo "With $first_cores runs ($((first_cores * channels)) cores) doing $first_each points each ($first_total points: rseed $first_start to $first_end)" >>${log}
echo "And $second_cores runs ($((second_cores * channels)) cores) doing $second_each points each ($second_total points: rseed $second_start to $second_end)" >>${log}
cat ${log}

make -j

five_parton() {
	./ggggg $1 $2 >"log.ggggg.$1-$2" 2>"err.ggggg.$1-$2" &

	./qbqggg $1 $2 >"log.qbqggg.$1-$2" 2>"err.qbqggg.$1-$2" &
	./qbgqgg $1 $2 >"log.qbgqgg.$1-$2" 2>"err.qbgqgg.$1-$2" &
	./ggqbqg $1 $2 >"log.ggqbqg.$1-$2" 2>"err.ggqbqg.$1-$2" &

	./uUdDg $1 $2 >"log.uUdDg.$1-$2" 2>"err.uUdDg.$1-$2" &
	./uDdUg $1 $2 >"log.uDdUg.$1-$2" 2>"err.uDdUg.$1-$2" &
	./udUDg $1 $2 >"log.udUDg.$1-$2" 2>"err.udUDg.$1-$2" &
	./ugUdD $1 $2 >"log.ugUdD.$1-$2" 2>"err.ugUdD.$1-$2" &

	./uUuUg $1 $2 >"log.uUuUg.$1-$2" 2>"err.uUuUg.$1-$2" &
	./uuUUg $1 $2 >"log.uuUUg.$1-$2" 2>"err.uuUUg.$1-$2" &
	./ugUuU $1 $2 >"log.ugUuU.$1-$2" 2>"err.ugUuU.$1-$2" &
}

for ((i = $first_start; i <= $first_end; i += $first_each)); do
	five_parton ${i} $((${i} + ${first_each}))
done
for ((i = $second_start; i <= $second_end; i += $second_each)); do
	five_parton ${i} $((${i} + ${second_each}))
done
