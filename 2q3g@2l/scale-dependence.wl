(* ::Package:: *)

(* A[nf,Nc] *)
(* Nf=nf/Nc *)


fixconstants = {\[Beta]0->11/3-(2 Nf)/3,\[Beta]1->34/3-(13 Nf)/3};


expr1=3/2*\[Beta]0*A0*Log[MU2]/.fixconstants//Collect[#,Nf]&


expr2=15/8 \[Beta]0^2 A[0,0] Log[MU2]^2+
	1/2 Log[MU2]*(A[0,0](949/108-(292 Nf)/27+(20 Nf^2)/9+3 \[Beta]1+20 Zeta[3])+
    5 \[Beta]0 (A[0,1]+Nf A[1,0]))/.fixconstants//Collect[#,Nf]&


(* A[0,1] *)
Coefficient[expr1, Nf,0]


(* A[1,0] *)
Coefficient[expr1, Nf,1]


(* A[0,2] *)
Coefficient[expr2, Nf, 0]//Collect[#,A[0,0]]&


(* A[1,1] *)
Coefficient[expr2, Nf, 1]//Collect[#,A[0,0]]&


(* A[2,0] *)
Coefficient[expr2, Nf, 2]//Collect[#,A[0,0]]&
