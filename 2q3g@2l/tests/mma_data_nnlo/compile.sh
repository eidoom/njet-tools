#!/usr/bin/env bash

i=0
for f in NJcompare_*_NNLO_nf*_[01]*.m; do
	end=${f#NJcompare_}
	strt=${end%.m}
	channel=${strt%_NNLO_nf*_*}
	# echo $channel
	strt2=${strt#*_NNLO_nf}
	nfp=${strt2%_*}
	# echo $nfp
	contrib=${strt2#*_}
	# echo $channel $contrib $nfp

	i=$((i+1))
	i=$((i%6))
	if [ $i -eq 1 ]; then
		echo -e "\n\n\n\n\n\n\n\n\n\n"
	fi

	cat $f
	echo ","
done
