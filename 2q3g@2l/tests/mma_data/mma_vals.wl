(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/2q3g@2l/tests/from_simon"];


(* ::Section:: *)
(*qbqggg*)


lo=Get["NJcompare_qbqggg_LO.m"];
nlo=Get["Mcompare_qbqggg_NLO.m"];


Total/@lo


Total/@nlo


(* ::Section:: *)
(*qbgqgg*)


l1nf0=Get["Mcompare_qbgqgg_NLO_nf0.m"];
l1nf1=Get["Mcompare_qbgqgg_NLO_nf1.m"];


l1=l1nf0+l1nf1


Table[If[Mod[i,2]==1,-1,1]*l1nf0[[i]],{i,Length[l1nf0]}]
