(* ::Package:: *)

(* ::Section::Closed:: *)
(*setup*)


(* ::Subsection::Closed:: *)
(*imports*)


<< FiniteFlow`
<< "InitTwoLoopToolsFF.m"
<< "InitDiagramsFF.m"
<< "setupfiles/Setup_2q3g.m"
Get["/home/ryan/git/ffamps_tools/ffamps_mathtools.wl"];
SetDirectory["/home/ryan/git/njet-tools/2q3g@2l/tests"];


(* ::Subsection::Closed:: *)
(*helper functions*)


MySparseDot[a_,b_MySparseMatrix,c_]:=Module[{dims,coeffs,res},
dims = b[[1]];
coeffs = a;
res = b[[2]] /. Rule[m[i_,j_],val_]:>coeffs[[i]]*val*c[[j]];
Return[Plus@@res];
]


EvalAmpData[coeffvecs_,funcvecs_,matrices_,components_,coeffrules1_,coeffrules2_,pfvalues_,mtvalues_]:=Module[{ccc,evalpfmonos,fff,res},
ccc = coeffvecs /. Dispatch[coeffrules1 /. Dispatch[coeffrules2 /. Dispatch[mtvalues]]];
evalpfmonos = AllPfuncMonomials /. Dispatch[pfvalues];
fff = funcvecs /. i_Integer:>evalpfmonos[[i]];
res = Table[Map[MySparseDot[ccc[[i]],#,fff[[i]]]&,matrices[[i]],{1}],{i,1,Length[coeffvecs]}];
Return[res];
];


Helicity[expr_]:= Module[{tt,l},
  tt = Simplify[(expr/.{spA[i_,j_]:>spA[i,j]/l[i]/l[j],spAA[i_,aa__,j_]:>spAA[i,aa,j]/l[i]/l[j],
    spB[i_,j_]:>spB[i,j]*l[i]*l[j],spBB[i_,aa__,j_]:>spBB[i,aa,j]*l[i]*l[j]})/expr];
  Table[Exponent[tt,l[kk]]/2,{kk,1,5}]]
  
Dimension[expr_]:=Module[{tt,l},
  tt = Simplify[(expr /. {spA[ii__]:>l spA[ii],spB[ii__]:>l spB[ii],s[ii__]:>l^2 s[ii],
          spAA[ii__]:>l^3 spAA[ii],spBB[ii__]:>l^3 spBB[ii]})/expr];
  If[SubsetQ[{l},Variables[tt]], Exponent[tt,l], $Failed]
  ]


(* ::Subsection::Closed:: *)
(*phase space points from NJet *)


NJpoints = {
{
{-5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01)},
{-5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),-5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01)},
{4.1073910982315066348366583596153313060445606953488138317670996975*10^(-01),2.0393336282060187875969689603056336410669948502908012872784752640*10^(-01),-3.5476001585959550819520387343562283172156314082837886676816954376*10^(-01),-3.5540554501787400909464813475556877925884131778543931518975391992*10^(-02)},
{2.3727316959679566082666512912901236627837762893951052847505653895*10^(-01),6.2348782873773384696615939988573771279175806522951082020998271084*10^(-02),1.6215556401520544732571078943500205666936605083661823976653862996*10^(-01),1.6160680475641929985268139965273259584645697784608918446422418796*10^(-01)},
{3.5198772058005367568966903490945374988270966507517310636780966875*10^(-01),-2.6628214569437526345631283601914107969040139661105826939167221142*10^(-01),1.9260445184439006086949308400062373328059166878603092098375072425*10^(-01),-1.2606625025463189894321658617718755083415116124462642887372803777*10^(-01)}
}
,
{
{-5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01)},
{-5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),-5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01)},
{4.5262742135982180670592623161018545879945393169341043890722259529*10^(-02),-3.7938713674424223290383548408598527943660810468538238289051855480*10^(-02),-3.1787775991454201325584117103184015411270895788827324039477187551*10^(-03),-2.4479893859525708073873188605583829127062868619558733981061038765*10^(-02)},
{4.9174017367771933949402098883515481972456696869828057377892443873*10^(-01),1.4265227585006038534645118876645468723586299893037794122650476153*10^(-01),3.2654221902632636583009588972125654157276913300520235426175849093*10^(-01),3.3886414061796883745764169923741987980184014223666066698348385382*10^(-01)},
{4.6299708418629847983538638800382768710478529122743237167041874134*10^(-01),-1.0471356217563616205606764035785832865969154624430458519100743372*10^(-01),-3.2336344142718094569753747801093991546171685646601417662941301175*10^(-01),-3.1438424675844312938376851063182964117992235289618296270782989241*10^(-01)}
}
,
{
{-5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01)},
{-5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),0.0000000000000000000000000000000000000000000000000000000000000000*10^(+00),-5.0000000000000000000000000000000000000000000000000000000000000000*10^(-01)},
{7.6196219725659045982833524441425421910620346418894603747235146074*10^(-02),-1.9783128890417815717567801142730722684543399668021331979189084312*10^(-02),4.6667929075996414141258413175517697202759230207367751219244998359*10^(-03),-7.3435092127290249277039246490588550424656575422514091933468070687*10^(-02)},
{4.7539311855278964714856145091159706786822552374603774964569881910*10^(-01),-3.5220386979451790227539552727007667036979018824601008812600548385*10^(-01),7.9684674949917261131334956264922514714520427209664501562734516618*10^(-02),3.0919476684969786335156510853877788053098693855924571661968270264*10^(-01)},
{4.4841066172155130686860502464696803353239066418742953267971861584*10^(-01),3.7198699868493571799296332841281291508067013499666930220515154767*10^(-01),-8.4351467857516902545460797582473495573891129218595864956093733668*10^(-02),-2.3575967472240761407452586204818538580180425807770456604338821802*10^(-01)}
}
};


invariants = {s[1,2],s[2,3],s[3,4],s[4,5],s[1,5]};


PS1 = MOM4@@@NJpoints[[1]];
EvalMTanalyticRules[PS1] = {};
invariantsvalues = GetMomentumTwistorExpression[invariants,PS1]
pfvalues1 = Get["pfuncseval_NJ1.m"];
mtvalues1 = fromvars[PSanalytic] /. Rule[a_,b_]:>Rule[a,GetMomentumTwistorExpression[b,PS1]];


PS2 = MOM4@@@NJpoints[[2]];
EvalMTanalyticRules[PS2] = {};
invariantsvalues = GetMomentumTwistorExpression[invariants,PS2]
pfvalues2 = Get["pfuncseval_NJ2.m"];
mtvalues2 = fromvars[PSanalytic] /. Rule[a_,b_]:>Rule[a,GetMomentumTwistorExpression[b,PS2]];


PS3 = MOM4@@@NJpoints[[3]];
EvalMTanalyticRules[PS3] = {};
invariantsvalues = GetMomentumTwistorExpression[invariants,PS3]
pfvalues3 = Get["pfuncseval_NJ3.m"];
mtvalues3 = fromvars[PSanalytic] /. Rule[a_,b_]:>Rule[a,GetMomentumTwistorExpression[b,PS3]];


pfvalues={pfvalues1,pfvalues2,pfvalues3};
mtvalues={mtvalues1,mtvalues2,mtvalues3};


(* ::Subsection::Closed:: *)
(*phases*)


hs={"+","-"};
allhelicities=Table[h1<>h2<>h3<>h4<>h5,{h1,hs},{h2,hs},{h3,hs},{h4,hs},{h5,hs}]//Flatten


conjHel[hel_]:=StringReplace[hel,{"-"->"+", "+"->"-"}];
degree[h_]:=Min[Count[StringSplit[h,""],"-"],Count[StringSplit[h,""],"+"]];
primary[h_]:=Count[StringSplit[h,""],"-"]<3;


qbqposQ[channel_]:=Which[
	channel==="qbqggg",{1,2},
    channel==="qbgqgg",{1,3},
    channel==="ggqbqg",{3,4}
] (* momenta of qb and q *)


MHV[channel_String,heli_String]:=Module[{tmpheli,negpos,qnegpos,qpluspos,gnegpos,qbqpos},
  qbqpos=qbqposQ[channel];
  tmpheli = StringSplit[heli,""];
  negpos = Flatten[Position[tmpheli,"-"]];
  If[Length[negpos]=!=2, Return[$Failed]];
  qnegpos = Intersection[qbqpos,negpos];
  If[Length[qnegpos]=!=1, Return[$Failed], qnegpos = qnegpos[[1]]];
  qpluspos = DeleteCases[qbqpos,qnegpos][[1]];
  gnegpos = DeleteCases[negpos,qnegpos][[1]];
  Return[ spA[qnegpos,gnegpos]^3*spA[qpluspos,gnegpos]/(spA[1,2]*spA[2,3]*spA[3,4]*spA[4,5]*spA[5,1]) ]
];


UHV[channel_String, heli_String] := Module[{tmpheli,qnegpos,qpluspos,ii,jj,kk,qbqpos},
  qbqpos=qbqposQ[channel];
  tmpheli = StringSplit[heli,""];
  qnegpos = Flatten[Position[tmpheli,"-"]];
  If[Length[qnegpos]=!=1, Return[$Failed],qnegpos=qnegpos[[1]]];
  If[!MemberQ[qbqpos,qnegpos], Return[$Failed]];
  qpluspos = DeleteCases[qbqpos,qnegpos][[1]];
  {ii,jj,kk} = DeleteCases[{1,2,3,4,5},qnegpos|qpluspos];
  Return[ (spA[qnegpos,qpluspos])/(spA[qpluspos, ii]*spA[ii, jj]*spA[jj, kk]*spA[kk, qpluspos]) ]  
];


phase[channel_String, hel_String]:=If[
	degree[hel]===2,
	MHV[channel,hel],
	UHV[channel,hel]];


(* ::Section::Closed:: *)
(*numerical evaluations*)


(* ::Subsection::Closed:: *)
(*partials*)


(* ::Subsubsection::Closed:: *)
(*functions*)


partialsComplexPairs[channel_, loop_, Nf_, Nc_, psp_, helicity_]:=Module[{file,amp,tmp,p},
	file = "../"<>channel<>"/src/partials_2q3g_"<>ToString[loop]<>"L_Nfp"<>ToString[Nf]<>"_Ncp"<>ToString[Nc]<>"_tHV_"<>helicity<>".m";
	If[FileExistsQ[file],
		amp = Get[file];
		tmp = EvalAmpData[Sequence@@amp,pfvalues[[psp]],mtvalues[[psp]]];
		p=((amp[[4]] /. pflip->1) . #)&/@tmp;
		ReIm/@p,
		0&/@Range[6]
	]
];


loop[channel_,helicities_, Nf_,Nc_]:=Module[{l},
    l=Nf+Nc;
    Table[
	    Table[
			partialsCopmlexPairs[channel,l,Nf,Nc,psp,h],
			{h,helicities}
		],
		{psp,3}
	]
];


loopChannel[channel_,hels_]:=Module[{l00,l01,l10},
	AllPfuncMonomials = Get["../"<>channel<>"/src/AllPfuncMonomials.m"];
	l00=loop[channel,hels,0,0]; (* 0L *)
	l01=loop[channel,hels,0,1]; (* 1L[Nc] *)
	l10=loop[channel,hels,1,0]; (* 1L[Nf] *)
	{l00,l01,l10}
]


(* ::Subsubsection::Closed:: *)
(*qbqggg*)


helicities={
"+-+++",
"+-++-",
"+-+-+",
"+--++",
"-++++",
"-+++-",
"-++-+",
"-+-++"
};
loopChannel["qbqggg",helicities]


(* ::Subsubsection::Closed:: *)
(*qbgqgg*)


helicities2={
"++-++",
"++-+-",
"++--+",
"+--++",
"-++++",
"-+++-",
"-++-+",
"--+++"
};
out2=loopChannel["qbgqgg",helicities2];


Export["qbgqgg_parts.txt",ToString[NumberForm[out2]]];


(* ::Subsubsection::Closed:: *)
(*ggqbqg*)


helicities3={
"+++-+",
"+++--",
"++-++",
"++-+-",
"+-+-+",
"+--++",
"-++-+",
"-+-++"
};
out3=loopChannel["ggqbqg",helicities3];


Export["ggqbqg_parts.txt",ToString[NumberForm[out3]]];


(* ::Subsection::Closed:: *)
(*squared helicity amplitudes*)


name[channel_, loop_, Nf_, Nc_, psp_, helicity_]:=
	"../"<>channel<>"/src/partials_2q3g_"<>ToString[loop]<>
	"L_Nfp"<>ToString[Nf]<>"_Ncp"<>ToString[Nc]<>"_tHV_"<>helicity<>".m";
get[file_,psp_,helicity_]:=Module[
	{amp,d,A,dpf,Apf,Ac,Acpf,phase0,phase0c,phase0pf,phase0cpf},
	amp = Get[file];
	If[
		primary[helicity],
		phase0 = GetMomentumTwistorExpression[
			phase[helicity],PSanalytic]
			/. mtvalues[[psp]];
		phase0c = GetMomentumTwistorExpression[
			phase[helicity]/.spA->spB/.spAA->spBB,PSanalytic] 
			/. mtvalues[[psp]];
		d = EvalAmpData[Sequence@@amp,pfvalues[[psp]],mtvalues[[psp]]];
		A=((amp[[4]] /. pflip->1) . #)&/@d;
		Ac = phase0c*Conjugate[A/phase0];
		Return[{A,Ac}];
		,
		phase0pf = GetMomentumTwistorExpression[
			phase[helicity],PSanalytic] 
			/. (mtvalues[[psp]]/.Rule[a_ex,b_]:>Rule[a,Conjugate[b]]);
	    phase0cpf = GetMomentumTwistorExpression[
	        phase[helicity]/.spA->spB/.spAA->spBB,PSanalytic] 
            /. (mtvalues[[psp]] /. Rule[a_ex,b_]:>Rule[a,Conjugate[b]]);			
		dpf = EvalAmpData[
			Sequence@@amp,
			pfvalues[[psp]],
			mtvalues[[psp]]/.Rule[a_ex,b_]:>Rule[a,Conjugate[b]]
			];
		Apf=((amp[[4]] /. pflip->-1) . #)&/@dpf;
		Acpf = phase0cpf*Conjugate[Apf/phase0pf];
		Return[{Apf,Acpf}];
		];
	]
parts[channel_, loop_, Nf_, Nc_, psp_, helicity_]:=Module[
	{file,chelicity,cfile,dz},
	file = name[channel,loop,Nf,Nc,psp,helicity];
	If[FileExistsQ[file],
		Return[get[file,psp,helicity]];
		,
		chelicity=conjHel[helicity];
		cfile = name[channel,loop,Nf,Nc,psp,chelicity];
		If[FileExistsQ[cfile],
			Return[get[cfile,psp,chelicity]];
			,
			dz = ConstantArray[0,6];
			Return[{dz, dz}];
		]
	]
];


mhvs={"-+-++", "+-+--", "-++-+", "+--+-", 
 "-+++-", "+---+", "+--++", "-++--", 
 "+-+-+", "-+-+-", "+-++-", "-+--+"};
Module[{v0,v0c,hs},
{v0,v0c}=parts["qbqggg",0,0,0,1,#];
v0 . v0c
]&/@mhvs;
lo=-Nc^4*Total@%/.Nc->3//Chop


(* ::Subsubsection:: *)
(*qbqggg*)


phelicities = 
	{"-++++","+-+++","-+-++","-++-+","-+++-","+--++","+-+-+","+-++-"};
nzhelicities=Join[phelicities,conjHel/@phelicities];
channel="qbqggg";
Module[{v0,v0c,hs,v1nf0,v1cnf0,v1nf1,v1cnf1,v1,v1c},
{v0,v0c}=parts[channel,0,0,0,1,#];
{v1nf0,v1cnf0}=parts[channel,1,0,1,1,#];
{v1nf1,v1cnf1}=parts[channel,1,1,0,1,#];
{v1,v1c}={v1nf0,v1cnf0}+5/3*{v1nf1,v1cnf1};
2*Re[v0c . v1]
]&/@allhelicities;
nlo=Nc^4*Total@%/.Nc->3


nlo/5.1055565970648458*10^03
%//Rationalize[#,10^-3]&


(* ::Subsubsection:: *)
(*alt*)


phase["qbqggg","+-+--"]


partVec[channel_,h_,psp_,l_,Nf_,Nc_]:=Module[{file,cfile,amp,camp,cevl,evl,ch,phase0,phase0c,phase0pf,phase0cpf,A,Ac,Apf,Acpf},
	If[primary[h],
		file=name[channel,l,Nf,Nc,psp,h];
		If[FileExistsQ[file],
			amp=Get[file];
			evl=EvalAmpData[Sequence@@amp,pfvalues[[psp]],mtvalues[[psp]]];
			phase0 = GetMomentumTwistorExpression[
				phase[h],PSanalytic]
				/. mtvalues[[psp]];
			phase0c = GetMomentumTwistorExpression[
				phase[h]/.spA->spB/.spAA->spBB,PSanalytic] 
				/. mtvalues[[psp]];
			A=((amp[[4]] /. pflip->1) . #)&/@evl;
			Ac = phase0c*Conjugate[A/phase0];
			Return[{A,Ac}];
			,
			ConstantArray[ConstantArray[0,6],2]
		]
		,
		ch=conjHel[h];
		cfile=name[channel,l,Nf,Nc,psp,ch];
		If[FileExistsQ[cfile],
			camp=Get[cfile];
			phase0pf = GetMomentumTwistorExpression[
				phase[ch],PSanalytic] 
				/. (mtvalues[[psp]]/.Rule[a_ex,b_]:>Rule[a,Conjugate[b]]);
		    phase0cpf = GetMomentumTwistorExpression[
		        phase[ch]/.spA->spB/.spAA->spBB,PSanalytic] 
                /. (mtvalues[[psp]] /. Rule[a_ex,b_]:>Rule[a,Conjugate[b]]);
            cevl = EvalAmpData[
				Sequence@@camp,
				pfvalues[[psp]],
				mtvalues[[psp]]/.Rule[a_ex,b_]:>Rule[a,Conjugate[b]]
				];
			Apf=((camp[[4]] /. pflip->-1) . #)&/@cevl;
			Acpf = phase0cpf*Conjugate[Apf/phase0pf];
			Return[{Apf,Acpf}];
			,
			ConstantArray[ConstantArray[0,6],2]
		]
	]
];


lo=-Nc^4*Total@Table[#[[1]] . #[[2]]&@partVec["qbqggg",h,1,0,0,0]//Chop,{h,allhelicities}]/.Nc->3


each[channel_,h_]:=Module[{pv0c,pv1nf0,pv1nf1,pv1},
pv0c=partVec[channel,h,1,0,0,0][[2]];
pv1nf0=partVec[channel,h,1,1,0,1][[1]];
pv1nf1=partVec[channel,h,1,0,1,1][[1]];
pv1=pv1nf0+5/3*pv1nf1;
2*Re[pv0c . pv1]
];


nlo=-Nc^4*Total@(each["qbqggg",#]&/@allhelicities)/.Nc->3


nlo/5.1055565970648458*10^03
%//Rationalize[#,10^-3]&


(*EOF*)
