(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[2], f[3], f[4], f[1], f[5]}, {f[7], f[6], f[8]}, {f[10], f[11], f[9]}, 
  {f[13], f[12], f[8]}, {f[2], f[3], f[4], f[14], f[5]}, 
  {f[7], f[10], f[13], f[11], f[15]}}, 
 {{1, 2, 3, 4, 6, 7, 11, 16, 17, 18, 21, 24, 27, 28, 31}, 
  {1, 4, 33, 36, 40, 16, 27}, {1, 2, 55, 4, 59, 33, 27}, 
  {1, 55, 4, 33, 11, 16, 27}, {1, 2, 3, 4, 6, 59, 36, 16, 17, 18, 21, 24, 27, 
   28, 31}, {1, 2, 4, 33, 40, 7, 27}}, 
 {{MySparseMatrix[{5, 15}, {m[1, 1] -> 2/3, m[2, 2] -> -2/3, m[2, 4] -> 2/3, 
     m[3, 4] -> 1/3, m[3, 8] -> -1/3, m[3, 13] -> -1/3, m[4, 2] -> -1/3, 
     m[4, 6] -> -1/6, m[4, 7] -> -1/6, m[4, 8] -> -1/3, m[4, 13] -> 1/2, 
     m[5, 3] -> 1/2, m[5, 5] -> -1/2, m[5, 9] -> -1, m[5, 10] -> 1, 
     m[5, 11] -> 1, m[5, 12] -> -1, m[5, 14] -> -1, m[5, 15] -> 1}], 
   MySparseMatrix[{5, 15}, {}]}, 
  {MySparseMatrix[{3, 7}, {m[1, 1] -> -2/3, m[2, 3] -> -1/3, m[2, 4] -> -1/6, 
     m[2, 5] -> -1/6, m[2, 6] -> -1/3, m[3, 2] -> 2/3, m[3, 6] -> -2/3, 
     m[3, 7] -> -2/3}], MySparseMatrix[{3, 7}, {}]}, 
  {MySparseMatrix[{3, 7}, {m[1, 1] -> 1/3, m[2, 2] -> -2/3, m[2, 4] -> 2/3, 
     m[3, 2] -> 1/3, m[3, 3] -> 1/6, m[3, 5] -> 1/6, m[3, 6] -> 1/3, 
     m[3, 7] -> -1/2}], MySparseMatrix[{3, 7}, {}]}, 
  {MySparseMatrix[{3, 7}, {m[1, 1] -> -2/3, m[2, 2] -> -1/6, m[2, 4] -> -1/3, 
     m[2, 5] -> -1/6, m[2, 6] -> -1/3, m[3, 3] -> 2/3, m[3, 6] -> -2/3, 
     m[3, 7] -> -2/3}], MySparseMatrix[{3, 7}, {}]}, 
  {MySparseMatrix[{5, 15}, {m[1, 1] -> 2/3, m[2, 2] -> -2/3, m[2, 4] -> 2/3, 
     m[3, 4] -> 1/3, m[3, 8] -> -1/3, m[3, 13] -> -1/3, m[4, 2] -> -1/3, 
     m[4, 6] -> -1/6, m[4, 7] -> -1/6, m[4, 8] -> -1/3, m[4, 13] -> 1/2, 
     m[5, 3] -> 1/2, m[5, 5] -> -1/2, m[5, 9] -> -1, m[5, 10] -> 1, 
     m[5, 11] -> 1, m[5, 12] -> -1, m[5, 14] -> -1, m[5, 15] -> 1}], 
   MySparseMatrix[{5, 15}, {}]}, 
  {MySparseMatrix[{5, 7}, {m[1, 1] -> 2/3, m[2, 1] -> 1/3, m[3, 1] -> -2/3, 
     m[4, 2] -> -2/3, m[4, 3] -> 2/3, m[5, 2] -> 1/3, m[5, 4] -> 1/3, 
     m[5, 5] -> 1/6, m[5, 6] -> 1/6, m[5, 7] -> -1/2}], 
   MySparseMatrix[{5, 7}, {}]}}, {1, pflip}, 
 {f[5] -> y[4]/(y[1]*y[2]*y[3]^2), f[4] -> y[5]/(y[1]*y[2]*y[3]^2) - 
    (2*y[6]^2)/(y[1]*y[2]*y[3]^2*y[7]^3) + (3*y[6]*y[8])/
     (y[1]*y[2]*y[3]^2*y[7]^2) - (3*y[9])/(y[1]*y[2]*y[3]^2*y[7]), 
  f[2] -> y[6]/(y[1]*y[3]*y[7]^2*y[10]) - (y[3]^2*y[12]^3*y[13])/
     (2*y[1]*y[10]^2*y[11]) - (y[2]*y[3]*y[6]^3*y[12]^3)/
     (y[1]*y[14]*y[15]^2) - (y[3]^3*y[12]^2*y[17]^3)/
     (2*y[1]*y[10]^2*y[14]*y[16]) + (y[6]^2*y[12]^2*y[18])/
     (2*y[1]*y[14]*y[15]) - y[19]/(2*y[1]*y[3]*y[7]*y[10]^2) - 
    y[20]/(2*y[1]*y[3]*y[14]), 
  f[3] -> (y[2]^2*y[3]*y[6]^3*y[12]^3)/(y[1]*y[14]*y[15]^3) - 
    y[21]/(2*y[1]*y[3]*y[14]) - (3*y[2]*y[6]^2*y[12]^2*y[22])/
     (2*y[1]*y[14]*y[15]^2) + (3*y[6]*y[12]*y[23])/(2*y[1]*y[3]*y[14]*y[15]), 
  f[1] -> (y[2]^2*y[3]^2*y[24])/(y[1]*y[6]*y[14]), 
  f[7] -> y[6]/(y[1]*y[3]*y[7]^2*y[10]) - (y[3]^2*y[12]^3*y[13])/
     (2*y[1]*y[10]^2*y[11]) + y[14]/(y[1]*y[3]) + 
    (y[12]^2*y[14]^2)/(2*y[1]*y[10]^2*y[16]) + (y[12]^2*y[14]^2)/
     (2*y[1]*y[25]) - (y[26]*y[27])/(2*y[1]*y[3]*y[7]*y[10]^2), 
  f[8] -> y[6]^2/(y[1]*y[2]*y[3]^2*y[7]^3) + 
    (3*y[4])/(2*y[1]*y[2]*y[3]^2*y[7]) - (3*y[6]*y[26])/
     (2*y[1]*y[2]*y[3]^2*y[7]^2) - y[28]/(2*y[1]*y[2]*y[3]^2), 
  f[6] -> (y[2]^2*y[3])/(y[1]*y[6]), 
  f[10] -> (y[2]^2*y[3]^2*y[12])/(y[1]*y[14]) + (2*y[2]*y[3]*y[6]^3*y[12]^3)/
     (y[1]*y[14]*y[15]^2) + (y[6]^3*y[10]*y[12]^2)/(y[1]*y[14]*y[16]) - 
    (y[12]*y[30])/(y[1]*y[29]) - (y[3]*y[6]^2*y[12]^2*y[31])/
     (y[1]*y[14]*y[15]), f[11] -> (y[2]^2*y[3])/(y[1]*y[14]) - 
    (y[2]^2*y[3]*y[6]^3*y[12]^3)/(y[1]*y[14]*y[15]^3) + 
    (3*y[2]^2*y[3]*y[6]^2*y[12]^2)/(2*y[1]*y[14]*y[15]^2) - 
    (3*y[2]^2*y[3]*y[6]*y[12])/(2*y[1]*y[14]*y[15]), 
  f[9] -> (y[2]*y[3]*y[24])/y[1], f[13] -> y[6]/(y[1]*y[3]*y[7]^2*y[10]) - 
    (y[3]^2*y[12]^3*y[13])/(2*y[1]*y[10]^2*y[11]) + y[14]/(y[1]*y[3]) + 
    (y[12]^2*y[14]^2)/(2*y[1]*y[10]^2*y[16]) - 
    (y[26]*y[27])/(2*y[1]*y[3]*y[7]*y[10]^2) - (y[12]*y[30])/(2*y[1]*y[29]), 
  f[12] -> (y[2]*y[3]*y[24])/(y[1]*y[6]), 
  f[14] -> (y[2]^2*y[3]^2*y[24])/(y[1]*y[6]), 
  f[15] -> (y[2]^2*y[3])/(y[1]*y[14])}, {y[1] -> ex[1], y[2] -> ex[2], 
  y[3] -> ex[3], y[4] -> 2 + 4*ex[3] + 2*ex[2]*ex[3] + 2*ex[3]^2 + 
    2*ex[2]*ex[3]^2 + ex[2]^2*ex[3]^2, 
  y[5] -> 11 + 22*ex[3] + 13*ex[2]*ex[3] + 11*ex[3]^2 + 13*ex[2]*ex[3]^2 + 
    5*ex[2]^2*ex[3]^2, y[6] -> 1 + ex[3] + ex[2]*ex[3], 
  y[7] -> 1 + ex[2]*ex[3] - ex[3]*ex[4] + ex[3]*ex[5], 
  y[8] -> 3 + 3*ex[3] + 2*ex[2]*ex[3], 
  y[9] -> 6 + 12*ex[3] + 8*ex[2]*ex[3] + 6*ex[3]^2 + 8*ex[2]*ex[3]^2 + 
    3*ex[2]^2*ex[3]^2, y[10] -> 1 + ex[3]*ex[5], y[11] -> ex[2] - ex[4], 
  y[12] -> ex[5], y[13] -> -ex[2] + ex[5] + ex[3]*ex[5], y[14] -> 1 + ex[3], 
  y[15] -> -ex[2] - ex[2]*ex[3] + ex[4] + ex[3]*ex[4] + ex[2]*ex[3]*ex[5], 
  y[16] -> -(ex[2]*ex[3]) + ex[4] + ex[3]*ex[4] + ex[2]*ex[3]*ex[5], 
  y[17] -> ex[2] + ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[18] -> -3 - 3*ex[3] + 3*ex[2]*ex[3] + ex[3]*ex[5] + ex[3]^2*ex[5] + 
    ex[2]*ex[3]^2*ex[5], y[19] -> 7 + 7*ex[3] + 5*ex[2]*ex[3] + 
    9*ex[3]*ex[5] + 9*ex[3]^2*ex[5] + 6*ex[2]*ex[3]^2*ex[5], 
  y[20] -> -5 - 10*ex[3] - 3*ex[2]*ex[3] - 5*ex[3]^2 - 3*ex[2]*ex[3]^2 + 
    ex[2]^2*ex[3]^3*ex[5], y[21] -> 6 + 12*ex[3] + 3*ex[2]*ex[3] + 
    6*ex[3]^2 + 3*ex[2]*ex[3]^2 + 2*ex[2]^2*ex[3]^2, 
  y[22] -> -1 - ex[3] + ex[2]*ex[3], 
  y[23] -> 2 + 4*ex[3] + 2*ex[3]^2 + ex[2]^2*ex[3]^2, y[24] -> 1 + ex[2], 
  y[25] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4], 
  y[26] -> 2 + 2*ex[3] + ex[2]*ex[3], y[27] -> 2 + 3*ex[3]*ex[5], 
  y[28] -> 2 + 4*ex[3] + ex[2]*ex[3] + 2*ex[3]^2 + ex[2]*ex[3]^2 + 
    2*ex[2]^2*ex[3]^2, y[29] -> ex[4], 
  y[30] -> ex[2]*ex[3] + ex[5] + ex[3]*ex[5], 
  y[31] -> 3*ex[2] + ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5]}}
