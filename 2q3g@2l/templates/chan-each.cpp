{%- from 'macros.jinja2' import comma -%}
/*
 * finrem/{{ amp }}/{{ channel }}/{{ amp }}-{{ channel }}.cpp
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#include "{{ amp }}-{{ channel }}.h"

template <typename T, typename P>
Amp{{ amp }}_{{ channel }}_a2l<T, P>::Amp{{ amp }}_{{ channel }}_a2l(const T scalefactor, const int mFC,
                               const NJetAmpTables &tables)
    : Base({{ '{{' }}
        {%- for ord in ords %}
        { {%- for i in ord %}{{ i }}{{ comma(loop) }}{% endfor -%} },
        {%- endfor %}
        {{ '}}' }},
        scalefactor, mFC, tables),
      {%- for hel in hels %}
      hamp{{ '%02d' % hel.dec }}(sf){{ comma(loop) }}
      {%- endfor %}
      {
  sf.resize({{ sfml }});

  // tree
  // primary w/o caching
  {%- for hel in hels if not hel.uhv %}
  hA00r[{{ hel.dec }}] = [this]() { return A00_vec({{ hel.dec }}); };
  {%- endfor %}
  // primary w/ caching
  {%- for hel in hels if not hel.uhv %}
  hA00p[{{ hel.dec }}] = [this]() { return A00_mem({{ hel.dec }}); };
  {%- endfor %}
  // conjugate
  {%- for hel in hels if not hel.uhv %}
  {%- set conj = 31 - hel.dec %}
  hA00p[{{ conj }}] = [this]() { return conjugate({{ hel.dec }}, hA00p); };
  {%- endfor %}
  {% for lo in loops if lo.order %}

  // loop: {{ lo.id }}
  // scaleless w/o caching
  {%- for hel in hels if not (hel.uhv and lo.order == 2) %}
  hA{{ lo.id }}r[{{ hel.dec }}] = [this]() { return A{{ lo.id }}_scaleless(hamp{{ '%02d' % hel.dec }}, phase{{ '%02d' % hel.dec }}()); };
  {%- endfor %}
  // primary w/ caching
  {%- for hel in hels if not (hel.uhv and lo.order == 2) %}
  hA{{ lo.id }}p[{{ hel.dec }}] = [this]() { return A{{ lo.id }}{% if hel.uhv %}_uhv{% endif %}_mem({{ hel.dec }}); };
  {%- endfor %}
  // conjugate
  {%- for hel in hels if not (hel.uhv and lo.order == 2) %}
  {%- set conj = 31 - hel.dec %}
  hA{{ lo.id }}p[{{ conj }}] = [this]() { return conjugate({{ hel.dec }}, hA{{ lo.id }}p); };
  {%- endfor %}
  {%- endfor %}
}

#ifdef USE_DDDD
template <typename T, typename P>
void Amp{{ amp }}_{{ channel }}_a2l<T, P>::copyCoeffs(const NJetAmp<dd_real> *const other) {
  copyCoeffsT(static_cast<const Amp{{ amp }}_{{ channel }}_a2l<dd_real, double> *const>(other));
};
#endif

template <typename T, typename P>
template <typename TT, typename PP>
void Amp{{ amp }}_{{ channel }}_a2l<T, P>::copyCoeffsT(const Amp{{ amp }}_{{ channel }}_a2l<TT, PP> *const other) {
{%- for hel in hels %}
  hamp{{ '%02d' % hel.dec }}.copyCoeffs(other->hamp{{ '%02d' % hel.dec }});
{%- endfor %}

  coeffs_cached = true;
};

// }{ kinematics: special functions (pentagon functions)

template <typename T, typename P> void Amp{{ amp }}_{{ channel }}_a2l<T, P>::setSpecFuncVals1L() {
  const std::array<int, 5> o{0, 1, 2, 3, 4};
  setsij(o.data());
  p.setKin(s12, s23, s34, s45, s15);
{% for indices in spec_funcs_1l %}
  p.eval_F_{% for i in indices %}{{ i }}{{ comma(loop, "_") }}{% endfor %}();
{%- endfor %}
}

template <typename T, typename P> void Amp{{ amp }}_{{ channel }}_a2l<T, P>::setSpecFuncVals() {
  const std::array<int, 5> o{0, 1, 2, 3, 4};
  setsij(o.data());
  p.setKin(s12, s23, s34, s45, s15);
{% for indices in spec_funcs %}
  p.eval_F_{% for i in indices %}{{ i }}{{ comma(loop, "_") }}{% endfor %}();
{%- endfor %}
}


#ifdef USE_SD
template class Amp{{ amp }}_{{ channel }}_a2l<double, double>;
#endif
#ifdef USE_DDSD
template class Amp{{ amp }}_{{ channel }}_a2l<dd_real, double>;
#endif
#ifdef USE_DDDD
template class Amp{{ amp }}_{{ channel }}_a2l<dd_real, dd_real>;
#endif
#ifdef USE_QD
template class Amp{{ amp }}_{{ channel }}_a2l<qd_real, qd_real>;
#endif
#ifdef USE_VC
template class Amp{{ amp }}_{{ channel }}_a2l<Vc::double_v, Vc::double_v>;
#endif

