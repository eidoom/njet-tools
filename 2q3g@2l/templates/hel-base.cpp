{%- from 'macros.jinja2' import comma -%}
/*
 * finrem/{{ amp }}/common/hel.cpp
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#include "hel.h"

// =============================================================================
// even parity contributions
// {{ '{{' }}===========================================================================

// constructor
template <typename T>
HelAmp{{ amp }}<T>::HelAmp{{ amp }}(const std::vector<std::complex<T>> &sf_,
    {%- for loop_obj in loops if loop_obj.order %}
    const int f{{ loop_obj.id }}l, 
    {% for p in range(partials) -%}const int m{{ loop_obj.id }}{{ p }}i, const int m{{ loop_obj.id }}{{ p }}j, {% endfor %}
    {% for p in range(partials) -%}const std::vector<int> &sfi{{ loop_obj.id }}{{ p }}_, {%- endfor %}
    {% for p in range(partials) -%}const std::vector<int> &fi{{ loop_obj.id }}{{ p }}_{{- comma(loop) -}}{%- endfor %}
    {{- comma(loop) -}}
    {% endfor %})
  : Base(sf_),
    {%- for loop_obj in loops if loop_obj.order %}
    f{{ loop_obj.id }}(f{{ loop_obj.id }}l), 
    {% for p in range(partials) -%}m{{ loop_obj.id }}{{ p }}(m{{ loop_obj.id }}{{ p }}i, m{{ loop_obj.id }}{{ p }}j), {% endfor %}
    {% for p in range(partials) -%}sfi{{ loop_obj.id }}{{ p }}(sfi{{ loop_obj.id }}{{ p }}_), {% endfor %}
    {% for p in range(partials) -%}fi{{ loop_obj.id }}{{ p }}(fi{{ loop_obj.id }}{{ p }}_), {% endfor %}
    c{{ loop_obj.id }}e({
    {%- for p in range(partials) %}std::vector<std::complex<T>>(m{{ loop_obj.id }}{{ p }}i){{- comma(loop) -}}{% endfor -%}
    }){{- comma(loop) -}}
    {% endfor %}
  {}

template <typename T>
template <typename U>
void HelAmp{{ amp }}<T>::copyCoeffs(const HelAmp{{ amp }}<U> &other) {
  {%- for loop_obj in loops if loop_obj.order %}
  c{{ loop_obj.id }}e = static_cast<std::vector<std::vector<std::complex<T>>>>(other.c{{ loop_obj.id }}e);
  conjVecs(c{{ loop_obj.id }}e);
  {%- endfor %}
}

{% for loop_obj in loops if loop_obj.order %}
template <typename T> void HelAmp{{ amp }}<T>::cacheCoeffs_{{ loop_obj.id }}() {
{%- for p in range(partials) %}
  coeffVec(c{{ loop_obj.id }}e[{{ p }}], f{{ loop_obj.id }}, fi{{ loop_obj.id }}{{ p }}, m{{ loop_obj.id }}{{ p }});
{%- endfor %}
}
{% endfor %}

{% for loop_obj in loops if loop_obj.order -%}
{%- for p in range(partials) %}
template <typename T> std::complex<T> HelAmp{{ amp }}<T>::pA{{ loop_obj.id }}{{ p }}e() {
  return part(c{{ loop_obj.id }}e[{{ p }}], f{{ loop_obj.id }}, fi{{ loop_obj.id }}{{ p }}, sfi{{ loop_obj.id }}{{ p }}, m{{ loop_obj.id }}{{ p }});
}
{% endfor %}
{% endfor %}
// {{ '}}' }}===========================================================================
// even and odd parity contributions
// {{ '{{' }}===========================================================================

// constructor
template <typename T>
HelAmp{{ amp }}Odd<T>::HelAmp{{ amp }}Odd(
    const std::vector<std::complex<T>> &sf_,
    {%- for loop_obj in loops if loop_obj.order %}
    const int f{{ loop_obj.id }}l, 
    {% for p in range(partials) -%}const int m{{ loop_obj.id }}{{ p }}i, const int m{{ loop_obj.id }}{{ p }}j, {% endfor %}
    {% for p in range(partials) -%}const std::vector<int> &sfi{{ loop_obj.id }}{{ p }}_, {%- endfor %}
    {% for p in range(partials) -%}
    const std::vector<int> &fi{{ loop_obj.id }}{{ p }}_
    {{- comma(loop) -}}
    {%- endfor %}
    {{- comma(loop) -}}
    {% endfor %})
  : Base(
    sf_,
    {%- for loop_obj in loops if loop_obj.order %}
    f{{ loop_obj.id }}l, 
    {% for p in range(partials) -%}m{{ loop_obj.id }}{{ p }}i, m{{ loop_obj.id }}{{ p }}j, {% endfor %}
    {% for p in range(partials) -%}sfi{{ loop_obj.id }}{{ p }}_, {%- endfor %}
    {% for p in range(partials) -%}fi{{ loop_obj.id }}{{ p }}_{{- comma(loop) -}}{%- endfor %}
    {{- comma(loop) -}}
    {% endfor %}),
    {%- for loop_obj in loops if loop_obj.parity_odd %}
    {% for p in range(partials) -%}m{{ loop_obj.id }}{{ p }}o(m{{ loop_obj.id }}{{ p }}i, m{{ loop_obj.id }}{{ p }}j), {% endfor %}
    c{{ loop_obj.id }}o({
    {%- for p in range(partials) %}std::vector<std::complex<T>>(m{{ loop_obj.id }}{{ p }}i){{- comma(loop) -}}{% endfor -%}
    }){{- comma(loop) -}}
    {% endfor %}
  {}

template <typename T>
template <typename U>
void HelAmp{{ amp }}Odd<T>::copyCoeffs(const HelAmp{{ amp }}Odd<U> &other) {
  Base::copyCoeffs(other);
  {% for loop_obj in loops if loop_obj.parity_odd %}
  c{{ loop_obj.id }}o = static_cast<std::vector<std::vector<std::complex<T>>>>(other.c{{ loop_obj.id }}o);
  conjVecs(c{{ loop_obj.id }}o);
  {%- endfor %}
};

{% for loop_obj in loops if loop_obj.parity_odd %}
template <typename T> void HelAmp{{ amp }}Odd<T>::cacheCoeffs_{{ loop_obj.id }}() {
  {% for p in range(partials) -%}
    coeffVec(c{{ loop_obj.id }}o[{{ p }}], f{{ loop_obj.id }}, fi{{ loop_obj.id }}{{ p }}, m{{ loop_obj.id }}{{ p }}o);
  {% endfor %}
}
{% endfor -%}

{% for loop_obj in loops if loop_obj.parity_odd -%}
{%- for p in range(partials) %}
template <typename T> std::complex<T> HelAmp{{ amp }}Odd<T>::pA{{ loop_obj.id }}{{ p }}o() {
  return part(c{{ loop_obj.id }}o[{{ p }}], f{{ loop_obj.id }}, fi{{ loop_obj.id }}{{ p }}, sfi{{ loop_obj.id }}{{ p }}, m{{ loop_obj.id }}{{ p }}o);
}
{% endfor %}
{% endfor -%}
// {{ '}}' }}===========================================================================

#ifdef USE_SD
template class HelAmp{{ amp }}<double>;
template class HelAmp{{ amp }}Odd<double>;
#endif
#ifdef USE_DD
template class HelAmp{{ amp }}<dd_real>;
template class HelAmp{{ amp }}Odd<dd_real>;
template void
HelAmp{{ amp }}<dd_real>::copyCoeffs<dd_real>(const HelAmp{{ amp }}<dd_real> &other);
template void HelAmp{{ amp }}Odd<dd_real>::copyCoeffs<dd_real>(
    const HelAmp{{ amp }}Odd<dd_real> &other);
#endif
#ifdef USE_QD
template class HelAmp{{ amp }}<qd_real>;
template class HelAmp{{ amp }}Odd<qd_real>;
#endif
#ifdef USE_VC
template class HelAmp{{ amp }}<Vc::double_v>;
template class HelAmp{{ amp }}Odd<Vc::double_v>;
#endif
