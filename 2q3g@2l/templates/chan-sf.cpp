/*
 * finrem/{{ amp }}/{{ channel }}/sf.cpp
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#include "{{ amp }}-{{ channel }}.h"

template <typename T, typename P>
void Amp{{ amp }}_{{ channel }}_a2l<T, P>::setSpecFuncMons1L() {
  std::array<std::complex<T>, {{ sfz1 }}> Z;

  {{ sfm1 }}
}

template <typename T, typename P> 
void Amp{{ amp }}_{{ channel }}_a2l<T, P>::setSpecFuncMons() {
  std::vector<std::complex<T>> Z({{ sfz }});

  {{ sfm }}
}

#ifdef USE_SD
template class Amp{{ amp }}_{{ channel }}_a2l<double, double>;
#endif
#ifdef USE_DDSD
template class Amp{{ amp }}_{{ channel }}_a2l<dd_real, double>;
#endif
#ifdef USE_DDDD
template class Amp{{ amp }}_{{ channel }}_a2l<dd_real, dd_real>;
#endif
#ifdef USE_QD
template class Amp{{ amp }}_{{ channel }}_a2l<qd_real, qd_real>;
#endif
#ifdef USE_VC
template class Amp{{ amp }}_{{ channel }}_a2l<Vc::double_v, Vc::double_v>;
#endif
