{%- from 'macros.jinja2' import comma -%}
/*
 * finrem/{{ amp }}/common/chan.h
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#pragma once

#include <array>
#include <complex>

#include "analytic/{{ amp }}-analytic.h"
#include "chsums/NJetAmpTables.h"
#include "finrem/common/fin-rem-amp.h"
#include "ngluon2/EpsQuintuplet.h"

#include "hel.h"

template <typename T, typename P>
class Amp{{ amp }}_a2l : public Amp{{ amp }}_a<T>, public FinRemAmp{{ legs }}<T, P, {{ partials }}> {
  // we need the analytic {{ amp }} base for the tree expressions
  using Base = Amp{{ amp }}_a<T>;
  using FinRem = FinRemAmp{{ legs }}<T, P, {{ partials }}>;

public:
  using HelPairVecFn = typename FinRem::HelPairVecFn;
  using AllHelPairVecFns = typename FinRem::AllHelPairVecFns;
  using HelAmp = typename Base::HelAmp;

  Amp{{ amp }}_a2l(const std::array<std::array<int, {{ legs }}>, {{ partials }}> &ords_, T scalefactor = 1., int mFC = 1,
              const NJetAmpTables &tables = Base::amptables());

  // all this SpecFunc stuff can be called by setSpecFuncs() or copySpecFuncs()
  // from chsums/NJetAmp.h

  virtual void printSpecFuncs() const override;

  virtual void initFinRem() override;

  using Base::copySpecFuncs;
  using Base::getSpecFuncs;
  {% for loop_obj in loops %}
  virtual std::array<LoopResult<std::complex<T>>, {{ partials }}> A{{ loop_obj.id }}p() override;
  {%- endfor %}

protected:
  enum Loops { {% for l in loops %}L{{ l.id }}{{ comma(loop) }}{% endfor %}, Number };

  bool coeffs_cached;

  std::complex<T> A00_single(int binhel, int part);

  std::array<LoopResult<std::complex<T>>, {{ partials }}> A00_vec(int binhel);

  {% for lo in loops %}
  AllHelPairVecFns hA{{ lo.id }}r;
  AllHelPairVecFns hA{{ lo.id }}p;
  {%- endfor %}

  std::array<std::array<int, {{ legs }}>, {{ partials }}> ords;

{% for lo in loops if lo.order %}
  std::array<LoopResult<std::complex<T>>, {{ partials }}>
  A{{ lo.id }}_scaleless(HelAmp{{ amp }}{% if lo.parity_odd %}Odd{% endif %}<T> &hamp, std::complex<T> phase);
{% endfor %}

  {% for lo in loops %}
  std::array<LoopResult<std::complex<T>>, {{ partials }}> A{{ lo.id }}_mem(int i);
  {% if lo.order == 1 %}
  std::array<LoopResult<std::complex<T>>, {{ partials }}> A{{ lo.id }}_uhv_mem(int i);
  {% endif %}
  {%- endfor %}

  using Base::sf;
  using Base::callLoopVec;
  using Base::callTree;
  using Base::hA0;
  using Base::HelicityOrder;
  using Base::mhelint;
  using Base::MuR2;
  using FinRem::call_cached;
  using FinRem::conjugate;
  using FinRem::gbl_sgn;
  using FinRem::p;
  using FinRem::pi;
  using FinRem::zeta3;
  using FinRem::x1;
  using FinRem::x2;
  using FinRem::x3;
  using FinRem::x4;
  using FinRem::x5;
};
