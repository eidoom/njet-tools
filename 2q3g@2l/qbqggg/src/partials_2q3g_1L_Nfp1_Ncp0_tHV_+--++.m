(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[2], f[3], f[1]}, {f[5], f[6], f[4]}, {f[8], f[9], f[10], f[11], f[7]}, 
  {f[13], f[6], f[12]}, {f[2], f[5], f[13], f[3], f[14]}, 
  {f[8], f[9], f[10], f[11], f[15]}}, {{1, 2, 4, 7, 11, 15, 24}, 
  {1, 2, 11, 29, 33, 38, 24}, {1, 2, 3, 7, 8, 38, 39, 49, 51, 55, 20, 44, 23, 
   24}, {1, 2, 4, 11, 38, 51, 24}, {1, 2, 7, 11, 29, 55, 24}, 
  {1, 2, 3, 7, 8, 15, 33, 38, 39, 49, 20, 44, 23, 24}}, 
 {{MySparseMatrix[{3, 7}, {m[1, 1] -> -1/3, m[2, 2] -> -2/3, m[2, 4] -> 2/3, 
     m[3, 3] -> -1/6, m[3, 4] -> -1/3, m[3, 5] -> -1/3, m[3, 6] -> -1/6, 
     m[3, 7] -> 2/3}], MySparseMatrix[{3, 7}, {}]}, 
  {MySparseMatrix[{3, 7}, {m[1, 1] -> 1/3, m[2, 2] -> 2/3, m[2, 6] -> -2/3, 
     m[3, 3] -> 1/3, m[3, 4] -> 1/6, m[3, 5] -> 1/6, m[3, 6] -> 1/3, 
     m[3, 7] -> -2/3}], MySparseMatrix[{3, 7}, {}]}, 
  {MySparseMatrix[{5, 14}, {m[1, 1] -> 2/3, m[2, 2] -> 2/3, m[2, 4] -> -2/3, 
     m[3, 2] -> -2/3, m[3, 6] -> 2/3, m[4, 3] -> 1, m[4, 5] -> -1, 
     m[4, 7] -> -1, m[4, 8] -> 1, m[4, 11] -> 1, m[4, 12] -> 1, 
     m[4, 13] -> 1/6, m[5, 4] -> 1/3, m[5, 6] -> 1/3, m[5, 9] -> 1/6, 
     m[5, 10] -> 1/6, m[5, 14] -> -2/3}], MySparseMatrix[{5, 14}, {}]}, 
  {MySparseMatrix[{3, 7}, {m[1, 1] -> 1/3, m[2, 2] -> 2/3, m[2, 5] -> -2/3, 
     m[3, 3] -> 1/6, m[3, 4] -> 1/3, m[3, 5] -> 1/3, m[3, 6] -> 1/6, 
     m[3, 7] -> -2/3}], MySparseMatrix[{3, 7}, {}]}, 
  {MySparseMatrix[{5, 7}, {m[1, 1] -> -1/3, m[2, 1] -> -1/3, m[3, 1] -> 1/3, 
     m[4, 2] -> -2/3, m[4, 3] -> 2/3, m[5, 3] -> -1/3, m[5, 4] -> -1/3, 
     m[5, 5] -> -1/6, m[5, 6] -> -1/6, m[5, 7] -> 2/3}], 
   MySparseMatrix[{5, 7}, {}]}, 
  {MySparseMatrix[{5, 14}, {m[1, 1] -> 2/3, m[2, 2] -> 2/3, m[2, 4] -> -2/3, 
     m[3, 2] -> -2/3, m[3, 8] -> 2/3, m[4, 3] -> 1, m[4, 5] -> -1, 
     m[4, 9] -> -1, m[4, 10] -> 1, m[4, 11] -> 1, m[4, 12] -> 1, 
     m[4, 13] -> 1/6, m[5, 4] -> 1/3, m[5, 6] -> 1/6, m[5, 7] -> 1/6, 
     m[5, 8] -> 1/3, m[5, 14] -> -2/3}], MySparseMatrix[{5, 14}, {}]}}, 
 {1, pflip}, {f[2] -> y[1]*y[2]^2*y[3]*y[4] - (y[1]*y[2]^2*y[6]*y[7])/
     (y[4]*y[5]) + (2*y[1]^2*y[2]^2*y[8]^2*y[9]^3)/y[10]^2 + 
    (y[1]*y[2]^2*y[8]*y[9]^3*y[12])/(y[5]*y[11]) - 
    (y[1]*y[2]^2*y[8]*y[9]^2*y[13])/y[10] - y[1]*y[2]^2*y[14], 
  f[3] -> (y[1]^3*y[2]^2*y[8]^2*y[9]^3)/y[10]^3 - 
    (3*y[1]^3*y[2]^2*y[8]*y[9]^2)/(2*y[10]^2) + (3*y[1]^3*y[2]^2*y[9])/
     (2*y[10]), f[1] -> y[1]^2*y[2], 
  f[5] -> y[1]*y[2]^2*y[3]*y[4] - (y[1]*y[2]^2*y[8]*y[15])/(y[5]*y[16]) - 
    (2*y[1]^2*y[2]^2*y[3]^2*y[9]^3*y[15]^2)/y[17]^2 - 
    (y[1]*y[2]^2*y[3]*y[9]^2*y[15]*y[18])/y[17] + 
    (y[1]*y[2]^2*y[9]^3*y[15]*y[20])/(y[5]*y[19]) - y[1]*y[2]^2*y[21], 
  f[6] -> (y[1]^3*y[2]^2*y[3]^2*y[9]^3*y[15]^2)/y[17]^3 - 
    (3*y[1]^3*y[2]^2*y[3]*y[9]^2*y[15])/(2*y[17]^2) + 
    (3*y[1]^3*y[2]^2*y[9])/(2*y[17]), f[4] -> (y[1]^3*y[2]^2)/(y[3]*y[15]), 
  f[11] -> y[2]^2*y[22], f[8] -> (y[1]^2*y[2]^2*y[8]^2*y[9]^3)/y[10]^2 + 
    (y[1]*y[2]^2*y[8]*y[9]^3*y[12])/(2*y[5]*y[11]) + 
    (y[1]^2*y[2]^2*y[3]^2*y[9]^3*y[15]^2)/y[17]^2 - 
    (y[1]*y[2]^2*y[9]^3*y[15]*y[20])/(2*y[5]*y[19]) - 
    (y[1]*y[2]^2*y[8]*y[9]^2*y[23])/(2*y[10]) + 
    (y[1]*y[2]^2*y[3]*y[9]^2*y[15]*y[24])/(2*y[17]), 
  f[9] -> (y[1]^3*y[2]^2*y[8]^2*y[9]^3)/y[10]^3 - 
    (3*y[1]^2*y[2]^2*y[3]*y[8]*y[9]^2*y[15])/(2*y[10]^2) + 
    (3*y[1]*y[2]^2*y[9]*y[22])/(2*y[10]), 
  f[10] -> (y[1]^3*y[2]^2*y[3]^2*y[9]^3*y[15]^2)/y[17]^3 + 
    (3*y[1]^2*y[2]^2*y[3]*y[8]*y[9]^2*y[15])/(2*y[17]^2) + 
    (3*y[1]*y[2]^2*y[9]*y[22])/(2*y[17]), 
  f[7] -> (y[1]^3*y[2]^2)/(y[3]*y[8]), 
  f[13] -> y[1]*y[2]^2*y[3]*y[4] - (y[1]*y[2]^2*y[6]*y[7])/(y[4]*y[5]) - 
    (2*y[1]^2*y[2]^2*y[3]^2*y[9]^3*y[15]^2)/y[17]^2 - 
    (y[1]*y[2]^2*y[3]*y[9]^2*y[15]*y[18])/y[17] + 
    (y[1]*y[2]^2*y[9]^3*y[15]*y[20])/(y[5]*y[19]) - y[1]*y[2]^2*y[25], 
  f[12] -> (y[1]^2*y[2]^2)/y[3], f[14] -> (y[1]^3*y[2]^2)/y[8], 
  f[15] -> (y[1]^3*y[2])/(y[3]*y[15])}, {y[1] -> ex[2], y[2] -> ex[3], 
  y[3] -> 1 + ex[3], y[4] -> ex[4], y[5] -> -1 + ex[5], 
  y[6] -> ex[2] + ex[5], y[7] -> ex[2]*ex[3] + ex[5] + ex[3]*ex[5], 
  y[8] -> 1 + ex[3] + ex[2]*ex[3], y[9] -> ex[5], 
  y[10] -> -ex[2] - ex[2]*ex[3] + ex[4] + ex[3]*ex[4] + ex[2]*ex[3]*ex[5], 
  y[11] -> -(ex[2]*ex[3]) + ex[4] + ex[3]*ex[4] + ex[2]*ex[3]*ex[5], 
  y[12] -> ex[2] + ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[13] -> 3*ex[2] + ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[14] -> ex[2] + 2*ex[2]*ex[3] + 2*ex[5] - ex[2]*ex[5] + 2*ex[3]*ex[5] + 
    ex[2]*ex[3]*ex[5], y[15] -> 1 + ex[2], y[16] -> 1 + ex[4] - ex[5], 
  y[17] -> -(ex[2]*ex[3]) + ex[4] + ex[3]*ex[4] + ex[2]*ex[5] + 
    ex[2]*ex[3]*ex[5], y[18] -> -3*ex[2] + ex[5] + ex[2]*ex[5] + 
    ex[3]*ex[5] + ex[2]*ex[3]*ex[5], y[19] -> -ex[2] + ex[4] + ex[2]*ex[5], 
  y[20] -> -ex[2] + ex[5] + ex[2]*ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[21] -> 1 + ex[2] + ex[3] + 2*ex[2]*ex[3] + 2*ex[5] + 2*ex[2]*ex[5] + 
    2*ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[22] -> 2 + 2*ex[2] + ex[2]^2 + 4*ex[3] + 6*ex[2]*ex[3] + 
    2*ex[2]^2*ex[3] + 2*ex[3]^2 + 4*ex[2]*ex[3]^2 + 2*ex[2]^2*ex[3]^2, 
  y[23] -> 3 + 3*ex[2] + 3*ex[3] + 3*ex[2]*ex[3] + ex[5] + ex[3]*ex[5] + 
    ex[2]*ex[3]*ex[5], y[24] -> 3 + 3*ex[3] + 3*ex[2]*ex[3] + ex[5] + 
    ex[2]*ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[25] -> ex[2] + 2*ex[2]*ex[3] + 2*ex[5] + 2*ex[2]*ex[5] + 2*ex[3]*ex[5] + 
    ex[2]*ex[3]*ex[5]}}
