(* ::Package:: *)

VSpoint = {1.322500000, \[Minus]0.994109498, 0.264471591, 0.267126049, \[Minus]0.883795230};


<< "InitTwoLoopToolsFF.m"
<< "setupfiles/Setup_5g.m"


Rx[a_] := {{1,0,0},{0,Cos[a],Sin[a]},{0,-Sin[a],Cos[a]}};
Ry[a_] := {{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_] := {{Cos[a],Sin[a],0},{-Sin[a],Cos[a],0},{0,0,1}};


GetPSpointX[s_,x1_,x2_,t1_,t2_,a1_,a2_] := Module[{p1,p2,p3,p4,p5},
p1 = -Sqrt[s]/2*{1,0,0,1};
p2 = -Sqrt[s]/2*{1,0,0,-1};
p3 = Sqrt[s]*x1/2*Join[{1},Rz[a1] . Ry[t1] . {0,0,1}];
p4 = Sqrt[s]*x2/2*Join[{1},Rz[a1] . Ry[t1] . Rz[a2] . Ry[t2] . {0,0,1}];
p5 = -p1-p2-p3-p4;
{p1,p2,p3,p4,p5} = MOM4@@@{p1,p2,p3,p4,p5};
Return[{p1,p2,p3,p4,p5}];
]


PStest = GetPSpointX[s,x1,x2,t1,t2,0,a2];
EvalMTanalyticRules[PStest] = {};

GetMomentumTwistorExpression[MP[p[5],p[5]],PStest] // Simplify;
Solve[%==0,Cos[t2]]
PStest = PStest /. Sin[t2]->Sqrt[1-Cos[t2]^2] //. %[[1]];
EvalMTanalyticRules[PStest] = {};

GetMomentumTwistorExpression[#,PStest]&/@
	{s[1,2],s[2,3],s[3,4],s[4,5],s[1,5]} // Simplify;
{%,VSpoint};
pssol = Solve[Equal@@@Transpose[%]]


PScheck = PStest /. pssol[[2]];
EvalMTanalyticRules[PScheck] = {};
GetMomentumTwistorExpression[#,PScheck]&/@
	{s[1,2],s[2,3],s[3,4],s[4,5],s[1,5],tr5[1,2,3,4]};
VSpoint-%[[1;;5]];
sola2 = Solve[%[[-1]]==0,a2]


PSmoms = PStest /. pssol[[2]] /. sola2[[2]];
EvalMTanalyticRules[PSmoms] = {};
GetMomentumTwistorExpression[#,PSmoms]&/@
	{s[1,2],s[2,3],s[3,4],s[4,5],s[1,5],tr5[1,2,3,4]};
VSpoint-%[[1;;5]]


DecimalForm[#,16]&/@PSmoms
