#! /usr/bin/env bash

cores=$(nproc)
channels=3
each=$((cores / channels))

source ~/env-form

for f in *; do
	if [[ $f =~ ^[qbg]{6}$ ]]; then
		echo -e "\n$f\n"
		cd $f
		../proc/loop.sh
		cd build
		make -j$each &
		cd ../..
	fi
done
