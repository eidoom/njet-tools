#!/usr/bin/env python3

from argparse import ArgumentParser
from pathlib import Path
from string import Template
import json
import re


if __name__ == "__main__":
    parser = ArgumentParser("Generate class function definitions for phases")
    args = parser.parse_args()

    p = Path.cwd()

    ps = re.compile(r"(s[AB])\[(\d, \d)\]")
    pp = re.compile(r"pow\[(.*?)\]")
    pu = re.compile(r"1 \+")

    with open("template.cpp", "r") as f:
        template = Template(f.read())

    template_header = Template("  virtual std::complex<T> phase${hdec}() override;\n")

    for channel in p.glob("phases_*.json"):
        # hels = json.loads(channel.read_text())
        with open(channel) as f:
            hels = json.load(f)

        output = "// } phases\n\n"

        output_header = ""

        for hel in hels:
            hel["amp"] = p.parent.name[:-3]
            hel["chan"] = channel.stem[7:]
            hel["hdec"] = sum([2 ** i for i, s in enumerate(hel["hchars"]) if s == "+"])
            if hel["hdec"] < 10:
                hel["hdec"] = "0" + str(hel["hdec"])

            # hel["expr"] = hel["expr"].translate(str.maketrans("[]","()"))
            hel["expr"] = re.sub(ps, r"a.\1(\2)", hel["expr"])
            hel["expr"] = re.sub(pp, r"njet_pow(\1)", hel["expr"])
            hel["expr"] = re.sub(pu, r"T(1.) +", hel["expr"])

            output_header += template_header.substitute(hel)

            output += template.substitute(hel) + "\n"

        with open(channel.stem + ".cpp", "w") as f:
            f.write(output_header + "\n")
            f.write(output)
