
u[1]=x2 + T(1.);
u[2]=x2 + T(3.);
u[3]=T(2.)*x3;
u[4]=u[3] + u[2];
u[5]=x5 - T(1.);
u[6]=x4 - u[5];
u[7]=x2*x5;
u[8]=u[7] + x5;
u[9]=u[8]*u[3];
u[10]=u[7] - x5;
u[11]=u[9] + T(4.) + u[10];
u[11]=x2*u[11];
u[12]=T(2.)*u[5];
u[11]= - u[12] + u[11];
u[13]=x3*u[1];
u[13]=T(1.) + u[13];
u[14]=T(3.)*x2;
u[15]=u[1]*u[14];
u[16]= - u[3] - T(4.) + u[15];
u[16]=x3*u[16];
u[16]=u[16] - T(2.) + u[14];
u[17]=njet_pow(x2,2)*u[9];
u[8]=u[8]*u[14];
u[18]=T(4.)*u[5];
u[8]= - u[18] + u[8];
u[8]=x2*u[8];
u[8]=u[17] - u[12] + u[8];
u[8]=x3*u[8];
u[19]=T(3.)*x5;
u[20]= - T(2.) + u[19];
u[20]=x2*u[20];
u[20]= - u[18] + u[20];
u[20]=x2*u[20];
u[8]=u[8] - u[18] + u[20];
u[8]=x3*u[8];
u[8]= - u[12] + u[8];
u[20]=T(1.) + x3;
u[21]=T(2.)*x2;
u[2]=u[2]*u[21];
u[2]=T(7.) + u[2];
u[2]=x2*u[2];
u[22]=x2 + T(2.);
u[22]=u[22]*x2;
u[22]=u[22] + T(1.);
u[23]=u[22]*x3;
u[2]=u[23] + T(3.) + u[2];
u[2]=u[2]*u[3];
u[24]=T(8.) + x2;
u[24]=x2*u[24];
u[24]=T(15.) + u[24];
u[24]=x2*u[24];
u[2]=u[2] + T(6.) + u[24];
u[2]=x3*u[2];
u[24]=T(5.) + x2;
u[24]=x2*u[24];
u[2]=u[2] + T(2.) + u[24];
u[24]=u[21] + T(5.);
u[24]=u[24]*u[5]*x2;
u[24]=u[24] + u[18];
u[24]=u[24]*x2;
u[24]=u[24] + u[5];
u[24]=u[24]*u[3];
u[19]=u[19] - T(4.);
u[19]=u[19]*x2;
u[25]= - T(7.) + T(6.)*x5;
u[25]=T(2.)*u[25] + u[19];
u[25]=x2*u[25];
u[25]=u[25] - T(24.) + T(23.)*x5;
u[25]=x2*u[25];
u[25]=T(20.)*u[5] + u[25];
u[25]=x2*u[25];
u[26]=T(6.)*u[5];
u[25]=u[24] + u[26] + u[25];
u[25]=x3*u[25];
u[27]= - T(4.) + T(5.)*x5;
u[28]=x2*u[27];
u[28]=u[28] - T(16.) + T(15.)*x5;
u[28]=x2*u[28];
u[28]=T(16.)*u[5] + u[28];
u[28]=x2*u[28];
u[25]=u[25] + u[26] + u[28];
u[25]=x3*u[25];
u[22]=u[5]*u[22];
u[22]=T(2.)*u[22];
u[25]=u[22] + u[25];
u[26]= - u[3] - T(1.) + x2;
u[26]=x3*u[26];
u[26]=T(1.) + u[26];
u[7]=u[9] + u[7] + u[27];
u[7]=x2*u[7];
u[7]=u[12] + u[7];
u[7]=x3*u[7];
u[7]=u[7] + u[18] + u[19];
u[7]=x3*u[7];
u[7]=u[12] + u[7];
u[9]=u[3] + T(2.) + u[15];
u[10]= - x2*u[10];
u[10]=u[18] + u[10];
u[10]=x2*u[10];
u[10]=u[17] + u[12] + u[10];
u[10]=x3*u[10];
u[10]=u[22] + u[10];
u[15]=T(3.) + u[21];
u[15]=x2*u[15];
u[15]= - T(1.) + u[15];
u[15]=x2*u[15];
u[15]= - u[23] - T(2.) + u[15];
u[3]=u[15]*u[3];
u[15]=T(10.) + u[14];
u[15]=x2*u[15];
u[15]=T(3.) + u[15];
u[15]=x2*u[15];
u[3]=u[3] - T(2.) + u[15];
u[3]=x3*u[3];
u[14]=u[14] + T(1.);
u[14]=u[14]*x2;
u[3]=u[14] + u[3];
u[15]= - T(3.) + T(2.)*x5;
u[15]=T(2.)*u[15] + u[19];
u[15]=x2*u[15];
u[15]=u[15] + T(6.) - T(7.)*x5;
u[15]=x2*u[15];
u[15]= - T(12.)*u[5] + u[15];
u[15]=x2*u[15];
u[15]= - u[24] - u[18] + u[15];
u[15]=x3*u[15];
u[17]=x5 - T(2.);
u[14]=u[17]*u[14];
u[14]= - u[18] + u[14];
u[14]=x2*u[14];
u[12]=u[15] - u[12] + u[14];

y[0] = x1;
y[1] = x2;
y[2] = u[1];
y[3] = x3;
y[4] = u[4];
y[5] = x4;
y[6] = u[6];
y[7] = u[5];
y[8] = u[11];
y[9] = u[13];
y[10] = u[16];
y[11] = u[8];
y[12] = u[20];
y[13] = u[2];
y[14] = u[25];
y[15] = u[26];
y[16] = u[7];
y[17] = u[9];
y[18] = u[10];
y[19] = u[3];
y[20] = u[12];
