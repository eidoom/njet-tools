(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[2], f[1], f[3]}, {f[5], f[6], f[7], f[4], f[8]}, 
  {f[2], f[13], f[15], f[10], f[9]}, {f[5], f[6], f[11], f[7], f[8]}, 
  {f[13], f[3], f[12]}, {f[15], f[10], f[14]}}, 
 {{1, 2, 4, 7, 11, 16, 27}, {1, 34, 36, 4, 6, 11, 15, 43, 46, 47, 23, 51, 26, 
   27, 53, 32}, {1, 11, 16, 56, 60, 43, 27}, {1, 2, 4, 6, 11, 15, 60, 43, 46, 
   47, 23, 51, 26, 27, 53, 32}, {1, 34, 4, 11, 16, 56, 27}, 
  {1, 36, 7, 11, 16, 43, 27}}, 
 {{MySparseMatrix[{3, 7}, {m[1, 1] -> 1/3, m[2, 2] -> 1/6, m[2, 3] -> 1/3, 
     m[2, 4] -> 1/6, m[2, 6] -> 1/3, m[2, 7] -> -1/3, m[3, 3] -> -2/3, 
     m[3, 5] -> 2/3, m[3, 7] -> 2/3}], MySparseMatrix[{3, 7}, {}]}, 
  {MySparseMatrix[{5, 16}, {m[1, 1] -> 2/3, m[2, 6] -> 2/3, m[2, 8] -> -2/3, 
     m[3, 4] -> -2/3, m[3, 6] -> 2/3, m[3, 14] -> 2/3, m[4, 2] -> 1/6, 
     m[4, 3] -> 1/6, m[4, 4] -> 1/3, m[4, 8] -> 1/3, m[4, 14] -> -2/3, 
     m[5, 5] -> -1/2, m[5, 7] -> 1/2, m[5, 9] -> 1, m[5, 10] -> -1, 
     m[5, 11] -> -1, m[5, 12] -> 1, m[5, 13] -> -1/6, m[5, 15] -> -1, 
     m[5, 16] -> 1}], MySparseMatrix[{5, 16}, {}]}, 
  {MySparseMatrix[{5, 7}, {m[1, 1] -> 1/3, m[2, 1] -> -1/3, m[3, 1] -> -1/3, 
     m[4, 2] -> -2/3, m[4, 6] -> 2/3, m[5, 3] -> 1/3, m[5, 4] -> 1/6, 
     m[5, 5] -> 1/6, m[5, 6] -> 1/3, m[5, 7] -> -1/6}], 
   MySparseMatrix[{5, 7}, {}]}, 
  {MySparseMatrix[{5, 16}, {m[1, 1] -> 2/3, m[2, 5] -> 2/3, m[2, 8] -> -2/3, 
     m[3, 2] -> -1/6, m[3, 3] -> -1/3, m[3, 7] -> -1/6, m[3, 8] -> -1/3, 
     m[3, 14] -> 1/3, m[4, 3] -> -2/3, m[4, 5] -> 2/3, m[4, 14] -> 2/3, 
     m[5, 4] -> -1/2, m[5, 6] -> 1/2, m[5, 9] -> 1, m[5, 10] -> -1, 
     m[5, 11] -> -1, m[5, 12] -> 1, m[5, 13] -> -1/6, m[5, 15] -> -1, 
     m[5, 16] -> 1}], MySparseMatrix[{5, 16}, {}]}, 
  {MySparseMatrix[{3, 7}, {m[1, 1] -> 1/3, m[2, 3] -> -2/3, m[2, 4] -> 2/3, 
     m[2, 7] -> 2/3, m[3, 2] -> -1/6, m[3, 3] -> -1/3, m[3, 5] -> -1/3, 
     m[3, 6] -> -1/6, m[3, 7] -> 2/3}], MySparseMatrix[{3, 7}, {}]}, 
  {MySparseMatrix[{3, 7}, {m[1, 1] -> -1/3, m[2, 4] -> -2/3, m[2, 6] -> 2/3, 
     m[3, 2] -> 1/6, m[3, 3] -> 1/6, m[3, 5] -> 1/3, m[3, 6] -> 1/3, 
     m[3, 7] -> -1/6}], MySparseMatrix[{3, 7}, {}]}}, {1, pflip}, 
 {f[2] -> (y[4]^3*y[5])/(y[1]*y[2]^2*y[3]^2*y[6]*y[7]^2) - 
    y[8]^3/(y[1]*y[3]^2*y[7]) - (2*y[4]^3)/(y[1]*y[2]*y[3]^2*y[7]*y[9]^2) + 
    y[1]^2/(y[2]^2*y[3]^2*y[7]*y[10]) - (y[4]^2*y[11])/
     (y[1]*y[2]*y[3]^2*y[7]^2*y[9]), 
  f[3] -> y[8]^3/(y[1]*y[2]*y[3]^2) - y[4]^3/(y[1]*y[2]*y[3]^2*y[9]^3) + 
    (3*y[4]^2*y[8])/(2*y[1]*y[2]*y[3]^2*y[9]^2) - 
    (3*y[4]*y[8]^2)/(2*y[1]*y[2]*y[3]^2*y[9]), 
  f[1] -> y[8]^3/(y[1]*y[2]*y[3]^2), f[8] -> y[12]/(y[2]^4*y[3]^2), 
  f[7] -> y[4]^3/(y[1]*y[2]*y[3]^2*y[9]^3) - 
    (y[8]*y[13])/(2*y[1]*y[2]^3*y[3]^2) - (3*y[4]^2*y[14])/
     (2*y[1]*y[2]^2*y[3]^2*y[9]^2) + (3*y[4]*y[15])/
     (2*y[1]*y[2]^3*y[3]^2*y[9]), 
  f[5] -> y[4]^3/(y[1]*y[2]*y[3]^2*y[7]*y[9]^2) + (y[1]^2*y[4]*y[7]^3*y[8])/
     (y[2]^3*y[3]^2*y[5]*y[16]^2) - (y[1]*y[18])/(2*y[3]^2*y[5]^2*y[7]*
      y[17]) - (y[2]*y[19]^3)/(2*y[1]*y[3]^2*y[5]^2*y[6]*y[7]^2) - 
    (y[1]*y[7]^2*y[8]*y[20])/(2*y[2]^3*y[3]^2*y[5]^2*y[16]) + 
    (y[4]^2*y[21])/(2*y[1]*y[2]^2*y[3]^2*y[7]^2*y[9]) + 
    (y[8]*y[22])/(2*y[1]*y[2]^3*y[3]^2*y[7]), 
  f[6] -> (y[1]^3*y[4]^2*y[7]^3)/(y[2]^4*y[3]^2*y[16]^3) - 
    y[23]/(2*y[2]^4*y[3]^2) - (3*y[1]^2*y[4]*y[7]^2*y[24])/
     (2*y[2]^4*y[3]^2*y[16]^2) + (3*y[1]*y[7]*y[25])/(2*y[2]^4*y[3]^2*y[16]), 
  f[4] -> y[8]^3/(y[3]*y[4]), 
  f[10] -> (y[1]^3*y[4]^2*y[7]^3)/(y[2]^4*y[3]^2*y[16]^3) + 
    (3*y[1]*y[7]*y[12])/(2*y[2]^4*y[3]^2*y[16]) - y[26]/(2*y[2]^4*y[3]^2) - 
    (3*y[1]^2*y[4]*y[7]^2*y[27])/(2*y[2]^4*y[3]^2*y[16]^2), 
  f[9] -> y[8]^2/(y[2]*y[3]*y[4]), f[11] -> y[8]^3/(y[1]*y[3]*y[4]), 
  f[13] -> (y[4]^3*y[5])/(y[1]*y[2]^2*y[3]^2*y[6]*y[7]^2) - 
    y[8]^3/(y[1]*y[3]^2*y[7]) - (2*y[4]^3)/(y[1]*y[2]*y[3]^2*y[7]*y[9]^2) - 
    (y[4]^2*y[11])/(y[1]*y[2]*y[3]^2*y[7]^2*y[9]) + 
    (y[1]*y[29])/(y[2]^2*y[3]^2*y[7]*y[28]), f[12] -> y[8]^2/(y[2]*y[3]), 
  f[15] -> (y[1]^2*y[7])/(y[2]^2*y[3]^2*y[5]^2*y[6]) + 
    (2*y[1]*y[8])/(y[2]^3*y[3]^2) + y[1]^2/(y[2]^2*y[3]^2*y[7]*y[10]) + 
    (2*y[1]^2*y[4]*y[7]^3*y[8])/(y[2]^3*y[3]^2*y[5]*y[16]^2) - 
    (y[1]*y[18])/(y[3]^2*y[5]^2*y[7]*y[17]) - (y[1]*y[7]^2*y[8]*y[27]*y[30])/
     (y[2]^3*y[3]^2*y[5]^2*y[16]), f[14] -> y[8]^3/(y[2]*y[3]^2*y[4])}, 
 {y[1] -> ex[2], y[2] -> 1 + ex[2], y[3] -> ex[3], 
  y[4] -> 1 + ex[3] + ex[2]*ex[3], y[5] -> 1 + ex[2] - ex[5], 
  y[6] -> 1 + ex[4] - ex[5], y[7] -> ex[5], y[8] -> 1 + ex[3], 
  y[9] -> 1 + ex[4], y[10] -> ex[2] - ex[4] + ex[5], 
  y[11] -> 1 + ex[3] + ex[2]*ex[3] - 3*ex[5] - 3*ex[3]*ex[5], 
  y[12] -> 1 + ex[2]^2 + 2*ex[3] + 2*ex[2]*ex[3] + ex[3]^2 + 
    2*ex[2]*ex[3]^2 + ex[2]^2*ex[3]^2, 
  y[13] -> 2 + ex[2] + 5*ex[2]^2 + 4*ex[3] + 5*ex[2]*ex[3] + ex[2]^2*ex[3] + 
    2*ex[3]^2 + 4*ex[2]*ex[3]^2 + 2*ex[2]^2*ex[3]^2, 
  y[14] -> 1 + 2*ex[2] + ex[3] + ex[2]*ex[3], 
  y[15] -> 1 + 2*ex[2] + 3*ex[2]^2 + 2*ex[3] + 4*ex[2]*ex[3] + 
    2*ex[2]^2*ex[3] + ex[3]^2 + 2*ex[2]*ex[3]^2 + ex[2]^2*ex[3]^2, 
  y[16] -> ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[2]*ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4] + ex[2]*ex[5], y[17] -> ex[2] - ex[4], 
  y[18] -> ex[2] - ex[5] - ex[3]*ex[5], 
  y[19] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - ex[3]*ex[5], 
  y[20] -> 6 + 3*ex[2] - 3*ex[2]^2 + 6*ex[3] + 12*ex[2]*ex[3] + 
    6*ex[2]^2*ex[3] - 5*ex[5] + 2*ex[2]*ex[5] - 5*ex[3]*ex[5] - 
    5*ex[2]*ex[3]*ex[5], y[21] -> 1 + ex[2] + ex[3] + 2*ex[2]*ex[3] + 
    ex[2]^2*ex[3] - 3*ex[5] - 6*ex[2]*ex[5] - 3*ex[3]*ex[5] - 
    3*ex[2]*ex[3]*ex[5], y[22] -> 1 + 3*ex[2] + 3*ex[2]^2 + ex[2]^3 + 
    2*ex[3] + 6*ex[2]*ex[3] + 6*ex[2]^2*ex[3] + 2*ex[2]^3*ex[3] + ex[3]^2 + 
    3*ex[2]*ex[3]^2 + 3*ex[2]^2*ex[3]^2 + ex[2]^3*ex[3]^2 - 3*ex[2]*ex[5] + 
    2*ex[2]^2*ex[5] - 3*ex[2]*ex[3]*ex[5] - 3*ex[2]^2*ex[3]*ex[5], 
  y[23] -> 5 - 3*ex[2] + 3*ex[2]^2 + 10*ex[3] + 7*ex[2]*ex[3] - 
    3*ex[2]^2*ex[3] + 5*ex[3]^2 + 10*ex[2]*ex[3]^2 + 5*ex[2]^2*ex[3]^2, 
  y[24] -> 2 - ex[2] + 2*ex[3] + 2*ex[2]*ex[3], 
  y[25] -> 3 - 2*ex[2] + ex[2]^2 + 6*ex[3] + 4*ex[2]*ex[3] - 
    2*ex[2]^2*ex[3] + 3*ex[3]^2 + 6*ex[2]*ex[3]^2 + 3*ex[2]^2*ex[3]^2, 
  y[26] -> 2 + 3*ex[2] + 3*ex[2]^2 + 4*ex[3] + 7*ex[2]*ex[3] + 
    3*ex[2]^2*ex[3] + 2*ex[3]^2 + 4*ex[2]*ex[3]^2 + 2*ex[2]^2*ex[3]^2, 
  y[27] -> 1 - ex[2] + ex[3] + ex[2]*ex[3], 
  y[28] -> -ex[2] + ex[4] + ex[2]*ex[5], 
  y[29] -> -ex[2] + ex[5] + ex[2]*ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[30] -> 3 + 3*ex[2] - 2*ex[5]}}
