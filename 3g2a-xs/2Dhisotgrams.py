#!/usr/bin/env python3

import argparse
import pathlib
import pickle

import matplotlib.cm
import matplotlib.pyplot
import numpy


def read_in(filename, args):
    file = "data" / pathlib.Path(filename)

    picklefile = file.parent / f".{file.name}.pickle"
    if not args.refresh and picklefile.is_file():
        print(f"  Using data cache {picklefile}")
        with open(picklefile, "rb") as f:
            return pickle.load(f)

    print(f"  Reading file {file}")
    with open(file, "r") as f:
        raw = f.readlines()

    data = numpy.genfromtxt(raw[3:-1], delimiter=" ")
    bins = numpy.append(data[:, 0], data[:, 2][-1])
    x = data[:, 1]

    valerrs = data[:, 3:] / 1e3  # pb

    if "yg1g2" in filename:
        factor = float(filename[-5])
        valerrs = valerrs * 100 ** (4 - factor)
    elif "cosCS" in filename:
        factor = float(filename[-5])
        valerrs = valerrs * 10 ** (5 - factor)

    vals = valerrs[:, ::2]
    errs = valerrs[:, 1::2]
    central_value = vals[:, 0]
    central_value_error = errs[:, 0]
    scale_variation_max = vals.max(axis=1)
    scale_variation_min = vals.min(axis=1)

    data = {
        "bins": bins,
        "x": x,
        "central_value": central_value,
        "central_value_error": central_value_error,
        "scale_variation_max": scale_variation_max,
        "scale_variation_min": scale_variation_min,
        "central_value_error": central_value_error,
    }

    print(f"    Saving data cache {picklefile}")
    with open(picklefile, "wb") as f:
        pickle.dump(data, f)

    return data


def safe_div(a, b):
    return numpy.divide(
        a,
        b,
        out=numpy.zeros_like(a),
        where=b != 0,
    )


def fix_edge(a):
    return numpy.append(a, 0.0)


def plot_out(values, args, obs_name, obs_fname, nn, ranges, log=True, ll="best"):
    print("  Rendering")

    missing_insert = True

    axis_label_font_size = 17
    axis_tick_label_size = 15

    fig, axa = matplotlib.pyplot.subplots(
        sharex=True,
        figsize=(6.4, 4.8),
        tight_layout=True,
    )

    axa.tick_params(axis="y", labelsize=axis_tick_label_size, which="both")

    if log:
        axa.set_yscale("log")
    else:
        axa.set_yscale("linear")

    colours = matplotlib.cm.tab10(range(2))

    for i in range(0, nn):
        LO = values["LO"][i]
        LOcv = LO["central_value"]

        if i == 0:
            if "cosCS" in obs_fname:
                axa.set_xlim((0, 0.8))
                axa.set_ylim((0.006, 10**5))
            else:
                axa.set_xlim(LO["bins"][[0, -1]])

        NLO = values["NLO"][i]

        mid_bin = LO["bins"][len(LO["bins"]) // 2 - 1]
        mid_val = LO["scale_variation_min"][len(LO["scale_variation_min"]) // 2 - 1]
        text = ranges[i]

        for (name, order), colour in zip(([("LO", LO), ("NLO", NLO)]), colours):
            axa.fill_between(
                order["bins"],
                fix_edge(order["scale_variation_max"]),
                fix_edge(order["scale_variation_min"]),
                step="post",
                facecolor=colour,
                alpha=0.2,
            )

            axa.stairs(
                order["central_value"],
                edges=order["bins"],
                baseline=None,
                label=name if not i else None,
                color=colour,
            )

            if "ptgg_ygg" in obs_fname:
                if i == 0:
                    xxyytext = (90, -8)
                else:
                    xxyytext = (90, -10)
            elif "mgg_ptgg_bins" in obs_fname:
                if i == 0:
                    xxyytext = (-10, -25)
                elif i == 1:
                    xxyytext = (-15, -17)
                elif i == 3:
                    xxyytext = (-15, -10)
                else:
                    xxyytext = (-15, -12)
            elif "mgg_ptgg_cuts" in obs_fname:
                if i == 0:
                    xxyytext = (60, 20)
                elif i == 1:
                    xxyytext = (-130, 16)
                elif i == 2:
                    xxyytext = (-30, -20)
                elif i == 3:
                    xxyytext = (-10, -15)
            else:
                xxyytext = (0, -10)

            axa.annotate(
                text,
                xy=(mid_bin, mid_val),
                xycoords="data",
                xytext=xxyytext,
                textcoords="offset points",
                fontsize=11,
            )

            if "ptgg_ygg" in obs_fname:
                xxyytext_insert = (-40, 0)
            elif "mgg_ptgg_bins" in obs_fname:
                xxyytext_insert = (10, 7)
            elif "mgg_ptgg_cuts" in obs_fname:
                xxyytext_insert = (-25, 13)
            elif "cosCS" in obs_fname:
                xxyytext_insert = (-100, 20)

            if missing_insert:
                if "cosCS" in obs_fname:
                    axa.annotate(
                        r"\noindent LHC 13\,TeV \texttt{NNPDF31}\quad {\normalsize $gg\to\gamma\gamma g\quad\mu=m_T/2$ \\ \texttt{NNLOjet}+\texttt{OpenLoops2}+\texttt{NJet}}",
                        xy=(mid_bin, max(order["scale_variation_max"])),
                        xycoords="data",
                        xytext=xxyytext_insert,
                        textcoords="offset points",
                        fontsize=12,
                    )
                else:
                    axa.annotate(
                        r"\noindent LHC 13\,TeV \texttt{NNPDF31} \\ {\normalsize $gg\to\gamma\gamma g\quad\mu=m_T/2$ \\ \texttt{NNLOjet}+\texttt{OpenLoops2}+\texttt{NJet}}",
                        xy=(mid_bin, max(order["scale_variation_max"])),
                        xycoords="data",
                        xytext=xxyytext_insert,
                        textcoords="offset points",
                        fontsize=12,
                    )
                missing_insert = False

            if args.mc_errors:
                axa.errorbar(
                    order["x"],
                    order["central_value"],
                    yerr=order["central_value_error"],
                    fmt="none",
                    ecolor=colour,
                )

    if "cosCS" in obs_fname:
        ylabel = f"d$\sigma$/d{obs_name} [pb]"
        xlabel = f"{obs_name} GeV"
        frameonbool = True

    else:
        ylabel = f"d$\sigma$/d{obs_name} [pb/GeV]"
        xlabel = obs_name
        frameonbool = False

    if "ptgg_ygg" in obs_fname:
        ll = "upper right"
        sizefac = 1
    elif "mgg_ptgg_bins" in obs_fname:
        ll = "upper right"
        sizefac = 0.7
    elif "mgg_ptgg_cuts" in obs_fname:
        ll = "upper right"
        sizefac = 1
    elif "cosCS" in obs_fname:
        ll = "upper left"
        sizefac = 0.7

    axa.set_ylabel(ylabel, fontsize=axis_label_font_size)
    axa.set_xlabel(xlabel, fontsize=axis_label_font_size)

    axa.legend(
        # labels=["LO", "NLO"],
        loc=ll,
        prop={"size": sizefac * axis_label_font_size},
        frameon=frameonbool,
    )

    filename = f"thesis/{obs_fname}.pdf"
    print(f"  Writing {filename}")
    fig.savefig(filename, bbox_inches="tight", pad_inches=0.025)

    matplotlib.pyplot.close(fig=fig)


def do_observable(obs, plotname, tex):

    dLO = []
    dNLO = []
    ranges_list = []

    for fname in obs.keys():
        dLO.append(read_in(f"LO.{fname}.dat", args))
        dNLO.append(read_in(f"NLO.{fname}.dat", args))

    ll = "best"

    for key in obs.keys():
        ranges_list.append(obs[key])

    plot_out(
        {"LO": dLO, "NLO": dNLO},
        args,
        obs_name=tex,
        obs_fname=plotname,
        ll=ll,
        log=True,
        nn=len(obs.keys()),
        ranges=ranges_list,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser("generate histograms")
    parser.add_argument(
        "-e", "--mc-errors", action="store_true", help="Show central value MC errors"
    )
    parser.add_argument(
        "-l", "--latex", action="store_true", help="Use LaTeX font and symbols"
    )
    parser.add_argument(
        "-r", "--refresh", action="store_true", help="Refresh cached data files"
    )
    # parser.add_argument(
    #     "-o",
    #     "--observable",
    #     type=str,
    #     help="Plot OBSERVABLE if included, otherwise plot all",
    # )
    args = parser.parse_args()

    pd = pathlib.Path("plots/")
    pd.mkdir(exist_ok=True)

    if args.latex:
        matplotlib.pyplot.rc("text", usetex=True)
        matplotlib.pyplot.rc("font", family="serif")

    mgg_ptgg_bins = {
        "Mg1g2_ptg1g2_1": r" 20GeV $< p_T(\gamma\gamma)< $ 160GeV",
        "Mg1g2_ptg1g2_2": r"160GeV $< p_T(\gamma\gamma)< $ 320GeV",
        "Mg1g2_ptg1g2_3": r"320GeV $< p_T(\gamma\gamma)< $ 480GeV",
        "Mg1g2_ptg1g2_4": r"480GeV $< p_T(\gamma\gamma)< $ 640GeV",
        "Mg1g2_ptg1g2_5": r"640GeV $< p_T(\gamma\gamma)< $ 800GeV",
    }

    mgg_ptgg_cuts = {
        "Mg1g2_3": r"$p_T(\gamma\gamma) >$  20GeV",
        "Mg1g2_ptg1g2_6": r"$p_T(\gamma\gamma) >$  50GeV",
        "Mg1g2_ptg1g2_7": r"$p_T(\gamma\gamma) >$ 100GeV",
        "Mg1g2_ptg1g2_8": r"$p_T(\gamma\gamma) >$ 200GeV",
    }

    cosCS_mgg = {
        "cosCS_Mg1g2_1": r"100GeV $< m(\gamma\gamma) <$ 140GeV,  $\times10^{4}$",
        "cosCS_Mg1g2_2": r"140GeV $< m(\gamma\gamma) <$ 180GeV,  $\times10^{3}$",
        "cosCS_Mg1g2_3": r"180GeV $< m(\gamma\gamma) <$ 220GeV,  $\times10^{2}$",
        "cosCS_Mg1g2_4": r"220GeV $< m(\gamma\gamma) <$ 260GeV,  $\times10$",
        "cosCS_Mg1g2_5": r"260GeV $< m(\gamma\gamma) <$ 300GeV",
    }

    ptgg_ygg = {
        "ptg1g2_yg1g2_1": r"0 $< |y(\gamma\gamma)| <$ 0.6,  $\times10^{6}$",
        "ptg1g2_yg1g2_2": r"0.6 $< |y(\gamma\gamma)| <$ 1.2,  $\times10^{4}$",
        "ptg1g2_yg1g2_3": r"1.2 $< |y(\gamma\gamma)| <$ 1.8,  $\times10^{2}$",
        "ptg1g2_yg1g2_4": r"1.8 $< |y(\gamma\gamma)| <$ 2.4 ",
    }

    do_observable(mgg_ptgg_bins, "2D_mgg_ptgg_bins", r"$m(\gamma\gamma)$")
    do_observable(mgg_ptgg_cuts, "2D_mgg_ptgg_cuts", r"$m(\gamma\gamma)$")
    do_observable(cosCS_mgg, "2D_cosCS_mgg", r"$|\cos{\phi_{CS}(\gamma\gamma)}|$")
    do_observable(ptgg_ygg, "2D_ptgg_ygg", r"$p_T(\gamma\gamma)$")
