#!/usr/bin/env python3

import argparse
import pathlib
import pickle

import matplotlib.cm
import matplotlib.pyplot
import numpy


def read_in(filename, args):
    file = "data" / pathlib.Path(filename)

    picklefile = file.parent / f".{file.name}.pickle"
    if not args.refresh and picklefile.is_file():
        print(f"  Using data cache {picklefile}")
        with open(picklefile, "rb") as f:
            return pickle.load(f)

    print(f"  Reading file {file}")
    with open(file, "r") as f:
        raw = f.readlines()

    data = numpy.genfromtxt(raw[3:-1], delimiter=" ")
    bins = numpy.append(data[:, 0], data[:, 2][-1])
    x = data[:, 1]

    valerrs = data[:, 3:] / 1e3  # pb
    vals = valerrs[:, ::2]
    errs = valerrs[:, 1::2]
    central_value = vals[:, 0]
    central_value_error = errs[:, 0]
    scale_variation_max = vals.max(axis=1)
    scale_variation_min = vals.min(axis=1)

    data = {
        "bins": bins,
        "x": x,
        "central_value": central_value,
        "central_value_error": central_value_error,
        "scale_variation_max": scale_variation_max,
        "scale_variation_min": scale_variation_min,
        "central_value_error": central_value_error,
    }

    print(f"    Saving data cache {picklefile}")
    with open(picklefile, "wb") as f:
        pickle.dump(data, f)

    return data


def safe_div(a, b):
    return numpy.divide(
        a,
        b,
        out=numpy.zeros_like(a),
        where=b != 0,
    )


def fix_edge(a):
    return numpy.append(a, 0.0)


def plot_out(values, args, obs_name, obs_fname, log=False, ll="best"):
    print("  Rendering")
    fig, (axa, axr) = matplotlib.pyplot.subplots(
        nrows=2,
        sharex=True,
        figsize=(6.4, 4.8),
        subplot_kw={},
        gridspec_kw={
            "height_ratios": (2, 1),
            "hspace": 0,
        },
    )

    if log:
        axa.set_yscale("log")
    else:
        axa.set_yscale("linear")

    axr.set_yscale("linear")

    colours = matplotlib.cm.tab10(range(2))

    LO = values["LO"]
    LOcv = LO["central_value"]
    LO["ratio_min"] = safe_div(LO["scale_variation_min"], LOcv)
    LO["ratio_max"] = safe_div(LO["scale_variation_max"], LOcv)

    axa.set_xlim(LO["bins"][[0, -1]])

    NLO = values["NLO"]
    NLO["ratio_min"] = safe_div(NLO["scale_variation_min"], LOcv)
    NLO["ratio_max"] = safe_div(NLO["scale_variation_max"], LOcv)

    # ayl = min(NLO["scale_variation_min"].min(), LO["scale_variation_min"].min())
    # ayu = max(NLO["scale_variation_max"].max(), LO["scale_variation_max"].max())
    # axa.set_ylim(ayl, ayu)

    # ryl = min(NLO["ratio_min"].min(), LO["ratio_min"].min())
    # ryu = max(NLO["ratio_max"].max(), LO["ratio_max"].max())
    # axr.set_ylim(ryl, ryu)

    for (name, order), colour in zip(values.items(), colours):
        axa.fill_between(
            order["bins"],
            fix_edge(order["scale_variation_max"]),
            fix_edge(order["scale_variation_min"]),
            step="post",
            facecolor=colour,
            alpha=0.2,
        )

        axa.stairs(
            order["central_value"],
            edges=order["bins"],
            baseline=None,
            label=name,
            color=colour,
        )

        if args.mc_errors:
            axa.errorbar(
                order["x"],
                order["central_value"],
                yerr=order["central_value_error"],
                fmt="none",
                ecolor=colour,
            )

        axr.fill_between(
            order["bins"],
            fix_edge(order["ratio_max"]),
            fix_edge(order["ratio_min"]),
            step="post",
            facecolor=colour,
            alpha=0.2,
        )

        ratio = safe_div(
            order["central_value"],
            LOcv,
        )

        axr.stairs(
            ratio,
            edges=order["bins"],
            baseline=None,
            label=name,
            color=colour,
        )

        if args.mc_errors:
            ratio_mcerr = safe_div(
                order["central_value_error"],
                LOcv,
            )

            axr.errorbar(
                order["x"],
                ratio,
                yerr=ratio_mcerr,
                fmt="none",
                ecolor=colour,
            )

    # introduce some space so tick labels don't overlap
    if "yg1g2" in obs_fname:
        al, au = axa.get_ylim()
        axa.set_ylim(al - 0.1, au)

        rl, ru = axr.get_ylim()
        axr.set_ylim(rl, ru + 0.1)

    if "pt" in obs_fname or "M" in obs_fname:
        axa.set_ylabel(f"d$\sigma$/d{obs_name} [pb/GeV]")
        axr.set_xlabel(f"{obs_name} GeV")
    else:
        axa.set_ylabel(f"d$\sigma$/d{obs_name} [pb]")
        axr.set_xlabel(obs_name)

    axa.legend(loc=ll)

    axr.set_ylabel("Ratio")

    filename = f"plots/{obs_fname}.pdf"
    print(f"  Writing {filename}")
    fig.savefig(filename)

    matplotlib.pyplot.close(fig=fig)


def do_observable(fname, tex):
    print(f"Plotting {fname}")

    dLO = read_in(f"LO.{fname}.dat", args)
    dNLO = read_in(f"NLO.{fname}.dat", args)

    ll = "best"
    for pos, obss in lls.items():
        if fname in obss:
            ll = pos
            break

    plot_out(
        {"LO": dLO, "NLO": dNLO},
        args,
        obs_name=tex,
        obs_fname=fname,
        ll=ll,
        log=fname in logs,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser("generate histograms")
    parser.add_argument(
        "-e", "--mc-errors", action="store_true", help="Show central value MC errors"
    )
    parser.add_argument(
        "-l", "--latex", action="store_true", help="Use LaTeX font and symbols"
    )
    parser.add_argument(
        "-r", "--refresh", action="store_true", help="Refresh cached data files"
    )
    parser.add_argument(
        "-o", "--observable", type=str, help="Plot OBSERVABLE if included, otherwise plot all"
    )
    args = parser.parse_args()

    pd = pathlib.Path("plots/")
    pd.mkdir(exist_ok=True)

    if args.latex:
        matplotlib.pyplot.rc("text", usetex=True)
        matplotlib.pyplot.rc("font", family="serif")

    obss = {
        "cosCS_1": r"$\cos{\phi_{CS}}$",
        "cosCS_2": r"$\cos{\phi_{CS}}$",
        "dphi_1": r"$\Delta\phi(\gamma\gamma)$",
        "dphi_2": r"$\Delta\phi(\gamma\gamma)$",
        "dy_1": r"$\Delta y(\gamma\gamma)$",
        "dy_2": r"$\Delta y(\gamma\gamma)$",
        "dy_3": r"$\Delta y(\gamma\gamma)$",
        "Mg1g2_1": r"$m(\gamma\gamma)$",
        "Mg1g2_2": r"$m(\gamma\gamma)$",
        "Mg1g2_3": r"$m(\gamma\gamma)$",
        "pt_g1g2_1": r"$p_T(\gamma\gamma)$",
        "pt_g1g2_2": r"$p_T(\gamma\gamma)$",
        "pt_g1g2_3": r"$p_T(\gamma\gamma)$",
        "ptg1_1": r"$p_T(\gamma_1)$",
        "ptg1_2": r"$p_T(\gamma_1)$",
        "ptg1_3": r"$p_T(\gamma_1)$",
        "ptg2_1": r"$p_T(\gamma_2)$",
        "ptg2_2": r"$p_T(\gamma_2)$",
        "ptg2_3": r"$p_T(\gamma_2)$",
        "yg1g2_1": r"$|y(\gamma\gamma)|$",
        "yg1g2_2": r"$|y(\gamma\gamma)|$",
        "yg1g2_3": r"$|y(\gamma\gamma)|$",
    }

    lls = {
        "upper left": (
            "cosCS_1",
            "cosCS_2",
            "dphi_1",
            "dphi_2",
        ),
    }

    logs = (
        "dphi_1",
        "dphi_2",
        "Mg1g2_1",
        "Mg1g2_2",
        "Mg1g2_3",
        "pt_g1g2_1",
        "pt_g1g2_2",
        "pt_g1g2_3",
        "ptg1_1",
        "ptg1_2",
        "ptg1_3",
        "ptg2_1",
        "ptg2_2",
        "ptg2_3",
    )

    if args.observable:
        do_observable(args.observable, obss[args.observable])
    else:
        for item in obss.items():
            do_observable(*item)
