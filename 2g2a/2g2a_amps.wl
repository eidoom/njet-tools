(* ::Package:: *)

<< "InitTwoLoopToolsFF.m"
SetDirectory["/home/ryan/git/njet-tools/2g2a"];
<< "setupfiles/Setup_ggaa.m"


amp2g2a["++++"]=32 ex[1]^4 ex[2]^2;
amp2g2a["-+++"]=(32 ex[1]^5 ex[2]^3)/(1+ex[2]);
amp2g2a["--++"]=-16 ex[1]^2 ex[2]^2 (2+ex[1]^2 ex[2] (1+3 ex[2]+4 ex[2]^2+2 ex[2]^3) FBOX1M[topo[{{1},{3},{2},{4}}]]+(2+4 ex[2]) Log[s[2,3]]-2 (1+2 ex[2]) Log[s[2,4]]);
amp2g2a["+--+"]=-(1/(ex[1]^2 ex[2]^2))16 (ex[1]^2 (2+4 ex[2]+3 ex[2]^2+ex[2]^3) FBOX1M[topo[{{1},{2},{4},{3}}]]+2 ex[2] (ex[2]+(2+ex[2]) Log[s[1,2]]-(2+ex[2]) Log[s[2,4]]));
amp2g2a["+-+-"]=(1/(ex[1]^2 ex[
  2]^2))16 (-2 - 4 ex[2] - 2 ex[2]^2 + 
   ex[1]^2 ex[2] FBOX1M[topo[{{1}, {2}, {3}, {4}}]] + 
   ex[1]^2 ex[2]^3 FBOX1M[topo[{{1}, {2}, {3}, {4}}]] + 
   2 Log[s[1, 2]] - 2 ex[2]^2 Log[s[1, 2]] - 2 Log[s[2, 3]] + 
   2 ex[2]^2 Log[s[2, 3]]);


(*MyLog[x_,y_]:=Log[Abs[x/y]] - I*Pi*(HeavisideTheta[-Re[x]/Re[y]]);
MyDiLog[x_,y_]:=Re[PolyLog[2,1-x/y]] - I*HeavisideTheta[-Re[x/y]]*MyLog[y-x,y]*Im[MyLog[x,y]];*)
MyLog[x_,y_]:=Log[x/y];
MyDiLog[x_,y_]:=PolyLog[2,1-x/y];
I4[s_,t_,mur2_]:=Module[{eps2,eps1,eps0},
eps2 = 2/s/t;
eps1 = 2/s/t*(MyLog[mur2,-s]+MyLog[mur2,-t]);
eps0 = 1/s/t*(MyLog[mur2,-s]^2+MyLog[mur2,-t]^2 - MyLog[-s,-t]^2 -Pi^2/2 +Pi^2/6 );
Return[{eps2,eps1,eps0}];
];
analyticSubs= {L[1,s_/t_]:>MyLog[-s,-t]/(1-s/t), Lhat[2, s_/t_]:>MyLog[-s,-t]/(1-s/t)^2+1/(1-s/t)};
I4hat[s_,t_,mur2_]:=Module[{eps2,eps1,eps0},
eps2 = 2/s/t;
eps1 = 2/s/t*(MyLog[mur2,-s]+MyLog[mur2,-t]);
eps0 = 1/s/t*(- MyLog[-s,-t]^2 -Pi^2/3);
Return[{eps2,eps1,eps0}];
];
fboxId={FBOX1M[topo[{{i_},{j_},{k_},{l_}}]]->I4hat[s[i,j], s[j,k], mU2][[3]]};
(*,Log[s_s]\[RuleDelayed]Log[Abs[s]]-I Pi HeavisideTheta[-s]*)
fboxId2=FBOX1M[topo[{{i_},{j_},{k_},{l_}}]]->I4[s[i,j], s[j,k], mU2][[3]];


Rationalize[0.0625,10^-3]


NJetForm[expr_]:=expr\
	/.ex[i_]:>x[i]\
	/.Power[a_,b_]->pow[a,b]\
	/.Log[a_]->log[a]\
	/.Abs[a_]->abs[a]\
	/.Pi->pi\
	//InputForm


pppp=amp2g2a["++++"] /.fboxId;


pppp//NJetForm


mppp=amp2g2a["-+++"] /.fboxId;


mppp//NJetForm


mmpp=amp2g2a["--++"] /.fboxId


mmpp=mmpp//Simplify


mmpp//NJetForm


pmmp=amp2g2a["+--+"] /.fboxId;


pmpm=amp2g2a["+-+-"]/.fboxId;


pmpm=pmpm//Simplify


pmpm//NJetForm


GetMomentumTwistorExpression[expr_, momenta_]:=SimplifyKinematics[expr//.(f:spA|spB|spAB|spAA|spBB)[i___,x_Integer,j___]:>f[i,p[x+1],j],momenta]


phase["++++"]=(spB[0,1]spB[2,3])^2;
phase["-+++"]=(spA[0,1]spB[1,2]spB[1,3])^2;
phase["--++"]=(spA[0,1]spB[2,3])^2;
phase["+--+"]=(spA[1,2]spB[0,3])^2;
phase["+-+-"]=(spB[0,2]spA[1,3])^2;
phase["-+-+"]=(spA[0,2]spB[1,3])^2;
phase["+--+"]=(spA[1,2]spB[0,3])^2;


GetMomentumTwistorExpression[phase[#],PSanalytic]&/@{"++++","-+++","--++","-+-+"}


phase["--++"]
GetMomentumTwistorExpression[phase["--++"],PSanalytic]


phase["+-+-"]
GetMomentumTwistorExpression[phase["+-+-"],PSanalytic]


phase["+--+"]
GetMomentumTwistorExpression[phase["+-+-"],PSanalytic]


fromMT/.lpS[i_,j_]:>s[i-1,j-1]


phase2["++++"]=(spB[0,1]spB[2,3])/(spA[0,1]spA[2,3]);
phase2["-+++"]=(spA[0,1]spA[0,3]spB[1,3])/(spA[2,3]spA[1,2]spA[1,3]);
phase2["--++"]=(spA[0,1]spB[2,3])/(spB[0,1]spA[2,3]);
phase2["+--+"]=(spA[1,2]spB[0,3])/(spB[1,2]spA[0,3]);
phase2["+-+-"]=(spA[1,3]spB[0,2])/(spB[1,3]spA[0,2]);


GetMomentumTwistorExpression[phase2[#],PSanalytic]&/@{"++++","-+++","--++","+-+-"}
