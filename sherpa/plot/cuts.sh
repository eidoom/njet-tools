#!/usr/bin/env bash

l=(0 50 100 200)
nn=1M-float64-new-sherpa

for a in ${l[@]}; do
    for b in ${l[@]}; do
        for c in ${l[@]}; do
            dir="myy${a}-ptyy${b}-ptj${c}"

            if [ ! -d "${dir}" ]; then
                cp -r template ${dir}

                cd ${dir} || exit 1

                perl -pi -e "s|^(SUF1=).*$|\1 1tev-cuts/${dir}|g" "Makefile"
                perl -pi -e "s|^(SUF2=).*$|\1 nn-cuts/${dir}|g" "Makefile"
                perl -pi -e "s|^(SUFFIX=).*$|\1 njet-vs-nn-${nn}-${dir}|g" "Makefile"
                perl -pi -e "s|^(LAB1=).*$|\1 NJet|g" "Makefile"
                perl -pi -e "s|^(LAB2=).*$|\1 NN|g" "Makefile"

                export RIVET_ANALYSIS_PATH=../../analysis-cuts-${dir}
                make

                cd .. || exit 1
            fi
        done
    done
done
