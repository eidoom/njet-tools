#!/usr/bin/env bash

dir="njet-vs-nn"
nn="1M-new-sherpa-float64"

if [ ! -d "${dir}" ]; then
    cp -r template ${dir}

    cd ${dir} || exit 1

    perl -pi -e "s|^(SUF1=).*$|\1 1tev/bak-hepmc-1M-new-sherpa-1e-16|g" "Makefile"
    perl -pi -e "s|^(SUF2=).*$|\1 nn/bak-hepmc-${nn}|g" "Makefile"
    perl -pi -e "s|^(SUFFIX=).*$|\1 ${dir}-${nn}-hepmc|g" "Makefile"
    perl -pi -e "s|^(LAB1=).*$|\1 NJet|g" "Makefile"
    perl -pi -e "s|^(LAB2=).*$|\1 NN|g" "Makefile"

    export RIVET_ANALYSIS_PATH=../../analysis-1tev
    make

    cd .. || exit 1
fi
