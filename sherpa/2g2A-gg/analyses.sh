#!/usr/bin/env bash
# To run jobs on the batch, do:
# ./analyses.sh <initial seed> <number of runs> <total number of events> 

init_seed=$1
num_runs=$2
end_seed=$(($init_seed + $num_runs - 1 ))
total_events=$3
run_events=$(($total_events / $num_runs))
output=part.

for rseed in $(seq -w ${init_seed} ${end_seed}); do
    name=${rseed}-2g2A-analysis
    run_filename=${name}
    sed "s/%j/${name}/g" analysis.sbatch >$run_filename
    run_output=$output$rseed
    sbatch $run_filename $rseed $run_events $run_output
    rm -f $run_filename
done
