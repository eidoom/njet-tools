---
title: NJet with Sherpa
author: Ryan Moodie
date: 3 Dec 2019
---

\newpage{}

# Usage

* Make comparison histograms like:
```shell
RIVET_ANALYSIS_PATH=analysis-diphoton rivet-mkhtml 3g2A-ctm/Analysis.yoda 3g2A-nn/Analysis.yoda --errs -t TITLE
```
and view on IPPP with, for example,
```shell
rm -rf ~/www/files/research/3g2A-njet-vs-nn-1tev
cp -r rivet-plots/ ~/www/files/research/3g2A-njet-vs-nn-1tev
```
* Or edit `Makefile` and use
```shell
make -B copy
```

## Flags

* `-DDEBUG` prints phase space point kinematics
* `-DTEST` return uniy for matrix element

# Directory contents

## Current work

### 2g2A-ctm
2g2A with custom analysis. Uses:
* NJET0q2gAA interface
* analysis-diphoton analysis
* integrator from 2g2A-std

### 2g2A-qq
Generates optimised integrator for 2g2A.

### 2g2A-opt
Optimised 2g2A. Uses: 
* analysis-diphoton analysis 
* NJET0q2gAA interface
* 2g2A-qq integrator

### 2g2A-std
2g2A with standard analysis. Uses:
* NJET0q2gAA interface

### 3g2A-ctm
3g2A with custom analysis. Uses
* analysis-diphoton analysis
* NJET0q3gAA interface
* integrator from 3g2A-std

### 3g2A-opt
Optimised 3g2A. Uses:
* analysis-diphoton custom analysis
* NJET0q3gAA interface 
* integrator from 3g2A-qq

### 3g2A-qq
Generates optimised integrator for 3g2A. Used in 3g2A-opt.

### 3g2A-std
3g2A with standard analysis. Uses:
* NJET0q3gAA interface

### 3g2A-nn
3g2A from neural net. Uses:
* analysis-diphoton custom analysis
* NN3g2A interface

### 4g2A-opt
Optimised 4g2A. Uses 
* analysis-diphoton custom analysis
* NJET0q4gAA interface
* integrator from 4g2A-qq

### 4g2A-qq
Generates optimised integrator for 4g2A. Used in 4g2A-opt.

### analysis-diphoton
A custom analysis for diphoton plus jets at hadron colliders.

### NJET0q2gAA
Bespoke NJet/Sherpa interface for 2g2A.

### NJET0q3gAA
Bespoke NJet/Sherpa interface for 3g2A.

### NJET0q4gAA
Bespoke NJet/Sherpa interface for 4g2A.

### NN3g2A
Interface to [Joe's neural net](https://github.com/JosephPB/n3jet/tree/master/c%2B%2B_calls) for 3g2A.

## Old work

### 2g2A-gg
Failed attempt at alternative optimised integrator for 2g2A.

### 4l+j-OL
Simple example of Sherpa with external ME provider, OpenLoops in this case.

### ggAA-LO-BLHA
Attempt to ggAA as in [NJet+Sherpa](https://bitbucket.org/njet/njet/wiki/NJetSherpa/NJetSherpa). Failure.

### njet-min-eg-BLHA
Trying to get [NJet+Sherpa](https://bitbucket.org/njet/njet/wiki/NJetSherpa/NJetSherpa) working.

### ppAA-min-eg
Pure sherpa LO pp->AA, no PDFs, showering, fragmentation

### ppAA-resummation
Pure sherpa LO pp->AA, with PDFs, showering, fragmentation

### ppAAj
Pure sherpa LO pp->AAj, with PDFs only

# System configuration

* Linux distribution

```sh
$ distro | grep Name
Name: Fedora 32 (Workstation Edition)
```

* Shell

```sh
$ echo $shell
zsh
```

* Python version

```sh
$ python -V
Python 3.7.5
```

* Have set

```sh
LOCAL=$HOME/local
```

# Installation

In future, use [these scripts](https://gitlab.dur.scotgrid.ac.uk/hepsw/install)?

## Summary

To install everything,

```sh
sudo dnf install lhapdf lhapdf-devel python3-lhapdf root HepMC HepMC-devel openmpi openmpi-devel gcc gcc-c++ gcc-gfortran make sqlite sqlite-devel python3-devel python3-Cython ImageMagick libzip libzip-devel texinfo
```

then follow FastJet, FastJet Contrib, YODA, RIVET, MPI and finally Sherpa.

## [LHAPDF](https://lhapdf.hepforge.org/)

* Install with system package manager

```sh
sudo dnf install lhapdf lhapdf-devel python3-lhapdf
```

* Then `./configure` Sherpa appended with `--enable-lhapdf=/usr` and (re)make.

## [FastJet](http://fastjet.fr/quickstart.html)

* This is needed by Sherpa and RIVET

* Install with

```sh
cd ~/scratch
curl -O http://fastjet.fr/repo/fastjet-3.3.2.tar.gz
tar --extract --gzip --verbose --file=fastjet-3.3.2.tar.gz
rm -f fastjet-3.3.2.tar.gz
cd fastjet-3.3.2
echo "export FJ_LOCAL=$LOCAL/fastjet" >> ~/.zshrc
source ~/.zshrc
./configure --prefix=$FJ_LOCAL --enable-allcxxplugins
make -j && make -j install
```

* Then `./configure` Sherpa appended with `--enable-fastjet=$FJ_LOCAL` and (re)make.

## [FastJet Contrib](https://fastjet.hepforge.org/contrib/)

* This is needed by Rivet

* Install with

```sh
cd ~/scratch
wget http://fastjet.hepforge.org/contrib/downloads/fjcontrib-1.042.tar.gz
tar --extract --gzip --verbose --file=fjcontrib-1.042.tar.gz
rm -f fjcontrib-1.042.tar.gz
cd fjcontrib-1.042
./configure --prefix=$FJ_LOCAL
make -j && make -j install
```

## [ROOT](https://root.cern.ch/)

* Simply

```sh
sudo dnf install root
```

* then `./configure` Sherpa with `--enable-root` and (re)make.

## [YODA](https://yoda.hepforge.org/)

* Using YODA 1.8

* Install prerequisites

```sh
sudo dnf install python3-root python3-Cython
```

and set up

```sh
echo "export YODA_LOCAL=$LOCAL/yoda" >> ~/.zshrc
exec $SHELL
echo "source $YODA_LOCAL/yodaenv.sh" >> ~/.zshrc 
```

* Install YODA by sourcing from [git remote](https://gitlab.com/hepcedar/yoda)

```sh
cd ~/git
git clone -b release-1-8 git@gitlab.com:hepcedar/yoda.git
cd ~/git/yoda
autoreconf -fi
mkdir ~/scratch/yoda-build
cd ~/scratch/yoda-build
~/git/yoda/configure --prefix=$YODA_LOCAL
make -j && make -j install
cp ~/scratch/yoda-build/yodaenv.sh $YODA_LOCAL
```

* `yodamerge` is broken on Python 3.
I don't want to install a Python 2 YODA on my laptop for this, so I'll just use the IPPP system for `yodamerge`, which is a good idea anyway since I'm doing the parallel analysis on there.
To get that working, I used [Marian's scripts](https://gitlab.dur.scotgrid.ac.uk/hepsw/install) by adding `source /mt/hepsw/el7/x86_64/YODA-1.7.7/YODAenv.sh` to my shell runcom (`~/.profile`).

## [HepMC2](http://hepmc.web.cern.ch/hepmc/)

* Install via system pacakge manager

```sh
sudo dnf install HepMC HepMC-devel
```

* This installs HepMC in `/usr`.

## [Rivet](https://rivet.hepforge.org/trac/wiki/GettingStarted)

* Using Rivet 2.7

* After:
    * manual installations
        * FastJet
        * FastJet Contrib
        * YODA
    * package manager installations
        * HepMC2

* Install Rivet from [git remote](https://gitlab.com/hepcedar/rivet)

```sh
sudo dnf install python3-devel ImageMagick
cd ~/git
git clone -b release-2-7-x git@gitlab.com:hepcedar/rivet.git
cd ~/git/rivet
autoreconf -fi
mkdir ~/scratch/rivet
cd ~/scratch/rivet
echo "export RIVET_LOCAL=$LOCAL/rivet" >> ~/.zshrc
exec $SHELL
~/git/rivet/configure --prefix=$RIVET_LOCAL --with-yoda=$YODA_LOCAL --with-hepmc=/usr --with-fastjet=$FJ_LOCAL
make -j && make -j install
cp ~/scratch/rivet/rivetenv.sh ~/scratch/rivet/rivetenv.csh $RIVET_LOCAL
echo "source $RIVET_LOCAL/rivetenv.sh" >> ~/.zshrc
exec $SHELL
```

* If reinstalling or changing version, make sure to delete old `$RIVET_LOCAL` and `~/scratch/rivet` then `exec $SHELL` (so `rivetenv.sh` not sourced)

* Then `./configure` Sherpa with 

```sh
--enable-hepmc2=/usr --enable-rivet=$RIVET_LOCAL
```

## [MPI](https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.8.html#MPI-parallelization)

### Installation

* Either install 

	* [OpenMPI](https://www.open-mpi.org/)

	```sh
	sudo dnf install openmpi openmpi-devel
	TMP1=$(locate "bin/mpic++" | grep openmpi)
	TMP2=${TMP1%/*}
	echo $TMP2
	echo "export PATH=$TMP2:$PATH" >> ~/.zshrc
	TMP3=$(locate lib/libfabric.so.1)
	TMP4=${TMP3%/*}
	echo $TMP4
	echo "export LD_LIBRARY_PATH=$TMP4:$LD_LIBRARY_PATH" >> ~/.zshrc
	```

	where TMP2 and TMP4 should each be a single directory and `mpiexec` is also in $TMP2, check with `ls $TMP2 | grep mpiexec`.

	* or [MPICH](https://www.mpich.org/) (untested) - above with `sed "s/openmpi/mpich/g"`

* Then `./configure` Sherpa appended with `--enable-mpi` and (re)make.

### Usage

* MPI can be run with `orterun`, or with `mpiexec` or `mpirun`, which are both symlinks to `oretrun`. I'll use `mpirun`.
* Simply prepend command with `mpirun` to parallelise, for example, `mpirun Sherpa -f Run.dat`.
* When running locally or over `ssh`, MPI will spawn as many processes as there are physical CPU cores. To use all logical CPU core on hyperthreaded Intel processors, use the switch `--use-hwthread-cpus`, for example, `mpirun --use-hwthread-cpus Sherpa`
* You can also choose the number of processes, `<number>`, yourself with `-n <number>`.

## [Sherpa](https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.8.html)

* Using Sherpa 2.2.8

* Install with 

```sh
sudo dnf install gcc gcc-c++ gcc-gfortran make texinfo sqlite sqlite-devel libzip libzip-devel
cd ~/git
git clone -b rel-2-2-8 https://gitlab.com/sherpa-team/sherpa.git
cd ~/git/sherpa
autoreconf -fi
echo "export SHERPA_LOCAL=$LOCAL/sherpa" >> ~/.zshrc
exec $SHELL
./configure --prefix=$SHERPA_LOCAL --enable-openloops=$OL_PREFIX --enable-lhole --enable-gzip --enable-fastjet=$FJ_LOCAL --enable-mpi --enable-root --enable-hepmc2=/usr --enable-rivet=$RIVET_LOCAL --enable-lhapdf=/usr
make -j && make -j install
echo "export PATH=$SHERPA_LOCAL/bin:$PATH" >> ~/.zshrc
exec $SHELL
```

* lhole enables the Binoth Les Houches interface

* gzip helps with large disk space usage

* Further options specified above for each installed program

* Note that making in a separate build folder to the source doesn't work for some reason.

# HEP particle IDs (PDG/BLHA)

Particle    |ID
------------|---
d           |1
u           |2
s           |3
c           |4
b           |5
t           |6
$e^-$       |11
$\nu_e$     |12
$\mu^-$     |13
$\nu_\mu$   |14
$\tau^-$    |15
$\nu_\tau$  |16
$g$         |21
$\gamma$    |22
$Z$         |23
$W^+$       |24
Higgs       |25

Particle container|ID |Included particles (antiparticles implicit)
------------------|---|------------------
Charged leptons   |90 |$e^-, \mu^-, \tau^-$
Neutral leptops   |91 |$\nu_e,\nu_\mu,\nu_\tau$
Parton            |93 |d,u,s,c,b,t,$g$
Quark             |94 |d,u,s,c,b,t

* Antiparticles are designated by negative numbers.

# Clean up

Remove generated files with 

```sh
make clean
```

# Python configuration

## Python 2 virtual environment

* Some stuff may need Python 2 (e.g. old njet.py)

* To make Python 2 virtual environments from Python 3, we need the tool `virtualenv`.
Install with

```sh
sudo dnf install python3-virtualenv
```

* Set up virtual environment

```sh
mkdir ~/venvs
virtualenv ~/venvs/python2 -p /usr/bin/python2
```

* Activate with 

```sh
source ~/venvs/python2/bin/activate
```

* Deactivate with 

```sh
deactivate
```

## Python 3.x virtual environments

* YODA 1.7.7 doesn't immediately support Python beyond 3.6. You can either:
    * use a virtual environment on Python 3.6 to run it. This was the method used for `rivet-bootstrap`.
    * or, as in the YODA manual installation, fix the issue by having the latest Cython version installed and running `touch ~/scratch/YODA-1.7.7/pyext/yoda/*.pyx`, since the issue is due to the tarball build system having an old version of Cython.
* Here's what I did to set up the virtual environment (before scrapping this method in favour of the fix).

### Step 1: getting old versions of Python 3

* Get Python 3.6 with

```sh
sudo dnf install python36
```

### Step 2: setting up the virtual environment

* Create the virtual environment using

```sh
python3.6 -m venv ~/venvs/python3.6
```

* Activate with

```sh
source ~/venvs/python3.6/bin/activate 
```

* Deactivate with 

```sh
deactivate
```

## TODO

* Should have an abstract interface class that the individual interfaces inherit from
