---
title: gg $\rightarrow \gamma \gamma$ with custom analysis using optimised integrator
author: Ryan Moodie
date: 12 Feb 2020
---

## Results

* <https://www.ippp.dur.ac.uk/~rmoodie/files/research/2g2A-qq/>

## Usage

### Running

* Run on IPPP system
* Uses ME library and order file from [previous 2g2A](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/NJET0q2gAA)
    * Run `make` there first
    * Files are symlinked here
* Uses optimised integrator library from [here](https://gitlab.com/jetdynamics/njet-tools/-/tree/master/sherpa/2g2A-qq) 
    * Run `make` there first
* Does grid optimisation using optimised integrator
    * `make Results.db CORES=<number>`
* Uses [custom Rivet analysis](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/analysis-diphoton)
    * Run `make` there first
* Then do analysis here. 
    * On IPPP system,
        * `make analysis` or `./local-analyses <init seed> <num runs> <total num events>`
        * `make copy`
