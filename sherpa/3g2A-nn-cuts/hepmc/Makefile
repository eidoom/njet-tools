RUNCARD=Run.dat
N=3
LIB=libSherpaNN${N}g2A.so # symlink
START?=1
EVENTS?=2000000
CORES?=$(shell nproc)

.PHONY: clean reset all events

all: events

include ../../../Makefile.inc

# Generate matrix element mapping/process information
Process/: $(RUNCARD) $(LIB)
	Sherpa -f $< INIT_ONLY=1 -l process.log || exit 0

# Do integration (grid optimisation)
Results.db: $(RUNCARD) Process/
	mpirun -n $(CORES) Sherpa -f $< -e 0 -a 0 -l integration.log

# Alternative event generation and analysis on IPPP system
# Use on a single workstation
# makes part.*.yoda
events: $(RUNCARD) Results.db
	./events.sh $(START) $(CORES) $(EVENTS) $<

# Remove temporary files
clean:
	-rm part.*.yoda *.out *.err *.log Sherpa_References.tex
	-rm -r Status__*/

# Remove results of calculation
reset: clean
	-rm Analysis.yoda Results.db Results.db.bak moms diffs errs histos res *.pickle time rivet-plots/ *.hepmc2g
	-rm -r Process/
