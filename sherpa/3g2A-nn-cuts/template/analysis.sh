#!/usr/bin/env bash

for f in ../hepmc/Events.*.hepmc2g; do
    g=${f#../hepmc/Events.}
    h=${g%.hepmc2g}
    rivet --analysis-path ../../analysis-cuts -a diphoton -o Analysis.${h}.yoda -n $1 --ignore-beams $f >${h}.log 2>&1 &
done

wait
