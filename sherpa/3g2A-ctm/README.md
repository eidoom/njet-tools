---
title: gg $\rightarrow$ g$\gamma \gamma$ with custom analysis
author: Ryan Moodie
date: 23 Jan 2020
---

## Results

* <https://www.ippp.dur.ac.uk/~rmoodie/files/research/3g2A-ctm/>

## Usage

### Running

* Uses interface from [NJET0q3g2A](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/NJET0q2g3A)
    * Run `make` there first
* Uses results (integration) from [previous 3g2A](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/3g2A-std)
    * Run `make Results.db` there first
* Uses [custom Rivet analysis](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/analysis-diphoton)
    * Run `make` there first
* Above files are symlinked here
* Then do analysis here. 
    * On IPPP system,
        * `make analysis`
            * Alternatively use `./local-analyses.sh` to run in parallel in a single machine (use `htop` to see when done)
        * `make Analysis.yoda`
        * `make rivet-plots/`
        * `make copy`
    * On laptop,
        * Test custom analysis with `make test`
        * `make show`

* Running the analysis creates a HepMC file with the events. 
If custom analysis changed, can use events again without rerunning Sherpa.
Run `RIVET_ANALYSIS_PATH=../analysis-diphoton rivet -a diphoton Events.hepmc2g` to generate yoda from hepmc.
If batch run, do that for each hepmc to make many yodas then merge yodas.
Then rivet-mkhtml.
**This fails because diphoton.cc needs to know what the beam is, which it usually infers from event generation(?). Commented out.**

### Cleaning

* Delete temporary files with `make flush`.
* Get rid also of output with `make clean`
* Get rid also of analysis with `make reset`
* Also get rid of pdfs with `make wipe`

## TODO

* DONE Make plots like those in 
    * <https://inspirehep.net/record/1249405>
    * <https://inspirehep.net/record/1273454>
    * These are quark initiated
* Compare to gluon initiated
    * <https://inspirehep.net/record/499580>

## Notes

* Sherpa 2 requires Rivet 2 - not 3!
* See <https://github.com/scikit-hep/pyjet> for jet info
* See <https://gitlab.com/hepcedar/rivet/blob/master/doc/tutorials/makeplots.md> for plot info
* See <http://fastjet.fr/repo/fastjet-doc-3.2.2.pdf> for fastjet docs
* See <https://rivet.hepforge.org/code/dev/namespaceRivet.html> for Rivet reference
