RUNCARD=Run.dat
CORES?=$(shell nproc)
N=3
START?=1
JOBS?=200
EVENTS?=1000000

.PHONY: clean reset wipe all flush analysis copy

all: copy

include ../../Makefile.inc

# Generate matrix element mapping/process information
Process/: $(RUNCARD) libProc_fsrchannels$(N).so libSherpaNJET0q$(N)gAA.so OLE_contract_$(N)g2A.lh
	Sherpa -f $< INIT_ONLY=1 -l process.log || exit 0

# Do integration (grid optimisation)
Results.db: $(RUNCARD) Process/ libProc_fsrchannels$(N).so libSherpaNJET0q$(N)gAA.so OLE_contract_$(N)g2A.lh
	mpirun -n $(CORES) Sherpa -f $< -e 0 -a 0 -l integration.log

# Event generation and analysis on IPPP system 
# on laptop, just do `Sherpa -f $(RUNCARD)`
# on ssh, use local-analyses.sh
analysis: $(RUNCARD) Results.db Process/ libProc_fsrchannels$(N).so libSherpaNJET0q$(N)gAA.so OLE_contract_$(N)g2A.lh
	./analyses.sh $(START) $(JOBS) $(EVENTS)

# After above, on IPPP system, you must manually do:
Analysis.yoda: part.*.yoda
	yodamerge -o $@ $^

# Generate the plots with Rivet, using custom analysis
rivet-plots/: Analysis.yoda 
	RIVET_ANALYSIS_PATH=../analysis-diphoton rivet-mkhtml -n $(CORES) $<

copy: rivet-plots/
	rm -rf ~/www/files/research/$(N)g2A-qq
	cp -r $< ~/www/files/research/$(N)g2A-qq

# Remove temporary files
flush:
	rm -rf part.*.yoda *.out *.err *.log Status__* Sherpa_References.tex 

# Remove compiled files
clean: flush
	rm -rf rivet-plots Process/

# Remove results of calculation
reset: clean
	rm -rf Results.db* Analysis.yoda Events.hepmc2g

# Remove everything
wipe: reset
	rm *.pdf
