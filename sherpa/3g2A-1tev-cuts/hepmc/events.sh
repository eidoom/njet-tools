#!/usr/bin/env bash
# To run jobs on a single machine, do
# ./events.sh <initial seed> <number of runs> <total number of events> <Sherpa run card>

init_seed=$1
num_runs=$2
end_seed=$(($init_seed + $num_runs - 1))
total_events=$3
run_events=$(($total_events / $num_runs))

for rseed in $(seq -w ${init_seed} ${end_seed}); do
    Sherpa -f $4 -R $rseed -e $run_events EVENT_OUTPUT=HepMC_GenEvent[Events.$rseed] >${rseed}.log 2>&1 &
done
