---
title: gg $\rightarrow$ $\gamma \gamma$ with custom analysis
author: Ryan Moodie
date: 28 Jan 2020
---

## Results

* <https://www.ippp.dur.ac.uk/~rmoodie/files/research/2g2A-ctm/>

## Usage

### Running

* Uses contract file and interface library from [NJET0q2gAA](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/NJET0q2gAA)
    * Run `make` there first
* Uses grid optimisation results from [previous 2g2A](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/2g2A-std)
    * Run `make Results.db` there first
    * Files are symlinked here
* Uses [custom Rivet analysis](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/analysis-2g2A)
    * Run `make` there first
* Then do analysis here. 
    * On IPPP system,
        * `make analysis`
        * `make copy`

### Cleaning

* Delete temporary files with `make flush`.
* Get rid also of output with `make clean`
* Get rid also of analysis with `make reset`
* Also get rid of pdfs with `make wipe`

## TODO

* Compare to gluon initiated
    * <https://inspirehep.net/record/499580>

## Notes

* Sherpa 2 requires Rivet 2 - not 3!
* See <https://github.com/scikit-hep/pyjet> for jet info
* See <https://gitlab.com/hepcedar/rivet/blob/master/doc/tutorials/makeplots.md> for plot info
* See <http://fastjet.fr/repo/fastjet-doc-3.2.2.pdf> for fastjet docs
* See <https://rivet.hepforge.org/code/dev/namespaceRivet.html> for Rivet reference
