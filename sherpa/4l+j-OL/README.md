---
title: 4l+j with OL
author: Ryan Moodie
date: 26 Nov 2019
---

# 4l+j with OpenLoops

## Installing libraries

Install OpenLoops and `ppllll` library with

```sh
echo "export OL_PREFIX=$HOME/git/OpenLoops" >> ~/.zshrc
echo "export PATH=$OL_PREFIX:$PATH" >> ~/.zshrc
cd ~/git
git clone https://gitlab.com/openloops/OpenLoops.git
source ~/.zshrc
openloops libinstall ppllll
```

and `./configure` Sherpa with `--enable-openloops=$OL_PREFIX`

## Running

Run with `Sherpa` in this directory.
It fails.

