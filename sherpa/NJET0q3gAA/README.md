---
title: gg $\rightarrow$ g$\gamma \gamma$ --- NJet/Sherpa interface
author: Ryan Moodie
date: 21 Jan 2020
---

## Usage

### Running

* `make` will do all compiles: contract file and ME library

### Cleaning

* Get rid of compilations with `make clean`
* Also get rid of pdfs with `make wipe`

## References

* <https://twiki.cern.ch/twiki/pub/LHCPhysics/ProposalTtbb/readme_sherpa21.txt>
* <https://bitbucket.org/njet/njet/wiki/NJetSherpa/NJetSherpa>
* <https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.8.html>
* <https://rivet.hepforge.org/analyses/ATLAS_2017_I1591327.html>
