#!/usr/bin/env python3
# coding=UTF-8

import math

import matplotlib.pyplot
import matplotlib.ticker
import pandas


class ErrPlot:
    def __init__(self, filename):

        with open(filename, "r") as datafile:
            data = pandas.Series(
                [
                    float(line.rstrip("\n").split(" ")[2])
                    for line in datafile.readlines()
                ]
            )

        data = data.where(data < 0.35)
        data = data.where(0.1 < data)

        fig, ax = matplotlib.pyplot.subplots()

        data.hist(grid=False, bins=300, ax=ax, density=True)
        ax.set_xlabel("Fractional standard error")
        ax.set_ylabel("Percentage of sample")
        ax.set_title("Sample size: 1M")
        # ax.set_major_formatter(matplotlib.ticker.PercentFormatter(1))

        fig.savefig("err.pdf", bbox_inches="tight", format="pdf")
        fig.savefig("err.png", bbox_inches="tight", format="png", dpi=300)

        matplotlib.pyplot.show()


if __name__ == "__main__":
    dp = ErrPlot("errs")
