#!/usr/bin/env python3
# coding=UTF-8

import pickle

import matplotlib.pyplot


class Plots:
    def __init__(self):
        with open("res.pickle", "rb") as f:
            self.njet = pickle.load(f)

        # cut = 1e0
        # cut = float('inf')
        # print(min(njet))
        # print(len([a for a in njet if a < 1e-9]))
        # print(max([a for a in njet if a != float("inf")]))
        # print(len([a for a in njet if a >= cut]))

        # njet = [a for a in njet if a < cut]

        self.all()
        self.cut()

    def all(self):
        fig, ax = matplotlib.pyplot.subplots()

        ax.hist(self.njet, bins=100, histtype="step", log=True, label="NJet")
        ax.legend(loc="best")
        ax.set_xlabel("$|A|^2$")
        ax.set_ylabel("Fraction of sample")
        ax.set_title(f"Sample size: {len(self.njet)}")

        fig.savefig("values.png", bbox_inches="tight", format="png", dpi=300)

    def cut(self):
        fig, ax = matplotlib.pyplot.subplots()

        cut = 1e-2
        njet = [a for a in self.njet if a < cut]

        ax.hist(njet, bins=100, histtype="step", log=True, label=f"NJet < {cut}")
        ax.legend(loc="best")
        ax.set_xlabel("$|A|^2$")
        ax.set_ylabel("Fraction of sample")
        ax.set_title(
            f"Sample size: {len(njet)}. Discarded: {len(self.njet)-len(njet)}."
        )

        fig.savefig("values_cut.png", bbox_inches="tight", format="png", dpi=300)


if __name__ == "__main__":
    p = Plots()
