#include <complex>
#include <iostream>
#include <string>
#include <vector>

#include "ir/split_g2gg-analytic.h"
#include "ir/split_g2ggg-analytic.h"
#include "ngluon2/Mom.h"

std::string genHelStr(const std::vector<int>& helInt)
{
    std::string helStr;
    for (int hel : helInt) {
        helStr.append(hel == 1 ? "+" : "-");
    }
    return helStr;
}

int main(int argc, char** argv)
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);

    if (argc != 1) {
        std::cout << "Warning: " << argv[0] << " doesn't accept command line arguments.\n";
    }

    // double collinear
    {
        const std::vector<MOM<double>> mom5 {
            {
                { 0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03 },
                { 0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, -0.5000000000000000E+03 },
                { 0.4585787878854402E+03, 0.1694532203096798E+03, 0.3796536620781987E+03, -0.1935024746502525E+03 },
                { 0.3640666207368177E+03, -0.1832986929319185E+02, -0.3477043013193671E+03, 0.1063496077587081E+03 },
                { 0.1773545913777421E+03, -0.1511233510164880E+03, -0.3194936075883156E+02, 0.8715286689154436E+02 },
            },
        };

#ifdef DEBUG
        for (MOM<double> m : mom5) {
            std::cout << m << '\n';
        }
#endif

        const MOM<double> p1 { mom5[2] };
        const MOM<double> p2 { mom5[3] };
        const MOM<double> p3 { mom5[4] };

        const MOM<double> p12 { p1 + p2 };
        const double x12_3 { dot(p1, p2) / dot(p12, p3) };
        const MOM<double> p12t { p12 - x12_3 * p3 };
        const MOM<double> p3t { (1 + x12_3) * p3 };

        const std::vector<MOM<double>> mappedMom3 {
            { -p12t, p1, p2 }
        };

#ifdef DEBUG
        std::cout << "sum(mapped momenta)=" << p1 + p2 - p12t << '\n';
#endif

        Splitg2gg_a<double> col2(1.);
        col2.setMomenta(mappedMom3, p3t);

        std::vector<int> hel34 { -1, -1, 1 };
        std::complex<double> ansHel34 { col2.born(hel34.data()) };
        std::cout << "sp^0_{g>gg}(" << genHelStr(hel34) << ") = " << ansHel34 << '\n';

        std::vector<int> hel32 { -1, 1, -1 };
        std::complex<double> ansHel32 { col2.born(hel32.data()) };
        std::cout << "sp^0_{g>gg}(" << genHelStr(hel32) << ") = " << ansHel32 << '\n';

        std::complex<double> ans3 { col2.born() };
        std::cout << "<sp^0_{g>gg}>    = " << ans3 << '\n';
    }

    // triple collinear
    {
        const std::vector<MOM<double>> mom6 {
            { 0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03 },
            { 0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, -0.5000000000000000E+03 },
            { 0.8855133305450298E+02, -0.2210069028768998E+02, 0.4008035319168533E+02, -0.7580543095693663E+02 },
            { 0.3283294192270985E+03, -0.1038496118834563E+03, -0.3019337553895401E+03, 0.7649492138716589E+02 },
            { 0.1523581094674306E+03, -0.1058809596665922E+03, -0.9770963832697571E+02, 0.4954838522679282E+02 },
            { 0.4307611382509676E+03, 0.2318312618377385E+03, 0.3595630405248305E+03, -0.5023787565702211E+02 }
        };

        const MOM<double> p1 { mom6[2] };
        const MOM<double> p2 { mom6[3] };
        const MOM<double> p3 { mom6[4] };
        const MOM<double> p123 { p1 + p2 + p3 };
        const MOM<double> p4 { mom6[5] };
        const double s123 { S(p123) };
        const double x123_4 { s123 / 2. / dot(p123, p4) };
        const MOM<double> p123t { p123 - x123_4 * p4 };
        const MOM<double> p4t { (1. + x123_4) * p4 };

        const std::vector<MOM<double>> mappedMom4 {
            { -p123t, p1, p2, p3 }
        };

        Splitg2ggg_a<double> col3(1.);
        col3.setMomenta(mappedMom4, p4t);

        std::vector<int> hel414 { -1, 1, 1, 1 };
        std::complex<double> ansHel14 { col3.born(hel414.data()) };
        std::cout << "sp^0_{g>ggg}(" << genHelStr(hel414) << ") = " << ansHel14 << '\n';

        std::vector<int> hel44 { -1, -1, 1, -1 };
        std::complex<double> ansHel4 { col3.born(hel44.data()) };
        std::cout << "sp^0_{g>ggg}(" << genHelStr(hel44) << ") = " << ansHel4 << '\n';
    }

    return 0;
}
