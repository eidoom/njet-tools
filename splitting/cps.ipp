#include <algorithm>
#include <cassert>
#include <iostream>
#include <numeric>
#include <string>
#include <typeinfo>

template <typename T>
cps<T>::cps(const int r_, const T sqrtS_)
    : r(r_)
    , sqrtS(sqrtS_)
{
}

template <typename T>
template <std::size_t mul>
void cps<T>::test(const std::array<MOM<T>, mul>& momenta)
{
    T zero { 1e-10 };
    std::for_each(momenta.cbegin(), momenta.cend(), [zero](const MOM<T>& m) { assert(abs(m.mass()) <= zero); });
    MOM<T> sum { std::accumulate(momenta.cbegin(), momenta.cend(), MOM<T>()) };
    assert(sum.x0 <= zero);
    assert(sum.x1 <= zero);
    assert(sum.x2 <= zero);
    assert(sum.x3 <= zero);
}

template <typename T>
template <std::size_t mul>
void cps<T>::print(const std::array<MOM<T>, mul>& momenta)
{
    std::cout << "Full kinematics\n";

    // std::for_each(momenta.begin(), momenta.end(), [n = 0](const MOM<T>& m) mutable { std::cout << "p" << n++ << "=" << m << '\n'; });
    for (std::size_t i { 0 }; i < mul; ++i) {
        std::cout << "p" << i << "=" << momenta[i] << '\n';
    }
    std::cout << '\n';

    for (std::size_t i { 0 }; i < mul; ++i) {
        for (std::size_t j { i + 1 }; j < mul; ++j) {
            const T s { 2 * dot(momenta[i], momenta[j]) };
            std::cout << "s" << i << j << "=" << ((s >= 0) ? " " : "") << s << "  ";
        }
        std::cout << '\n';
    }
}

template <typename T>
std::array<MOM<T>, 5>
cps<T>::c5(const T y1, const T y2, const T theta, const T alpha)
{
    assert(0. <= y1);
    assert(0. <= y2);
    assert(y1 + y2 <= 1.);

    const int mul { 5 };
    assert(r < mul);
    std::array<MOM<T>, mul> momenta;

    const T x1 { static_cast<T>(1.) - y1 };
    const T x2 { static_cast<T>(1.) - y2 };

    const T cosBeta { static_cast<T>(1.) + static_cast<T>(2.) * (static_cast<T>(1.) - x1 - x2) / x1 / x2 };
    assert((-1. <= cosBeta) && (cosBeta <= 1.));
    const T sinBeta { sqrt(static_cast<T>(1.) - pow(cosBeta, 2.)) };
    assert((-1. <= sinBeta) && (sinBeta <= 1.));

    assert((0. <= theta) && (theta <= M_PI));
    const T cosTheta { cos(theta) };
    const T sinTheta { sin(theta) };

    assert((0. <= alpha) && (alpha <= M_PI));
    const T cosAlpha { cos(alpha) };
    const T sinAlpha { sin(alpha) };

    momenta[0] = sqrtS / static_cast<T>(2.) * MOM<T>(-static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), -static_cast<T>(1.));
    momenta[1] = sqrtS / static_cast<T>(2.) * MOM<T>(-static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), static_cast<T>(1.));
    momenta[2] = x1 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), sinTheta, static_cast<T>(0.), cosTheta);
    momenta[3] = x2 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), cosAlpha * cosTheta * sinBeta + cosBeta * sinTheta, sinAlpha * sinBeta, cosBeta * cosTheta - cosAlpha * sinBeta * sinTheta);
    momenta[4] = -std::accumulate(momenta.begin(), momenta.end() - 1, MOM<T>());

    test(momenta);

    std::rotate(momenta.begin(), momenta.begin() + r, momenta.end());

#ifdef DEBUG
    print(momenta);
#endif

    return momenta;
}

template <typename T>
std::array<MOM<T>, 5>
cps<T>::c5b(const T y1, const T theta, const T alpha)
{
    assert(0. <= y1);

    const int mul { 5 };
    std::array<MOM<T>, mul> momenta;

    const T x1 { static_cast<T>(1.) - y1 };
    assert((0. <= x1) && (x1 <= 1.));

    assert((0. <= theta) && (theta <= M_PI));
    const T ct { cos(theta) };
    const T st { sin(theta) };

    assert((0. <= alpha) && (alpha <= M_PI));
    const T ca { cos(alpha) };
    const T sa { sin(alpha) };

    const T x2 { static_cast<T>(8.) / (8 - static_cast<T>(2.) * x1 + static_cast<T>(2.) * ca * x1 - static_cast<T>(2.) * (ct * ct) * x1 + static_cast<T>(2.) * ca * (ct * ct) * x1 + static_cast<T>(2.) * (st * st) * x1 - static_cast<T>(2.) * ca * (st * st) * x1) - (8 * x1) / (8 - static_cast<T>(2.) * x1 + static_cast<T>(2.) * ca * x1 - static_cast<T>(2.) * (ct * ct) * x1 + static_cast<T>(2.) * ca * (ct * ct) * x1 + static_cast<T>(2.) * (st * st) * x1 - static_cast<T>(2.) * ca * (st * st) * x1) };
    assert((0. <= x2) && (x2 <= 1.));
    assert((0. <= 2 - x1 - x2) && (2 - x1 - x2 <= 1.));

    momenta[0] = sqrtS / static_cast<T>(2.) * MOM<T>(-static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), -static_cast<T>(1.));
    momenta[1] = sqrtS / static_cast<T>(2.) * MOM<T>(-static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), static_cast<T>(1.));
    momenta[2] = x1 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), st, static_cast<T>(0.), ct);
    momenta[3] = x2 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), st, -ct * sa, ca * ct);
    momenta[4] = -std::accumulate(momenta.begin(), momenta.end() - 1, MOM<T>());

    test(momenta);

    std::rotate(momenta.begin(), momenta.begin() + r, momenta.end());

#ifdef DEBUG
    print(momenta);
#endif

    return momenta;
}

template <typename T>
std::array<MOM<T>, 6>
cps<T>::c6(const T alpha, const T beta, const T gamma)
{
    const int mul { 6 };
    assert(r < mul);
    std::array<MOM<T>, mul> momenta;

    const T x1 { 0.12 };
    const T x2 { 0.37 };

    const T ca { cos(alpha) };
    const T cb { cos(beta) };
    const T cg { cos(gamma) };

    const T sa { sin(alpha) };
    const T sb { sin(beta) };
    const T sg { sin(gamma) };

    // x3 fixes masslessness of p5
    const T x3 {
        (4 - 4 * x1 + x1 * x1 - ca * ca * x1 * x1 - sa * sa * x1 * x1 - 4 * x2 + 2 * x1 * x2 - 2 * ca * ca * cb * x1 * x2 - 2 * sa * sa * x1 * x2 + x2 * x2 - ca * ca * cb * cb * x2 * x2 - sa * sa * x2 * x2 - ca * ca * sb * sb * x2 * x2) / (2 * (2 - x1 + ca * ca * cb * x1 + cg * sa * sa * x1 - ca * sa * sb * sg * x1 - x2 + ca * ca * cb * cb * x2 + cg * sa * sa * x2 + ca * ca * cg * sb * sb * x2))
    };

    // generate momenta recursively
    // incoming momenta are back-to-back
    momenta[0] = -sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), static_cast<T>(1.));
    momenta[1] = -sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), -static_cast<T>(1.));
    // p2 = rotate p1 by alpha about x
    momenta[2] = x1 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), static_cast<T>(0.), -sa, ca);
    // p3 = rotate p2 by beta about y
    momenta[3] = x2 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), ca * sb, -sa, ca * cb);
    // p4 = rotate p3 by gamma about z
    momenta[4] = x3 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), ca * cg * sb + sa * sg, -(cg * sa) + ca * sb * sg, ca * cb);
    // p5 given by momentum conservation
    momenta[5] = -std::accumulate(momenta.begin(), momenta.end() - 1, MOM<T>());

    test(momenta);

    std::rotate(momenta.begin(), momenta.begin() + r, momenta.end());

#ifdef DEBUG
    print(momenta);
#endif

    return momenta;
}

template <typename T>
std::array<MOM<T>, 7>
cps<T>::c7(const T alpha, const T beta, const T gamma, const T delta)
{
    const int mul { 7 };
    assert(r < mul);
    std::array<MOM<T>, mul> momenta;

    const T x1 { 0.12 };
    const T x2 { 0.37 };
    const T x3 { 0.23 };

    const T ca { cos(alpha) };
    const T cb { cos(beta) };
    const T cg { cos(gamma) };
    const T cd { cos(delta) };

    const T sa { sin(alpha) };
    const T sb { sin(beta) };
    const T sg { sin(gamma) };
    const T sd { sin(delta) };

    // x4 fixes masslessness of p6
    const T x4 {
        (4 - 4 * x1 + x1 * x1 - ca * ca * (x1 * x1) - sa * sa * (x1 * x1) - 4 * x2 + 2 * x1 * x2 - 2 * (ca * ca) * cb * x1 * x2 - 2 * (sa * sa) * x1 * x2 + x2 * x2 - ca * ca * (cb * cb) * (x2 * x2) - sa * sa * (x2 * x2) - ca * ca * (sb * sb) * (x2 * x2) - 4 * x3 + 2 * x1 * x3 - 2 * (ca * ca) * cb * x1 * x3 - 2 * cg * (sa * sa) * x1 * x3 + 2 * ca * sa * sb * sg * x1 * x3 + 2 * x2 * x3 - 2 * (ca * ca) * (cb * cb) * x2 * x3 - 2 * cg * (sa * sa) * x2 * x3 - 2 * (ca * ca) * cg * (sb * sb) * x2 * x3 + x3 * x3 - ca * ca * (cb * cb) * (x3 * x3) - cg * cg * (sa * sa) * (x3 * x3) - ca * ca * (cg * cg) * (sb * sb) * (x3 * x3) - sa * sa * (sg * sg) * (x3 * x3) - ca * ca * (sb * sb) * (sg * sg) * (x3 * x3)) / (2. * (2 - x1 + ca * ca * cb * cd * x1 + cd * cg * (sa * sa) * x1 + ca * cb * sa * sd * x1 - ca * cg * sa * sd * x1 - ca * cd * sa * sb * sg * x1 + ca * ca * sb * sd * sg * x1 - x2 + ca * ca * (cb * cb) * cd * x2 + cd * cg * (sa * sa) * x2 + ca * ca * cg * (sb * sb) * x2 + ca * cb * sa * sd * x2 - ca * cb * cg * sa * sd * x2 + ca * sa * sb * sg * x2 - ca * cd * sa * sb * sg * x2 + ca * ca * cb * sb * sd * sg * x2 - x3 + ca * ca * (cb * cb) * cd * x3 + cd * (cg * cg) * (sa * sa) * x3 + ca * ca * (cg * cg) * (sb * sb) * x3 + 2 * ca * cg * sa * sb * sg * x3 - 2 * ca * cd * cg * sa * sb * sg * x3 + sa * sa * (sg * sg) * x3 + ca * ca * cd * (sb * sb) * (sg * sg) * x3))
    };

    // generate momenta recursively
    // incoming momenta are back-to-back
    momenta[0] = -sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), static_cast<T>(1.));
    momenta[1] = -sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), -static_cast<T>(1.));
    // p2 = rotate p1 by alpha about x
    momenta[2] = x1 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), static_cast<T>(0.), -sa, ca);
    // p3 = rotate p2 by beta about y
    momenta[3] = x2 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), ca * sb, -sa, ca * cb);
    // p4 = rotate p3 by gamma about z
    momenta[4] = x3 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), ca * cg * sb + sa * sg, -(cg * sa) + ca * sb * sg, ca * cb);
    // p5 given by momentum conservation
    momenta[5] = x4 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), ca * cg * sb + sa * sg, -(ca * cb * sd) + cd * (-(cg * sa) + ca * sb * sg), ca * cb * cd + sd * (-(cg * sa) + ca * sb * sg));
    momenta[6] = -std::accumulate(momenta.begin(), momenta.end() - 1, MOM<T>());

    test(momenta);

    std::rotate(momenta.begin(), momenta.begin() + r, momenta.end());

#ifdef DEBUG
    print(momenta);
#endif

    return momenta;
}
