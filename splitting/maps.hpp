#ifndef MAPS_HPP
#define MAPS_HPP

#include <array>

#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

namespace maps {

// Catani-Seymour mapping
template <typename T, int mulL_, int mulR_, int mulS_>
class Map {
protected:
    static constexpr int mulF { mulL_ };
    static constexpr int mulR { mulR_ };
    static constexpr int mulS { mulS_ };
    int nc; // nc=number collinear, eg. g2gg is nc=2
    std::array<int, mulF> o;
    std::array<MOM<T>, mulR> reducedMom;
    std::array<MOM<T>, mulS> splitMom;

    void print();
    void test();

    const std::array<MOM<T>, mulS_>& splitPS() const;
    const std::array<MOM<T>, mulR_>& reducedPS() const;
    virtual const MOM<T>& recoil() const = 0;

    Map(int o1);
};

// Catani-Seymour 3->2 mapping
// double collinear
// for o1||(o1+1) (zero indexed) (12;3)
template <typename T, int mulF, int mulR>
class Map2 : protected Map<T, mulF, mulR, 3> {
private:
    const MOM<T> p1;
    const MOM<T> p2;
    const MOM<T> p3;
    const MOM<T> p12;
    const T x12_3;
    const MOM<T> p12t;
    const MOM<T> p3t;

protected:
    using base = Map<T, mulF, mulR, 3>;
    using base::mulS;
    using base::nc;
    using base::o;
    using base::print;
    using base::reducedMom;
    using base::splitMom;

    const MOM<T>& recoil() const override;

    Map2(int o1, const std::array<MOM<T>, mulF>& mom);
};

// Catani-Seymour 4->2 mapping
// triple collinear
// for o1||(o1+1)||(o1+2) (zero indexed) (123;4)
template <typename T, int mulF, int mulR>
class Map3 : protected Map<T, mulF, mulR, 4> {
private:
    const MOM<T> p1;
    const MOM<T> p2;
    const MOM<T> p3;
    const MOM<T> p123;
    const MOM<T> p4;
    const T s123;
    const T x123_4;
    const MOM<T> p123t;
    const MOM<T> p4t;

protected:
    using base = Map<T, mulF, mulR, 4>;
    using base::mulS;
    using base::nc;
    using base::o;
    using base::print;
    using base::reducedMom;
    using base::splitMom;

    const MOM<T>& recoil() const override;

    Map3(int o1, const std::array<MOM<T>, mulF>& mom);
};

// Catani-Seymour 4->2 mapping on 7-point PS
// for o1||(o1+1)||(o1+2) (zero indexed) (123;4)
template <typename T>
class Map7_5 : protected Map3<T, 7, 5> {
protected:
    using base = Map3<T, 7, 5>;
    using base::mulF;
    using base::mulR;
    using base::mulS;
    using base::o;

public:
    Map7_5(int o1, const std::array<MOM<T>, mulF>& mom)
        : base(o1, mom)
    {
    }
};

// Catani-Seymour 4->2 mapping on 6-point PS
// for o1||(o1+1)||(o1+2) (zero indexed) (123;4)
template <typename T>
class Map6_4 : protected Map3<T, 6, 4> {
protected:
    using base = Map3<T, 6, 4>;
    using base::mulF;
    using base::mulR;
    using base::mulS;
    using base::o;

public:
    Map6_4(int o1, const std::array<MOM<T>, mulF>& mom)
        : base(o1, mom)
    {
    }
};

// Catani-Seymour 3->2 mapping on 6-point PS
// for o1||(o1+1) (zero indexed) (12;3)
template <typename T>
class Map6_5 : protected Map2<T, 6, 5> {
protected:
    using base = Map2<T, 6, 5>;
    using base::mulF;
    using base::mulR;
    using base::mulS;
    using base::o;

public:
    Map6_5(int o1, const std::array<MOM<T>, mulF>& mom6)
        : base(o1, mom6)
    {
    }
};

// Catani-Seymour 3->2 mapping on 5-point PS
// for o1||(o1+1) (zero indexed) (12;3)
template <typename T>
class Map5_4 : protected Map2<T, 5, 4> {
protected:
    using base = Map2<T, 5, 4>;
    using base::mulF;
    using base::mulR;
    using base::mulS;
    using base::o;

public:
    Map5_4(int o1, const std::array<MOM<T>, mulF>& mom5)
        : base(o1, mom5)
    {
    }
};

} // namespace maps

#include "maps.ipp"

#endif // MAPS_HPP
