#include <array>
#include <cmath>
#include <iostream>

#include "ngluon2/Mom.h"

#include "cf.hpp"
#include "cps.hpp"

// degree: of collinearity (lambda=10^-degree)
// point: in phase space (between 0 and pi)
template <typename T>
void run_6(T degree, T point, bool sq, bool trees, bool loops)
{
    std::cout << "Lambda=" << degree << ", PS(" << point << ")\n";

    const T lambda { pow(10, -degree) };
    const std::array<MOM<T>, 6> mom6 { cps<T>(2).c6(point, lambda, lambda) };
    cf::Compare6_4_g2qqg<T> compare(0, mom6);

    compare.all(sq, trees, loops);

    std::cout << '\n';
}

template <typename T>
void run_6_b(T degree, T point, bool sq, bool trees, bool loops)
{
    std::cout << "Lambda=" << degree << ", PS(" << point << ")\n";

    const T lambda { pow(10, -degree) };
    const std::array<MOM<T>, 6> mom6 { cps<T>(0).c6(point, lambda, lambda) };
    cf::Compare6_4_g2qqg_b<T> compare(2, mom6);

    compare.all(sq, trees, loops);

    std::cout << '\n';
}

int main(int argc, char** argv)
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    if (argc != 1) {
        std::cout << "Warning: " << argv[0] << " doesn't accept command line arguments.\n\n";
    }

    // sq, trees, loops

    // run_6<qd_real>(8, M_PI / 3, false, true, false);
    // run_6<qd_real>(8, M_PI / 3, true, true, false);

    run_6_b<qd_real>(8, M_PI / 3, false, true, false);
    // run_6_b<qd_real>(8, M_PI / 3, true, true, false);

    // run_7<qd_real>(8, M_PI / 3, false, true, false);

    std::cout << '\n';
    return 0;
}
