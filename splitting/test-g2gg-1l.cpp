#include <array>
#include <cmath>
#include <iostream>

#include "ngluon2/Mom.h"

#include "cf.hpp"
#include "cps.hpp"

// template <typename T>
// void run_5b(T degree, T finite1, T finite2, bool sq, bool trees, bool loops)
// {
//     std::cout << "g2gg: 5g 2||3 (zero indexed), 1-loop, PS(" << finite1 << ", " << finite2 << ")\n\n";

//     const std::array<MOM<T>, 5> mom { cps::c5b<T>(finite1, finite2, pow(10, -degree)) };

//     cf::Compare5_4<T> compare(2, mom);
//     // std::array<int, 5> hel;

//     // hel = { -1, -1, -1, +1, +1, +1 };
//     // compare.helAmpF(hel, true);

// compare.all(sq, trees, loops);

//     std::cout << '\n';
// }

template <typename T>
void run_5(T degree, T finite1, T finite2, T finite3, bool sq, bool trees, bool loops, bool hs)
{
    std::cout << "Lambda=" << degree << ", PS(" << finite1 << ", " << finite2 << ")\n";

    const std::array<MOM<T>, 5> mom { cps<T>(0).c5(pow(10., -degree), finite1, finite2, finite3) };

    cf::Compare5_4_g2gg<T> compare(2, mom);

    if (hs) {
        if (trees) {
            compare.amp2T();
        }
        if (loops) {
            compare.amp2V();
        }
    } else {
        compare.all(sq, trees, loops);
    }

    std::cout << '\n';
}

template <typename T>
void run_6(T degree, T finite1, T finite2, bool sq, bool trees, bool loops, bool hs)
{
    std::cout << "Lambda=" << degree << ", PS(" << finite1 << ", " << finite2 << ")\n";

    const std::array<MOM<T>, 6> mom { cps<T>().c6(finite1, finite2, pow(10, -degree)) };

    cf::Compare6_5_g2gg<T> compare(3, mom);

    if (hs) {
        if (trees) {
            compare.amp2T();
        }
        if (loops) {
            compare.amp2V();
        }
    } else {
        compare.all(sq, trees, loops);
    }

    std::cout << '\n';
}

template <typename T>
void run_6b(T degree, T finite1, T finite2, bool sq, bool trees, bool loops)
{
    std::cout << "Lambda=" << degree << ", PS(" << finite1 << ", " << finite2 << ")\n";

    const std::array<MOM<T>, 6> mom { cps<T>().c6(finite1, pow(10, -degree), finite2) };

    cf::Compare6_5_g2gg<T> compare(2, mom);

    compare.all(sq, trees, loops);

    std::cout << '\n';
}

int main(int argc, char** argv)
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    if (argc != 1) {
        std::cout << "Warning: " << argv[0] << " doesn't accept command line arguments.\n\n";
    }

    // sq (colour-sum), trees, loops, helicity-sum (overrides sq)

    // 5 point kinematics is wrong!

    // run_5<qd_real>(10, 0.1, M_PI / 2., M_PI / 2., false, true, false, false);
    // run_5<qd_real>(10, 0.1, M_PI / 2., M_PI / 2., false, false, true, false);
    // run_5<qd_real>(10, 0.1, M_PI / 2., M_PI / 2., false, true, true, false);

    // run_5<qd_real>(10, 0.1, M_PI / 2., M_PI / 2., true, true, false, false);
    // run_5<qd_real>(10, 0.1, M_PI / 2., M_PI / 2., true, false, true, false);
    // run_5<qd_real>(10, 0.1, M_PI / 2., M_PI / 2., true, true, true, false);

    run_5<qd_real>(10, 0.1, M_PI / 2., M_PI / 2., true, true, false, true);
    // run_5<qd_real>(10, 0.1, M_PI / 2., M_PI / 2., true, false, true, true);
    // run_5<qd_real>(10, 0.1, M_PI / 2., M_PI / 2., true, true, true, true);

    // run_6<double>(2, M_PI / 3, M_PI / 5, false, true, false, false);
    // run_6<double>(2, M_PI / 3, M_PI / 5, false, false, true, false);
    // run_6<double>(2, M_PI / 3, M_PI / 5, false, true, true, false);

    // run_6<double>(7, M_PI / 3, M_PI / 5, true, true, false, false);
    // run_6<double>(3, M_PI / 3, M_PI / 5, true, false, true, false);
    // run_6<dd_real>(10, M_PI / 3, M_PI / 5, true, true, true, false);
    
    // run_6<double>(2, M_PI / 3, M_PI / 5, true, true, false, true);
    // run_6<double>(2, M_PI / 3, M_PI / 5, true, false, true, true);
    // run_6<double>(3, M_PI / 3, M_PI / 5, true, true, true, true);

    return 0;
}
