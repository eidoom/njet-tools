#include <iomanip>
#include <iostream>
#include <vector>

//#include "analytic/0q3g2A-analytic.h"
//#include "analytic/0q5g-analytic.h"
//#include "analytic/2q3g-analytic.h"
#include "chsums/0q3gA.h"
#include "chsums/0q4gA.h"
#include "chsums/0q5g.h"
#include "chsums/0q6g.h"
#include "chsums/2q1gA.h"
#include "chsums/2q2gA.h"
#include "chsums/2q3g.h"
#include "chsums/2q4g.h"
//#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/EpsQuintuplet.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

const int W { 7 };

const Flavour<double> Ax {
    StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C())
};

const Flavour<double> Au {
    StandardModel::Au(StandardModel::u(), StandardModel::ubar())
};

const int Nc { 3 };
const int Nf { 5 };
const double mur2 { 1. };

// five-point
// ~~~~~~~~~~

// for leading order, we can use the pentagon functions implementation (adjoint colour basis)
// colour bases described in Sec. 2.1 of
// https://eidoom.gitlab.io/phd-first-year-report/progress-report-first-year-phd.pdf
//template <typename T, typename P>
//void hel3g2a_p(const std::vector<MOM<T>>& momenta)
//{
//    Amp0q3g2A_a2l<T, P> amp(Ax);
//    amp.setNf(Nf);
//    amp.setNc(Nc);
//    amp.setMuR2(mur2);
//
//    amp.setMomenta(momenta);
//    amp.initFinRem();
//    amp.setSpecFuncs1L();
//
//    T born { amp.c1lx1l_fin() };
//    std::cout << std::setw(W) << "A11: " << born << '\n';
//
//    for (auto h : Amp0q3gAAStatic::HSarr) {
//        for (int i { 0 }; i < 5; ++i) {
//            std::cout << (h[i] == 1 ? '+' : '-');
//        }
//        std::cout << "\n";
//
//        amp.setHelicity(h);
//        std::complex<T> part { amp.AFp()[0].loop };
//        std::cout << std::setw(W) << "pQ: " << part << '\n';
//
//        T bornH { amp.c1lx1l_fin(h) };
//        std::cout << std::setw(W) << "hA11: " << bornH << '\n';
//    }
//}

// alternatively, can use old implementation (fundamental colour basis)
//template <typename T>
//void hel3g2a(const std::vector<MOM<T>>& momenta)
//{
//    Amp0q3g2A_a<T> amp(Ax);
//    amp.setNf(Nf);
//    amp.setNc(Nc);
//    amp.setMuR2(mur2);
//
//    amp.setMomenta(momenta);
//
//    T born { amp.virtsq().get0().real() };
//    std::cout << std::setw(W) << "A11: " << born << '\n';
//
//    for (auto h : Amp0q3gAAStatic::HSarr) {
//        for (int i { 0 }; i < 5; ++i) {
//            std::cout << (h[i] == 1 ? '+' : '-');
//        }
//        std::cout << "\n";
//
//        // return the quark loop partial amplitude
//        // with gluon ordering (0,1,2) and photons at (3,4)
//        amp.setHelicity(h);
//        std::complex<T> pQ { amp.AF(0, 1, 2).loop.get0() };
//        std::cout << std::setw(W) << "pQ: " << pQ << '\n';
//
//        // return the helicity amplitude squared
//        // one-loop squared
//        T born { amp.virtsq(h).get0().real() };
//        std::cout << std::setw(W) << "hA11: " << born << '\n';
//    }
//}

//template <typename T>
//void hel5g(const std::vector<MOM<T>>& momenta)
//{
//    Amp0q5g_a<T> amp;
//    amp.setNf(Nf);
//    amp.setNc(Nc);
//    amp.setMuR2(mur2);
//
//    amp.setMomenta(momenta);
//
//    Eps3<T> virt { amp.virt() };
//    std::cout << std::setw(W) << "A10: " << virt << '\n';
//
//    for (auto h : Amp0q5gStatic::HSarr) {
//        for (int i { 0 }; i < 5; ++i) {
//            std::cout << (h[i] == 1 ? '+' : '-');
//        }
//        std::cout << "\n";
//
//        amp.setHelicity(h);
//
//        // quark loop
//        Eps3<T> pQ { amp.AF(0, 1, 2, 3, 4).loop };
//        std::cout << std::setw(W) << "pQ: " << pQ << '\n';
//
//        // gluon loop
//        Eps3<T> pG { amp.AL(0, 1, 2, 3, 4).loop };
//        std::cout << std::setw(W) << "pG: " << pG << '\n';
//
//        // one-loop * tree
//        Eps3<T> virt { amp.virt(h) };
//        std::cout << std::setw(W) << "hA10: " << virt << '\n';
//    }
//}

//template <typename T>
//void hel2q3g(const std::vector<MOM<T>>& momenta)
//{
//    Amp2q3g_a<T> amp;
//    amp.setNf(Nf);
//    amp.setNc(Nc);
//    amp.setMuR2(mur2);
//
//    amp.setMomenta(momenta);
//
//    Eps3<T> virt { amp.virt() };
//    std::cout << std::setw(W) << "A10: " << virt << '\n';
//
//    for (auto h : Amp2q3gStatic::HSarr) {
//        for (int i { 0 }; i < 5; ++i) {
//            std::cout << (h[i] == 1 ? '+' : '-');
//        }
//        std::cout << "\n";
//
//        amp.setHelicity(h);
//
//        Eps3<T> pQ { amp.AF(0, 1, 2, 3, 4).loop };
//        std::cout << std::setw(W) << "pQ: " << pQ << '\n';
//
//        // mixed gluon and quark loop
//        Eps3<T> pM { amp.AL(0, 1, 2, 3, 4).loop };
//        std::cout << std::setw(W) << "pM: " << pM << '\n';
//
//        Eps3<T> virt { amp.virt(h) };
//        std::cout << std::setw(W) << "hA10: " << virt << '\n';
//    }
//}

    template <typename T>
void hel2q1g2a(const std::vector<MOM<T>>& momenta)
{
    // photons are coupled to external quarks
    Amp2q1gAA<T> amp(Au);
    amp.setNf(Nf);
    amp.setNc(Nc);
    amp.setMuR2(mur2);
    amp.setMomenta(momenta);

    // photons are coupled to the quark loop
    Amp2q1gAAx<T> ampxx(Ax);
    ampxx.setNf(Nf);
    ampxx.setNc(Nc);
    ampxx.setMuR2(mur2);
    ampxx.setMomenta(momenta);

    Eps3<T> virt { amp.virt() + ampxx.virt() };
    std::cout << std::setw(W) << "A10: " << virt << '\n';

    for (auto h : Amp2q1gAAStatic::HSarr) {
        for (int i { 0 }; i < 5; ++i) {
            std::cout << (h[i] == 1 ? '+' : '-');
        }
        std::cout << "\n";

        amp.setHelicity(h);
        ampxx.setHelicity(h);

        // photons are coupled to external quarks
        Eps3<T> pM { amp.AL(0, 1, 2).loop };
        std::cout << std::setw(W) << "pM: " << pM << '\n';

        // photons are coupled to the quark loop
        Eps3<T> pQxx { ampxx.AFxx(0, 1, 2).loop };
        std::cout << std::setw(W) << "pQll: " << pQxx << '\n';

        Eps3<T> virt { amp.virt(h) + ampxx.virt(h) };

        std::cout << std::setw(W) << "hA10: " << virt << '\n';
    }
}

// six-point
// ~~~~~~~~~~

    template <typename T>
void hel4g2a(const std::vector<MOM<T>>& momenta)
{
    Amp0q4gAA<T> amp(Ax);
    amp.setNf(Nf);
    amp.setNc(Nc);
    amp.setMuR2(mur2);

    amp.setMomenta(momenta);

    for (auto h : Amp0q4gAAStatic::HSarr) {
        for (int i { 0 }; i < 6; ++i) {
            std::cout << (h[i] == 1 ? '+' : '-');
        }
        std::cout << ": ";

        T born { amp.virtsq(h).get0().real() };
        std::cout << std::setw(W) << "hA11: " << born << '\n';
    }
}

    template <typename T>
void hel6g(const std::vector<MOM<T>>& momenta)
{
    Amp0q6g<T> amp;
    amp.setNf(Nf);
    amp.setNc(Nc);
    amp.setMuR2(mur2);

    amp.setMomenta(momenta);

    for (auto h : Amp0q6gStatic::HSarr) {
        for (int i { 0 }; i < 6; ++i) {
            std::cout << (h[i] == 1 ? '+' : '-');
        }
        std::cout << ": ";

        Eps3<T> virt { amp.virt(h) };

        std::cout << std::setw(W) << "hA10: " << virt << '\n';
    }
}

    template <typename T>
void hel2q4g(const std::vector<MOM<T>>& momenta)
{
    Amp2q4g<T> amp;
    amp.setNf(Nf);
    amp.setNc(Nc);
    amp.setMuR2(mur2);

    amp.setMomenta(momenta);

    for (auto h : Amp2q4gStatic::HSarr) {
        for (int i { 0 }; i < 6; ++i) {
            std::cout << (h[i] == 1 ? '+' : '-');
        }
        std::cout << ": ";

        Eps3<T> virt { amp.virt(h) };

        std::cout << std::setw(W) << "hA10: " << virt << '\n';
    }
}

    template <typename T>
void hel2q2g2a(const std::vector<MOM<T>>& momenta)
{
    // Amp2q2gAA<T> amp(Au);
    // amp.setNf(Nf);
    // amp.setNc(Nc);
    // amp.setMuR2(mur2);
    // amp.setMomenta(momenta);

    Amp2q2gAAx<T> ampx(Au);
    ampx.setNf(Nf);
    ampx.setNc(Nc);
    ampx.setMuR2(mur2);
    ampx.setMomenta(momenta);
    //ampx.setLoopInduced(true);

    // Amp2q2gAAx<T> ampxx(Ax);
    // ampxx.setNf(Nf);
    // ampxx.setNc(Nc);
    // ampxx.setMuR2(mur2);
    // ampxx.setMomenta(momenta);

    // Eps3<T> virt { amp.virt() };
    Eps5<T> virtx { ampx.virtsq() };
    //Eps3<T> virtxx { ampxx.virt() };

    // std::cout << std::setw(W) << "A10: " << virt << '\n';
    std::cout << std::setw(W) << "  A10x: " << virtx << '\n';
    //std::cout << std::setw(W) << " A10xx: " << virtxx << '\n';

    //for (auto h : Amp2q2gAAStatic::HSarr) {
    //    for (int i { 0 }; i < 6; ++i) {
    //        std::cout << (h[i] == 1 ? '+' : '-');
    //    }
    //    std::cout << "\n";

    //    // amp.setHelicity(h);
    //    ampx.setHelicity(h);
    //    // ampxx.setHelicity(h);

    //    // Eps3<T> pQ { amp.AF(0, 1, 2, 3).loop };
    //    // std::cout << std::setw(W) << "pQ: " << pQ << '\n';

    //    // // photons are coupled to external quarks
    //    // Eps3<T> pM { amp.AL(0, 1, 2, 3).loop };
    //    // std::cout << std::setw(W) << "pM: " << pM << '\n';

    //    // one photon coupled to the quark loop, one photon coupled to external quark
    //    ampx.fvSet(0);
    //    Eps3<T> pQx { ampx.AFx(0, 1, 2, 3).loop };
    //    //Eps3<T> pQx { ampxx.AFx(0, 1, 2, 3).loop };
    //    std::cout << std::setw(W) << "pQel: " << pQx << '\n';

    //    // photons are coupled to the quark loop
    //    ampx.fvSet(1);
    //    Eps3<T> pQxx { ampx.AFxx(0, 1, 2, 3).loop };
    //    // Eps3<T> pQxx { ampxx.AFxx(0, 1, 2, 3).loop };
    //    std::cout << std::setw(W) << "pQll: " << pQxx << '\n';

    //    return;

    //    // Eps3<T> virt { amp.virt(h) };

    //    // std::cout << std::setw(W) << "hA10: " << virt << '\n';
    //}
}

    template <typename T>
void run()
{
    std::cout << std::scientific << std::setprecision(16)
        << '\n'
        << "Notation:" << '\n'
        << "  pQ:   quark-loop partial amplitude" << '\n'
        << "  pQee: quark-loop partial amplitude with photons coupled to external quarks" << '\n'
        << "  pQel: quark-loop partial amplitude with one photon coupled to external quark and one to loop" << '\n'
        << "  pQll: quark-loop partial amplitude with photons coupled to the loop" << '\n'
        << "  pG:   gluon-loop partial amplitude" << '\n'
        << "  pM:   mixed-parton-loop partial amplitude" << '\n'
        << "  hA10: helicity amplitude squared: one-loop x tree (colour sum)" << '\n'
        << "  hA11: helicity amplitude squared: one-loop x one-loop (colour sum)" << '\n'
        << "  A10:  amplitude squared: one-loop x tree (helicity sum)" << '\n'
        << "  A11:  amplitude squared: one-loop x one-loop (helicity sum)" << '\n';

    const std::vector<double> scales2 { { 0 } };
    const int rseed { 1 };

    PhaseSpace<T> ps5(5, rseed);
    std::vector<MOM<T>> momenta5 { ps5.getPSpoint() };
    refineM(momenta5, momenta5, scales2);

    PhaseSpace<T> ps6(6, rseed);
    std::vector<MOM<T>> momenta6 { ps6.getPSpoint() };
    refineM(momenta6, momenta6, scales2);

    //std::cout << '\n'
    //          << "Five-point" << '\n';

    //for (int i { 0 }; i < 5; ++i) {
    //    std::cout << "p" << i << "=" << momenta5[i] << '\n';
    //}

    //std::cout << '\n'
    //          << "3g2a (pentagon functions)" << '\n';

    //hel3g2a_p<T, T>(momenta5);

    //std::cout << '\n'
    //          << "3g2a" << '\n';

    //hel3g2a<T>(momenta5);

    //std::cout << '\n'
    //          << "5g" << '\n';

    //hel5g<T>(momenta5);

    //std::cout << '\n'
    //          << "2q3g" << '\n';

    //hel2q3g<T>(momenta5);

    //std::cout << '\n'
    //          << "2q1g2a" << '\n';

    //hel2q1g2a<T>(momenta5);

    //std::cout << '\n'
    //          << "Six-point" << '\n';

    //for (int i { 0 }; i < 6; ++i) {
    //    std::cout << "p" << i << "=" << momenta6[i] << '\n';
    //}

    std::cout << '\n'
        << "2q2g2a" << '\n';

    hel2q2g2a<T>(momenta6);

    //std::cout << '\n'
    //          << "4g2a" << '\n';

    //hel4g2a<T>(momenta6);

    //std::cout << '\n'
    //          << "6g" << '\n';

    //hel6g<T>(momenta6);

    //std::cout << '\n'
    //          << "2q4g" << '\n';

    //hel2q4g<T>(momenta6);

    std::cout << '\n';
}

int main()
{
    run<double>();
}
