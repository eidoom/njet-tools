---
title: NJet tools
author: Ryan Moodie
date: 4 Dec 2019
---

# Environment configuration

* On my laptop, using `zsh` configured at `~/.zshrc`
    * Local packages installed to `$LOCAL/PACKAGE` where `LOCAL=$HOME/local`
* On my IPPP system, using `bash` configured at `~/.profile` with `source $HOME/.profile`  in `~/.bashrc`
    * `~/.profile` contains:
    ```shell
    export LOCAL=$HOME/local
    export PATH=$LOCAL/bin:$PATH
    export LD_LIBRARY_PATH=$LOCAL/lib:$LD_LIBRARY_PATH
    ```
    * Local packages installed to `$LOCAL`
* On both, `GIT=$HOME/git`
* README pdfs generated from md with `pandoc` using `tex`
    * `sudo dnf install pandoc texlive-collection-basic texlive-fvextra texlive-collection-fontsrecommended texlive-ulem`

# Installing njet-develop

* On my laptop

    ```shell
    sudo dnf install autoconf automake libtool gcc gcc-c++ gcc-gfortran make
    cd ~/git
    git clone --recursive git@bitbucket.org:njet/njet-develop.git
    cd njet-develop
    autoreconf -fi
    mkdir -p ~/scratch/njet-build
    cd ~/scratch/njet-build
    echo "export NJET_LOCAL=$LOCAL/njet" >> ~/.zshrc
    exec $SHELL
    ~/git/njet-develop/configure --prefix=$NJET_LOCAL --with-oneloop1 --enable-f256
    make -j # fails
    cd lib-oneloop1
    make -j
    cd ..
    make check # optional
    make -j install
    echo "export PATH=$NJET_LOCAL/bin:$PATH" >> ~/.zshrc
    echo "export LD_LIBRARY_PATH=$NJET_LOCAL/lib:$LD_LIBRARY_PATH" >> ~/.zshrc
    exec $SHELL
    ```

* On my IPPP system (with `LOCAL=$HOME/local` and otherwise as above [so `NJET_LOCAL=LOCAL` and `VC_LOCAL=LOCAL`])

    ```shell
    cd ~/git
    git clone --recursive git@bitbucket.org:njet/njet-develop.git
    cd njet-develop
    autoreconf -fi
    mkdir -p ~/build/njet
    cd ~/build/njet  
    ~/git/njet-develop/configure --prefix=$NJET_LOCAL --with-oneloop1 --enable-f256
    make -j
    make install
    ```

    If there are issues with the `make`,
    ```shell
    cd lib-oneloop1
    make -j
    cd ..
    ```
    then try again.

* On Torino servers running Rocky Linux 8, which uses `gcc8` by default, we can make use of `gcc-toolset-N` (N=9,10,11,12; install with same name and `scl-utils`) to select the version of `gcc` to use.

    ```shell
    scl enable gcc-toolset-N bash
    ```

* Makefiles then need

    ```shell
    CPPFLAGS+= -I$(NJET_LOCAL)/include -I$(GIT)/njet-develop
    LDFLAGS+= -L$(NJET_LOCAL)/lib
    COMMON+= -DUSE_DD -DUSE_QD
    LIBS+= -lnjet2 -lnjet2tools -lqd
    ```

# Installing [libVc](https://github.com/VcDevel/Vc)

* Install on my laptop (with `VC_LOCAL=$LOCAL/Vc`)

    ```shell
    sudo dnf install cmake
    cd ~/git
    git clone --recursive git@github.com:VcDevel/Vc.git
    mkdir -p ~/scratch/Vc
    cd ~/scratch/Vc
    cmake -DCMAKE_INSTALL_PREFIX=$VC_LOCAL -DBUILD_TESTING=OFF ~/git/Vc
    make -j
    make install
    echo "export LD_LIBRARY_PATH=$VC_LOCAL/lib:$LD_LIBRARY_PATH" >> ~/.zshrc
    exec $SHELL
    ```

    then include in Makefile

    ```shell
    CPPFLAGS+= -I$(VC_LOCAL)/include
    LDFLAGS+= -L$(VC_LOCAL)/lib
    COMMON+= -DUSE_VC
    LIBS+= -lVc
    ```

* Warning this doesn't work!
    * Vc 1.4 breaks when used with GCC 9.2.1 
        * e.g. standard library class (complex) and functions (abs) involve comparisons, which Vc types break
    * NJet compile seems to need `CXXFLAGS+='-DUSE_VC -I$(VC_LOCAL)/include'`
    * `Vc_1.4` branch is for fixing this (not done)

* On the IPPP system, need newer cmake (with `VC_LOCAL=$LOCAL`)

    ```shell
    mkdir ~/programs
    cd ~/programs
    wget https://github.com/Kitware/CMake/releases/download/v3.16.1/cmake-3.16.1-Linux-x86_64.tar.gz
    tar xvzf cmake-3.16.1-Linux-x86_64.tar.gz
    echo "alias cmake=~/programs/cmake-3.16.1-Linux-x86_64/bin/cmake" >> ~/.profile
    ```
    then proceed as above.

# Updating BLHA interface

* On `zsh`

    ```shell
    cd ~/git/njet-develop
    ./blha/njet.py -d >! chsums/NJetChannels.h
    ```

* On `bash`

    ```shell
    cd ~/git/njet-develop
    ./blha/njet.py -d > chsums/NJetChannels.h
    ```

# Debugging files in njet-develop

```shell
libtool --mode=execute gdb --args ./path/to/compiled/file
(gdb) run
(gdb) bt
```

# Updating NJet public and packaging a release

```shell
git clone --recurse-submodules git@bitbucket.org:njet/njet.git
cd njet
git remote add upstream git@bitbucket.org:njet/njet-develop.git
git checkout njet3
git fetch upstream master
git merge upstream/master
# resolve merge conflicts
# set version in configure.ac
# update NEWS.rst
git commit -am "..."
git push
autoreconf -i
mkdir build
cd build
../configure
make dist
```

To make the `1L` version, just
```shell
tar xf njet-VERSION.tar.gz
mv njet-VERSION njet-VERSION-1L
cd njet-VERSION-1L/finrem
find . \( -name "*.h" -o -name "*.cpp" -o -name "*.ipp" -o -name "*.am" \) -type f -delete
find . -name Makefile.in -exec sh -c 'echo "" > {}' \;
cd ../..
tar caf njet-VERSION-1L.tar.gz njet-VERSION-1L
```

# Installing FiniteFlow

https://github.com/peraro/finiteflow

Append to `~/.zshrc-private`
```vim

## FiniteFlow
export FF_LOCAL=$LOCAL/finiteflow
```
and `source`.

CMake is not correctly configured so we have to build in the source directory :(
```shell
sudo dnf install libuuid-devel flint-devel gmp-devel cmake
cd ~/git
git clone git@github.com:peraro/finiteflow.git
cd finiteflow
cmake -DCMAKE_INSTALL_PREFIX=$FF_LOCAL .
make tests
make -j
make install
```

Append to `~/.Mathematica/Kernel/init.m`
```vim

$FiniteFlowPath = "/home/ryan/git/finiteflow/mathlink"
If[Not[MemberQ[$Path,$FiniteFlowPath]],$Path = Flatten[{$Path, $FiniteFlowPath }]];
```

## On IPPP server

```shell
. ~/env-cpp
cd ~/git
git clone https://github.com/peraro/flint-finiteflow-dep.git
cd flint-finiteflow-dep
cmake -DCMAKE_INSTALL_PREFIX=/mt/home/rmoodie/local/flint .
make install
cd ~/git
git clone git@github.com:peraro/finiteflow.git
cd finiteflow
cmake -DCMAKE_INSTALL_PREFIX=$LOCAL/finiteflow -DCMAKE_PREFIX_PATH=$LOCAL/flint .
make -j
make install
echo "export LD_LIBRARY_PATH=\$LOCAL/finiteflow/lib:\$LD_LIBRARY_PATH" >> ~/env-ff
. ~/env-ff
```

# OneLOop

```shell
git clone git@bitbucket.org:hameren/oneloop.git
cd oneloop/
vi Config
?
```

# QCDLoop

https://qcdloop.web.cern.ch/qcdloop/

https://github.com/scarrazza/qcdloop

```shell
git clone https://github.com/scarrazza/qcdloop.git
cd qcdloop
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$QCDLOOP_LOCAL -DENABLE_EXAMPLES=ON
make -j
make install
```

# QD

See in `pentagon`.

# FORM

See [form-example](https://gitlab.com/eidoom/form-example).

# Mathematica

Download from [Wolfram User Portal](https://user.wolfram.com/portal).
Install with
```shell
sudo bash ~/Downloads/Mathematica_<version>_LINUX.sh
```
Takes a while...

# qgraf

[Download](https://cfif.ist.utl.pt/~paulo/d.html) (user `anonymous`, blank password)
```shell
mkdir ~/build/qgraf
cd ~/build/qgraf
mv ~/Downloads/qgraf-<version>.tgz .
tar xf qgraf-<version>.tgz
gfortran qgraf-<version>.f08 -o qgraf
mkdir ~/local/qgraf/bin
cp qgraf ~/local/qgraf/bin
```

# LiteRed

<https://www.inp.nsk.su/~lee/programs/LiteRed/>

```shell
mkdir -p ~/tar/litered
mkdir ~/Applications
cd ~/tar/litered
wget https://www.inp.nsk.su/~lee/programs/LiteRed/LiteRedV1/LiteRedV1.83.zip
unzip LiteRedV1.83.zip
cp -r Setup/* ~/Applications
vim /home/ryan/.Mathematica/Kernel/init.m
```
and add
```vim
$appsPath = "/home/ryan/Applications/"
If[Not[MemberQ[$Path,$appsPath]],$Path = Flatten[{$Path, $appsPath }]];
```

# finiteflow-mathtools

https://github.com/peraro/finiteflow-mathtools

```shell
cd ~/git
git clone git@github.com:peraro/finiteflow-mathtools.git
vim /home/ryan/.Mathematica/Kernel/init.m
```
and add
```vim
$FFMathTools = "/home/ryan/git/finiteflow-mathtools/packages"
If[Not[MemberQ[$Path,$FFMathTools]],$Path = Flatten[{$Path, $FFMathTools }]];
```

# Eigen

See [./cpp-tests/eigen-test/README.md](./cpp-tests/eigen-test/README.md).

# LHAPDF

https://lhapdf.hepforge.org/

https://gitlab.com/hepcedar/lhapdf/-/issues/13

```shell
sudo dnf install python3.9 python-devel
python3.9 -m venv ~/venv/py3.9
source ~/venv/py3.9/bin/activate
VERSION=6.5.1
mkdir -p ~/tar/LHAPDF
cd ~/tar/LHAPDF
wget https://lhapdf.hepforge.org/downloads/?f=LHAPDF-$VERSION.tar.gz -O LHAPDF-$VERSION.tar.gz
tar xf LHAPDF-$VERSION.tar.gz
cd LHAPDF-$VERSION/
./configure --prefix=$LOCAL/lhapdf
export PYTHONPATH=$PYTHONPATH:$LOCAL/lhapdf/lib/python3.9/site-packages
make -j`nproc`
make install
```
Add to rc:
```vim
export PATH=$PATH:$LOCAL/lhapdf/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LOCAL/lhapdf/lib
export PYTHONPATH=$PYTHONPATH:$LOCAL/lhapdf/lib64/python3.9/site-packages
export LHAPDF_DATA_PATH=$LOCAL/lhapdf/share/LHAPDF/
```
Add PDF sets
```shell
# wget https://lhapdfsets.web.cern.ch/lhapdfsets/current/NNPDF31_nlo_as_0118.tar.gz -O- | tar xz -C $LHAPDF_DATA_PATH
lhapdf update
lhapdf install NNPDF31_nlo_as_0118
```

# myfiniteflowexamples

https://gitlab.com/sbadger/myfiniteflowexamples

```shell
git clone git@gitlab.com:sbadger/myfiniteflowexamples.git
```

Append to `~/.Mathematica/Kernel/init.m`
```vim
$ffegPath = "/home/ryan/git/myfiniteflowexamples/"
If[Not[MemberQ[$Path,$ffegPath]],$Path = Flatten[{$Path, $ffegPath }]];
```

In repo root, create `InitFORMpaths.m`
```vim
$FORMpath = "/home/ryan/local/form/bin/"
$FORMinclude = "/home/ryan/git/myfiniteflowexamples/form/"
$EDITpath = "/home/ryan/git/myfiniteflowexamples/bin/"
```

# GiNaC

[GiNaC](https://www.ginac.de/)

Fedora:
```shell
sudo dnf install ginac
```

Rocky Linux 8:
```shell
dnf config-manager --set-enabled powertools
dnf install epel-release
dnf install cln-devel readline-devel
dnf install doxygen xfig texlive texinfo  # for docs
wget https://www.ginac.de/ginac-1.8.6.tar.bz2
tar xf ginac-1.8.6.tar.bz2
cd ginac-1.8.6
mkdir build
cd build
../configure
make -j
make -j check
make install
```
Then to use with `pkg-config`,
```shell
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig/"
```

# Singular

[Singular](https://github.com/Singular/Singular) [src](https://github.com/Singular/Singular)

## Fedora

```shell
sudo dnf install Singular
```

## Rocky Linux 8

First install Flint.

### Binaries

```shell
wget ftp://jim.mathematik.uni-kl.de/pub/Math/Singular/UNIX/Singular-4.3.1-x86_64-Linux.tar.gz
tar xf Singular-4.3.1-x86_64-Linux.tar.gz -C /usr/local/
```

### Source tar

Untested
```shell
dnf install gcc gcc-c++ autoconf autogen libtool readline-devel gmp-devel mpfr-devel graphviz
wget ftp://jim.mathematik.uni-kl.de/pub/Math/Singular/SOURCES/4-3-2/singular-4.3.2.tar.gz
tar xf singular-4.3.2.tar.gz
cd singular-4.3.2
cd build
mkdir build
make -j
make install
```

# OpenLoops

See also `sherpa/4l+j-OL/README.md`.

Fedora
```shell
sudo dnf install gcc gcc-c++ gcc-gfortran
wget https://openloops.hepforge.org/downloads?f=OpenLoops-2.1.2.tar.gz
tar xf downloads\?f\=OpenLoops-2.1.2.tar.gz
cd OpenLoops-2.1.2
./scons gjobs=`nproc`
./openloops libinstall eevjj
```
