#!/usr/bin/env python3

import subprocess, multiprocessing


if __name__ == "__main__":
    with open("run.log", "w") as f:
        meta = subprocess.run(["./script.bash"], stdout=f)
    meta.check_returncode()

    meta = subprocess.run(
        ["./fail.bash"], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT
    )
    meta.check_returncode()
