PROGRAM test_spread
  INTEGER :: a = 1, b(2) = (/ 1, 2 /), c(4) = (/ 1, 2, 3, 4 /)
  WRITE(*,*) SPREAD(A, 1, 2)
  WRITE(*,*)
  WRITE(*,*) SPREAD(B, 1, 2)
  WRITE(*,*)
  WRITE(*,*) SPREAD(C, 2, 20)
  WRITE(*,*)
  WRITE(*,*) SPREAD(SPREAD(C, 2, 4), 3, 5)
END PROGRAM
