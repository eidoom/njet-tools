# 3g2a@2l

```shell
math -script proc/proc.wl
math -script proc/proc_nf2.wl
math -script proc/specFns.wl
./proc/conv-spec-fns.sh build/specFns.m # then copy appropriate parts of build/specFns.cpp to njet-develop/finrem/common/pentagons.* (also for specFns1L.m)
./proc/loop.sh # edit path for ippp
cd build
make -j`nproc`
cd ..
./proc/split-sm.sh # do header entries for split sparse matrix filling methods manually (obsolete?)
./proc/makefile.py # copy over contents of Makefile.am.part manually
./proc/init.sh
./proc/init-main.sh
./proc/hel_headers.py
#./proc/resize_coeffs.py concat/**/src_hel.cpp # obsolete
./proc/fin.sh
l export/* | parallel -j`nproc` clang-format -i {}
rsync -Pavh export/ ~/git/njet-develop/finrem/0q3g2A
```
