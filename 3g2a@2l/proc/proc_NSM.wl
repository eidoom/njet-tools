(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/3g2a@2l"];
in="src/";
outpre="build/";
allHels={
"+++++",
"++++-",
"+++-+",
"+++--",
"++-++",
"++-+-",
"++--+",
"+-+++",
"+-++-",
"+-+-+",
"+--++",
"-++++",
"-+++-",
"-++-+",
"-+-++",
"--+++"};
PF=1;


finsOne=Get[in<>"partials_3g2a_1L_tHV_"<>#<>".m"]&/@allHels;


finsTwoLC=Get[in<>"partials_3g2a_2L_Ncp1_tHV_"<>#<>".m"]&/@allHels;
finsTwoSLC=Get[in<>"partials_3g2a_2L_Ncpm1_tHV_"<>#<>".m"]&/@allHels;


nsm[src_,ord_,par_]:=Module[{sm1,e1,e0,v1,u1,r1,p1,n1,ncols,nrows,nnz},
sm1=src[[3,ord,par]];
e1=sm1[[2]]/.{m[i_,j_]:>m[j,i]};
e0=e1/.{m[i_,j_]:>m[i-1,j-1]};
v1=e0/.Rule[a_,b_]->b;
u1=DeleteDuplicates[Abs/@v1];
r1=w[#-1]->u1[[#]]&/@Range[Length[u1]];
p1=r1/.Rule[a_,b_]->Rule[b,a];
p1=Join[p1,p1/.Rule[a_,b_]->Rule[-a,-b]];
n1=e0/.Rule[a_,b_]:>Rule[a,(b/.p1)];
ncols=sm1[[1,1]];
nrows=sm1[[1,2]];
nnz=Table[Length[Select[e1,#[[1,2]]==i&]],{i,ncols}];
NSM[{nrows, ncols},nnz,r1,n1]
]


export[fins_,hels_,out_,odd_] := ParallelDo[
    fin=fins[[i]];
    hel="h"<>hels[[i]];
    o=outpre<>out<>"/";
    Export[o<>hel<>"/e"<>ToString[#]<>"alt.m",nsm[fin,#,1]]&/@Range[PF];
    If[odd,Export[o<>hel<>"/o"<>ToString[#]<>"alt.m",nsm[fin,#,2]]&/@Range[PF]];
    ,{i,Length[hels]}
];


export[finsOne, allHels, "one", False];


export[finsTwoLC, allHels, "twoLC", True];
export[finsTwoSLC, allHels, "twoSLC", True];
