#!/usr/bin/env python3

import sys
import re

pattern = re.compile(
    r"(mf1\((\d+), \d+\).*?m2LC1e\((\d+), \d+\).*?m2LC1o\((\d+), \d+\).*?m2SLC1e\((\d+), \d+\).*?m2SLC1o\((\d+), \d+\).*?{)(\s*m)",
    flags=re.DOTALL
)

for filename in sys.argv[1:]:
    print(f"Searching {filename}...")
    with open(filename, "r") as f:
        contents = f.read()
        match = pattern.search(contents)
    if match:
        print("    Substituting...")
        with open(filename, "w") as f:
            new = pattern.sub(
                r"""\1

  c_f[0].resize(\2);
  c_2lce[0].resize(\3);
  c_2lco[0].resize(\4);
  c_2slce[0].resize(\5);
  c_2slco[0].resize(\6);\7""",
                contents.rstrip(),
            )
            f.write(new)
    else:
        print("    No match.")
