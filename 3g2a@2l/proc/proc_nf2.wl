(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/3g2a@2l"];
in="src/";
outpre="build/";
allHels={
"+++++",
"++++-",
"+++-+",
"+++--",
"++-++",
"++-+-",
"++--+",
"+-+++",
"+-++-",
"+-+-+",
"+--++",
"-++++",
"-+++-",
"-++-+",
"-+-++",
"--+++"};
PF=1;


sm[fin_,ord_,par_]:=Module[
	{a=fin[[3,ord,par]],entries1,entries0,ncols,nrows,nnz},
	entries1=a[[2]]/.{m[i_,j_]:>m[j,i]};
	entries0=entries1/.{m[i_,j_]:>m[i-1,j-1]};
	ncols=a[[1,1]];
	nrows=a[[1,2]];
	nnz=Table[Length[Select[entries1,#[[1,2]]==i&]],{i,ncols}];
	SM[{nrows, ncols},nnz,entries0]
];
fv[fin_]:=Module[
	{a=First[#]-1&/@#&/@fin[[1]]},
	fi[#]->With[{b=a[[#]]},V[Length[b],b]]&/@Range[Length[a]]
];
fms[fin_]:=Sort[fin[[5]]]/.{(fn:(f|y))[i_]:>fn[i-1]};
yms[fin_]:=Sort[fin[[6]]]/.{y[i_]:>y[i-1]};
Fv[fin_]:=Module[
	{a=#-1&/@fin[[2]]},
	Fi[#]->With[{b=a[[#]]},V[Length[b],b]]&/@Range[Length[a]]
];


export[fins_,hels_,out_,odd_] := ParallelDo[
    fin=fins[[i]];
    hel="h"<>hels[[i]];
    o=outpre<>out<>"/";
    Export[o<>hel<>"/Fv.m",Fv[fin]];
    Export[o<>hel<>"/fv.m",fv[fin]];
    fmsi=fms[fin];
    Export[o<>hel<>"/fm.m",fmsi];
    Export[o<>hel<>"/f_size.txt", Length[fmsi]];
    ymsi=yms[fin];
    Export[o<>hel<>"/ym.m",ymsi];
    Export[o<>hel<>"/y_size.txt", Length[ymsi]];
    Export[o<>hel<>"/e"<>ToString[#]<>".m",sm[fin,#,1]]&/@Range[PF];
    If[odd,Export[o<>hel<>"/o"<>ToString[#]<>".m",sm[fin,#,2]]&/@Range[PF]];
    ,{i,Length[hels]}
];


fins=Get[in<>"partials_3g2a_2L_Ncp0_nf_tHV_"<>#<>".m"]&/@allHels;


export[fins, allHels, "nf", False];
