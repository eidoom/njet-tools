#!/usr/bin/env bash

ROOT=/home/ryan/git/njet-tools/3g2a@2l
cd $ROOT
MAKE=build/Makefile
PF=1

echo -e "# Automatically generated Makefile from $0 #\n\n.PHONY: all one twoLC twoSLC twoF two sms1 yms1 fms1 sms2lc yms2lc fms2lc sms2slc yms2slc fms2slc sms2f yms2f fms2f prepFORM\nall: Fm.cpp Fm1L.cpp one two\none: sms1 yms1 fms1\ntwoLC: sms2lc yms2lc fms2lc\ntwoSLC: sms2slc yms2slc fms2slc\ntwoF: sms2f yms2f fms2f\ntwo: twoLC twoSLC twoF\n" >$MAKE

declare -a prepFORM

declare -a sms1
declare -a yms1
declare -a fms1

for dir in build/one/h*; do
    dir=${dir#build/}
    ps=('e')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            sms1+=("$dir/${p}${o}.cpp")
            echo -e "
$dir/${p}${o}.cpp: $dir/${p}${o}.m
\t../proc/conv-sm.sh $< f false" \
                >>$MAKE
        done
    done

    echo -e "
$dir/ym.frm: $dir/ym.m
\t../proc/conv-y.sh $<" \
        >>$MAKE

    echo -e "
$dir/fm.frm: $dir/fm.m
\t../proc/conv-f.sh $<" \
        >>$MAKE

    echo -e "
$dir/ym.c: $dir/ym.frm
\ttform $< >${dir}/ym.log 2>&1" \
        >>$MAKE

    echo -e "
$dir/fm.c: $dir/fm.frm
\ttform $< >${dir}/fm.log 2>&1" \
        >>$MAKE

    echo -e "
$dir/ym.cpp: $dir/ym.c
\t../proc/pp-form.sh $<" \
        >>$MAKE

    echo -e "
$dir/fm.cpp: $dir/fm.c
\t../proc/pp-form.sh $<" \
        >>$MAKE

    yms1+=("$dir/ym.cpp")
    fms1+=("$dir/fm.cpp")
    prepFORM+=("$dir/ym.frm" "$dir/fm.frm")
done

declare -a sms2lc
declare -a yms2lc
declare -a fms2lc

for dir in build/twoLC/h*; do
    dir=${dir#build/}
    ps=('e' 'o')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            sms2lc+=("$dir/${p}${o}.cpp")
            echo -e "
$dir/${p}${o}.cpp: $dir/${p}${o}.m
\t../proc/conv-sm.sh $< 2LC true" \
                >>$MAKE
        done
    done

    echo -e "
$dir/ym.frm: $dir/ym.m
\t../proc/conv-y.sh $<" \
        >>$MAKE

    echo -e "
$dir/fm.frm: $dir/fm.m
\t../proc/conv-f.sh $<" \
        >>$MAKE

    echo -e "
$dir/ym.c: $dir/ym.frm
\ttform $< >${dir}/ym.log 2>&1" \
        >>$MAKE

    echo -e "
$dir/fm.c: $dir/fm.frm
\ttform $< >${dir}/fm.log 2>&1" \
        >>$MAKE

    echo -e "
$dir/ym.cpp: $dir/ym.c
\t../proc/pp-form.sh $<" \
        >>$MAKE

    echo -e "
$dir/fm.cpp: $dir/fm.c
\t../proc/pp-form.sh $<" \
        >>$MAKE

    yms2lc+=("$dir/ym.cpp")
    fms2lc+=("$dir/fm.cpp")
    prepFORM+=("$dir/ym.frm" "$dir/fm.frm")
done

declare -a sms2slc
declare -a yms2slc
declare -a fms2slc

for dir in build/twoSLC/h*; do
    dir=${dir#build/}
    ps=('e' 'o')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            sms2slc+=("$dir/${p}${o}.cpp")
            echo -e "
$dir/${p}${o}.cpp: $dir/${p}${o}.m
\t../proc/conv-sm.sh $< 2SLC true" \
                >>$MAKE
        done
    done

    echo -e "
$dir/ym.frm: $dir/ym.m
\t../proc/conv-y.sh $<" \
        >>$MAKE

    echo -e "
$dir/fm.frm: $dir/fm.m
\t../proc/conv-f.sh $<" \
        >>$MAKE

    echo -e "
$dir/ym.c: $dir/ym.frm
\ttform $< >${dir}/ym.log 2>&1" \
        >>$MAKE

    echo -e "
$dir/fm.c: $dir/fm.frm
\ttform $< >${dir}/fm.log 2>&1" \
        >>$MAKE

    echo -e "
$dir/ym.cpp: $dir/ym.c
\t../proc/pp-form.sh $<" \
        >>$MAKE

    echo -e "
$dir/fm.cpp: $dir/fm.c
\t../proc/pp-form.sh $<" \
        >>$MAKE

    yms2slc+=("$dir/ym.cpp")
    fms2slc+=("$dir/fm.cpp")
    prepFORM+=("$dir/ym.frm" "$dir/fm.frm")
done

declare -a sms2f
declare -a yms2f
declare -a fms2f

for dir in build/nf/h*; do
    dir=${dir#build/}
    ps=('e')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            sms2f+=("$dir/${p}${o}.cpp")
            echo -e "
$dir/${p}${o}.cpp: $dir/${p}${o}.m
\t../proc/conv-sm.sh $< 2F false" \
                >>$MAKE
        done
    done

    echo -e "
$dir/ym.frm: $dir/ym.m
\t../proc/conv-y.sh $<" \
        >>$MAKE

    echo -e "
$dir/fm.frm: $dir/fm.m
\t../proc/conv-f.sh $<" \
        >>$MAKE

    echo -e "
$dir/ym.c: $dir/ym.frm
\ttform $< >${dir}/ym.log 2>&1" \
        >>$MAKE

    echo -e "
$dir/fm.c: $dir/fm.frm
\ttform $< >${dir}/fm.log 2>&1" \
        >>$MAKE

    echo -e "
$dir/ym.cpp: $dir/ym.c
\t../proc/pp-form.sh $<" \
        >>$MAKE

    echo -e "
$dir/fm.cpp: $dir/fm.c
\t../proc/pp-form.sh $<" \
        >>$MAKE

    yms2f+=("$dir/ym.cpp")
    fms2f+=("$dir/fm.cpp")
    prepFORM+=("$dir/ym.frm" "$dir/fm.frm")
done

echo -e "
Fm.frm: Fm.m
\t../proc/conv-F.sh $<" \
    >>$MAKE

echo -e "
Fm.c: Fm.frm
\ttform $< >Fm.log 2>&1" \
    >>$MAKE

echo -e "
Fm.cpp: Fm.c
\t../proc/pp-form.sh $<" \
    >>$MAKE

echo -e "
Fm1L.frm: Fm1L.m
\t../proc/conv-F.sh $<" \
    >>$MAKE

echo -e "
Fm1L.c: Fm1L.frm
\ttform $< >Fm1L.log 2>&1" \
    >>$MAKE

echo -e "
Fm1L.cpp: Fm1L.c
\t../proc/pp-form.sh $<" \
    >>$MAKE

echo -e "\nsms1: ${sms1[@]}" >>$MAKE
echo -e "\nyms1: ${yms1[@]}" >>$MAKE
echo -e "\nfms1: ${fms1[@]}" >>$MAKE

echo -e "\nsms2lc: ${sms2lc[@]}" >>$MAKE
echo -e "\nyms2lc: ${yms2lc[@]}" >>$MAKE
echo -e "\nfms2lc: ${fms2lc[@]}" >>$MAKE

echo -e "\nsms2slc: ${sms2slc[@]}" >>$MAKE
echo -e "\nyms2slc: ${yms2slc[@]}" >>$MAKE
echo -e "\nfms2slc: ${fms2slc[@]}" >>$MAKE

echo -e "\nsms2f: ${sms2f[@]}" >>$MAKE
echo -e "\nyms2f: ${yms2f[@]}" >>$MAKE
echo -e "\nfms2f: ${fms2f[@]}" >>$MAKE

echo -e "\nprepFORM: Fm1L.frm Fm.frm sms1 sms2lc sms2slc sms2f ${prepFORM[@]}" >>$MAKE
