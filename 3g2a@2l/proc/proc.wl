(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/3g2a@2l"];
in="src/";
outpre="build/";
uhvs={
"+++++",
"++++-",
"+++-+",
"++-++",
"+-+++",
"-++++"
};
allHels={
"+++++",
"++++-",
"+++-+",
"+++--",
"++-++",
"++-+-",
"++--+",
"+-+++",
"+-++-",
"+-+-+",
"+--++",
"-++++",
"-+++-",
"-++-+",
"-+-++",
"--+++"};
mhvs={
"+++--",
"++-+-",
"++--+",
"+-++-",
"+-+-+",
"+--++",
"-+++-",
"-++-+",
"-+-++",
"--+++"
};
PF=1;


sm[fin_,ord_,par_]:=Module[
	{a=fin[[3,ord,par]],entries1,entries0,ncols,nrows,nnz},
	entries1=a[[2]]/.{m[i_,j_]:>m[j,i]};
	entries0=entries1/.{m[i_,j_]:>m[i-1,j-1]};
	ncols=a[[1,1]];
	nrows=a[[1,2]];
	nnz=Table[Length[Select[entries1,#[[1,2]]==i&]],{i,ncols}];
	SM[{nrows, ncols},nnz,entries0]
];
fv[fin_]:=Module[
	{a=First[#]-1&/@#&/@fin[[1]]},
	fi[#]->With[{b=a[[#]]},V[Length[b],b]]&/@Range[Length[a]]
];
fms[fin_]:=Sort[fin[[5]]]/.{(fn:(f|y))[i_]:>fn[i-1]};
yms[fin_]:=Sort[fin[[6]]]/.{y[i_]:>y[i-1]};
Fv[fin_]:=Module[
	{a=#-1&/@fin[[2]]},
	Fi[#]->With[{b=a[[#]]},V[Length[b],b]]&/@Range[Length[a]]
];
export[fins_,hels_,out_,odd_] := ParallelDo[
    fin=fins[[i]];
    hel="h"<>hels[[i]];
    Fvi=Fv[fin];
    fvi=fv[fin];
    fmsi=fms[fin];
    fmsl=Length[fmsi];
    ymsi=yms[fin];
    ymsl=Length[ymsi];
    o=outpre<>out<>"/";
    Export[o<>hel<>"/Fv.m",Fvi];
    Export[o<>hel<>"/fv.m",fvi];
    Export[o<>hel<>"/fm.m",fmsi];
    Export[o<>hel<>"/f_size.txt", fmsl];
    Export[o<>hel<>"/ym.m",ymsi];
    Export[o<>hel<>"/y_size.txt", ymsl];
    Export[o<>hel<>"/e"<>ToString[#]<>".m",sm[fin,#,1]]&/@Range[PF];
    If[odd,Export[o<>hel<>"/o"<>ToString[#]<>".m",sm[fin,#,2]]&/@Range[PF]];
    ,{i,Length[hels]}
];


Fmons=Get[in<>"AllPfuncMonomials.m"];


Fml=Length[Fmons];
Fms=SF[#-1]->Fmons[[#]]&/@Range[Fml];
Export[outpre<>"Fm.m",Fms];
Export[outpre<>"F_size.txt", Fml];


finsOne=Get[in<>"partials_3g2a_1L_tHV_"<>#<>".m"]&/@allHels;


finsTwoLC=Get[in<>"partials_3g2a_2L_Ncp1_tHV_"<>#<>".m"]&/@allHels;
finsTwoSLC=Get[in<>"partials_3g2a_2L_Ncpm1_tHV_"<>#<>".m"]&/@allHels;


export[finsOne, allHels, "one", False];
export[finsTwoLC, allHels, "twoLC", True];
export[finsTwoSLC, allHels, "twoSLC", True];
