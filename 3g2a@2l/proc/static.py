mhvs = (
    "pppmm",
    "ppmpm",
    "ppmmp",
    "pmppm",
    "pmpmp",
    "pmmpp",
    "mpppm",
    "mppmp",
    "mpmpp",
    "mmppp",
)

uhvs = (
    "ppppp",
    "ppppm",
    "pppmp",
    "ppmpp",
    "pmppp",
    "mpppp",
)

hels = uhvs + mhvs

def num2str(num):
    out = ["m"] * 5
    for i in range(4, -1, -1):
        cur = 2 ** i
        if num // cur:
            out[i] = "p"
            num %= cur
    return "".join(out)


def str2num(strg):
    num = 0
    for i in range(5):
        if list(strg)[i] == "p":
            num += 2 ** i
    return num

def start(filename, years):
    return f"""/*
 * finrem/{filename}
 *
 * This file is part of NJet library
 * Copyright (C) {years} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

"""

def hel_end(proc, hstr):
    return f"""

#ifdef USE_SD
template class Amp{proc}_a2v_{hstr.upper()}<double>;
#endif
#ifdef USE_DD
template class Amp{proc}_a2v_{hstr.upper()}<dd_real>;
#endif
#ifdef USE_QD
template class Amp{proc}_a2v_{hstr.upper()}<qd_real>;
#endif
#ifdef USE_VC
template class Amp{proc}_a2v_{hstr.upper()}<Vc::double_v>;
#endif
"""
