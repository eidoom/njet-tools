#!/usr/bin/env bash

ROOT=/home/ryan/git/njet-tools/3g2a@2l
cd $ROOT
PF=1

SRC1="build//one/"
SRC2LC="build//twoLC/"
SRC2SLC="build//twoSLC/"
SRC2F="build//nf/"
DES="concat/"

mkdir -p ${DES}

order=(
    01234
)

SRC_MAIN="${DES}/src_main.cpp"

echo >${SRC_MAIN}

hels() {
    h="${1#*//*//h}"

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    J=$j
    if (($j < 10)); then
        j="0$j"
    fi
}

echo -e "\n// }{ 1L\n// primary\n" >>${SRC_MAIN}

for s in ${SRC1}/h*; do
    hels $s

    F=$(cat ${s}/fm.count)

    perl -p -e "s|HH|$j|g; s|SSSSS|$h_symb|g; s|FFF|$F|g; s|LL|f|g; s|NNN|one|g; s|PPPPP|0q3g2A|g; s|SSS||g;" templates/template_raw.cpp >>${SRC_MAIN}
done

echo -e "// cached\n" >>${SRC_MAIN}

for s in ${SRC1}/h*; do
    hels $s

    F=$(cat ${s}/fm.count)

    perl -p -e "s|HH|$j|g; s|II|$J|g; s|SSSSS|$h_symb|g; s|FFF|$F|g; s|LL|f|g; s|NNN|one_loop|g; s|PPPPP|0q3g2A|g;" templates/template_cache.cpp >>${SRC_MAIN}
    echo >>${SRC_MAIN}
done

echo -e "// }{ 2L LC\n// primary\n" >>${SRC_MAIN}

for s in ${SRC2LC}/h*; do
    hels $s

    F=$(cat ${s}/fm.count)

    perl -p -e "s|HH|$j|g; s|SSSSS|$h_symb|g; s|FFF|$F|g; s|LL|2LC|g; s|NNN|two|g; s|PPPPP|0q3g2A|g; s|SSS|lc_|g;" templates/template_raw.cpp >>${SRC_MAIN}
done

echo -e "// cached\n" >>${SRC_MAIN}

for s in ${SRC2LC}/h*; do
    hels $s

    F=$(cat ${s}/fm.count)

    perl -p -e "s|HH|$j|g; s|II|$J|g; s|SSSSS|$h_symb|g; s|FFF|$F|g; s|LL|2LC|g; s|NNN|two_loop_LC|g; s|PPPPP|0q3g2A|g;" templates/template_cache.cpp >>${SRC_MAIN}
    echo >>${SRC_MAIN}
done

echo -e "// }{ 2L SLC\n// primary\n" >>${SRC_MAIN}

for s in ${SRC2SLC}/h*; do
    hels $s

    F=$(cat ${s}/fm.count)

    perl -p -e "s|HH|$j|g; s|SSSSS|$h_symb|g; s|FFF|$F|g; s|LL|2SLC|g; s|NNN|two|g; s|PPPPP|0q3g2A|g; s|SSS|slc_|g;" templates/template_raw.cpp >>${SRC_MAIN}
done

echo -e "// cached\n" >>${SRC_MAIN}

for s in ${SRC2SLC}/h*; do
    hels $s

    F=$(cat ${s}/fm.count)

    perl -p -e "s|HH|$j|g; s|II|$J|g; s|SSSSS|$h_symb|g; s|FFF|$F|g; s|LL|2SLC|g; s|NNN|two_loop_SLC|g; s|PPPPP|0q3g2A|g;" templates/template_cache.cpp >>${SRC_MAIN}
    echo >>${SRC_MAIN}
done

echo -e "// }{ 2L F\n// primary\n" >>${SRC_MAIN}

for s in ${SRC2F}/h*; do
    hels $s

    F=$(cat ${s}/fm.count)

    perl -p -e "s|HH|$j|g; s|SSSSS|$h_symb|g; s|FFF|$F|g; s|LL|2F|g; s|NNN|two|g; s|PPPPP|0q3g2A|g; s|SSS|f_|g;" templates/template_raw.cpp >>${SRC_MAIN}
done

echo -e "// cached\n" >>${SRC_MAIN}

for s in ${SRC2SLC}/h*; do
    hels $s

    F=$(cat ${s}/fm.count)

    perl -p -e "s|HH|$j|g; s|II|$J|g; s|SSSSS|$h_symb|g; s|FFF|$F|g; s|LL|2F|g; s|NNN|two_loop_f|g; s|PPPPP|0q3g2A|g;" templates/template_cache.cpp >>${SRC_MAIN}
    echo >>${SRC_MAIN}
done
