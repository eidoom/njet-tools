#!/usr/bin/env python3

import argparse, sys, os, sqlite3, pathlib

import numpy, matplotlib.pyplot, matplotlib.cm

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, "../test-stability/")
sys.path.insert(0, filename)

import reader
import plotter


def histo(
    data,
    names,
    cutoff=1e-3,
    label="",
    x_min_pow=None,
    x_max_pow=None,
    num_bins=100,
    leg_loc="best",
    xlog=True,
    ylog=True,
    w=6.4,
    r=3 / 4,
    legend=True,
    xlabel="",
    vector=False,
    lines=[],
    line=None,
):
    fig, ax = matplotlib.pyplot.subplots(figsize=(w, w * r))

    if xlog:
        matplotlib.pyplot.xscale("log")
        mini = (
            numpy.log10(numpy.min([numpy.min(x) for x in data]))
            if x_min_pow is None
            else x_min_pow
        )
        maxi = (
            numpy.log10(numpy.max([numpy.max(x) for x in data]))
            if x_max_pow is None
            else x_max_pow
        )
        bins = numpy.logspace(mini, maxi, num=num_bins, base=10)
    else:
        bins = num_bins

    colours = matplotlib.cm.tab10(range(len(data)))

    for datum, name, colour in zip(data, names, colours):
        ax.hist(
            datum,
            bins=bins,
            histtype="step",
            log=ylog,
            label=name,
            color=colour,
        )

    for dline, colour in zip(lines, colours):
        ax.axvline(dline, color=colour, linewidth=1)

    if line:
        ax.axvline(line, color="black", linewidth=2)

    if legend:
        ax.legend(loc=leg_loc)

    ax.set_xlabel(xlabel)
    ax.set_ylabel("Frequency")

    plotter.write(
        fig,
        f"histo{plotter.slug(label)}_{plotter.ll(xlog)}{plotter.ll(ylog)}_cut_{cutoff}",
        vector,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "analyse timing of 3g2A 2L run over NNLOJET 1L phase space"
    )
    parser.add_argument(
        "-s",
        "--physical-phase-space",
        action="store_true",
        help="use random samples from the NNLOJET 1L phase space rather than uniform",
    )
    parser.add_argument(
        "-p",
        "--plot",
        action="store_true",
        help="produce plots",
    )
    args = parser.parse_args()

    ps = "physical" if args.physical_phase_space else "uniform"

    folder = pathlib.Path("data")
    folder.mkdir(exist_ok=True)
    file = folder / f"points_{ps}.db"

    if file.is_file():
        print(f"Opening database {file}")
        con = sqlite3.connect(file)
        cur = con.cursor()

    else:
        dr = (
            "../collab/3g2a_phys_res"
            if args.physical_phase_space
            else "../test-stability/3g2a_res"
        )
        print("Using", dr)

        all_3g2a = reader.read_proc(dr, "result")

        con = sqlite3.connect(file)
        cur = con.cursor()

        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS point (
                id INTEGER PRIMARY KEY NOT NULL,
                psp INTEGER NOT NULL,
                prec INTEGER NOT NULL,
                tsf INTEGER NOT NULL,
                tc INTEGER NOT NULL
            ) STRICT
            """
        )

        for i, ps in enumerate(all_3g2a):
            for p in ps:
                cur.execute(
                    """
                    INSERT INTO point (psp, prec, tsf, tc)
                    VALUES (?, ?, ?, ?)
                    """,
                    (p[0], i, p[5], p[6]),
                )
        con.commit()

        print(f"Database {file} created")

    print("times: SF s, C s, (SF + C) s, SF / (SF + C)")

    print("average times for f64")
    res = cur.execute(
        """
        SELECT
            AVG(tsf) / 1e6,
            AVG(tc) / 1e6
        FROM point
        WHERE prec = 0
        """
    )
    (dsf, dc) = res.fetchone()
    print(dsf, dc, d := dsf + dc, dsf / d)

    print("average times for evaluation strategy")
    res = cur.execute(
        """
        SELECT
            AVG(ctsf) / 1e6,
            AVG(ctc) / 1e6
        FROM (
            SELECT
                SUM(tsf) AS ctsf,
                SUM(tc) AS ctc
            FROM point
            GROUP BY psp
        )
        """
    )
    (tsf, tc) = res.fetchone()
    print(tsf, tc, t := tsf + tc, tsf / t)

    if args.plot:
        plotter.init_plots()

        print("average cumulative times at each precision level")
        res = cur.execute(
            """
            SELECT AVG(t)
            FROM (
                SELECT
                    SUM(tsf + tc) / 1e6 AS t,
                    MAX(prec) AS mprec
                FROM point
                GROUP BY psp
            )
            GROUP BY mprec
            ORDER BY mprec
            """
        )
        ((dd,), (qd,), (qq,)) = res.fetchall()
        print(dd, qd, qq)

        times = []
        for prec in range(3):
            res = cur.execute(
                """
                SELECT t
                FROM (
                    SELECT
                        SUM(tsf + tc) / 1e6 AS t,
                        MAX(prec) AS mprec
                    FROM point
                    GROUP BY psp
                )
                WHERE mprec = ?
                """,
                (prec,),
            )
            times.append(numpy.array(res.fetchall())[:, 0])

        histo(
            times,
            names=(
                "DD",
                "QD",
                "QQ",
            ),
            xlabel="Time (s)",
            label=f"timing_{ps}",
            num_bins=50,
            lines=(dd, qd, qq),
            line=tsf + tc,
            vector=True,
        )
