#include <cstdlib>
#include <iostream>
#include <vector>

#include "analytic/0q3g2A-analytic.h"
#include "chsums/0q3gA.h"
#include "chsums/NJetAccuracy.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"

template <typename T>
void run(const int start, const int end)
{
    const int Nc { 3 };
    const int Nf { 5 };
    const double mur { 91.188 };
    const std::vector<double> scales2 { { 0 } };

    const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };

    NJetAccuracy<T>* const amp { NJetAccuracy<T>::template create<Amp0q3g2A_a2l<T, T>>(Ax) };
    amp->setNf(Nf);
    amp->setNc(Nc);
    amp->setMuR2(mur * mur);

    for (int p { start }; p < end + 1; ++p) {
        std::cout << "==================== Test point " << p
                  << " ====================" << '\n';

        std::vector<MOM<T>> momenta { njet_random_point<T>(p) };
        refineM(momenta, momenta, scales2);

        std::cout << "{" << '\n';
        for (std::size_t i { 0 }; i < momenta.size(); ++i) {
            std::cout << momenta[i] << (i == 4 ? "" : ",") << '\n';
        }
        std::cout << "}" << '\n';

        amp->setMomenta(momenta);
        amp->initFinRem();
        amp->setSpecFuncs();
        amp->c1lx1l_fin();
        amp->c2lx1l_fin();

        std::cout
            << "1lx1l val:     " << amp->c1lx1l_fin_value() << '\n'
            << "1lx1l abs err: " << amp->c1lx1l_fin_error() << '\n'
            << "1lx1l rel err: " << abs(amp->c1lx1l_fin_error() / amp->c1lx1l_fin_value()) << '\n'
            << "2lx1l val:     " << amp->c2lx1l_fin_value() << '\n'
            << "2lx1l abs err: " << amp->c2lx1l_fin_error() << '\n'
            << "2lx1l rel err: " << abs(amp->c2lx1l_fin_error() / amp->c2lx1l_fin_value()) << '\n'
            << "2lx1l/1lx1l:   " << amp->c2lx1l_fin_value() / amp->c1lx1l_fin_value() << '\n'
            << '\n';
        //  }
    }
}

int main(int argc, char *argv[]) {
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(32);
    std::cout << '\n';

  if (argc != 3) {
    std::cerr << "\nError: incorrect arguments!\n\n";
    std::exit(EXIT_FAILURE);
  }


    run<dd_real>(std::atoi(argv[1]), std::atoi(argv[2]));
}
