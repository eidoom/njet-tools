#include <algorithm>
#include <array>
#include <chrono>
#include <complex>
#include <cstddef>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

#include "analytic/0q3g2A-analytic.h"
#include "chsums/0q3gA.h"
#include "chsums/NJetAccuracy.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"

using std::abs;

template <typename T, typename P>
void run(const int start, const int end)
{
    std::cout << "H2/H1:" << '\n';

    const int num { end - start + 1 };
    const int legs { 5 };
    const int Nc { 3 };
    const int Nf { 5 };
    const std::vector<double> scales2 { { 0 } };
    const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };
    const PhysMom::Data<double> data("../collab/points_new.txt");

    Amp0q3g2A_a2l<T, P> amp(Ax);
    amp.setNf(Nf);
    amp.setNc(Nc);

    T H2i {};

    for (int p { start }; p <= end; ++p) {
        std::vector<MOM<T>> momenta(legs);
        for (int i { 0 }; i < legs; ++i) {
            momenta[i] = MOM<T>(data.points[p].momenta[i][0], data.points[p].momenta[i][1],
                data.points[p].momenta[i][2], data.points[p].momenta[i][3]);
        }
        refineM(momenta, momenta, scales2);

        amp.setMuR2(std::pow(data.points[p].mu, 2));

        amp.setMomenta(momenta);
        amp.initFinRem();
        amp.setSpecFuncs();

        T H2oH1 { amp.c2lx1l_fin() / amp.c1lx1l_fin() };

        std::cout << p << ' ' << H2oH1 << '\n';

        H2i += abs(H2oH1);
    }

    std::cout << "==================== Conclusion ====================" << '\n';

    std::cout << "Mean magnitude: " << H2i / T(num) << '\n';
}

int main(int argc, char* argv[])
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    if (argc != 3) {
        std::cerr
            << '\n'
            << "Error: incorrect arguments!"
            << '\n'
            << "       use as ./paper_table <init rseed (>=0)> <final rseed (inclusive)>"
            << '\n'
            << '\n';
        std::exit(EXIT_FAILURE);
    }

    run<double, double>(std::atoi(argv[1]), std::atoi(argv[2]));
}
