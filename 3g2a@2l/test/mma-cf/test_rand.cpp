#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <vector>

#include <qd/qd_real.h>

//#include "finrem/0q3g2A/0q3g2A-2l.h"
// #include "chsums/0q3gA.h"
#include "chsums/NJetAnalytic.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"

template <typename T>
void run(const int start, const int end)
{
    // const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };
    //const int Nc { 3 };
    //const int Nf { 5 };
    //const double mur { 1. };
    const std::vector<double> scales2 { { 0 } };

    NJetAnalytic<T> an(1., 5);

    // Amp0q3gAA<T> amp(Ax);

    //Amp0q3g2A_a2l<T, T> amp(Ax);
    //amp.setNf(Nf);
    //amp.setNc(Nc);
    //amp.setMuR2(mur * mur);

    for (int p { start }; p <= end; ++p) {
        std::cout << "==================== Point " << p
                  << " ====================" << '\n';

        std::vector<MOM<T>> momenta { njet_random_point<T>(p) };
        refineM(momenta, momenta, scales2);

        an.setMomenta(momenta.data());

        std::cout << "{" << '\n';
        for (std::size_t i { 0 }; i < momenta.size(); ++i) {
            std::cout << momenta[i] << (i == 4 ? "" : ",") << '\n';
        }
        std::cout << "}" << '\n';

        for (std::size_t i { 0 }; i < momenta.size(); ++i) {
            for (std::size_t j { i + 1 }; j < momenta.size(); ++j) {
                std::cout << "s" << i << j << ": " << an.lS(i, j) << '\n';
            }
        }

        std::cout << "eps5: " << an.eps5() << '\n';

        //amp.setMomenta(momenta);
        //amp.initFinRem();
        // amp.setSpecFuncs();

        // std::cout << '\n';
        // amp.printSpecFuncs();
        // std::cout << '\n';
    }
}

int main(int argc, char* argv[])
{
    std::cout << std::scientific << std::setprecision(64);
    std::cout << '\n';

    if (argc != 3) {
        std::cerr << "Error: run as `./test_rand <initial rseed> <final rseed (inclusive)>`, where rseed is the random number seed for the phase space point generator (rseed>=1)." << '\n';
        std::exit(EXIT_FAILURE);
    }

    const int start { std::atoi(argv[1]) };

    if (start < 1) {
        std::cerr << "Error: initial rseed must be >= 1!\n";
        std::exit(EXIT_FAILURE);
    }

    const int end { std::atoi(argv[2]) };

    if (end < start) {
        std::cerr << "Error: final rseed must be >= initial rseed!\n";
        std::exit(EXIT_FAILURE);
    }

    //std::cout << "double" << '\n' << '\n';
    //run<double>(start, end);

    //std::cout << "dd_real" << '\n' << '\n';
    run<qd_real>(start, end);

    std::cout << '\n';
}
