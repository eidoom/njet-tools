#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "chsums/NJetAccuracy.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"
#include "calc2.hpp"

void run(const Data<double>& data, const int start, const int end)
{
    const int legs { 5 };
    const int Nc { 3 };
    const int Nf { 5 };
    const std::vector<double> scales2 { { 0 } };
    const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };

    NJetAccuracy<double>* const ampDD { NJetAccuracy<double>::template create<Amp0q3g2A_a2l<double, double>>(Ax) };
    ampDD->setNf(Nf);
    ampDD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQQ { NJetAccuracy<dd_real>::template create<Amp0q3g2A_a2l<dd_real, dd_real>>(Ax) };
    ampQQ->setNf(Nf);
    ampQQ->setNc(Nc);

    for (int i { start }; i < end; ++i) {
        std::vector<MOM<double>> psD(legs);
        for (int j { 0 }; j < legs; ++j) {
            psD[j] = MOM<double>(
                data.points[i].momenta[j][0], data.points[i].momenta[j][1],
                data.points[i].momenta[j][2], data.points[i].momenta[j][3]);
        }
        refineM(psD, psD, scales2);

        ampDD->setMuR2(std::pow(data.points[i].mu, 2));
        ampDD->setMomenta(psD);
        ampDD->setSpecFuncs();
        ampDD->initFinRem();

        const double born_valD { ampDD->c1lx1l_fin() };
        const double virt_valD { ampDD->c2lx1l_fin() };

        const double born_errD { std::abs(ampDD->c1lx1l_fin_error() / ampDD->c1lx1l_fin_value()) };
        const double virt_errD { std::abs(ampDD->c2lx1l_fin_error() / ampDD->c2lx1l_fin_value()) };

        {
            std::ofstream o("result.DD." + std::to_string(start), std::ios::app);
            o.setf(std::ios_base::scientific);
            o.precision(16);
            o
                << i << ' '
                << born_valD << ' '
                << born_errD << ' '
                << virt_valD << ' '
                << virt_errD << ' '
                << '\n';
        }

        std::vector<MOM<dd_real>> psQ(legs);
        refineM(psD, psQ, scales2);

        ampQQ->setMuR2(std::pow(data.points[i].mu, 2));
        ampQQ->setMomenta(psQ);
        ampQQ->setSpecFuncs();
        ampQQ->initFinRem();

        const dd_real born_valQ { ampQQ->c1lx1l_fin() };
        const dd_real virt_valQ { ampQQ->c2lx1l_fin() };

        const dd_real born_errQ { abs(ampQQ->c1lx1l_fin_error() / ampQQ->c1lx1l_fin_value()) };
        const dd_real virt_errQ { abs(ampQQ->c2lx1l_fin_error() / ampQQ->c2lx1l_fin_value()) };

        {
            std::ofstream o("result.QQ." + std::to_string(start), std::ios::app);
            o.setf(std::ios_base::scientific);
            o.precision(16);
            o
                << i << ' '
                << born_valQ << ' '
                << born_errQ << ' '
                << virt_valQ << ' '
                << virt_errQ << ' '
                << '\n';
        }
    }
}

int main(int argc, char* argv[])
{
    if (argc != 3 && argc != 4) {
        std::cerr << "Error: incorrect arguments!\nRun as `./test <points file> <initial point> <final point (exclusive, optional)>`, where rseed is the random number seed for the phase space point generator." << '\n';
        std::exit(EXIT_FAILURE);
    }

    const std::string filename { argv[1] };
    const int start { std::atoi(argv[2]) };
    const int end { argc == 4 ? std::atoi(argv[3]) : start + 1 };
    const Data<double> data(filename);

    if (end > static_cast<int>(data.points.size())) {
        std::cerr << "Error: end is beyond length of momenta!\n";
        std::exit(EXIT_FAILURE);
    }

    run(data, start, end);
}
