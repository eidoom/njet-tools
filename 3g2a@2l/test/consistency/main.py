#!/usr/bin/env python3

import argparse
import sys
import os

import numpy
from matplotlib_venn import venn2
import matplotlib

dirname = os.path.dirname(__file__)
path1 = os.path.join(dirname, "../test-stability/")
sys.path.insert(0, path1)

import reader
import plotter


def d(a, b):
    # return abs(1 - b / a)
    # return abs((a - b) / a)
    # return abs(1 - a / b)
    # return abs((a - b) / b)
    # return 2 * abs((a - b) / (a + b))
    # return abs((a - b) / max(a, b))
    return abs((a - b) / min(a, b))


def scaling(dd, cut=1e-3, print_=False, loops=2):
    print(
        "Scaling test picks up on the following points with a cut of",
        100 * cut,
        "% difference at",
        loops,
        "loop" + ("s" if loops > 1 else ""),
    )

    if print_:
        print("pt", "err")

    if loops == 2:
        j = 4
    elif loops == 1:
        j = 2

    count = 0
    fails = set()

    for pt in dd:
        n = int(pt[0])
        err = pt[j]
        if err > cut:
            if print_:
                print(n, err)
            count += 1
            fails.add(n)

    print(count, "out of", len(qq), "(", round(100 * count / len(qq), 1), "%)\n")

    return fails


## ordered data
# def consistency(dd, qq, loops=2, cut=0.1, print_all=False, print_fails=False, dp=1):
#     print(
#         "Checking if f128 result withing error of f64 result at",
#         loops,
#         "loop" + ("s" if loops > 1 else ""),
#     )

#     fails = 0
#     fails_cut = 0

#     if loops == 2:
#         j = 3
#     elif loops == 1:
#         j = 1

#     for dd_pt, qq_pt in zip(dd, qq):
#         failed = False

#         dd_val, dd_err = dd_pt[[j, j + 1]]
#         qq_val, qq_err = qq_pt[[j, j + 1]]

#         rel_diff = d(dd_val, qq_val)
#         threshold = max(dd_err, cut)

#         if rel_diff > cut:
#             fails_cut += 1

#         if rel_diff > threshold:
#             fails += 1
#             failed = True

#         if (print_fails and failed) or print_all:
#             n = int(dd_pt[0])
#             print(n)
#             print("DD", dd_val, dd_err)
#             print("QQ", qq_val, qq_err)
#             print("d ", rel_diff, "Fail" if failed else "Pass")
#             print()

#     fail_perc = round(100 * fails / len(dd), dp)
#     print(
#         f"{fails} failed out of {len(dd)} ({fail_perc}%) to meet d < max(DD err, {100 * cut}%)"
#     )

#     soft_fail_perc = round(100 * fails_cut / len(dd), dp)
#     print(
#         f"{fails_cut} failed out of {len(dd)} ({soft_fail_perc}%) to meet d < {100 * cut}%\n"
#     )


def print_point(i, dd_pt, dd_err, qq_pt, qq_err, rel_diff, failed):
    print(i)
    print("DD", dd_pt, dd_err)
    print("QQ", qq_pt, qq_err)
    print("d ", rel_diff, "Fail" if failed else "Pass")
    print()


# unordered data
def consistency_dict(
    dd_val,
    dd_err,
    qq_val,
    qq_err,
    loops=2,
    cut=0.1,
    print_all=False,
    print_fails=False,
    dp=1,
):
    print(
        "Checking if f128 result within error of f64 result at",
        loops,
        "loop" + ("s" if loops > 1 else ""),
    )

    fails = 0
    fails_cut = 0
    fails_err = 0
    fails_cut_set = set()

    for i, qq_pt in qq_val.items():
        failed = False

        dd_pt = dd_val[i]
        err = dd_err[i]

        rel_diff = d(dd_pt, qq_pt)
        threshold = max(err, cut)

        if rel_diff > err:
            fails_err += 1

        if rel_diff > cut:
            fails_cut += 1
            fails_cut_set.add(i)

        if rel_diff > threshold:
            fails += 1
            failed = True

        if (print_fails and failed) or print_all:
            print_point(i, dd_pt, err, qq_pt, qq_err[i], rel_diff, failed)

    fail_perc = round(100 * fails / len(dd), dp)
    print(
        f"{fails} fails out of {len(qq)} ({fail_perc}%) to meet d < max(DD err, {100 * cut}%)"
    )

    soft_fail_perc = round(100 * fails_cut / len(dd), dp)
    print(
        f"{fails_cut} failed out of {len(qq)} ({soft_fail_perc}%) to meet d < {100 * cut}%"
    )

    hard_fail_perc = round(100 * fails_err / len(dd), dp)
    print(
        f"{fails_err} failed out of {len(qq)} ({hard_fail_perc}%) to meet d < DD err (from scaling test)\n"
    )

    return fails_cut_set


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "analyse consistency of 3g2A 2L run over phase space"
    )
    parser.add_argument(
        "-a",
        "--all",
        action="store_true",
        help="print all",
    )
    parser.add_argument(
        "-f",
        "--fails",
        action="store_true",
        help="print failures",
    )
    args = parser.parse_args()

    dd = reader.read_memo("result.DD.csv")
    qq = reader.read_memo("result.QQ.csv")

    dd_1l_val = {}
    dd_1l_err = {}
    qq_1l_val = {}
    qq_1l_err = {}

    dd_2l_val = {}
    dd_2l_err = {}
    qq_2l_val = {}
    qq_2l_err = {}

    for dd_pt in dd:
        i = int(dd_pt[0])
        dd_1l_val[i] = dd_pt[1]
        dd_1l_err[i] = dd_pt[2]
        dd_2l_val[i] = dd_pt[3]
        dd_2l_err[i] = dd_pt[4]

    for qq_pt in qq:
        i = int(qq_pt[0])
        qq_1l_val[i] = qq_pt[1]
        qq_1l_err[i] = qq_pt[2]
        qq_2l_val[i] = qq_pt[3]
        qq_2l_err[i] = qq_pt[4]

    cut = 1e-4

    print("d = relative error\n")

    scaling(dd, cut=cut, loops=1)

    fails_scaling_2l = scaling(dd, cut=cut, loops=2)

    consistency_dict(
        dd_1l_val,
        dd_1l_err,
        qq_1l_val,
        qq_1l_err,
        cut=cut,
        print_all=args.all,
        print_fails=args.fails,
        loops=1,
    )

    fails_qq_2l = consistency_dict(
        dd_2l_val,
        dd_2l_err,
        qq_2l_val,
        qq_2l_err,
        cut=cut,
        print_all=args.all,
        print_fails=args.fails,
        loops=2,
    )

    got = fails_qq_2l.intersection(fails_scaling_2l)
    missed = fails_qq_2l.difference(fails_scaling_2l)
    print(
        "2L scaling test misses",
        len(missed),
        "points (",
        round(100 * len(missed) / len(fails_qq_2l), 1),
        "% of reevaluation fails;",
        round(100 * len(missed) / len(qq_2l_val), 1),
        "% of all evaluations) that fail to meet the cut on reevaluation in higher precision (false positive)",
    )

    extra = fails_scaling_2l.difference(fails_qq_2l)
    print(
        "2L scaling test failed",
        len(extra),
        "extra points (",
        round(100 * len(extra) / len(fails_qq_2l), 1),
        "% of reevaluation fails;",
        round(100 * len(extra) / len(qq_2l_val), 1),
        "% of all evaluations) that succeeded in meeting the cut on reevaluation in higher precision (false negative)",
    )

    weak_cut = 10 * cut
    perc = round(100 * weak_cut, 1)
    print(
        "\nFalse positives with reevaluation d >",
        perc,
        "%",
    )

    count = 0
    worst = 0
    false_positives = []
    for i in missed:
        rel_diff = d(dd_2l_val[i], qq_2l_val[i])

        false_positives.append(rel_diff)

        if rel_diff > weak_cut:
            print_point(
                i,
                dd_2l_val[i],
                dd_2l_err[i],
                qq_2l_val[i],
                qq_2l_err[i],
                rel_diff,
                True,
            )

            if rel_diff > worst:
                worst = rel_diff

            count += 1

    false_positives = numpy.array(false_positives)

    print(
        "Of the false positives,",
        count,
        "(",
        round(100 * count / len(missed), 1),
        "% of false positives;",
        round(100 * count / len(qq_2l_val), 4),
        "% of all evaluations) had d >",
        perc,
    )
    print("Worst was d =", worst)

    plotter.histo_single(
        (false_positives,),
        names=("false positives",),
        xlabel="Relative error",
        label="false_pos",
        x_min_pow=-4,
        x_max_pow=-2,
        num_bins=20,
        legend=False,
        xlog=False,
    )

    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")
    cmap = matplotlib.cm.get_cmap("tab10")
    fig, axs = matplotlib.pyplot.subplots()
    v = venn2(
        subsets=(len(missed), len(extra), len(got)),
        set_labels=("Re-evaluation", "Scaling"),
        set_colors=(cmap(0), cmap(3)),
        ax=axs,
        subset_label_formatter=lambda x: str(round(100 * x / len(qq_2l_val), 1))
        + r"\%",
    )
    # venn2([fails_qq_2l, fails_scaling_2l], set_labels=("Reevaluation", "Scaling"), ax=axes)

    # fp = v.get_label_by_id('10')
    # fp.set_text(fp.get_text() + "\nFalse positive")

    axs.annotate(
        "False positive",
        xy=v.get_label_by_id("10").get_position() + numpy.array([0, -0.1]),
        ha="center",
    )
    axs.annotate(
        "True negative",
        xy=v.get_label_by_id("11").get_position() + numpy.array([0, -0.1]),
        ha="center",
    )
    axs.annotate(
        "False negative",
        xy=v.get_label_by_id("01").get_position() + numpy.array([0, -0.1]),
        ha="center",
    )

    fig.savefig(
        "euler.pdf",
        bbox_inches="tight",
    )
    fig.savefig(
        "euler.png",
        bbox_inches="tight",
    )
