#!/usr/bin/env python3


def cf_njet(file_njet, cut):
    print("Comparing OL results against NJet results from", file_njet)

    njet = {}
    with open(file_njet) as f:
        for line in f:
            lst = line.split()
            njet[int(lst[0])] = float(lst[1])

    for i, me in njet.items():
        ratio = ol[i] / me
        if abs(1 - ratio) > cut:
            print(i, ratio)


if __name__ == "__main__":
    Nc = 3
    cut = 1e-5

    file_ol = "../collab/points_new.txt"

    print("Printing point ratios = OL/NJet that had disagreement >", 100 * cut, "%")
    print("Getting OL results from", file_ol)

    ol = {}
    with open(file_ol) as f:
        i = 0
        for line in f:
            if line[:7] == "bare ME":
                ol[i] = (11.0 / 9.0) ** 2 * Nc * (Nc ** 2 - 1) * float(line[9:])
                i += 1

    cf_njet("result.DD.csv", cut)
    cf_njet("result.QQ.csv", cut)
