#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "chsums/NJetAccuracy.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"

void run(const int start, const int end)
{
    const std::vector<double> scales2 { { 0 } };
    const double sqrtS { 10. };
    const double mur { sqrtS / 2. };
    const int Nc { 3 };
    const int Nf { 5 };

    const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };

    NJetAccuracy<double>* const ampDD { NJetAccuracy<double>::template create<Amp0q3g2A_a2l<double, double>>(Ax) };
    ampDD->setMuR2(mur * mur);
    ampDD->setNf(Nf);
    ampDD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQQ { NJetAccuracy<dd_real>::template create<Amp0q3g2A_a2l<dd_real, dd_real>>(Ax) };
    ampQQ->setMuR2(mur * mur);
    ampQQ->setNf(Nf);
    ampQQ->setNc(Nc);

    // rseed = p
    for (int p { start }; p < end; ++p) {
        std::vector<MOM<double>> psD { njet_random_point<double>(p, sqrtS) };
        refineM(psD, psD, scales2);

        ampDD->setMomenta(psD);

        ampDD->setSpecFuncs();

        ampDD->initFinRem();

        const double born_valD { ampDD->c1lx1l_fin() };
        const double virt_valD { ampDD->c2lx1l_fin() };

        const double born_errD { std::abs(ampDD->c1lx1l_fin_error() / ampDD->c1lx1l_fin_value()) };
        const double virt_errD { std::abs(ampDD->c2lx1l_fin_error() / ampDD->c2lx1l_fin_value()) };

        {
            std::ofstream o("result.DD." + std::to_string(start), std::ios::app);
            o.setf(std::ios_base::scientific);
            o.precision(16);
            o
                << p << ' '
                << born_valD << ' '
                << born_errD << ' '
                << virt_valD << ' '
                << virt_errD << ' '
                << '\n';
        }

        std::vector<MOM<dd_real>> psQ { njet_random_point<dd_real>(p, sqrtS) };
        refineM(psQ, psQ, scales2);

        ampQQ->setMomenta(psQ);

        ampQQ->setSpecFuncs();

        ampQQ->initFinRem();

        const dd_real born_valQ { ampQQ->c1lx1l_fin() };
        const dd_real virt_valQ { ampQQ->c2lx1l_fin() };

        const dd_real born_errQ { abs(ampQQ->c1lx1l_fin_error() / ampQQ->c1lx1l_fin_value()) };
        const dd_real virt_errQ { abs(ampQQ->c2lx1l_fin_error() / ampQQ->c2lx1l_fin_value()) };

        {
            std::ofstream o("result.QQ." + std::to_string(start), std::ios::app);
            o.setf(std::ios_base::scientific);
            o.precision(16);
            o
                << p << ' '
                << born_valQ << ' '
                << born_errQ << ' '
                << virt_valQ << ' '
                << virt_errQ << ' '
                << '\n';
        }
    }
}

int main(int argc, char* argv[])
{
    if (argc != 3) {
        std::cerr << "Error: run as `./test <initial rseed> <final rseed (exclusive)>`, where rseed is the random number seed for the phase space point generator." << '\n';
        std::exit(EXIT_FAILURE);
    }

    const int start { std::atoi(argv[1]) };
    const int end { std::atoi(argv[2]) };
    run(start, end);
}
