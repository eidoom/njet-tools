#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "chsums/NJetAccuracy.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"

void run(const int start, const int end)
{
    std::cout << "Initialising amplitude classes..." << '\n';

    using TP = std::chrono::time_point<std::chrono::high_resolution_clock>;

    TP t0;
    TP t1;

    long d_specDD { 0 };
    long d_specQD { 0 };
    long d_specQQ { 0 };

    long d_elseDD { 0 };
    long d_elseQD { 0 };
    long d_elseQQ { 0 };

    const std::vector<double> scales2 { { 0 } };
    const double sqrtS { 10. };
    const double mur { sqrtS / 2. };
    const int Nc { 3 };
    const int Nf { 0 };

    const double cutoff_errD { 1e-3 };
    const dd_real cutoff_errQ { static_cast<dd_real>(cutoff_errD) };

    const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };

    NJetAccuracy<double>* const ampDD { NJetAccuracy<double>::template create<Amp0q3g2A_a2l<double, double>>(Ax) };
    ampDD->setMuR2(mur * mur);
    ampDD->setNf(Nf);
    ampDD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQD { NJetAccuracy<dd_real>::template create<Amp0q3g2A_a2l<dd_real, double>>(Ax) };
    ampQD->setMuR2(mur * mur);
    ampQD->setNf(Nf);
    ampQD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQQ { NJetAccuracy<dd_real>::template create<Amp0q3g2A_a2l<dd_real, dd_real>>(Ax) };
    ampQQ->setMuR2(mur * mur);
    ampQQ->setNf(Nf);
    ampQQ->setNc(Nc);

    // rseed = p
    for (int p { start }; p < end; ++p) {
        std::cout
            << "Evaluating point " << p << " ..." << '\n'
            << "  try coeff(double)+specialFn(double)..." << '\n';

        std::vector<MOM<double>> psD { njet_random_point<double>(p, sqrtS) };
        refineM(psD, psD, scales2);
        ampDD->setMomenta(psD);

        t0 = std::chrono::high_resolution_clock::now();
        ampDD->setSpecFuncs();
        t1 = std::chrono::high_resolution_clock::now();
        d_specDD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

        t0 = std::chrono::high_resolution_clock::now();
        ampDD->initFinRem();
        const double born_valD { ampDD->c1lx1l_fin() };
        const double virt_valD { ampDD->c2lx1l_fin() };
        t1 = std::chrono::high_resolution_clock::now();
        d_elseDD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

        const double born_errD { std::abs(ampDD->c1lx1l_fin_error() / ampDD->c1lx1l_fin_value()) };
        const double virt_errD { std::abs(ampDD->c2lx1l_fin_error() / ampDD->c2lx1l_fin_value()) };

        {
            std::ofstream o("result.DD." + std::to_string(start), std::ios::app);
            o.setf(std::ios_base::scientific);
            o.precision(16);
            o
                << p << ' '
                << born_valD << ' '
                << born_errD << ' '
                << virt_valD << ' '
                << virt_errD << ' '
                << d_specDD << ' '
                << d_elseDD << ' '
                << '\n';
        }

        if (virt_errD < cutoff_errD) {
            std::cout
                << "    stability test passed" << '\n';
        } else {
            std::cout
                << "    stability test failed" << '\n'
                << "  try coeff(quad)+specialFn(double)..." << '\n';

            dd_real born_valQ;
            dd_real virt_valQ;

            dd_real born_errQ;
            dd_real virt_errQ;

            std::vector<MOM<dd_real>> psQ { njet_random_point<dd_real>(p, sqrtS) };
            refineM(psQ, psQ, scales2);

            ampQD->setMomenta(psQ);

            t0 = std::chrono::high_resolution_clock::now();
            ampQD->copySpecFuncs(ampDD);
            t1 = std::chrono::high_resolution_clock::now();
            d_specQD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

            t0 = std::chrono::high_resolution_clock::now();
            ampQD->initFinRem();
            born_valQ = ampQD->c1lx1l_fin();
            virt_valQ = ampQD->c2lx1l_fin();
            t1 = std::chrono::high_resolution_clock::now();
            d_elseQD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

            born_errQ = abs(ampQD->c1lx1l_fin_error() / ampQD->c1lx1l_fin_value());
            virt_errQ = abs(ampQD->c2lx1l_fin_error() / ampQD->c2lx1l_fin_value());

            {
                std::ofstream o("result.QD." + std::to_string(start), std::ios::app);
                o.setf(std::ios_base::scientific);
                o.precision(16);
                o
                    << p << ' '
                    << born_valQ << ' '
                    << born_errQ << ' '
                    << virt_valQ << ' '
                    << virt_errQ << ' '
                    << d_specQD << ' '
                    << d_elseQD << ' '
                    << '\n';
            }

            if (virt_errQ < cutoff_errQ) {
                std::cout
                    << "    stability test passed" << '\n';

            } else {
                std::cout
                    << "    stability test failed" << '\n'
                    << "  try coeff(quad)+specialFn(quad)..." << '\n';

                ampQQ->setMomenta(psQ);

                t0 = std::chrono::high_resolution_clock::now();
                ampQQ->setSpecFuncs();
                t1 = std::chrono::high_resolution_clock::now();
                d_specQQ = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

                t0 = std::chrono::high_resolution_clock::now();
                ampQQ->initFinRem();
                ampQQ->copyCoeffs(ampQD);
                born_valQ = ampQQ->c1lx1l_fin();
                virt_valQ = ampQQ->c2lx1l_fin();
                t1 = std::chrono::high_resolution_clock::now();
                d_elseQQ = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

                born_errQ = abs(ampQQ->c1lx1l_fin_error() / ampQQ->c1lx1l_fin_value());
                virt_errQ = abs(ampQQ->c2lx1l_fin_error() / ampQQ->c2lx1l_fin_value());

                {
                    std::ofstream o("result.QQ." + std::to_string(start), std::ios::app);
                    o.setf(std::ios_base::scientific);
                    o.precision(16);
                    o
                        << p << ' '
                        << born_valQ << ' '
                        << born_errQ << ' '
                        << virt_valQ << ' '
                        << virt_errQ << ' '
                        << d_specQQ << ' '
                        << d_elseQQ << ' '
                        << '\n';
                }

                if (virt_errQ < cutoff_errQ) {
                    std::cout
                        << "    stability test passed" << '\n';
                } else {
                    std::cout
                        << "    stability test failed (END)" << '\n';
                }
            }
        }
    }
}

int main(int argc, char* argv[])
{
    std::cout << '\n';

    if (argc == 3) {
        const int start { std::atoi(argv[1]) };
        const int end { std::atoi(argv[2]) };
        run(start, end);
    } else {
        std::cerr << "Run as `./test <initial rseed> <final rseed (exclusive)>`, where rseed is the random number seed for the phase space point generator." << '\n';
        std::exit(EXIT_FAILURE);
    }

    std::cout << '\n';
}
