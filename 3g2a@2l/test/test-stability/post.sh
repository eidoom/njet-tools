#!/usr/bin/env bash

mkdir -p ~/www/files/research/3g2a2l
cat result.DD.* > ~/www/files/research/3g2a2l/result.DD.csv
cat result.QD.* > ~/www/files/research/3g2a2l/result.QD.csv
cat result.QQ.* > ~/www/files/research/3g2a2l/result.QQ.csv
