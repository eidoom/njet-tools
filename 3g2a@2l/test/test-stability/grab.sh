#!/usr/bin/env bash

mkdir -p 3g2a_res
cd 3g2a_res

rm result.*.csv

wget 'https://www.ippp.dur.ac.uk/~rmoodie/files/research/3g2a2l/result.DD.csv'
wget 'https://www.ippp.dur.ac.uk/~rmoodie/files/research/3g2a2l/result.QD.csv'
wget 'https://www.ippp.dur.ac.uk/~rmoodie/files/research/3g2a2l/result.QQ.csv'

cd ..
