#!/usr/bin/env python3
# coding=UTF-8

import matplotlib
import numpy
import reader
import plotter


def passed(data, i, cutoff):
    return data[data[:, i] < cutoff]


def failed(data, i, cutoff):
    return data[data[:, i] > cutoff]


def cut(data, i, cutoff):
    return (passed(data, i, cutoff), failed(data, i, cutoff))


def perc(subset, data, dp=1):
    return round(100 * len(subset) / len(data), dp)


def an_cut(total, this_prec, passed, failed, name, dp=1):
    print(f"{name} all:  {len(this_prec)}")
    print(
        f"{name} pass: {len(passed)} ({perc(passed, this_prec, dp)}% {name}; {perc(passed, total, dp)}% all)"
    )
    print(
        f"{name} fail: {len(failed)} ({perc(failed, this_prec, dp)}% {name}; {perc(failed, total, dp)}% all)"
    )


def stability(
    all_dd,
    all_qd,
    all_qq,
    i,
    cutoff=1e-3,
    dp=1,
):
    pass_dd, fail_dd = cut(all_dd, i, cutoff)
    pass_qd, fail_qd = cut(all_qd, i, cutoff)
    pass_qq, fail_qq = cut(all_qq, i, cutoff)

    an_cut(all_dd, all_dd, pass_dd, fail_dd, "SD/SD", dp)
    an_cut(all_dd, all_qd, pass_qd, fail_qd, "DD/SD", dp)
    an_cut(all_dd, all_qq, pass_qq, fail_qq, "DD/DD", dp)

    data = [
        pass_dd[:, i],
        pass_qd[:, i],
        pass_qq[:, i],
        fail_dd[:, i],
        fail_qd[:, i],
        fail_qq[:, i],
    ]
    return data


def average_time(datum, name="total", dp=1, tf=1e6, tu="s"):
    mean = numpy.mean(datum)
    print(f"Average {name} time: {round(mean/tf, dp)} {tu}")
    return mean


def average_time_contrib(data, contrib, name, dp=1, tf=1e6, tu="s"):
    if data.size > 0:
        return average_time(data[:, contrib], name=name, dp=dp, tf=tf, tu=tu)


def average_time_single(data, specf, coeff, name, dp=1, tf=1e6, tu="s"):
    if data.size > 0:
        return average_time(
            data[:, specf] + data[:, coeff], name=name, dp=dp, tf=tf, tu=tu
        )


def average_ratio(datum, name="total", dp=1):
    ratio = numpy.mean(datum)
    percentage = 100 * ratio / (1 + ratio)
    print(
        f"Average {name} specf/coeff ratio: {round(ratio, dp)} (specf take {round(percentage, dp)}%)"
    )


def average_ratio_single(data, specf, coeff, name, dp=1):
    if data.size > 0:
        average_ratio(data[:, specf] / data[:, coeff], name=name, dp=dp)


def timing_cutoff(
    dataDD,
    dataQD,
    dataQQ,
    err,
    specf,
    coeff,
    cutoff=1e-3,
    dp=1,
):
    t_specf = []
    t_coeff = []

    incQD = []
    incQQ = []

    t_specf_D_pass = []
    t_specf_Q_pass = []
    t_coeff_D_pass = []
    t_coeff_Q_pass = []
    t_specf_D_fail = []
    t_specf_Q_fail = []
    t_coeff_D_fail = []
    t_coeff_Q_fail = []

    for i, errDD, tDD_specf, tDD_coeff in dataDD[:, [0, err, specf, coeff]]:
        tt_specf = tDD_specf
        tt_coeff = tDD_coeff

        if errDD < cutoff:
            t_specf_D_pass.append(tt_specf)
            t_coeff_D_pass.append(tt_coeff)
        else:
            t_coeff_D_fail.append(tDD_coeff)

            matchQD = dataQD[dataQD[:, 0] == i]
            # this checks if there exists a QD result
            # this won't be true if one was running when I killed the data generation jobs
            if matchQD.size > 0:
                rQD = matchQD[0, [specf, coeff, err]]
                incQD.append([rQD[0], rQD[1]])

                tt_specf += rQD[0]
                tt_coeff += rQD[1]

                if rQD[2] < cutoff:
                    t_specf_D_pass.append(tt_specf)
                    t_coeff_Q_pass.append(tt_coeff)
                else:
                    t_specf_D_fail.append(tt_specf)

                    matchQQ = dataQQ[dataQQ[:, 0] == i]
                    # this checks if there exists a QQ result
                    if matchQQ.size > 0:
                        rQQ = matchQQ[0, [specf, coeff, err]]
                        incQQ.append([rQQ[0], rQQ[1]])

                        tt_specf += rQQ[0]
                        tt_coeff += rQQ[1]

                        if rQQ[2] < cutoff:
                            t_coeff_Q_pass.append(tt_coeff)
                            t_specf_Q_pass.append(tt_specf)
                        else:
                            t_coeff_Q_fail.append(tt_coeff)
                            t_specf_Q_fail.append(tt_specf)

                    else:
                        print(f"Error: rseed {int(i)} is missing DD/DD result!")
            else:
                print(f"Error: rseed {int(i)} is missing DD/SD result!")

        t_specf.append(tt_specf)
        t_coeff.append(tt_coeff)

    t_specf = numpy.array(t_specf)
    t_coeff = numpy.array(t_coeff)
    incQD = numpy.array(incQD)
    incQQ = numpy.array(incQQ)
    t_specf_D_pass = numpy.array(t_specf_D_pass)
    t_specf_Q_pass = numpy.array(t_specf_Q_pass)
    t_coeff_D_pass = numpy.array(t_coeff_D_pass)
    t_coeff_Q_pass = numpy.array(t_coeff_Q_pass)
    t_specf_D_fail = numpy.array(t_specf_D_fail)
    t_specf_Q_fail = numpy.array(t_specf_Q_fail)
    t_coeff_D_fail = numpy.array(t_coeff_D_fail)
    t_coeff_Q_fail = numpy.array(t_coeff_Q_fail)

    t = t_specf + t_coeff
    r = t_specf / t_coeff

    tf = 1e6
    tu = "s"

    t_all_specf_av = average_time(t_specf, name="specf", dp=dp, tf=tf, tu=tu)
    t_all_coeff_av = average_time(t_coeff, name="coeff", dp=dp, tf=tf, tu=tu)
    average_time(t, dp=dp, tf=tf, tu=tu)
    average_ratio(r, dp=dp)

    t_dd_specf_av = average_time_contrib(
        dataDD, specf, name="specf SD/SD", dp=dp, tf=tf, tu=tu
    )
    t_dd_coeff_av = average_time_contrib(
        dataDD, coeff, name="coeff SD/SD", dp=dp, tf=tf, tu=tu
    )
    average_time_single(dataDD, specf, coeff, name="total SD/SD", dp=dp, tf=tf, tu=tu)
    average_ratio_single(dataDD, specf, coeff, name="SD/SD", dp=dp)

    t_qd_specf_av = average_time_contrib(
        incQD, 0, name="specf DD/SD", dp=dp, tf=tf, tu=tu
    )
    t_qd_coeff_av = average_time_contrib(
        incQD, 1, name="coeff DD/SD", dp=dp, tf=tf, tu=tu
    )
    average_time_single(incQD, 0, 1, name="DD/SD", dp=dp, tf=tf, tu=tu)
    average_ratio_single(incQD, 0, 1, name="DD/SD", dp=dp)

    t_qq_specf_av = average_time_contrib(
        incQQ, 0, name="specf DD/SD", dp=dp, tf=tf, tu=tu
    )
    t_qq_coeff_av = average_time_contrib(
        incQQ, 1, name="coeff DD/SD", dp=dp, tf=tf, tu=tu
    )
    average_time_single(incQQ, 0, 1, name="DD/DD", dp=dp, tf=tf, tu=tu)
    average_ratio_single(incQQ, 0, 1, name="DD/DD", dp=dp)

    specfs = (t_all_specf_av, t_dd_specf_av, t_qd_specf_av, t_qq_specf_av)
    coeffs = (t_all_coeff_av, t_dd_coeff_av, t_qd_coeff_av, t_qq_coeff_av)

    # if plot:
    #     plotter.histo_single(
    #         r,
    #         xlabel="specf/coeff",
    #         label=name + "_ratio",
    #         cutoff=cutoff,
    #         x_min_pow=0,
    #         x_max_pow=3,
    #         xlog=True,
    #         ylog=False,
    #         normalisation=1e6,
    #     )

    # plotter.histo_single(
    #     (t_specf, t_coeff),
    #     names=("spec", "coeff"),
    #     xlabel="Time (s)",
    #     cutoff=cutoff,
    #     x_min_pow=low,
    #     x_max_pow=high,
    #     xlog=True,
    #     ylog=True,
    #     label=name + "_sfvc",
    #         normalisation=1e6,
    # )

    # plotter.histo_single(
    #     (
    #         t_specf_D_pass,
    #         t_specf_Q_pass,
    #         t_coeff_D_pass,
    #         t_coeff_Q_pass,
    #     ),
    #     names=(
    #         "specf SD pass",
    #         "specf DD pass",
    #         "coeff SD pass",
    #         "coeff DD pass",
    #     ),
    #     xlabel="Time (s)",
    #     cutoff=cutoff,
    #     x_min_pow=low,
    #     x_max_pow=high,
    #     xlog=True,
    #     ylog=True,
    #     label=name + "_sfvc_detail",
    #         normalisation=1e6,
    # )

    return (
        t,
        specfs,
        coeffs,
        t_specf,
        t_coeff,
        t_specf_D_pass,
        t_specf_Q_pass,
        t_coeff_D_pass,
        t_coeff_Q_pass,
        t_specf_D_fail,
        t_specf_Q_fail,
        t_coeff_D_fail,
        t_coeff_Q_fail,
    )


if __name__ == "__main__":
    x_min_pow = -22
    leg_loc = "upper left"
    cutoff = 1e-3
    plot = True
    vector = True
    dp = 3

    print("3g2a")
    all_3g2a_dd, all_3g2a_qd, all_3g2a_qq = reader.read_proc("3g2a_res", "result")
    data1 = stability(
        all_3g2a_dd,
        all_3g2a_qd,
        all_3g2a_qq,
        4,
        cutoff=cutoff,
        dp=dp,
    )
    (
        cumulative3g2a,
        specfs3g2a,
        coeffs3g2a,
        t_specf3g2a,
        t_coeff3g2a,
        t_specf_D_pass_3g2a,
        t_specf_Q_pass_3g2a,
        t_coeff_D_pass_3g2a,
        t_coeff_Q_pass_3g2a,
        t_specf_D_fail_3g2a,
        t_specf_Q_fail_3g2a,
        t_coeff_D_fail_3g2a,
        t_coeff_Q_fail_3g2a,
    ) = timing_cutoff(
        all_3g2a_dd,
        all_3g2a_qd,
        all_3g2a_qq,
        4,
        5,
        6,
        cutoff=cutoff,
        dp=dp,
    )

    print("\n5g")
    all_5g_dd, all_5g_qd, all_5g_qq = reader.read_proc_trunc(
        "5g_res", "result", len(all_3g2a_dd)
    )
    data2 = stability(
        all_5g_dd,
        all_5g_qd,
        all_5g_qq,
        8,
        cutoff=cutoff,
        dp=dp,
    )
    (
        cumulative5g,
        specfs5g,
        coeffs5g,
        t_specf5g,
        t_coeff5g,
        t_specf_D_pass_5g,
        t_specf_Q_pass_5g,
        t_coeff_D_pass_5g,
        t_coeff_Q_pass_5g,
        t_specf_D_fail_5g,
        t_specf_Q_fail_5g,
        t_coeff_D_fail_5g,
        t_coeff_Q_fail_5g,
    ) = timing_cutoff(
        all_5g_dd,
        all_5g_qd,
        all_5g_qq,
        8,
        9,
        10,
        cutoff=cutoff,
        dp=dp,
    )

    if plot:

        # alow = 0.2
        # ahigh = 3.2

        # plotter.histo_single(
        #     (cumulative5g, cumulative3g2a),
        #     names=("5g", "3g2a"),
        #     xlabel="Time (s)",
        #     cutoff=cutoff,
        #     x_min_pow=alow,
        #     x_max_pow=ahigh,
        #     xlog=True,
        #     ylog=True,
        #         normalisation=1e6,
        # )

        # plotter.histo_single(
        #     (cumulative5g, cumulative3g2a),
        #     names=("5g", "3g2a"),
        #     xlabel="Time (s)",
        #     cutoff=cutoff,
        #     x_min_pow=alow,
        #     x_max_pow=ahigh,
        #     xlog=True,
        #     ylog=False,
        #         normalisation=1e6,
        # )

        # plotter.bar_timing_inset(
        #     specfs3g2a,
        #     coeffs3g2a,
        #     specfs5g,
        #     coeffs5g,
        #     logy=False,
        #     name="",
        # )

        names = [
            "SD/SD pass",
            "DD/SD pass",
            "DD/DD pass",
            "SD/SD fail",
            "DD/SD fail",
        ]

        # plotter.histo_single(
        #     data1,
        #     names,
        #     xlabel="Relative error",
        #     x_min_pow=-15,
        #     x_max_pow=3,
        #     label="3g2a_prelim_stability_cf",
        # )

        plotter.histo_single(
            (
                t_specf_D_pass_3g2a,
                t_specf_Q_pass_3g2a,
                t_coeff_D_pass_3g2a,
                t_coeff_Q_pass_3g2a,
            ),
            names=(
                "specf SD pass",
                "specf DD pass",
                "coeff SD pass",
                "coeff DD pass",
            ),
            xlabel="Time (s)",
            label="3g2a_prelim_timing_cf",
            normalisation=1e6,
            x_min_pow=-1.3,
            x_max_pow=3.5,
        )

        plotter.histo_pair(
            (data1, data2),
            names,
            cutoff=cutoff,
            procs=("3g2a", "5g"),
            x_min_pow=x_min_pow,
            ylog=False,
            xlabel="Relative error",
            label="stability",
            vector=vector,
        )

        plotter.histo_pair(
            (data1, data2),
            names,
            cutoff=cutoff,
            procs=("3g2a", "5g"),
            x_min_pow=x_min_pow,
            ylog=True,
            xlabel="Relative error",
            label="stability",
            vector=vector,
        )

        low = -1.5
        high = 3.5

        plotter.histo_pair(
            (
                (
                    t_specf_D_pass_3g2a,
                    t_specf_Q_pass_3g2a,
                    t_coeff_D_pass_3g2a,
                    t_coeff_Q_pass_3g2a,
                ),
                (
                    t_specf_D_pass_5g,
                    t_specf_Q_pass_5g,
                    t_coeff_D_pass_5g,
                    t_coeff_Q_pass_5g,
                ),
            ),
            names=(
                "specf SD pass",
                "specf DD pass",
                "coeff SD pass",
                "coeff DD pass",
            ),
            xlabel="Time (s)",
            cutoff=cutoff,
            x_min_pow=low,
            x_max_pow=high,
            xlog=True,
            ylog=True,
            label="timing_detail",
            procs=("3g2a", "5g"),
            leg_loc="upper right",
            text_reloc=True,
            normalisation=1e6,
            vector=vector,
        )
