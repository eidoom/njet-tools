#include <cmath>
#include <limits>
#include <vector>

#include "ngluon2/Mom.h"

template <typename T>
std::vector<MOM<T>> infinitesimal_perturbation(std::vector<MOM<T>> momenta)
{
    for (MOM<T>& momentum : momenta) {
        momentum.x0 = std::nextafter(momentum.x0, std::numeric_limits<T>::max());
        momentum.x1 = std::nextafter(momentum.x1, std::numeric_limits<T>::max());
        momentum.x2 = std::nextafter(momentum.x2, std::numeric_limits<T>::max());
        momentum.x3 = std::nextafter(momentum.x3, std::numeric_limits<T>::max());
    }
    return momenta;
}
