#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "chsums/NJetAccuracy.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

template <typename T>
class Data {
public:
    Data(const std::string& filename, const int m_ = 5, const int d_ = 4, const std::string& delim = " ")
        : momenta()
        , me()
        , d(d_)
        , m(m_)
        , delimiter(delim)
    {
        std::ifstream data(filename);
        std::string line;
        std::vector<std::vector<T>> point(m, std::vector<T>(d));
        std::vector<T> mom(d);
        int j { 0 }, pos, k { 0 };

        while (std::getline(data, line)) {
            if (line == "") {
                continue;
            }
            if (k != 2) {
                if (line.find('*') != std::string::npos) {
                    ++k;
                }
            } else {
                if (j != m) {
                    int i { 0 };
                    while (i < d) {
                        pos = line.find(delimiter);
                        std::string comp { line.substr(0, pos) };
                        line.erase(0, pos + delimiter.length());
                        if (comp != "") {
                            mom[i++] = (j < 2 ? -1 : +1) * std::stod(comp);
                        }
                    }
                    point[j++] = mom;
                } else {
                    momenta.push_back(point);
                    j = 0;
                    pos = line.find(delimiter);
                    line.erase(0, pos + delimiter.length());
                    me.push_back(std::stod(line));
                }
            }
        }
    };

    std::vector<std::vector<std::vector<T>>> momenta;
    std::vector<T> me;

private:
    const int d;
    const int m;
    const std::string delimiter;
};

template <typename T>
void printMom(const std::vector<std::vector<std::vector<T>>>& all)
{
    for (const std::vector<std::vector<T>>& pt : all) {
        for (const std::vector<T>& mom : pt) {
            for (const T& comp : mom) {
                std::cout << comp << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }
}

template <typename T>
void test(const Data<T>& data, const int start, const int end)
{
    std::cout
        << "Notation:" << '\n'
        << "  ratio    = |a/b|" << '\n'
        << "  abs diff = |a-b|" << '\n'
        << "  rel diff = 2*|a-b|/(a+b)" << '\n'
        << '\n';

    const int legs { 5 };
    const int Nc { 3 };
    const int Nf { 5 };
    const int pow_alpha { 2 };
    const int pow_alpha_s { 3 };
    const T alpha { 7.2973525693e-3 };
    const T mz2 { std::pow(91.1876, 2) };
    const T alpha_s_mz2 { 0.1179 };
    const T b0 { (11. * Nc - 2. * Nf) / 3. };
    const std::vector<T> scales2 { { 0 } }; // no scales
    const Flavour<T> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };
    Amp0q3g2A_a2l<T, T> amp(Ax, 1.);
    amp.setNf(Nf);
    amp.setNc(Nc);

    for (int i { start }; i < end; ++i) {
        std::vector<MOM<T>> psp(legs);
        for (int j { 0 }; j < legs; ++j) {
            psp[j] = MOM<T>(
                data.momenta[i][j][0], data.momenta[i][j][1],
                data.momenta[i][j][2], data.momenta[i][j][3]);
        }
        refineM(psp, psp, scales2);

        //const T s12 { 2. * dot(psp[0], psp[1]) };
        //for (const MOM<T>& m : psp) {
        //    std::cout << m << '\n';
        //}

        const T s45 { 2. * dot(psp[3], psp[4]) };
        const T alpha_s { 1. / ((1. / alpha_s_mz2) + b0 * std::log(s45 / mz2)) };

        amp.setMomenta(psp);

        amp.setMuR2(s45);

        amp.setSpecFuncs1L();
        amp.initFinRem();

        const T raw { amp.c1lx1l_fin() };

        const T coupling { std::pow(alpha, pow_alpha) * std::pow(alpha_s, pow_alpha_s) };

        const T res { coupling * raw };

        //const T abs_diff { std::abs(res - data.me[i]) };
        //const T av { 0.5 * (res + data.me[i]) };
        //const T rel_diff { abs_diff / av };
        const T ratio { std::abs(res / data.me[i]) };

        std::cout
            //<< "NNLOJET:  " << data.me[i] << '\n'
            //<< "NJet:     " << res << '\n'
            << "ratio:    " << ratio << '\n'
            //<< "abs diff: " << abs_diff << '\n'
            //<< "rel diff: " << rel_diff << '\n'
            //<< '\n'
	    ;
    }
}

void run(const std::vector<std::vector<std::vector<double>>>& momenta, const int start, const int end)
{
    std::cout << "Initialising amplitude classes..." << '\n';

    using TP = std::chrono::time_point<std::chrono::high_resolution_clock>;
    TP t0, t1;

    const int legs { 5 }, Nc { 3 }, Nf { 5 };
    const std::vector<double> scales2 { { 0 } };

    const double cutoff_errD { 1e-3 };
    const dd_real cutoff_errQ { static_cast<dd_real>(cutoff_errD) };

    const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };

    NJetAccuracy<double>* const ampDD { NJetAccuracy<double>::template create<Amp0q3g2A_a2l<double, double>>(Ax) };
    ampDD->setNf(Nf);
    ampDD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQD { NJetAccuracy<dd_real>::template create<Amp0q3g2A_a2l<dd_real, double>>(Ax) };
    ampQD->setNf(Nf);
    ampQD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQQ { NJetAccuracy<dd_real>::template create<Amp0q3g2A_a2l<dd_real, dd_real>>(Ax) };
    ampQQ->setNf(Nf);
    ampQQ->setNc(Nc);

    for (int i { start }; i < end; ++i) {
        std::cout
            << "Evaluating point " << i << " ..." << '\n'
            << "  try coeff(double)+specialFn(double)..." << '\n';

        std::vector<MOM<double>> psD(legs);
        for (int j { 0 }; j < legs; ++j) {
            psD[j] = MOM<double>(
                momenta[i][j][0], momenta[i][j][1],
                momenta[i][j][2], momenta[i][j][3]);
        }
        refineM(psD, psD, scales2);
        MOM<double> p2yD { psD[3] + psD[4] };
        double m2yD { std::sqrt(dot(p2yD, p2yD)) };

        t0 = std::chrono::high_resolution_clock::now();
        ampDD->setMuR2(std::pow(m2yD, 2));
        ampDD->setMomenta(psD);
        ampDD->setSpecFuncs();
        ampDD->initFinRem();
        ampDD->c1lx1l_fin();
        ampDD->c2lx1l_fin();
        t1 = std::chrono::high_resolution_clock::now();

        const double errD { std::abs(ampDD->c2lx1l_fin_error() / ampDD->c2lx1l_fin_value()) };

        {
            std::ofstream o("result.DD." + std::to_string(start), std::ios::app);
            o.setf(std::ios_base::scientific, std::ios_base::floatfield);
            o.precision(16);
            o
                << i << ' '
                << ampDD->c1lx1l_fin_value() << ' '
                << std::abs(ampDD->c1lx1l_fin_error() / ampDD->c1lx1l_fin_value()) << ' '
                << ampDD->c2lx1l_fin_value() << ' '
                << errD << ' '
                << std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() << ' '
                << '\n';
        }

        if (errD < cutoff_errD) {
            std::cout
                << "    stability test passed" << '\n';
        } else {
            std::cout
                << "    stability test failed" << '\n'
                << "  try coeff(quad)+specialFn(double)..." << '\n';

            dd_real errQ;

            std::vector<MOM<dd_real>> psQ(legs);
            for (int j { 0 }; j < legs; ++j) {
                psQ[j] = MOM<dd_real>(
                    momenta[i][j][0], momenta[i][j][1],
                    momenta[i][j][2], momenta[i][j][3]);
            }
            refineM(psQ, psQ, scales2);
            MOM<dd_real> p2yQ { psQ[3] + psQ[4] };
            dd_real m2yQ { sqrt(dot(p2yQ, p2yQ)) };

            t0 = std::chrono::high_resolution_clock::now();
            ampQD->setMuR2(pow(m2yQ, 2));
            ampQD->setMomenta(psQ);
            ampQD->copySpecFuncs(ampDD);
            ampQD->initFinRem();
            ampQD->c1lx1l_fin();
            ampQD->c2lx1l_fin();
            t1 = std::chrono::high_resolution_clock::now();

            errQ = abs(ampQD->c2lx1l_fin_error() / ampQD->c2lx1l_fin_value());

            {
                std::ofstream o("result.QD." + std::to_string(start), std::ios::app);
                o.setf(std::ios_base::scientific, std::ios_base::floatfield);
                o.precision(16);
                o
                    << i << ' '
                    << ampQD->c1lx1l_fin_value() << ' '
                    << abs(ampQD->c1lx1l_fin_error() / ampQD->c1lx1l_fin_value()) << ' '
                    << ampQD->c2lx1l_fin_value() << ' '
                    << errQ << ' '
                    << std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() << ' '
                    << '\n';
            }

            if (errQ < cutoff_errQ) {
                std::cout
                    << "    stability test passed" << '\n';

            } else {
                std::cout
                    << "    stability test failed" << '\n'
                    << "  try coeff(quad)+specialFn(quad)..." << '\n';

                t0 = std::chrono::high_resolution_clock::now();
                ampQQ->setMuR2(pow(m2yQ, 2));
                ampQQ->setMomenta(psQ);
                ampQQ->setSpecFuncs();
                ampQQ->initFinRem();
                ampQQ->copyCoeffs(ampQD);
                ampQQ->c1lx1l_fin();
                ampQQ->c2lx1l_fin();
                t1 = std::chrono::high_resolution_clock::now();

                errQ = abs(ampQQ->c2lx1l_fin_error() / ampQQ->c2lx1l_fin_value());

                {
                    std::ofstream o("result.QQ." + std::to_string(start), std::ios::app);
                    o.setf(std::ios_base::scientific, std::ios_base::floatfield);
                    o.precision(16);
                    o
                        << i << ' '
                        << ampQQ->c1lx1l_fin_value() << ' '
                        << abs(ampQQ->c1lx1l_fin_error() / ampQQ->c1lx1l_fin_value()) << ' '
                        << ampQQ->c2lx1l_fin_value() << ' '
                        << errQ << ' '
                        << std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() << ' '
                        << '\n';
                }

                if (errQ < cutoff_errQ) {
                    std::cout
                        << "    stability test passed" << '\n';
                } else {
                    std::cout
                        << "    stability test failed (END)" << '\n';
                }
            }
        }
    }
}

int main(int argc, char* argv[])
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    if (argc != 3 && argc != 4) {
        std::cerr << "Incorrect arguments!\n";
        std::exit(EXIT_FAILURE);
    }

    const std::string filename { argv[1] };
    const int start { std::atoi(argv[2]) },
              end { argc == 4 ? std::atoi(argv[3]) : start + 1 };
    Data<double> data(filename);

    if (end > static_cast<int>(data.momenta.size())) {
        std::cerr << "end is beyond length of momenta!\n";
        std::exit(EXIT_FAILURE);
    }

    // printMom(data.momenta);
    //for (double a : data.me)
    //    std::cout << a << '\n';

    //run(data.momenta, start, end);
    test<double>(data, start, end);
}
