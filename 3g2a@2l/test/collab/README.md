# collab

## calc DEPRECATED
* 3g2a stability test: calculate 2L results with scaling test over physical points from `points.txt` (1e5)

## test DEPRECATED
* compare against bare 1L `points2.txt` (10)

## test2 DEPRECATED
* compare against bare 1L `points_new.txt` (1e5)

## calc2
* 3g2a stability test: calculate 2L results with scaling test over physical points from `points_new.txt` (1e5)
* handles the additional information included in new data file

* bare MEs agree
* NJet prefactor is incomplete (TODO). Currently, for 1L prefactor, we have quark fractional EM charges and colour: `std::pow(11./9., 2) * Nc*(std::pow(Nc, 2)-1)`. Coupling should remain independent so can be run as desired, but will add loop/symmetry/average factors.
* NNLOJET prefactor = 
```
1 
    * (1/(Nc^2-1)/(Nc^2-1)/2/2)              colour-spin-average over incoming gluons                           TODO
    * (4*pi*alpha_strong(mu^2))^3            strong coupling
    * (4*pi*alpha)^2                         em coupling
    * Nc*(Nc^2-1)/4*32                       colour
    * (11/9)^2                               quark charges
    * (1/16/pi^2)^2                          loop factor                                                        TODO
    * 1/2                                    symmetry factor for the two final state identical photons          TODO
```

## test3
* compare against Matteo's timings (`f64` only) in `points_timing.txt`.
