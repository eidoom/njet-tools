#!/usr/bin/env bash

LCL=3g2A_phys_res
mkdir -p $LCL
cd $LCL

rm result.*.csv

RMT=files/research/3g2A2l/phys-fix
wget "https://www.ippp.dur.ac.uk/~rmoodie/$RMT/result.DD.csv"
wget "https://www.ippp.dur.ac.uk/~rmoodie/$RMT/result.QD.csv"
wget "https://www.ippp.dur.ac.uk/~rmoodie/$RMT/result.QQ.csv"

cd ..
