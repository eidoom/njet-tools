(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[1], f[2], f[3], f[4], f[5], f[6], f[7], f[8], f[9], f[10]}}, 
 {{1, 18, 2, 20, 21, 23, 34, 35, 37, 38, 39, 44, 105, 45, 46, 48, 51, 52, 64, 
   65, 67, 70, 71, 74, 78, 79, 106, 107, 82, 12, 93, 13, 15, 108, 109}}, 
 {{MySparseMatrix[{10, 35}, {m[1, 1] -> -2, m[2, 2] -> 2, m[2, 4] -> -2, 
     m[3, 2] -> -2, m[3, 14] -> 2, m[4, 3] -> 1, m[4, 5] -> -1, 
     m[4, 8] -> -1, m[4, 9] -> -1, m[4, 11] -> 1, m[4, 12] -> 1, 
     m[4, 15] -> -1, m[4, 16] -> 1, m[4, 17] -> 1, m[4, 18] -> -1, 
     m[4, 20] -> -1, m[4, 21] -> 1, m[4, 22] -> 1, m[4, 23] -> -1, 
     m[4, 24] -> 2, m[4, 25] -> -2, m[5, 3] -> 2, m[5, 5] -> -2, 
     m[5, 9] -> -2, m[5, 12] -> 2, m[5, 15] -> -2, m[5, 16] -> 2, 
     m[5, 22] -> 2, m[5, 23] -> -2, m[5, 24] -> 2, m[5, 25] -> -2, 
     m[5, 26] -> 2, m[5, 29] -> -2, m[6, 3] -> 2, m[6, 5] -> -2, 
     m[6, 15] -> -2, m[6, 16] -> 2, m[6, 24] -> 2, m[6, 26] -> 2, 
     m[6, 30] -> 1/3, m[7, 2] -> -2, m[7, 7] -> 2, m[7, 31] -> 2, 
     m[8, 2] -> 2, m[8, 10] -> -2, m[8, 31] -> -2, m[9, 2] -> 2, 
     m[9, 19] -> -2, m[9, 31] -> -2, m[10, 3] -> 1, m[10, 6] -> -1, 
     m[10, 9] -> -1, m[10, 13] -> 1, m[10, 17] -> 2, m[10, 18] -> -2, 
     m[10, 20] -> -2, m[10, 21] -> 2, m[10, 24] -> 2, m[10, 25] -> -2, 
     m[10, 27] -> 2, m[10, 28] -> -2, m[10, 32] -> -2, m[10, 33] -> -2, 
     m[10, 34] -> 2, m[10, 35] -> 2}], MySparseMatrix[{10, 35}, {}]}}, 
 {1, pflip}, {f[10] -> y[4]/(y[1]*y[2]*y[3]), 
  f[9] -> y[5]/(y[1]*y[2]*y[3]) + (y[2]*y[3]^2*y[5]^2*y[6]^2)/(y[1]*y[7]^2) + 
    (y[3]^2*y[5]*y[6]*y[8]*y[9])/(y[1]*y[7]) - y[5]/(y[1]*y[2]*y[3]*y[10]), 
  f[6] -> (y[3]^2*y[11])/y[1], f[1] -> (y[2]^2*y[3]^2*y[6])/y[1] - 
    (y[3]^2*y[5]*y[6]^2*y[8]*y[12])/(y[1]*y[7]*y[13]) + 
    (y[2]^2*y[3]^2*y[5]*y[6]*y[8])/(y[1]*y[13]*y[14]*y[15]*y[16]) + 
    (y[3]*y[5]*y[6]^2*y[8]*y[12]^3)/(y[1]*y[16]*y[17]) - 
    (y[3]*y[6]*y[19]*y[20])/(y[1]*y[15]*y[18]) + (y[3]*y[5]^3*y[6]^2)/
     (y[1]*y[21]) - (y[3]^2*y[6]^2*y[8]^3*y[12]^2)/(y[1]*y[22]), 
  f[4] -> (y[3]*y[23])/(y[1]*y[2]), 
  f[7] -> (y[2]*y[3]^2*y[5]^2*y[6]^2)/(y[1]*y[7]^2) + 
    (y[3]^2*y[5]*y[6]*y[8]*y[9])/(y[1]*y[7]) + 
    (y[2]*y[3]*y[6]^2*y[8]^2*y[12]^3)/(y[1]*y[17]^2) + y[24] - 
    (y[5]*y[6]*y[8]*y[12]^2*y[25])/(y[1]*y[17]), 
  f[5] -> (y[3]^2*y[26])/(y[1]*y[2]), 
  f[8] -> y[8]/(y[1]*y[2]) + (y[2]*y[3]*y[6]^2*y[8]^2*y[12]^3)/
     (y[1]*y[17]^2) - (y[5]*y[6]*y[8]*y[12]^2*y[25])/(y[1]*y[17]) + 
    y[8]/(y[1]*y[2]*y[27]), f[2] -> y[5]/(y[1]*y[2]*y[3]*y[10]) - 
    y[12]/(y[1]*y[2]*y[3]) - (y[2]*y[3]*y[5]^3*y[6]^2)/(y[1]*y[21]^2) + 
    (y[5]^2*y[6]*y[12]*y[28])/(y[1]*y[21]), 
  f[3] -> 1/(y[1]*y[2]) + (y[2]*y[3]^2*y[6]^2*y[8]^3*y[12]^2)/
     (y[1]*y[22]^2) + y[8]/(y[1]*y[2]*y[27]) + 
    (y[3]^2*y[6]*y[8]^2*y[12]*y[29])/(y[1]*y[22])}, 
 {y[1] -> ex[1], y[2] -> ex[2], y[3] -> ex[3], 
  y[4] -> 1 + 2*ex[3] + ex[2]*ex[3], y[5] -> 1 + ex[3] + ex[2]*ex[3], 
  y[6] -> ex[5], y[7] -> ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[2]*ex[4] - 
    ex[3]*ex[4] - ex[2]*ex[3]*ex[4] + ex[2]*ex[5], y[8] -> 1 + ex[2], 
  y[9] -> 3 + 2*ex[3], y[10] -> 1 + ex[2]*ex[3] - ex[3]*ex[4] + ex[3]*ex[5], 
  y[11] -> 2 + 2*ex[2] + ex[2]^2 + 4*ex[3] + 6*ex[2]*ex[3] + 
    2*ex[2]^2*ex[3] + 2*ex[3]^2 + 4*ex[2]*ex[3]^2 + 2*ex[2]^2*ex[3]^2, 
  y[12] -> 1 + ex[3], y[13] -> 1 + ex[2] - ex[5], y[14] -> 1 + ex[4] - ex[5], 
  y[15] -> -1 + ex[5], y[16] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - 
    ex[3]*ex[5], y[17] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - 
    ex[3]*ex[4] - ex[2]*ex[3]*ex[4] - ex[2]*ex[5], y[18] -> ex[4], 
  y[19] -> ex[2] + ex[5], y[20] -> ex[2]*ex[3] + ex[5] + ex[3]*ex[5], 
  y[21] -> -ex[2] - ex[2]*ex[3] + ex[4] + ex[3]*ex[4] + ex[2]*ex[3]*ex[5], 
  y[22] -> -(ex[2]*ex[3]) + ex[4] + ex[3]*ex[4] + ex[2]*ex[5] + 
    ex[2]*ex[3]*ex[5], y[23] -> 2 + 2*ex[2] + ex[2]^2, y[24] -> ex[1]^(-1), 
  y[25] -> -1 + 2*ex[3], y[26] -> 4 + 5*ex[2] + 2*ex[2]^2 + 5*ex[3] + 
    8*ex[2]*ex[3] + 3*ex[2]^2*ex[3] + 2*ex[3]^2 + 4*ex[2]*ex[3]^2 + 
    2*ex[2]^2*ex[3]^2, y[27] -> -1 + ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + 
    ex[5] + ex[3]*ex[5], y[28] -> -1 + 2*ex[3] + 2*ex[2]*ex[3], 
  y[29] -> 3 + 2*ex[3] + 2*ex[2]*ex[3]}}
