(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[1]}}, {{1}}, {{MySparseMatrix[{1, 1}, {m[1, 1] -> -2/3}], 
   MySparseMatrix[{1, 1}, {}]}}, {1, pflip}, 
 {f[1] -> (y[1]^3*y[2]^2*y[4]^2*y[5])/(y[3]*y[6]) - 
    (y[1]^3*y[2]^2*y[4]^2*y[8])/y[7]^2 - (y[1]^3*y[2]^2*y[4]^2*y[9])/
     (y[3]*y[6]*y[7])}, {y[1] -> ex[1], y[2] -> ex[2], y[3] -> 1 + ex[2], 
  y[4] -> ex[3], y[5] -> 1 + ex[3], y[6] -> 1 + ex[3] + ex[2]*ex[3], 
  y[7] -> 1 + ex[4] - ex[5], y[8] -> -1 + ex[5], 
  y[9] -> 2 + ex[2] + 2*ex[3] + 2*ex[2]*ex[3] - ex[5] - ex[3]*ex[5]}}
