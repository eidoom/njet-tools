(* ::Package:: *)

(* ::Section::Closed:: *)
(*Set up*)


SetAttributes[fundamentalDelta,Orderless];
Format[fundamentalDynkinIndex]:=Subscript["T","F"];
Format[A[a__]]:=Subscript["A",Length[List[a]]][a];
Format[fundamentalTrace[a__]]:=Subscript["tr","F"][a];
(*Format[dagger[a_]]:=Power[a,"\[ConjugateTranspose]"];*)
applyRules[expr_,rules_]:=FixedPoint[ExpandAll[#]/.rules&,expr];
rules={
	dagger[-1*a_]->-dagger[a],
	dagger[a_Integer]->a,
	dagger[I*a_]->-I*dagger[a],
	dagger[a___f*b___fundamentalTrace]->dagger[a]*dagger[b],
	dagger[a_T*b_T]->dagger[a]*dagger[b],
	dagger[T[a_,i_,j_]]->T[a,j,i],
	dagger[a_+ b_]:>dagger[a]+dagger[b],
	dagger[a_fundamentalTrace]:>Reverse[a],
	dagger[a_f]:>a,
	a_fundamentalTrace:>Module[
		{p=FirstPosition[a,Min[List@@a]][[1]]},
		Join[a[[p;;]],a[[;;p-1]]]
		]/;a[[1]]!=Min[List@@a],
	a_f:>(-I/fundamentalDynkinIndex)*(fundamentalTrace@@a-Reverse[fundamentalTrace@@a]),
	t_fundamentalTrace:>Times@@(Module[
		{i},
		T[a[t[[#]]],i[#],i[Mod[#+1,Length[t],1]]]&/@Range[Length[t]]
		]),
	T[a_,i_,j_]*T[b_,j_,i_]:>fundamentalDynkinIndex*adjointDelta[a,b],
	T[a_,i1_,i2_]*T[a_,i3_,i4_]:>
		fundamentalDynkinIndex(fundamentalDelta[i1,i4]*fundamentalDelta[i2,i3]
								-(1/Nc)*fundamentalDelta[i1,i2]*fundamentalDelta[i3,i4]),
	adjointDelta[i_,i_]:>Nc^2-1,
	adjointDelta[i1_,i2_]^2:>Nc^2-1,
	adjointDelta[i1_,i2_]*adjointDelta[i2_,i3_]:>adjointDelta[i1,i3],
	T[a1_,i1_,i2_]*adjointDelta[a1_,a2_]:>T[a2,i1,i2],
	fundamentalDelta[i_,i_]:>Nc,
	fundamentalDelta[i1_,i2_]^2:>Nc,
	fundamentalDelta[i1_,i2_]*fundamentalDelta[i2_,i3_]:>fundamentalDelta[i1,i3],
	T[a1_,i1_,i2_]*fundamentalDelta[i1_,i3_]:>T[a1,i3,i2],
	T[a1_,i1_,i2_]*fundamentalDelta[i2_,i3_]:>T[a1,i1,i3],
	T[a_,i_,i_]->0
	};
nonCyclicalPermutations[n_]:=Prepend[#,1]&/@Permutations[Range[2,n]];
qcd=fundamentalDynkinIndex->1/2;
export[expr_]:=CForm[expr/.{Nc^6->Nc6,Nc^5->Nc5,Nc^4->Nc4,Nc^3->Nc3,Nc^2->Nc2,Nc^(-2)->1/Nc2}];


fullFundamentalAmplitude[n_]:=Plus@@(fundamentalTrace@@#*A@@#&/@(nonCyclicalPermutations[n]));
reflectedFundamentalAmplitude[n_]:=Module[
	{amp1,amp2,amp3},
	amp1=fullFundamentalAmplitude[n];
	amp2=amp1/.a_A:>Power[-1,Length[a]]*Prepend[Reverse[a[[2;;]]],a[[1]]]/;a[[2]]>a[[-1]];
	amp3=Collect[amp2,A[a__]];
	Return[amp3];
	];
processAmplitude[amplitude_]:=Module[
	{partialAmplitudes},
	partialAmplitudes=DeleteDuplicates[Cases[amplitude,_A,Infinity]];
	Return[{Coefficient[amplitude,#]&/@partialAmplitudes,partialAmplitudes}];
	]; 
fundamentalColourMatrixNormalisation[n_]:=2*fundamentalDynkinIndex^n*Nc^(n-6)*(Nc^2-1);
fundamentalColourMatrix[fundamentalAmplitude_,norm_]:=Module[
	{
		len,
		coefficients,
		partialAmplitudesVector,
		matrix
		},
	{coefficients,partialAmplitudesVector}=processAmplitude[fundamentalAmplitude];
	len=Length[coefficients];
	matrix=applyRules[
		Table[coefficients[[i1]]*dagger[coefficients[[i2]]],{i1,len},{i2,len}],
		rules
		];
	Return[{
		Expand[Factor/@(matrix/norm)],
		partialAmplitudesVector
		}];
	];
fullFundamentalColourMatrix[n_]:=fundamentalColourMatrix[fullFundamentalAmplitude,n];
reflectedFundamentalColourMatrix[n_]:=fundamentalColourMatrix[reflectedFundamentalAmplitude,n];
generateNJetColMat[amp_,norm_]:=Module[
	{len,sol,mat,vec,size,facs,facRep,matRepd},
	Print[amp];
	len=Length[Cases[amp,_A,Infinity]];
	If[len==1,
		sol=fundamentalColourMatrix[amp,norm]/.qcd;,
		Print[norm/.qcd];
		sol=fundamentalColourMatrix[amp,norm];
		];
	Print[MatrixForm/@sol];
	mat=sol[[1]];
	vec=sol[[2]];
	Print[(#-1&/@vec[[#]])&/@Range[len]];
	size=Length[mat[[1]]];
	facs=DeleteDuplicates[Flatten[mat]];
	facRep=facs[[#]]->#-1&/@Range[Length[facs]];
	matRepd=mat/.facRep;
	If[len!=1,
		Print["Normalisation"];
		Print[export[norm/.qcd]];
		Print["Colour matrix"];
		Print[MatrixForm[matRepd]];
(*
		Print["Upper triangle"];
		Print[Flatten[Table[Table[matRepd[[i,j]],{i,j,size}],{j,size}]]];
*)
		Print["Lower triangle"];
		Print[DeleteCases[Flatten[Table[Table[If[i<=j,matRepd[[i,j]],Null],{i,size}],{j,size}]],Null]];
		];
	Print["Element values"];
	Print[export/@facs];
	];


(* ::Section:: *)
(*Run*)


(* 2g2A colour matrix *)
generateNJetColMat[fullFundamentalAmplitude[2],1]


generateNJetColMat[reflectedFundamentalAmplitude[3],1]


(* 3g2A colour matrix *)
generateNJetColMat[fullFundamentalAmplitude[3],fundamentalColourMatrixNormalisation[3]/2*Nc^2]


generateNJetColMat[reflectedFundamentalAmplitude[3]*f[3,4,5],1]


(* 3g2A colour correlation, but only one of them *)
generateNJetColMat[fullFundamentalAmplitude[3]*f[3,4,5],fundamentalColourMatrixNormalisation[4]]


generateNJetColMat[reflectedFundamentalAmplitude[4],fundamentalColourMatrixNormalisation[4]]


(* 4g2A colour matrix *)
generateNJetColMat[fullFundamentalAmplitude[4],fundamentalColourMatrixNormalisation[4]/2]


(* g2gg gluon loop colour matrix *)
applyRules[f[a,b,c]*f[b,d,f]*f[d,a,e]*f[f,e,c],rules]/.qcd
%/Nc//Simplify


applyRules[f[a,b,c]*T[a[a],ii,jj]*T[a[b],kk,ii]*T[a[c],jj,kk],rules]/.qcd//Simplify


(* g2qqg tree colour matrix *)
AA=Module[{k1},T[a[0],i1,k1]*T[a[3],k1,i2]];
BB=Module[{k2},T[a[3],i1,k2]*T[a[0],k2,i2]];
g2qqgPF=(Nc^2-1)/4/Nc
(applyRules[AA*dagger[AA],rules]/.qcd)/g2qqgPF//Simplify
(applyRules[AA*dagger[BB],rules]/.qcd)/g2qqgPF//Simplify
(applyRules[BB*dagger[AA],rules]/.qcd)/g2qqgPF//Simplify
(applyRules[BB*dagger[BB],rules]/.qcd)/g2qqgPF//Simplify


(* q2qg 1L colour matrix *)
AA=Module[{k1},T[a[1],i1,k1]*T[a[3],k1,i2]*f[1,3,2]];
BB=Module[{b,k1,k2},T[b[1],i1,k1]*T[a[2],k1,k2]*T[b[1],k2,i2]];
g2qqgPF=1/4
(applyRules[AA*dagger[AA],rules]/.qcd)/g2qqgPF//Simplify
(applyRules[AA*dagger[BB],rules]/.qcd)/g2qqgPF//Simplify
(applyRules[BB*dagger[AA],rules]/.qcd)/g2qqgPF//Simplify
(applyRules[BB*dagger[BB],rules]/.qcd)/g2qqgPF//Simplify


2*applyRules[dagger[T[a[2],i1,i2]] T[a[2],i1,i2],rules]/.qcd//Simplify
2*applyRules[( dagger[T[a[2],i1,i2]] T[a[2],i1,i2])/Nc^2,rules]/.qcd//Simplify


(* EOF *)
