#!/usr/bin/env bash

# ../conv-form-y.sh ys.m && tform ys.frm
# ../conv-form-f.sh fs.m && tform fs.frm
# ../conv-form-eps-fast.sh eps-fast.m && tform eps-fast.frm
# ../conv-form-eps.sh eps.m && tform eps.frm

./pp-form.sh ys.c
./pp-form.sh fs.c
# ./pp-form.sh eps-fast.c
./pp-form.sh eps.c

INT=int.cpp
OUT=end.cpp

cat template_start.cpp > ${OUT}

i=`cat *.tmp | sort -n | tail -1`
echo "std::array<TreeValue, ${i}> Z;" >>${OUT}

cat ys.cpp >> ${OUT}
cat fs.cpp >> ${OUT}
cat eps.cpp >> ${OUT}
# cat eps-fast.cpp >> ${OUT}

cat template_end.cpp >> ${OUT}

cp -f ${OUT} ${INT}

# for n in $(seq 1 $i); do
#     m=$((n - 1))
#     perl -pi -e "s|\[$n\]|[$m]|g" ${OUT}
# done

clang-format -i ${OUT}
