#!/usr/bin/env bash

l="
07
11
13
14
19
21
22
25
26
28
"

for li in ${l[@]};do 
    echo "template <typename T>
std::array<typename NJetAmp5<T>::DoubleLoopValue, 12> NJetAmp5<T>::hA2gP${li}()
{
    return std::array<DoubleLoopValue, 12>();
}
"
done

for li in ${l[@]};do 
    echo "using BaseClass::hA2gP${li};"
done

for li in ${l[@]};do 
    echo "virtual std::array<DoubleLoopValue, 12> hA2gP${li}();"
done

for li in ${l[@]};do 
    echo "virtual std::array<DoubleLoopValue, 12> hA2gP${li}() override;"
done

for li in ${l[@]};do 
    echo "template <typename T>
std::array<typename Amp0q5g_a<T>::DoubleLoopValue, 12> Amp0q5g_a<T>::hA2gP${li}()
{
    return std::array<DoubleLoopValue, 12>();
}
"
done
