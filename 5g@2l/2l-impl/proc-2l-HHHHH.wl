(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/2l-impl/HHHHH"];


data=Get["../P2_HHHHH_lr_apart.m"];


epsQuin2[o_]:=Module[{one},
one=(((Plus@@(data[[1,o,#]]/eps^(5-#)&/@Range[5])))//Series[#,{eps,0,0}]&)//Normal;
inveps[Abs[#],o]->Coefficient[one,eps,#]&/@Range[0,-4,-1]
]


res2=epsQuin2/@Range[12];


Export["eps.m",res2];
Export["fs.m",data[[2]]];
Export["ys.m",data[[3]]];
