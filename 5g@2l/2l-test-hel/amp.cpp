#include <array>
#include <cassert>
#include <chrono>
#include <complex>
#include <iostream>
#include <random>
#include <vector>

#include "analytic/0q5g-2l-analytic.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

#include "Hel.hpp"
//#include "PhaseSpace.hpp"

const int N { 5 };

template <typename T>
void runA(const Helicity<N> hels, Amp0q5g_a2l<T>& amp)
{
    amp.setHelicity(hels.data());

    auto t0 { std::chrono::high_resolution_clock::now() };
    std::array<LoopResult<Eps5o2<T>>, 12> amps { amp.ALe2() };
    auto t1 { std::chrono::high_resolution_clock::now() };
    auto t2 { std::chrono::high_resolution_clock::now() };
    std::array<LoopResult<Eps5o2<T>>, 12> pol { amp.hAg28e2_pol() };
    std::array<LoopResult<Eps5o2<T>>, 12> fin { amp.hAg28e2_fin() };
    auto t3 { std::chrono::high_resolution_clock::now() };

    std::array<std::complex<T>, 12> trees { {
        amp.A0(0, 1, 2, 3, 4),
        amp.A0(0, 3, 4, 2, 1),
        amp.A0(0, 4, 2, 3, 1),
        amp.A0(0, 1, 3, 4, 2),
        amp.A0(0, 1, 4, 2, 3),
        amp.A0(0, 2, 3, 4, 1),
        amp.A0(0, 2, 1, 3, 4),
        amp.A0(0, 3, 4, 1, 2),
        amp.A0(0, 2, 3, 1, 4),
        amp.A0(0, 3, 1, 4, 2),
        amp.A0(0, 4, 2, 1, 3),
        amp.A0(0, 4, 1, 2, 3),
    } };

    //std::array<LoopResult<T>, 12> old_amps { amp.AL_penta() };

    for (int i { 0 }; i < 12; ++i) {
	Eps5o2<T> nw{pol[i].loop / trees[i]};
        std::cout << "pol_" << i << ": " << nw << '\n';
    }
    std::cout << '\n';

    for (int i { 0 }; i < 12; ++i) {
	Eps5o2<T> nw{fin[i].loop / trees[i]};
        std::cout << "fin_" << i << ": " << nw << '\n';
    }
    std::cout << '\n';

    for (int i { 0 }; i < 12; ++i) {
        std::cout << "ALe2_" << i << ": " << amps[i].loop / trees[i] << '\n';
	Eps5o2<T> nw{(fin[i].loop + pol[i].loop) / trees[i]};
        std::cout << "new_" << i << ":  " << nw << '\n';
        //std::cout << "ALe2: " << amps[i].loop.get2() << '\n';
        //std::cout << "AL: " << old_amps[i].loop.get2() << '\n';
    }
    std::cout << '\n';

    //std::array<LoopResult<T>, 12> old_amps { {
    //    amp.AL(0, 1, 2, 3, 4),
    //    amp.AL(0, 3, 4, 2, 1),
    //    amp.AL(0, 4, 2, 3, 1),
    //    amp.AL(0, 1, 3, 4, 2),
    //    amp.AL(0, 1, 4, 2, 3),
    //    amp.AL(0, 2, 3, 4, 1),
    //    amp.AL(0, 2, 1, 3, 4),
    //    amp.AL(0, 3, 4, 1, 2),
    //    amp.AL(0, 2, 3, 1, 4),
    //    amp.AL(0, 3, 1, 4, 2),
    //    amp.AL(0, 4, 2, 1, 3),
    //    amp.AL(0, 4, 1, 2, 3),
    //} };

    // for (int i { 0 }; i < 12; ++i) {
    //     std::cout << "ALe2[/e2]/A0: " << amps[i].loop.get2() / trees[i] << '\n';
    //std::cout << "AL[/e2]/A0: " << old_amps[i].loop.get2() / trees[i] << '\n';
    //std::cout << "ALe2[/e2]/AL[/e2]: " << amps[i].loop.get2() / old_amps[i].loop.get2() << '\n';
    // }

    std::cout << "Old evaluation time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count() << "ms\n";
    std::cout << "New evaluation time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t3 - t2).count() << "ms\n";

    std::cout << '\n';
}

template <typename T>
void runA2(const Helicity<N> hels, Amp0q5g_a2l<T>& amp)
{
    amp.setHelicity(hels.data());

    std::array<LoopResult<Eps5<T>>, 12> amps { amp.A2L() };

    //for (int i { 0 }; i < 12; ++i) {
    //    std::cout << "ALe2: " << amps[i].loop << '\n';
    //}

    std::array<std::complex<T>, 12> trees { {
        amp.A0(0, 1, 2, 3, 4),
        amp.A0(0, 3, 4, 2, 1),
        amp.A0(0, 4, 2, 3, 1),
        amp.A0(0, 1, 3, 4, 2),
        amp.A0(0, 1, 4, 2, 3),
        amp.A0(0, 2, 3, 4, 1),
        amp.A0(0, 2, 1, 3, 4),
        amp.A0(0, 3, 4, 1, 2),
        amp.A0(0, 2, 3, 1, 4),
        amp.A0(0, 3, 1, 4, 2),
        amp.A0(0, 4, 2, 1, 3),
        amp.A0(0, 4, 1, 2, 3),
    } };

    // std::array<LoopResult<T>, 12> old_amps { {
    //     amp.AL(0, 1, 2, 3, 4),
    //     amp.AL(0, 3, 4, 2, 1),
    //     amp.AL(0, 4, 2, 3, 1),
    //     amp.AL(0, 1, 3, 4, 2),
    //     amp.AL(0, 1, 4, 2, 3),
    //     amp.AL(0, 2, 3, 4, 1),
    //     amp.AL(0, 2, 1, 3, 4),
    //     amp.AL(0, 3, 4, 1, 2),
    //     amp.AL(0, 2, 3, 1, 4),
    //     amp.AL(0, 3, 1, 4, 2),
    //     amp.AL(0, 4, 2, 1, 3),
    //     amp.AL(0, 4, 1, 2, 3),
    // } };

    for (int i { 0 }; i < 12; ++i) {
        std::cout << "A2L[/e4]/A0: " << amps[i].loop.get4() / trees[i] << '\n';
        // std::cout << "ALe2/AL: " << amps[i].loop / old_amps[i].loop << '\n';
    }
    std::cout << '\n';
}

template <typename T>
void runC(const Helicity<N> hels, Amp0q5g_a2l<T>& amp)
{
    // Eps5<T> virtsq { amp.virtsq_penta(hels.data()) }; // e4,3 virtsq=virtsqe2

    auto t9 { std::chrono::high_resolution_clock::now() };
    Eps5<T> virtsqe2 { amp.virtsqe2(hels.data()) };
    auto t10 { std::chrono::high_resolution_clock::now() };

    auto t1 { std::chrono::high_resolution_clock::now() };
    T born { amp.born(hels.data()) };
    auto t2 { std::chrono::high_resolution_clock::now() };

    std::cout << "colour sum\n"
              // << "B:        " << born << '\n'
              // << "Time:     " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << "us\n"
              // << "V2:       " << virtsqe2 << '\n'
              // << "Time:     " << std::chrono::duration_cast<std::chrono::milliseconds>(t10 - t9).count() << "ms\n"
              // << "V2e0:     " << virtsq << '\n'
              << "V2[/e4]/B: " << virtsqe2.get4().real() / born << '\n';
}

template <typename T>
void runC2(const Helicity<N> hels, Amp0q5g_a2l<T>& amp)
{
    // auto t3 { std::chrono::high_resolution_clock::now() };
    // Eps3<T> virt { amp.virt_penta(hels.data()) };
    // auto t4 { std::chrono::high_resolution_clock::now() };

    auto t7 { std::chrono::high_resolution_clock::now() };
    Eps5<T> dblvirt { amp.dblvirt(hels.data()) };
    auto t8 { std::chrono::high_resolution_clock::now() };

    auto t1 { std::chrono::high_resolution_clock::now() };
    T born { amp.born(hels.data()) };
    auto t2 { std::chrono::high_resolution_clock::now() };

    std::cout << "colour sum\n"
              // << "B:        " << born << '\n'
              // << "Time:     " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << "us\n"
              // << "V:        " << virt << '\n'
              // << "Time:     " << std::chrono::duration_cast<std::chrono::microseconds>(t4 - t3).count() << "us\n"
              // << "2V:       " << dblvirt << '\n'
              // << "Time:     " << std::chrono::duration_cast<std::chrono::milliseconds>(t8 - t7).count() << "ms\n"
              << "2V[/e4]/B: " << dblvirt.get4().real() / born << '\n';
}

template <typename T>
void runH(Amp0q5g_a2l<T>& amp)
{
    auto t9 { std::chrono::high_resolution_clock::now() };
    Eps5<T> dblvirtH { amp.dblvirt() };
    auto t10 { std::chrono::high_resolution_clock::now() };

    auto t7 { std::chrono::high_resolution_clock::now() };
    Eps5<T> virtsqe2H { amp.virtsqe2() };
    auto t8 { std::chrono::high_resolution_clock::now() };

    //auto t5 { std::chrono::high_resolution_clock::now() };
    //Eps5<T> virtsqH { amp.virtsq_penta() };
    //auto t6 { std::chrono::high_resolution_clock::now() };

    auto t1 { std::chrono::high_resolution_clock::now() };
    Eps3<T> virtH { amp.virt_penta() };
    auto t2 { std::chrono::high_resolution_clock::now() };

    auto t3 { std::chrono::high_resolution_clock::now() };
    T bornH { amp.born() };
    auto t4 { std::chrono::high_resolution_clock::now() };

    std::cout << "Helicity sum:\n"
              << "B:       " << bornH << '\n'
              << "Time:    " << std::chrono::duration_cast<std::chrono::microseconds>(t4 - t3).count() << "us\n"
              << "V:       " << virtH << '\n'
              << "Time:    " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << "us\n"
              //<< "V2:      " << virtsqH << '\n'
              //<< "Time:    " << std::chrono::duration_cast<std::chrono::milliseconds>(t6 - t5).count() << "ms\n"
              << "V2e2:    " << virtsqe2H << '\n'
              << "Time:    " << std::chrono::duration_cast<std::chrono::milliseconds>(t8 - t7).count() << "ms\n"
              << "2V:      " << dblvirtH << '\n'
              << "Time:    " << std::chrono::duration_cast<std::chrono::milliseconds>(t10 - t9).count() << "ms\n";

    std::cout
        << "2V[/e4]/B: " << dblvirtH.get4().real() / bornH << '\n'
        << "V2e2[/e4]/B: " << virtsqe2H.get4().real() / bornH << '\n';
}

template <typename T>
void loop_matrix(const int num_ps = 10, const int num_nc = 1, const int num_mur = 1)
{
    //std::random_device dev;
    //std::mt19937_64 rng(dev());
    //const double boundary { 0.01 };
    //std::uniform_real_distribution<double> dist_y1(boundary, 1. - boundary);
    //std::uniform_real_distribution<double> dist_a(boundary, M_PI - boundary);

    //double y1;
    //double y2;
    //double theta;
    //double alpha;

    const int legs { 5 };
    // const T scale_test_factor { 1. };
    const double sqrtS { 1. };

    std::vector<Flavour<double>> flavours(legs, StandardModel::G());

    for (int i { 0 }; i < num_ps; ++i) {

        //y1 = dist_y1(rng);
        //std::uniform_real_distribution<double> dist_y2(boundary, 1. - y1 - boundary);
        //y2 = dist_y2(rng);
        //theta = dist_a(rng);
        //alpha = dist_a(rng);
        //std::cout << y1 << " "
        //          << y2 << " "
        //          << theta << " "
        //          << alpha << " "
        //          << '\n';

        //y1 = 2.5466037534799052e-01;
        //y2 = 3.9999592899158604e-01;
        //theta = 1.2398476487204491e+00;
        //alpha = 4.3891163982516607e-01;

        //const std::array<MOM<T>, N> moms { phase_space_point(static_cast<T>(y1), static_cast<T>(y2), static_cast<T>(theta), static_cast<T>(alpha)) };

        PhaseSpace<T> ps(legs, flavours.data(), i + 1, sqrtS);
        const std::vector<MOM<T>> momenta { ps.getPSpoint() };
        ps.showPSpoint();

        for (int Nc { 3 }; Nc < 3 * std::pow(10, num_nc); Nc *= 10) {
            //std::cout << "Nc=" << Nc;
            for (T mur { 1. }; mur < std::pow(2., num_mur); mur *= 2.) {

                const T scale { 1. }; // scale factor for scaling test
                Amp0q5g_a2l<T> amp(scale);
                amp.setNf(0);
                amp.setNc(Nc);
                amp.setMomenta(momenta);
                auto t5 { std::chrono::high_resolution_clock::now() };
                amp.setpenta2l();
                auto t6 { std::chrono::high_resolution_clock::now() };
                std::cout << "\nPentagon evaluation time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t6 - t5).count() << "ms\n";
                //std::cout << "MuR=" << mur << '\n';
                amp.setMuR2(mur * mur);
                //std::cout << "MuR2=" << log(amp.MuR2()) << '\n';
                std::cout << "MuR2=" << mur * mur << '\n';

                //for (int i0 { -1 }; i0 < 2; i0 += 2) {
                //    for (int i1 { -1 }; i1 < 2; i1 += 2) {
                //        for (int i2 { -1 }; i2 < 2; i2 += 2) {
                //            for (int i3 { -1 }; i3 < 2; i3 += 2) {
                //                for (int i4 { -1 }; i4 < 2; i4 += 2) {
                //                    const Helicity<N> hels { { i4, i3, i2, i1, i0 } };
                const Helicity<N> hels { { -1, -1, 1, 1, 1 } };
                //                    if (hels.order() == 1 && !hels.more_plus()) {
                std::cout << hels << '\n';

                runA<T>(hels, amp);
                //                        //runC2<T>(hels, amp);
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}

                std::cout << '\n';

                // std::cout << '\n';
                // for (int i { 0 }; i < N; ++i) {
                //     for (int j { i + 1 }; j < N; ++j) {
                //         std::cout << "s" << i + 1 << j + 1 << "=" << dot(mom[i], mom[j]) * 2 << '\n';
                //     }
                // }

                // std::cout << "{" << '\n';
                // for (int j { 0 }; j < N - 1; ++j) {
                //     std::cout << moms[j] << ',' << '\n';
                // }
                // std::cout << moms[N - 1] << "\n},\n";

                // runH<T>(amp);
            }
        }
    }
}

int main()
{
    std::cout.precision(8);
    std::cout.setf(std::ios_base::scientific);

    loop_matrix<double>(1, 1, 1);
}
