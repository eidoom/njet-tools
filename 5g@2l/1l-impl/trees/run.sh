#!/usr/bin/env bash

math -script trees.wl

./conv-form.sh trees.m

tform trees.frm

./pp-form.sh trees.c

clang-format -i trees.cpp
