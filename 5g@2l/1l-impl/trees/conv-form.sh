#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
FRM=${BASE}.frm
C=${BASE}.c
Z=${BASE}.tmp

echo -e "#-\nS x1,x2,x3,x4,x5;" >$FRM
echo "ExtraSymbols,array,Z;" >>$FRM

perl \
    -0777p \
    -e "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|ex\[(\d)\]|x\1|g;
        s|\{?(a\d{2} )->( .*?)[\},][\s\n]*|L \1=\2;\n|gs;
        " \
    ${IN} >>${FRM}

echo -e "\n.sort\nFormat O4;\nFormat C;\n" >>$FRM

as=$(rg -o "a\d{2}" $FRM)

for a in ${as[@]}; do
    echo "#optimize $a" >>$FRM
    echo "#write <$C> \"\ntemplate <typename T>\ntypename Amp0q5g_a<T>::TreeValue Amp0q5g_a<T>::hA0${a#a}X()\n{\"" >>$FRM
    echo "#write <$C> \"std::array<TreeValue, \`optimmaxvar_'> Z;\"" >>$FRM
    echo "#write <$C> \"%O\"" >>$FRM
    echo "#write <$C> \"return %E;\", ${a}" >>$FRM
    echo -e "#write <$C> \"}\"\n" >>$FRM
done

echo ".end" >>$FRM
