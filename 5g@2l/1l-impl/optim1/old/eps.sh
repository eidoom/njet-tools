#!/usr/bin/env bash

# use on FORM processed file

OUT=$1

echo -e "std::array<LoopValue, 12> amps{" >${OUT}

for i in {1..11}; do
    echo -e "LoopValue(ie_0_$i, ie_1_$i, ie_2_$i)," >>${OUT}
done
i=12
echo -e "LoopValue(ie_0_$i, ie_1_$i, ie_2_$i)" >>${OUT}

echo -e "};" >>${OUT}
