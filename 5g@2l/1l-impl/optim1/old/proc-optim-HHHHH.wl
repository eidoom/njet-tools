(* ::Package:: *)

SetDirectory[NotebookDirectory[]];


data=Get["../P1_HHHHH_lr_apart.m"];


epsTrip[o_]:=Module[{one},
one=(((Plus@@(data[[1,o,#]]/eps^(3-#)&/@Range[3])))//Series[#,{eps,0,0}]&)//Normal;
Coefficient[one,eps,#]&/@Range[0,-2,-1]
]


res=epsTrip/@Range[12];


all=Join[data[[2;;3]]//Reverse,{res}];


Export["all.m",all//.{Power[a_,b_]->pow[a,b]}];
