(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/1l-impl/optim1/mpmpp"];


data=Get["../P1_m+m++_lr_apart.m"];


epsTrip[o_]:=Module[{one},
one=(((Plus@@(data[[1,o,#]]/eps^(3-#)&/@Range[3])))//Series[#,{eps,0,0}]&)//Normal;
Coefficient[one,eps,#]&/@Range[0,-2,-1]
]


res=epsTrip/@Range[12];


all=Join[data[[2;;3]]//Reverse,{res}];


Export["all.m",all//.{Power[a_,b_]->pow[a,b]}]; (* no FORM *)


epsTrip2[o_]:=Module[{one},
one=(((Plus@@(data[[1,o,#]]/eps^(3-#)&/@Range[3])))//Series[#,{eps,0,0}]&)//Normal;
inveps[Abs[#],o]->Coefficient[one,eps,#]&/@Range[0,-2,-1]
]


res2=epsTrip2/@Range[12];


all2=Join[data[[2;;3]]//Reverse,{res2}];


Export["all.m",all2]; (* FORM *)
