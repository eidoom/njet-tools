(* ::Package:: *)

SetDirectory[NotebookDirectory[]];


mmppp=Get["../P1_mm+++.m"];


o=1;


mmppp[[o,1]]


mmppp[[o,2]]


mmppp[[o,3]]


mmppp[[o+1]]


correction=(1+eps^2*Pi^2/12)*(MuR2)^eps;


Series[correction,{eps,0,2}]


epsTrip[o_]:=Module[{one},
one=(((Plus@@(mmppp[[2*(o-1)+1,#]]/eps^(3-#)&/@Range[3])*correction)//Simplify)//Series[#,{eps,0,0}]&)//Normal;
Coefficient[one,eps,#]&/@Range[0,-2,-1]
]


res=epsTrip/@Range[12];


Export["epsTrip.m",res];


rules=mmppp[[2*#]]&/@Range[12];


all=Transpose[{rules,res}]


Export["all.m",all//.{Power[a_,b_]->pow[a,b]}];


Flatten[mmppp//.{Plus->List,Times->List}];
Select[%,MatchQ[#,_F]&]//DeleteDuplicates;
%//Sort
Export["pentas.m",%];
