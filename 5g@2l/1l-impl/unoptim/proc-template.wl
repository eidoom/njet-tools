(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/1l-impl/1L_5g_analyticpartialamps/HHHHH"];


data=Get["../P1_HHHHH.m"];


correction=(1+eps^2*5*Pi^2/12)*(MuR2)^eps;


epsTrip[o_]:=Module[{one},
one=(((Plus@@(data[[2*(o-1)+1,#]]/eps^(3-#)&/@Range[3])*correction)//Simplify)//Series[#,{eps,0,0}]&)//Normal;
Coefficient[one,eps,#]&/@Range[0,-2,-1]
]


res=epsTrip/@Range[12];


rules=data[[2*#]]&/@Range[12];


all=Transpose[{rules,res}]


Export["all.m",all//.{Power[a_,b_]->pow[a,b]}];
