#include <iostream>
#include <vector>

#include "ngluon2/Mom.h"

#include "PhaseSpace.hpp"

template <typename T>
void run(const int rseed)
{
    const double sqrtS { 1. };
    const std::vector<MOM<T>> momenta { njet_random_point<T>(rseed, sqrtS) };

    export_momenta(momenta);
    std::cout << '\n';

    // print_invariant_ratios(momenta);
    // std::cout << '\n';

    // print_spurious_poles(momenta);
    // std::cout << '\n';
}

int main(int argc, char* argv[])
{
    assert(argc == 2);
    const int rseed { std::atoi(argv[1]) };

    std::cout << '\n';

    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(18);

    std::cout << "rseed = " << rseed << '\n' << '\n';

    run<dd_real>(rseed);

    std::cout << '\n';
}
