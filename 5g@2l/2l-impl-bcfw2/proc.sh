#!/usr/bin/env bash

# ../conv-form-y.sh ys.m && tform ys.frm
# ../conv-form-f.sh fs.m && tform fs.frm
# ../conv-form-eps.sh eps.m && tform eps.frm
# ../conv-form-eps-fast.sh eps-fast.m && tform eps-fast.frm

# ./pp-form.sh ys.c
# ./pp-form.sh fs.c
# ./pp-form.sh eps.c
# for i in {1..12}; do
#     ./pp-form.sh e${i}_even.c
#     clang-format -i e${i}_even.cpp &
#     ./pp-form.sh e${i}_odd.c
#     clang-format -i e${i}_odd.cpp &
# done
# ./pp-form.sh eps-fast.c

# INT_R=int_coeffs.cpp
# COEFFS=coeffs.cpp
# cat template_coeffs_start.cpp > ${COEFFS}
# cat ys.cpp >> ${COEFFS}
# cat fs.cpp >> ${COEFFS}
# cat template_coeffs_end.cpp >> ${COEFFS}
# cp -f ${COEFFS} ${INT_R}

for i in {1..12}; do
    FILE=eps_p${i}.cpp
    perl -p -e "s|HH|28|g; s|PP|$i|g;" template_perm_start.cpp >${FILE}
    cat e${i}_odd.cpp >>${FILE}
    perl -p -e "s|HH|28|g; s|PP|$i|g;" template_perm_middle.cpp >>${FILE}
    cat e${i}_even.cpp >>${FILE}
    cat template_perm_end.cpp >>${FILE}
    clang-format -i ${FILE} &
done

# for n in $(seq 1 $i); do
#     m=$((n - 1))
#     perl -pi -e "s|\[$n\]|[$m]|g" ${COEFFS}
# done

# for n in $(seq 1 $j); do
#     m=$((n - 1))
#     perl -pi -e "s|\[$n\]|[$m]|g" ${VAL}
# done


# clang-format -i ${COEFFS}
# clang-format -i ${VAL}
