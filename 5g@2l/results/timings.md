# timings pre SM alloc

## 100 amp D/D -O1
100 points, amp class, D/D, -O1
Compiled on ws10 in under 10 minutes

Init time (mainly filling sparse matrices):  5521ms
Averages per PS point:
SFval+SFmon time:  973ms
virt time:    722us
virtsq time:  1us
dblvirt time: 80ms

and with individual timings for SFvals and SFmons:

Init time (mainly filling sparse matrices):  4183ms
SFval time:  968ms
SFmon time:  60us
virt time:    634us
virtsq time:  1us
dblvirt time: 94ms

## 100 amp D/D -O2
100 points, amp class, D/D, -O2
Similar compile time to -O1

Init time (mainly filling sparse matrices):  5758ms
Averages per PS point:
SFval+SFmon time:  1006ms
virt time:    618us
virtsq time:  1us
dblvirt time: 78ms

## 100 amp D/D -Os
100 points, amp class, D/D, -Os
Similar compile time to -O1

Init time (mainly filling sparse matrices):  4461ms
SFval+SFmon time:  1019ms
virt time:    743us
virtsq time:  1us
dblvirt time: 99ms

# timings post SM alloc

## 100 amp D/D -O1
100 points, amp class, D/D, -O1
Still compiles on ws10 in under 10 minutes

Init time (mainly filling sparse matrices):  286ms
SFeval time:  974ms
SFmons time:  81us
virt time:    618us
virtsq time:  1us
dblvirt time: 77ms

## 100 amp D/D -Os
100 points, amp class, D/D, -Os
Similar compile time to -O1

Init time (mainly filling sparse matrices):  525ms
SFeval time:  968ms
SFmons time:  56us
virt time:    641us
virtsq time:  1us
dblvirt time: 95ms

# sizes
## sizes -O1/s (same)
336K    lib/libnjet2an0q5g2vppmpp.so.0.0.0
336K    lib/libnjet2an0q5g2vppppp.so.0.0.0
344K    lib/libnjet2an0q5g2vmpppp.so.0.0.0
344K    lib/libnjet2an0q5g2vpmppp.so.0.0.0
344K    lib/libnjet2an0q5g2vpppmp.so.0.0.0
344K    lib/libnjet2an0q5g2vppppm.so.0.0.0
704K    lib/libnjet2an0q5g2l.so.0.0.0
26M     lib/libnjet2an0q5g2vpmmpp.so.0.0.0
68M     lib/libnjet2an0q5g2vpppmm.so.0.0.0
71M     lib/libnjet2an0q5g2vpmpmp.so.0.0.0
72M     lib/libnjet2an0q5g2vpmppm.so.0.0.0
73M     lib/libnjet2an0q5g2vppmmp.so.0.0.0
75M     lib/libnjet2an0q5g2vppmpm.so.0.0.0

699MB  lib/

# compiile times
-O1 on everything: less than 1 hour on ws10

# bad PS points
cutoff=1e-3

660 bad points in 100k: 0.66%
460 passed qd: 69.7%
120 passed qq: 18.2%
80 failed: 12.1%

Random point timing:
coeffs(double): 409 ms
special(double): 3.7 s
coeffs(quad): 16.1 s
special(quad): 3.3 mins

Average times:
coeffs(double): 373.8 ms
special(double): 4.5 s
coeffs(quad): 13.9 s
special(quad): 7.0 mins
