#!/usr/bin/env bash

in=$1
out=`basename $1 .csv`_num.csv

perl -p -e "s|DD|1|g" $in > $out
perl -pi -e "s|QD|2|g" $in
perl -pi -e "s|QQ|3|g" $in
perl -pi -e "s|FAIL|4|g" $in
