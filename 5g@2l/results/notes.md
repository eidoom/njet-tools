---
title: 5g2L
author: Ryan Moodie
date: 6 Feb 2021
---

# Stability
Show relative error over 10,000 random points for each contribution.
Bin errors to make histograms.
x-axis shows exponent of error in scientific notation.

## Born
![](born_err_histo.pdf)

## Virt
![](virt_err_histo.pdf)

## Virtsq
![](virtsq_err_histo.pdf)

## Dblvirt
![](dblvirt_err_histo.pdf)

# Phase space slices
Parameterisation:
$$
\begin{aligned}
\cos{\beta} &= 1 + 2\frac{1-x_1-x_2}{x_1 x_2} \\
p_1 &= \frac{\sqrt{s}}{2} (-1,0,0,-1) \\
p_2 &= \frac{\sqrt{s}}{2} (-1,0,0,1) \\
p_3 &= x_1 \frac{\sqrt{s}}{2} (1,\sin{\theta},0,\cos{\theta}) \\
p_4 &= x_1 \frac{\sqrt{s}}{2} (1,\cos{\alpha}\cos{\theta}\sin{\beta}+\cos{\beta}\sin{\theta},\sin{\alpha}\sin{\beta},\cos{\beta}\cos{\theta}-\cos{\alpha}\sin{\beta}\sin{\theta}) \\
p_5 &= -p_1-p_2-p_3-p_4
\end{aligned}
$$
Fix: $\theta=\frac{\pi}{3}, \alpha=\frac{\pi}{5}$

## $x_1$ slice
Fix: $x_2=0.7$

![](x1.pdf)
![](x1r.pdf)

## $x_2$ slice
Fix: $x_1=0.7$

![](x2.pdf)
![](x2r.pdf)
