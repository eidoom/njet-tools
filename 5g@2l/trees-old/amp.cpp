#include <array>
#include <cassert>
#include <chrono>
#include <complex>
#include <iostream>
#include <limits>
#include <random>

#include "analytic/0q5g-analytic.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

const int N { 5 };

template <typename T>
std::complex<T> runA(const Helicity<N> hels, const std::array<MOM<T>, N> mom, const T Nc, const T mur)
{
    const T scale { 1. };
    Amp0q5g_a<T> amp(scale);
    amp.setNc(Nc);
    amp.setMomenta(mom.data());
    amp.setHelicity(hels.data());

    // std::array<std::complex<T>, 12> trees { {
    //     amp.A0(0, 1, 2, 3, 4),
    //     amp.A0(0, 3, 4, 2, 1),
    //     amp.A0(0, 4, 2, 3, 1),
    //     amp.A0(0, 1, 3, 4, 2),
    //     amp.A0(0, 1, 4, 2, 3),
    //     amp.A0(0, 2, 3, 4, 1),
    //     amp.A0(0, 2, 1, 3, 4),
    //     amp.A0(0, 3, 4, 1, 2),
    //     amp.A0(0, 2, 3, 1, 4),
    //     amp.A0(0, 3, 1, 4, 2),
    //     amp.A0(0, 4, 2, 1, 3),
    //     amp.A0(0, 4, 1, 2, 3),
    // } };

    std::complex<T> tree { amp.A0() };

    // std::cout << " A0: " << i_ * tree << '\n';
    // std::cout << "cA0: " << std::conj(i_ * tree) << '\n';

    return tree;
}

template <typename T>
std::complex<T> runC(const Helicity<N> hels, const std::array<MOM<T>, N> mom, const T Nc)
{
    const T scale { 1. };
    Amp0q5g_a<T> amp(scale);
    amp.setNc(Nc);
    amp.setMomenta(mom.data());

    T partC { amp.born(hels.data()) };

    std::cout
        << "born:  " << partC
        << '\n';

    return partC;
}

template <typename T>
std::complex<T> runH(const std::array<MOM<T>, N> mom, const T Nc)
{
    const T scale { 1. };

    Amp0q5g_a<T> amp(scale);
    amp.setNc(Nc);
    amp.setMomenta(mom.data());

    auto t1 { std::chrono::high_resolution_clock::now() };
    T partH { amp.born() };
    auto t2 { std::chrono::high_resolution_clock::now() };

    std::cout
        << "Helicity sum:\n"
        << "born:  " << partH
        << "\nTime:  " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << "us"
        << '\n';

    return partH;
}

template <typename T>
T invariant(const std::array<MOM<T>, N>& moms, const int i, const int j)
{
    return 2 * dot(moms[i], moms[j]);
}

template <typename T>
void loop_matrix(const int num_ps = 1)
{
    std::cout << '\n';

    std::random_device dev;
    std::mt19937_64 rng(dev());
    const T boundary { 0.01 };
    std::uniform_real_distribution<T> dist_y1(boundary, 1. - boundary);
    std::uniform_real_distribution<T> dist_a(boundary, M_PI - boundary);

    for (int i { 0 }; i < num_ps; ++i) {

        T y1 { dist_y1(rng) };
        std::uniform_real_distribution<T> dist_y2(boundary, 1. - y1 - boundary);
        T y2 { dist_y2(rng) };
        T theta { dist_a(rng) };
        T alpha { dist_a(rng) };

        // const T y1 { 2.5466037534799052e-01 };
        // const T y2 { 3.9999592899158604e-01 };
        // const T theta { 1.2398476487204491e+00 };
        // const T alpha { 4.3891163982516607e-01 };

        std::cout << "y1   =" << y1 << '\n'
                  << "y2   =" << y2 << '\n'
                  << "theta=" << theta << '\n'
                  << "alpha=" << alpha << '\n'
                  << '\n';

        const std::array<MOM<T>, N> moms { phase_space_point(y1, y2, theta, alpha) };

        T smallest { std::numeric_limits<T>::max() };
        T largest { T() };
        for (int i { 0 }; i < N; ++i) {
            for (int j { i + 1 }; j < N; ++j) {
                const T inv { invariant(moms, i, j) };
                const T mag { abs(inv) };
                if (mag > largest) {
                    largest = mag;
                }
                if (mag < smallest) {
                    smallest = mag;
                }
                std::cout
                    << "s" << i << j << "="
                    << (inv < T(0.) ? "" : " ")
                    << inv
                    << '\n';
            }
        }
        std::cout
            // << "smallest="
            // << smallest
            // << '\n'
            // << "largest="
            // << largest
            // << '\n'
            << "greatest kinematic scale separation="
            << smallest / largest
            << '\n'
            << '\n';

        std::cout
            << "Invariants:\n"
            << '('
            << invariant(moms, 0, 1) << ", "
            << invariant(moms, 1, 2) << ", "
            << invariant(moms, 2, 3) << ", "
            << invariant(moms, 3, 4) << ", "
            << invariant(moms, 4, 0)
            << ')'
            << '\n';

        T Nc { 3. };
        std::cout << "Nc=" << Nc << '\n';

        for (int i0 { -1 }; i0 < 2; i0 += 2) {
            for (int i1 { -1 }; i1 < 2; i1 += 2) {
                for (int i2 { -1 }; i2 < 2; i2 += 2) {
                    for (int i3 { -1 }; i3 < 2; i3 += 2) {
                        for (int i4 { -1 }; i4 < 2; i4 += 2) {
                            const Helicity<N> hels { { i4, i3, i2, i1, i0 } };
                            if (hels.order() < 2) {
                                // const Helicity<N> hels { { 1, 1, 1, -1, -1 } };
                                std::cout << hels << '\n';

                                // const std::complex<T> hamp_virt0 { runA<T>(hels, moms, Nc, mur) };
                                const std::complex<T> hamp_virt { runC<T>(hels, moms, Nc) };
                            }
                        }
                    }
                }
            }
        }

        // std::cout << "{" << '\n';
        // for (int j { 0 }; j < N - 1; ++j) {
        //     std::cout << moms[j] << ',' << '\n';
        // }
        // std::cout << moms[N - 1] << "\n},\n";

        const std::complex<T> hamp_virt2 { runH<T>(moms, Nc) };
    }
    std::cout << '\n';
}

int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);

    loop_matrix<double>(1);
}
