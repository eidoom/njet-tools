# new-format
## NJet compile
```shell
CPPFLAGS=`pkg-config PentagonFunctions eigen3 --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` CXXFLAGS='-std=c++17 -Wall -Wextra -O0' $SCRATCH/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --enable-an0q5g2l
```
Option: ` --with-qd=no`
## Usage
```shell
math -script mmppp/proc-1l-new-mmppp.wl
./loops.sh
./init.sh
```
## Notes
From Simon:
>1) split the poles and finite remainder into two files
>
>2) organise each file, P<LOOP>_<HELICITY>_fin_lr_<SCHEME>m, as follows:
>
>  contains a lit of 6 elements:
>
>  [[1]] the monomials in the pentagon functions
>  [[2]] the independent rational coefficients (f[i])
>  [[3]] a list of 12x5 objects called ‘sparsematrix’. It contains the dimensions and non-zero entries of the matrix M so that
>
>A[[1]].M.A[[2]] gives the relevant eps component of the parity even partial amplitude vector
>
>  [[4]] same as [[3]] for parity odd
>  [[5]] the coefficients f in terms of factors
>  [[6]] the polynomial factors
>
>ideally the vector [[1]] would be global for all helicities but I didn’t make sure of that so far.
