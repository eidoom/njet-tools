    void hAg_coeffs_PPP(TreeValue x1, TreeValue x2, TreeValue x3, TreeValue x4, TreeValue x5);

    Eps5o2<T> hAg_p1e_PPP();
    Eps5o2<T> hAg_p2e_PPP();
    Eps5o2<T> hAg_p3e_PPP();
    Eps5o2<T> hAg_p4e_PPP();
    Eps5o2<T> hAg_p5e_PPP();
    Eps5o2<T> hAg_p6e_PPP();
    Eps5o2<T> hAg_p7e_PPP();
    Eps5o2<T> hAg_p8e_PPP();
    Eps5o2<T> hAg_p9e_PPP();
    Eps5o2<T> hAg_p10e_PPP();
    Eps5o2<T> hAg_p11e_PPP();
    Eps5o2<T> hAg_p12e_PPP();

    Eps5o2<T> hAg_p1o_PPP();
    Eps5o2<T> hAg_p2o_PPP();
    Eps5o2<T> hAg_p3o_PPP();
    Eps5o2<T> hAg_p4o_PPP();
    Eps5o2<T> hAg_p5o_PPP();
    Eps5o2<T> hAg_p6o_PPP();
    Eps5o2<T> hAg_p7o_PPP();
    Eps5o2<T> hAg_p8o_PPP();
    Eps5o2<T> hAg_p9o_PPP();
    Eps5o2<T> hAg_p10o_PPP();
    Eps5o2<T> hAg_p11o_PPP();
    Eps5o2<T> hAg_p12o_PPP();

