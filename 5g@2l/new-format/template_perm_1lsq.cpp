
// permutation OOOOO, parity SSSS
template <typename T> Eps5o2<T> Amp0q5g_a2v_CCCCC<T>::hAg_pPPTT_AAA() {
  return Eps5o2<T>(eps_comp(mIE2PTTOPPAAA), eps_comp(mIE1PTTOPPAAA),
                   eps_comp(mIE0PTTOPPAAA), eps_comp(mIEm1PTTOPPAAA),
                   eps_comp(mIEm2PTTOPPAAA));
}
