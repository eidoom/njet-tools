#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
C=${BASE}.cpp
PART=$2  # "pol" or "fin"

E=${BASE:1:1}
E=$((3-${E}))
if [ "$E" == "-2" ]; then
    E="m2"
elif [ "$E" == "-1" ]; then
    E="m1"
fi
P=${BASE:2:1}
if [ "${BASE:4:1}" == "." ]; then
    O=${BASE:3:1}
else
    O=${BASE:3:2}
fi

perl \
    -0777p \
    -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)$||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)$||gm;
        s|SM\[\d+, \{\d+, \d+\}, \{\}\]\s*||gs;
        s|SM\[\d+, \{\d+, \d+\}, \{(.+?)\}\]\s*|\1,\nmIE${E}P${P}O${O}${PART}.makeCompressed();\n\n|gs;
        s|m\[(\d+), (\d+)\] -> ([\-\d/]+),\s*|mIE${E}P${P}O${O}${PART}.insert(\2, \1) = \3;\n|g;
        s|([ (\-={\/]\s{0,2})(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        s|([ (\-={\/]\s{0,2})(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        " \
    ${IN} >${C}
