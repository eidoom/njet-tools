#!/usr/bin/env bash

cd mmppp

parts=("pol" "fin")
for part in ${parts[@]}; do
    cd ${part}

    ../../conv-y.sh ys.m
    ../../conv-f.sh fs.m
    ../../conv-F.sh Fs.m

    for e in {1..5}; do
        ps=('e' 'o')
        for p in ${ps[@]}; do
            for o in {1..12}; do
                ../../conv-e-1l.sh e${e}${p}${o}.m ${part}
                clang-format -i e${e}${p}${o}.cpp
            done
        done
    done

    tform ys.frm
    tform fs.frm  # slow for finite
    tform Fs.frm  # slow for finite (1.5 mins)

    ../../pp-form.sh ys.c
    ../../pp-form.sh fs.c
    ../../pp-y.sh
    ../../pp-form.sh Fs.c

    clang-format -i ys.cpp
    clang-format -i fs.cpp
    clang-format -i Fs.cpp

    cd ..
done

cd ..
