
template <typename T>
typename Amp0q5g_a2v_CCCCC<T>::TreeValue
Amp0q5g_a2v_CCCCC<T>::eps_comp(typename Eigen::SparseMatrix<T> mat) {
  TreeValue e;
  for (int k{0}; k < mat.outerSize(); ++k) {
    for (typename Eigen::SparseMatrix<T>::InnerIterator it(mat, k); it; ++it) {
      e += sf[it.row()] * it.value() * f[it.col()];
    }
  }
  return e;
}
