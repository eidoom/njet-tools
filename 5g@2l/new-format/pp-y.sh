#!/usr/bin/env bash

i=$(cat ys.ycount)
for n in $(seq 1 $i); do
    m=$((n - 1))
    perl -pi -e "s|y\[$n\]|y[$m]|g" fs.cpp
done
