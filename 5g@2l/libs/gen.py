#!/usr/bin/env python3

with open("libs", "r") as f:
    headers = [line.strip().split(" ") for line in f]

bases = [[h[:-11] for h in hs] for hs in headers]
procs = [p[0].replace("-", "") for p in bases]
srcs = [[b + "-analytic.cpp" for b in bs] for bs in bases]
libs = ["libnjet2an" + p for p in procs]
ts = ("SD", "VC", "DD", "QD")

with open("Makefile.am", "w") as f:

    f.write("## Process this file with automake to produce Makefile.in\n")
    f.write("ACLOCAL_AMFLAGS = -I m4\n\n")
    f.write("AM_CXXFLAGS = @NJET_CXXFLAGS@\n\n")

    f.write(
        "EXTRA_LTLIBRARIES = "
        + " ".join([" ".join([li + t + ".la" for li in libs]) for t in ts])
        + "\n\n"
    )

    f.write("lib_LTLIBRARIES =\n\n")

    for p, bs, hs, a in zip(libs, srcs, headers, procs):
        f.write("if ENABLE_AN" + a.upper() + "\n")
        f.write("lib_LTLIBRARIES += " + p + ".la\n")

        f.write(f"{p}_la_SOURCES = blank-staticfix.cpp\n")
        t = ts[0]
        f.write(f"{p}{t}_la_SOURCES = " + " ".join(bs) + "\n")
        f.write(f"EXTRA_{p}{t}_la_SOURCES = " + " ".join(hs) + "\n")
        f.write(f"{p}{t}_la_CPPFLAGS = -DUSE_SD\n")
        f.write(f"{p}{t}_la_CXXFLAGS = -O2\n")
        for t, o in zip(ts[1:], (2, 2, 1)):
            f.write(f"{p}{t}_la_SOURCES = $(lib{p}{ts[0]}_la_SOURCES)\n")
            f.write(f"{p}{t}_la_CPPFLAGS = -DUSE_{t}\n")
            f.write(f"{p}{t}_la_CXXFLAGS = -O{o}\n")
        f.write(f"{p}_la_LIBADD = {p}{ts[0]}.la\n")
        f.write(f"{p}_la_DEPENDENCIES = {p}{ts[0]}.la\n")

        for t in ts[1:]:
            f.write(f"if ENABLE_{t}\n")
            f.write(f"{p}_la_LIBADD += {p}{t}.la\n")
            f.write(f"{p}_la_DEPENDENCIES += {p}{t}.la\n")
            f.write("endif\n")

        f.write("endif\n\n")

    f.write("CLEANFILES = $(EXTRA_LTLIBRARIES)\n")
