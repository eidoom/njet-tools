#!/usr/bin/env bash

all=()
bases=()
while IFS= read -r line; do
    # echo "lib_LTLIBRARIES = $line"
    IFS=' ' read -r -a array <<< "$line"
    # echo "${array[@]}"
    next=()
    for a in ${array[@]}; do
        next+=(${a%-analytic.h})
    done
    all+=(${next[@]})
    first=${array[0]}
    bases+=(${first%-analytic.h})
done <./libs

libs="lib_LTLIBRARIES ="
for b in ${bases[@]}; do
    libs+=" libnjet2an${b}.la"
done
echo $libs
echo

elibs="EXTRA_LTLIBRARIES ="
for b in ${bases[@]}; do
    elibs+=" libnjet2an${b}SD.la"
    elibs+=" libnjet2an${b}VC.la"
    elibs+=" libnjet2an${b}DD.la"
    elibs+=" libnjet2an${b}QD.la"
done
echo $elibs
echo

for b in ${bases[@]}; do
    echo "libnjet2an${b[0]}_la_SOURCES = blank-staticfix.cpp"
done
echo

for i in `seq 0 ${#bases[@]}`; do
    a="lib${bases[i]}SD_la_SOURCES ="
    bb=${all[i]}
    for b in ${bb[@]}; do
        a+=" ${b}-analytic.cpp" 
    done
    echo $a
done
echo
