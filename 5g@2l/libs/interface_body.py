#!/usr/bin/env python3


def entry(name):
    return (
        f"#ifdef NJET_ENABLE_AN{name.upper()}\n"
        + f"val = NJetAccuracy<T>::template create<Amp{name}_a<T>>();\n"
        + "#else\n"
        + f"val = NJetAccuracy<T>::template create<Amp{name}<T>>();\n"
        + "#endif\n"
    )


with open("libs", "r") as f:
    headers = [line.strip().split(" ") for line in f]

bases = [[h[:-11] for h in hs] for hs in headers]
procs = [p[0].replace("-", "") for p in bases]

for p in procs:
    print(entry(p))
