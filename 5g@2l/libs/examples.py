#!/usr/bin/env python3

from static import hels

line = "ex_3j_2l_LDADD = ../libnjet2.la ../analytic/libnjet2an0q5g2l.la"
for h in hels:
    line += f" ../analytic/libnjet2an0q5g2v{h}.la"
print(line)
