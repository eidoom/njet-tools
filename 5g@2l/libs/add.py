#!/usr/bin/env python3

ts = ("SD", "VC", "DD", "QD")

p = "libnjet2an0q5g2l"

print(p + ".la" + "\n")

for t in ts[1:]:
    print("if ENABLE_2L")
    print(f"{p}_la_LIBADD += {p}{t}.la")
    print(f"{p}_la_DEPENDENCIES += {p}{t}.la")
    print("endif\n")
