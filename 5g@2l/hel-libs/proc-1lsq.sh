#!/usr/bin/env bash

MAIN=main_file_1lsq.cpp
rm -f ${MAIN}
touch ${MAIN}

order=(
    01234
    03421
    04231
    01342
    01423
    02341
    02134
    03412
    02314
    03142
    04213
    04123
)

root="`git root`/5g@2l/1lsq-impl"
l=1
dir="${root}/new_exprs"
for f in $dir/P${l}_*.m; do
    # echo $f
    g=${f#$dir/P${l}_epsexp_}
    h=${g%_lr*.m}
    d=hel_${h}
    df=1lsq_${d}
    mkdir -p ${df}

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (( $j < 10 )); then
        j="0$j"
    fi

    echo $h $h_symb $h_char $j

    COEFFS=${df}/coeffs.cpp
    perl -p -e "s|CCCCC|$h_char|g;" template_start.cpp >${COEFFS}
    perl -p -e "s|HH|$j|g; s|SSSSS|$h_symb|g; s|CCCCC|$h_char|g;" template_coeffs_start_1lsq.cpp >>${COEFFS}
    cat ${root}/${d}/ys.cpp >>${COEFFS}
    cat ${root}/${d}/fs.cpp >>${COEFFS}
    echo "}" >>${COEFFS}
    perl -p -e "s|CCCCC|$h_char|g;" template_end.cpp >>${COEFFS}
    # clang-format -i ${COEFFS}

    FILE=${df}/eps.cpp
    perl -p -e "s|CCCCC|$h_char|g;" template_start.cpp >${FILE}
    for i in {1..12}; do
        perl -p -e "s|HH|$j|g; s|PP|$i|g; s|SSSSS|$h|g; s|OOOOO|${order[$(($i - 1))]}|g; s|CCCCC|$h_char|g;" template_perm_start_1lsq.cpp >>${FILE}
        cat ${root}/${d}/e${i}_odd.cpp >>${FILE}
        perl -p -e "s|HH|$j|g; s|PP|$i|g; s|SSSSS|$h|g; s|OOOOO|${order[$(($i - 1))]}|g; s|CCCCC|$h_char|g;" template_perm_middle_1lsq.cpp >>${FILE}
        cat ${root}/${d}/e${i}_even.cpp >>${FILE}
    done
    perl -p -e "s|CCCCC|$h_char|g;" template_end.cpp >>${FILE}
    # clang-format -i ${FILE}

    F=`cat ${root}/${d}/fs.fcount`
    perl -p -e "s|HH|$j|g; s|SSSSS|$h|g; s|FFF|$F|g; s|CCCCC|$h_char|g;" template_1lsq.cpp >>${MAIN}

    HEADER=${df}/header.h
    perl -p -e "s|CCCCC|$h_char|g;" template_header.cpp >${HEADER}
done

perl -pi -e "s|hA007\(|hA07(|g" ${MAIN}

perl -pi -e "s|^  const TreeValue phase\{\-i_ \* hA031\(o\.data\(\)\) \/ hA031X\(\)\}\;$|  const TreeValue sExpr{T(1.) / CyclicSpinorsA(o.data())};\n  const TreeValue xExpr{x3*njet_pow(x1, 3)*njet_pow(x2, 2)};\n  const TreeValue phase{-i_ * sExpr / xExpr};|gm" ${MAIN}

perl -0777pi -e "s|  const std::array<int, 5> o{0, 1, 2, 3, 4};\n  const TreeValue phase\{\-i_ \* hA015\(o\.data\(\)\) \/ hA015X\(\)\}\;|  const TreeValue sExpr{(sA(1, 4)*sA(4, 0)*sB(0, 1))/(sA(0, 1)*sA(1, 2)*sA(2, 3)*sA(3, 0))};\n  const TreeValue xExpr{((T(1.) + (T(1.) + x2)*x3)*njet_pow(x1, 2))/x3};\n  const TreeValue phase{-i_ * sExpr / xExpr};|g" ${MAIN}

perl -0777pi -e "s|  const std::array<int, 5> o{0, 1, 2, 3, 4};\n  const TreeValue phase\{\-i_ \* hA023\(o\.data\(\)\) \/ hA023X\(\)\}\;|  const TreeValue sExpr{(sA(0, 3)*sA(3, 4)*sB(4, 0))/(sA(0, 1)*sA(1, 2)*sA(2, 4)*sA(4, 0))};\n  const TreeValue xExpr{(x3*(x2 - x4 + x5)*njet_pow(x1, 2))/(T(1.) + x3)};\n  const TreeValue phase{-i_ * sExpr / xExpr};|g" ${MAIN}

perl -0777pi -e "s|  const std::array<int, 5> o{0, 1, 2, 3, 4};\n  const TreeValue phase\{\-i_ \* hA027\(o\.data\(\)\) \/ hA027X\(\)\}\;|  const TreeValue sExpr{(sA(2, 3)*sA(4, 2)*sB(3, 4))/(sA(0, 1)*sA(1, 3)*sA(3, 4)*sA(4, 0))};\n  const TreeValue xExpr{(x2*x3*(T(1.) + x3)*x5*njet_pow(x1, 2))/(T(1.) + x2)};\n  const TreeValue phase{-i_ * sExpr / xExpr};|g" ${MAIN}

perl -0777pi -e "s|  const std::array<int, 5> o{0, 1, 2, 3, 4};\n  const TreeValue phase\{\-i_ \* hA029\(o\.data\(\)\) \/ hA029X\(\)\}\;|  const TreeValue sExpr{(sA(1, 2)*sA(3, 1)*sB(2, 3))/(sA(0, 2)*sA(2, 3)*sA(3, 4)*sA(4, 0))};\n  const TreeValue xExpr{x2*(T(1.) + x2)*x3*((T(1.) + x3)*x4 + x2*x3*(-T(1.) + x5))*njet_pow(x1, 2)};\n  const TreeValue phase{-i_ * sExpr / xExpr};|g" ${MAIN}

perl -0777pi -e "s|  const std::array<int, 5> o{0, 1, 2, 3, 4};\n  const TreeValue phase\{\-i_ \* hA030\(o\.data\(\)\) \/ hA030X\(\)\}\;|  const TreeValue sExpr{(sA(0, 1)*sA(2, 0)*sB(1, 2))/(sA(1, 2)*sA(2, 3)*sA(3, 4)*sA(4, 1))};\n  const TreeValue xExpr{(x4*njet_pow(x1, 6)*njet_pow(x2, 3)*njet_pow(x3, 2))/(T(1.) + x3 + x2*x3)};\n  const TreeValue phase{-i_ * sExpr / xExpr};|g" ${MAIN}

clang-format -i ${MAIN}
