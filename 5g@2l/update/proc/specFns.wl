(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/update"];
in="src/";
outpre="build/";
dedup[a_]:=Select[Variables[a],MatchQ[#,_F]&];


(* ::Section:: *)
(*All*)


Fmons=Get[in<>"AllPfuncMonomials.m"];


Ffns=dedup[Fmons];


Ffns//Length


Export[outpre<>"specFns.m",Ffns];


(* ::Section:: *)
(*1 L*)


allHels={
"+++++",
"++++-",
"+++-+",
"+++--",
"++-++",
"++-+-",
"++--+",
"+-+++",
"+-++-",
"+-+-+",
"+--++",
"-++++",
"-+++-",
"-++-+",
"-+-++",
"--+++"};


finsOne=Get[in<>"partials_5g_1L_tHV_"<>#<>".m"]&/@allHels;


Fmoni1L=Sort[DeleteDuplicates[Flatten[Table[#[[2,i]]&/@finsOne,{i,12}]]]]


Ffns1l=dedup[Fmons[[#]]&/@Fmoni1L]


Export[outpre<>"specFns1L.m",Ffns1l];


Fmons1L=Table[SF[i-1]->If[MemberQ[Fmoni1L,i],Fmons[[i]],0],{i,Max[Fmoni1L]}]


Export[outpre<>"Fm1L.m",Fmons1L];
Export[outpre<>"F_size1L.txt", Fmons1L//Length];
