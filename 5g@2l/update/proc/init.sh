#!/usr/bin/env bash

ROOT=/home/ryan/git/njet-tools/5g@2l/update
cd $ROOT
PF=12

SRC1="$ROOT/build//one/"
SRC2="$ROOT/build//two/"
DES="$ROOT/concat/"

mkdir -p ${DES}

order=(
    01234
    03421
    04231
    01342
    01423
    02341
    02134
    03412
    02314
    03142
    04213
    04123
)

for s in ${SRC1}/h*; do
    h="${s#*//*//h}"

    s1=$s
    s2=${SRC2}/h$h

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j == 31 || $j == 30 || $j == 29 || $j == 27 || $j == 23 || $j == 15)); then
        UHV=true
    else
        UHV=false
    fi

    if $UHV; then
        echo $h UHV
    else
        echo $h MHV
    fi

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"
    mkdir -p $d

    SRC_HEL_I="${d}/src_hel.cpp"

    # F=$(cat build/**/h${h}/fm.count | sort -n | tail -1)
    F1=$(cat ${s1}/fm.count)

    # Y=$(cat build/**/h${h}/ym.count | sort -n | tail -1)
    Y1=$(cat ${s1}/ym.count)

    # Z=$(cat build/**/h${h}/*.tmp | sort -n | tail -1)
    # Z=$((Z + 1))

    FZ1=$(cat ${s1}/fm.tmp)
    FZ1=$((FZ1 + 1))

    YZ1=$(cat ${s1}/ym.tmp)
    YZ1=$((YZ1 + 1))

    if ! $UHV; then
        F2=$(cat ${s2}/fm.count)

        Y2=$(cat ${s2}/ym.count)

        FZ2=$(cat ${s2}/fm.tmp)
        FZ2=$((FZ2 + 1))

        YZ2=$(cat ${s2}/ym.tmp)
        YZ2=$((YZ2 + 1))
    fi

    perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q5g|g; s|YYYY|2020, 2021|g;" templates/template_start.cpp >${SRC_HEL_I}
    perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q5g|g; " templates/template_src_hel.cpp >>${SRC_HEL_I}

    # base class initialiser list 1L
    echo ", $F1" >>${SRC_HEL_I}

    # F sparse matrices init
    ps=('e')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|, \1, \2|gms" ${s1}/${p}${o}.m >>${SRC_HEL_I}
        done
    done

    # F special function indices vector init
    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?Fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
        ${s1}/Fv.m >>${SRC_HEL_I}

    # F coefficient indices vector init
    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
        ${s1}/fv.m >>${SRC_HEL_I}

    if ! $UHV; then
        # base class initialiser list 2L
        echo ", $F2 " >>${SRC_HEL_I}

        # 2LC sparse matrices init
        ps=('e')
        for p in ${ps[@]}; do
            for ((o = 1; o <= $PF; o++)); do
                perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|, \1, \2|gms" ${s2}/${p}${o}.m >>${SRC_HEL_I}
            done
        done

        # 2LC special function indices vector init
        perl \
            -0777p \
            -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?Fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
            ${s2}/Fv.m >>${SRC_HEL_I}

        # 2LC coefficient indices vector init
        perl \
            -0777p \
            -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
            ${s2}/fv.m >>${SRC_HEL_I}
    fi

    # finish init list
    echo -e ") {\n" >>${SRC_HEL_I}

    # resize coeff vectors F
    for ((o = 1; o <= $PF; o++)); do
        perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|  c_g[$((o - 1))].resize(\1);\n|gms" ${s1}/e${o}.m >>${SRC_HEL_I}
    done
    echo >>${SRC_HEL_I}

    if ! $UHV; then
        # resize coeff vectors 2LC
        ps=('e' 'o')
        for p in ${ps[@]}; do
            for ((o = 1; o <= $PF; o++)); do
                perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|  c_2g${p}[$((o - 1))].resize(\1);\n|gms" ${s2}/${p}${o}.m >>${SRC_HEL_I}
            done
        done
        echo >>${SRC_HEL_I}
    fi

    # fill G sparse matrix
    ps=('e')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            perl -p -e "s|m_l1pEo(\d+)|m_g\1|g;" ${s1}/${p}${o}.cpp >>${SRC_HEL_I}
        done
    done

    if ! $UHV; then
        # fill 2LC sparse matrix
        if $UHV; then
            ps=('e' 'o')
            for p in ${ps[@]}; do
                for ((o = 1; o <= $PF; o++)); do
                    cat ${s2}/${p}${o}.cpp >>${SRC_HEL_I}
                done
            done
        else
            for ((o = 1; o <= $PF; o++)); do
                echo "fill_m_2g${o}();" >>${SRC_HEL_I}
                SRC_HEL_SM="${d}/src_hel_lc_${o}.cpp"
                perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q5g|g; s|YYYY|2020, 2021|g;" templates/template_start.cpp >${SRC_HEL_SM}
                echo -e "template<typename T>\nvoid Amp0q3g2A_a2v_${h_char}<T>::fill_m_2g${o}() {" >>${SRC_HEL_SM}
                # for alt in ${s2}/e${o}.cpp_*; do
                #     n=${alt#${s2}/e${o}.cpp_}
                #     echo "fill_m_2g${o}e${n}();" >>${SRC_HEL_SM}
                # done
            done
            for ((o = 1; o <= $PF; o++)); do
                SRC_HEL_SM="${d}/src_hel_lc_${o}.cpp"
                cat ${s2}/o${o}.cpp >>${SRC_HEL_SM}
                echo -e "}" >>${SRC_HEL_SM}
                perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q5g|g;" templates/template_end.cpp >>${SRC_HEL_SM}
            done
        fi
    fi

    # finish constructor
    echo -e "}\n" >>${SRC_HEL_I}

    ./proc/filler.py $h_char >>${SRC_HEL_I}

    # F coefficient monomials
    perl -p -e "s|CCCCC|$h_char|g; s|GGG|g|g; s|PPPPP|0q5g|g;" templates/template_coeffs_start.cpp >>${SRC_HEL_I}
    echo -e "std::array<std::complex<T>, $Y1> w;" >>${SRC_HEL_I}
    echo -e "std::array<std::complex<T>, $YZ1> u;" >>${SRC_HEL_I}
    perl -p -e "s|Z|u|g; s|y|w|g;" ${s1}/ym.cpp >>${SRC_HEL_I}
    echo -e "\nstd::array<std::complex<T>, $FZ1> v;" >>${SRC_HEL_I}
    perl -p -e "s|f\[|f_g[|g; s|Z|v|g; s|y|w|g;" ${s1}/fm.cpp >>${SRC_HEL_I}
    echo -e "}\n" >>${SRC_HEL_I}

    # 2LC coefficient monomials
    if ! $UHV; then
        perl -p -e "s|CCCCC|$h_char|g; s|GGG|2g|g; s|PPPPP|0q5g|g;" templates/template_coeffs_start.cpp >>${SRC_HEL_I}
        echo -e "std::array<std::complex<T>, $Y2> w;" >>${SRC_HEL_I}
        echo -e "std::array<std::complex<T>, $YZ2> u;" >>${SRC_HEL_I}
        perl -p -e "s|Z|u|g; s|y|w|g;" ${s2}/ym.cpp >>${SRC_HEL_I}
        echo -e "\nstd::array<std::complex<T>, $FZ2> v;" >>${SRC_HEL_I}
        perl -p -e "s|f\[|f_2g[|g; s|Z|v|g; s|y|w|g;" ${s2}/fm.cpp >>${SRC_HEL_I}
        echo -e "}\n" >>${SRC_HEL_I}
    fi

    perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q5g|g;" templates/template_end.cpp >>${SRC_HEL_I}
done
