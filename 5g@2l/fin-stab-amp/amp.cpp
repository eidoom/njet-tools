#include <array>
#include <cassert>
#include <chrono>
#include <complex>
#include <iostream>
#include <random>

#include "analytic/0q5g-2l-analytic.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

const int N { 5 };

template <typename T>
T loop(const std::array<MOM<T>, N>& moms, const T scale = 1.)
{
    Amp0q5g_a2l<T> amp(scale);
    amp.setNf(0);
    amp.setNc(3);
    amp.setMuR2(1.);
    amp.setMomenta(moms.data());
    amp.setpenta2l();

    //const T born { amp.born_fin() };
    //const T virt { amp.virt_fin() };
    //const T rvob { virt / born };
    //const T virtsq { amp.virtsq_fin() };
    //const T rv2ob { virtsq / born };
    //const T dblvirt { amp.dblvirt_fin() };
    //const T r2vob { dblvirt / born };

    int hel[] {-1,-1,1,1,1};
    //const T born { amp.born_fin(hel) };
    const T virt { amp.virt_fin(hel) };
    //const T rvob { virt / born };
    //const T virtsq { amp.virtsq_fin(hel) };
    //const T rv2ob { virtsq / born };
    //const T dblvirt { amp.dblvirt_fin(hel) };
    //const T r2vob { dblvirt / born };

    std::cout
        << "scale:        " << scale << '\n'
        //<< "born:         " << born << '\n'
        << "virt:         " << virt << '\n'
        //<< "virt/born:    " << rvob << '\n'
        //<< "virtsq:       " << virtsq << '\n'
        //<< "virtsq/born:  " << rv2ob << '\n'
        //<< "dblvirt:      " << dblvirt << '\n'
        //<< "dblvirt/born: " << r2vob << '\n'
        << '\n';

    return 1.;
}

template <typename T>
void scaling(const int num = 1)
{
    // point A
    const T y1 { 2.5466037534799052e-01 };
    const T y2 { 3.9999592899158604e-01 };
    const T theta { 1.2398476487204491e+00 };
    const T alpha { 4.3891163982516607e-01 };

    for (int i { 0 }; i < num; ++i) {
        std::cout
            << "===============" << '\n'
            << "Scaling test " << i << '\n';

        //std::random_device dev;
        //std::mt19937_64 rng(dev());
        //const T boundary { 0.01 };
        //std::uniform_real_distribution<T> dist_y1(boundary, 1. - boundary);
        //std::uniform_real_distribution<T> dist_a(boundary, M_PI - boundary);
    
        //T y1 { dist_y1(rng) };
        //std::uniform_real_distribution<T> dist_y2(boundary, 1. - y1 - boundary);
        //T y2 { dist_y2(rng) };
        //T theta { dist_a(rng) };
        //T alpha { dist_a(rng) };
    
        std::array<MOM<T>, N> moms { phase_space_point(y1, y2, theta, alpha) };
    
        const T one { loop<T>(moms, 1.) };
        const T two { loop<T>(moms, 1.2857142857142855) };
        std::cout
            //<< "dblvirt diff: " << one - two << '\n'
            << '\n';
    }
}

int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);
    std::cout
        << '\n'
        << "------------------------------" << '\n'
        << "Scaling tests of helicity sums" << '\n'
        << "------------------------------" << '\n'
        << '\n';

    scaling<double>();
}
