(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/1l-part"];


all=Get["tmp.out"]


part=Get["tmp_part.out"][[1]];
prim=Get["tmp_prim.out"][[1]];


a=part[[5]]


b=prim[[5]]


Table[Coefficient[b,eps,i]/Coefficient[a,eps,i],{i,0,-2,-1}]


Nc^2/2/.Nc->3
