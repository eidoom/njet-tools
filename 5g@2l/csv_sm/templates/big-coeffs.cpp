{%- from 'macros.jinja2' import comma, matrix -%}
/*  
 * finrem/{{ amp }}/{{ channel }}/{{ h_char }}-{{ loop_obj.id }}.cpp
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#include "{{ h_char }}.h"

template <typename T>
void Amp{{ amp }}_{{ channel }}_{{ h_char }}<T>::hA{{ loop_obj.id }}_coeffs(const std::complex<T> x1, const std::complex<T> x2, const std::complex<T> x3, const std::complex<T> x4, const std::complex<T> x5) {
    std::vector<std::complex<T>> u({{ loop_obj.yz }});
    std::vector<std::complex<T>> y({{ loop_obj.yl }});
    {{ loop_obj.yms }}
    std::vector<std::complex<T>> v({{ loop_obj.fz }});
    {{ loop_obj.fms }}
}

#ifdef USE_SD
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<double>;
#endif
#ifdef USE_DD
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<dd_real>;
#endif
#ifdef USE_QD
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<qd_real>;
#endif
#ifdef USE_VC
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<Vc::double_v>;
#endif
