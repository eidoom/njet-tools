{%- from 'macros.jinja2' import comma, matrix -%}
{% macro lst(num, name) %}
{%- if num > 128 %}
std::vector<std::complex<T>> {{ name }}({{ num }});
{%- else %}
std::array<std::complex<T>, {{ num }}> {{ name }};
{%- endif %}
{% endmacro %}
/*  
 * finrem/{{ amp }}/{{ channel }}/{{ h_char }}.cpp
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#include "{{ h_char }}.h"

template <typename T>
Amp{{ amp }}_{{ channel }}_{{ h_char }}<T>::Amp{{ amp }}_{{ channel }}_{{ h_char }}(const std::vector<std::complex<T>> &sf_)
    : Base(sf_, "{{ h_char }}",
             {%- for loop_obj in ld if loop_obj.order %}
             {{ loop_obj.fl }}, 
             {% for sfiv in loop_obj.sfis -%}
             { {%- for i in sfiv %}{{ i }}{{- comma(loop) }}{% endfor -%} },
             {% endfor %}
             {%- for fiv in loop_obj.fis -%}
             { {%- for i in fiv %}{{ i }}{{- comma(loop) }}{% endfor -%} }{{- comma(loop) }}
             {% endfor -%}
             {{- comma(loop) }}
             {%- endfor -%}) {}

{% for loop_obj in ld if (loop_obj.order and not loop_obj.parity_odd) %}
template <typename T>
void Amp{{ amp }}_{{ channel }}_{{ h_char }}<T>::hA{{ loop_obj.id }}_coeffs(const std::complex<T> x1, const std::complex<T> x2, const std::complex<T> x3, const std::complex<T> x4, const std::complex<T> x5) {
    {{ lst(loop_obj.yz, "u") }}
    {{ lst(loop_obj.yl, "y") }}
    {{ loop_obj.yms }}
    {{ lst(loop_obj.fz, "v") }}
    {{ loop_obj.fms }}

}
{% endfor %}

#ifdef USE_SD
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<double>;
#endif
#ifdef USE_DD
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<dd_real>;
#endif
#ifdef USE_QD
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<qd_real>;
#endif
#ifdef USE_VC
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<Vc::double_v>;
#endif
