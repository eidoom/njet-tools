# 5g@2l > fermion_loops

Derived from `../4q1g@2l`.

## Usage

```shell
./proc/init.py
cd ggggg
math -script proc/proc.wl
../proc/loop.sh
cd build
make -j
cd ../..
./proc/gen_src.py
```

ords
```
                             {0, 1, 2, 3, 4},
                             {0, 1, 3, 4, 2},
                             {0, 1, 4, 2, 3},
                             {0, 2, 1, 3, 4},
                             {0, 2, 3, 1, 4},
                             {0, 2, 3, 4, 1},
                             {0, 3, 1, 4, 2},
                             {0, 3, 4, 1, 2},
                             {0, 3, 4, 2, 1},
                             {0, 4, 1, 2, 3},
                             {0, 4, 2, 1, 3},
                             {0, 4, 2, 3, 1},
```

scale
```
template <typename T, typename P>
std::array<LoopResult<std::complex<T>>, 12>
Amp0q5g_a2l<T, P>::one_loop(const int i) {
  return FinRem::call_cached(
      L00, L01, i, hA00pr, hA01pr,
      [this](const std::complex<T> tree, const std::complex<T> loop) {
        return loop + T(11.) / T(2.) * tree * log(MuR2());
      });
};

template <typename T, typename P>
std::array<LoopResult<std::complex<T>>, 12>
Amp0q5g_a2l<T, P>::two_loop(const int i) {
  return FinRem::call_cached(
      L00, L01, L02, i, hA00pr, hA01pr, hA02pr,
      [this](const std::complex<T> tree, const std::complex<T> loop,
             const std::complex<T> dblloop) {
        return dblloop - T(55.) / T(6.) * loop * log(MuR2()) -
               (T(127.) / T(6.) + T(55.) / T(72.) * pi * pi + T(5.) * zeta3) *
                   tree * log(MuR2()) -
               T(605.) / T(24.) * tree * log(MuR2()) * log(MuR2());
      });
};
```

## TODO
* tree stuff not defined: L00,A00_mem,hA00r/p
