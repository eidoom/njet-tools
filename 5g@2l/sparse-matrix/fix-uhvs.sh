#!/usr/bin/env bash

DIR="out-$1"
MAIN="${DIR}/src_main.cpp"
# HEAD="${DIR}/hdr_main.cpp"

perl -pi -e "s|hA007\(|hA07(|g" ${MAIN}

perl -pi -e "s|^  const std::complex<T> phase\{-i_ \* hA031\(o\.data\(\)\) \/ hA031X\(\)\}\;$|  const std::complex<T> sExpr{T(1.) / CyclicSpinorsA(o.data())};\n  const std::complex<T> xExpr{x3*njet_pow(x1, 3)*njet_pow(x2, 2)};\n  const std::complex<T> phase{-i_ * sExpr / xExpr};|gm" ${MAIN}

perl -0777pi -e "s|  const std::array<int, 5> o{0, 1, 2, 3, 4};\n  const std::complex<T> phase\{-i_ \* hA015\(o\.data\(\)\) \/ hA015X\(\)\}\;|  const std::complex<T> sExpr{(sA(1, 4)*sA(4, 0)*sB(0, 1))/(sA(0, 1)*sA(1, 2)*sA(2, 3)*sA(3, 0))};\n  const std::complex<T> xExpr{((T(1.) + (T(1.) + x2)*x3)*njet_pow(x1, 2))/x3};\n  const std::complex<T> phase{-i_ * sExpr / xExpr};|g" ${MAIN}

perl -0777pi -e "s|  const std::array<int, 5> o{0, 1, 2, 3, 4};\n  const std::complex<T> phase\{-i_ \* hA023\(o\.data\(\)\) \/ hA023X\(\)\}\;|  const std::complex<T> sExpr{(sA(0, 3)*sA(3, 4)*sB(4, 0))/(sA(0, 1)*sA(1, 2)*sA(2, 4)*sA(4, 0))};\n  const std::complex<T> xExpr{(x3*(x2 - x4 + x5)*njet_pow(x1, 2))/(T(1.) + x3)};\n  const std::complex<T> phase{-i_ * sExpr / xExpr};|g" ${MAIN}

perl -0777pi -e "s|  const std::array<int, 5> o{0, 1, 2, 3, 4};\n  const std::complex<T> phase\{-i_ \* hA027\(o\.data\(\)\) \/ hA027X\(\)\}\;|  const std::complex<T> sExpr{(sA(2, 3)*sA(4, 2)*sB(3, 4))/(sA(0, 1)*sA(1, 3)*sA(3, 4)*sA(4, 0))};\n  const std::complex<T> xExpr{(x2*x3*(T(1.) + x3)*x5*njet_pow(x1, 2))/(T(1.) + x2)};\n  const std::complex<T> phase{-i_ * sExpr / xExpr};|g" ${MAIN}

perl -0777pi -e "s|  const std::array<int, 5> o{0, 1, 2, 3, 4};\n  const std::complex<T> phase\{-i_ \* hA029\(o\.data\(\)\) \/ hA029X\(\)\}\;|  const std::complex<T> sExpr{(sA(1, 2)*sA(3, 1)*sB(2, 3))/(sA(0, 2)*sA(2, 3)*sA(3, 4)*sA(4, 0))};\n  const std::complex<T> xExpr{x2*(T(1.) + x2)*x3*((T(1.) + x3)*x4 + x2*x3*(-T(1.) + x5))*njet_pow(x1, 2)};\n  const std::complex<T> phase{-i_ * sExpr / xExpr};|g" ${MAIN}

perl -0777pi -e "s|  const std::array<int, 5> o{0, 1, 2, 3, 4};\n  const std::complex<T> phase\{-i_ \* hA030\(o\.data\(\)\) \/ hA030X\(\)\}\;|  const std::complex<T> sExpr{(sA(0, 1)*sA(2, 0)*sB(1, 2))/(sA(1, 2)*sA(2, 3)*sA(3, 4)*sA(4, 1))};\n  const std::complex<T> xExpr{(x4*njet_pow(x1, 6)*njet_pow(x2, 3)*njet_pow(x3, 2))/(T(1.) + x3 + x2*x3)};\n  const std::complex<T> phase{-i_ * sExpr / xExpr};|g" ${MAIN}
