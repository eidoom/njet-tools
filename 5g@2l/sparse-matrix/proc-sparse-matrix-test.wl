(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/sparse-matrix"];
in="expr-test/exprs/"
out="expr-test/proc/"


fin=Get[in<>"test1L.m"];
Fmons=Get[in<>"AllPfuncMonomials.m"];


sm[ord_,par_]:=Module[
	{a=fin[[3,ord,par]],entries},
	entries=a[[2]]/.{m[i_,j_]:>m[i-1,j-1]};
	SM[Length[entries],a[[1]],entries]
];


Fml=Length[Fmons];
Fms=SF[#-1]->Fmons[[#]]&/@Range[Fml];


fv=Module[
	{a=First[#]-1&/@#&/@fin[[1]]},
	fi[#]->With[{b=a[[#]]},V[Length[b],b]]&/@Range[Length[a]]
];


Fv=Module[
	{a=#-1&/@fin[[2]]},
	Fi[#]->With[{b=a[[#]]},V[Length[b],b]]&/@Range[Length[a]]
];


fms=fin[[5]]/.{(fn:(f|y))[i_]:>fn[i-1]};


yms=fin[[6]]/.{y[i_]:>y[i-1]};


Export[out<>"Fm.m",Fms];
Export[out<>"F_size.txt", Fml];
Export[out<>"fv.m",fv];
Export[out<>"Fv.m",Fv];
Export[out<>"fm.m",fms];
Export[out<>"f_size.txt", Length[fms]];
Export[out<>"ym.m",yms];
Export[out<>"y_size.txt", Length[yms]];


Export[out<>"e"<>ToString[#]<>".m",sm[#,1]]&/@Range[12];
Export[out<>"o"<>ToString[#]<>".m",sm[#,2]]&/@Range[12];
