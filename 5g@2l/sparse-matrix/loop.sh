#!/usr/bin/env bash

ROOT="expr-one"
NOM="one"
SRC="${ROOT}/${NOM}/"

for f in ${SRC}/h*; do
    echo $f
done | parallel './loop-single.sh'

cd "${ROOT}"

../conv-F.sh Fm.m
tform Fm.frm
../pp-form.sh Fm.c
clang-format -i Fm.cpp

cd ..
