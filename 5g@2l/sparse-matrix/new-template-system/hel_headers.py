#!/usr/bin/env python3

import sys
import os

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, "../../../3g2a@2l/proc/")
sys.path.insert(0, filename)

import static


def hdr(h, two):
    app = "2L" if two else ""
    txt = f"""/*
 * finrem/0q5g/0q5g-2v-{h}.h
 *
 * This file is part of NJet library
 * Copyright (C) 2020, 2021 NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#ifndef FINREM_0Q5G_2V_{h.upper()}_H
#define FINREM_0Q5G_2V_{h.upper()}_H

#include <array>
#include <complex>
#include <vector>

#include <Eigen/SparseCore>

#include \"../../ngluon2/EpsQuintuplet.h\"
#include \"../../ngluon2/utility.h\"

#include \"0q5g-2v-hel.h\"

template <typename T>
class Amp0q5g_a2v_{h} : HelAmp0q5g{app}<T> {{
  using Base = HelAmp0q5g{app}<T>;

public:
  Amp0q5g_a2v_{h}(const std::vector<std::complex<T>> &sf_);

  using Base::c_g;"""
    if two:
        txt += """
  using Base::c_2g;"""
    txt += """
  using Base::coeffVec;
  using Base::f_2g;
  using Base::f_g;
  using Base::sf;

  std::vector<std::complex<T>> y;
  std::vector<std::complex<T>> Z;
"""
    if two:
        txt += """
  virtual void fill_m_2g1() override;
  virtual void fill_m_2g2() override;
  virtual void fill_m_2g3() override;
  virtual void fill_m_2g4() override;
  virtual void fill_m_2g5() override;
  virtual void fill_m_2g6() override;
  virtual void fill_m_2g7() override;
  virtual void fill_m_2g8() override;
  virtual void fill_m_2g9() override;
  virtual void fill_m_2g10() override;
  virtual void fill_m_2g11() override;
  virtual void fill_m_2g12() override;

  // }{ 1l
"""
    txt += """
  Eigen::SparseMatrix<T> m_g1, m_g2, m_g3, m_g4, m_g5, m_g6, m_g7, m_g8, m_g9,
      m_g10, m_g11, m_g12;

  std::vector<int> sfi_g1, sfi_g2, sfi_g3, sfi_g4, sfi_g5, sfi_g6, sfi_g7,
      sfi_g8, sfi_g9, sfi_g10, sfi_g11, sfi_g12;

  std::vector<int> fi_g1, fi_g2, fi_g3, fi_g4, fi_g5, fi_g6, fi_g7, fi_g8,
      fi_g9, fi_g10, fi_g11, fi_g12;

  virtual void hAg_coeffs(std::complex<T> x1, std::complex<T> x2,
                          std::complex<T> x3, std::complex<T> x4,
                          std::complex<T> x5) override;

  virtual std::complex<T> pAg1() override;
  virtual std::complex<T> pAg2() override;
  virtual std::complex<T> pAg3() override;
  virtual std::complex<T> pAg4() override;
  virtual std::complex<T> pAg5() override;
  virtual std::complex<T> pAg6() override;
  virtual std::complex<T> pAg7() override;
  virtual std::complex<T> pAg8() override;
  virtual std::complex<T> pAg9() override;
  virtual std::complex<T> pAg10() override;
  virtual std::complex<T> pAg11() override;
  virtual std::complex<T> pAg12() override;
"""
    if two:
        txt += """
  // }{ 2l

  Eigen::SparseMatrix<T> m_2g1e, m_2g2e, m_2g3e, m_2g4e, m_2g5e, m_2g6e, m_2g7e,
      m_2g8e, m_2g9e, m_2g10e, m_2g11e, m_2g12e, m_2g1o, m_2g2o, m_2g3o, m_2g4o,
      m_2g5o, m_2g6o, m_2g7o, m_2g8o, m_2g9o, m_2g10o, m_2g11o, m_2g12o;

  std::vector<int> sfi_2g1, sfi_2g2, sfi_2g3, sfi_2g4, sfi_2g5, sfi_2g6,
      sfi_2g7, sfi_2g8, sfi_2g9, sfi_2g10, sfi_2g11, sfi_2g12;

  std::vector<int> fi_2g1, fi_2g2, fi_2g3, fi_2g4, fi_2g5, fi_2g6, fi_2g7,
      fi_2g8, fi_2g9, fi_2g10, fi_2g11, fi_2g12;

  virtual void hA2g_coeffs(std::complex<T> x1, std::complex<T> x2,
                           std::complex<T> x3, std::complex<T> x4,
                           std::complex<T> x5) override;

  virtual std::complex<T> pA2g1e() override;
  virtual std::complex<T> pA2g2e() override;
  virtual std::complex<T> pA2g3e() override;
  virtual std::complex<T> pA2g4e() override;
  virtual std::complex<T> pA2g5e() override;
  virtual std::complex<T> pA2g6e() override;
  virtual std::complex<T> pA2g7e() override;
  virtual std::complex<T> pA2g8e() override;
  virtual std::complex<T> pA2g9e() override;
  virtual std::complex<T> pA2g10e() override;
  virtual std::complex<T> pA2g11e() override;
  virtual std::complex<T> pA2g12e() override;

  virtual std::complex<T> pA2g1o() override;
  virtual std::complex<T> pA2g2o() override;
  virtual std::complex<T> pA2g3o() override;
  virtual std::complex<T> pA2g4o() override;
  virtual std::complex<T> pA2g5o() override;
  virtual std::complex<T> pA2g6o() override;
  virtual std::complex<T> pA2g7o() override;
  virtual std::complex<T> pA2g8o() override;
  virtual std::complex<T> pA2g9o() override;
  virtual std::complex<T> pA2g10o() override;
  virtual std::complex<T> pA2g11o() override;
  virtual std::complex<T> pA2g12o() override;

  // }
"""
    txt += f"""}};

#endif // FINREM_0Q5G_2V_{h.upper()}_H"""
    return txt


def num2char(num):
    out = ["m"] * 5
    for i in range(4, -1, -1):
        cur = 2 ** i
        if num // cur:
            out[i] = "p"
            num %= cur
    return "".join(out)


if __name__ == "__main__":
    # for i in range(16, 32):
    #     h = num2char(i)
    for h in static.uhvs:
        print(h)
        with open(os.path.join(dirname, f"out/0q5g-2v-{h}.h"), "w") as f:
            f.write(hdr(h, False))
    for h in static.mhvs:
        print(h)
        with open(os.path.join(dirname, f"out/0q5g-2v-{h}.h"), "w") as f:
            f.write(hdr(h, True))
