#!/usr/bin/env python3

import sys
import re

pattern = re.compile(
    r"(m_g1\((\d+), \d+\),\s+m_g2\((\d+), \d+\),\s+m_g3\((\d+), \d+\),\s+m_g4\((\d+), \d+\),\s+m_g5\((\d+), \d+\),\s+m_g6\((\d+), \d+\),\s+m_g7\((\d+), \d+\),\s+m_g8\((\d+), \d+\),\s+m_g9\((\d+), \d+\),\s+m_g10\((\d+), \d+\),\s+m_g11\((\d+), \d+\),\s+m_g12\((\d+), \d+\),\s+.*? {)(\s+m)",
    flags=re.DOTALL
)

for filename in sys.argv[1:]:
    print(f"Searching {filename}...")
    with open(filename, "r") as f:
        contents = f.read()
        match = pattern.search(contents)
    if match:
        print("    Substituting...")
        with open(filename, "w") as f:
            new = pattern.sub(
                r"""\1

  c_g[0].reserve(\2);
  c_g[1].reserve(\3);
  c_g[2].reserve(\4);
  c_g[3].reserve(\5);
  c_g[4].reserve(\6);
  c_g[5].reserve(\7);
  c_g[6].reserve(\8);
  c_g[7].reserve(\9);
  c_g[8].reserve(\10);
  c_g[9].reserve(\11);
  c_g[10].reserve(\12);
  c_g[11].reserve(\13);\14""",
                contents.rstrip(),
            )
            f.write(new)
    else:
        print("    No match.")
