#!/usr/bin/env python3

import sys
import re

pattern = re.compile(r"((f_(2?)g)\[\d+\] = Z\[\d+\];)\n\}")

for filename in sys.argv[1:]:
    with open(filename, "r") as f:
        contents = f.read()
        match = pattern.search(contents)
    if match:
        with open(filename, "w") as f:
            new = pattern.sub(r"""\1

  coeffVec(c_\3g[0], \2, fi_g1, m_g1);
  coeffVec(c_\3g[1], \2, fi_g2, m_g2);
  coeffVec(c_\3g[2], \2, fi_g3, m_g3);
  coeffVec(c_\3g[3], \2, fi_g4, m_g4);
  coeffVec(c_\3g[4], \2, fi_g5, m_g5);
  coeffVec(c_\3g[5], \2, fi_g6, m_g6);
  coeffVec(c_\3g[6], \2, fi_g7, m_g7);
  coeffVec(c_\3g[7], \2, fi_g8, m_g8);
  coeffVec(c_\3g[8], \2, fi_g9, m_g9);
  coeffVec(c_\3g[9], \2, fi_g10, m_g1);
  coeffVec(c_\3g[10], \2, fi_g11, m_g11);
  coeffVec(c_\3g[11], \2, fi_g12, m_g12);
}""", contents.rstrip())
            f.write(new)

