#!/usr/bin/env python3

import sys
import re

pattern = re.compile(
    r"(m_2g1e\((\d+), \d+\),\s+m_2g2e\((\d+), \d+\),\s+m_2g3e\((\d+), \d+\),\s+m_2g4e\((\d+), \d+\),\s+m_2g5e\((\d+), \d+\),\s+m_2g6e\((\d+), \d+\),\s+m_2g7e\((\d+), \d+\),\s+m_2g8e\((\d+), \d+\),\s+m_2g9e\((\d+), \d+\),\s+m_2g10e\((\d+), \d+\),\s+m_2g11e\((\d+), \d+\),\s+m_2g12e\((\d+), \d+\),\s+m_2g1o\((\d+), \d+\),\s+m_2g2o\((\d+), \d+\),\s+m_2g3o\((\d+), \d+\),\s+m_2g4o\((\d+), \d+\),\s+m_2g5o\((\d+), \d+\),\s+m_2g6o\((\d+), \d+\),\s+m_2g7o\((\d+), \d+\),\s+m_2g8o\((\d+), \d+\),\s+m_2g9o\((\d+), \d+\),\s+m_2g10o\((\d+), \d+\),\s+m_2g11o\((\d+), \d+\),\s+m_2g12o\((\d+), \d+\),.*c_g\[11\].resize\(\d+\);\n)",
    flags=re.DOTALL
)

for filename in sys.argv[1:]:
    print(f"Searching {filename}...")
    with open(filename, "r") as f:
        contents = f.read()
        match = pattern.search(contents)
    if match:
        print("    Substituting...")
        with open(filename, "w") as f:
            new = pattern.sub(
                r"""\1
  c_2ge[0].resize(\2);
  c_2ge[1].resize(\3);
  c_2ge[2].resize(\4);
  c_2ge[3].resize(\5);
  c_2ge[4].resize(\6);
  c_2ge[5].resize(\7);
  c_2ge[6].resize(\8);
  c_2ge[7].resize(\9);
  c_2ge[8].resize(\10);
  c_2ge[9].resize(\11);
  c_2ge[10].resize(\12);
  c_2ge[11].resize(\13);

  c_2go[0].resize(\14);
  c_2go[1].resize(\15);
  c_2go[2].resize(\16);
  c_2go[3].resize(\17);
  c_2go[4].resize(\18);
  c_2go[5].resize(\19);
  c_2go[6].resize(\20);
  c_2go[7].resize(\21);
  c_2go[8].resize(\22);
  c_2go[9].resize(\23);
  c_2go[10].resize(\24);
  c_2go[11].resize(\25);
""",
                contents.rstrip(),
            )
            f.write(new)
    else:
        print("    No match.")
