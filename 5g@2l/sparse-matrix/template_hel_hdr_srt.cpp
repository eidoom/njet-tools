/*
 * analytic/0q5g-2v-hel-analytic.h
 *
 * This file is part of NJet library
 * Copyright (C) 2021 NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#ifndef ANALYTIC_0Q5G_ANALYTIC_2V_HEL_H
#define ANALYTIC_0Q5G_ANALYTIC_2V_HEL_H

#include <array>
#include <complex>
#include <vector>

#include <Eigen/SparseCore>

#include "../ngluon2/EpsQuintuplet.h"
#include "../ngluon2/utility.h"
#include "pentagons.h"


