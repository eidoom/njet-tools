(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/sparse-matrix"];
Fms=Get["expr-one/exprs/AllPfuncMonomials.m"];


Fs=DeleteDuplicates[Select[Variables[Fms],MatchQ[#,_F]&]];


Export["expr-one/Fs.m",Fs];
