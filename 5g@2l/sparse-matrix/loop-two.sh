#!/usr/bin/env bash

NOM="two"
SRC="expr-one/${NOM}"

declare -a yms
declare -a fms
declare -a sms

echo -e "# Automatically generated Makefile from $0 #\n.PHONY: all yms fms sms" >Makefile

for dir in ${SRC}/h*; do
    ps=('e' 'o')
    for p in ${ps[@]}; do
        for o in {1..12}; do
            sms+=("$dir/${p}${o}.cpp")
            echo -e "
$dir/${p}${o}.cpp: $dir/${p}${o}.m
\t./conv-sm-2l.sh $<
\tclang-format -i \$@" \
                >>Makefile
        done
    done

    echo -e "
$dir/ym.frm: $dir/ym.m
\t./conv-y.sh $<" \
        >>Makefile

    echo -e "
$dir/fm.frm: $dir/fm.m
\t./conv-f.sh $<" \
        >>Makefile

    echo -e "
$dir/ym.c: $dir/ym.frm
\ttform $< >${dir}/ym.log 2>&1" \
        >>Makefile

    echo -e "
$dir/fm.c: $dir/fm.frm
\ttform $< >${dir}/fm.log 2>&1" \
        >>Makefile

    echo -e "
$dir/ym.cpp: $dir/ym.c
\t./pp-form.sh $<
\tclang-format -i \$@" \
        >>Makefile

    echo -e "
$dir/fm.cpp: $dir/fm.c
\t./pp-form.sh $<
\tclang-format -i \$@" \
        >>Makefile

    yms+=("$dir/ym.cpp")
    fms+=("$dir/fm.cpp")
done

echo -e "\nyms: ${yms[@]}" >>Makefile
echo -e "\nfms: ${fms[@]}" >>Makefile
echo -e "\nsms: ${sms[@]}" >>Makefile
echo -e "\nall: yms fms sms" >>Makefile
