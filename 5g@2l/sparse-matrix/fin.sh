ROOT="out-one"
SRC1="${ROOT}/one"
SRC2="expr-one/two"
DES="export"

mkdir -p ${DES}

for f in ${SRC1}/h*; do
    h_symb=${f#*h}

    h_char=""
    for i in {0..4}; do
        if [ "${h_symb:$i:1}" = "+" ]; then
            h_char+="p"
        else
            h_char+="m"
        fi
    done

    cp "${f}/hdr_hel.cpp" "${DES}/0q5g-2v-${h_char}-analytic.h"
    cp "${f}/src_hel.cpp" "${DES}/0q5g-2v-${h_char}-analytic.cpp"
    for o in {1..12}; do
        cp "${f}/src_hel_${o}.cpp" "${DES}/0q5g-2v-${h_char}-2l-o${o}-analytic.cpp"
    done
done

cp ${ROOT}/hdr_main.cpp ${DES}/0q5g-2l-analytic.h.part
cp ${ROOT}/src_main.cpp ${DES}/0q5g-2l-analytic.cpp.part
clang-format -i ${DES}/0q5g-2l-analytic.cpp.part

echo "Ignore missing UHV copies"

echo "For Makefile.am"
for f in ${SRC2}/h*; do
    h_symb=${f#*h}

    h_char=""
    for i in {0..4}; do
        if [ "${h_symb:$i:1}" = "+" ]; then
            h_char+="p"
        else
            h_char+="m"
        fi
    done

    for o in {1..12}; do
        echo "0q5g-2v-${h_char}-2l-o${o}-analytic.cpp \\"
    done
done
