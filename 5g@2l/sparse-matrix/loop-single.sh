#!/usr/bin/env bash

cd "$1"
../../../conv-y.sh ym.m
../../../conv-f.sh fm.m

ps=('e' 'o')
for p in ${ps[@]}; do
    for o in {1..12}; do
        ../../../conv-sm.sh ${p}${o}.m
        clang-format -i ${p}${o}.cpp
    done
done

tform ym.frm
tform fm.frm

../../../pp-form.sh ym.c
../../../pp-form.sh fm.c

clang-format -i ym.cpp
clang-format -i fm.cpp

cd ../../..
