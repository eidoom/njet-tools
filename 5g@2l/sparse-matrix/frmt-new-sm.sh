#!/usr/bin/env bash

find export-new | parallel -j`nproc` 'clang-format -i'
