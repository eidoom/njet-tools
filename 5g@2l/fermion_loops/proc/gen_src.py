#!/usr/bin/env python3

from argparse import ArgumentParser
from copy import deepcopy
from dataclasses import asdict
from collections import OrderedDict, defaultdict
from pathlib import Path
import json

import jinja2

import init


def sort_loops(dict_item):
    # loop ID = f"{Nf power}{Nc power}"
    key = dict_item[0]
    # (3 since NNLO) * (# loops = Nf power + Nc power) + (Nc power)
    return init._sort_loops(sum(map(lambda x: int(x), key)), int(key[1]))


# TODO mma script should output in CHL ordering not CLH
def get_paths(p):
    paths_chan = defaultdict(list)
    # eg qbgqgg
    for pchannel in filter(
        lambda x: all([x.is_dir(), x not in p.top, x.name[0] != "."]), p.root.iterdir()
    ):
        chan = pchannel.parts[0]
        # eg p1LNfp0Ncp1
        for ploop in filter(lambda x: x.is_dir(), (pchannel / "build").iterdir()):
            # loop = "".join([ploop.parts[2][i] for i in (6, 10)])
            # eg h--+++
            for phel in filter(lambda x: x.is_dir(), ploop.iterdir()):
                # helicity in symbol representation
                # shel = phel.name[1:]
                paths_chan[chan].append(phel)
            # paths_l[loop].append(dict(paths_h))
        # paths_chan[chan].append(OrderedDict(sorted(paths_l.items(), key=sort_loops)))
    # paths_chan = dict(paths_chan)

    # return paths_chan

    paths_chl = {}
    for chan, group in paths_chan.items():
        hels = defaultdict(dict)

        for path in group:
            # loop identifier: Nf power, Nc power
            loop = "".join(map(lambda i: path.parts[2][i], (6, 10)))
            hel = path.name[1:]
            hels[hel][loop] = path

        paths_chl[chan] = dict(hels)

    paths_ord = {}
    for c, cg in paths_chl.items():
        hel_ord = {}
        for h, hg in cg.items():
            hel_ord[h] = OrderedDict(sorted(hg.items(), key=sort_loops))
        paths_ord[c] = hel_ord

    return paths_ord


def sort_sms(sm_path):
    # sort by partial amplitude/colour structure index
    return int(sm_path.stem[1:])


def build(p, paths, args):
    # initialise jinja2 template
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(p.templates))
    template_hel = env.get_template("hel-each.cpp")
    template_hel_bc = env.get_template("big-coeffs.cpp")
    template_sm = env.get_template("hel-sm.cpp")
    template_sm_i = env.get_template("hel-sm-i.cpp")
    template_chan = env.get_template("chan-each.cpp")
    template_sf_src = env.get_template("chan-sf.cpp")
    template_hel_hdr = env.get_template("hel-each.h")
    makefile = env.get_template("Makefile.channel.am")

    # init template substitutions
    subs = json.loads((p.root / "subs.json").read_text())

    for channel, hel_paths in paths.items():
        subs["channel"] = channel
        subs["ords"] = subs["perms"][channel]
        pchannel = p.export / channel

        subs["hels"] = []

        for hel, loop_paths in hel_paths.items():
            hdcls = init.Hel.from_symbs(hel)
            subs["h_char"] = hdcls.chars
            subs["mhv"] = hdcls.mhv
            subs["uhv"] = hdcls.uhv
            hdict = asdict(hdcls)

            subs["ld"] = []

            for loop_id, path in loop_paths.items():
                loop_obj = asdict(init.Loop(*loop_id))

                if args.sparse_matrices or args.helicities:
                    print("Reading sparse matrices...")

                    # load even sparse matrices
                    loop_obj["smes"] = []
                    for e in sorted(path.glob("e*.json"), key=sort_sms):
                        loop_obj["smes"].append(json.loads(e.read_text()))

                    # load odd sparse matrices
                    loop_obj["smos"] = []
                    for o in sorted(path.glob("o*.json"), key=sort_sms):
                        loop_obj["smos"].append(json.loads(o.read_text()))

                if args.sparse_matrices and loop_obj["smos"]:
                    subs["loop_id"] = loop_id
                    for i, sme in enumerate(loop_obj["smes"]):
                        subs["partial"] = i
                        subs["sme"] = sme
                        num = len(sme["entries"])
                        if num > 60000:
                            sme["split"] = True

                            half = num // 2

                            subs["index"] = 0
                            subs["sme"]["hentries"] = sme["entries"][:half]
                            init.fill(
                                subs,
                                template_sm_i,
                                pchannel / f"{subs['h_char']}-{loop_id}{i}-sm-0.cpp",
                            )

                            subs["index"] = 1
                            subs["sme"]["hentries"] = sme["entries"][half:]
                            init.fill(
                                subs,
                                template_sm_i,
                                pchannel / f"{subs['h_char']}-{loop_id}{i}-sm-1.cpp",
                            )
                        else:
                            init.fill(
                                subs,
                                template_sm,
                                pchannel / f"{subs['h_char']}-{loop_id}{i}-sm.cpp",
                            )

                if args.helicities:
                    # coefficients vector size
                    loop_obj["fl"] = int((path / "f_size.txt").read_text())
                    # coefficient intermediate expressions vector size
                    loop_obj["yl"] = int((path / "y_size.txt").read_text())
                    # coefficients operation optimisation cache vector size
                    loop_obj["fz"] = 1 + int((path / "fm.tmp").read_text())
                    # coefficients intermediate expressions operation optimisation cache vector size
                    loop_obj["yz"] = 1 + int((path / "ym.tmp").read_text())

                    # special function indices vector init
                    loop_obj["sfis"] = json.loads((path / "Fv.json").read_text())

                    # coefficient indices vector init
                    loop_obj["fis"] = json.loads((path / "fv.json").read_text())

                    print("Reading coefficients...")
                    loop_obj["yms"] = (path / "ym.cpp").read_text()
                    loop_obj["fms"] = (path / "fm.cpp").read_text()

                    subs["ld"].append(loop_obj)

            if args.helicities:
                init.fill(subs, template_hel_hdr, pchannel / f"{subs['h_char']}.h")
                init.fill(subs, template_hel, pchannel / f"{subs['h_char']}.cpp")

                for loop_obj in subs["ld"]:
                    if loop_obj["parity_odd"]:
                        subs["loop_obj"] = loop_obj
                        init.fill(
                            subs,
                            template_hel_bc,
                            pchannel / f"{subs['h_char']}-{loop_obj['id']}-c.cpp",
                        )

            hdict["ld"] = deepcopy(subs["ld"])
            subs["hels"].append(hdict)

        init.fill(subs, makefile, pchannel / "Makefile.am")

        if args.special_functions:
            gbl = p.root / channel / "build"

            subs["sfml1"] = int((gbl / "F_size1L.txt").read_text())
            subs["sfz1"] = 1 + int((gbl / "Fm1L.tmp").read_text())

            subs["sfml"] = int((gbl / "F_size.txt").read_text())
            subs["sfz"] = 1 + int((gbl / "Fm.tmp").read_text())

            # subs["hels"] = [init.Hel.from_symbs(h) for h in hel_paths.keys()]

            subs["sfm1"] = (gbl / "Fm1L.cpp").read_text()
            subs["sfm"] = (gbl / "Fm.cpp").read_text()

            subs["spec_funcs_1l"] = json.loads((gbl / "specFns1L.json").read_text())
            subs["spec_funcs"] = json.loads((gbl / "specFns.json").read_text())

            init.fill(subs, template_chan, pchannel / f"{subs['amp']}-{channel}.cpp")
            init.fill(subs, template_sf_src, pchannel / "sf.cpp")


if __name__ == "__main__":
    parser = ArgumentParser("Generate the source files")
    parser.add_argument(
        "-m",
        "--sparse-matrices",
        action="store_true",
        help="generate the large sparse matrix files",
    )
    parser.add_argument(
        "-s",
        "--helicities",
        action="store_true",
        help="generate the helicity class files",
    )
    parser.add_argument(
        "-p",
        "--special-functions",
        action="store_true",
        help="generate the special function monomial files",
    )
    parser.add_argument(
        "-a",
        "--all",
        action="store_true",
        help="generate all source files",
    )
    args = parser.parse_args()

    if args.all:
        args.helicities = True
        args.special_functions = True
        args.sparse_matrices = True

    p = init.Paths()
    paths = get_paths(p)
    build(p, paths, args)
