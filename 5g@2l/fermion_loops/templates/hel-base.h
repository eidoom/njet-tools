{%- from 'macros.jinja2' import comma -%}
/*
 * finrem/{{ amp }}/common/hel.h
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#pragma once

#include <complex>
#include <vector>

#include <Eigen/SparseCore>

#ifdef USE_DD
#include <qd/dd_real.h>
#endif

#ifdef USE_QD
#include <qd/qd_real.h>
#endif

#include "finrem/common/fin-rem-hel-amp.h"

// =============================================================================
// Abstract base class for {{ amp }} finite remainder helicity amplitude channels
// at one loop.
// Identifiers are three indices: (Nf power)(Nc power)(partial).
// {{ '{{' }}===========================================================================

template <typename T> class HelAmp{{ amp }}1L : public FinRemHelAmp<T> {
  using Base = FinRemHelAmp<T>;

public:
  explicit HelAmp{{ amp }}1L(const std::vector<std::complex<T>> &sf_,
             {%- for loop_obj in loops if loop_obj.order == 1 %}
             int f{{ loop_obj.id }}l, 
             {% for p in range(partials) -%}int m{{ loop_obj.id }}{{ p }}i, int m{{ loop_obj.id }}{{ p }}j, {% endfor %}
             {% for p in range(partials) -%}const std::vector<int> &sfi{{ loop_obj.id }}{{ p }}_, {%- endfor %}
             {% for p in range(partials) -%}const std::vector<int> &fi{{ loop_obj.id }}{{ p }}_{{- comma(loop) -}}{%- endfor %}
             {{- comma(loop) -}}
             {% endfor %});

  // fill coefficient basis vectors
  {% for loop_obj in loops if loop_obj.order == 1 -%}
  virtual void hA{{ loop_obj.id }}_coeffs(std::complex<T> x1, std::complex<T> x2,
                          std::complex<T> x3, std::complex<T> x4,
                          std::complex<T> x5) = 0;
  {% endfor %}

  // fill all partial coefficient vectors
  {%- for loop_obj in loops if loop_obj.order == 1 %}
  {% if loop_obj.parity_odd %}virtual {% endif %}void cacheCoeffs_{{ loop_obj.id }}();
  {%- endfor %}

  // evaluate partial amplitudes
  {% for loop_obj in loops if loop_obj.order == 1 -%}
    {%- for p in range(partials) %}
  std::complex<T> pA{{ loop_obj.id }}{{ p }}e();
    {%- endfor %}
  {% endfor %}

  // coefficient basis vectors (shared between partials)
  std::vector<std::complex<T>>
    {% for loop_obj in loops if loop_obj.order == 1 -%}
      f{{ loop_obj.id }}
      {{- comma(loop) -}}
    {%- endfor %};

  // vectors of partial coefficient vectors (for direct scalar product with special function monomials)
  std::vector<std::vector<std::complex<T>>>
    {% for loop_obj in loops if loop_obj.order == 1 -%}
      c{{ loop_obj.id }}e
      {{- comma(loop) -}}
    {%- endfor %};

  // copy partial coefficient vectors from another instance (used to cache values when reevaluating special functions in higher precision)
  template <typename U> void copyCoeffs(const HelAmp{{ amp }}1L<U> &other);

  using Base::coeffVec;
  using Base::conjVecs;

protected:
  using Base::part;

  // sparse matrices for multiplication of coefficient and special function monomial vectors
  Eigen::SparseMatrix<T>
    {% for loop_obj in loops if loop_obj.order == 1 -%}
      {%- for p in range(partials) -%}
        m{{ loop_obj.id }}{{ p }}
        {{- comma(loop) -}}
      {%- endfor -%}
      {{- comma(loop) -}}
    {%- endfor %};

  // partial index vectors for coefficients and special function monomials
  const std::vector<int> 
    {% for loop_obj in loops if loop_obj.order == 1 -%}
      {%- for p in range(partials) -%}
        fi{{ loop_obj.id }}{{ p }}, sfi{{ loop_obj.id }}{{ p }}
        {{- comma(loop) -}}
      {%- endfor -%}
      {{- comma(loop) -}}
    {%- endfor %};
};

// {{ '}}' }}===========================================================================
// base class for 2L cases with only parity even contributions
// {{ '{{' }}===========================================================================

template <typename T> class HelAmp{{ amp }} : public HelAmp{{ amp }}1L<T> {
  using Base = HelAmp{{ amp }}1L<T>;

public:
  explicit HelAmp{{ amp }}(const std::vector<std::complex<T>> &sf_,
             {%- for loop_obj in loops if loop_obj.order %}
             int f{{ loop_obj.id }}l, 
             {% for p in range(partials) -%}int m{{ loop_obj.id }}{{ p }}i, int m{{ loop_obj.id }}{{ p }}j, {% endfor %}
             {% for p in range(partials) -%}const std::vector<int> &sfi{{ loop_obj.id }}{{ p }}_, {%- endfor %}
             {% for p in range(partials) -%}const std::vector<int> &fi{{ loop_obj.id }}{{ p }}_{{- comma(loop) -}}{%- endfor %}
             {{- comma(loop) -}}
             {% endfor %});

  // fill coefficient basis vectors
  {% for loop_obj in loops if loop_obj.order == 2 -%}
  virtual void hA{{ loop_obj.id }}_coeffs(std::complex<T> x1, std::complex<T> x2,
                          std::complex<T> x3, std::complex<T> x4,
                          std::complex<T> x5) = 0;
  {% endfor %}

  // fill all partial coefficient vectors
  {%- for loop_obj in loops if loop_obj.order == 2 %}
  {% if loop_obj.parity_odd %}virtual {% endif %}void cacheCoeffs_{{ loop_obj.id }}();
  {%- endfor %}

  // evaluate partial amplitudes
  {% for loop_obj in loops if loop_obj.order == 2 -%}
    {%- for p in range(partials) %}
  std::complex<T> pA{{ loop_obj.id }}{{ p }}e();
    {%- endfor %}
  {% endfor %}

  // coefficient basis vectors (shared between partials)
  std::vector<std::complex<T>>
    {% for loop_obj in loops if loop_obj.order == 2 -%}
      f{{ loop_obj.id }}
      {{- comma(loop) -}}
    {%- endfor %};

  // vectors of partial coefficient vectors (for direct scalar product with special function monomials)
  std::vector<std::vector<std::complex<T>>>
    {% for loop_obj in loops if loop_obj.order == 2 -%}
      c{{ loop_obj.id }}e
      {{- comma(loop) -}}
    {%- endfor %};

  // copy partial coefficient vectors from another instance (used to cache values when reevaluating special functions in higher precision)
  template <typename U> void copyCoeffs(const HelAmp{{ amp }}<U> &other);

  using Base::coeffVec;
  using Base::conjVecs;

protected:
  using Base::part;
  {%- for loop_obj in loops if loop_obj.order == 1 %}
  {%- for p in range(partials) %}
  using Base::fi{{ loop_obj.id }}{{ p }};
  using Base::sfi{{ loop_obj.id }}{{ p }};
  {%- endfor %}
  {%- endfor %}

  // sparse matrices for multiplication of coefficient and special function monomial vectors
  Eigen::SparseMatrix<T>
    {% for loop_obj in loops if loop_obj.order == 2 -%}
      {%- for p in range(partials) -%}
        m{{ loop_obj.id }}{{ p }}
        {{- comma(loop) -}}
      {%- endfor -%}
      {{- comma(loop) -}}
    {%- endfor %};

  // partial index vectors for coefficients and special function monomials
  const std::vector<int> 
    {% for loop_obj in loops if loop_obj.order == 2 -%}
      {%- for p in range(partials) -%}
        fi{{ loop_obj.id }}{{ p }}, sfi{{ loop_obj.id }}{{ p }}
        {{- comma(loop) -}}
      {%- endfor -%}
      {{- comma(loop) -}}
    {%- endfor %};
};

// {{ '}}' }}===========================================================================
// base class for 2L cases with parity odd and even contributions
// {{ '{{' }}===========================================================================

template <typename T> class HelAmp{{ amp }}Odd : public HelAmp{{ amp }}<T> {
  using Base = HelAmp{{ amp }}<T>;

public:
  explicit HelAmp{{ amp }}Odd(const std::vector<std::complex<T>> &sf_,
             {%- for loop_obj in loops if loop_obj.order %}
             int f{{ loop_obj.id }}l, 
             {% for p in range(partials) -%}int m{{ loop_obj.id }}{{ p }}i, int m{{ loop_obj.id }}{{ p }}j, {% endfor %}
             {% for p in range(partials) -%}const std::vector<int> &sfi{{ loop_obj.id }}{{ p }}_, {%- endfor %}
             {% for p in range(partials) -%}
             const std::vector<int> &fi{{ loop_obj.id }}{{ p }}_
             {{- comma(loop) -}}
             {%- endfor %}
             {{- comma(loop) -}}
             {% endfor %});

  // fill all partial coefficient vectors
  {%- for loop_obj in loops if loop_obj.parity_odd %}
  virtual void cacheCoeffs_{{ loop_obj.id }}() override final;
  {%- endfor %}

  {% for loop_obj in loops if loop_obj.parity_odd %}
    {%- for p in range(partials) %}
  std::complex<T> pA{{ loop_obj.id }}{{ p }}o();
    {%- endfor %}
  {% endfor %}

  std::vector<std::vector<std::complex<T>>>
    {% for loop_obj in loops if loop_obj.parity_odd -%}
      c{{ loop_obj.id }}o
      {{- comma(loop) -}}
    {%- endfor %};

  template <typename U> void copyCoeffs(const HelAmp{{ amp }}Odd<U> &other);

  using Base::coeffVec;
  using Base::conjVecs;
  {%- for loop_obj in loops if loop_obj.order %}
  using Base::f{{ loop_obj.id }};
  {%- endfor %}

protected:
  using Base::part;
  {%- for loop_obj in loops if loop_obj.order %}
  {%- for p in range(partials) %}
  using Base::fi{{ loop_obj.id }}{{ p }};
  using Base::sfi{{ loop_obj.id }}{{ p }};
  {%- endfor %}
  {%- endfor %}

  Eigen::SparseMatrix<T>
    {% for loop_obj in loops if loop_obj.parity_odd -%}
      {%- for p in range(partials) -%}
        m{{ loop_obj.id }}{{ p }}o
        {{- comma(loop) -}}
      {%- endfor -%}
      {{- comma(loop) -}}
    {%- endfor %};
};

// {{ '}}' }}===========================================================================
