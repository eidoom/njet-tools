#!/usr/bin/env bash

for f in exprs/P2_*_lr_bcfw_*_*.m; do
    g=${f#exprs/P2_}
    h=${g%_lr_bcfw_*_*.m}
    cd $h
    ../conv-form-y.sh ys.m
    ../conv-form-f.sh fs.m
    for i in {1..12}; do
        ../conv-form-eps-par.sh e$i.m
    done
    cd ..
done
