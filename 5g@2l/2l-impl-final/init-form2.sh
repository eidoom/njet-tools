#!/usr/bin/env bash

for f in new_exprs/P2_*_lr_bcfw_*_*.m; do
    g=${f#new_exprs/P2_}
    h=${g%_lr_bcfw_*_*.m}2
    cd $h
    ../conv-form-y.sh ys.m
    ../conv-form-f.sh fs.m
    for i in {1..12}; do
        ../conv-form-eps-par.sh e$i.m
    done
    cd ..
done
