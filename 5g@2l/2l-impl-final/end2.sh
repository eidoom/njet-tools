#!/usr/bin/env bash

dir=new_exprs
for f in $dir/P2_*_lr_bcfw_*_*.m; do
    g=${f#$dir/P2_}
    h=${g%_lr_bcfw_*_*.m}
    d=${h}2

    cp $d/coeffs.cpp output2/0q5g-analytic-2l-$h-coeffs.cpp

    for i in {1..12}; do
        cp $d/eps_p${i}.cpp output2/0q5g-analytic-2l-$h-p$i.cpp
    done

done
