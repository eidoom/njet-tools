#!/usr/bin/env bash

OUT=header.cpp

l=(07 11 13 14 19 21 22 25 26 28)

for i in ${l[@]}; do
    perl -p -e "s|HH|$i|g" template_header.cpp >> $OUT
done
