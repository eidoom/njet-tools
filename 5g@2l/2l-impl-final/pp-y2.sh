#!/usr/bin/env bash

dir=new_exprs
for f in $dir/P2_*_lr_bcfw_*_*.m; do
    g=${f#$dir/P2_}
    h=${g%_lr_bcfw_*_*.m}2
    cd $h

    i=`cat ys.ycount`
    for n in $(seq 1 $i); do
        m=$((n - 1))
        perl -pi -e "s|y\[$n\]|y[$m]|g" fs.cpp
    done

    cd ..
done
