#!/usr/bin/env bash

for f in exprs/P2_*_lr_bcfw_*_*.m; do
    g=${f#exprs/P2_}
    h=${g%_lr_bcfw_*_*.m}

    cp $h/coeffs.cpp output/0q5g-analytic-2l-$h-coeffs.cpp

    for i in {1..12}; do
        cp $h/eps_p${i}.cpp output/0q5g-analytic-2l-$h-p$i.cpp
    done

done
