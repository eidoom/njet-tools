#!/usr/bin/env bash

dir=new_exprs
for f in $dir/P2_*_lr_bcfw_*_*.m; do
    g=${f#$dir/P2_}
    h=${g%_lr_bcfw_*_*.m}2
    cd $h
    tform ys.frm >form.ys.log 2>form.ys.err &
    tform fs.frm >form.fs.log 2>form.fs.err &
    for i in {1..12}; do
        tform e${i}.frm >form.${i}.log 2>form.${i}.err &
    done
    cd ..
done
