(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/2l-impl-final"];


data={
Get["m+++m/P2_m+++m.m"],
Get["m++m+/P2_m++m+.m"],
Get["m+m++/P2_m+m++.m"],
Get["mm+++/P2_mm+++.m"],
Get["+++mm/P2_+++mm.m"],
Get["++m+m/P2_++m+m.m"],
Get["++mm+/P2_++mm+.m"],
Get["+m++m/P2_+m++m.m"],
Get["+m+m+/P2_+m+m+.m"],
Get["+mm++/P2_+mm++.m"]
};


ys=data[[;;,3]];


polys=Flatten[ys]/.{Rule[a_,b_]->b}


unique=DeleteDuplicates[Out[7]]


(polys//Length)-(unique//Length)


unique[[1]]
Position[polys,unique[[1]]]


unique[[4]]
Position[polys,unique[[4]]]


unique[[5]]
Position[polys,unique[[5]]]


unique[[6]]
Position[polys,unique[[6]]]


unique[[8]]
Position[polys,unique[[8]]]


unique[[10]]//Simplify
Position[polys,unique[[10]]]


Position[polys,unique[[#]]]&/@Range[10]


PolynomialRemainder


PolynomialReduce


GroebnerBasis


PolynomialRemainder[1+x-x^2,1+x,x]


(2-x)*(1+x)-1//Expand



