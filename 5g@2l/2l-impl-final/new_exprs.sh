#!/usr/bin/env bash

mkdir new_exprs
for f in exprs2/*; do
    b=${f#exprs2/}
    g=exprs/$b
    if [ -f $g ]; then 
        diff $f $g
    else
        cp $f new_exprs/
    fi
done
