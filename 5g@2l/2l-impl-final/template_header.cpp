    std::array<LoopResult<Eps5<T>>, 12> hA2gHH();
    void hA2gHHcoeffs();

    LoopValue<Eps5<T>> hA2gHHp1e();
    LoopValue<Eps5<T>> hA2gHHp2e();
    LoopValue<Eps5<T>> hA2gHHp3e();
    LoopValue<Eps5<T>> hA2gHHp4e();
    LoopValue<Eps5<T>> hA2gHHp5e();
    LoopValue<Eps5<T>> hA2gHHp6e();
    LoopValue<Eps5<T>> hA2gHHp7e();
    LoopValue<Eps5<T>> hA2gHHp8e();
    LoopValue<Eps5<T>> hA2gHHp9e();
    LoopValue<Eps5<T>> hA2gHHp10e();
    LoopValue<Eps5<T>> hA2gHHp11e();
    LoopValue<Eps5<T>> hA2gHHp12e();

    LoopValue<Eps5<T>> hA2gHHp1o();
    LoopValue<Eps5<T>> hA2gHHp2o();
    LoopValue<Eps5<T>> hA2gHHp3o();
    LoopValue<Eps5<T>> hA2gHHp4o();
    LoopValue<Eps5<T>> hA2gHHp5o();
    LoopValue<Eps5<T>> hA2gHHp6o();
    LoopValue<Eps5<T>> hA2gHHp7o();
    LoopValue<Eps5<T>> hA2gHHp8o();
    LoopValue<Eps5<T>> hA2gHHp9o();
    LoopValue<Eps5<T>> hA2gHHp10o();
    LoopValue<Eps5<T>> hA2gHHp11o();
    LoopValue<Eps5<T>> hA2gHHp12o();

