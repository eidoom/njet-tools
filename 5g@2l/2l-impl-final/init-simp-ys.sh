#!/usr/bin/env bash

l=2
dir=exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_}
    h=${g%_lr*.m}
    d=${h}
    mkdir $d
    cp -f $f $d/P${l}_$h.m
    m=simp-ys-${l}l-${h}.wl
    perl -p -e "s|HHHHH|$h|g;" simp-ys.wl >$d/$m
    cd $d
    math -script $m
    cd ..
done
