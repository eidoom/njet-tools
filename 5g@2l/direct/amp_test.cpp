#include <array>
#include <iostream>

#include "analytic/0q4g-analytic.h"
#include "ngluon2/Mom.h"

#include "hel.hpp"

template <typename T>
void run()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);

    const int n { 4 };

    const std::array<MOM<T>, n> mom {
        {
            {
                0.5000000000000000E+03,
                0.0000000000000000E+00,
                0.0000000000000000E+00,
                0.5000000000000000E+03,
            },
            {
                0.5000000000000000E+03,
                0.0000000000000000E+00,
                0.0000000000000000E+00,
                -0.5000000000000000E+03,
            },
            {
                0.4585787878854402E+03,
                0.1694532203096798E+03,
                0.3796536620781987E+03,
                -0.1935024746502525E+03,
            },
            {
                0.3640666207368177E+03,
                -0.1832986929319185E+02,
                -0.3477043013193671E+03,
                0.1063496077587081E+03,
            },
        },
    };

    const T scale { 1. };
    Amp0q4g_a<T> amp(scale);

    amp.setMomenta(mom.data());

    Helicity<n> hels { -1, -1, +1, +1 };

    const T amp_val { amp.born(hels.data()) };

    std::cout
        << '\n'
        << amp_val
        << '\n'
        << '\n';
}

// int main(int argc, char* argv[])
int main()
{
    run<double>();
}
