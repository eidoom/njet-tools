#include <array>
#include <cassert>
#include <chrono>
#include <complex>
#include <iostream>
#include <random>

#include "analytic/0q5g-2l-analytic.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

const int N { 5 };

template <typename T>
T invariant(const std::array<MOM<T>, N>& moms, const int i, const int j)
{
    return 2 * dot(moms[i], moms[j]);
}

template <typename T>
void loop(const int num_ps = 1)
{
    const int Nc { 3 };
    const T mur { 1. };
    const T scale { 1. };

    Amp0q5g_a<T> amp(scale);
    amp.setNf(0);
    amp.setNc(Nc);
    amp.setMuR2(mur * mur);

    std::random_device dev;
    std::mt19937_64 rng(dev());
    const T boundary { 0.01 };
    std::uniform_real_distribution<T> dist_y1(boundary, 1. - boundary);
    std::uniform_real_distribution<T> dist_a(boundary, M_PI - boundary);

    for (int i { 0 }; i < num_ps; ++i) {

        T y1 { dist_y1(rng) };
        std::uniform_real_distribution<T> dist_y2(boundary, 1. - y1 - boundary);
        T y2 { dist_y2(rng) };
        T theta { dist_a(rng) };
        T alpha { dist_a(rng) };

        const std::array<MOM<T>, N> moms { phase_space_point(y1, y2, theta, alpha) };

        amp.setMomenta(moms.data());

        std::array<Helicity<N>, 2> hels_list { { { -1, -1, +1, +1, +1 }, { +1, +1, -1, -1, -1 } } };
        // for (int i0 { -1 }; i0 < 2; i0 += 2) {
        //     for (int i1 { -1 }; i1 < 2; i1 += 2) {
        //         for (int i2 { -1 }; i2 < 2; i2 += 2) {
        //             for (int i3 { -1 }; i3 < 2; i3 += 2) {
        //                 for (int i4 { -1 }; i4 < 2; i4 += 2) {
        //                     const Helicity<N> hels { { i4, i3, i2, i1, i0 } };
        // const Helicity<N> hels { { -1, -1, +1, +1, +1 } };
        for (Helicity<N> hels : hels_list) {
            if (hels.order() == 1) {
                std::cout << hels << '\n';
                amp.setHelicity(hels.data());
                auto t0 { std::chrono::high_resolution_clock::now() };
                std::complex<T> num { amp.A0() };
                std::cout << " A0(12345)  =" << num << '\n';
                auto t1 { std::chrono::high_resolution_clock::now() };
                std::cout << "-A0(12345)^*=" << -std::conj(num) << '\n';
                auto t2 { std::chrono::high_resolution_clock::now() };

                std::cout
                    << "Time 1:  " << std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() << "us"
                    << '\n'
                    << "Time 2:  " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << "us"
                    << '\n';
            }
            // }
            // }
            // }
            // }
            // }
        }
        std::cout
            << '\n';
    }
    std::cout
        << '\n';
}

int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);
    std::cout << '\n';

    loop<double>(3);
}
