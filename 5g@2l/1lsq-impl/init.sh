#!/usr/bin/env bash

mkdir new_output

l=1
dir=new_exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_epsexp_}
    h=${g%_lr*.m}
    d=hel_${h}
    mkdir $d
    cp -f $f $d/P${l}_$h.m
    m=proc-${l}lsq-${h}.wl
    perl -p -e "s|HHHHH|$h|g;" proc-${l}lsq-HHHHH.wl >$d/$m
    cd $d
    math -script $m
    cd ..
done
