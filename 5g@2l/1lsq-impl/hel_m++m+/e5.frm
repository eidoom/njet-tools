#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tcr41;
S tcr42;
S tcr43;
S tci21;
S tcr44;
S tcr31;
S tcr45;
S tcr33;
S tcr32;
S f67;
S f65;
S f62;
S f187;
S f69;
S f74;
S f71;
S f72;
S f218;
S f287;
S f249;
S f232;
S f242;
S f88;
S f258;
S f259;
S f3;
S f81;
S f2;
S f1;
S f251;
S f85;
S f6;
S f95;
S f23;
S f24;
S f99;
S f274;
S f271;
S f48;
S f272;
S f46;
S f195;
S f59;
S f205;
S f193;
S f53;
S f50;
S f57;
S f198;
S f55;
S fvu1u2u9;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u204;
S fvu4u139;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u3;
S fvu4u83;
S fvu4u18;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu4u10;
S fvu4u120;
S fvu4u121;
S fvu3u51;
S fvu4u91;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu5 = (-2556*f1 + 2556*f2 + 2556*f6 - 36*f46 + 
      11*f48)/81 + ((-f88 + 12*f259)*fvu3u3)/9 + 
    ((f88 - 12*f259)*fvu3u4)/12 - (11*(f88 - 12*f259)*fvu3u5)/36 + 
    ((f88 - 12*f259)*fvu3u6)/9 + ((-f88 + 12*f259)*fvu3u7)/6 - 
    (8*(f88 - 12*f259)*fvu3u8)/9 + 
    ((f50 + 12*f271 - 12*f272)*fvu3u10)/6 + 
    ((-f88 + 12*f259)*fvu3u11)/9 + ((f88 - 12*f259)*fvu3u12)/36 + 
    ((f88 - 12*f259)*fvu3u13)/36 + ((-f88 + 12*f259)*fvu3u17)/12 + 
    ((2*f74 + f88 + 24*f198 - 12*f259)*fvu3u18)/12 + 
    ((f88 - 12*f259)*fvu3u24)/9 + ((f88 - 12*f259)*fvu3u51)/9 + 
    ((f88 - 12*f259)*fvu3u52)/18 - (2*(f88 - 12*f259)*fvu3u55)/9 + 
    ((-2*f88 + 3*f95 + 36*f258 + 24*f259 - 36*f287)*fvu3u56)/18 + 
    ((-f81 - f88 + 12*f187 - 12*f205 + 12*f218 - 12*f242 + 
       12*f259)*fvu3u57)/6 + ((f88 - 12*f259)*fvu3u59)/6 + 
    ((f88 - 12*f259)*fvu3u61)/3 + ((-f1 + f2 + f6)*fvu4u10)/4 + 
    ((-f1 + f2 + f6)*fvu4u18)/4 + ((f1 - f2 - f6)*fvu4u120)/4 + 
    ((-f1 + f2 + f6)*fvu4u121)/4 + ((-f1 + f2 + f6)*fvu4u204)/
     4 + ((-f1 + f2 + f6)*fvu1u1u1^4)/24 - 
    (11*(f88 - 12*f259)*fvu1u1u2^3)/54 + 
    ((-f1 + f2 + f6)*fvu1u1u3^4)/24 + 
    ((-f88 + 12*f259)*fvu1u1u6^3)/9 + 
    ((-f1 + f2 + f6)*fvu1u1u8^4)/24 - 
    (8*(f88 - 12*f259)*fvu1u1u9^3)/27 + 
    ((-f1 + f2 + f6)*fvu1u1u9^4)/24 + 
    fvu1u1u4^2*(((-f88 + 12*f259)*fvu1u1u5)/36 + 
      ((f88 - 12*f259)*fvu1u1u8)/36 - 
      (7*(f88 - 12*f259)*fvu1u1u9)/36) + 
    ((-f1 + f2 + f6)*fvu1u1u10^4)/24 + 
    ((-6*f88 + f99 - 6*f249 + f251 + 72*f258 + 72*f259 - 
       72*f287)*fvu2u1u13)/9 + 
    ((-f85 + f99 + 72*f187 - 72*f205 + 72*f218 - 72*f242 + 
       72*f258 - 72*f287)*fvu2u1u15)/9 + 
    ((f88 - 12*f259)*fvu1u2u6*tci11^2)/3 + 
    (521*(f1 - f2 - f6)*tci11^4)/4320 + 
    ((-432*f1 + 432*f2 + 432*f6 + f48 - 3*f62 - 24*f187 + 
       24*f205 - 24*f218 + 24*f242 - 24*f258 + 24*f271 - 
       24*f272 + 24*f287)*tci12)/27 + 
    ((6*f88 - f99 + 6*f249 - f251 - 72*f258 - 72*f259 + 
       72*f287)*fvu1u2u2*tci12)/9 - 
    (2*(f88 - 12*f259)*fvu2u1u2*tci12)/9 + 
    ((f88 - 12*f259)*fvu2u1u3*tci12)/3 + 
    (2*(f88 - 12*f259)*fvu2u1u11*tci12)/9 + 
    fvu1u1u3^3*((11*f1 - 11*f2 - 11*f6 + f53 + 12*f193)/18 + 
      ((f1 - f2 - f6)*tci12)/6) + 
    fvu1u1u8^3*((f88 - 12*f259)/18 + ((f1 - f2 - f6)*tci12)/
       6) + fvu1u1u1^3*((-f53 + f88 - 12*f193 - 12*f259)/18 + 
      ((f1 - f2 - f6)*tci12)/6) + 
    fvu1u1u3^2*((-72*f1 + 72*f2 + 72*f6 + f46 + f55 - 3*f59 + 
        3*f65 - 6*f88 + f99 - 3*f187 - 72*f193 + 3*f205 - 
        3*f218 + 3*f242 - 6*f249 + f251 + 69*f258 + 72*f259 + 
        3*f271 - 3*f272 - 69*f287)/18 + 
      ((f88 - 12*f259)*fvu1u1u4)/12 + ((f88 - 12*f259)*fvu1u1u6)/
       18 + ((f88 - 12*f259)*fvu1u1u8)/12 + 
      ((f88 - 12*f259)*fvu1u1u9)/9 + ((-f88 + 12*f259)*fvu1u1u10)/
       6 - (7*(f1 - f2 - f6)*tci11^2)/24 + 
      ((-11*f1 + 11*f2 + 11*f6 - f53 - 12*f193)*tci12)/6) + 
    fvu2u1u1*((-6*f69 + f72 + 6*f74 + 6*f88 + 72*f198 + 
        6*f249 - f251 - 72*f259)/9 - (2*(f88 - 12*f259)*tci12)/
       3) + fvu2u1u9*((-6*f23 + f24 - 6*f50 - 6*f69 + f72 + 
        6*f74 + 72*f198 - 72*f271 + 72*f272)/9 - 
      (2*(f88 - 12*f259)*tci12)/9) + 
    fvu1u1u5^2*(((-f88 + 12*f259)*fvu1u1u9)/12 + 
      ((f88 - 12*f259)*tci12)/36) + 
    fvu1u1u9^2*((-f85 + f99 + 75*f187 - 75*f205 + 75*f218 - 
        75*f242 + 75*f258 - 3*f271 + 3*f272 - 75*f287)/18 + 
      ((-f88 + 12*f259)*fvu1u1u10)/18 + 
      ((-f1 + f2 + f6)*tci11^2)/24 + 
      ((f88 - 12*f259)*tci12)/12) + fvu1u1u10^2*
     (f59/6 + ((-f1 + f2 + f6)*tci11^2)/24 + 
      ((f88 - 12*f259)*tci12)/6) + 
    fvu1u1u2^2*(((-f88 + 12*f259)*fvu1u1u4)/18 + 
      ((f88 - 12*f259)*fvu1u1u5)/18 + ((-f88 + 12*f259)*fvu1u1u8)/
       18 + ((-f88 + 12*f259)*fvu1u1u9)/9 + 
      (2*(f88 - 12*f259)*tci12)/9) + 
    fvu2u1u12*((6*f23 - f24 + 6*f50 + f85 - 72*f187 + 72*f205 - 
        72*f218 + 72*f242 + 72*f271 - 72*f272)/9 + 
      ((f88 - 12*f259)*tci12)/3) + 
    fvu1u1u6^2*(((f88 - 12*f259)*fvu1u1u9)/6 + 
      ((-f88 + 12*f259)*fvu1u1u10)/9 + ((-f88 + 12*f259)*tci12)/
       18) + fvu1u1u8^2*((6*f23 - f24 + 6*f50 - 3*f65 + f85 - 
        72*f187 + 72*f205 - 72*f218 + 72*f242 + 72*f271 - 
        72*f272)/18 + ((-f88 + 12*f259)*fvu1u1u9)/18 + 
      ((f88 - 12*f259)*fvu1u1u10)/6 - 
      (7*(f1 - f2 - f6)*tci11^2)/24 + 
      ((-f88 + 12*f259)*tci12)/2) + 
    fvu1u1u1^2*((-6*f23 + f24 - 6*f50 - f55 - 12*f69 + 2*f72 + 
        12*f74 + 6*f88 + 72*f193 + 144*f198 + 6*f249 - f251 - 
        72*f259 - 72*f271 + 72*f272)/18 + 
      ((f88 - 12*f259)*fvu1u1u2)/18 + ((-f88 + 12*f259)*fvu1u1u4)/
       18 + ((-f88 + 12*f259)*fvu1u1u8)/18 + 
      ((-f88 + 12*f259)*fvu1u1u9)/18 + 
      ((-f88 + 12*f259)*fvu1u1u10)/6 - 
      (7*(f1 - f2 - f6)*tci11^2)/24 + 
      ((3*f53 - f88 + 36*f193 + 12*f259)*tci12)/18) + 
    fvu1u2u9*(((-f88 + 12*f259)*tci11^2)/3 + 
      ((-6*f23 + f24 - 6*f50 - f85 + 72*f187 - 72*f205 + 
         72*f218 - 72*f242 - 72*f271 + 72*f272)*tci12)/9) + 
    tci11^2*((-504*f1 + 504*f2 + 504*f6 + 7*f46 - 18*f59 - 
        12*f69 + 2*f72 + 12*f74 + 2*f85 + 2*f99 - 162*f187 + 
        144*f198 + 162*f205 - 162*f218 + 162*f242 + 126*f258 + 
        18*f271 - 18*f272 - 126*f287)/108 + 
      ((-33*f1 + 33*f2 + 33*f6 - 3*f50 + 2*f74 + f81 + 5*f88 + 
         f95 - 12*f187 + 24*f198 + 12*f205 - 12*f218 + 12*f242 + 
         12*f258 - 60*f259 - 36*f271 + 36*f272 - 12*f287)*
        tci12)/36) + fvu1u1u5*(((-f88 + 12*f259)*fvu1u1u9^2)/12 + 
      ((-f88 + 12*f259)*fvu2u1u4)/9 + 
      ((-f88 + 12*f259)*tci11^2)/27 + 
      ((-f88 + 12*f259)*fvu1u1u9*tci12)/6) + 
    fvu1u1u6*(((f88 - 12*f259)*fvu1u1u9^2)/6 - 
      (2*(f88 - 12*f259)*fvu2u1u14)/9 + 
      (2*(f88 - 12*f259)*tci11^2)/27 + 
      ((f88 - 12*f259)*fvu1u1u9*tci12)/3 - 
      (2*(f88 - 12*f259)*fvu1u1u10*tci12)/9) + 
    fvu1u1u4*(((-f88 + 12*f259)*fvu1u1u5^2)/36 + 
      ((f88 - 12*f259)*fvu1u1u8^2)/36 - 
      (7*(f88 - 12*f259)*fvu1u1u9^2)/36 + 
      ((f88 - 12*f259)*fvu2u1u2)/9 + ((-f88 + 12*f259)*fvu2u1u3)/
       3 - (2*(f88 - 12*f259)*fvu2u1u11)/9 + 
      ((f88 - 12*f259)*tci11^2)/27 + 
      (7*(f88 - 12*f259)*fvu1u1u9*tci12)/18 + 
      ((-f88 + 12*f259)*fvu1u2u6*tci12)/3 + 
      fvu1u1u5*(((f88 - 12*f259)*fvu1u1u9)/6 + 
        ((f88 - 12*f259)*tci12)/18) + 
      fvu1u1u8*(((-f88 + 12*f259)*fvu1u1u9)/9 + 
        ((-f88 + 12*f259)*tci12)/18)) + 
    fvu1u1u2*((2*(f88 - 12*f259)*fvu1u1u4^2)/9 + 
      ((f88 - 12*f259)*fvu1u1u5^2)/9 + 
      ((f88 - 12*f259)*fvu1u1u8^2)/18 + 
      (5*(f88 - 12*f259)*fvu1u1u9^2)/18 + 
      ((-f88 + 12*f259)*fvu2u1u3)/3 + ((-f88 + 12*f259)*fvu2u1u4)/
       9 - (5*(f88 - 12*f259)*fvu2u1u11)/9 + 
      ((f88 - 12*f259)*tci11^2)/27 + 
      ((f88 - 12*f259)*fvu1u1u5*tci12)/9 - 
      (2*(f88 - 12*f259)*fvu1u1u9*tci12)/9 + 
      ((-f88 + 12*f259)*fvu1u2u6*tci12)/3 + 
      fvu1u1u8*(((f88 - 12*f259)*fvu1u1u9)/9 + 
        ((-f88 + 12*f259)*tci12)/9) + 
      fvu1u1u4*(((-f88 + 12*f259)*fvu1u1u5)/9 + 
        ((f88 - 12*f259)*fvu1u1u8)/9 + (2*(f88 - 12*f259)*
          fvu1u1u9)/9 + ((-f88 + 12*f259)*tci12)/9)) + 
    ((-f1 + f2 + f6)*tcr11^4)/24 + 
    tci11^2*((-7*f50 - 2*f74 - 6*f81 + 7*f88 - 6*f95 + 
        72*f187 - 24*f198 - 72*f205 + 72*f218 - 72*f242 - 
        72*f258 - 84*f259 - 84*f271 + 84*f272 + 72*f287)/36 + 
      ((f1 - f2 - f6)*tci12)/2)*tcr12 + 
    (5*(f1 - f2 - f6)*tci11^2*tcr12^2)/18 + 
    ((f50 + 2*f74 - f88 + 24*f198 + 12*f259 + 12*f271 - 
        12*f272)/9 - 2*(f1 - f2 - f6)*tci12)*tcr12^3 + 
    (19*(f1 - f2 - f6)*tcr12^4)/36 + 
    tcr11^3*((f50 - 4*f74 - 2*f88 - 48*f198 + 24*f259 + 
        12*f271 - 12*f272)/18 + ((-f1 + f2 + f6)*tci12)/3 + 
      ((-f1 + f2 + f6)*tcr12)/3) + 
    ((f1 - f2 - f6)*tci11^2 - 
      (2*(3*f50 - 3*f74 - 8*f88 - 36*f198 + 96*f259 + 36*f271 - 
         36*f272)*tci12)/9)*tcr21 + 
    tcr11^2*((5*(f1 - f2 - f6)*tci11^2)/4 + 
      ((-9*f50 + 6*f74 + 10*f88 + 72*f198 - 120*f259 - 108*f271 + 
         108*f272)*tci12)/18 + (f1 - f2 - f6)*tci12*
       tcr12 + ((f1 - f2 - f6)*tcr12^2)/2 + 
      (-f1 + f2 + f6)*tcr21) + 
    ((-f50 - 2*f74 + f88 - 24*f198 - 12*f259 - 12*f271 + 
        12*f272)/3 + 6*(f1 - f2 - f6)*tci12)*tcr31 + 
    ((-f50 - 2*f74 + f88 - 24*f198 - 12*f259 - 12*f271 + 
        12*f272)/12 + (3*(f1 - f2 - f6)*tci12)/2)*tcr32 + 
    ((616*f1 - 616*f2 - 616*f6 + 11*f50 + 74*f74 - 18*f81 - 
        11*f88 - 18*f95 + 216*f187 + 888*f198 - 216*f205 + 
        216*f218 - 216*f242 - 216*f258 + 132*f259 + 132*f271 - 
        132*f272 + 216*f287)/72 - (9*(f1 - f2 - f6)*tci12)/4)*
     tcr33 + fvu1u1u10*(-f62/9 - (2*(f88 - 12*f259)*fvu2u1u14)/
       9 - (4*(f88 - 12*f259)*fvu2u1u15)/9 + 
      ((f85 - 72*f187 + 72*f205 - 72*f218 + 72*f242)*tci12)/9 + 
      tci11^2*((-f88 + 12*f259)/9 + ((f1 - f2 - f6)*tci12)/
         24) - (7*(f1 - f2 - f6)*tcr33)/3) + 
    fvu1u1u9*((-8*(f187 - f205 + f218 - f242 + f258 - f271 + 
         f272 - f287))/9 + ((f88 - 12*f259)*fvu1u1u10^2)/6 - 
      (7*(f88 - 12*f259)*fvu2u1u11)/9 + 
      ((-f88 + 12*f259)*fvu2u1u12)/3 - 
      (4*(f88 - 12*f259)*fvu2u1u15)/9 + 
      ((f99 + 72*f258 - 72*f287)*tci12)/9 + 
      ((f88 - 12*f259)*fvu1u2u9*tci12)/3 + 
      tci11^2*((-7*(f88 - 12*f259))/54 + 
        ((f1 - f2 - f6)*tci12)/24) + 
      fvu1u1u10*((f85 - 72*f187 + 72*f205 - 72*f218 + 72*f242)/
         9 + ((f1 - f2 - f6)*tci11^2)/24 + 
        ((-f88 + 12*f259)*tci12)/9) - 
      (7*(f1 - f2 - f6)*tcr33)/3) + 
    fvu1u1u1*((f57 - 432*f193)/27 + 
      (2*(f88 - 12*f259)*fvu1u1u2^2)/9 + 
      ((-f88 + 12*f259)*fvu1u1u3^2)/6 + 
      ((-f88 + 12*f259)*fvu1u1u4^2)/9 + 
      ((-f88 + 12*f259)*fvu1u1u8^2)/9 + 
      ((-f88 + 12*f259)*fvu1u1u9^2)/9 + 
      ((f88 - 12*f259)*fvu2u1u1)/3 + ((f88 - 12*f259)*fvu2u1u2)/
       9 + ((f88 - 12*f259)*fvu2u1u9)/9 + 
      (2*(f88 - 12*f259)*fvu2u1u11)/9 + 
      ((6*f23 - f24 + 6*f50 + f55 - 6*f88 - 72*f193 - 6*f249 + 
         f251 + 72*f259 + 72*f271 - 72*f272)*tci12)/9 + 
      tci11^2*((-21*f53 - 46*f88 - 252*f193 + 552*f259)/108 + 
        ((f1 - f2 - f6)*tci12)/6) + 
      fvu1u1u2*(((-f88 + 12*f259)*fvu1u1u4)/9 + 
        ((-f88 + 12*f259)*fvu1u1u8)/9 + 
        ((-f88 + 12*f259)*fvu1u1u9)/9 + ((f88 - 12*f259)*tci12)/
         9) + fvu1u1u4*(((f88 - 12*f259)*fvu1u1u8)/9 + 
        ((f88 - 12*f259)*fvu1u1u9)/9 + (2*(f88 - 12*f259)*
          tci12)/9) + fvu1u1u8*((6*f69 - f72 - 6*f74 - 72*f198)/
         9 + ((f88 - 12*f259)*fvu1u1u9)/9 + 
        ((f1 - f2 - f6)*tci11^2)/24 + 
        (2*(f88 - 12*f259)*tci12)/9) + 
      fvu1u1u3*((6*f69 - f72 - 6*f74 - 72*f198)/9 + 
        ((f1 - f2 - f6)*tci11^2)/24 + 
        ((f88 - 12*f259)*tci12)/3) + fvu1u1u10*
       ((-6*f88 - 6*f249 + f251 + 72*f259)/9 + 
        ((-f1 + f2 + f6)*tci11^2)/24 + 
        ((f88 - 12*f259)*tci12)/3) + 
      fvu1u1u9*((6*f23 - f24 + 6*f50 + 72*f271 - 72*f272)/9 + 
        ((-f1 + f2 + f6)*tci11^2)/24 + 
        ((-f88 + 12*f259)*tci12)/9) - 
      (7*(f1 - f2 - f6)*tcr33)/3) + 
    fvu1u1u3*((432*f1 - 432*f2 - 432*f6 - f48 - f57 + 3*f62 - 
        3*f67 + 24*f187 + 432*f193 - 24*f205 + 24*f218 - 
        24*f242 + 24*f258 - 24*f271 + 24*f272 - 24*f287)/27 + 
      ((f88 - 12*f259)*fvu1u1u4^2)/12 + 
      ((f88 - 12*f259)*fvu1u1u6^2)/18 + 
      ((f88 - 12*f259)*fvu1u1u8^2)/12 + 
      ((f88 - 12*f259)*fvu1u1u9^2)/9 + 
      ((-f88 + 12*f259)*fvu1u1u10^2)/6 + 
      ((f88 - 12*f259)*fvu2u1u1)/3 + 
      ((72*f1 - 72*f2 - 72*f6 - f46 - f55 + 3*f59 - 3*f65 + 
         3*f187 + 72*f193 - 3*f205 + 3*f218 - 3*f242 + 3*f258 - 
         3*f271 + 3*f272 - 3*f287)*tci12)/9 + 
      tci11^2*((77*f1 - 77*f2 - 77*f6 + 7*f53 + f88 + 
          84*f193 - 12*f259)/36 + ((f1 - f2 - f6)*tci12)/4) + 
      fvu1u1u9*((-f99 - 72*f258 + 72*f287)/9 + 
        ((f88 - 12*f259)*fvu1u1u10)/9 + 
        ((-f1 + f2 + f6)*tci11^2)/24 - 
        (2*(f88 - 12*f259)*tci12)/9) + 
      fvu1u1u10*((6*f88 + 6*f249 - f251 - 72*f259)/9 + 
        ((f1 - f2 - f6)*tci11^2)/24 + 
        ((f88 - 12*f259)*tci12)/3) + 
      fvu1u1u6*(((-f88 + 12*f259)*fvu1u1u9)/3 + 
        (2*(f88 - 12*f259)*fvu1u1u10)/9 + 
        ((-f88 + 12*f259)*tci12)/9) + 
      fvu1u1u4*(((-f88 + 12*f259)*fvu1u1u8)/6 + 
        ((-f88 + 12*f259)*tci12)/6) + 
      fvu1u1u8*((-6*f69 + f72 + 6*f74 + 72*f198)/9 + 
        ((-f1 + f2 + f6)*tci11^2)/24 + 
        ((-f88 + 12*f259)*tci12)/6) - 
      (7*(f1 - f2 - f6)*tcr33)/3) + 
    fvu1u1u8*(f67/9 + ((f88 - 12*f259)*fvu1u1u9^2)/9 + 
      ((f88 - 12*f259)*fvu2u1u9)/9 - 
      (2*(f88 - 12*f259)*fvu2u1u11)/9 + 
      ((-f88 + 12*f259)*fvu2u1u12)/3 + (f65*tci12)/3 + 
      ((f88 - 12*f259)*fvu1u2u9*tci12)/3 + 
      tci11^2*((73*(f88 - 12*f259))/108 + 
        ((f1 - f2 - f6)*tci12)/4) + 
      fvu1u1u9*((-6*f23 + f24 - 6*f50 - 72*f271 + 72*f272)/9 + 
        ((f1 - f2 - f6)*tci11^2)/24 - 
        (2*(f88 - 12*f259)*tci12)/9) + 
      fvu1u1u10*((-f85 + 72*f187 - 72*f205 + 72*f218 - 72*f242)/
         9 + ((-f1 + f2 + f6)*tci11^2)/24 + 
        ((-f88 + 12*f259)*tci12)/3) - 
      (7*(f1 - f2 - f6)*tcr33)/3) + 
    tcr11*(tci11^2*((21*f50 + 25*f88 - 300*f259 + 252*f271 - 
          252*f272)/108 - (11*(f1 - f2 - f6)*tci12)/12) + 
      ((-3*(f1 - f2 - f6)*tci11^2)/2 + 
        ((f50 - f88 + 12*f259 + 12*f271 - 12*f272)*tci12)/3)*
       tcr12 + ((-6*f74 - 5*f88 - 72*f198 + 60*f259)/9 + 
        2*(f1 - f2 - f6)*tci12)*tcr21 - 
      2*(f1 - f2 - f6)*tcr31 + ((-f1 + f2 + f6)*tcr32)/
       2 + (37*(f1 - f2 - f6)*tcr33)/12) + 
    (f1 - f2 - f6)*tcr41 + (2*(f1 - f2 - f6)*tcr42)/3 + 
    ((f1 - f2 - f6)*tcr43)/4 + 2*(f1 - f2 - f6)*tcr44 + 
    ((f1 - f2 - f6)*tcr45)/2;
L ieu1ueu5 = 
   (-432*f1 + 432*f2 + 432*f6 + f48)/27 + 
    ((-f1 + f2 + f6)*fvu3u3)/3 + ((f1 - f2 - f6)*fvu3u4)/4 - 
    (11*(f1 - f2 - f6)*fvu3u5)/12 + ((f1 - f2 - f6)*fvu3u6)/3 + 
    ((-f1 + f2 + f6)*fvu3u7)/2 - (8*(f1 - f2 - f6)*fvu3u8)/3 + 
    ((-f1 + f2 + f6)*fvu3u10)/2 + ((-f1 + f2 + f6)*fvu3u11)/3 + 
    ((f1 - f2 - f6)*fvu3u12)/12 + ((f1 - f2 - f6)*fvu3u13)/12 + 
    ((-f1 + f2 + f6)*fvu3u17)/4 + ((-f1 + f2 + f6)*fvu3u18)/4 + 
    ((f1 - f2 - f6)*fvu3u24)/3 + ((f1 - f2 - f6)*fvu3u51)/3 + 
    ((f1 - f2 - f6)*fvu3u52)/6 - (2*(f1 - f2 - f6)*fvu3u55)/3 + 
    ((f1 - f2 - f6)*fvu3u56)/6 + (-f1 + f2 + f6)*fvu3u57 + 
    ((f1 - f2 - f6)*fvu3u59)/2 + (f1 - f2 - f6)*fvu3u61 + 
    ((f1 - f2 - f6)*fvu1u1u1^3)/3 - 
    (11*(f1 - f2 - f6)*fvu1u1u2^3)/18 + 
    ((f1 - f2 - f6)*fvu1u1u3^3)/6 + 
    ((-f1 + f2 + f6)*fvu1u1u6^3)/3 + 
    ((f1 - f2 - f6)*fvu1u1u8^3)/3 - 
    (13*(f1 - f2 - f6)*fvu1u1u9^3)/18 + 
    fvu1u1u4^2*(((-f1 + f2 + f6)*fvu1u1u5)/12 + 
      ((f1 - f2 - f6)*fvu1u1u8)/12 - 
      (7*(f1 - f2 - f6)*fvu1u1u9)/12) + 
    ((f1 - f2 - f6)*fvu1u1u10^3)/6 + 
    ((-f88 + f95 + 12*f258 + 12*f259 - 12*f287)*fvu2u1u13)/3 + 
    ((-f81 + f95 + 12*f187 - 12*f205 + 12*f218 - 12*f242 + 
       12*f258 - 12*f287)*fvu2u1u15)/3 + (f1 - f2 - f6)*
     fvu1u2u6*tci11^2 + ((-72*f1 + 72*f2 + 72*f6 + f46 - 
       3*f59 - 3*f187 + 3*f205 - 3*f218 + 3*f242 - 3*f258 + 
       3*f271 - 3*f272 + 3*f287)*tci12)/9 + 
    ((f1 - f2 - f6)*fvu1u1u10^2*tci12)/2 + 
    ((f88 - f95 - 12*f258 - 12*f259 + 12*f287)*fvu1u2u2*
      tci12)/3 - (2*(f1 - f2 - f6)*fvu2u1u2*tci12)/3 + 
    (f1 - f2 - f6)*fvu2u1u3*tci12 + 
    (2*(f1 - f2 - f6)*fvu2u1u11*tci12)/3 + 
    fvu2u1u1*((f74 + f88 + 12*f198 - 12*f259)/3 - 
      2*(f1 - f2 - f6)*tci12) + fvu1u1u8^2*
     ((f50 + f81 - 12*f187 + 12*f205 - 12*f218 + 12*f242 + 
        12*f271 - 12*f272)/6 + ((-f1 + f2 + f6)*fvu1u1u9)/6 + 
      ((f1 - f2 - f6)*fvu1u1u10)/2 - 2*(f1 - f2 - f6)*
       tci12) + fvu2u1u9*((-f50 + f74 + 12*f198 - 12*f271 + 
        12*f272)/3 - (2*(f1 - f2 - f6)*tci12)/3) + 
    fvu1u1u1^2*((-f50 + f53 + 2*f74 + f88 + 12*f193 + 24*f198 - 
        12*f259 - 12*f271 + 12*f272)/6 + 
      ((f1 - f2 - f6)*fvu1u1u2)/6 + 
      ((-f1 + f2 + f6)*fvu1u1u4)/6 + 
      ((-f1 + f2 + f6)*fvu1u1u8)/6 + 
      ((-f1 + f2 + f6)*fvu1u1u9)/6 + 
      ((-f1 + f2 + f6)*fvu1u1u10)/2 - 
      (2*(f1 - f2 - f6)*tci12)/3) + 
    fvu1u1u5^2*(((-f1 + f2 + f6)*fvu1u1u9)/4 + 
      ((f1 - f2 - f6)*tci12)/12) + 
    fvu1u1u9^2*((-f81 + f95 + 12*f187 - 12*f205 + 12*f218 - 
        12*f242 + 12*f258 - 12*f287)/6 + 
      ((-f1 + f2 + f6)*fvu1u1u10)/6 + 
      ((f1 - f2 - f6)*tci12)/4) + 
    fvu1u1u2^2*(((-f1 + f2 + f6)*fvu1u1u4)/6 + 
      ((f1 - f2 - f6)*fvu1u1u5)/6 + 
      ((-f1 + f2 + f6)*fvu1u1u8)/6 + 
      ((-f1 + f2 + f6)*fvu1u1u9)/3 + 
      (2*(f1 - f2 - f6)*tci12)/3) + 
    fvu2u1u12*((f50 + f81 - 12*f187 + 12*f205 - 12*f218 + 
        12*f242 + 12*f271 - 12*f272)/3 + (f1 - f2 - f6)*
       tci12) + tci11^2*((-77*f1 + 77*f2 + 77*f6 + 2*f74 + 
        2*f81 + 2*f95 - 24*f187 + 24*f198 + 24*f205 - 24*f218 + 
        24*f242 + 24*f258 - 24*f287)/36 + 
      ((-f1 + f2 + f6)*tci12)/12) + 
    fvu1u1u6^2*(((f1 - f2 - f6)*fvu1u1u9)/2 + 
      ((-f1 + f2 + f6)*fvu1u1u10)/3 + 
      ((-f1 + f2 + f6)*tci12)/6) + 
    fvu1u1u3^2*((-11*f1 + 11*f2 + 11*f6 - f53 - f88 + f95 - 
        12*f193 + 12*f258 + 12*f259 - 12*f287)/6 + 
      ((f1 - f2 - f6)*fvu1u1u4)/4 + ((f1 - f2 - f6)*fvu1u1u6)/
       6 + ((f1 - f2 - f6)*fvu1u1u8)/4 + 
      ((f1 - f2 - f6)*fvu1u1u9)/3 + 
      ((-f1 + f2 + f6)*fvu1u1u10)/2 + 
      ((-f1 + f2 + f6)*tci12)/2) + 
    fvu1u1u10*(-f59/3 - (2*(f1 - f2 - f6)*fvu2u1u14)/3 - 
      (4*(f1 - f2 - f6)*fvu2u1u15)/3 + 
      ((-f1 + f2 + f6)*tci11^2)/4 + 
      ((f81 - 12*f187 + 12*f205 - 12*f218 + 12*f242)*tci12)/
       3) + fvu1u2u9*((-f1 + f2 + f6)*tci11^2 + 
      ((-f50 - f81 + 12*f187 - 12*f205 + 12*f218 - 12*f242 - 
         12*f271 + 12*f272)*tci12)/3) + 
    fvu1u1u5*(((-f1 + f2 + f6)*fvu1u1u9^2)/4 + 
      ((-f1 + f2 + f6)*fvu2u1u4)/3 + 
      ((-f1 + f2 + f6)*tci11^2)/9 + 
      ((-f1 + f2 + f6)*fvu1u1u9*tci12)/2) + 
    fvu1u1u6*(((f1 - f2 - f6)*fvu1u1u9^2)/2 - 
      (2*(f1 - f2 - f6)*fvu2u1u14)/3 + 
      (2*(f1 - f2 - f6)*tci11^2)/9 + (f1 - f2 - f6)*
       fvu1u1u9*tci12 - (2*(f1 - f2 - f6)*fvu1u1u10*tci12)/
       3) + fvu1u1u4*(((-f1 + f2 + f6)*fvu1u1u5^2)/12 + 
      ((f1 - f2 - f6)*fvu1u1u8^2)/12 - 
      (7*(f1 - f2 - f6)*fvu1u1u9^2)/12 + 
      ((f1 - f2 - f6)*fvu2u1u2)/3 + (-f1 + f2 + f6)*
       fvu2u1u3 - (2*(f1 - f2 - f6)*fvu2u1u11)/3 + 
      ((f1 - f2 - f6)*tci11^2)/9 + 
      (7*(f1 - f2 - f6)*fvu1u1u9*tci12)/6 + 
      (-f1 + f2 + f6)*fvu1u2u6*tci12 + 
      fvu1u1u5*(((f1 - f2 - f6)*fvu1u1u9)/2 + 
        ((f1 - f2 - f6)*tci12)/6) + 
      fvu1u1u8*(((-f1 + f2 + f6)*fvu1u1u9)/3 + 
        ((-f1 + f2 + f6)*tci12)/6)) + 
    fvu1u1u9*((-f187 + f205 - f218 + f242 - f258 + f271 - 
        f272 + f287)/3 + ((f1 - f2 - f6)*fvu1u1u10^2)/2 - 
      (7*(f1 - f2 - f6)*fvu2u1u11)/3 + (-f1 + f2 + f6)*
       fvu2u1u12 - (4*(f1 - f2 - f6)*fvu2u1u15)/3 - 
      (11*(f1 - f2 - f6)*tci11^2)/36 + 
      ((f95 + 12*f258 - 12*f287)*tci12)/3 + 
      (f1 - f2 - f6)*fvu1u2u9*tci12 + 
      fvu1u1u10*((f81 - 12*f187 + 12*f205 - 12*f218 + 12*f242)/
         3 + ((-f1 + f2 + f6)*tci12)/3)) + 
    fvu1u1u1*((f55 - 72*f193)/9 + (2*(f1 - f2 - f6)*fvu1u1u2^2)/
       3 + ((-f1 + f2 + f6)*fvu1u1u3^2)/2 + 
      ((-f1 + f2 + f6)*fvu1u1u4^2)/3 + 
      ((-f1 + f2 + f6)*fvu1u1u8^2)/3 + 
      ((-f1 + f2 + f6)*fvu1u1u9^2)/3 + (f1 - f2 - f6)*
       fvu2u1u1 + ((f1 - f2 - f6)*fvu2u1u2)/3 + 
      ((f1 - f2 - f6)*fvu2u1u9)/3 + 
      (2*(f1 - f2 - f6)*fvu2u1u11)/3 - 
      (25*(f1 - f2 - f6)*tci11^2)/36 + 
      ((f50 - f53 - f88 - 12*f193 + 12*f259 + 12*f271 - 
         12*f272)*tci12)/3 + fvu1u1u2*
       (((-f1 + f2 + f6)*fvu1u1u4)/3 + 
        ((-f1 + f2 + f6)*fvu1u1u8)/3 + 
        ((-f1 + f2 + f6)*fvu1u1u9)/3 + 
        ((f1 - f2 - f6)*tci12)/3) + 
      fvu1u1u8*((-f74 - 12*f198)/3 + ((f1 - f2 - f6)*fvu1u1u9)/
         3 + (2*(f1 - f2 - f6)*tci12)/3) + 
      fvu1u1u4*(((f1 - f2 - f6)*fvu1u1u8)/3 + 
        ((f1 - f2 - f6)*fvu1u1u9)/3 + 
        (2*(f1 - f2 - f6)*tci12)/3) + 
      fvu1u1u3*((-f74 - 12*f198)/3 + (f1 - f2 - f6)*tci12) + 
      fvu1u1u10*((-f88 + 12*f259)/3 + (f1 - f2 - f6)*tci12) + 
      fvu1u1u9*((f50 + 12*f271 - 12*f272)/3 + 
        ((-f1 + f2 + f6)*tci12)/3)) + 
    fvu1u1u2*((2*(f1 - f2 - f6)*fvu1u1u4^2)/3 + 
      ((f1 - f2 - f6)*fvu1u1u5^2)/3 + 
      ((f1 - f2 - f6)*fvu1u1u8^2)/6 + 
      (5*(f1 - f2 - f6)*fvu1u1u9^2)/6 + (-f1 + f2 + f6)*
       fvu2u1u3 + ((-f1 + f2 + f6)*fvu2u1u4)/3 - 
      (5*(f1 - f2 - f6)*fvu2u1u11)/3 + 
      ((f1 - f2 - f6)*tci11^2)/9 + 
      ((f1 - f2 - f6)*fvu1u1u5*tci12)/3 - 
      (2*(f1 - f2 - f6)*fvu1u1u9*tci12)/3 + 
      (-f1 + f2 + f6)*fvu1u2u6*tci12 + 
      fvu1u1u8*(((f1 - f2 - f6)*fvu1u1u9)/3 + 
        ((-f1 + f2 + f6)*tci12)/3) + 
      fvu1u1u4*(((-f1 + f2 + f6)*fvu1u1u5)/3 + 
        ((f1 - f2 - f6)*fvu1u1u8)/3 + 
        (2*(f1 - f2 - f6)*fvu1u1u9)/3 + 
        ((-f1 + f2 + f6)*tci12)/3)) + 
    fvu1u1u3*((72*f1 - 72*f2 - 72*f6 - f46 - f55 + 3*f59 - 
        3*f65 + 3*f187 + 72*f193 - 3*f205 + 3*f218 - 3*f242 + 
        3*f258 - 3*f271 + 3*f272 - 3*f287)/9 + 
      ((f1 - f2 - f6)*fvu1u1u4^2)/4 + 
      ((f1 - f2 - f6)*fvu1u1u6^2)/6 + 
      ((f1 - f2 - f6)*fvu1u1u8^2)/4 + 
      ((f1 - f2 - f6)*fvu1u1u9^2)/3 + 
      ((-f1 + f2 + f6)*fvu1u1u10^2)/2 + (f1 - f2 - f6)*
       fvu2u1u1 + (2*(f1 - f2 - f6)*tci11^2)/3 + 
      ((11*f1 - 11*f2 - 11*f6 + f53 + 12*f193)*tci12)/3 + 
      fvu1u1u9*((-f95 - 12*f258 + 12*f287)/3 + 
        ((f1 - f2 - f6)*fvu1u1u10)/3 - 
        (2*(f1 - f2 - f6)*tci12)/3) + 
      fvu1u1u10*((f88 - 12*f259)/3 + (f1 - f2 - f6)*tci12) + 
      fvu1u1u6*((-f1 + f2 + f6)*fvu1u1u9 + 
        (2*(f1 - f2 - f6)*fvu1u1u10)/3 + 
        ((-f1 + f2 + f6)*tci12)/3) + 
      fvu1u1u8*((f74 + 12*f198)/3 + ((-f1 + f2 + f6)*tci12)/
         2) + fvu1u1u4*(((-f1 + f2 + f6)*fvu1u1u8)/2 + 
        ((-f1 + f2 + f6)*tci12)/2)) + 
    fvu1u1u8*(f65/3 + ((f1 - f2 - f6)*fvu1u1u9^2)/3 + 
      ((f1 - f2 - f6)*fvu2u1u9)/3 - 
      (2*(f1 - f2 - f6)*fvu2u1u11)/3 + (-f1 + f2 + f6)*
       fvu2u1u12 + (47*(f1 - f2 - f6)*tci11^2)/18 + 
      (f1 - f2 - f6)*fvu1u2u9*tci12 + 
      fvu1u1u9*((-f50 - 12*f271 + 12*f272)/3 - 
        (2*(f1 - f2 - f6)*tci12)/3) + 
      fvu1u1u10*((-f81 + 12*f187 - 12*f205 + 12*f218 - 12*f242)/
         3 + (-f1 + f2 + f6)*tci12)) + 
    (13*(f1 - f2 - f6)*tci12*tcr11^2)/6 + 
    ((f1 - f2 - f6)*tcr11^3)/6 + 
    ((f1 - f2 - f6)*tci11^2*tcr12)/3 - 
    (4*(f1 - f2 - f6)*tcr12^3)/3 + 
    (16*(f1 - f2 - f6)*tci12*tcr21)/3 + 
    tcr11*(((f1 - f2 - f6)*tci11^2)/9 - 
      2*(f1 - f2 - f6)*tci12*tcr12 + 
      ((f1 - f2 - f6)*tcr21)/3) + 4*(f1 - f2 - f6)*
     tcr31 + (f1 - f2 - f6)*tcr32 + 
    (37*(f1 - f2 - f6)*tcr33)/6;
L ieu0ueu5 = 
   (-72*f1 + 72*f2 + 72*f6 + f46)/9 + 
    ((-f1 + f2 + f6)*fvu1u1u1^2)/2 + 
    ((-f1 + f2 + f6)*fvu1u1u3^2)/2 + 
    ((-f1 + f2 + f6)*fvu1u1u8^2)/2 + 
    ((-f1 + f2 + f6)*fvu1u1u9^2)/2 + 
    ((-f1 + f2 + f6)*fvu1u1u10^2)/2 - 
    (7*(f1 - f2 - f6)*tci11^2)/4 - 
    (11*(f1 - f2 - f6)*tci12)/3 + (f1 - f2 - f6)*fvu1u1u10*
     tci12 + fvu1u1u9*((f1 - f2 - f6)*fvu1u1u10 + 
      (f1 - f2 - f6)*tci12) + 
    fvu1u1u3*((11*f1 - 11*f2 - 11*f6 + f53 + 12*f193)/3 + 
      (-f1 + f2 + f6)*fvu1u1u8 + (-f1 + f2 + f6)*fvu1u1u9 + 
      (f1 - f2 - f6)*fvu1u1u10 + (f1 - f2 - f6)*tci12) + 
    fvu1u1u8*((f1 - f2 - f6)*fvu1u1u9 + (-f1 + f2 + f6)*
       fvu1u1u10 + (f1 - f2 - f6)*tci12) + 
    fvu1u1u1*((-f53 - 12*f193)/3 + (f1 - f2 - f6)*fvu1u1u3 + 
      (f1 - f2 - f6)*fvu1u1u8 + (-f1 + f2 + f6)*fvu1u1u9 + 
      (-f1 + f2 + f6)*fvu1u1u10 + (-f1 + f2 + f6)*tci12);
L ieum1ueu5 = (-11*(f1 - f2 - f6))/3 + 
    (f1 - f2 - f6)*fvu1u1u1 + (f1 - f2 - f6)*fvu1u1u3 + 
    (f1 - f2 - f6)*fvu1u1u8 + (f1 - f2 - f6)*fvu1u1u9 + 
    (f1 - f2 - f6)*fvu1u1u10 - 3*(f1 - f2 - f6)*tci12;
L ieum2ueu5 = -5*(f1 - f2 - f6);
L ieu2uou5 = (-2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u28)/3 - (2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u39)/3 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
      fvu4u49)/9 - (2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u51)/9 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
      fvu4u80)/24 - (7*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u83)/24 + (3*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u91)/8 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
      fvu4u93)/24 - (2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u100)/9 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u102)/4 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
      fvu4u111)/8 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
      fvu4u113)/4 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
      fvu4u114)/2 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u129)/3 - (13*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u132)/6 + (7*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u139)/4 - (2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u141)/3 - (2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u146)/9 - (7*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u148)/12 + (2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u174)/3 + (2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u182)/3 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u190)/9 + (2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u192)/9 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u213)/24 + (67*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u215)/24 - (9*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u219)/8 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u221)/24 + (2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u225)/9 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u233)/8 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u234)/4 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u235)/2 + (5*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      fvu4u244)/3 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
      fvu4u246)/3 + (-f3 + f6 - 2*f71 - f195 - f232 + f274)*
     fvu4u250 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
      fvu4u252)/3 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
      fvu4u255)/9 + (-f3 + f6 - 2*f71 - f195 - f232 + f274)*
     fvu4u273 + (f3 - f6 + 2*f71 + f195 + f232 - f274)*
     fvu4u277 + fvu3u71*(((f3 - f6 + 2*f71 + f195 + f232 - 
         f274)*fvu1u1u3)/3 + ((-f3 + f6 - 2*f71 - f195 - f232 + 
         f274)*fvu1u1u6)/3 + ((f3 - f6 + 2*f71 + f195 + f232 - 
         f274)*fvu1u1u7)/3) + 
    fvu3u25*(((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u3)/3 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u6)/3 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u7)/3) + fvu3u23*
     (((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu1u1u1)/6 + 
      ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu1u1u6)/6 + 
      ((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u8)/6 + 
      ((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u10)/6) + 
    fvu3u43*(((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u2)/
       6 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u4)/
       6 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u6)/
       6 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu1u1u7)/
       6 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu1u1u9)/
       3 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u10)/
       6) + fvu3u63*(((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u2)/2 - (3*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u3)/4 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u5)/3 - (19*(f3 - f6 + 2*f71 + f195 + f232 - 
         f274)*fvu1u1u6)/12 + ((-f3 + f6 - 2*f71 - f195 - 
         f232 + f274)*fvu1u1u7)/6 + 
      ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu1u1u8)/3 + 
      ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu1u1u9)/6 + 
      (7*(f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u10)/4) + 
    fvu3u70*(((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u6)/
       6 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu1u1u8)/
       6 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu1u1u10)/
       6) + fvu3u45*(((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u2)/2 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u3)/6 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u5)/3 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u6)/6 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u8)/2 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u9)/6 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u10)/6) + (13*(f3 - f6 + 2*f71 + f195 + f232 - 
       f274)*tci11^3*tci12)/30 + 
    fvu3u78*(((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u2)/6 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u3)/6 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u4)/6 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u6)/6 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u8)/6 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u9)/3 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci12)/3) + fvu3u80*
     (((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u2)/3 + 
      ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu1u1u3)/6 + 
      ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu1u1u4)/6 + 
      ((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u5)/3 + 
      ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu1u1u6)/3 + 
      ((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u7)/2 - 
      (5*(f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u8)/6 + 
      ((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu1u1u9)/2 + 
      (4*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci12)/3) + 
    fvu3u62*(((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u2)/3 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u3)/2 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u4)/6 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u5)/3 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*
        fvu1u1u6)/3 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u7)/6 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u8)/2 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u9)/2 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        fvu1u1u10)/3 + (-f3 + f6 - 2*f71 - f195 - f232 + f274)*
       tci12) - (5*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      tci11^2*tci21)/27 + tci12*
     ((8*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci31)/5 + 
      8*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci32) - 
    (1036*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci41)/135 - 
    (32*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci42)/5 - 
    (8*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci43)/3 + 
    ((53*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci11^3)/
       216 + (5*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci12*
        tci21)/6 - (6*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci31)/5 + 8*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
       tci32)*tcr11 + (61*(f3 - f6 + 2*f71 + f195 + f232 - 
       f274)*tci11*tcr11^3)/360 + 
    fvu1u1u10*((169*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci11^3)/3240 + (25*(f3 - f6 + 2*f71 + f195 + f232 - 
         f274)*tci12*tci21)/18 + 
      (38*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci31)/5 + 
      (19*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci21*
        tcr11)/6 - (19*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci11*tcr11^2)/120) + fvu1u1u2*
     ((53*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci11^3)/
       540 + (36*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci31)/5 + 3*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
       tci21*tcr11 - (3*(f3 - f6 + 2*f71 + f195 + f232 - 
         f274)*tci11*tcr11^2)/20) + 
    fvu1u1u5*(((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci11^3)/90 - (8*(f3 - f6 + 2*f71 + f195 + f232 - 
         f274)*tci12*tci21)/9 - 
      (8*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci31)/5 - 
      (2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci21*
        tcr11)/3 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci11*tcr11^2)/30) + fvu1u1u8*
     ((-133*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci11^3)/
       1620 + (4*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci12*
        tci21)/3 - (12*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci31)/5 + (-f3 + f6 - 2*f71 - f195 - f232 + f274)*
       tci21*tcr11 + ((f3 - f6 + 2*f71 + f195 + f232 - 
         f274)*tci11*tcr11^2)/20) + 
    fvu1u1u6*((-11*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci11^3)/360 + ((-f3 + f6 - 2*f71 - f195 - f232 + 
         f274)*tci12*tci21)/2 - 
      (18*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci31)/5 - 
      (3*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci21*
        tcr11)/2 + (3*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci11*tcr11^2)/40) + fvu1u1u7*
     ((-7*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci11^3)/
       324 - (8*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci12*
        tci21)/9 - 4*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
       tci31 - (5*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci21*tcr11)/3 + ((f3 - f6 + 2*f71 + f195 + f232 - 
         f274)*tci11*tcr11^2)/12) + 
    fvu1u1u9*((-19*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci11^3)/540 - (10*(f3 - f6 + 2*f71 + f195 + f232 - 
         f274)*tci12*tci21)/9 - 
      (28*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci31)/5 - 
      (7*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci21*
        tcr11)/3 + (7*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci11*tcr11^2)/60) + fvu1u1u3*
     ((-391*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci11^3)/
       3240 + ((f3 - f6 + 2*f71 + f195 + f232 - f274)*tci12*
        tci21)/6 - (42*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci31)/5 - (7*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci21*tcr11)/2 + (7*(f3 - f6 + 2*f71 + f195 + 
         f232 - f274)*tci11*tcr11^2)/40) + 
    fvu1u1u1*(((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu3u25)/
       3 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu3u43)/6 + 
      ((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu3u45)/3 + 
      ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu3u62)/6 + 
      (13*(f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu3u63)/12 + 
      ((f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu3u70)/6 + 
      ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu3u71)/3 + 
      ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu3u78)/6 + 
      ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*fvu3u80)/6 + 
      (19*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci11^3)/
       216 + ((-f3 + f6 - 2*f71 - f195 - f232 + f274)*tci12*
        tci21)/6 + 6*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
       tci31 + (5*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci21*tcr11)/2 + ((-f3 + f6 - 2*f71 - f195 - f232 + 
         f274)*tci11*tcr11^2)/8) + 
    ((-109*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci11^3)/
       243 - (20*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci12*
        tci21)/3)*tcr12 + 
    (-2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci11*
       tci12 - (20*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci21)/3)*tcr12^2 + tcr11^2*
     (((-f3 + f6 - 2*f71 - f195 - f232 + f274)*tci11*
        tci12)/30 - (3*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
        tci21)/2 + (-f3 + f6 - 2*f71 - f195 - f232 + f274)*
       tci11*tcr12) + (65*(f3 - f6 + 2*f71 + f195 + f232 - 
       f274)*tci11*tcr33)/27;
L ieu1uou5 = 
   (f3 - f6 + 2*f71 + f195 + f232 - f274)*fvu3u62 - 
    (11*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci11^3)/270 - 
    (2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*tci12*
      tci21)/3 - (24*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
      tci31)/5 - 2*(f3 - f6 + 2*f71 + f195 + f232 - f274)*
     tci21*tcr11 + ((f3 - f6 + 2*f71 + f195 + f232 - 
       f274)*tci11*tcr11^2)/10;
L ieu0uou5 = 0;
L ieum1uou5 = 0;
L ieum2uou5 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou5+w^2*ieu1uou5+w^3*ieu0uou5+w^4*ieum1uou5+w^5*ieum2uou5;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou5a = K[w^1];
L ieu1uou5a = K[w^2];
L ieu0uou5a = K[w^3];
L ieum1uou5a = K[w^4];
L ieum2uou5a = K[w^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_odd.c> "%O"
#write <e5_odd.c> "return Eps5o2<T>("
#write <e5_odd.c> "%E", ieu2uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu0uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum2uou5a
#write <e5_odd.c> ");\n}"
L H=+u^1*ieu2ueu5+u^2*ieu1ueu5+u^3*ieu0ueu5+u^4*ieum1ueu5+u^5*ieum2ueu5;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu5a = H[u^1];
L ieu1ueu5a = H[u^2];
L ieu0ueu5a = H[u^3];
L ieum1ueu5a = H[u^4];
L ieum2ueu5a = H[u^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_even.c> "%O"
#write <e5_even.c> "return Eps5o2<T>("
#write <e5_even.c> "%E", ieu2ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu0ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum2ueu5a
#write <e5_even.c> ");\n}"
.end
