#!/usr/bin/env bash

l=1
dir=new_exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_epsexp_}
    h=${g%_lr*.m}
    d=hel_${h}
    cd $d
    ../conv-form-y.sh ys.m
    ../conv-form-f.sh fs.m
    for i in {1..12}; do
        ../conv-form-eps-par.sh e$i.m
    done
    cd ..
done
