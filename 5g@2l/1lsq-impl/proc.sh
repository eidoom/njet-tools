#!/usr/bin/env bash

MAIN=main_file.cpp
rm ${MAIN}
touch ${MAIN}

order=(
    01234
    03421
    04231
    01342
    01423
    02341
    02134
    03412
    02314
    03142
    04213
    04123
)

l=1
dir=new_exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_epsexp_}
    h=${g%_lr*.m}
    d=hel_${h}

    cd ${d}

    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
        fi
    done

    if (( $j < 10 )); then
        j="0$j"
    fi

    echo $h $j

    COEFFS=coeffs.cpp
    perl -p -e "s|HH|$j|g; s|SSSSS|$h|g;" ../template_coeffs_start.cpp >${COEFFS}
    cat ys.cpp >>${COEFFS}
    cat fs.cpp >>${COEFFS}
    cat ../template_coeffs_end.cpp >>${COEFFS}
    clang-format -i ${COEFFS}

    FILE=eps.cpp
    cat ../template_eps_start.cpp >${FILE}
    for i in {1..12}; do
        perl -p -e "s|HH|$j|g; s|PP|$i|g; s|SSSSS|$h|g; s|OOOOO|${order[$(($i - 1))]}|g;" ../template_perm_start.cpp >>${FILE}
        cat e${i}_odd.cpp >>${FILE}
        perl -p -e "s|HH|$j|g; s|PP|$i|g; s|SSSSS|$h|g; s|OOOOO|${order[$(($i - 1))]}|g;" ../template_perm_middle.cpp >>${FILE}
        cat e${i}_even.cpp >>${FILE}
    done
    cat ../template_eps_end.cpp >>${FILE}
    clang-format -i ${FILE}

    F=`cat fs.fcount`
    perl -p -e "s|HH|$j|g; s|SSSSS|$h|g; s|FFF|$F|g;" ../template_1lsq.cpp >>../${MAIN}

    cd ..
done

perl -pi -e "s|hA007\(|hA07(|g" ${MAIN}
clang-format -i ${MAIN}
