// SSSSS
template <typename T>
std::array<TwoLoopResult<Eps5o2<T>>, 12> Amp0q5g_a2l<T>::hAgHHe2()
{
    std::array<TwoLoopResult<Eps5o2<T>>, 12> amps { hAgCCe2() };
    for (TwoLoopResult<Eps5o2<T>> &amp : amps ) {
        amp = { -amp.loopsqC, -amp.loopsq };
    }
    return amps;
}

