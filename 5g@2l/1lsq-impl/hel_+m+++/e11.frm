#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f66;
S f180;
S f65;
S f64;
S f181;
S f132;
S f62;
S f131;
S f61;
S f130;
S f60;
S f69;
S f68;
S f74;
S f124;
S f127;
S f77;
S f70;
S f120;
S f121;
S f72;
S f122;
S f73;
S f128;
S f129;
S f157;
S f156;
S f158;
S f88;
S f148;
S f149;
S f19;
S f3;
S f80;
S f17;
S f14;
S f83;
S f15;
S f11;
S f4;
S f97;
S f94;
S f28;
S f95;
S f91;
S f23;
S f22;
S f21;
S f176;
S f26;
S f25;
S f24;
S f99;
S f38;
S f160;
S f161;
S f31;
S f32;
S f165;
S f40;
S f166;
S f58;
S f59;
S f100;
S f101;
S f106;
S f56;
S f54;
S f104;
S f105;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu4u279;
S fvu4u91;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u330;
S fvu4u100;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u325;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu11 = (-36*f3 + 11*f4 - 556*f19 + 63*f22 - 
      556*f58 - 63*f60 + 52*f104 - 52*f120)/27 + 
    ((-f61 - 2*f101)*fvu3u1)/3 + ((f61 + 2*f101)*fvu3u2)/6 + 
    (2*f61*fvu3u3)/9 - (f101*fvu3u4)/6 + ((11*f61 - f101)*fvu3u5)/
     18 + ((-f61 - 2*f101)*fvu3u6)/9 + ((2*f61 + f91)*fvu3u7)/6 + 
    (16*f61*fvu3u8)/9 - (f61*fvu3u10)/6 + (2*(f61 + f101)*fvu3u11)/
     9 + ((4*f61 - f101)*fvu3u12)/18 + ((-f61 + 3*f101)*fvu3u13)/
     18 + (f61*fvu3u14)/3 + (f101*fvu3u15)/6 + 
    ((f61 + f99 - f101)*fvu3u17)/6 + ((f61 - f101)*fvu3u18)/6 + 
    ((-f61 - 2*f101)*fvu3u19)/9 - (2*f61*fvu3u24)/9 - 
    (f61*fvu3u31)/9 - (f101*fvu3u32)/18 + (2*f101*fvu3u36)/9 + 
    ((-f61 - 3*f95 + 3*f101)*fvu3u37)/18 + 
    ((-f61 + f101)*fvu3u38)/6 - (f101*fvu3u40)/6 + 
    (f61*fvu3u42)/3 + ((9*f61 - 4*f101)*fvu1u1u1^3)/18 + 
    ((11*f61 - 15*f101)*fvu1u1u2^3)/27 + 
    ((f61 - 6*f101)*fvu1u1u3^3)/18 + ((-2*f61 + f101)*fvu1u1u5^3)/
     9 + (f101*fvu1u1u6^3)/9 + ((f61 - f101)*fvu1u1u7^3)/9 + 
    (4*f61*fvu1u1u9^3)/27 + fvu1u1u8^2*
     ((6*f38 - f40 + 6*f61 - 3*f83 + 6*f91 - 6*f156 + f158)/
       18 + (f61*fvu1u1u9)/9) + fvu1u1u4^2*
     (((f61 - 3*f101)*fvu1u1u5)/18 + ((4*f61 - f101)*fvu1u1u8)/
       18 + ((7*f61 - f101)*fvu1u1u9)/18) + 
    ((-6*f94 + f97 + f100 + 6*f101)*fvu2u1u4)/9 + 
    ((-6*f23 + f26 + 6*f94 - 6*f95 - f97 - 6*f101)*fvu2u1u7)/
     9 + ((6*f38 - f40 + 6*f61 + 6*f91 - 6*f156 + f158)*
      fvu2u1u12)/9 - (2*(3*f61 - f101)*fvu1u2u6*tci11^2)/9 + 
    ((-f61 - 2*f101)*fvu1u2u7*tci11^2)/3 + 
    ((-16*f3 + 2*f4 - 16*f11 + 2*f14 + 16*f15 - 2*f17 - 
       36*f19 - 16*f21 + 4*f22 + 2*f24 + 16*f25 - 2*f28 - 
       16*f31 + 2*f32 - 32*f54 + 4*f56 - 16*f58 - 4*f59 - 
       2*f60 - 16*f62 + 2*f65 - 8*f68 + f69 - 8*f70 + f73 - 
       16*f77 + 2*f80 - 16*f83 + 2*f104 - 16*f105 + 2*f106 - 
       f120 + 32*f121 - 4*f122 - 2*f124 + 32*f127 - 4*f128 + 
       16*f129 + 8*f130 - 2*f131 - f132 + 16*f148 - 2*f149 - 
       4*f157 + 16*f160 - 2*f161 + 8*f165 - f166 - f176 + 
       32*f180 - 4*f181)*tci12)/18 + 
    ((-6*f38 + f40 - 6*f61 - 6*f91 + 6*f156 - f158)*fvu1u2u9*
      tci12)/9 + (4*f101*fvu2u1u1*tci12)/3 - 
    (4*(f61 - f101)*fvu2u1u2*tci12)/9 - 
    (2*(3*f61 - f101)*fvu2u1u3*tci12)/9 + 
    ((f61 + 2*f101)*fvu2u1u5*tci12)/3 - 
    (4*(4*f61 - f101)*fvu2u1u9*tci12)/9 + 
    fvu2u1u11*((6*f38 - f40 + 6*f91 + f100)/9 - 
      (4*f61*tci12)/9) + fvu1u1u2^2*
     ((6*f38 - f40 - 3*f64 + 6*f91 - 6*f94 + f97 + 2*f100 + 
        6*f101)/18 + (f101*fvu1u1u3)/6 + ((f61 + f101)*fvu1u1u4)/
       9 - (f61*fvu1u1u5)/9 + (f101*fvu1u1u7)/6 + 
      (f61*fvu1u1u8)/9 + ((2*f61 + f101)*fvu1u1u9)/9 + 
      ((-8*f61 - f101)*tci12)/18) + 
    fvu1u1u7^2*((6*f23 - f26 - 6*f61 - 3*f72 + 6*f95 + 6*f156 - 
        f158)/18 + ((f61 + 2*f101)*fvu1u1u8)/18 - 
      (f61*fvu1u1u9)/6 + ((-2*f61 - f101)*tci12)/18) + 
    fvu2u1u10*((6*f23 - f26 - 6*f61 + 6*f95 + 6*f156 - f158)/
       9 - (2*(f61 - f101)*tci12)/9) + 
    fvu1u1u6^2*((f101*fvu1u1u7)/9 - (f101*fvu1u1u8)/18 + 
      (f101*tci12)/18) + fvu1u1u9^2*
     (-f77/6 + ((-10*f61 + f101)*tci12)/18) + 
    fvu1u1u3^2*(-(f101*fvu1u1u4)/6 + ((f61 + f101)*fvu1u1u7)/6 + 
      ((f61 - f101)*fvu1u1u8)/6 + ((-3*f61 + 2*f101)*tci12)/
       6) + fvu1u1u5^2*((3*f3 - 27*f19 + 3*f22 - 6*f23 + f26 - 
        27*f58 - 3*f60 + 3*f64 + 3*f72 + 3*f77 + 3*f83 + 
        6*f94 - 6*f95 - f97 - 6*f101 + 3*f104 - 3*f120)/18 - 
      (f101*fvu1u1u6)/6 + (f61*fvu1u1u7)/18 + 
      ((f61 - 3*f101)*fvu1u1u8)/18 + ((f61 - f101)*fvu1u1u9)/6 + 
      ((-f61 + 3*f101)*tci12)/9) + 
    fvu1u1u1^2*(-(f61*fvu1u1u2)/9 - (f61*fvu1u1u3)/6 + 
      ((-f61 + f101)*fvu1u1u4)/9 - (f61*fvu1u1u7)/3 + 
      ((f61 + 2*f101)*fvu1u1u8)/18 - (f61*fvu1u1u9)/18 + 
      ((-23*f61 + 8*f101)*tci12)/18) + 
    tci11^2*((3*f3 - 27*f19 + 3*f22 + 12*f38 - 2*f40 - 
        27*f58 - 3*f60 + 48*f61 - 18*f83 + 12*f91 + 12*f94 - 
        2*f97 + 2*f100 - 12*f101 + 3*f104 - 3*f120 - 48*f156 + 
        8*f158)/108 + ((7*f61 + 3*f91 - 3*f95 + 10*f101)*tci12)/
       108) + fvu1u2u8*((-2*(f61 - f101)*tci11^2)/9 + 
      ((6*f23 - f26 - 6*f61 + 6*f95 + 6*f156 - f158)*tci12)/
       9) + fvu1u1u9*(f80/9 + (2*(7*f61 - f101)*fvu2u1u11)/9 + 
      (2*(6*f61 - f101)*tci11^2)/27 + 
      ((6*f61 - 6*f156 + f158)*tci12)/9) + 
    fvu1u1u8*((16*f3 - 2*f4 + 16*f11 - 2*f14 - 16*f15 + 2*f17 + 
        36*f19 + 16*f21 - 4*f22 - 2*f24 - 16*f25 + 2*f28 + 
        16*f31 - 2*f32 + 32*f54 - 4*f56 + 16*f58 + 4*f59 + 
        2*f60 + 16*f62 - 2*f65 + 8*f68 - f69 + 8*f70 - f73 + 
        16*f77 - 2*f80 + 16*f83 - 2*f104 + 16*f105 - 2*f106 + 
        f120 - 32*f121 + 4*f122 + 2*f124 - 32*f127 + 4*f128 - 
        16*f129 - 8*f130 + 2*f131 + f132 - 16*f148 + 2*f149 + 
        4*f157 - 16*f160 + 2*f161 - 8*f165 + f166 + f176 - 
        32*f180 + 4*f181)/18 + (f61*fvu1u1u9^2)/9 + 
      (2*(4*f61 - f101)*fvu2u1u9)/9 + (2*(f61 - f101)*fvu2u1u10)/
       9 + (4*f61*fvu2u1u11)/9 + (f61*tci11^2)/18 + 
      (f83*tci12)/3 + (2*(f61 - f101)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*((-6*f61 + 6*f156 - f158)/9 - (2*f61*tci12)/
         9)) + fvu1u1u7*(f74/9 + ((-f61 + 4*f101)*fvu1u1u8^2)/18 - 
      (f61*fvu1u1u9^2)/6 - (2*(f61 - 3*f101)*fvu2u1u7)/9 + 
      (2*f101*fvu2u1u8)/9 + (2*(f61 - f101)*fvu2u1u10)/9 + 
      ((-f61 + 2*f101)*tci11^2)/9 + 
      ((6*f61 - 6*f156 + f158)*tci12)/9 + 
      (2*(f61 - f101)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*((6*f61 - 6*f156 + f158)/9 - (f61*tci12)/3) + 
      fvu1u1u8*((-6*f23 + f26 - 6*f95)/9 + 
        ((-f61 - 2*f101)*tci12)/9)) + 
    fvu1u1u1*((-4*f61*fvu1u1u2^2)/9 + 
      ((-f61 + 2*f101)*fvu1u1u3^2)/6 - 
      (2*(f61 - f101)*fvu1u1u4^2)/9 + 
      ((-f61 - 2*f101)*fvu1u1u5^2)/6 - (f61*fvu1u1u7^2)/6 + 
      ((-7*f61 + 4*f101)*fvu1u1u8^2)/18 + (f61*fvu1u1u9^2)/18 - 
      (2*f101*fvu2u1u1)/3 + (2*(f61 - f101)*fvu2u1u2)/9 + 
      (2*(4*f61 - f101)*fvu2u1u9)/9 - (4*f61*fvu2u1u11)/9 + 
      (2*(5*f61 + 4*f101)*tci11^2)/27 + 
      (5*f61*fvu1u1u9*tci12)/9 + fvu1u1u2*((2*f61*fvu1u1u4)/9 + 
        (2*f61*fvu1u1u8)/9 + (2*f61*fvu1u1u9)/9 - 
        (2*f61*tci12)/9) + fvu1u1u7*((f61*fvu1u1u9)/3 + 
        (2*f61*tci12)/3) + fvu1u1u8*((-2*f61*fvu1u1u9)/9 + 
        ((7*f61 - 4*f101)*tci12)/9) + 
      fvu1u1u3*((f61*fvu1u1u4)/3 + ((f61 - 2*f101)*tci12)/3) + 
      fvu1u1u4*(((-f61 - 2*f101)*fvu1u1u8)/9 - (2*f61*fvu1u1u9)/
         9 + (4*(f61 - f101)*tci12)/9)) + 
    fvu1u1u6*(-(f101*fvu1u1u8^2)/18 + (2*f101*fvu2u1u8)/9 - 
      (2*f101*tci11^2)/27 + (f101*fvu1u1u8*tci12)/9 + 
      fvu1u1u7*((-2*f101*fvu1u1u8)/9 + (2*f101*tci12)/9)) + 
    fvu1u1u3*(-(f101*fvu1u1u4^2)/6 + ((f61 + 2*f101)*fvu1u1u5^2)/
       6 + (f101*fvu1u1u7^2)/6 + ((f61 - f101)*fvu1u1u8^2)/6 - 
      (2*f101*fvu2u1u1)/3 + ((-f61 - 2*f101)*fvu2u1u5)/3 + 
      (2*(3*f61 - f101)*tci11^2)/9 + 
      ((-f61 - 2*f101)*fvu1u1u5*tci12)/3 + 
      ((-f61 - f101)*fvu1u1u7*tci12)/3 + 
      ((-f61 + f101)*fvu1u1u8*tci12)/3 + 
      ((f61 + 2*f101)*fvu1u2u7*tci12)/3 + 
      fvu1u1u4*(((-f61 + f101)*fvu1u1u8)/3 + (f101*tci12)/3)) + 
    fvu1u1u2*(f66/9 + (f101*fvu1u1u3^2)/6 - 
      (2*(2*f61 - f101)*fvu1u1u4^2)/9 + 
      ((-2*f61 + 3*f101)*fvu1u1u5^2)/9 + (f101*fvu1u1u7^2)/6 - 
      (f61*fvu1u1u8^2)/9 + ((-5*f61 + 2*f101)*fvu1u1u9^2)/9 + 
      (2*(3*f61 - f101)*fvu2u1u3)/9 + (2*(f61 - 3*f101)*fvu2u1u4)/
       9 + (2*(5*f61 - f101)*fvu2u1u11)/9 - 
      (2*(f61 - 3*f101)*tci11^2)/27 + 
      ((6*f38 - f40 + 6*f91)*tci12)/9 + 
      (2*(3*f61 - f101)*fvu1u2u6*tci12)/9 + 
      fvu1u1u5*(-f100/9 - (2*f61*tci12)/9) + 
      fvu1u1u8*((-6*f38 + f40 - 6*f91)/9 - (2*f61*fvu1u1u9)/9 + 
        (2*f61*tci12)/9) + fvu1u1u4*((2*f61*fvu1u1u5)/9 - 
        (2*f61*fvu1u1u8)/9 - (2*(2*f61 + f101)*fvu1u1u9)/9 + 
        (2*(f61 - f101)*tci12)/9) + fvu1u1u3*
       (-(f101*fvu1u1u7)/3 - (f101*tci12)/3) + 
      fvu1u1u7*((6*f94 - f97 - 6*f101)/9 + (f101*tci12)/3) + 
      fvu1u1u9*(-f100/9 + (2*(2*f61 + f101)*tci12)/9)) + 
    fvu1u1u5*((-16*f3 - 16*f11 + 2*f14 + 16*f15 - 2*f17 + 
        124*f19 - 16*f21 - 14*f22 + 2*f24 + 16*f25 - 2*f28 - 
        16*f31 + 2*f32 - 32*f54 + 4*f56 + 144*f58 - 4*f59 + 
        16*f60 - 16*f62 + 2*f65 - 2*f66 - 8*f68 + f69 - 8*f70 + 
        f73 - 2*f74 - 16*f77 - 16*f83 - 14*f104 - 16*f105 + 
        2*f106 + 15*f120 + 32*f121 - 4*f122 - 2*f124 + 32*f127 - 
        4*f128 + 16*f129 + 8*f130 - 2*f131 - f132 + 16*f148 - 
        2*f149 - 4*f157 + 16*f160 - 2*f161 + 8*f165 - f166 - 
        f176 + 32*f180 - 4*f181)/18 - (f101*fvu1u1u6^2)/6 + 
      ((f61 - 2*f101)*fvu1u1u7^2)/6 + ((f61 - 3*f101)*fvu1u1u8^2)/
       18 + ((f61 - f101)*fvu1u1u9^2)/6 + 
      (2*(f61 - 3*f101)*fvu2u1u4)/9 + ((-f61 - 2*f101)*fvu2u1u5)/
       3 - (2*(f61 - 3*f101)*fvu2u1u7)/9 + 
      ((13*f61 - 54*f101)*tci11^2)/108 + 
      ((-6*f23 + f26 - 6*f95)*tci12)/9 + 
      ((f61 + 2*f101)*fvu1u2u7*tci12)/3 + 
      fvu1u1u7*((-6*f94 + f97 + 6*f101)/9 - (f61*fvu1u1u8)/9 + 
        (f61*tci12)/9) + fvu1u1u9*(f100/9 + 
        ((f61 - f101)*tci12)/3) + fvu1u1u6*((f101*fvu1u1u8)/3 - 
        (f101*tci12)/3) + fvu1u1u8*((6*f23 - f26 + 6*f95)/9 + 
        ((-f61 + 3*f101)*tci12)/9)) + 
    fvu1u1u4*(((f61 - 3*f101)*fvu1u1u5^2)/18 + 
      ((4*f61 - f101)*fvu1u1u8^2)/18 + 
      ((7*f61 - f101)*fvu1u1u9^2)/18 + (2*(f61 - f101)*fvu2u1u2)/
       9 + (2*(3*f61 - f101)*fvu2u1u3)/9 + (4*f61*fvu2u1u11)/9 - 
      (f61*tci11^2)/27 + ((-7*f61 + f101)*fvu1u1u9*tci12)/9 + 
      (2*(3*f61 - f101)*fvu1u2u6*tci12)/9 + 
      fvu1u1u8*((2*f61*fvu1u1u9)/9 + ((-4*f61 + f101)*tci12)/
         9) + fvu1u1u5*(((-f61 + f101)*fvu1u1u9)/3 + 
        ((-f61 + 3*f101)*tci12)/9)) + 
    ((29*f61 - 8*f101)*tci12*tcr11^2)/18 + 
    ((-9*f61 + 4*f101)*tcr11^3)/18 + 
    ((16*f61 - 3*f91 + 3*f95 + 4*f101)*tci11^2*tcr12)/18 + 
    (2*(13*f61 - 10*f101)*tci12*tcr21)/9 + 
    tcr11*((-4*(f61 + 2*f101)*tci11^2)/27 - 
      (2*f61*tci12*tcr12)/3 - (10*(f61 - f101)*tcr21)/9) + 
    ((-26*f61 - 9*f91 + 9*f95 - 12*f99 + 12*f101)*tcr33)/36;
L ieu1ueu11 = (f4 - 80*f19 + 9*f22 - 80*f58 - 9*f60 + 
      8*f104 - 8*f120)/9 + ((f91 + 2*f99 + f101)*fvu1u1u2^2)/6 + 
    ((-f95 - f101)*fvu1u1u5^2)/6 + ((-f61 + f95)*fvu1u1u7^2)/6 + 
    ((f61 + f91)*fvu1u1u8^2)/6 + 
    fvu1u1u8*(f83/3 - (f61*fvu1u1u9)/3) + 
    ((f99 + f101)*fvu2u1u4)/3 + ((-f95 - f101)*fvu2u1u7)/3 + 
    ((-f61 + f95)*fvu2u1u10)/3 + ((f91 + f99)*fvu2u1u11)/3 + 
    ((f61 + f91)*fvu2u1u12)/3 + ((4*f61 + f91 + f99 - f101)*
      tci11^2)/18 - (f83*tci12)/3 + 
    ((-f61 + f95)*fvu1u2u8*tci12)/3 + 
    ((-f61 - f91)*fvu1u2u9*tci12)/3 + 
    fvu1u1u9*(f77/3 + (f61*tci12)/3) + 
    fvu1u1u7*(f72/3 - (f95*fvu1u1u8)/3 + (f61*fvu1u1u9)/3 + 
      (f61*tci12)/3) + fvu1u1u2*(f64/3 - (f99*fvu1u1u5)/3 - 
      (f101*fvu1u1u7)/3 - (f91*fvu1u1u8)/3 - (f99*fvu1u1u9)/3 + 
      (f91*tci12)/3) + fvu1u1u5*
     ((-f3 + 9*f19 - f22 + 9*f58 + f60 - f64 - f72 - f77 - 
        f83 - f104 + f120)/3 + (f101*fvu1u1u7)/3 + 
      (f95*fvu1u1u8)/3 + (f99*fvu1u1u9)/3 - (f95*tci12)/3);
L ieu0ueu11 = (f3 - 9*f19 + f22 - 9*f58 - f60 + f104 - 
     f120)/3;
L ieum1ueu11 = 0;
L ieum2ueu11 = 0;
L ieu2uou11 = -2*f88*fvu4u28 - 2*f88*fvu4u39 - 
    (2*f88*fvu4u51)/3 - 2*f88*fvu4u80 - 2*f88*fvu4u83 - 
    2*f88*fvu4u91 - 2*f88*fvu4u93 + (2*f88*fvu4u100)/3 - 
    (2*f88*fvu4u102)/3 + 2*f88*fvu4u139 - (2*f88*fvu4u148)/3 - 
    (3*f88*fvu4u171)/4 + (11*f88*fvu4u174)/4 + (11*f88*fvu4u182)/4 - 
    (3*f88*fvu4u184)/4 + (7*f88*fvu4u192)/6 - (f88*fvu4u199)/4 - 
    (f88*fvu4u201)/2 - f88*fvu4u202 - 4*f88*fvu4u213 + 
    2*f88*fvu4u215 - 2*f88*fvu4u221 + (2*f88*fvu4u225)/3 - 
    f88*fvu4u244 + (3*f88*fvu4u252)/2 - (f88*fvu4u255)/6 + 
    (11*f88*fvu4u271)/4 + (9*f88*fvu4u273)/4 - (f88*fvu4u277)/4 + 
    (3*f88*fvu4u279)/4 - (2*f88*fvu4u282)/3 + (f88*fvu4u289)/4 + 
    (f88*fvu4u290)/2 + f88*fvu4u291 + 2*f88*fvu4u295 + 
    2*f88*fvu4u325 - 2*f88*fvu4u330 + 
    fvu3u25*(f88*fvu1u1u1 - f88*fvu1u1u3 + f88*fvu1u1u6 - 
      f88*fvu1u1u7) + fvu3u71*(f88*fvu1u1u3 - f88*fvu1u1u6 + 
      f88*fvu1u1u7) + fvu3u81*(f88*fvu1u1u2 + f88*fvu1u1u5 + 
      f88*fvu1u1u8) + fvu3u45*(-(f88*fvu1u1u2) - f88*fvu1u1u3 + 
      f88*fvu1u1u9) + fvu3u43*(-(f88*fvu1u1u2) - f88*fvu1u1u6 + 
      f88*fvu1u1u7 + f88*fvu1u1u9) + 
    fvu3u82*(-(f88*fvu1u1u5) - f88*fvu1u1u6 + f88*fvu1u1u7 - 
      f88*fvu1u1u8 + f88*fvu1u1u9) + 
    fvu3u63*(-(f88*fvu1u1u6) - f88*fvu1u1u7 - f88*fvu1u1u9 + 
      2*f88*fvu1u1u10) - (151*f88*tci11^3*tci12)/135 + 
    fvu3u78*(-(f88*fvu1u1u2) + 2*f88*fvu1u1u3 - f88*fvu1u1u5 + 
      f88*fvu1u1u8 - 2*f88*tci12) + 
    fvu3u80*(f88*fvu1u1u3 - f88*fvu1u1u5 + (f88*fvu1u1u6)/2 - 
      (f88*fvu1u1u7)/2 + (f88*fvu1u1u8)/2 - (3*f88*fvu1u1u9)/2 - 
      (3*f88*tci12)/2) - (2*f88*tci11^2*tci21)/9 + 
    fvu1u1u1*(f88*fvu3u45 - f88*fvu3u71 - f88*fvu3u78 - 
      f88*fvu3u81 + f88*fvu3u82 + (8*f88*tci11^3)/27 - 
      8*f88*tci12*tci21) + fvu1u1u3*((-4*f88*tci11^3)/27 + 
      4*f88*tci12*tci21) + fvu1u1u5*((-4*f88*tci11^3)/27 + 
      4*f88*tci12*tci21) + fvu1u1u8*((-5*f88*tci11^3)/27 + 
      5*f88*tci12*tci21) + tci12*((-48*f88*tci31)/5 - 
      16*f88*tci32) - (4*f88*tci41)/3 + 
    ((-4*f88*tci11^3)/9 + 8*f88*tci12*tci21)*tcr11 + 
    (f88*tci11*tci12*tcr11^2)/5 + 
    fvu1u1u10*((11*f88*tci11^3)/135 + (4*f88*tci12*tci21)/
       3 + (48*f88*tci31)/5 + 4*f88*tci21*tcr11 - 
      (f88*tci11*tcr11^2)/5) + 
    fvu1u1u2*(f88*fvu3u63 + f88*fvu3u80 + (31*f88*tci11^3)/
       270 - (4*f88*tci12*tci21)/3 + (24*f88*tci31)/5 + 
      2*f88*tci21*tcr11 - (f88*tci11*tcr11^2)/10) + 
    fvu1u1u6*(-(f88*tci11^3)/270 - (5*f88*tci12*tci21)/3 - 
      (24*f88*tci31)/5 - 2*f88*tci21*tcr11 + 
      (f88*tci11*tcr11^2)/10) + 
    fvu1u1u7*((-7*f88*tci11^3)/90 + (f88*tci12*tci21)/3 - 
      (24*f88*tci31)/5 - 2*f88*tci21*tcr11 + 
      (f88*tci11*tcr11^2)/10) + 
    fvu1u1u9*((-7*f88*tci11^3)/90 + (f88*tci12*tci21)/3 - 
      (24*f88*tci31)/5 - 2*f88*tci21*tcr11 + 
      (f88*tci11*tcr11^2)/10) + 
    (40*f88*tci12*tci21*tcr12)/3 + 4*f88*tci11*tci12*
     tcr12^2;
L ieu1uou11 = 2*f88*fvu3u81 - 
    (4*f88*tci11^3)/27 + 4*f88*tci12*tci21;
L ieu0uou11 = 0;
L ieum1uou11 = 0;
L ieum2uou11 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou11+w^2*ieu1uou11+w^3*ieu0uou11+w^4*ieum1uou11+w^5*ieum2uou11;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou11a = K[w^1];
L ieu1uou11a = K[w^2];
L ieu0uou11a = K[w^3];
L ieum1uou11a = K[w^4];
L ieum2uou11a = K[w^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_odd.c> "%O"
#write <e11_odd.c> "return Eps5o2<T>("
#write <e11_odd.c> "%E", ieu2uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu0uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum2uou11a
#write <e11_odd.c> ");\n}"
L H=+u^1*ieu2ueu11+u^2*ieu1ueu11+u^3*ieu0ueu11+u^4*ieum1ueu11+u^5*ieum2ueu11;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu11a = H[u^1];
L ieu1ueu11a = H[u^2];
L ieu0ueu11a = H[u^3];
L ieum1ueu11a = H[u^4];
L ieum2ueu11a = H[u^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_even.c> "%O"
#write <e11_even.c> "return Eps5o2<T>("
#write <e11_even.c> "%E", ieu2ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu0ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum2ueu11a
#write <e11_even.c> ");\n}"
.end
