#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f136;
S f180;
S f65;
S f181;
S f134;
S f62;
S f132;
S f130;
S f60;
S f69;
S f139;
S f68;
S f138;
S f124;
S f77;
S f127;
S f120;
S f121;
S f122;
S f128;
S f157;
S f89;
S f8;
S f19;
S f146;
S f80;
S f17;
S f14;
S f144;
S f15;
S f145;
S f7;
S f143;
S f11;
S f141;
S f178;
S f28;
S f92;
S f22;
S f177;
S f176;
S f25;
S f119;
S f35;
S f37;
S f58;
S f59;
S f56;
S f54;
S fvu1u2u9;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu4u282;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u3;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu4u313;
S fvu3u51;
S fvu4u91;
S fvu3u53;
S fvu4u309;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu6 = (99*f58 - 11*f59 + 11*f60 + 11*f120 + 
      63*f124 - 11*f157 - 11*f176)/27 - (f145*fvu3u3)/9 + 
    ((-2*f141 + f145)*fvu3u4)/12 - (11*f145*fvu3u5)/36 + 
    (f145*fvu3u6)/9 - (f145*fvu3u7)/6 - (8*f145*fvu3u8)/9 - 
    (f145*fvu3u11)/9 + (f145*fvu3u12)/36 + (f145*fvu3u13)/36 - 
    (f145*fvu3u17)/12 + (f145*fvu3u18)/12 + (f145*fvu3u24)/9 + 
    (f119*fvu3u35)/6 + (f145*fvu3u51)/9 + 
    ((3*f143 + f145)*fvu3u52)/18 - (f138*fvu3u53)/6 - 
    (2*f145*fvu3u55)/9 - (f145*fvu3u56)/9 - (f145*fvu3u57)/6 + 
    (f145*fvu3u59)/6 + (f145*fvu3u61)/3 + (f145*fvu1u1u1^3)/18 - 
    (11*f145*fvu1u1u2^3)/54 - (f145*fvu1u1u6^3)/9 + 
    (f145*fvu1u1u8^3)/18 - (8*f145*fvu1u1u9^3)/27 + 
    fvu1u1u4^2*((-6*f35 + f37 + 6*f119 + 3*f134 - f139)/18 - 
      (f145*fvu1u1u5)/36 + (f145*fvu1u1u8)/36 - 
      (7*f145*fvu1u1u9)/36) + fvu1u1u3^2*
     ((-3*f121 + 3*f124 - 3*f127 - 3*f130 - 3*f134 - f144 - 
        f146)/18 + (f145*fvu1u1u4)/12 + (f145*fvu1u1u6)/18 + 
      (f145*fvu1u1u8)/12 + (f145*fvu1u1u9)/9 - 
      (f145*fvu1u1u10)/6) + ((-6*f35 + f37 + 6*f119 - f139)*
      fvu2u1u6)/9 + ((-f144 - f146)*fvu2u1u13)/9 + 
    ((f139 - f144)*fvu2u1u14)/9 + (f145*fvu1u2u6*tci11^2)/3 - 
    (f145*fvu1u2u9*tci11^2)/3 + 
    ((9*f58 - f59 + f60 + f120 - f122 + 9*f124 - f132 - 
       f157 - f176)*tci12)/9 + 
    ((f144 + f146)*fvu1u2u2*tci12)/9 + 
    ((6*f35 - f37 - 6*f119 + f139)*fvu1u2u3*tci12)/9 + 
    (f145*fvu2u1u3*tci12)/3 - (2*f145*fvu2u1u9*tci12)/9 + 
    (2*f145*fvu2u1u11*tci12)/9 + (f145*fvu2u1u12*tci12)/3 + 
    fvu1u1u10*(-f122/9 - (2*f145*fvu2u1u14)/9 - 
      (4*f145*fvu2u1u15)/9 - (f145*tci11^2)/9 - 
      (f139*tci12)/9) + fvu2u1u1*
     ((6*f89 - f92 - 6*f141 + f146)/9 - (2*f145*tci12)/3) + 
    fvu1u1u8^2*(-(f145*fvu1u1u9)/18 + (f145*fvu1u1u10)/6 - 
      (f145*tci12)/2) + fvu2u1u2*
     ((6*f35 - f37 + 6*f89 - f92 - 6*f119 - 6*f141)/9 - 
      (2*f145*tci12)/9) + fvu1u1u1^2*
     ((6*f35 - f37 + 12*f89 - 2*f92 - 6*f119 + 3*f127 - 
        12*f141 + f146)/18 + (f145*fvu1u1u2)/18 - 
      (f145*fvu1u1u4)/18 - (f145*fvu1u1u8)/18 - 
      (f145*fvu1u1u9)/18 - (f145*fvu1u1u10)/6 - 
      (f145*tci12)/18) + fvu1u1u6^2*
     ((3*f130 + f139 - f144)/18 + (f145*fvu1u1u9)/6 - 
      (f145*fvu1u1u10)/9 - (f145*tci12)/18) + 
    fvu1u1u5^2*(-(f145*fvu1u1u9)/12 + (f145*tci12)/36) + 
    fvu1u1u9^2*(-(f145*fvu1u1u10)/18 + (f145*tci12)/12) + 
    fvu1u1u10^2*(f121/6 + (f145*tci12)/6) + 
    fvu1u1u2^2*(-(f145*fvu1u1u4)/18 + (f145*fvu1u1u5)/18 - 
      (f145*fvu1u1u8)/18 - (f145*fvu1u1u9)/9 + 
      (2*f145*tci12)/9) + tci11^2*
     ((12*f89 - 2*f92 - 18*f121 + 21*f124 - 18*f130 - 2*f139 - 
        12*f141 - 2*f144)/108 + 
      ((-3*f119 - f138 - 2*f141 - f143 + 5*f145)*tci12)/36) + 
    fvu1u1u5*(-(f145*fvu1u1u9^2)/12 - (f145*fvu2u1u4)/9 - 
      (f145*tci11^2)/27 - (f145*fvu1u1u9*tci12)/6) + 
    fvu1u1u8*((f145*fvu1u1u9^2)/9 + (f145*fvu2u1u9)/9 - 
      (2*f145*fvu2u1u11)/9 - (f145*fvu2u1u12)/3 + 
      (73*f145*tci11^2)/108 - (2*f145*fvu1u1u9*tci12)/9 - 
      (f145*fvu1u1u10*tci12)/3 + (f145*fvu1u2u9*tci12)/3) + 
    fvu1u1u9*((f145*fvu1u1u10^2)/6 - (7*f145*fvu2u1u11)/9 - 
      (f145*fvu2u1u12)/3 - (4*f145*fvu2u1u15)/9 - 
      (7*f145*tci11^2)/54 - (f145*fvu1u1u10*tci12)/9 + 
      (f145*fvu1u2u9*tci12)/3) + 
    fvu1u1u6*(-f132/9 + (f145*fvu1u1u9^2)/6 - 
      (2*f145*fvu2u1u14)/9 + (2*f145*tci11^2)/27 - 
      (f144*tci12)/9 + (f145*fvu1u1u9*tci12)/3 + 
      fvu1u1u10*(-f139/9 - (2*f145*tci12)/9)) + 
    fvu1u1u2*((2*f145*fvu1u1u4^2)/9 + (f145*fvu1u1u5^2)/9 + 
      (f145*fvu1u1u8^2)/18 + (5*f145*fvu1u1u9^2)/18 - 
      (f145*fvu2u1u3)/3 - (f145*fvu2u1u4)/9 - 
      (5*f145*fvu2u1u11)/9 + (f145*tci11^2)/27 + 
      (f145*fvu1u1u5*tci12)/9 - (2*f145*fvu1u1u9*tci12)/9 - 
      (f145*fvu1u2u6*tci12)/3 + fvu1u1u8*((f145*fvu1u1u9)/9 - 
        (f145*tci12)/9) + fvu1u1u4*(-(f145*fvu1u1u5)/9 + 
        (f145*fvu1u1u8)/9 + (2*f145*fvu1u1u9)/9 - 
        (f145*tci12)/9)) + fvu1u1u4*
     ((8*f7 - f8 - 16*f11 + 2*f14 + 16*f15 - 2*f17 - 19*f19 + 
        2*f22 + 16*f25 - 2*f28 - 16*f54 + 2*f56 + f58 - 2*f59 - 
        16*f62 + 2*f65 - 16*f68 + 2*f69 + 8*f77 - f80 - 
        2*f124 + 8*f127 - f128 - 8*f134 - 2*f157 + 8*f177 - 
        f178 + 8*f180 - f181)/9 - (f145*fvu1u1u5^2)/36 + 
      ((6*f35 - f37 - 6*f119)*fvu1u1u6)/9 + (f145*fvu1u1u8^2)/
       36 - (7*f145*fvu1u1u9^2)/36 + (f139*fvu1u1u10)/9 + 
      (f145*fvu2u1u2)/9 - (f145*fvu2u1u3)/3 - 
      (2*f145*fvu2u1u11)/9 + (f145*tci11^2)/27 - 
      (f134*tci12)/3 + (7*f145*fvu1u1u9*tci12)/18 - 
      (f145*fvu1u2u6*tci12)/3 + fvu1u1u8*(-(f145*fvu1u1u9)/9 - 
        (f145*tci12)/18) + fvu1u1u5*((f145*fvu1u1u9)/6 + 
        (f145*tci12)/18)) + fvu1u1u1*(-f128/9 + 
      (2*f145*fvu1u1u2^2)/9 - (f145*fvu1u1u3^2)/6 - 
      (f145*fvu1u1u4^2)/9 + ((-6*f35 + f37 + 6*f119)*fvu1u1u6)/
       9 - (f145*fvu1u1u8^2)/9 - (f145*fvu1u1u9^2)/9 + 
      (f145*fvu2u1u1)/3 + (f145*fvu2u1u2)/9 + (f145*fvu2u1u9)/9 + 
      (2*f145*fvu2u1u11)/9 - (23*f145*tci11^2)/54 + 
      ((-6*f35 + f37 + 6*f119 - 3*f127 - f146)*tci12)/9 - 
      (f145*fvu1u1u9*tci12)/9 + fvu1u1u2*(-(f145*fvu1u1u4)/9 - 
        (f145*fvu1u1u8)/9 - (f145*fvu1u1u9)/9 + 
        (f145*tci12)/9) + fvu1u1u8*((f145*fvu1u1u9)/9 + 
        (2*f145*tci12)/9) + fvu1u1u4*((-6*f89 + f92 + 6*f141)/
         9 + (f145*fvu1u1u8)/9 + (f145*fvu1u1u9)/9 + 
        (2*f145*tci12)/9) + fvu1u1u3*((-6*f89 + f92 + 6*f141)/
         9 + (f145*tci12)/3) + fvu1u1u10*(-f146/9 + 
        (f145*tci12)/3)) + fvu1u1u3*
     ((-8*f7 + f8 + 16*f11 - 2*f14 - 16*f15 + 2*f17 + 19*f19 - 
        2*f22 - 16*f25 + 2*f28 + 16*f54 - 2*f56 - 10*f58 + 
        3*f59 - f60 + 16*f62 - 2*f65 + 16*f68 - 2*f69 - 8*f77 + 
        f80 - f120 + f122 - 7*f124 - 8*f127 + 2*f128 + f132 + 
        8*f134 + 3*f157 + f176 - 8*f177 + f178 - 8*f180 + f181)/
       9 + (f145*fvu1u1u4^2)/12 + (f145*fvu1u1u6^2)/18 + 
      (f145*fvu1u1u8^2)/12 + (f145*fvu1u1u9^2)/9 - 
      (f145*fvu1u1u10^2)/6 + (f145*fvu2u1u1)/3 + 
      (f145*tci11^2)/36 + ((f121 - f124 + f127 + f130 + f134)*
        tci12)/3 - (f145*fvu1u1u8*tci12)/6 + 
      fvu1u1u9*((f145*fvu1u1u10)/9 - (2*f145*tci12)/9) + 
      fvu1u1u4*((6*f89 - f92 - 6*f141)/9 - (f145*fvu1u1u8)/6 - 
        (f145*tci12)/6) + fvu1u1u6*(f144/9 - (f145*fvu1u1u9)/
         3 + (2*f145*fvu1u1u10)/9 - (f145*tci12)/9) + 
      fvu1u1u10*(f146/9 + (f145*tci12)/3)) + 
    ((-9*f119 - 6*f141 + 10*f145)*tci12*tcr11^2)/18 + 
    ((f119 + 4*f141 - 2*f145)*tcr11^3)/18 + 
    ((-7*f119 + 6*f138 + 2*f141 + 6*f143 + 7*f145)*tci11^2*
      tcr12)/36 + ((f119 - 2*f141 - f145)*tcr12^3)/9 - 
    (2*(3*f119 + 3*f141 - 8*f145)*tci12*tcr21)/9 + 
    tcr11*(((21*f119 + 25*f145)*tci11^2)/108 + 
      ((f119 - f145)*tci12*tcr12)/3 + 
      ((6*f141 - 5*f145)*tcr21)/9) + 
    ((-f119 + 2*f141 + f145)*tcr31)/3 + 
    ((-f119 + 2*f141 + f145)*tcr32)/12 + 
    ((11*f119 + 18*f138 - 74*f141 + 18*f143 - 11*f145)*tcr33)/
     72;
L ieu1ueu6 = (9*f58 - f59 + f60 + f120 + 9*f124 - 
      f157 - f176)/9 + ((-f119 - 2*f141 + f145)*fvu1u1u1^2)/6 + 
    ((-f143 - f145)*fvu1u1u3^2)/6 + ((f119 - f138)*fvu1u1u4^2)/
     6 + ((f138 - f143)*fvu1u1u6^2)/6 + 
    fvu1u1u4*(-f134/3 - (f119*fvu1u1u6)/3 + (f138*fvu1u1u10)/3) + 
    fvu1u1u3*((f121 - f124 + f127 + f130 + f134)/3 - 
      (f141*fvu1u1u4)/3 + (f143*fvu1u1u6)/3 + 
      (f145*fvu1u1u10)/3) + ((-f141 + f145)*fvu2u1u1)/3 + 
    ((-f119 - f141)*fvu2u1u2)/3 + ((f119 - f138)*fvu2u1u6)/3 + 
    ((-f143 - f145)*fvu2u1u13)/3 + ((f138 - f143)*fvu2u1u14)/3 + 
    ((-f138 - f141 - f143)*tci11^2)/18 + 
    ((-f121 + f124 - f130)*tci12)/3 + 
    ((f143 + f145)*fvu1u2u2*tci12)/3 + 
    ((-f119 + f138)*fvu1u2u3*tci12)/3 + 
    fvu1u1u10*(-f121/3 - (f138*tci12)/3) + 
    fvu1u1u6*(-f130/3 - (f138*fvu1u1u10)/3 - (f143*tci12)/3) + 
    fvu1u1u1*(-f127/3 + (f141*fvu1u1u3)/3 + (f141*fvu1u1u4)/3 + 
      (f119*fvu1u1u6)/3 - (f145*fvu1u1u10)/3 + 
      ((f119 - f145)*tci12)/3);
L ieu0ueu6 = f124/3;
L ieum1ueu6 = 0;
L ieum2ueu6 = 0;
L ieu2uou6 = -(f136*fvu4u25)/3 - (f136*fvu4u28)/3 - 
    (7*f136*fvu4u39)/3 - (f136*fvu4u41)/3 + (f136*fvu4u49)/3 - 
    (5*f136*fvu4u51)/9 + (5*f136*fvu4u80)/12 - 
    (37*f136*fvu4u83)/12 - (7*f136*fvu4u91)/4 + 
    (5*f136*fvu4u93)/12 + (f136*fvu4u100)/3 - 
    (23*f136*fvu4u102)/18 + (f136*fvu4u111)/4 + (f136*fvu4u113)/2 + 
    f136*fvu4u114 - (8*f136*fvu4u129)/3 + (f136*fvu4u132)/3 - 
    (f136*fvu4u139)/6 - (8*f136*fvu4u141)/3 + (4*f136*fvu4u146)/9 - 
    (11*f136*fvu4u148)/18 + (5*f136*fvu4u171)/12 + 
    (23*f136*fvu4u174)/12 - (f136*fvu4u182)/12 + 
    (5*f136*fvu4u184)/12 + (f136*fvu4u190)/9 - (f136*fvu4u192)/18 + 
    (f136*fvu4u199)/4 + (f136*fvu4u201)/2 + f136*fvu4u202 + 
    (7*f136*fvu4u213)/12 - (f136*fvu4u215)/4 + (9*f136*fvu4u219)/4 + 
    (7*f136*fvu4u221)/12 - (4*f136*fvu4u225)/9 - (f136*fvu4u233)/4 - 
    (f136*fvu4u234)/2 - f136*fvu4u235 + (17*f136*fvu4u244)/3 + 
    (4*f136*fvu4u246)/3 - (7*f136*fvu4u252)/2 - 
    (17*f136*fvu4u255)/18 - (5*f136*fvu4u271)/12 - 
    (43*f136*fvu4u273)/12 + (67*f136*fvu4u277)/12 - 
    (5*f136*fvu4u279)/12 - (f136*fvu4u282)/3 - (f136*fvu4u289)/4 - 
    (f136*fvu4u290)/2 - f136*fvu4u291 - (2*f136*fvu4u293)/3 - 
    (2*f136*fvu4u295)/3 - 2*f136*fvu4u309 + 2*f136*fvu4u313 + 
    fvu3u78*((2*f136*fvu1u1u2)/3 - (2*f136*fvu1u1u3)/3 - 
      (2*f136*fvu1u1u9)/3) + fvu3u70*((f136*fvu1u1u2)/6 - 
      (f136*fvu1u1u3)/6 - (f136*fvu1u1u9)/6) + 
    fvu3u45*(-(f136*fvu1u1u2)/2 - (7*f136*fvu1u1u3)/6 - 
      (2*f136*fvu1u1u6)/3 + (2*f136*fvu1u1u7)/3 + 
      (f136*fvu1u1u9)/2) + fvu3u23*((f136*fvu1u1u1)/3 + 
      (f136*fvu1u1u2)/6 + (f136*fvu1u1u6)/2 - (f136*fvu1u1u7)/6 - 
      (f136*fvu1u1u8)/3 - (f136*fvu1u1u9)/6 - 
      (f136*fvu1u1u10)/3) + fvu3u43*(-(f136*fvu1u1u2)/6 - 
      (f136*fvu1u1u4)/3 - (f136*fvu1u1u6)/6 + (f136*fvu1u1u7)/6 + 
      (f136*fvu1u1u9)/2 - (f136*fvu1u1u10)/3) + 
    fvu3u25*(-(f136*fvu1u1u2)/3 - (7*f136*fvu1u1u3)/6 + 
      f136*fvu1u1u4 - (2*f136*fvu1u1u5)/3 + (5*f136*fvu1u1u6)/6 - 
      (f136*fvu1u1u7)/6 + (f136*fvu1u1u10)/3) + 
    fvu3u81*((f136*fvu1u1u3)/6 + (f136*fvu1u1u4)/3 + 
      (f136*fvu1u1u6)/6 - (f136*fvu1u1u7)/6 - (f136*fvu1u1u9)/3 + 
      (f136*fvu1u1u10)/3) + fvu3u62*((-4*f136*fvu1u1u2)/3 + 
      (2*f136*fvu1u1u4)/3 + (2*f136*fvu1u1u5)/3 - 
      (4*f136*fvu1u1u6)/3 + (2*f136*fvu1u1u7)/3 + 
      (4*f136*fvu1u1u10)/3) + (13*f136*tci11^3*tci12)/15 + 
    fvu3u71*(-(f136*fvu1u1u2)/3 + (4*f136*fvu1u1u3)/3 + 
      f136*fvu1u1u4 - (2*f136*fvu1u1u5)/3 - (4*f136*fvu1u1u6)/3 + 
      (f136*fvu1u1u7)/3 + (f136*fvu1u1u8)/3 - 2*f136*tci12) + 
    fvu3u82*((f136*fvu1u1u3)/3 + (f136*fvu1u1u4)/3 - 
      (f136*fvu1u1u7)/3 + (f136*fvu1u1u8)/3 - (f136*fvu1u1u9)/3 - 
      (2*f136*tci12)/3) + fvu3u63*((2*f136*fvu1u1u2)/3 + 
      (f136*fvu1u1u3)/2 - f136*fvu1u1u4 + (2*f136*fvu1u1u5)/3 - 
      (f136*fvu1u1u6)/2 - (f136*fvu1u1u7)/3 - (f136*fvu1u1u8)/3 - 
      (f136*fvu1u1u9)/3 - (f136*fvu1u1u10)/6 + 2*f136*tci12) + 
    fvu3u80*((2*f136*fvu1u1u2)/3 + f136*fvu1u1u3 - 
      (19*f136*fvu1u1u6)/6 + (19*f136*fvu1u1u7)/6 - 
      (17*f136*fvu1u1u8)/6 + (13*f136*fvu1u1u9)/6 + 
      (17*f136*tci12)/6) - (10*f136*tci11^2*tci21)/27 + 
    fvu1u1u8*((-17*f136*tci11^3)/81 + (17*f136*tci12*tci21)/
       3) + tci12*((16*f136*tci31)/5 + 16*f136*tci32) - 
    (2072*f136*tci41)/135 - (64*f136*tci42)/5 - 
    (16*f136*tci43)/3 + ((1259*f136*tci11^3)/1620 + 
      f136*tci12*tci21 + (84*f136*tci31)/5 + 
      16*f136*tci32)*tcr11 - (11*f136*tci11*tcr11^3)/
     180 + fvu1u1u2*((181*f136*tci11^3)/1620 + 
      (2*f136*tci12*tci21)/9 + (44*f136*tci31)/5 + 
      (11*f136*tci21*tcr11)/3 - (11*f136*tci11*tcr11^2)/
       60) + fvu1u1u6*((-13*f136*tci11^3)/162 + 
      (46*f136*tci12*tci21)/9 + 8*f136*tci31 + 
      (10*f136*tci21*tcr11)/3 - (f136*tci11*tcr11^2)/6) + 
    fvu1u1u3*((73*f136*tci11^3)/1620 - (f136*tci12*tci21)/
       3 + (12*f136*tci31)/5 + f136*tci21*tcr11 - 
      (f136*tci11*tcr11^2)/20) + 
    fvu1u1u9*((103*f136*tci11^3)/540 - 
      (49*f136*tci12*tci21)/9 - (4*f136*tci31)/5 - 
      (f136*tci21*tcr11)/3 + (f136*tci11*tcr11^2)/60) + 
    fvu1u1u5*((f136*tci11^3)/45 - (16*f136*tci12*tci21)/9 - 
      (16*f136*tci31)/5 - (4*f136*tci21*tcr11)/3 + 
      (f136*tci11*tcr11^2)/15) + 
    fvu1u1u4*((-31*f136*tci11^3)/270 + (4*f136*tci12*tci21)/
       3 - (24*f136*tci31)/5 - 2*f136*tci21*tcr11 + 
      (f136*tci11*tcr11^2)/10) + 
    fvu1u1u10*((-13*f136*tci11^3)/180 - (f136*tci12*tci21)/
       9 - (28*f136*tci31)/5 - (7*f136*tci21*tcr11)/3 + 
      (7*f136*tci11*tcr11^2)/60) + 
    fvu1u1u7*((221*f136*tci11^3)/1620 - 
      (19*f136*tci12*tci21)/3 - (36*f136*tci31)/5 - 
      3*f136*tci21*tcr11 + (3*f136*tci11*tcr11^2)/20) + 
    fvu1u1u1*((f136*fvu3u25)/6 + (f136*fvu3u43)/3 + 
      (7*f136*fvu3u45)/6 - (2*f136*fvu3u62)/3 - (7*f136*fvu3u63)/6 + 
      (f136*fvu3u70)/6 - (2*f136*fvu3u71)/3 + (2*f136*fvu3u78)/3 - 
      f136*fvu3u80 - (f136*fvu3u81)/2 - (f136*fvu3u82)/3 - 
      (179*f136*tci11^3)/1620 + (f136*tci12*tci21)/3 - 
      (36*f136*tci31)/5 - 3*f136*tci21*tcr11 + 
      (3*f136*tci11*tcr11^2)/20) + 
    ((-218*f136*tci11^3)/243 - (40*f136*tci12*tci21)/3)*
     tcr12 + (-4*f136*tci11*tci12 - (40*f136*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f136*tci11*tci12)/15 + 
      5*f136*tci21 - 2*f136*tci11*tcr12) + 
    (130*f136*tci11*tcr33)/27;
L ieu1uou6 = 
   -2*f136*fvu3u63 - (11*f136*tci11^3)/135 - 
    (4*f136*tci12*tci21)/3 - (48*f136*tci31)/5 - 
    4*f136*tci21*tcr11 + (f136*tci11*tcr11^2)/5;
L ieu0uou6 = 0;
L ieum1uou6 = 0;
L ieum2uou6 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou6+w^2*ieu1uou6+w^3*ieu0uou6+w^4*ieum1uou6+w^5*ieum2uou6;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou6a = K[w^1];
L ieu1uou6a = K[w^2];
L ieu0uou6a = K[w^3];
L ieum1uou6a = K[w^4];
L ieum2uou6a = K[w^5];
.sort
#write <e6.tmp> "`optimmaxvar_'"
#write <e6_odd.c> "%O"
#write <e6_odd.c> "return Eps5o2<T>("
#write <e6_odd.c> "%E", ieu2uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieu1uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieu0uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieum1uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieum2uou6a
#write <e6_odd.c> ");\n}"
L H=+u^1*ieu2ueu6+u^2*ieu1ueu6+u^3*ieu0ueu6+u^4*ieum1ueu6+u^5*ieum2ueu6;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu6a = H[u^1];
L ieu1ueu6a = H[u^2];
L ieu0ueu6a = H[u^3];
L ieum1ueu6a = H[u^4];
L ieum2ueu6a = H[u^5];
.sort
#write <e6.tmp> "`optimmaxvar_'"
#write <e6_even.c> "%O"
#write <e6_even.c> "return Eps5o2<T>("
#write <e6_even.c> "%E", ieu2ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieu1ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieu0ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieum1ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieum2ueu6a
#write <e6_even.c> ");\n}"
.end
