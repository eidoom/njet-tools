#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f67;
S f65;
S f63;
S f62;
S f61;
S f60;
S f69;
S f75;
S f77;
S f72;
S f78;
S f9;
S f8;
S f81;
S f80;
S f49;
S f59;
S f51;
S fvu1u2u8;
S fvu3u78;
S fvu4u25;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu1u1u9;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu4u39;
S fvu4u290;
S fvu4u291;
S fvu4u293;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u279;
S fvu4u91;
S fvu4u309;
S fvu4u93;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u330;
S fvu4u100;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u325;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu7 = (8*f61)/9 + ((-f59 + 3*f69 - 3*f75)*fvu3u1)/
     18 + (f69*fvu3u2)/3 - (2*(f59 + 3*f75)*fvu3u3)/9 + 
    ((-6*f8 + f9 + 6*f69)*fvu3u4)/9 + (f69*fvu3u5)/3 + 
    ((-6*f8 + f9 + 6*f69)*fvu3u6)/9 - (f69*fvu3u11)/3 + 
    ((6*f8 - f9 - 6*f69)*fvu3u12)/9 + (f69*fvu3u13)/3 - 
    (2*(5*f59 + f75)*fvu3u14)/9 + ((-6*f8 + f9 + 6*f69)*fvu3u15)/
     9 + ((-f59 - f69)*fvu3u17)/3 + 
    ((6*f8 - f9 - 6*f69 - f81)*fvu3u18)/9 + 
    ((-f59 - f69)*fvu3u19)/3 + (2*(3*f59 + f75)*fvu3u22)/9 + 
    (2*(3*f59 + f75)*fvu3u31)/9 + ((6*f8 - f9 - 6*f69 - f81)*
      fvu3u32)/9 - (2*(2*f59 - f75)*fvu3u33)/9 + 
    (2*(3*f59 + f75)*fvu3u34)/9 - (f75*fvu3u35)/18 + 
    ((-f69 - f75)*fvu3u36)/3 + ((6*f8 - f9 - 6*f69 - f77)*
      fvu3u37)/9 + ((16*f59 - 3*f69 + 3*f72 - 4*f75)*fvu3u38)/18 + 
    ((2*f59 - f72)*fvu3u40)/6 + (2*f75*fvu3u44)/9 + 
    (f72*fvu1u1u1^3)/3 - (f65*fvu1u1u2^3)/3 + 
    (2*(2*f59 - f75)*fvu1u1u3^3)/9 + 
    ((-f59 - 2*f69 - f75)*fvu1u1u5^3)/6 + 
    ((-f60 + f61 - f62 + f63 - f65)*fvu1u1u6^3)/6 + 
    ((-2*f59 - f75)*fvu1u1u7^3)/9 + fvu1u1u4^2*
     ((-17*f59 - 3*f75)/27 - (f75*fvu1u1u5)/6 - 
      (f59*fvu1u1u6)/18 - (f59*fvu1u1u7)/9 - (f75*fvu1u1u8)/3 + 
      (f75*fvu1u1u9)/3) + ((3*f59 + f75)*fvu2u1u4)/18 - 
    (f59*fvu2u1u6)/9 - (2*f75*fvu2u1u7)/9 + 
    (f75*fvu1u2u7*tci11^2)/3 + 
    ((f59 - 2*f75)*fvu1u2u8*tci11^2)/3 + 
    fvu1u1u9*(((f59 + 3*f75)*fvu2u1u11)/18 + 
      ((-f59 - 3*f75)*tci11^2)/9) + ((2*f59 + 5*f75)*tci12)/
     9 - (f77*fvu1u1u9^2*tci12)/9 - (f59*fvu1u2u3*tci12)/3 + 
    (2*(f59 + 3*f75)*fvu2u1u1*tci12)/9 + 
    ((-f59 + 2*f75)*fvu2u1u2*tci12)/3 + 
    ((-6*f59 - f75)*fvu2u1u5*tci12)/18 + 
    ((7*f59 + f75)*fvu2u1u9*tci12)/18 + 
    ((-7*f59 - f75)*fvu2u1u10*tci12)/9 + 
    fvu2u1u8*(-f81/9 - (f59*tci12)/9) + 
    tci11^2*((4*f59 - f72 + f75 + f78)/18 - 
      (f62*tci12)/3) + fvu1u1u1^2*((2*f75*fvu1u1u2)/27 - 
      (2*(f59 + f75)*fvu1u1u3)/9 + ((-6*f49 + f51 + 6*f72)*
        fvu1u1u4)/9 + ((-f59 + 2*f75)*fvu1u1u6)/3 - 
      (2*(3*f59 + f75)*fvu1u1u7)/9 - (f72*fvu1u1u8)/3 + 
      ((6*f49 - f51 - 6*f72)*tci12)/9) + 
    fvu1u1u6^2*((f60 - f61 + f62 - f63 + f65)/3 + 
      ((f59 + 3*f75)*fvu1u1u7)/18 - (f75*fvu1u1u8)/3 + 
      ((-f59 - 3*f75)*tci12)/9) + fvu1u1u3^2*
     ((-4*(2*f59 - f75)*fvu1u1u4)/9 - (2*(f59 - f75)*fvu1u1u7)/
       3 + (2*(f59 + f75)*fvu1u1u8)/9 - (2*(2*f59 - f75)*tci12)/
       9) + fvu1u1u5^2*((-f59 - 2*f75)/9 + 
      ((-2*f59 + f75)*fvu1u1u6)/6 - (2*f59*fvu1u1u7)/9 + 
      ((-3*f59 - 2*f75)*fvu1u1u8)/6 + ((-f59 + f75)*fvu1u1u9)/6 + 
      ((2*f59 - f75)*tci12)/3) + fvu1u1u2^2*
     ((-8*f65)/9 - (2*(2*f59 - f75)*fvu1u1u3)/9 - 
      (2*f75*fvu1u1u4)/9 + (f80*fvu1u1u5)/9 - (f59*fvu1u1u6)/9 + 
      ((-2*f59 + f75)*fvu1u1u7)/18 + ((-f59 - f75)*fvu1u1u9)/6 + 
      (f75*tci12)/6) + fvu1u1u7^2*((f75 - f78)/6 + 
      (2*(f59 + f75)*fvu1u1u8)/9 + (2*(f59 + f75)*tci12)/9) + 
    fvu2u1u3*(f81/9 - (2*(f59 + 3*f75)*tci12)/9) + 
    fvu1u2u6*((f75*tci11^2)/6 + (f81*tci12)/9) + 
    fvu1u1u7*(-f60/3 + ((f59 + f75)*fvu1u1u8^2)/6 + 
      ((-f59 - 3*f75)*fvu2u1u7)/9 + ((f59 + f75)*fvu2u1u8)/6 + 
      ((-f59 - f75)*fvu2u1u10)/3 + ((3*f60 + f77 - f80)*
        tci11^2)/18 + (2*f59*tci12)/9 + 
      ((f59 + 3*f75)*fvu1u1u8*tci12)/18 + 
      (f75*fvu1u2u8*tci12)/6) + 
    fvu1u1u8*(-(f59*fvu2u1u8)/9 + (f75*fvu2u1u9)/3 - 
      (f75*fvu2u1u10)/3 + (f59*tci11^2)/9 + 
      ((f59 + f75)*fvu1u2u8*tci12)/3) + 
    fvu1u1u3*(((11*f59 - 16*f75)*fvu1u1u4^2)/54 - 
      (f75*fvu1u1u5^2)/6 + ((-2*f59 + f75)*fvu1u1u7^2)/18 + 
      (f75*fvu1u1u8^2)/3 + (2*f75*fvu2u1u1)/9 + 
      ((-f59 + 2*f75)*fvu2u1u5)/9 + ((-f59 - 6*f75)*tci11^2)/
       18 - (f75*fvu1u1u5*tci12)/9 + 
      ((f59 - f75)*fvu1u1u7*tci12)/6 + 
      ((-f59 + f75)*fvu1u1u8*tci12)/3 + 
      ((-f59 - 4*f75)*fvu1u2u7*tci12)/18 + 
      fvu1u1u4*(-(f75*fvu1u1u8)/6 + ((f59 - 2*f75)*tci12)/
         18)) + fvu1u1u6*((6*f49 - f51 + 3*f65 - 6*f72 + f81)/
       18 + (4*f59*fvu1u1u7^2)/9 + (f75*fvu1u1u8^2)/18 + 
      (2*(f59 + f75)*fvu2u1u8)/9 + 
      (8*(f60 - f61 + f62 - f63 + f65)*tci11^2)/9 - 
      (f59*tci12)/6 - (f75*fvu1u1u8*tci12)/9 + 
      fvu1u1u7*((-4*f59)/9 + ((-2*f59 + f75)*fvu1u1u8)/18 + 
        ((2*f59 - f75)*tci12)/9)) + 
    fvu1u1u4*((2*(6*f59 + f75))/27 + (f77*fvu1u1u5^2)/9 - 
      (f59*fvu1u1u6^2)/6 + (5*f59*fvu1u1u7^2)/18 + 
      (2*(f59 + f75)*fvu1u1u8^2)/9 + (2*(f59 + 3*f75)*fvu1u1u9^2)/
       9 + ((-f59 + f75)*fvu2u1u2)/6 - (f75*fvu2u1u3)/6 + 
      (5*f59*fvu2u1u8)/9 + ((f72 - f78)*tci12)/6 + 
      (2*(f59 + f75)*fvu1u1u8*tci12)/9 + 
      (2*f75*fvu1u1u9*tci12)/9 + ((f59 + 6*f75)*fvu1u2u6*
        tci12)/18 + fvu1u1u6*((2*f59)/9 + (f59*fvu1u1u7)/3 + 
        (2*f59*fvu1u1u8)/3 - (f59*tci12)/3) + 
      fvu1u1u7*((-2*f59)/9 - (2*f59*fvu1u1u8)/9 - 
        (2*f59*tci12)/9) + fvu1u1u5*((-5*f59)/54 - 
        (2*f75*fvu1u1u9)/9 - (2*f75*tci12)/9)) + 
    fvu1u1u2*((8*f65)/9 + ((-2*f59 + f75)*fvu1u1u3^2)/6 + 
      (2*(f59 - f75)*fvu1u1u4^2)/9 + 
      ((f59 + f75 + f78)*fvu1u1u5^2)/6 + (2*f59*fvu1u1u6^2)/9 + 
      ((-9*f59 + 4*f75)*fvu1u1u7^2)/18 + 
      ((3*f59 - 2*f75)*fvu1u1u9^2)/6 - (2*(2*f59 - f75)*fvu2u1u3)/
       9 + ((-5*f59 + 4*f75)*fvu2u1u4)/9 + 
      ((-f59 + 2*f75)*fvu2u1u11)/6 + 
      ((7*f59 + 3*f69 - 3*f72 - 10*f75)*tci11^2)/108 + 
      (f59*tci12)/6 + ((-3*f59 + 2*f75)*fvu1u1u9*tci12)/3 + 
      ((5*f59 - 4*f75)*fvu1u2u6*tci12)/18 + 
      fvu1u1u5*(f81/9 + (f59*tci12)/3) + 
      fvu1u1u6*((-2*f59)/9 + (16*f59*tci12)/9) + 
      fvu1u1u7*((13*f59 - 8*f75)/18 + ((7*f59 - 2*f75)*tci12)/
         18) + fvu1u1u4*((5*f59)/9 - (f59*fvu1u1u5)/3 + 
        ((-f59 - 3*f75)*fvu1u1u9)/18 - (f75*tci12)/6) + 
      fvu1u1u3*(((-f59 + 2*f75)*fvu1u1u7)/9 + 
        ((11*f59 + f75)*tci12)/18)) + 
    fvu1u1u5*((-7*f59 + 3*f75)/27 + ((f59 - f75)*fvu1u1u6^2)/3 - 
      (f75*fvu1u1u7^2)/6 + ((2*f59 - f75)*fvu1u1u8^2)/3 - 
      (2*(f59 - f75)*fvu1u1u9^2)/3 + ((f59 + 3*f75)*fvu2u1u4)/
       18 + ((-2*f59 + f75)*fvu2u1u5)/18 + (f75*fvu2u1u7)/18 + 
      (f63*tci11^2)/3 - (2*f59*tci12)/3 + 
      ((f59 - 2*f75)*fvu1u1u8*tci12)/3 + 
      ((-f59 + 2*f75)*fvu1u1u9*tci12)/3 + 
      ((11*f59 + 4*f75)*fvu1u2u7*tci12)/18 + 
      fvu1u1u7*((-2*f59 + f75)/6 + (2*f59*fvu1u1u8)/9 - 
        (2*f59*tci12)/9) + fvu1u1u6*((-2*f59)/9 + 
        ((f59 - 2*f75)*fvu1u1u8)/6 + ((-f59 + 2*f75)*tci12)/
         3)) + fvu1u1u1*((4*(f59 - f75)*fvu1u1u2^2)/3 - 
      (f72*fvu1u1u3^2)/3 + ((-f59 + f72)*fvu1u1u4^2)/3 + 
      ((6*f49 - f51 - 6*f72 + f81)*fvu1u1u5^2)/9 + 
      (4*(2*f59 - f75)*fvu1u1u6^2)/9 - 
      (10*(f59 - 2*f75)*fvu1u1u7^2)/9 + ((f72 - f78)*fvu1u1u8^2)/
       3 + ((-f59 + 2*f75)*fvu2u1u1)/3 + ((f59 - 2*f75)*fvu2u1u2)/
       6 + (f78*fvu2u1u8)/3 + ((-f59 + f75)*fvu2u1u9)/6 + 
      (f75*tci11^2)/18 + ((-6*f49 + f51 + 6*f72 - f80)*
        fvu1u1u8*tci12)/9 + fvu1u1u3*(((f59 - 2*f75)*fvu1u1u4)/
         3 + ((6*f49 - f51 - 6*f72)*tci12)/9) + 
      fvu1u1u4*(((f75 - f78)*fvu1u1u6)/3 + 
        ((f77 - f80)*fvu1u1u7)/9 + 
        ((-6*f49 + f51 + 6*f72 - f81)*fvu1u1u8)/9 + 
        ((f59 - f72)*tci12)/3) + fvu1u1u7*
       (((9*f59 - 4*f75)*fvu1u1u8)/18 + 
        (2*(4*f59 - 5*f75)*tci12)/9) + 
      fvu1u1u2*((-4*(f59 + f75)*fvu1u1u6)/9 - 
        (2*(f59 + f75)*tci12)/9) + fvu1u1u6*
       ((-2*(3*f59 + f75)*fvu1u1u7)/9 + ((f59 + 16*f75)*fvu1u1u8)/
         54 + ((-7*f59 + 8*f75)*tci12)/18)) + 
    ((-4*f59 + f75)*tci12*tcr11^2)/18 + 
    (2*(f59 + f75)*tcr11^3)/9 + 
    ((-f59 - f75)*tci11^2*tcr12)/9 - 
    (2*(2*f59 + f75)*tci12*tcr21)/9 + 
    tcr11*((-2*(7*f59 + f75)*tci11^2)/9 + 
      (2*f59*tci12*tcr12)/9 + (2*(3*f59 + f75)*tcr21)/9) + 
    ((12*f49 - 2*f51 + 3*f61 + 18*f65 - 12*f72 + 2*f77 + 
       2*f80 + 8*f81)*tcr33)/108;
L ieu1ueu7 = 
   f61/3 - (f65*fvu1u1u2^2)/3 + (52*f61*fvu1u1u4^2)/27 + 
    ((12*f8 - 2*f9 - 3*f63 - 12*f69 - f77 - f81)*fvu1u1u5^2)/
     18 - (2*(f59 + 3*f75)*fvu1u1u6^2)/27 + 
    fvu1u1u4*((-6*f49 + f51 + 3*f62 + 6*f72 - f80)/18 - 
      (4*f59*fvu1u1u5)/9 - (2*f59*fvu1u1u6)/9 - 
      (f59*fvu1u1u7)/9) + (f59*fvu2u1u3)/3 + 
    ((4*f59 - f75)*fvu2u1u4)/9 + (f59*fvu2u1u6)/9 + 
    (2*f75*fvu2u1u7)/9 - (f59*fvu2u1u8)/3 - (8*f60*tci11^2)/9 - 
    (8*f62*tci12)/9 - (f59*fvu1u2u3*tci12)/6 + 
    (f59*fvu1u2u6*tci12)/3 + fvu1u1u2*
     (f65/3 - (2*f59*fvu1u1u4)/9 + (f59*fvu1u1u5)/3 - 
      (f59*fvu1u1u6)/6 + ((2*f59 - f75)*fvu1u1u7)/9 - 
      (f59*tci12)/3) + fvu1u1u7*((13*f59 + 54*f75)/108 - 
      (f59*tci12)/9) + fvu1u1u6*((f59 - f72)/6 + 
      (2*f59*fvu1u1u7)/9 + (f59*tci12)/18) + 
    fvu1u1u5*((8*f63)/9 + (2*f59*fvu1u1u6)/9 + (f75*fvu1u1u7)/3 + 
      (4*f59*tci12)/9);
L ieu0ueu7 = 
   (-26*f59 - 9*f69 + 9*f72 - 12*f75 - 12*f78)/36;
L ieum1ueu7 = 0;
L ieum2ueu7 = 0;
L ieu2uou7 = -(f67*fvu4u25) - f67*fvu4u28 - f67*fvu4u39 - 
    f67*fvu4u41 + (f67*fvu4u49)/3 - (f67*fvu4u51)/3 - 
    f67*fvu4u80 - f67*fvu4u83 - f67*fvu4u91 - f67*fvu4u93 + 
    (f67*fvu4u100)/3 - (f67*fvu4u102)/3 + (7*f67*fvu4u171)/4 + 
    (5*f67*fvu4u174)/4 - (3*f67*fvu4u182)/4 - (f67*fvu4u184)/4 - 
    (f67*fvu4u190)/3 - (f67*fvu4u192)/2 + (f67*fvu4u199)/4 + 
    (f67*fvu4u201)/2 + f67*fvu4u202 - 2*f67*fvu4u213 + 
    (2*f67*fvu4u225)/3 + 7*f67*fvu4u244 - (3*f67*fvu4u252)/2 - 
    (7*f67*fvu4u255)/6 + (f67*fvu4u271)/4 - (17*f67*fvu4u273)/4 + 
    (17*f67*fvu4u277)/4 + (f67*fvu4u279)/4 - (f67*fvu4u282)/3 - 
    (f67*fvu4u289)/4 - (f67*fvu4u290)/2 - f67*fvu4u291 - 
    2*f67*fvu4u293 - 2*f67*fvu4u309 + 2*f67*fvu4u325 - 
    2*f67*fvu4u330 + ((f67*fvu3u45)/2 - (f67*fvu3u70)/2 - 
      (f67*fvu3u81)/2)*fvu1u1u1 + fvu3u78*(-(f67*fvu1u1u4) - 
      f67*fvu1u1u5 + f67*fvu1u1u6) + 
    fvu3u25*((f67*fvu1u1u1)/2 - (f67*fvu1u1u3)/2 + 
      (f67*fvu1u1u6)/2 - (f67*fvu1u1u7)/2) + 
    fvu3u81*((f67*fvu1u1u3)/2 + (f67*fvu1u1u6)/2 - 
      (f67*fvu1u1u7)/2) + fvu3u23*((f67*fvu1u1u2)/2 + 
      (f67*fvu1u1u6)/2 - (f67*fvu1u1u7)/2 - (f67*fvu1u1u9)/2) + 
    fvu3u70*((f67*fvu1u1u2)/2 - (f67*fvu1u1u3)/2 + 
      f67*fvu1u1u4 + f67*fvu1u1u5 + f67*fvu1u1u7 - 
      (f67*fvu1u1u9)/2) + fvu3u45*(-(f67*fvu1u1u2)/2 - 
      (f67*fvu1u1u3)/2 + (f67*fvu1u1u9)/2) + 
    fvu3u43*(-(f67*fvu1u1u6)/2 + (f67*fvu1u1u7)/2 + 
      (f67*fvu1u1u9)/2) + (151*f67*tci11^3*tci12)/135 + 
    fvu3u71*(f67*fvu1u1u4 - f67*fvu1u1u5 - f67*fvu1u1u6 - 
      2*f67*tci12) + fvu3u82*(f67*fvu1u1u4 - f67*fvu1u1u5 - 
      f67*fvu1u1u6 - 2*f67*tci12) + 
    fvu3u80*((-5*f67*fvu1u1u6)/2 + (5*f67*fvu1u1u7)/2 - 
      (7*f67*fvu1u1u8)/2 + (5*f67*fvu1u1u9)/2 + 
      (7*f67*tci12)/2) + (2*f67*tci11^2*tci21)/9 + 
    fvu1u1u4*((4*f67*tci11^3)/27 - 4*f67*tci12*tci21) + 
    fvu1u1u5*((4*f67*tci11^3)/27 - 4*f67*tci12*tci21) + 
    fvu1u1u3*(f67*fvu3u71 + f67*fvu3u78 + f67*fvu3u82 - 
      (4*f67*tci11^3)/27 + 4*f67*tci12*tci21) + 
    fvu1u1u8*((-7*f67*tci11^3)/27 + 7*f67*tci12*tci21) + 
    tci12*((48*f67*tci31)/5 + 16*f67*tci32) + 
    (4*f67*tci41)/3 + (4*f67*tci11^3*tcr11)/27 - 
    (f67*tci11*tci12*tcr11^2)/5 + 
    fvu1u1u7*((19*f67*tci11^3)/60 - (23*f67*tci12*tci21)/
       3 + (12*f67*tci31)/5 + f67*tci21*tcr11 - 
      (f67*tci11*tcr11^2)/20) + 
    fvu1u1u9*((91*f67*tci11^3)/540 - (11*f67*tci12*tci21)/
       3 + (12*f67*tci31)/5 + f67*tci21*tcr11 - 
      (f67*tci11*tcr11^2)/20) + 
    fvu1u1u2*(-(f67*fvu3u43)/2 + f67*fvu3u80 + 
      (49*f67*tci11^3)/540 - (10*f67*tci12*tci21)/3 - 
      (12*f67*tci31)/5 - f67*tci21*tcr11 + 
      (f67*tci11*tcr11^2)/20) + 
    fvu1u1u6*((-19*f67*tci11^3)/60 + (23*f67*tci12*tci21)/
       3 - (12*f67*tci31)/5 - f67*tci21*tcr11 + 
      (f67*tci11*tcr11^2)/20) - 
    (40*f67*tci12*tci21*tcr12)/3 - 4*f67*tci11*tci12*
     tcr12^2;
L ieu1uou7 = 2*f67*fvu3u70 + 
    (4*f67*tci11^3)/27 - 4*f67*tci12*tci21;
L ieu0uou7 = 0;
L ieum1uou7 = 0;
L ieum2uou7 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou7+w^2*ieu1uou7+w^3*ieu0uou7+w^4*ieum1uou7+w^5*ieum2uou7;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou7a = K[w^1];
L ieu1uou7a = K[w^2];
L ieu0uou7a = K[w^3];
L ieum1uou7a = K[w^4];
L ieum2uou7a = K[w^5];
.sort
#write <e7.tmp> "`optimmaxvar_'"
#write <e7_odd.c> "%O"
#write <e7_odd.c> "return Eps5o2<T>("
#write <e7_odd.c> "%E", ieu2uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieu1uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieu0uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieum1uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieum2uou7a
#write <e7_odd.c> ");\n}"
L H=+u^1*ieu2ueu7+u^2*ieu1ueu7+u^3*ieu0ueu7+u^4*ieum1ueu7+u^5*ieum2ueu7;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu7a = H[u^1];
L ieu1ueu7a = H[u^2];
L ieu0ueu7a = H[u^3];
L ieum1ueu7a = H[u^4];
L ieum2ueu7a = H[u^5];
.sort
#write <e7.tmp> "`optimmaxvar_'"
#write <e7_even.c> "%O"
#write <e7_even.c> "return Eps5o2<T>("
#write <e7_even.c> "%E", ieu2ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieu1ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieu0ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieum1ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieum2ueu7a
#write <e7_even.c> ");\n}"
.end
