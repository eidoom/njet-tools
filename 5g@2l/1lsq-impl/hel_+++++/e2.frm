#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f16;
S f1;
S f39;
S f48;
S f45;
S f30;
S f47;
S f32;
S f41;
S f35;
S f43;
S f37;
S f58;
S f52;
S f50;
S f56;
S f54;
S fvu1u2u9;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu2u1u9;
S fvu3u6;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u279;
S fvu4u313;
S fvu4u91;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u41;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu2 = f35/3 + ((6*f1 - f16 - f50 - 6*f56)*fvu3u1)/
     9 + ((f30 - f48)*fvu3u2)/3 + ((-f50 + f58)*fvu3u3)/9 + 
    ((3*f30 - 2*f48 + 2*f56)*fvu3u4)/6 + 
    (2*(f30 - f48 + f56)*fvu3u5)/3 + 
    ((-f30 + 4*f48 - 6*f56)*fvu3u6)/18 + (f56*fvu3u7)/9 + 
    (f45*fvu3u10)/6 + (2*(f48 - f56)*fvu3u11)/3 + 
    ((-f30 - 2*f48 + f56)*fvu3u12)/9 + 
    ((f30 + 2*f48 - f56)*fvu3u13)/3 + 
    ((f30 + 2*f48 - f56)*fvu3u14)/12 + ((3*f52 - f56)*fvu3u15)/
     18 - (f52*fvu3u17)/3 - (f54*fvu3u18)/9 - (f52*fvu3u19)/3 - 
    (f54*fvu3u22)/9 + ((-f32 - f35 + f37 + f39 + f41)*
      fvu1u1u1^3)/3 + (8*(f32 + f35 - f39)*fvu1u1u2^3)/9 + 
    (f56*fvu1u1u3^3)/6 + (f56*fvu1u1u5^3)/9 + 
    ((-6*f1 + f16 - 3*f37 + 6*f56 - f58)*fvu1u1u8^2)/9 + 
    fvu1u1u4^2*((8*f35)/9 + (5*f56*fvu1u1u5)/18 + 
      (f56*fvu1u1u8)/9 + (2*f56*fvu1u1u9)/9) + 
    (f56*fvu2u1u2)/18 - (f56*fvu2u1u3)/18 - 
    (13*f56*fvu2u1u11)/108 + ((f47 - f58)*fvu2u1u12)/9 - 
    (f56*fvu1u2u7*tci11^2)/9 + (f56*tci12)/3 - 
    (f56*fvu1u2u6*tci12)/18 + ((-f47 + f58)*fvu1u2u9*tci12)/
     9 + (f56*fvu2u1u1*tci12)/3 + (f56*fvu2u1u5*tci12)/9 + 
    fvu1u1u8*((f32 + f35 - f39)/3 + (f47*fvu1u1u9)/9 + 
      (f56*fvu2u1u9)/3 - (f37*tci11^2)/3 - (8*f37*tci12)/9) + 
    fvu1u1u1^2*((-f30 + f45)/6 + (f52*fvu1u1u2)/3 + 
      (f54*fvu1u1u3)/9 - (f52*fvu1u1u4)/3 - (f54*fvu1u1u8)/9 + 
      ((-f32 - f35 + f37 + f39 + f41)*tci12)/3) + 
    fvu1u1u9*(f56/9 - (f56*fvu2u1u11)/9 + 
      ((f45 + 2*f52 - f56)*tci11^2)/6 + 
      ((-7*f30 - 5*f56)*tci12)/36) + 
    tci11^2*((8*f32)/9 + ((f30 - 2*f48 - f56)*tci12)/6) + 
    fvu1u1u9^2*(-f56/3 - (17*f56*tci12)/27) + 
    fvu1u1u5^2*(-(f56*fvu1u1u9)/3 - (2*f56*tci12)/9) + 
    fvu2u1u9*((f47 + f54)/9 + (f56*tci12)/9) + 
    fvu1u1u2^2*((-2*f56)/27 + (f56*fvu1u1u4)/6 + 
      (f56*fvu1u1u5)/9 - (f56*fvu1u1u9)/3 + (f56*tci12)/6) + 
    fvu1u1u3^2*((-2*f56*fvu1u1u4)/9 + (4*f56*fvu1u1u8)/9 + 
      (4*f56*tci12)/9) + fvu1u1u5*(-(f56*fvu1u1u9^2)/6 - 
      (f56*fvu2u1u4)/3 - (f56*fvu2u1u5)/3 + (4*f56*tci11^2)/9 + 
      (f56*fvu1u1u9*tci12)/3 + (f56*fvu1u2u7*tci12)/6) + 
    fvu1u1u3*((2*f56*fvu1u1u4^2)/9 + (f56*fvu1u1u5^2)/9 - 
      (f56*fvu1u1u8^2)/6 - (f56*fvu2u1u1)/9 - 
      (2*f56*fvu2u1u5)/9 - (f56*tci11^2)/3 - 
      (f56*fvu1u1u5*tci12)/6 + (f56*fvu1u1u8*tci12)/3 + 
      (f56*fvu1u2u7*tci12)/6 + fvu1u1u4*((f56*fvu1u1u8)/6 + 
        (f56*tci12)/18)) + fvu1u1u1*((-8*f41)/9 + 
      ((6*f1 - f16 + f54 - 6*f56)*fvu1u1u3^2)/9 - 
      (f56*fvu1u1u4^2)/9 + (2*f56*fvu1u1u5^2)/9 + 
      (f56*fvu1u1u8^2)/9 - (f47*fvu1u1u9)/9 + (f56*fvu2u1u1)/6 - 
      (f56*fvu2u1u9)/6 + ((3*f41 + f47 - f58)*tci11^2)/18 - 
      (f41*tci12)/3 + fvu1u1u2*((f52 - f56)/3 + 
        ((f52 - f56)*tci12)/3) + fvu1u1u3*((f56*fvu1u1u4)/3 - 
        (f56*tci12)/6) + fvu1u1u8*(-f58/9 + (f56*tci12)/9) + 
      fvu1u1u4*(-f30/6 + (f56*fvu1u1u8)/9 + (2*f56*tci12)/9)) + 
    fvu1u1u4*((-5*f56)/6 + (2*f56*fvu1u1u5^2)/9 - 
      (f56*fvu1u1u8^2)/18 + (f56*fvu1u1u9^2)/6 + 
      (52*f35*tci12)/27 + fvu1u1u5*((-4*f56*fvu1u1u9)/9 - 
        (2*f56*tci12)/9) + fvu1u1u8*(f58/9 + (f56*tci12)/9) + 
      fvu1u1u9*(-f56/3 + (f56*tci12)/2)) + 
    fvu1u1u2*((f32 + f35 - f37 - f39 - f41)/6 - 
      (f56*fvu1u1u3^2)/18 - (f56*fvu1u1u4^2)/9 - 
      (5*f56*fvu1u1u5^2)/18 - (f56*fvu1u1u8)/3 + 
      (5*f56*fvu1u1u9^2)/9 + (2*f56*fvu2u1u4)/9 + 
      (f56*fvu2u1u11)/3 - (f39*tci11^2)/3 - (4*f56*tci12)/9 - 
      (f56*fvu1u1u3*tci12)/6 + (f56*fvu1u1u5*tci12)/9 + 
      fvu1u1u9*(f56/3 - (f56*tci12)/6) + 
      fvu1u1u4*(f56/18 - (f56*fvu1u1u5)/3 + (f56*fvu1u1u9)/3 + 
        ((-6*f1 + f16 + 6*f56)*tci12)/9)) + 
    (f48*tci12*tcr11^2)/3 + (f50*tcr11^3)/9 + 
    ((-3*f48 + f56)*tci11^2*tcr12)/18 - (f48*tcr12^3)/3 + 
    (f48*tci12*tcr21)/3 + 
    tcr11*(((7*f30 - 6*f45 + 2*f48 + 6*f52 - 19*f56)*
        tci11^2)/36 + (f50*tci12*tcr12)/9 - 
      (f56*tcr21)/6) - (f50*tcr31)/9 + 
    ((-f48 - f56)*tcr32)/3 + ((f45 - f48 - f56)*tcr33)/18;
L ieu1ueu2 = (-11*f30 - 18*f45 - 74*f48 + 18*f52 + 
      11*f56)/72 - (8*(f32 + f35 - f37 - f39 - f41)*fvu1u1u1^2)/
     9 + (f39*fvu1u1u2^2)/6 + ((-f30 + f56)*fvu1u1u8^2)/3 + 
    fvu1u1u8*((3*f30 + f45 - 2*f48 - f52 - 4*f56)/36 + 
      (f45*fvu1u1u9)/3) + fvu1u1u4*((7*f56)/18 + 
      (f30*fvu1u1u8)/3 - (8*f56*fvu1u1u9)/9) + 
    (2*f56*fvu2u1u2)/9 - (f56*fvu2u1u3)/9 + 
    ((f45 + f52)*fvu2u1u9)/3 + (f56*fvu2u1u11)/3 + 
    ((-f30 + f45)*fvu2u1u12)/3 + (f32*tci11^2)/3 + 
    ((6*f1 - f16 + 3*f37 - 2*f50 - 6*f56 + f58)*tci12)/18 + 
    (f56*fvu1u2u6*tci12)/18 + ((f30 - f45)*fvu1u2u9*tci12)/
     3 + fvu1u1u9*((6*f1 - f16 - 3*f32 + f47 + 2*f54 - 6*f56)/
       18 - (4*f56*tci12)/9) + fvu1u1u2*((-8*f39)/9 + 
      (2*f56*fvu1u1u4)/9 - (f56*fvu1u1u8)/6 - 
      (2*f56*fvu1u1u9)/9 - (f56*tci12)/3) + 
    fvu1u1u1*(-f41/3 + ((6*f1 - f16 + f54 - 6*f56)*fvu1u1u2)/
       9 + ((-f30 + f56)*fvu1u1u4)/3 - (f30*fvu1u1u8)/3 - 
      (f45*fvu1u1u9)/3 + (f56*tci12)/54);
L ieu0ueu2 = (12*f1 - 2*f16 + 18*f32 + 21*f35 - 18*f39 + 
     2*f47 - 2*f50 - 12*f56)/108;
L ieum1ueu2 = 0;
L ieum2ueu2 = 0;
L ieu2uou2 = (4*f43*fvu4u25)/3 - (10*f43*fvu4u28)/3 + 
    (2*f43*fvu4u39)/3 + (4*f43*fvu4u41)/3 - (4*f43*fvu4u49)/9 - 
    (2*f43*fvu4u51)/3 + (2*f43*fvu4u80)/3 + (8*f43*fvu4u83)/3 - 
    2*f43*fvu4u91 + (2*f43*fvu4u93)/3 - (2*f43*fvu4u100)/9 + 
    (4*f43*fvu4u102)/9 + (2*f43*fvu4u139)/3 - (2*f43*fvu4u148)/9 - 
    (17*f43*fvu4u171)/12 + (3*f43*fvu4u174)/4 + (3*f43*fvu4u182)/4 - 
    (17*f43*fvu4u184)/12 + (17*f43*fvu4u192)/18 - (f43*fvu4u199)/4 - 
    (f43*fvu4u201)/2 - f43*fvu4u202 - (2*f43*fvu4u213)/3 + 
    (2*f43*fvu4u215)/3 - (2*f43*fvu4u221)/3 - 5*f43*fvu4u244 - 
    (8*f43*fvu4u246)/3 + 2*f43*fvu4u250 + (13*f43*fvu4u252)/6 + 
    (f43*fvu4u255)/2 - (7*f43*fvu4u271)/12 + (43*f43*fvu4u273)/12 - 
    (43*f43*fvu4u277)/12 - (7*f43*fvu4u279)/12 + 
    (2*f43*fvu4u282)/3 + (f43*fvu4u289)/4 + (f43*fvu4u290)/2 + 
    f43*fvu4u291 + (2*f43*fvu4u293)/3 + (2*f43*fvu4u295)/3 - 
    2*f43*fvu4u313 + fvu3u71*((-2*f43*fvu1u1u2)/3 + 
      (2*f43*fvu1u1u5)/3 + (2*f43*fvu1u1u8)/3) + 
    fvu3u43*((f43*fvu1u1u6)/3 - (f43*fvu1u1u7)/3 - 
      (f43*fvu1u1u9)/3) + fvu3u23*((-2*f43*fvu1u1u2)/3 - 
      (2*f43*fvu1u1u6)/3 + (2*f43*fvu1u1u7)/3 + 
      (2*f43*fvu1u1u9)/3) + fvu3u81*((-2*f43*fvu1u1u3)/3 - 
      (f43*fvu1u1u4)/3 - (2*f43*fvu1u1u6)/3 + 
      (2*f43*fvu1u1u7)/3 + (f43*fvu1u1u9)/3 - 
      (f43*fvu1u1u10)/3) + fvu3u70*(-(f43*fvu1u1u3)/3 - 
      (f43*fvu1u1u6)/3 + (f43*fvu1u1u8)/3 - (f43*fvu1u1u9)/3 + 
      (f43*fvu1u1u10)/3) + fvu3u45*((f43*fvu1u1u3)/3 - 
      (2*f43*fvu1u1u5)/3 + (4*f43*fvu1u1u6)/3 - 
      (5*f43*fvu1u1u7)/3 + f43*fvu1u1u8 + (2*f43*fvu1u1u9)/3 + 
      (f43*fvu1u1u10)/3) + fvu3u63*(-(f43*fvu1u1u6)/3 - 
      (f43*fvu1u1u7)/3 - (f43*fvu1u1u9)/3 + (2*f43*fvu1u1u10)/
       3) + (13*f43*tci11^3*tci12)/15 + 
    fvu3u80*(-(f43*fvu1u1u2)/3 + (f43*fvu1u1u3)/3 + 
      (f43*fvu1u1u4)/3 - (2*f43*fvu1u1u5)/3 + 
      (11*f43*fvu1u1u6)/6 - (13*f43*fvu1u1u7)/6 + 
      (5*f43*fvu1u1u8)/2 - (13*f43*fvu1u1u9)/6 - 
      (7*f43*tci12)/2) + fvu3u78*(-(f43*fvu1u1u2)/3 + 
      (f43*fvu1u1u3)/3 + (f43*fvu1u1u4)/3 - (f43*fvu1u1u6)/3 + 
      (f43*fvu1u1u8)/3 - (2*f43*tci12)/3) + 
    fvu3u82*(-(f43*fvu1u1u3)/3 - (f43*fvu1u1u4)/3 + 
      (f43*fvu1u1u7)/3 - (f43*fvu1u1u8)/3 + (f43*fvu1u1u9)/3 + 
      (2*f43*tci12)/3) + fvu3u25*((2*f43*fvu1u1u1)/3 + 
      (f43*fvu1u1u2)/3 + (f43*fvu1u1u3)/3 - 2*f43*fvu1u1u4 + 
      (2*f43*fvu1u1u5)/3 + f43*fvu1u1u6 - (2*f43*fvu1u1u7)/3 - 
      f43*fvu1u1u8 - f43*fvu1u1u9 - (f43*fvu1u1u10)/3 + 
      2*f43*tci12) - (10*f43*tci11^2*tci21)/27 + 
    fvu1u1u10*((4*f43*tci11^3)/81 - (4*f43*tci12*tci21)/
       3) + tci12*((16*f43*tci31)/5 + 16*f43*tci32) - 
    (2072*f43*tci41)/135 - (64*f43*tci42)/5 - 
    (16*f43*tci43)/3 + ((217*f43*tci11^3)/405 + 
      (4*f43*tci12*tci21)/3 + 16*f43*tci32)*tcr11 + 
    (13*f43*tci11*tcr11^3)/45 + 
    fvu1u1u1*((-4*f43*fvu3u45)/3 - (2*f43*fvu3u71)/3 - 
      (f43*fvu3u78)/3 + (f43*fvu3u80)/3 + f43*fvu3u81 + 
      (f43*fvu3u82)/3 + (53*f43*tci11^3)/405 + 
      (48*f43*tci31)/5 + 4*f43*tci21*tcr11 - 
      (f43*tci11*tcr11^2)/5) + 
    fvu1u1u2*((f43*fvu3u43)/3 + (f43*fvu3u63)/3 + 
      (f43*fvu3u70)/3 + (23*f43*tci11^3)/162 - 
      (8*f43*tci12*tci21)/9 + 8*f43*tci31 + 
      (10*f43*tci21*tcr11)/3 - (f43*tci11*tcr11^2)/6) + 
    fvu1u1u5*((-2*f43*tci11^3)/45 + (32*f43*tci12*tci21)/
       9 + (32*f43*tci31)/5 + (8*f43*tci21*tcr11)/3 - 
      (2*f43*tci11*tcr11^2)/15) + 
    fvu1u1u6*((161*f43*tci11^3)/810 - (43*f43*tci12*tci21)/
       9 + (8*f43*tci31)/5 + (2*f43*tci21*tcr11)/3 - 
      (f43*tci11*tcr11^2)/30) + 
    fvu1u1u7*((-161*f43*tci11^3)/810 + (43*f43*tci12*tci21)/
       9 - (8*f43*tci31)/5 - (2*f43*tci21*tcr11)/3 + 
      (f43*tci11*tcr11^2)/30) + 
    fvu1u1u8*((4*f43*tci11^3)/135 - (13*f43*tci12*tci21)/
       3 - (48*f43*tci31)/5 - 4*f43*tci21*tcr11 + 
      (f43*tci11*tcr11^2)/5) + 
    fvu1u1u4*((-11*f43*tci11^3)/135 - (4*f43*tci12*tci21)/
       3 - (48*f43*tci31)/5 - 4*f43*tci21*tcr11 + 
      (f43*tci11*tcr11^2)/5) + 
    fvu1u1u9*((-83*f43*tci11^3)/270 + 3*f43*tci12*tci21 - 
      (72*f43*tci31)/5 - 6*f43*tci21*tcr11 + 
      (3*f43*tci11*tcr11^2)/10) + 
    ((-218*f43*tci11^3)/243 - (40*f43*tci12*tci21)/3)*
     tcr12 + (-4*f43*tci11*tci12 - (40*f43*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f43*tci11*tci12)/15 - 
      2*f43*tci21 - 2*f43*tci11*tcr12) + 
    (130*f43*tci11*tcr33)/27;
L ieu1uou2 = 
   -2*f43*fvu3u25 - (11*f43*tci11^3)/135 - 
    (4*f43*tci12*tci21)/3 - (48*f43*tci31)/5 - 
    4*f43*tci21*tcr11 + (f43*tci11*tcr11^2)/5;
L ieu0uou2 = 0;
L ieum1uou2 = 0;
L ieum2uou2 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou2+w^2*ieu1uou2+w^3*ieu0uou2+w^4*ieum1uou2+w^5*ieum2uou2;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou2a = K[w^1];
L ieu1uou2a = K[w^2];
L ieu0uou2a = K[w^3];
L ieum1uou2a = K[w^4];
L ieum2uou2a = K[w^5];
.sort
#write <e2.tmp> "`optimmaxvar_'"
#write <e2_odd.c> "%O"
#write <e2_odd.c> "return Eps5o2<T>("
#write <e2_odd.c> "%E", ieu2uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieu1uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieu0uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieum1uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieum2uou2a
#write <e2_odd.c> ");\n}"
L H=+u^1*ieu2ueu2+u^2*ieu1ueu2+u^3*ieu0ueu2+u^4*ieum1ueu2+u^5*ieum2ueu2;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu2a = H[u^1];
L ieu1ueu2a = H[u^2];
L ieu0ueu2a = H[u^3];
L ieum1ueu2a = H[u^4];
L ieum2ueu2a = H[u^5];
.sort
#write <e2.tmp> "`optimmaxvar_'"
#write <e2_even.c> "%O"
#write <e2_even.c> "return Eps5o2<T>("
#write <e2_even.c> "%E", ieu2ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieu1ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieu0ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieum1ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieum2ueu2a
#write <e2_even.c> ");\n}"
.end
