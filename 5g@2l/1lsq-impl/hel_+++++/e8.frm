#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f151;
S f150;
S f153;
S f152;
S f155;
S f154;
S f156;
S f148;
S f149;
S f81;
S f146;
S f147;
S f144;
S f145;
S f113;
S f112;
S f59;
S f52;
S f54;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu4u233;
S fvu4u141;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu3u14;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu2u1u8;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu4u91;
S fvu3u53;
S fvu4u309;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu8 = (-52*f146)/27 - (2*f151*fvu3u1)/9 + 
    (f151*fvu3u2)/18 - (2*f151*fvu3u3)/9 - (f151*fvu3u4)/6 - 
    (4*f155*fvu3u5)/9 - (f151*fvu3u6)/6 - (5*f155*fvu3u7)/9 - 
    (f155*fvu3u8)/6 + (5*f155*fvu3u11)/18 + 
    ((f144 + f151)*fvu3u12)/6 + ((-f151 - 3*f155)*fvu3u13)/9 + 
    (f151*fvu3u14)/9 + ((-8*f151 - 3*f155)*fvu3u15)/18 - 
    (2*f155*fvu3u17)/9 + (2*f151*fvu3u18)/9 + (2*f151*fvu3u19)/9 + 
    (2*f151*fvu3u22)/9 - (2*f155*fvu3u24)/9 - 
    (2*(f151 - 3*f155)*fvu3u31)/9 - (2*f151*fvu3u32)/9 - 
    (2*f151*fvu3u33)/9 + (2*f151*fvu3u34)/9 + (2*f151*fvu3u35)/9 - 
    (4*f151*fvu3u36)/9 + ((-4*f151 + f155)*fvu3u37)/18 - 
    (f151*fvu3u40)/18 + (2*f151*fvu3u44)/9 + (5*f155*fvu3u52)/9 - 
    (7*f155*fvu3u53)/18 - (2*f155*fvu3u55)/9 + (2*f155*fvu3u56)/9 - 
    (2*f155*fvu3u57)/9 + ((4*f151 - f155)*fvu3u59)/9 - 
    (f151*fvu1u1u1^3)/6 + ((-6*f59 + f81 + 6*f112 - f113 + 
       3*f145 - 3*f146 - 3*f147 + 3*f148 - 3*f149 - 6*f153 + 
       6*f155)*fvu1u1u2^3)/18 + (f151*fvu1u1u3^3)/6 - 
    (4*f151*fvu1u1u5^3)/9 + ((f145 - f146 - f147 + f148 - f149)*
      fvu1u1u6^3)/3 + ((6*f52 - f54 - 6*f154)*fvu1u1u7^3)/9 + 
    (f155*fvu1u1u8^3)/3 + (2*(13*f151 + 3*f155)*fvu1u1u9^3)/27 + 
    fvu1u1u4^2*((-f144 + f153)/6 - (2*f151*fvu1u1u5)/3 + 
      (4*f151*fvu1u1u6)/9 + (2*f151*fvu1u1u7)/3 + 
      ((9*f151 - 5*f155)*fvu1u1u8)/54 - (f155*fvu1u1u9)/3) - 
    (f155*fvu2u1u6)/18 + (2*f151*fvu2u1u14)/9 + 
    (f151*fvu2u1u15)/3 + (2*f155*fvu1u2u2*tci11^2)/3 + 
    (16*f155*fvu1u2u7*tci11^2)/9 - (f155*fvu1u2u8*tci11^2)/
     6 - (2*f155*fvu1u2u9*tci11^2)/27 + 
    ((-3*f148 + f152 - f156)*tci12)/18 + 
    (f155*fvu1u2u3*tci12)/3 + (2*f155*fvu2u1u1*tci12)/9 - 
    (f155*fvu2u1u2*tci12)/18 + (f155*fvu2u1u5*tci12)/6 - 
    (f155*fvu2u1u8*tci12)/9 + (2*f155*fvu2u1u9*tci12)/9 + 
    (13*f155*fvu2u1u10*tci12)/18 + (2*f155*fvu2u1u12*tci12)/
     3 - (f155*fvu2u1u13*tci12)/3 + 
    tci11^2*((-48*f59 + 8*f81 + 12*f112 - 2*f113 + 18*f145 - 
        21*f146 - 18*f147 + 18*f148 - 18*f149 + 2*f152 - 
        12*f153 + 48*f155 + 2*f156)/108 - (5*f151*tci12)/27) + 
    fvu1u1u3^2*(-(f151*fvu1u1u4)/9 + (2*f155*fvu1u1u6)/3 + 
      ((-f151 - 6*f155)*fvu1u1u8)/18 + (f155*fvu1u1u9)/3 - 
      (2*f155*fvu1u1u10)/3 + (f151*tci12)/6) + 
    fvu1u1u5^2*(((-f151 - f154)*fvu1u1u6)/3 + 
      ((6*f52 - f54 - f152 - 6*f154)*fvu1u1u7)/9 - 
      (2*f151*fvu1u1u8)/3 + (2*f155*fvu1u1u9)/9 + 
      (2*f151*tci12)/9) + fvu1u1u6^2*
     (f149/6 + ((-f153 + f155)*fvu1u1u7)/3 + 
      ((-6*f59 + f81 + 6*f112 - f113 - 6*f153 + 6*f155)*
        fvu1u1u8)/9 - (2*f155*fvu1u1u9)/9 + (f155*fvu1u1u10)/6 + 
      ((-f144 + f153)*tci12)/3) + fvu1u1u7^2*
     ((f154*fvu1u1u8)/3 + ((-6*f52 + f54 + 6*f154)*tci12)/9) + 
    fvu1u1u1^2*(((-7*f151 - 2*f155)*fvu1u1u2)/18 + 
      ((-12*f151 - f155)*fvu1u1u3)/18 + 
      ((4*f151 - f155)*fvu1u1u4)/9 - (2*f151*fvu1u1u6)/9 - 
      (4*f151*fvu1u1u7)/9 + ((4*f151 - f155)*fvu1u1u8)/9 + 
      (4*f155*fvu1u1u9)/9 + ((-8*f151 - f155)*tci12)/18) + 
    fvu1u1u10^2*(f151 - (4*f155*tci12)/9) + 
    fvu1u1u8^2*((2*f155*fvu1u1u9)/9 - (2*f155*fvu1u1u10)/9 + 
      (f155*tci12)/18) + fvu2u1u3*(-f155/2 + 
      (f155*tci12)/6) + fvu1u2u6*((2*f155*tci11^2)/9 + 
      (f155*tci12)/3) + fvu1u1u9^2*
     ((12*f52 - 2*f54 + 6*f59 - f81 - 3*f145 - f152 - 12*f154 - 
        6*f155)/18 + (7*f155*tci12)/18) + 
    fvu2u1u11*((-4*f155)/9 + (2*f155*tci12)/3) + 
    fvu1u1u2^2*((-f145 + f146 + f147 - f148 + f149)/3 + 
      (2*f151*fvu1u1u4)/9 - (2*f151*fvu1u1u5)/9 + 
      ((-f151 + 11*f155)*fvu1u1u6)/18 + (f155*fvu1u1u8)/6 - 
      (f155*fvu1u1u9)/3 - (f155*fvu1u1u10)/6 + 
      ((2*f151 + f155)*tci12)/18) + 
    fvu1u1u10*((-f153 + f155)/6 + (2*f155*fvu2u1u13)/3 - 
      (2*f155*fvu2u1u14)/9 - (10*f155*fvu2u1u15)/9 + 
      (f148*tci11^2)/3 + (f156*tci12)/9 + 
      (2*f155*fvu1u2u2*tci12)/9) + 
    fvu1u1u7*(((-f154 - f155)*fvu1u1u8^2)/3 + 
      ((6*f52 - f54 + 6*f59 - f81 - 6*f154 - 6*f155)*fvu2u1u7)/
       9 - (f155*fvu2u1u8)/3 + (f155*fvu2u1u10)/6 - 
      (f155*tci11^2)/6 + ((6*f52 - f54 + 6*f59 - f81 - 
         6*f154 - 6*f155)*fvu1u1u8*tci12)/9 + 
      ((-f154 - f155)*fvu1u2u8*tci12)/3) + 
    fvu1u1u8*(-(f155*fvu1u1u9^2)/9 - (f155*fvu1u1u10^2)/3 - 
      (f155*fvu2u1u8)/9 - (f155*fvu2u1u9)/3 - 
      (f155*fvu2u1u10)/6 + (f155*fvu2u1u11)/6 + 
      (5*f155*fvu2u1u12)/9 - (f155*tci11^2)/3 + 
      (f155*fvu1u1u9*tci12)/3 - (2*f155*fvu1u1u10*tci12)/9 - 
      (4*f155*fvu1u2u8*tci12)/9 + (4*f155*fvu1u2u9*tci12)/
       9) + fvu1u1u9*((8*f145)/9 + (f155*fvu1u1u10)/3 - 
      (7*f155*fvu1u1u10^2)/9 - (7*f155*fvu2u1u11)/27 - 
      (f155*fvu2u1u12)/9 - (2*f155*fvu2u1u15)/9 + 
      ((-8*f151 - 3*f155)*tci11^2)/27 + (f155*tci12)/54 + 
      ((-6*f59 + f81 + 6*f155)*fvu1u2u9*tci12)/9) + 
    fvu1u1u3*((f151*fvu1u1u4^2)/3 + (f151*fvu1u1u5^2)/3 - 
      (f155*fvu1u1u6^2)/3 - (f151*fvu1u1u8^2)/6 - 
      (f155*fvu1u1u9^2)/9 + (4*f151*fvu2u1u1)/9 - 
      (f151*fvu2u1u5)/6 - (2*f155*fvu2u1u13)/9 + 
      (f151*tci11^2)/3 + (f151*fvu1u1u5*tci12)/9 - 
      (f151*fvu1u1u8*tci12)/3 + (2*f155*fvu1u1u9*tci12)/9 + 
      (2*f155*fvu1u2u2*tci12)/9 - (2*f151*fvu1u2u7*tci12)/9 + 
      fvu1u1u4*((-2*f151*fvu1u1u8)/9 - (2*f151*tci12)/9) + 
      fvu1u1u6*((f155*fvu1u1u9)/9 - (f155*fvu1u1u10)/9 - 
        (f155*tci12)/9)) + fvu1u1u6*((-8*f149)/9 + 
      ((-6*f112 + f113 + 6*f153 - f156)*fvu1u1u7^2)/9 + 
      (f154*fvu1u1u8^2)/3 - (f155*fvu1u1u9^2)/9 + 
      (f154*fvu2u1u8)/3 - (f155*fvu2u1u14)/3 - 
      (7*f151*tci11^2)/27 - (f154*tci12)/3 + 
      ((-6*f52 + f54 + 6*f154)*fvu1u1u8*tci12)/9 + 
      fvu1u1u9*(f151/2 + (f155*tci12)/18) + 
      fvu1u1u10*((8*f155)/9 + (f155*tci12)/6)) + 
    fvu1u1u4*((-8*f147)/9 - (13*f151*fvu1u1u5^2)/18 - 
      (f151*fvu1u1u6^2)/3 + (f151*fvu1u1u7^2)/6 - 
      (2*f151*fvu1u1u8^2)/9 - (2*f155*fvu1u1u9^2)/9 - 
      (10*f155*fvu1u1u10)/9 - (2*(4*f151 + f155)*fvu2u1u2)/9 + 
      (2*f151*fvu2u1u3)/3 - (2*(4*f151 + f155)*fvu2u1u8)/9 - 
      (f155*fvu2u1u11)/9 + ((-6*f112 + f113 + 3*f147 + 6*f153 - 
         f156)*tci11^2)/18 + (2*(f151 + 6*f155)*tci12)/27 - 
      (2*f151*fvu1u2u6*tci12)/9 + fvu1u1u8*
       (-(f155*fvu1u1u9)/9 - (2*f151*tci12)/3) + 
      fvu1u1u7*((2*f151*fvu1u1u8)/9 - (2*f151*tci12)/9) + 
      fvu1u1u5*(((6*f59 - f81 - 6*f155)*fvu1u1u9)/9 + 
        (f151*tci12)/3) + fvu1u1u6*((-2*f151)/3 - 
        (f152*fvu1u1u7)/9 - (2*f151*fvu1u1u8)/3 + 
        (20*f151*tci12)/9) + fvu1u1u9*((8*f155)/9 + 
        (2*f155*tci12)/9)) + fvu1u1u1*
     (((-4*f151 + f155)*fvu1u1u2^2)/9 + (f151*fvu1u1u3^2)/3 + 
      (4*f151*fvu1u1u4^2)/9 + (2*(4*f151 + f155)*fvu1u1u5^2)/9 + 
      (2*(4*f151 + f155)*fvu1u1u6^2)/9 + (f151*fvu1u1u7^2)/18 + 
      ((2*f151 - 9*f155)*fvu1u1u8^2)/18 + (11*f155*fvu1u1u9^2)/54 - 
      (f151*fvu2u1u1)/3 + (2*f151*fvu2u1u2)/9 - 
      (f151*fvu2u1u8)/9 + (f151*fvu2u1u9)/6 - 
      (f155*fvu2u1u11)/6 - (f151*tci11^2)/3 + 
      (f155*fvu1u1u9*tci12)/18 + fvu1u1u3*
       ((5*f151*fvu1u1u4)/9 - (8*f151*tci12)/9) + 
      fvu1u1u2*((-8*f155*fvu1u1u4)/9 - (5*f151*fvu1u1u6)/18 - 
        (2*f155*fvu1u1u8)/3 + (2*f155*fvu1u1u9)/9 - 
        (2*f151*tci12)/9) + fvu1u1u7*((f151*fvu1u1u8)/6 - 
        (f151*tci12)/9) + fvu1u1u8*((-4*f155*fvu1u1u9)/9 + 
        (f151*tci12)/18) + fvu1u1u4*(-(f151*fvu1u1u6)/6 - 
        (f151*fvu1u1u7)/3 + (f151*fvu1u1u8)/3 + 
        (4*f155*fvu1u1u9)/9 + (f152*tci12)/9) + 
      fvu1u1u6*((2*(2*f151 + f155)*fvu1u1u7)/9 + 
        (16*f151*fvu1u1u8)/9 + ((-2*f151 + f155)*tci12)/18)) + 
    fvu1u1u2*((-12*f151 - 3*f153 + 3*f154 + 7*f155)/108 + 
      (5*f151*fvu1u1u3^2)/9 - (2*f151*fvu1u1u4^2)/3 + 
      (5*f151*fvu1u1u5^2)/9 + (2*(4*f151 + f155)*fvu1u1u6^2)/9 + 
      ((-6*f59 + f81 + 6*f155)*fvu1u1u8^2)/9 - 
      (f155*fvu1u1u9^2)/3 + (2*f155*fvu1u1u10^2)/3 + 
      (f151*fvu2u1u3)/3 + (2*f151*fvu2u1u4)/3 - 
      (f155*fvu2u1u11)/3 + ((-f145 + f146 + f147 - f148 + f149)*
        tci11^2)/3 + ((16*f151 + 13*f155)*tci12)/108 + 
      ((2*f151 + f155)*fvu1u1u3*tci12)/18 - 
      (10*f151*fvu1u1u5*tci12)/9 + ((-f151 - f155)*fvu1u2u6*
        tci12)/9 + fvu1u1u4*((4*f155)/3 + 
        ((-2*f151 - f155)*fvu1u1u5)/9 - (f155*fvu1u1u8)/3 + 
        (f155*fvu1u1u9)/3 + (2*f151*tci12)/9) + 
      fvu1u1u9*(f155/3 - (f155*tci12)/6) + 
      fvu1u1u8*((f155*fvu1u1u9)/3 + ((-6*f59 + f81 + 6*f155)*
          fvu1u1u10)/9 - (f155*tci12)/6) + 
      fvu1u1u10*(-f155/3 + (f155*tci12)/3) + 
      fvu1u1u6*(f151/3 + (2*(4*f151 + f155)*tci12)/9)) + 
    fvu1u1u5*(((-f144 + f151)*fvu1u1u6^2)/3 + 
      ((-6*f112 + f113 + 6*f153)*fvu1u1u8^2)/9 + 
      (2*f155*fvu1u1u9^2)/9 - (f153*fvu2u1u4)/3 + 
      ((6*f112 - f113 - 6*f153)*fvu2u1u5)/9 + 
      ((f153 - f155)*fvu2u1u7)/3 + 
      ((6*f59 - f81 - 6*f112 + f113 + 6*f153 - 6*f155)*
        tci11^2)/9 - (f153*fvu1u1u8*tci12)/3 - 
      (4*f155*fvu1u1u9*tci12)/9 + ((6*f112 - f113 - 6*f153)*
        fvu1u2u7*tci12)/9 + fvu1u1u7*(-(f153*fvu1u1u8)/6 + 
        (f153*tci12)/3) + fvu1u1u6*(((f152 - f156)*fvu1u1u8)/
         9 + ((8*f151 + 3*f153 - 3*f154 + 16*f155)*tci12)/18)) - 
    (f155*tci12*tcr11^2)/3 - (2*f155*tcr11^3)/9 + 
    (f155*tci11^2*tcr12)/2 + (5*f155*tci12*tcr21)/18 + 
    tcr11*((f155*tci11^2)/3 - f155*tci12*tcr12 + 
      (2*f155*tcr21)/9) + 
    ((-12*f144 - 12*f151 + 9*f153 - 9*f154 - 26*f155)*tcr33)/
     36;
L ieu1ueu8 = (-8*f146)/9 - (2*f151*fvu1u1u2^2)/27 + 
    ((-3*f151 - 17*f155)*fvu1u1u4^2)/27 + 
    (8*(f145 - f146 - f147 + f148 - f149)*fvu1u1u6^2)/9 + 
    ((-f151 - 2*f154 - f155)*fvu1u1u9^2)/6 + 
    fvu1u1u4*(-f147/3 + (2*f151*fvu1u1u6)/3 + 
      (4*f155*fvu1u1u9)/9 - (2*f155*fvu1u1u10)/3) - 
    (2*f155*fvu2u1u3)/9 + (7*f155*fvu2u1u6)/18 - 
    (4*f155*fvu2u1u11)/9 + (16*f151*fvu2u1u14)/9 + 
    ((-2*f151 - f155)*fvu2u1u15)/18 + 
    ((f144 + f151 - f153 + 4*f155)*tci11^2)/18 + 
    ((-f144 + f151)*tci12)/6 - (f155*fvu1u2u3*tci12)/9 + 
    (11*f155*fvu1u2u6*tci12)/18 + 
    fvu1u1u10*((8*f148)/9 + (f144*tci12)/3) + 
    fvu1u1u6*(-f149/3 + ((14*f151 - 3*f154)*fvu1u1u9)/18 + 
      (f155*fvu1u1u10)/2 + ((-6*f52 + f54 + 6*f154)*tci12)/9) + 
    fvu1u1u9*(f145/3 - (14*f155*fvu1u1u10)/9 - 
      (2*f155*tci12)/3) + fvu1u1u2*
     ((-8*(f145 - f146 - f147 + f148 - f149))/9 - 
      (2*f155*fvu1u1u4)/3 + (4*f151*fvu1u1u6)/9 - 
      (4*f155*fvu1u1u9)/9 + (f155*fvu1u1u10)/6 + 
      (2*(2*f151 + f155)*tci12)/9);
L ieu0ueu8 = -f146/3;
L ieum1ueu8 = 0;
L ieum2ueu8 = 0;
L ieu2uou8 = (2*f150*fvu4u25)/3 + (2*f150*fvu4u28)/3 + 
    (2*f150*fvu4u39)/3 + (2*f150*fvu4u41)/3 - (2*f150*fvu4u51)/9 + 
    (17*f150*fvu4u80)/12 - (f150*fvu4u83)/12 - (3*f150*fvu4u91)/4 + 
    (17*f150*fvu4u93)/12 - (5*f150*fvu4u102)/18 + 
    (f150*fvu4u111)/4 + (f150*fvu4u113)/2 + f150*fvu4u114 - 
    (2*f150*fvu4u129)/3 + (13*f150*fvu4u132)/3 - 
    (25*f150*fvu4u139)/6 - (2*f150*fvu4u141)/3 + 
    (4*f150*fvu4u146)/9 + (13*f150*fvu4u148)/18 - 
    (7*f150*fvu4u171)/12 + (23*f150*fvu4u174)/12 - 
    (25*f150*fvu4u182)/12 - (7*f150*fvu4u184)/12 + 
    (4*f150*fvu4u190)/9 - (f150*fvu4u192)/18 + (f150*fvu4u199)/4 + 
    (f150*fvu4u201)/2 + f150*fvu4u202 + (7*f150*fvu4u213)/12 - 
    (17*f150*fvu4u215)/4 + (17*f150*fvu4u219)/4 + 
    (7*f150*fvu4u221)/12 - (4*f150*fvu4u225)/9 - (f150*fvu4u233)/4 - 
    (f150*fvu4u234)/2 - f150*fvu4u235 + (5*f150*fvu4u244)/3 - 
    (2*f150*fvu4u246)/3 - (3*f150*fvu4u252)/2 - 
    (5*f150*fvu4u255)/18 - (17*f150*fvu4u271)/12 - 
    (19*f150*fvu4u273)/12 + (19*f150*fvu4u277)/12 - 
    (17*f150*fvu4u279)/12 - (f150*fvu4u289)/4 - (f150*fvu4u290)/2 - 
    f150*fvu4u291 - (2*f150*fvu4u293)/3 - (2*f150*fvu4u295)/3 - 
    2*f150*fvu4u309 + fvu3u45*((f150*fvu1u1u3)/3 + 
      (f150*fvu1u1u6)/3 - (f150*fvu1u1u7)/3) + 
    fvu3u70*((2*f150*fvu1u1u2)/3 - (2*f150*fvu1u1u3)/3 - 
      (2*f150*fvu1u1u9)/3) + fvu3u78*((2*f150*fvu1u1u2)/3 - 
      (2*f150*fvu1u1u3)/3 - (2*f150*fvu1u1u9)/3) + 
    fvu3u63*(-(f150*fvu1u1u2)/3 + (5*f150*fvu1u1u3)/2 + 
      (2*f150*fvu1u1u5)/3 + (5*f150*fvu1u1u6)/2 + 
      (2*f150*fvu1u1u7)/3 - (f150*fvu1u1u8)/3 - 
      (f150*fvu1u1u9)/3 - (19*f150*fvu1u1u10)/6) + 
    fvu3u25*((2*f150*fvu1u1u2)/3 + (f150*fvu1u1u3)/3 - 
      (2*f150*fvu1u1u5)/3 + (f150*fvu1u1u6)/3 + 
      (f150*fvu1u1u7)/3 - (2*f150*fvu1u1u10)/3) + 
    fvu3u43*((f150*fvu1u1u2)/3 - (f150*fvu1u1u4)/3 + 
      (f150*fvu1u1u6)/3 - (f150*fvu1u1u7)/3 - 
      (f150*fvu1u1u10)/3) + fvu3u23*((f150*fvu1u1u1)/3 - 
      (f150*fvu1u1u2)/3 + (f150*fvu1u1u7)/3 - (f150*fvu1u1u8)/3 + 
      (f150*fvu1u1u9)/3 - (f150*fvu1u1u10)/3) + 
    fvu3u62*(-(f150*fvu1u1u2)/3 - (f150*fvu1u1u4)/3 + 
      (2*f150*fvu1u1u5)/3 - (f150*fvu1u1u6)/3 - 
      (f150*fvu1u1u7)/3 + (f150*fvu1u1u10)/3) + 
    fvu3u81*((f150*fvu1u1u4)/3 - (f150*fvu1u1u6)/3 + 
      (f150*fvu1u1u7)/3 - (f150*fvu1u1u9)/3 + 
      (f150*fvu1u1u10)/3) - (151*f150*tci11^3*tci12)/135 + 
    fvu3u71*(-(f150*fvu1u1u2)/3 + (f150*fvu1u1u3)/3 + 
      2*f150*fvu1u1u4 - (2*f150*fvu1u1u5)/3 - (f150*fvu1u1u6)/3 - 
      (2*f150*fvu1u1u7)/3 + (f150*fvu1u1u8)/3 + f150*fvu1u1u9 + 
      f150*fvu1u1u10 - 2*f150*tci12) + 
    fvu3u82*((f150*fvu1u1u3)/3 + (f150*fvu1u1u4)/3 - 
      (f150*fvu1u1u7)/3 + (f150*fvu1u1u8)/3 - (f150*fvu1u1u9)/3 - 
      (2*f150*tci12)/3) + fvu3u80*((-7*f150*fvu1u1u6)/6 + 
      (7*f150*fvu1u1u7)/6 - (5*f150*fvu1u1u8)/6 + 
      (7*f150*fvu1u1u9)/6 + (5*f150*tci12)/6) - 
    (2*f150*tci11^2*tci21)/9 + 
    fvu1u1u8*((-5*f150*tci11^3)/81 + (5*f150*tci12*tci21)/
       3) + fvu1u1u4*((-4*f150*tci11^3)/27 + 
      4*f150*tci12*tci21) + tci12*((-48*f150*tci31)/5 - 
      16*f150*tci32) - (4*f150*tci41)/3 + 
    ((-23*f150*tci11^3)/324 + (7*f150*tci12*tci21)/3 + 
      12*f150*tci31)*tcr11 + ((f150*tci11*tci12)/5 + 
      5*f150*tci21)*tcr11^2 - (f150*tci11*tcr11^3)/4 + 
    fvu1u1u6*((149*f150*tci11^3)/1620 + 
      (28*f150*tci12*tci21)/9 + (76*f150*tci31)/5 + 
      (19*f150*tci21*tcr11)/3 - (19*f150*tci11*tcr11^2)/
       60) + fvu1u1u3*(-(f150*fvu3u81)/3 + (41*f150*tci11^3)/324 + 
      f150*tci12*tci21 + 12*f150*tci31 + 
      5*f150*tci21*tcr11 - (f150*tci11*tcr11^2)/4) + 
    fvu1u1u2*(-(f150*fvu3u80)/3 + (22*f150*tci11^3)/405 + 
      (8*f150*tci12*tci21)/9 + (32*f150*tci31)/5 + 
      (8*f150*tci21*tcr11)/3 - (2*f150*tci11*tcr11^2)/
       15) + fvu1u1u7*((103*f150*tci11^3)/810 - 
      (5*f150*tci12*tci21)/3 + (24*f150*tci31)/5 + 
      2*f150*tci21*tcr11 - (f150*tci11*tcr11^2)/10) + 
    fvu1u1u5*((f150*tci11^3)/45 - (16*f150*tci12*tci21)/9 - 
      (16*f150*tci31)/5 - (4*f150*tci21*tcr11)/3 + 
      (f150*tci11*tcr11^2)/15) + 
    fvu1u1u9*((-2*f150*tci11^3)/135 - (7*f150*tci12*tci21)/
       9 - (16*f150*tci31)/5 - (4*f150*tci21*tcr11)/3 + 
      (f150*tci11*tcr11^2)/15) + 
    fvu1u1u1*(-(f150*fvu3u25)/3 + (f150*fvu3u43)/3 - 
      (f150*fvu3u45)/3 + (f150*fvu3u62)/3 - (13*f150*fvu3u63)/6 + 
      (2*f150*fvu3u70)/3 - (2*f150*fvu3u71)/3 + (2*f150*fvu3u78)/3 - 
      (f150*fvu3u82)/3 - (25*f150*tci11^3)/324 - 
      (7*f150*tci12*tci21)/3 - 12*f150*tci31 - 
      5*f150*tci21*tcr11 + (f150*tci11*tcr11^2)/4) + 
    fvu1u1u10*((-29*f150*tci11^3)/108 - (f150*tci12*tci21)/
       9 - 20*f150*tci31 - (25*f150*tci21*tcr11)/3 + 
      (5*f150*tci11*tcr11^2)/12) + 
    (40*f150*tci12*tci21*tcr12)/3 + 
    4*f150*tci11*tci12*tcr12^2;
L ieu1uou8 = 2*f150*fvu3u71 - (4*f150*tci11^3)/27 + 
    4*f150*tci12*tci21;
L ieu0uou8 = 0;
L ieum1uou8 = 0;
L ieum2uou8 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou8+w^2*ieu1uou8+w^3*ieu0uou8+w^4*ieum1uou8+w^5*ieum2uou8;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou8a = K[w^1];
L ieu1uou8a = K[w^2];
L ieu0uou8a = K[w^3];
L ieum1uou8a = K[w^4];
L ieum2uou8a = K[w^5];
.sort
#write <e8.tmp> "`optimmaxvar_'"
#write <e8_odd.c> "%O"
#write <e8_odd.c> "return Eps5o2<T>("
#write <e8_odd.c> "%E", ieu2uou8a
#write <e8_odd.c> ", "
#write <e8_odd.c> "%E", ieu1uou8a
#write <e8_odd.c> ", "
#write <e8_odd.c> "%E", ieu0uou8a
#write <e8_odd.c> ", "
#write <e8_odd.c> "%E", ieum1uou8a
#write <e8_odd.c> ", "
#write <e8_odd.c> "%E", ieum2uou8a
#write <e8_odd.c> ");\n}"
L H=+u^1*ieu2ueu8+u^2*ieu1ueu8+u^3*ieu0ueu8+u^4*ieum1ueu8+u^5*ieum2ueu8;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu8a = H[u^1];
L ieu1ueu8a = H[u^2];
L ieu0ueu8a = H[u^3];
L ieum1ueu8a = H[u^4];
L ieum2ueu8a = H[u^5];
.sort
#write <e8.tmp> "`optimmaxvar_'"
#write <e8_even.c> "%O"
#write <e8_even.c> "return Eps5o2<T>("
#write <e8_even.c> "%E", ieu2ueu8a
#write <e8_even.c> ", "
#write <e8_even.c> "%E", ieu1ueu8a
#write <e8_even.c> ", "
#write <e8_even.c> "%E", ieu0ueu8a
#write <e8_even.c> ", "
#write <e8_even.c> "%E", ieum1ueu8a
#write <e8_even.c> ", "
#write <e8_even.c> "%E", ieum2ueu8a
#write <e8_even.c> ");\n}"
.end
