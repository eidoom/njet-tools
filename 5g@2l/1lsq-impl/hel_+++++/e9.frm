#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f137;
S f136;
S f135;
S f133;
S f61;
S f130;
S f139;
S f138;
S f68;
S f78;
S f129;
S f146;
S f3;
S f80;
S f142;
S f143;
S f140;
S f141;
S f23;
S f24;
S f99;
S f118;
S f117;
S f35;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu4u233;
S fvu4u141;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u139;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu2u1u8;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu3u51;
S fvu4u91;
S fvu3u53;
S fvu4u93;
S fvu3u52;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u325;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu9 = (-13*(3*f3 - 3*f35 + f61 + 3*f68 + f99 - 
       5*f146))/27 - (8*f138*fvu3u1)/9 - (f138*fvu3u2)/9 + 
    (5*f138*fvu3u3)/36 + (f138*fvu3u4)/36 - (f138*fvu3u5)/12 + 
    (5*f138*fvu3u6)/12 + (2*f138*fvu3u7)/9 + (f138*fvu3u8)/9 + 
    ((-f138 - 2*f140)*fvu3u11)/9 + ((-f138 + f140)*fvu3u12)/18 - 
    (f138*fvu3u13)/6 + (f138*fvu3u17)/3 + (5*f138*fvu3u18)/6 + 
    (f138*fvu3u19)/18 - (5*f138*fvu3u24)/18 + (2*f140*fvu3u31)/9 + 
    ((-f129 - f140)*fvu3u32)/6 - (f140*fvu3u33)/6 - 
    (f140*fvu3u34)/6 - (f140*fvu3u36)/9 + (f140*fvu3u37)/9 + 
    ((6*f23 - f24 - 6*f78 + f80 - 6*f142 - 6*f143)*fvu3u38)/9 + 
    (2*f138*fvu3u40)/3 + (2*f140*fvu3u44)/9 - (f138*fvu3u51)/6 - 
    (2*f140*fvu3u52)/9 - (4*f140*fvu3u53)/9 - (5*f138*fvu3u56)/18 - 
    (f138*fvu3u57)/18 + (2*f140*fvu3u59)/9 - (f138*fvu3u61)/3 - 
    (35*f138*fvu1u1u1^3)/18 - (f138*fvu1u1u2^3)/9 + 
    (35*f140*fvu1u1u3^3)/27 - (2*f140*fvu1u1u4^3)/9 + 
    ((3*f3 - 3*f35 + f61 + 3*f68 + f99 + 4*f130 + 4*f133 - 
       4*f135 - 4*f136 - 5*f146)*fvu1u1u5^3)/12 + 
    (2*(f138 + f140)*fvu1u1u6^3)/9 + (f135*fvu1u1u7^3)/3 + 
    (f138*fvu1u1u8^3)/3 - (f138*fvu1u1u9^3)/3 + 
    (8*f138*fvu2u1u5)/27 - (11*f138*fvu2u1u7)/36 + 
    (f142*fvu2u1u14)/3 + (8*f140*fvu1u2u3*tci11^2)/9 + 
    ((f138 + f142)*fvu1u2u6*tci11^2)/3 + 
    ((f129 - f140)*fvu1u2u8*tci11^2)/3 + 
    (2*f138*fvu1u2u9*tci11^2)/9 + ((3*f138 + 2*f140)*tci12)/
     36 - (10*f138*fvu1u2u7*tci12)/9 + (f138*fvu2u1u1*tci12)/
     3 + (2*(f138 - f140)*fvu2u1u2*tci12)/9 + 
    ((f129 + f138)*fvu2u1u3*tci12)/3 + 
    ((-6*f117 + f118 + 6*f129 - f141)*fvu2u1u6*tci12)/9 + 
    ((-3*f129 - 2*f138 - 8*f140 - 3*f142)*fvu2u1u9*tci12)/18 - 
    (f142*fvu2u1u10*tci12)/3 + (41*f138*fvu2u1u11*tci12)/18 - 
    (2*f138*fvu2u1u12*tci12)/3 + fvu1u1u8^2*
     ((5*f138*fvu1u1u9)/9 - (2*f138*fvu1u1u10)/9 - 
      (f138*tci12)/3) + fvu1u1u5^2*(-f136/6 - 
      (4*f140*fvu1u1u6)/9 + (4*f140*fvu1u1u7)/9 - 
      (f140*fvu1u1u8)/9 + (f138*fvu1u1u9)/3 + 
      (f139*fvu1u1u10)/9 - (f138*tci12)/9) + 
    fvu1u1u2^2*((f138*fvu1u1u4)/9 + (2*f138*fvu1u1u5)/9 - 
      (f138*fvu1u1u8)/9 + (f138*fvu1u1u9)/9 + (f138*tci12)/9) + 
    fvu1u2u2*((-10*f138*tci11^2)/3 + (f138*tci12)/3) + 
    fvu1u1u1^2*((2*f138*fvu1u1u2)/9 - (f138*fvu1u1u4)/9 - 
      (f138*fvu1u1u5)/9 + (2*f140*fvu1u1u6)/9 - 
      (2*f140*fvu1u1u7)/9 - (f138*fvu1u1u8)/9 + 
      (f138*fvu1u1u9)/9 - (5*f138*fvu1u1u10)/6 + 
      (5*f138*tci12)/3) + fvu2u1u13*((-25*f138)/9 + 
      (56*f138*tci12)/9) + fvu1u1u9^2*
     ((-2*(f138 - f140)*fvu1u1u10)/9 + 
      ((-11*f138 - 4*f140)*tci12)/54) + 
    fvu1u1u6^2*((6*f23 - f24 - 6*f78 + f80 + 3*f130 - 6*f142 - 
        6*f143)/18 + (f141*fvu1u1u7)/9 - (5*f140*fvu1u1u8)/9 + 
      (10*f140*fvu1u1u9)/9 - (f140*tci12)/6) + 
    fvu1u1u4^2*(-(f138*fvu1u1u5)/9 - (2*f140*fvu1u1u6)/9 - 
      (4*f140*fvu1u1u7)/9 - (5*f138*fvu1u1u8)/18 - 
      (7*f138*fvu1u1u9)/36 + (5*f140*fvu1u1u10)/18 + 
      (2*f140*tci12)/9) + fvu1u1u7^2*
     (f135/3 + (f140*fvu1u1u8)/3 - (f140*fvu1u1u10)/3 - 
      (f141*tci12)/9) + fvu2u1u8*(-f138/6 - (f142*tci12)/6) + 
    tci11^2*((-9*f3 - 48*f23 + 8*f24 + 9*f35 - 3*f61 - 
        9*f68 + 48*f78 - 8*f80 - 3*f99 - 48*f117 + 8*f118 + 
        48*f129 - 72*f135 + 40*f139 - 8*f141 + 48*f142 + 
        48*f143 + 15*f146)/432 + ((f129 + 2*f138 + f142)*tci12)/
       6) + fvu1u1u3^2*(-f140 - (f138*fvu1u1u4)/3 + 
      (5*f140*fvu1u1u6)/18 - (f139*fvu1u1u8)/9 + 
      (5*f138*fvu1u1u9)/12 - (5*f138*fvu1u1u10)/6 + 
      ((f129 - 2*f140 - f143)*tci12)/6) + 
    fvu1u1u10^2*((-8*f135)/9 + ((-f140 - f143)*tci12)/3) + 
    fvu1u1u10*((8*f140)/27 + (f138*fvu2u1u13)/3 + 
      ((-6*f78 + f80 - f141 - 6*f143)*fvu2u1u14)/9 + 
      ((-f138 - f142)*fvu2u1u15)/3 + (4*f140*tci11^2)/27 - 
      (f138*tci12)/3 + ((6*f117 - f118 - 6*f129 - f139)*
        fvu1u2u2*tci12)/9) + fvu1u1u6*((-8*f130)/9 + 
      (f140*fvu1u1u7)/3 - (f140*fvu1u1u7^2)/3 + 
      (f141*fvu1u1u8^2)/9 - (2*f140*fvu1u1u9^2)/3 + 
      (f140*fvu1u1u10)/6 + (2*f140*fvu1u1u10^2)/3 + 
      (2*f140*fvu2u1u6)/3 - (4*f140*fvu2u1u8)/9 + 
      (f140*fvu2u1u14)/6 + ((-5*f138 - 8*f140)*tci11^2)/54 + 
      ((-f142 - f143)*tci12)/3 + (2*f140*fvu1u1u8*tci12)/3 + 
      (20*f140*fvu1u1u9*tci12)/9 + (f140*fvu1u2u3*tci12)/6) + 
    fvu1u1u9*((2*(f138 - f140)*fvu1u1u10^2)/9 + 
      (f138*fvu2u1u11)/3 + (f139*fvu2u1u12)/9 - 
      (2*(f138 - f140)*fvu2u1u15)/9 + ((-f129 - f138)*tci11^2)/
       3 - (2*(f138 - f140)*fvu1u1u10*tci12)/9 - 
      (2*(f138 - f140)*fvu1u2u9*tci12)/9) + 
    fvu1u1u8*(((77*f138 + 8*f140)*fvu1u1u9^2)/108 + 
      (2*f140*fvu2u1u8)/3 + (f138*fvu2u1u9)/6 - 
      (4*f140*fvu2u1u10)/9 + ((f138 + 2*f140)*fvu2u1u11)/9 + 
      (f138*fvu2u1u12)/3 - (7*f138*tci11^2)/9 + 
      ((-5*f138 + 3*f140)*fvu1u1u9*tci12)/27 + 
      ((f138 + 2*f140)*fvu1u1u10*tci12)/18 + 
      (8*f140*fvu1u2u8*tci12)/9 + ((5*f138 + 2*f140)*fvu1u2u9*
        tci12)/36) + fvu1u1u2*((f138*fvu1u1u4^2)/18 - 
      (f138*fvu1u1u5^2)/3 - (f138*fvu1u1u8^2)/9 + 
      (5*f138*fvu1u1u9^2)/12 + ((f138 + 2*f140)*fvu2u1u3)/18 + 
      (5*f138*fvu2u1u4)/12 - (5*f138*fvu2u1u11)/6 - 
      (5*f138*tci11^2)/6 - (f138*fvu1u1u5*tci12)/3 + 
      (5*f138*fvu1u1u9*tci12)/12 + ((f138 - f140)*fvu1u2u6*
        tci12)/18 + fvu1u1u4*((f138*fvu1u1u5)/9 - 
        (f138*fvu1u1u8)/9 + (5*f138*fvu1u1u9)/18 - 
        (2*f138*tci12)/9) + fvu1u1u8*((-5*f138*fvu1u1u9)/9 + 
        (f138*tci12)/27)) + fvu1u1u7*(-f135/3 - 
      (8*f140*fvu1u1u8^2)/9 + (16*f140*fvu1u1u10^2)/9 - 
      (f140*fvu2u1u7)/6 + (2*f140*fvu2u1u8)/3 - 
      (2*f140*fvu2u1u10)/3 + (8*f135*tci11^2)/9 + 
      ((-6*f117 + f118 + 6*f129)*tci12)/9 + 
      (4*f140*fvu1u1u8*tci12)/9 - (8*f140*fvu1u2u8*tci12)/9 + 
      fvu1u1u10*((4*f140)/9 - (8*f140*tci12)/9)) + 
    fvu1u1u4*((7*f138*fvu1u1u5^2)/18 - (f140*fvu1u1u6^2)/3 + 
      (2*f140*fvu1u1u7^2)/3 - (f138*fvu1u1u8^2)/3 - 
      (f138*fvu1u1u9^2)/12 + (2*f140*fvu1u1u10^2)/9 - 
      (f138*fvu2u1u2)/12 - (f138*fvu2u1u3)/6 + 
      (2*f140*fvu2u1u6)/3 - (2*f140*fvu2u1u8)/3 - 
      (f138*fvu2u1u11)/6 - (f138*tci11^2)/3 - 
      (f138*fvu1u1u9*tci12)/6 - (f140*fvu1u1u10*tci12)/9 - 
      (f140*fvu1u2u3*tci12)/3 + ((-5*f138 + 4*f140)*fvu1u2u6*
        tci12)/36 + fvu1u1u5*(-(f138*fvu1u1u9)/3 + 
        (5*f138*tci12)/9) + fvu1u1u8*((-2*f138*fvu1u1u9)/9 + 
        ((4*f138 - 9*f140)*tci12)/54) + 
      fvu1u1u6*((-5*f140*fvu1u1u7)/9 + (f140*fvu1u1u8)/6 - 
        (f140*tci12)/3) + fvu1u1u7*((-2*f140*fvu1u1u8)/3 + 
        (4*f140*fvu1u1u10)/9 - (f140*tci12)/3)) + 
    fvu1u1u5*((8*f136)/9 + (2*f140*fvu1u1u6^2)/9 + 
      (f140*fvu1u1u8^2)/2 + (f138*fvu1u1u9^2)/3 + 
      (f138*fvu1u1u10^2)/18 + (f138*fvu2u1u4)/9 + 
      (f140*fvu2u1u7)/3 + ((-4*f138 + 7*f140)*tci11^2)/27 - 
      (f138*tci12)/3 - (8*f140*fvu1u1u8*tci12)/9 + 
      (f139*fvu1u1u9*tci12)/9 + fvu1u1u10*
       (-f138/2 - (f138*tci12)/18) + 
      fvu1u1u6*(-f138/9 + (4*f140*fvu1u1u8)/9 - 
        (4*f140*tci12)/9) + fvu1u1u7*((6*f117 - f118 - 6*f129)/
         9 - (f140*fvu1u1u8)/3 - (f140*tci12)/6)) + 
    fvu1u1u1*((-5*f138*fvu1u1u2^2)/9 + (f138*fvu1u1u3^2)/3 - 
      (5*f138*fvu1u1u4^2)/9 - (f138*fvu1u1u5^2)/9 + 
      (2*f140*fvu1u1u6^2)/9 + (f140*fvu1u1u7^2)/3 + 
      (5*f138*fvu1u1u8^2)/3 + (2*f138*fvu1u1u9^2)/9 - 
      (11*f138*fvu1u1u10^2)/54 + (f138*fvu2u1u1)/18 - 
      (f138*fvu2u1u2)/18 - (f140*fvu2u1u8)/9 - 
      (f138*fvu2u1u9)/9 + (2*f138*fvu2u1u11)/9 + 
      (2*f138*tci11^2)/9 + (f138*fvu1u1u3*tci12)/3 - 
      (2*f138*fvu1u1u9*tci12)/27 - (f138*fvu1u1u10*tci12)/
       18 + fvu1u1u4*((2*f140*fvu1u1u6)/9 - (2*f140*fvu1u1u7)/9 + 
        (f138*fvu1u1u8)/9 + (10*f138*fvu1u1u9)/9 - 
        (f138*tci12)/9) + fvu1u1u2*((5*f138*fvu1u1u4)/9 + 
        (f138*fvu1u1u8)/9 + (10*f138*fvu1u1u9)/9 + 
        (f138*tci12)/6) + fvu1u1u8*((5*f138*fvu1u1u9)/9 + 
        (5*f138*tci12)/9) + fvu1u1u5*(-(f138*fvu1u1u10)/6 + 
        (2*f138*tci12)/3) + fvu1u1u7*((f140*fvu1u1u8)/3 - 
        (f140*tci12)/3) + fvu1u1u6*((-4*f140*fvu1u1u7)/9 - 
        (f140*fvu1u1u8)/6 - (f140*tci12)/6)) + 
    fvu1u1u3*((f138 - f140)/9 + ((f138 - f140)*fvu1u1u4^2)/18 - 
      (f138*fvu1u1u5)/3 + (f140*fvu1u1u6^2)/6 + 
      ((6*f78 - f80 + 6*f143)*fvu1u1u7)/9 - (f138*fvu1u1u8^2)/3 + 
      (2*(f138 - f140)*fvu1u1u9^2)/9 - (f138*fvu1u1u10^2)/36 + 
      (f138*fvu2u1u1)/18 + (5*f138*fvu2u1u13)/36 - 
      (f133*tci11^2)/3 - (8*f133*tci12)/9 - 
      (f139*fvu1u1u8*tci12)/9 + (f138*fvu1u2u2*tci12)/6 + 
      fvu1u1u10*(-f138/36 + (5*f138*tci12)/36) + 
      fvu1u1u9*((5*f138*fvu1u1u10)/3 - (2*(f138 - f140)*tci12)/
         9) + fvu1u1u6*(f142/3 + (f140*fvu1u1u9)/3 + 
        (4*f140*tci12)/9) + fvu1u1u4*
       (((-f138 - 2*f140)*fvu1u1u8)/9 + ((-f138 + f140)*tci12)/
         9)) - (f140*tci12*tcr11^2)/2 + (f140*tcr11^3)/9 - 
    (7*f140*tci11^2*tcr12)/9 - (2*f140*tci12*tcr21)/9 + 
    tcr11*((-4*f140*tci11^2)/9 - (16*f140*tci12*tcr12)/
       9 + ((-2*f140 + f143)*tcr21)/6) + 
    ((-9*f129 - 26*f138 + 12*f140 - 9*f142 - 12*f143)*tcr33)/
     36;
L ieu1ueu9 = 
   (-2*(3*f3 - 3*f35 + f61 + 3*f68 + f99 - 5*f146))/9 + 
    ((-6*f78 + f80 - 6*f117 + f118 + 6*f129 + 3*f133 - 2*f141 - 
       6*f143)*fvu1u1u3^2)/18 + 
    (2*(3*f3 - 3*f35 + f61 + 3*f68 + f99 + 4*f130 + 4*f133 - 
       4*f135 - 4*f136 - 5*f146)*fvu1u1u5^2)/9 + 
    ((-f142 - f143)*fvu1u1u6^2)/6 + 
    fvu1u1u3*((-f138 + f140)/9 + (f138*fvu1u1u5)/6 + 
      ((-6*f23 + f24 + 6*f142)*fvu1u1u6)/9 + (f143*fvu1u1u7)/3 - 
      (7*f138*fvu1u1u10)/36) + ((-6*f117 + f118 + 6*f129 + f139)*
      fvu2u1u5)/9 + (5*f138*fvu2u1u7)/12 + (5*f138*fvu2u1u8)/9 - 
    (5*f138*fvu2u1u13)/6 + ((6*f23 - f24 - 6*f142)*fvu2u1u14)/9 + 
    ((f129 + 5*f138 - f140 + f142 + f143)*tci11^2)/18 + 
    ((-6*f23 + f24 - 6*f117 + f118 + 6*f129 - 3*f135 + 2*f139 + 
       6*f142)*tci12)/18 - (10*f138*fvu1u2u2*tci12)/9 + 
    ((-6*f23 + f24 + f139 + 6*f142)*fvu1u2u7*tci12)/9 + 
    fvu1u1u7*((3*f129 + 20*f138 + 12*f140 + 3*f142)/108 - 
      (8*f140*fvu1u1u10)/9 + (f129*tci12)/3) + 
    fvu1u1u5*(f136/3 - (f138*fvu1u1u6)/6 - (f129*fvu1u1u7)/3 + 
      (f138*fvu1u1u10)/6 - (2*f138*tci12)/9) + 
    fvu1u1u10*((-3*f3 + 3*f35 - f61 - 3*f68 - f99 - 4*f130 - 
        4*f133 + 4*f135 + 4*f136 + 5*f146)/24 + 
      ((6*f23 - f24 - f139 - 6*f142)*tci12)/9) + 
    fvu1u1u6*(-f130/3 - (f140*fvu1u1u7)/3 + (2*f140*fvu1u1u10)/
       9 + ((-6*f23 + f24 + 6*f142)*tci12)/9);
L ieu0ueu9 = (-3*f3 + 3*f35 - f61 - 3*f68 - f99 + 
     5*f146)/12;
L ieum1ueu9 = 0;
L ieum2ueu9 = 0;
L ieu2uou9 = (4*f137*fvu4u28)/3 + (4*f137*fvu4u39)/3 - 
    (4*f137*fvu4u49)/9 + (4*f137*fvu4u51)/9 + (7*f137*fvu4u80)/12 + 
    (25*f137*fvu4u83)/12 + (3*f137*fvu4u91)/4 + 
    (7*f137*fvu4u93)/12 - (2*f137*fvu4u100)/9 + 
    (7*f137*fvu4u102)/6 - (f137*fvu4u111)/4 - (f137*fvu4u113)/2 - 
    f137*fvu4u114 + (4*f137*fvu4u129)/3 - (11*f137*fvu4u132)/3 + 
    (7*f137*fvu4u139)/2 + (4*f137*fvu4u141)/3 - 
    (2*f137*fvu4u146)/9 - (f137*fvu4u148)/2 - (4*f137*fvu4u174)/3 - 
    (4*f137*fvu4u182)/3 + (4*f137*fvu4u190)/9 - 
    (4*f137*fvu4u192)/9 + (41*f137*fvu4u213)/12 + 
    (35*f137*fvu4u215)/12 - (17*f137*fvu4u219)/4 - 
    (7*f137*fvu4u221)/12 - (4*f137*fvu4u225)/9 + (f137*fvu4u233)/4 + 
    (f137*fvu4u234)/2 + f137*fvu4u235 - (4*f137*fvu4u244)/3 - 
    (4*f137*fvu4u246)/3 + (2*f137*fvu4u252)/3 + 
    (2*f137*fvu4u255)/9 - 2*f137*fvu4u277 - 2*f137*fvu4u325 + 
    fvu3u71*((-2*f137*fvu1u1u3)/3 + (2*f137*fvu1u1u6)/3 - 
      (2*f137*fvu1u1u7)/3) + fvu3u25*((2*f137*fvu1u1u3)/3 - 
      (2*f137*fvu1u1u6)/3 + (2*f137*fvu1u1u7)/3) + 
    fvu3u62*((2*f137*fvu1u1u2)/3 - (f137*fvu1u1u4)/3 - 
      (f137*fvu1u1u5)/3 + (2*f137*fvu1u1u6)/3 - 
      (f137*fvu1u1u7)/3 - (2*f137*fvu1u1u10)/3) + 
    fvu3u70*((2*f137*fvu1u1u6)/3 - (2*f137*fvu1u1u8)/3 - 
      (2*f137*fvu1u1u10)/3) + fvu3u43*((2*f137*fvu1u1u2)/3 - 
      (f137*fvu1u1u4)/3 + (2*f137*fvu1u1u6)/3 - 
      (2*f137*fvu1u1u7)/3 - (f137*fvu1u1u9)/3 - 
      (f137*fvu1u1u10)/3) + fvu3u45*((2*f137*fvu1u1u3)/3 + 
      (f137*fvu1u1u5)/3 + (2*f137*fvu1u1u6)/3 - f137*fvu1u1u7 - 
      (f137*fvu1u1u9)/3 + (f137*fvu1u1u10)/3) + 
    fvu3u23*((-2*f137*fvu1u1u1)/3 - (2*f137*fvu1u1u6)/3 + 
      (2*f137*fvu1u1u8)/3 + (2*f137*fvu1u1u10)/3) + 
    fvu3u63*((-5*f137*fvu1u1u3)/2 - (f137*fvu1u1u5)/3 - 
      (11*f137*fvu1u1u6)/6 - (2*f137*fvu1u1u7)/3 + 
      (2*f137*fvu1u1u8)/3 + (f137*fvu1u1u9)/3 + 
      (5*f137*fvu1u1u10)/2) - (151*f137*tci11^3*tci12)/135 + 
    fvu3u80*((-2*f137*fvu1u1u2)/3 - (2*f137*fvu1u1u3)/3 + 
      (f137*fvu1u1u4)/3 + (f137*fvu1u1u5)/3 + 
      (2*f137*fvu1u1u6)/3 - f137*fvu1u1u7 + (2*f137*fvu1u1u8)/3 - 
      (2*f137*tci12)/3) + fvu3u78*((-2*f137*fvu1u1u2)/3 - 
      (f137*fvu1u1u3)/3 + (f137*fvu1u1u4)/3 + 2*f137*fvu1u1u5 - 
      (f137*fvu1u1u6)/3 + f137*fvu1u1u7 - (2*f137*fvu1u1u8)/3 + 
      (f137*fvu1u1u9)/3 + f137*fvu1u1u10 + (4*f137*tci12)/3) - 
    (2*f137*tci11^2*tci21)/9 + 
    fvu1u1u8*((4*f137*tci11^3)/81 - (4*f137*tci12*tci21)/
       3) + tci12*((-48*f137*tci31)/5 - 16*f137*tci32) - 
    (4*f137*tci41)/3 + ((-35*f137*tci11^3)/108 + 
      (f137*tci12*tci21)/3 - 12*f137*tci31)*tcr11 + 
    ((f137*tci11*tci12)/5 - 5*f137*tci21)*tcr11^2 + 
    (f137*tci11*tcr11^3)/4 + fvu1u1u1*((-2*f137*fvu3u25)/3 + 
      (f137*fvu3u43)/3 - (2*f137*fvu3u45)/3 + (f137*fvu3u62)/3 + 
      (11*f137*fvu3u63)/6 + (2*f137*fvu3u70)/3 + 
      (2*f137*fvu3u71)/3 - (2*f137*fvu3u78)/3 + (f137*fvu3u80)/3 + 
      (19*f137*tci11^3)/108 - (f137*tci12*tci21)/3 + 
      12*f137*tci31 + 5*f137*tci21*tcr11 - 
      (f137*tci11*tcr11^2)/4) + 
    fvu1u1u10*((-79*f137*tci11^3)/1620 + 
      (41*f137*tci12*tci21)/9 + (44*f137*tci31)/5 + 
      (11*f137*tci21*tcr11)/3 - (11*f137*tci11*tcr11^2)/
       60) + fvu1u1u7*((-29*f137*tci11^3)/405 + 
      (28*f137*tci12*tci21)/9 + (16*f137*tci31)/5 + 
      (4*f137*tci21*tcr11)/3 - (f137*tci11*tcr11^2)/15) + 
    fvu1u1u9*(-(f137*tci11^3)/90 + (8*f137*tci12*tci21)/9 + 
      (8*f137*tci31)/5 + (2*f137*tci21*tcr11)/3 - 
      (f137*tci11*tcr11^2)/30) + 
    fvu1u1u5*((-37*f137*tci11^3)/270 + 
      (28*f137*tci12*tci21)/9 - (8*f137*tci31)/5 - 
      (2*f137*tci21*tcr11)/3 + (f137*tci11*tcr11^2)/30) + 
    fvu1u1u6*(-(f137*tci11^3)/36 - (11*f137*tci12*tci21)/
       3 - 12*f137*tci31 - 5*f137*tci21*tcr11 + 
      (f137*tci11*tcr11^2)/4) + 
    fvu1u1u3*((-25*f137*tci11^3)/324 - (7*f137*tci12*tci21)/
       3 - 12*f137*tci31 - 5*f137*tci21*tcr11 + 
      (f137*tci11*tcr11^2)/4) + 
    (40*f137*tci12*tci21*tcr12)/3 + 
    4*f137*tci11*tci12*tcr12^2;
L ieu1uou9 = 2*f137*fvu3u78 - (4*f137*tci11^3)/27 + 
    4*f137*tci12*tci21;
L ieu0uou9 = 0;
L ieum1uou9 = 0;
L ieum2uou9 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou9+w^2*ieu1uou9+w^3*ieu0uou9+w^4*ieum1uou9+w^5*ieum2uou9;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou9a = K[w^1];
L ieu1uou9a = K[w^2];
L ieu0uou9a = K[w^3];
L ieum1uou9a = K[w^4];
L ieum2uou9a = K[w^5];
.sort
#write <e9.tmp> "`optimmaxvar_'"
#write <e9_odd.c> "%O"
#write <e9_odd.c> "return Eps5o2<T>("
#write <e9_odd.c> "%E", ieu2uou9a
#write <e9_odd.c> ", "
#write <e9_odd.c> "%E", ieu1uou9a
#write <e9_odd.c> ", "
#write <e9_odd.c> "%E", ieu0uou9a
#write <e9_odd.c> ", "
#write <e9_odd.c> "%E", ieum1uou9a
#write <e9_odd.c> ", "
#write <e9_odd.c> "%E", ieum2uou9a
#write <e9_odd.c> ");\n}"
L H=+u^1*ieu2ueu9+u^2*ieu1ueu9+u^3*ieu0ueu9+u^4*ieum1ueu9+u^5*ieum2ueu9;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu9a = H[u^1];
L ieu1ueu9a = H[u^2];
L ieu0ueu9a = H[u^3];
L ieum1ueu9a = H[u^4];
L ieum2ueu9a = H[u^5];
.sort
#write <e9.tmp> "`optimmaxvar_'"
#write <e9_even.c> "%O"
#write <e9_even.c> "return Eps5o2<T>("
#write <e9_even.c> "%E", ieu2ueu9a
#write <e9_even.c> ", "
#write <e9_even.c> "%E", ieu1ueu9a
#write <e9_even.c> ", "
#write <e9_even.c> "%E", ieu0ueu9a
#write <e9_even.c> ", "
#write <e9_even.c> "%E", ieum1ueu9a
#write <e9_even.c> ", "
#write <e9_even.c> "%E", ieum2ueu9a
#write <e9_even.c> ");\n}"
.end
