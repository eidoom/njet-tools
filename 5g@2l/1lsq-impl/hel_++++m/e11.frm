#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f67;
S f64;
S f61;
S f60;
S f74;
S f75;
S f126;
S f122;
S f78;
S f151;
S f159;
S f89;
S f88;
S f81;
S f16;
S f3;
S f83;
S f85;
S f10;
S f86;
S f95;
S f92;
S f27;
S f177;
S f98;
S f24;
S f160;
S f162;
S f58;
S f59;
S f102;
S f103;
S f50;
S f107;
S f57;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu4u279;
S fvu4u91;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u330;
S fvu4u100;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u325;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu11 = (11*f3 + 74*f16 + 729*f57 + 630*f58 - 
      74*f59 - 63*f60 + 63*f107 + 63*f122 + 11*f126 + 74*f151 - 
      11*f159 + 74*f177)/27 + ((-f98 + 2*f102)*fvu3u1)/3 + 
    ((f98 - 2*f102)*fvu3u2)/6 + (2*f98*fvu3u3)/9 + 
    (f102*fvu3u4)/6 + ((11*f98 + f102)*fvu3u5)/18 + 
    ((-f98 + 2*f102)*fvu3u6)/9 + ((f61 + 2*f98)*fvu3u7)/6 + 
    (16*f98*fvu3u8)/9 - (f98*fvu3u10)/6 + (2*(f98 - f102)*fvu3u11)/
     9 + ((4*f98 + f102)*fvu3u12)/18 + ((-f98 - 3*f102)*fvu3u13)/
     18 + (f98*fvu3u14)/3 - (f102*fvu3u15)/6 + 
    ((f92 + f98 + f102)*fvu3u17)/6 + ((f98 + f102)*fvu3u18)/6 + 
    ((-f98 + 2*f102)*fvu3u19)/9 - (2*f98*fvu3u24)/9 - 
    (f98*fvu3u31)/9 + (f102*fvu3u32)/18 - (2*f102*fvu3u36)/9 + 
    ((-f98 - 3*f102 - 3*f103)*fvu3u37)/18 + 
    ((-f98 - f102)*fvu3u38)/6 + (f102*fvu3u40)/6 + 
    (f98*fvu3u42)/3 + ((9*f98 + 4*f102)*fvu1u1u1^3)/18 + 
    ((11*f98 + 15*f102)*fvu1u1u2^3)/27 + 
    ((f98 + 6*f102)*fvu1u1u3^3)/18 + ((-2*f98 - f102)*fvu1u1u5^3)/
     9 - (f102*fvu1u1u6^3)/9 + ((f98 + f102)*fvu1u1u7^3)/9 + 
    (4*f98*fvu1u1u9^3)/27 + fvu1u1u8^2*
     ((6*f10 - f50 + 6*f61 + 3*f74 + 6*f98 - 6*f160 + f162)/
       18 + (f98*fvu1u1u9)/9) + fvu1u1u4^2*
     (((f98 + 3*f102)*fvu1u1u5)/18 + ((4*f98 + f102)*fvu1u1u8)/
       18 + ((7*f98 + f102)*fvu1u1u9)/18) + 
    ((6*f85 - f88 + f95 - 6*f102)*fvu2u1u4)/9 + 
    ((6*f24 - f27 - 6*f85 + f88 + 6*f102 - 6*f103)*fvu2u1u7)/
     9 + ((6*f10 - f50 + 6*f61 + 6*f98 - 6*f160 + f162)*
      fvu2u1u12)/9 - (2*(3*f98 + f102)*fvu1u2u6*tci11^2)/9 + 
    ((-f98 + 2*f102)*fvu1u2u7*tci11^2)/3 + (f75*tci12)/9 + 
    ((-6*f10 + f50 - 6*f61 - 6*f98 + 6*f160 - f162)*fvu1u2u9*
      tci12)/9 - (4*f102*fvu2u1u1*tci12)/3 - 
    (4*(f98 + f102)*fvu2u1u2*tci12)/9 - 
    (2*(3*f98 + f102)*fvu2u1u3*tci12)/9 + 
    ((f98 - 2*f102)*fvu2u1u5*tci12)/3 - 
    (4*(4*f98 + f102)*fvu2u1u9*tci12)/9 + 
    fvu2u1u11*((6*f10 - f50 + 6*f61 + f95)/9 - 
      (4*f98*tci12)/9) + fvu1u1u1^2*(-(f98*fvu1u1u2)/9 - 
      (f98*fvu1u1u3)/6 + ((-f98 - f102)*fvu1u1u4)/9 - 
      (f98*fvu1u1u7)/3 + ((f98 - 2*f102)*fvu1u1u8)/18 - 
      (f98*fvu1u1u9)/18 + ((-23*f98 - 8*f102)*tci12)/18) + 
    fvu1u1u5^2*((6*f24 - f27 - 3*f64 - 6*f85 + f88 + 6*f102 - 
        6*f103)/18 + (f102*fvu1u1u6)/6 + (f98*fvu1u1u7)/18 + 
      ((f98 + 3*f102)*fvu1u1u8)/18 + ((f98 + f102)*fvu1u1u9)/6 + 
      ((-f98 - 3*f102)*tci12)/9) + 
    fvu1u1u3^2*((f102*fvu1u1u4)/6 + ((f98 - f102)*fvu1u1u7)/6 + 
      ((f98 + f102)*fvu1u1u8)/6 + ((-3*f98 - 2*f102)*tci12)/
       6) + fvu1u1u9^2*(-f83/6 + ((-10*f98 - f102)*tci12)/18) + 
    fvu1u1u6^2*(-(f102*fvu1u1u7)/9 + (f102*fvu1u1u8)/18 - 
      (f102*tci12)/18) + fvu1u1u2^2*
     ((6*f10 - f50 + 6*f61 + 3*f78 + 6*f85 - f88 + 2*f95 - 
        6*f102)/18 - (f102*fvu1u1u3)/6 + ((f98 - f102)*fvu1u1u4)/
       9 - (f98*fvu1u1u5)/9 - (f102*fvu1u1u7)/6 + 
      (f98*fvu1u1u8)/9 + ((2*f98 - f102)*fvu1u1u9)/9 + 
      ((-8*f98 + f102)*tci12)/18) + 
    fvu1u1u7^2*((3*f16 - 6*f24 + f27 + 30*f57 + 30*f58 - 
        3*f59 - 3*f60 + 3*f64 - 3*f74 - 3*f78 + 3*f83 - 6*f98 + 
        6*f103 + 3*f107 + 3*f122 + 3*f151 + 6*f160 - f162 + 
        3*f177)/18 + ((f98 - 2*f102)*fvu1u1u8)/18 - 
      (f98*fvu1u1u9)/6 + ((-2*f98 + f102)*tci12)/18) + 
    fvu2u1u10*((-6*f24 + f27 - 6*f98 + 6*f103 + 6*f160 - f162)/
       9 - (2*(f98 + f102)*tci12)/9) + 
    tci11^2*((12*f10 + 3*f16 - 2*f50 + 30*f57 + 30*f58 - 
        3*f59 - 3*f60 + 12*f61 + 18*f74 - 12*f85 + 2*f88 + 
        2*f95 + 48*f98 + 12*f102 + 3*f107 + 3*f122 + 3*f151 - 
        48*f160 + 8*f162 + 3*f177)/108 + 
      ((3*f61 + 7*f98 - 10*f102 - 3*f103)*tci12)/108) + 
    fvu1u2u8*((-2*(f98 + f102)*tci11^2)/9 + 
      ((-6*f24 + f27 - 6*f98 + 6*f103 + 6*f160 - f162)*tci12)/
       9) + fvu1u1u9*(f86/9 + (2*(7*f98 + f102)*fvu2u1u11)/9 + 
      (2*(6*f98 + f102)*tci11^2)/27 + 
      ((6*f98 - 6*f160 + f162)*tci12)/9) + 
    fvu1u1u8*(-f75/9 + (f98*fvu1u1u9^2)/9 + 
      (2*(4*f98 + f102)*fvu2u1u9)/9 + (2*(f98 + f102)*fvu2u1u10)/
       9 + (4*f98*fvu2u1u11)/9 + (f98*tci11^2)/18 - 
      (f74*tci12)/3 + (2*(f98 + f102)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*((-6*f98 + 6*f160 - f162)/9 - (2*f98*tci12)/
         9)) + fvu1u1u4*(((f98 + 3*f102)*fvu1u1u5^2)/18 + 
      ((4*f98 + f102)*fvu1u1u8^2)/18 + 
      ((7*f98 + f102)*fvu1u1u9^2)/18 + (2*(f98 + f102)*fvu2u1u2)/
       9 + (2*(3*f98 + f102)*fvu2u1u3)/9 + (4*f98*fvu2u1u11)/9 - 
      (f98*tci11^2)/27 + ((-7*f98 - f102)*fvu1u1u9*tci12)/9 + 
      (2*(3*f98 + f102)*fvu1u2u6*tci12)/9 + 
      fvu1u1u5*(((-f98 - f102)*fvu1u1u9)/3 + 
        ((-f98 - 3*f102)*tci12)/9) + 
      fvu1u1u8*((2*f98*fvu1u1u9)/9 + ((-4*f98 - f102)*tci12)/
         9)) + fvu1u1u3*((f102*fvu1u1u4^2)/6 + 
      ((f98 - 2*f102)*fvu1u1u5^2)/6 - (f102*fvu1u1u7^2)/6 + 
      ((f98 + f102)*fvu1u1u8^2)/6 + (2*f102*fvu2u1u1)/3 + 
      ((-f98 + 2*f102)*fvu2u1u5)/3 + (2*(3*f98 + f102)*tci11^2)/
       9 + ((-f98 + 2*f102)*fvu1u1u5*tci12)/3 + 
      ((-f98 + f102)*fvu1u1u7*tci12)/3 + 
      ((-f98 - f102)*fvu1u1u8*tci12)/3 + 
      ((f98 - 2*f102)*fvu1u2u7*tci12)/3 + 
      fvu1u1u4*(((-f98 - f102)*fvu1u1u8)/3 - (f102*tci12)/3)) + 
    fvu1u1u6*((f102*fvu1u1u8^2)/18 - (2*f102*fvu2u1u8)/9 + 
      (2*f102*tci11^2)/27 - (f102*fvu1u1u8*tci12)/9 + 
      fvu1u1u7*((2*f102*fvu1u1u8)/9 - (2*f102*tci12)/9)) + 
    fvu1u1u2*(-f81/9 - (f102*fvu1u1u3^2)/6 - 
      (2*(2*f98 + f102)*fvu1u1u4^2)/9 + 
      ((-2*f98 - 3*f102)*fvu1u1u5^2)/9 - (f102*fvu1u1u7^2)/6 - 
      (f98*fvu1u1u8^2)/9 + ((-5*f98 - 2*f102)*fvu1u1u9^2)/9 + 
      (2*(3*f98 + f102)*fvu2u1u3)/9 + (2*(f98 + 3*f102)*fvu2u1u4)/
       9 + (2*(5*f98 + f102)*fvu2u1u11)/9 - 
      (2*(f98 + 3*f102)*tci11^2)/27 + 
      ((6*f10 - f50 + 6*f61)*tci12)/9 + 
      (2*(3*f98 + f102)*fvu1u2u6*tci12)/9 + 
      fvu1u1u5*(-f95/9 - (2*f98*tci12)/9) + 
      fvu1u1u8*((-6*f10 + f50 - 6*f61)/9 - (2*f98*fvu1u1u9)/9 + 
        (2*f98*tci12)/9) + fvu1u1u9*(-f95/9 + 
        (2*(2*f98 - f102)*tci12)/9) + 
      fvu1u1u7*((-6*f85 + f88 + 6*f102)/9 - (f102*tci12)/3) + 
      fvu1u1u3*((f102*fvu1u1u7)/3 + (f102*tci12)/3) + 
      fvu1u1u4*((2*f98*fvu1u1u5)/9 - (2*f98*fvu1u1u8)/9 - 
        (2*(2*f98 - f102)*fvu1u1u9)/9 + (2*(f98 + f102)*tci12)/
         9)) + fvu1u1u5*(f67/9 + (f102*fvu1u1u6^2)/6 + 
      ((f98 + 2*f102)*fvu1u1u7^2)/6 + ((f98 + 3*f102)*fvu1u1u8^2)/
       18 + ((f98 + f102)*fvu1u1u9^2)/6 + 
      (2*(f98 + 3*f102)*fvu2u1u4)/9 + ((-f98 + 2*f102)*fvu2u1u5)/
       3 - (2*(f98 + 3*f102)*fvu2u1u7)/9 + 
      ((13*f98 + 54*f102)*tci11^2)/108 + 
      ((6*f24 - f27 - 6*f103)*tci12)/9 + 
      ((f98 - 2*f102)*fvu1u2u7*tci12)/3 + 
      fvu1u1u7*((6*f85 - f88 - 6*f102)/9 - (f98*fvu1u1u8)/9 + 
        (f98*tci12)/9) + fvu1u1u8*((-6*f24 + f27 + 6*f103)/9 + 
        ((-f98 - 3*f102)*tci12)/9) + 
      fvu1u1u6*(-(f102*fvu1u1u8)/3 + (f102*tci12)/3) + 
      fvu1u1u9*(f95/9 + ((f98 + f102)*tci12)/3)) + 
    fvu1u1u7*((-f3 - 10*f16 - 99*f57 - 90*f58 + 10*f59 + 
        9*f60 - f67 + f75 + f81 - f86 - 9*f107 - 9*f122 - 
        f126 - 10*f151 + f159 - 10*f177)/9 + 
      ((-f98 - 4*f102)*fvu1u1u8^2)/18 - (f98*fvu1u1u9^2)/6 - 
      (2*(f98 + 3*f102)*fvu2u1u7)/9 - (2*f102*fvu2u1u8)/9 + 
      (2*(f98 + f102)*fvu2u1u10)/9 + ((-f98 - 2*f102)*tci11^2)/
       9 + ((6*f98 - 6*f160 + f162)*tci12)/9 + 
      (2*(f98 + f102)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*((6*f98 - 6*f160 + f162)/9 - (f98*tci12)/3) + 
      fvu1u1u8*((6*f24 - f27 - 6*f103)/9 + 
        ((-f98 + 2*f102)*tci12)/9)) + 
    fvu1u1u1*((-4*f98*fvu1u1u2^2)/9 + 
      ((-f98 - 2*f102)*fvu1u1u3^2)/6 - 
      (2*(f98 + f102)*fvu1u1u4^2)/9 + 
      ((-f98 + 2*f102)*fvu1u1u5^2)/6 - (f98*fvu1u1u7^2)/6 + 
      ((-7*f98 - 4*f102)*fvu1u1u8^2)/18 + (f98*fvu1u1u9^2)/18 + 
      (2*f102*fvu2u1u1)/3 + (2*(f98 + f102)*fvu2u1u2)/9 + 
      (2*(4*f98 + f102)*fvu2u1u9)/9 - (4*f98*fvu2u1u11)/9 + 
      (2*(5*f98 - 4*f102)*tci11^2)/27 + 
      (5*f98*fvu1u1u9*tci12)/9 + fvu1u1u2*((2*f98*fvu1u1u4)/9 + 
        (2*f98*fvu1u1u8)/9 + (2*f98*fvu1u1u9)/9 - 
        (2*f98*tci12)/9) + fvu1u1u7*((f98*fvu1u1u9)/3 + 
        (2*f98*tci12)/3) + fvu1u1u4*(((-f98 + 2*f102)*fvu1u1u8)/
         9 - (2*f98*fvu1u1u9)/9 + (4*(f98 + f102)*tci12)/9) + 
      fvu1u1u3*((f98*fvu1u1u4)/3 + ((f98 + 2*f102)*tci12)/3) + 
      fvu1u1u8*((-2*f98*fvu1u1u9)/9 + ((7*f98 + 4*f102)*tci12)/
         9)) + ((29*f98 + 8*f102)*tci12*tcr11^2)/18 + 
    ((-9*f98 - 4*f102)*tcr11^3)/18 + 
    ((-3*f61 + 16*f98 - 4*f102 + 3*f103)*tci11^2*tcr12)/18 + 
    (2*(13*f98 + 10*f102)*tci12*tcr21)/9 + 
    tcr11*((-4*(f98 - 2*f102)*tci11^2)/27 - 
      (2*f98*tci12*tcr12)/3 - (10*(f98 + f102)*tcr21)/9) + 
    ((-9*f61 - 12*f92 - 26*f98 - 12*f102 + 9*f103)*tcr33)/36;
L ieu1ueu11 = (f3 + 10*f16 + 99*f57 + 90*f58 - 10*f59 - 
      9*f60 + 9*f107 + 9*f122 + f126 + 10*f151 - f159 + 
      10*f177)/9 + ((f61 + 2*f92 - f102)*fvu1u1u2^2)/6 + 
    ((f102 - f103)*fvu1u1u5^2)/6 + ((-f98 + f103)*fvu1u1u7^2)/6 + 
    ((f61 + f98)*fvu1u1u8^2)/6 + 
    fvu1u1u8*(-f74/3 - (f98*fvu1u1u9)/3) + 
    ((f92 - f102)*fvu2u1u4)/3 + ((f102 - f103)*fvu2u1u7)/3 + 
    ((-f98 + f103)*fvu2u1u10)/3 + ((f61 + f92)*fvu2u1u11)/3 + 
    ((f61 + f98)*fvu2u1u12)/3 + ((f61 + f92 + 4*f98 + f102)*
      tci11^2)/18 + (f74*tci12)/3 + 
    ((-f98 + f103)*fvu1u2u8*tci12)/3 + 
    ((-f61 - f98)*fvu1u2u9*tci12)/3 + 
    fvu1u1u2*(-f78/3 - (f92*fvu1u1u5)/3 + (f102*fvu1u1u7)/3 - 
      (f61*fvu1u1u8)/3 - (f92*fvu1u1u9)/3 + (f61*tci12)/3) + 
    fvu1u1u9*(f83/3 + (f98*tci12)/3) + 
    fvu1u1u7*((-f16 - 10*f57 - 10*f58 + f59 + f60 - f64 + 
        f74 + f78 - f83 - f107 - f122 - f151 - f177)/3 - 
      (f103*fvu1u1u8)/3 + (f98*fvu1u1u9)/3 + (f98*tci12)/3) + 
    fvu1u1u5*(f64/3 - (f102*fvu1u1u7)/3 + (f103*fvu1u1u8)/3 + 
      (f92*fvu1u1u9)/3 - (f103*tci12)/3);
L ieu0ueu11 = (f16 + 10*f57 + 10*f58 - f59 - f60 + 
     f107 + f122 + f151 + f177)/3;
L ieum1ueu11 = 0;
L ieum2ueu11 = 0;
L ieu2uou11 = -2*f89*fvu4u28 - 2*f89*fvu4u39 - 
    (2*f89*fvu4u51)/3 - 2*f89*fvu4u80 - 2*f89*fvu4u83 - 
    2*f89*fvu4u91 - 2*f89*fvu4u93 + (2*f89*fvu4u100)/3 - 
    (2*f89*fvu4u102)/3 + 2*f89*fvu4u139 - (2*f89*fvu4u148)/3 - 
    (3*f89*fvu4u171)/4 + (11*f89*fvu4u174)/4 + (11*f89*fvu4u182)/4 - 
    (3*f89*fvu4u184)/4 + (7*f89*fvu4u192)/6 - (f89*fvu4u199)/4 - 
    (f89*fvu4u201)/2 - f89*fvu4u202 - 4*f89*fvu4u213 + 
    2*f89*fvu4u215 - 2*f89*fvu4u221 + (2*f89*fvu4u225)/3 - 
    f89*fvu4u244 + (3*f89*fvu4u252)/2 - (f89*fvu4u255)/6 + 
    (11*f89*fvu4u271)/4 + (9*f89*fvu4u273)/4 - (f89*fvu4u277)/4 + 
    (3*f89*fvu4u279)/4 - (2*f89*fvu4u282)/3 + (f89*fvu4u289)/4 + 
    (f89*fvu4u290)/2 + f89*fvu4u291 + 2*f89*fvu4u295 + 
    2*f89*fvu4u325 - 2*f89*fvu4u330 + 
    fvu3u25*(f89*fvu1u1u1 - f89*fvu1u1u3 + f89*fvu1u1u6 - 
      f89*fvu1u1u7) + fvu3u71*(f89*fvu1u1u3 - f89*fvu1u1u6 + 
      f89*fvu1u1u7) + fvu3u81*(f89*fvu1u1u2 + f89*fvu1u1u5 + 
      f89*fvu1u1u8) + fvu3u45*(-(f89*fvu1u1u2) - f89*fvu1u1u3 + 
      f89*fvu1u1u9) + fvu3u43*(-(f89*fvu1u1u2) - f89*fvu1u1u6 + 
      f89*fvu1u1u7 + f89*fvu1u1u9) + 
    fvu3u82*(-(f89*fvu1u1u5) - f89*fvu1u1u6 + f89*fvu1u1u7 - 
      f89*fvu1u1u8 + f89*fvu1u1u9) + 
    fvu3u63*(-(f89*fvu1u1u6) - f89*fvu1u1u7 - f89*fvu1u1u9 + 
      2*f89*fvu1u1u10) - (151*f89*tci11^3*tci12)/135 + 
    fvu3u78*(-(f89*fvu1u1u2) + 2*f89*fvu1u1u3 - f89*fvu1u1u5 + 
      f89*fvu1u1u8 - 2*f89*tci12) + 
    fvu3u80*(f89*fvu1u1u3 - f89*fvu1u1u5 + (f89*fvu1u1u6)/2 - 
      (f89*fvu1u1u7)/2 + (f89*fvu1u1u8)/2 - (3*f89*fvu1u1u9)/2 - 
      (3*f89*tci12)/2) - (2*f89*tci11^2*tci21)/9 + 
    fvu1u1u1*(f89*fvu3u45 - f89*fvu3u71 - f89*fvu3u78 - 
      f89*fvu3u81 + f89*fvu3u82 + (8*f89*tci11^3)/27 - 
      8*f89*tci12*tci21) + fvu1u1u3*((-4*f89*tci11^3)/27 + 
      4*f89*tci12*tci21) + fvu1u1u5*((-4*f89*tci11^3)/27 + 
      4*f89*tci12*tci21) + fvu1u1u8*((-5*f89*tci11^3)/27 + 
      5*f89*tci12*tci21) + tci12*((-48*f89*tci31)/5 - 
      16*f89*tci32) - (4*f89*tci41)/3 + 
    ((-4*f89*tci11^3)/9 + 8*f89*tci12*tci21)*tcr11 + 
    (f89*tci11*tci12*tcr11^2)/5 + 
    fvu1u1u10*((11*f89*tci11^3)/135 + (4*f89*tci12*tci21)/
       3 + (48*f89*tci31)/5 + 4*f89*tci21*tcr11 - 
      (f89*tci11*tcr11^2)/5) + 
    fvu1u1u2*(f89*fvu3u63 + f89*fvu3u80 + (31*f89*tci11^3)/
       270 - (4*f89*tci12*tci21)/3 + (24*f89*tci31)/5 + 
      2*f89*tci21*tcr11 - (f89*tci11*tcr11^2)/10) + 
    fvu1u1u6*(-(f89*tci11^3)/270 - (5*f89*tci12*tci21)/3 - 
      (24*f89*tci31)/5 - 2*f89*tci21*tcr11 + 
      (f89*tci11*tcr11^2)/10) + 
    fvu1u1u7*((-7*f89*tci11^3)/90 + (f89*tci12*tci21)/3 - 
      (24*f89*tci31)/5 - 2*f89*tci21*tcr11 + 
      (f89*tci11*tcr11^2)/10) + 
    fvu1u1u9*((-7*f89*tci11^3)/90 + (f89*tci12*tci21)/3 - 
      (24*f89*tci31)/5 - 2*f89*tci21*tcr11 + 
      (f89*tci11*tcr11^2)/10) + 
    (40*f89*tci12*tci21*tcr12)/3 + 4*f89*tci11*tci12*
     tcr12^2;
L ieu1uou11 = 2*f89*fvu3u81 - 
    (4*f89*tci11^3)/27 + 4*f89*tci12*tci21;
L ieu0uou11 = 0;
L ieum1uou11 = 0;
L ieum2uou11 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou11+w^2*ieu1uou11+w^3*ieu0uou11+w^4*ieum1uou11+w^5*ieum2uou11;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou11a = K[w^1];
L ieu1uou11a = K[w^2];
L ieu0uou11a = K[w^3];
L ieum1uou11a = K[w^4];
L ieum2uou11a = K[w^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_odd.c> "%O"
#write <e11_odd.c> "return Eps5o2<T>("
#write <e11_odd.c> "%E", ieu2uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu0uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum2uou11a
#write <e11_odd.c> ");\n}"
L H=+u^1*ieu2ueu11+u^2*ieu1ueu11+u^3*ieu0ueu11+u^4*ieum1ueu11+u^5*ieum2ueu11;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu11a = H[u^1];
L ieu1ueu11a = H[u^2];
L ieu0ueu11a = H[u^3];
L ieum1ueu11a = H[u^4];
L ieum2ueu11a = H[u^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_even.c> "%O"
#write <e11_even.c> "return Eps5o2<T>("
#write <e11_even.c> "%E", ieu2ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu0ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum2ueu11a
#write <e11_even.c> ");\n}"
.end
