#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f67;
S f65;
S f64;
S f62;
S f69;
S f68;
S f76;
S f126;
S f70;
S f71;
S f122;
S f72;
S f128;
S f78;
S f129;
S f79;
S f151;
S f159;
S f81;
S f3;
S f83;
S f14;
S f82;
S f13;
S f84;
S f87;
S f86;
S f96;
S f29;
S f28;
S f93;
S f90;
S f177;
S f26;
S f99;
S f31;
S f59;
S f53;
S f100;
S f51;
S f107;
S f57;
S f55;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu4u132;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu4u313;
S fvu4u91;
S fvu4u93;
S fvu4u113;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u330;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu1 = (36*f57 - 11*f59)/27 - (f90*fvu3u1)/3 + 
    ((-f51 + f90)*fvu3u2)/6 + (f90*fvu3u3)/9 + 
    ((3*f79 - 2*f90)*fvu3u4)/18 + (f90*fvu3u5)/18 - 
    (2*f90*fvu3u6)/9 - (2*f90*fvu3u11)/9 - (f90*fvu3u12)/18 + 
    ((-2*f90 - 3*f96)*fvu3u13)/18 - (f90*fvu3u14)/9 + 
    ((f84 - f90)*fvu3u15)/6 + (f90*fvu3u17)/6 - (f90*fvu3u18)/6 - 
    (f90*fvu3u19)/9 + (f90*fvu3u22)/3 - (7*f90*fvu1u1u1^3)/18 + 
    (f90*fvu1u1u2^3)/3 + (f90*fvu1u1u3^3)/18 - 
    (f90*fvu1u1u5^3)/9 + fvu1u1u4^2*(-f53/6 + (f90*fvu1u1u5)/9 - 
      (f90*fvu1u1u8)/18 + (f90*fvu1u1u9)/18) + 
    ((f82 + f93)*fvu2u1u2)/9 + ((f93 + f99)*fvu2u1u3)/9 + 
    ((-f87 + f99)*fvu2u1u4)/9 + fvu1u1u8*((-2*f90*fvu2u1u9)/9 - 
      (f90*tci11^2)/54) + fvu1u1u9*((2*f90*fvu2u1u11)/9 + 
      (2*f90*tci11^2)/27) + 
    ((2*f3 - 8*f13 + f14 + 8*f26 - 8*f28 - f29 + f31 - f55 + 
       18*f57 - 2*f59 - 8*f62 - 8*f64 + f65 + f67 - 8*f69 - 
       f70 + f71 - 8*f72 + 8*f78 - f81 - 8*f83 + f86 + f107 + 
       f122 + f126 + 8*f128 - f129 + f151 - f159 + f177)*
      tci12)/9 - (f90*fvu1u1u9^2*tci12)/18 + 
    ((f93 + f99)*fvu1u2u6*tci12)/9 + (4*f90*fvu2u1u9*tci12)/
     9 + fvu1u1u3^2*((-3*f72 - f87 + f100)/18 - 
      (f90*fvu1u1u4)/9 - (f90*fvu1u1u8)/6 - (f90*tci12)/2) + 
    fvu1u1u5^2*(-f62/6 + (f90*fvu1u1u9)/6 - (f90*tci12)/9) + 
    fvu1u1u2^2*((3*f53 - 3*f57 + 3*f62 + 3*f68 + 3*f72 - f87 + 
        f93 + 2*f99)/18 - (f90*fvu1u1u4)/6 - (f90*fvu1u1u5)/18 - 
      (f90*fvu1u1u9)/9 + (f90*tci12)/6) + 
    fvu2u1u5*((-f87 + f100)/9 + (f90*tci12)/3) + 
    fvu1u1u1^2*((-3*f68 + 2*f82 + f93 - f100)/18 - 
      (f90*fvu1u1u2)/6 + (f90*fvu1u1u3)/18 + (f90*fvu1u1u4)/6 + 
      (f90*fvu1u1u8)/9 + (5*f90*tci12)/6) + 
    fvu2u1u1*((f82 - f100)/9 + (8*f90*tci12)/9) + 
    tci11^2*((-18*f53 - 3*f57 - 18*f68 - 18*f72 + 2*f82 - 
        2*f87 + 2*f93)/108 + ((-3*f51 + 2*f79 - f84 + 4*f90 - 
         f96)*tci12)/36) + fvu1u2u7*(-(f90*tci11^2)/3 + 
      ((f87 - f100)*tci12)/9) + 
    fvu1u1u5*(f65/9 + (f90*fvu1u1u9^2)/6 + (4*f90*fvu2u1u4)/9 - 
      (f90*fvu2u1u5)/3 + (13*f90*tci11^2)/108 - 
      (f99*tci12)/9 + (f90*fvu1u1u9*tci12)/3 + 
      (f90*fvu1u2u7*tci12)/3) + 
    fvu1u1u4*(f55/9 + (f90*fvu1u1u5^2)/9 - (f90*fvu1u1u8^2)/18 + 
      (f90*fvu1u1u9^2)/18 + (f53*tci12)/3 + 
      (f90*fvu1u1u8*tci12)/9 - (f90*fvu1u1u9*tci12)/9 + 
      fvu1u1u5*(f99/9 - (f90*fvu1u1u9)/3 - (2*f90*tci12)/9)) + 
    fvu1u1u3*((-2*f3 + 8*f13 - f14 - 8*f26 + 8*f28 + f29 - 
        f31 - 18*f57 + 2*f59 + 8*f62 + 8*f64 - f65 - f67 + 
        8*f69 - f71 + 8*f72 - 8*f78 + f81 + 8*f83 - f86 - 
        f107 - f122 - f126 - 8*f128 + f129 - f151 + f159 - 
        f177)/9 - (f90*fvu1u1u4^2)/9 + (f90*fvu1u1u5^2)/6 - 
      (f90*fvu1u1u8^2)/6 - (4*f90*fvu2u1u1)/9 - 
      (f90*fvu2u1u5)/3 + (17*f90*tci11^2)/27 + 
      (f72*tci12)/3 + (f90*fvu1u1u8*tci12)/3 + 
      (f90*fvu1u2u7*tci12)/3 + fvu1u1u5*
       (-f100/9 - (f90*tci12)/3) + fvu1u1u4*
       (f82/9 + (f90*fvu1u1u8)/3 + (2*f90*tci12)/9)) + 
    fvu1u1u1*(f70/9 + (5*f90*fvu1u1u3^2)/18 + 
      (f90*fvu1u1u4^2)/6 + (f100*fvu1u1u5)/9 - 
      (f90*fvu1u1u5^2)/6 + (2*f90*fvu1u1u8^2)/9 - 
      (4*f90*fvu2u1u1)/9 - (2*f90*fvu2u1u9)/9 - 
      (f90*tci11^2)/3 + ((3*f68 - f93 + f100)*tci12)/9 - 
      (4*f90*fvu1u1u8*tci12)/9 + fvu1u1u3*
       (-f82/9 - (f90*fvu1u1u4)/9 - (5*f90*tci12)/9) + 
      fvu1u1u4*(-f82/9 - (2*f90*fvu1u1u8)/9 - (f90*tci12)/3) + 
      fvu1u1u2*(-f93/9 + (f90*tci12)/3)) + 
    fvu1u1u2*((2*f3 - 8*f13 + f14 + 8*f26 - 8*f28 - f29 + 
        f31 - f55 + 18*f57 - f59 - 8*f62 - 8*f64 + f67 - 
        8*f69 - f70 + f71 - 8*f72 + 8*f78 - f81 - 8*f83 + 
        f86 + f107 + f122 + f126 + 8*f128 - f129 + f151 - 
        f159 + f177)/9 + (f90*fvu1u1u3^2)/6 - 
      (f90*fvu1u1u4^2)/6 - (5*f90*fvu1u1u5^2)/18 - 
      (2*f90*fvu1u1u9^2)/9 + (4*f90*fvu2u1u4)/9 + 
      (2*f90*fvu2u1u11)/9 - (f90*tci11^2)/9 - (f87*tci12)/9 - 
      (2*f90*fvu1u1u9*tci12)/9 + fvu1u1u3*
       (f87/9 - (f90*tci12)/3) + fvu1u1u5*
       (-f99/9 - (f90*tci12)/9) + fvu1u1u4*
       (-f99/9 + (f90*fvu1u1u5)/9 + (2*f90*fvu1u1u9)/9 + 
        (f90*tci12)/3)) + ((-3*f51 + 2*f79 - 2*f90)*tci12*
      tcr11^2)/6 + ((f51 - 4*f79 + 6*f90)*tcr11^3)/18 + 
    ((-7*f51 - 2*f79 + 6*f84 + 19*f90 + 6*f96)*tci11^2*
      tcr12)/36 + ((f51 + 2*f79 - f90)*tcr12^3)/9 - 
    (2*(f51 - f79 + f90)*tci12*tcr21)/3 + 
    tcr11*(((7*f51 + 5*f90)*tci11^2)/36 + 
      ((f51 - f90)*tci12*tcr12)/3 - (2*(f79 - f90)*tcr21)/
       3) + ((-f51 - 2*f79 + f90)*tcr31)/3 + 
    ((-f51 - 2*f79 + f90)*tcr32)/12 + 
    ((11*f51 + 74*f79 + 18*f84 - 11*f90 + 18*f96)*tcr33)/72;
L ieu1ueu1 = -f59/9 + ((-f51 + 2*f79 + f90)*fvu1u1u1^2)/
     6 + ((-f84 + f90 + 2*f96)*fvu1u1u2^2)/6 + 
    ((f51 - f84)*fvu1u1u3^2)/6 + 
    fvu1u1u3*(f72/3 + (f79*fvu1u1u4)/3 - (f51*fvu1u1u5)/3) + 
    fvu1u1u4*(f53/3 + (f96*fvu1u1u5)/3) + 
    ((-f51 + f79)*fvu2u1u1)/3 + ((f79 + f90)*fvu2u1u2)/3 + 
    ((f90 + f96)*fvu2u1u3)/3 + ((-f84 + f96)*fvu2u1u4)/3 + 
    ((f51 - f84)*fvu2u1u5)/3 + ((f79 - f84 + f90)*tci11^2)/
     18 + ((-f53 - f68 - f72)*tci12)/3 + 
    ((f90 + f96)*fvu1u2u6*tci12)/3 + 
    ((-f51 + f84)*fvu1u2u7*tci12)/3 + 
    fvu1u1u2*((-f53 + f57 - f62 - f68 - f72)/3 + 
      (f84*fvu1u1u3)/3 - (f96*fvu1u1u4)/3 - (f96*fvu1u1u5)/3 - 
      (f84*tci12)/3) + fvu1u1u1*(f68/3 - (f90*fvu1u1u2)/3 - 
      (f79*fvu1u1u3)/3 - (f79*fvu1u1u4)/3 + (f51*fvu1u1u5)/3 + 
      ((f51 - f90)*tci12)/3) + 
    fvu1u1u5*(f62/3 - (f96*tci12)/3);
L ieu0ueu1 = -f57/3;
L ieum1ueu1 = 0;
L ieum2ueu1 = 0;
L ieu2uou1 = (5*f76*fvu4u25)/3 - (f76*fvu4u28)/3 - 
    (7*f76*fvu4u39)/3 - (f76*fvu4u41)/3 - (f76*fvu4u49)/3 - 
    (5*f76*fvu4u51)/9 + (5*f76*fvu4u80)/12 - (37*f76*fvu4u83)/12 - 
    (7*f76*fvu4u91)/4 + (5*f76*fvu4u93)/12 + (f76*fvu4u100)/3 - 
    (23*f76*fvu4u102)/18 + (f76*fvu4u111)/4 + (f76*fvu4u113)/2 + 
    f76*fvu4u114 - (8*f76*fvu4u129)/3 + (7*f76*fvu4u132)/3 - 
    (f76*fvu4u139)/6 - (8*f76*fvu4u141)/3 + (4*f76*fvu4u146)/9 + 
    (f76*fvu4u148)/18 + (5*f76*fvu4u171)/12 - (f76*fvu4u174)/12 - 
    (f76*fvu4u182)/12 + (5*f76*fvu4u184)/12 + (f76*fvu4u190)/9 - 
    (f76*fvu4u192)/18 + (f76*fvu4u199)/4 + (f76*fvu4u201)/2 + 
    f76*fvu4u202 + (7*f76*fvu4u213)/12 - (f76*fvu4u215)/4 + 
    (9*f76*fvu4u219)/4 + (7*f76*fvu4u221)/12 - (4*f76*fvu4u225)/9 - 
    (f76*fvu4u233)/4 - (f76*fvu4u234)/2 - f76*fvu4u235 + 
    (17*f76*fvu4u244)/3 + (4*f76*fvu4u246)/3 - (7*f76*fvu4u252)/2 - 
    (17*f76*fvu4u255)/18 - (5*f76*fvu4u271)/12 - 
    (43*f76*fvu4u273)/12 + (67*f76*fvu4u277)/12 - 
    (5*f76*fvu4u279)/12 - (f76*fvu4u282)/3 - (f76*fvu4u289)/4 - 
    (f76*fvu4u290)/2 - f76*fvu4u291 + (4*f76*fvu4u293)/3 - 
    (2*f76*fvu4u295)/3 + 2*f76*fvu4u313 + 2*f76*fvu4u330 + 
    fvu3u71*(-(f76*fvu1u1u2)/3 + (f76*fvu1u1u3)/3 + 
      (f76*fvu1u1u5)/3 - (f76*fvu1u1u6)/3 + (f76*fvu1u1u7)/3 + 
      (f76*fvu1u1u8)/3) + fvu3u78*((2*f76*fvu1u1u2)/3 - 
      (2*f76*fvu1u1u3)/3 - (2*f76*fvu1u1u9)/3) + 
    fvu3u70*((f76*fvu1u1u2)/6 - (f76*fvu1u1u3)/6 - 
      (f76*fvu1u1u9)/6) + fvu3u45*(-(f76*fvu1u1u2)/2 - 
      (7*f76*fvu1u1u3)/6 - (2*f76*fvu1u1u6)/3 + 
      (2*f76*fvu1u1u7)/3 + (f76*fvu1u1u9)/2) + 
    fvu3u43*(-(f76*fvu1u1u2)/6 - (f76*fvu1u1u4)/3 - 
      (f76*fvu1u1u6)/6 + (f76*fvu1u1u7)/6 + (f76*fvu1u1u9)/2 - 
      (f76*fvu1u1u10)/3) + fvu3u63*((2*f76*fvu1u1u2)/3 + 
      (3*f76*fvu1u1u3)/2 - (f76*fvu1u1u5)/3 + (f76*fvu1u1u6)/2 - 
      (f76*fvu1u1u7)/3 - (f76*fvu1u1u8)/3 - (f76*fvu1u1u9)/3 - 
      (f76*fvu1u1u10)/6) + fvu3u25*(-(f76*fvu1u1u2)/3 - 
      (7*f76*fvu1u1u3)/6 + f76*fvu1u1u4 - (2*f76*fvu1u1u5)/3 + 
      (5*f76*fvu1u1u6)/6 - (f76*fvu1u1u7)/6 + 
      (f76*fvu1u1u10)/3) + fvu3u81*((f76*fvu1u1u3)/6 + 
      (f76*fvu1u1u4)/3 + (f76*fvu1u1u6)/6 - (f76*fvu1u1u7)/6 - 
      (f76*fvu1u1u9)/3 + (f76*fvu1u1u10)/3) + 
    fvu3u62*((-4*f76*fvu1u1u2)/3 + (2*f76*fvu1u1u4)/3 + 
      (2*f76*fvu1u1u5)/3 - (4*f76*fvu1u1u6)/3 + 
      (2*f76*fvu1u1u7)/3 + (4*f76*fvu1u1u10)/3) + 
    (13*f76*tci11^3*tci12)/15 + 
    fvu3u23*((f76*fvu1u1u1)/3 + (f76*fvu1u1u2)/6 + 
      f76*fvu1u1u3 + f76*fvu1u1u4 + f76*fvu1u1u5 - 
      (f76*fvu1u1u6)/2 - (f76*fvu1u1u7)/6 - (f76*fvu1u1u8)/3 - 
      (f76*fvu1u1u9)/6 - (f76*fvu1u1u10)/3 - 2*f76*tci12) + 
    fvu3u82*((-2*f76*fvu1u1u3)/3 - (2*f76*fvu1u1u4)/3 + 
      f76*fvu1u1u5 + f76*fvu1u1u6 - (f76*fvu1u1u7)/3 + 
      (f76*fvu1u1u8)/3 - (f76*fvu1u1u9)/3 + (4*f76*tci12)/3) + 
    fvu3u80*((2*f76*fvu1u1u2)/3 + f76*fvu1u1u3 - 
      (19*f76*fvu1u1u6)/6 + (19*f76*fvu1u1u7)/6 - 
      (17*f76*fvu1u1u8)/6 + (13*f76*fvu1u1u9)/6 + 
      (17*f76*tci12)/6) - (10*f76*tci11^2*tci21)/27 + 
    fvu1u1u8*((-17*f76*tci11^3)/81 + (17*f76*tci12*tci21)/
       3) + tci12*((16*f76*tci31)/5 + 16*f76*tci32) - 
    (2072*f76*tci41)/135 - (64*f76*tci42)/5 - 
    (16*f76*tci43)/3 + ((1259*f76*tci11^3)/1620 + 
      f76*tci12*tci21 + (84*f76*tci31)/5 + 
      16*f76*tci32)*tcr11 - (11*f76*tci11*tcr11^3)/180 + 
    fvu1u1u6*((f76*tci11^3)/810 + (58*f76*tci12*tci21)/9 + 
      (88*f76*tci31)/5 + (22*f76*tci21*tcr11)/3 - 
      (11*f76*tci11*tcr11^2)/30) + 
    fvu1u1u2*((181*f76*tci11^3)/1620 + (2*f76*tci12*tci21)/
       9 + (44*f76*tci31)/5 + (11*f76*tci21*tcr11)/3 - 
      (11*f76*tci11*tcr11^2)/60) + 
    fvu1u1u3*((73*f76*tci11^3)/1620 - (f76*tci12*tci21)/3 + 
      (12*f76*tci31)/5 + f76*tci21*tcr11 - 
      (f76*tci11*tcr11^2)/20) + 
    fvu1u1u9*((103*f76*tci11^3)/540 - (49*f76*tci12*tci21)/
       9 - (4*f76*tci31)/5 - (f76*tci21*tcr11)/3 + 
      (f76*tci11*tcr11^2)/60) + 
    fvu1u1u4*((-31*f76*tci11^3)/270 + (4*f76*tci12*tci21)/
       3 - (24*f76*tci31)/5 - 2*f76*tci21*tcr11 + 
      (f76*tci11*tcr11^2)/10) + 
    fvu1u1u10*((-13*f76*tci11^3)/180 - (f76*tci12*tci21)/
       9 - (28*f76*tci31)/5 - (7*f76*tci21*tcr11)/3 + 
      (7*f76*tci11*tcr11^2)/60) + 
    fvu1u1u7*((221*f76*tci11^3)/1620 - (19*f76*tci12*tci21)/
       3 - (36*f76*tci31)/5 - 3*f76*tci21*tcr11 + 
      (3*f76*tci11*tcr11^2)/20) + 
    fvu1u1u1*((f76*fvu3u25)/6 + (f76*fvu3u43)/3 + 
      (7*f76*fvu3u45)/6 - (2*f76*fvu3u62)/3 - (7*f76*fvu3u63)/6 + 
      (f76*fvu3u70)/6 - (2*f76*fvu3u71)/3 + (2*f76*fvu3u78)/3 - 
      f76*fvu3u80 - (f76*fvu3u81)/2 - (f76*fvu3u82)/3 - 
      (179*f76*tci11^3)/1620 + (f76*tci12*tci21)/3 - 
      (36*f76*tci31)/5 - 3*f76*tci21*tcr11 + 
      (3*f76*tci11*tcr11^2)/20) + 
    fvu1u1u5*((-8*f76*tci11^3)/135 - (28*f76*tci12*tci21)/
       9 - (64*f76*tci31)/5 - (16*f76*tci21*tcr11)/3 + 
      (4*f76*tci11*tcr11^2)/15) + 
    ((-218*f76*tci11^3)/243 - (40*f76*tci12*tci21)/3)*
     tcr12 + (-4*f76*tci11*tci12 - (40*f76*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f76*tci11*tci12)/15 + 
      5*f76*tci21 - 2*f76*tci11*tcr12) + 
    (130*f76*tci11*tcr33)/27;
L ieu1uou1 = 
   2*f76*fvu3u23 - (11*f76*tci11^3)/135 - 
    (4*f76*tci12*tci21)/3 - (48*f76*tci31)/5 - 
    4*f76*tci21*tcr11 + (f76*tci11*tcr11^2)/5;
L ieu0uou1 = 0;
L ieum1uou1 = 0;
L ieum2uou1 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou1+w^2*ieu1uou1+w^3*ieu0uou1+w^4*ieum1uou1+w^5*ieum2uou1;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou1a = K[w^1];
L ieu1uou1a = K[w^2];
L ieu0uou1a = K[w^3];
L ieum1uou1a = K[w^4];
L ieum2uou1a = K[w^5];
.sort
#write <e1.tmp> "`optimmaxvar_'"
#write <e1_odd.c> "%O"
#write <e1_odd.c> "return Eps5o2<T>("
#write <e1_odd.c> "%E", ieu2uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieu1uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieu0uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieum1uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieum2uou1a
#write <e1_odd.c> ");\n}"
L H=+u^1*ieu2ueu1+u^2*ieu1ueu1+u^3*ieu0ueu1+u^4*ieum1ueu1+u^5*ieum2ueu1;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu1a = H[u^1];
L ieu1ueu1a = H[u^2];
L ieu0ueu1a = H[u^3];
L ieum1ueu1a = H[u^4];
L ieum2ueu1a = H[u^5];
.sort
#write <e1.tmp> "`optimmaxvar_'"
#write <e1_even.c> "%O"
#write <e1_even.c> "return Eps5o2<T>("
#write <e1_even.c> "%E", ieu2ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieu1ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieu0ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieum1ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieum2ueu1a
#write <e1_even.c> ");\n}"
.end
