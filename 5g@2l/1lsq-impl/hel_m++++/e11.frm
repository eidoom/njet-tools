#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f180;
S f181;
S f63;
S f61;
S f124;
S f125;
S f75;
S f126;
S f76;
S f77;
S f121;
S f122;
S f123;
S f78;
S f79;
S f153;
S f155;
S f89;
S f9;
S f8;
S f148;
S f81;
S f146;
S f3;
S f147;
S f80;
S f83;
S f82;
S f85;
S f7;
S f84;
S f6;
S f87;
S f86;
S f4;
S f96;
S f179;
S f94;
S f178;
S f95;
S f93;
S f91;
S f173;
S f22;
S f20;
S f27;
S f26;
S f175;
S f25;
S f98;
S f174;
S f24;
S f38;
S f49;
S f48;
S f160;
S f30;
S f161;
S f46;
S f33;
S f34;
S f164;
S f110;
S f35;
S f43;
S f36;
S f109;
S f52;
S f102;
S f103;
S f50;
S f51;
S f101;
S f106;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu4u279;
S fvu4u91;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u330;
S fvu4u100;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u325;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu11 = (-110*f3 + 11*f4 - 11*f35 - 11*f75 + 
      74*f83 - 11*f106 - 11*f121)/27 + ((-f95 - 2*f98)*fvu3u1)/3 + 
    ((f95 + 2*f98)*fvu3u2)/6 + (2*f95*fvu3u3)/9 - (f98*fvu3u4)/6 + 
    ((11*f95 - f98)*fvu3u5)/18 + ((-f95 - 2*f98)*fvu3u6)/9 + 
    ((2*f95 + f101)*fvu3u7)/6 + (16*f95*fvu3u8)/9 - 
    (f95*fvu3u10)/6 + (2*(f95 + f98)*fvu3u11)/9 + 
    ((4*f95 - f98)*fvu3u12)/18 + ((-f95 + 3*f98)*fvu3u13)/18 + 
    (f95*fvu3u14)/3 + (f98*fvu3u15)/6 + 
    ((f95 - f98 + f102)*fvu3u17)/6 + ((f95 - f98)*fvu3u18)/6 + 
    ((-f95 - 2*f98)*fvu3u19)/9 - (2*f95*fvu3u24)/9 - 
    (f95*fvu3u31)/9 - (f98*fvu3u32)/18 + (2*f98*fvu3u36)/9 + 
    ((-3*f77 - f95 + 3*f98)*fvu3u37)/18 + ((-f95 + f98)*fvu3u38)/
     6 - (f98*fvu3u40)/6 + (f95*fvu3u42)/3 + 
    ((9*f95 - 4*f98)*fvu1u1u1^3)/18 + 
    ((11*f95 - 15*f98)*fvu1u1u2^3)/27 + 
    ((f95 - 6*f98)*fvu1u1u3^3)/18 + ((-2*f95 + f98)*fvu1u1u5^3)/
     9 + (f98*fvu1u1u6^3)/9 + ((f95 - f98)*fvu1u1u7^3)/9 + 
    (4*f95*fvu1u1u9^3)/27 + fvu1u1u8^2*
     ((6*f61 - f63 - 3*f89 + 6*f95 + 6*f101 - 6*f146 + f164)/
       18 + (f95*fvu1u1u9)/9) + fvu1u1u4^2*
     (((f95 - 3*f98)*fvu1u1u5)/18 + ((4*f95 - f98)*fvu1u1u8)/18 + 
      ((7*f95 - f98)*fvu1u1u9)/18) + 
    ((-6*f94 + f96 + 6*f98 + f103)*fvu2u1u4)/9 + 
    ((-6*f20 + f51 - 6*f77 + 6*f94 - f96 - 6*f98)*fvu2u1u7)/9 + 
    ((6*f61 - f63 + 6*f95 + 6*f101 - 6*f146 + f164)*fvu2u1u12)/
     9 - (2*(3*f95 - f98)*fvu1u2u6*tci11^2)/9 + 
    ((-f95 - 2*f98)*fvu1u2u7*tci11^2)/3 + 
    ((20*f3 - 2*f4 - 16*f6 + 2*f7 - 16*f8 + 2*f9 + 16*f24 - 
       2*f25 - 16*f26 + 2*f27 + 16*f30 - 2*f33 - 8*f34 + f35 + 
       f36 + 3*f38 + 16*f43 - 2*f46 - 8*f48 + 8*f49 + f50 - 
       f52 + 20*f75 - 2*f76 + 16*f78 + 16*f79 - 2*f80 - 2*f81 + 
       8*f82 - 2*f83 - f84 - 8*f85 + f87 - 8*f89 + f106 + 
       16*f109 - 2*f110 + 2*f121 - 8*f122 + f123 + 16*f124 + 
       f125 - 2*f126 + 8*f153 - f155 + 8*f160 - f161 + 8*f173 - 
       f174 + 4*f175 + 8*f178 - f179 - 8*f180 + f181)*tci12)/
     9 + ((-6*f61 + f63 - 6*f95 - 6*f101 + 6*f146 - f164)*
      fvu1u2u9*tci12)/9 + (4*f98*fvu2u1u1*tci12)/3 - 
    (4*(f95 - f98)*fvu2u1u2*tci12)/9 - 
    (2*(3*f95 - f98)*fvu2u1u3*tci12)/9 + 
    ((f95 + 2*f98)*fvu2u1u5*tci12)/3 - 
    (4*(4*f95 - f98)*fvu2u1u9*tci12)/9 + 
    fvu2u1u11*((6*f61 - f63 + 6*f101 + f103)/9 - 
      (4*f95*tci12)/9) + fvu1u1u2^2*
     ((6*f61 - f63 + 3*f79 + 3*f83 - 3*f86 + 3*f89 + 3*f91 - 
        6*f94 + f96 + 6*f98 + 6*f101 + 2*f103)/18 + 
      (f98*fvu1u1u3)/6 + ((f95 + f98)*fvu1u1u4)/9 - 
      (f95*fvu1u1u5)/9 + (f98*fvu1u1u7)/6 + (f95*fvu1u1u8)/9 + 
      ((2*f95 + f98)*fvu1u1u9)/9 + ((-8*f95 - f98)*tci12)/18) + 
    fvu1u1u7^2*((6*f20 - f51 + 6*f77 - 3*f91 - 6*f95 + 6*f146 - 
        f164)/18 + ((f95 + 2*f98)*fvu1u1u8)/18 - 
      (f95*fvu1u1u9)/6 + ((-2*f95 - f98)*tci12)/18) + 
    fvu2u1u10*((6*f20 - f51 + 6*f77 - 6*f95 + 6*f146 - f164)/
       9 - (2*(f95 - f98)*tci12)/9) + 
    fvu1u1u6^2*((f98*fvu1u1u7)/9 - (f98*fvu1u1u8)/18 + 
      (f98*tci12)/18) + fvu1u1u9^2*(-f79/6 + 
      ((-10*f95 + f98)*tci12)/18) + 
    fvu1u1u3^2*(-(f98*fvu1u1u4)/6 + ((f95 + f98)*fvu1u1u7)/6 + 
      ((f95 - f98)*fvu1u1u8)/6 + ((-3*f95 + 2*f98)*tci12)/6) + 
    fvu1u1u5^2*((-6*f20 + f51 - 6*f77 + 3*f86 + 6*f94 - f96 - 
        6*f98)/18 - (f98*fvu1u1u6)/6 + (f95*fvu1u1u7)/18 + 
      ((f95 - 3*f98)*fvu1u1u8)/18 + ((f95 - f98)*fvu1u1u9)/6 + 
      ((-f95 + 3*f98)*tci12)/9) + fvu1u1u1^2*
     (-(f95*fvu1u1u2)/9 - (f95*fvu1u1u3)/6 + 
      ((-f95 + f98)*fvu1u1u4)/9 - (f95*fvu1u1u7)/3 + 
      ((f95 + 2*f98)*fvu1u1u8)/18 - (f95*fvu1u1u9)/18 + 
      ((-23*f95 + 8*f98)*tci12)/18) + 
    tci11^2*((12*f61 - 2*f63 + 3*f83 - 18*f89 + 12*f94 + 
        48*f95 - 2*f96 - 12*f98 + 12*f101 + 2*f103 - 48*f146 + 
        8*f164)/108 + ((-3*f77 + 7*f95 + 10*f98 + 3*f101)*
        tci12)/108) + fvu1u2u8*((-2*(f95 - f98)*tci11^2)/9 + 
      ((6*f20 - f51 + 6*f77 - 6*f95 + 6*f146 - f164)*tci12)/
       9) + fvu1u1u9*(f81/9 + (2*(7*f95 - f98)*fvu2u1u11)/9 + 
      (2*(6*f95 - f98)*tci11^2)/27 + 
      ((6*f95 - 6*f146 + f164)*tci12)/9) + 
    fvu1u1u8*((-20*f3 + 2*f4 + 16*f6 - 2*f7 + 16*f8 - 2*f9 - 
        16*f24 + 2*f25 + 16*f26 - 2*f27 - 16*f30 + 2*f33 + 
        8*f34 - f35 - f36 - 3*f38 - 16*f43 + 2*f46 + 8*f48 - 
        8*f49 - f50 + f52 - 20*f75 + 2*f76 - 16*f78 - 16*f79 + 
        2*f80 + 2*f81 - 8*f82 + 2*f83 + f84 + 8*f85 - f87 + 
        8*f89 - f106 - 16*f109 + 2*f110 - 2*f121 + 8*f122 - 
        f123 - 16*f124 - f125 + 2*f126 - 8*f153 + f155 - 
        8*f160 + f161 - 8*f173 + f174 - 4*f175 - 8*f178 + 
        f179 + 8*f180 - f181)/9 + (f95*fvu1u1u9^2)/9 + 
      (2*(4*f95 - f98)*fvu2u1u9)/9 + (2*(f95 - f98)*fvu2u1u10)/
       9 + (4*f95*fvu2u1u11)/9 + (f95*tci11^2)/18 + 
      (f89*tci12)/3 + (2*(f95 - f98)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*((-6*f95 + 6*f146 - f164)/9 - (2*f95*tci12)/
         9)) + fvu1u1u7*((19*f3 - 2*f4 - 16*f6 + 2*f7 - 16*f8 + 
        2*f9 + f22 + 16*f24 - 2*f25 - 16*f26 + 2*f27 + 16*f30 - 
        2*f33 + 2*f35 + 2*f38 + 16*f43 - 2*f46 - 8*f48 + 
        8*f49 + f50 - f52 + 2*f75 + 16*f78 + 16*f79 - 2*f80 - 
        2*f81 + 8*f82 - 2*f83 - f84 + 8*f91 + 16*f109 - 
        2*f110 + 2*f121 - 16*f122 + 2*f123 + 16*f124 + 2*f125 - 
        2*f126 + 16*f160 - 2*f161 + 2*f175)/9 + 
      ((-f95 + 4*f98)*fvu1u1u8^2)/18 - (f95*fvu1u1u9^2)/6 - 
      (2*(f95 - 3*f98)*fvu2u1u7)/9 + (2*f98*fvu2u1u8)/9 + 
      (2*(f95 - f98)*fvu2u1u10)/9 + ((-f95 + 2*f98)*tci11^2)/
       9 + ((6*f95 - 6*f146 + f164)*tci12)/9 + 
      (2*(f95 - f98)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*((6*f95 - 6*f146 + f164)/9 - (f95*tci12)/3) + 
      fvu1u1u8*((-6*f20 + f51 - 6*f77)/9 + 
        ((-f95 - 2*f98)*tci12)/9)) + 
    fvu1u1u1*((-4*f95*fvu1u1u2^2)/9 + ((-f95 + 2*f98)*fvu1u1u3^2)/
       6 - (2*(f95 - f98)*fvu1u1u4^2)/9 + 
      ((-f95 - 2*f98)*fvu1u1u5^2)/6 - (f95*fvu1u1u7^2)/6 + 
      ((-7*f95 + 4*f98)*fvu1u1u8^2)/18 + (f95*fvu1u1u9^2)/18 - 
      (2*f98*fvu2u1u1)/3 + (2*(f95 - f98)*fvu2u1u2)/9 + 
      (2*(4*f95 - f98)*fvu2u1u9)/9 - (4*f95*fvu2u1u11)/9 + 
      (2*(5*f95 + 4*f98)*tci11^2)/27 + (5*f95*fvu1u1u9*tci12)/
       9 + fvu1u1u2*((2*f95*fvu1u1u4)/9 + (2*f95*fvu1u1u8)/9 + 
        (2*f95*fvu1u1u9)/9 - (2*f95*tci12)/9) + 
      fvu1u1u7*((f95*fvu1u1u9)/3 + (2*f95*tci12)/3) + 
      fvu1u1u8*((-2*f95*fvu1u1u9)/9 + ((7*f95 - 4*f98)*tci12)/
         9) + fvu1u1u3*((f95*fvu1u1u4)/3 + 
        ((f95 - 2*f98)*tci12)/3) + fvu1u1u4*
       (((-f95 - 2*f98)*fvu1u1u8)/9 - (2*f95*fvu1u1u9)/9 + 
        (4*(f95 - f98)*tci12)/9)) + 
    fvu1u1u6*(-(f98*fvu1u1u8^2)/18 + (2*f98*fvu2u1u8)/9 - 
      (2*f98*tci11^2)/27 + (f98*fvu1u1u8*tci12)/9 + 
      fvu1u1u7*((-2*f98*fvu1u1u8)/9 + (2*f98*tci12)/9)) + 
    fvu1u1u3*(-(f98*fvu1u1u4^2)/6 + ((f95 + 2*f98)*fvu1u1u5^2)/
       6 + (f98*fvu1u1u7^2)/6 + ((f95 - f98)*fvu1u1u8^2)/6 - 
      (2*f98*fvu2u1u1)/3 + ((-f95 - 2*f98)*fvu2u1u5)/3 + 
      (2*(3*f95 - f98)*tci11^2)/9 + 
      ((-f95 - 2*f98)*fvu1u1u5*tci12)/3 + 
      ((-f95 - f98)*fvu1u1u7*tci12)/3 + 
      ((-f95 + f98)*fvu1u1u8*tci12)/3 + 
      ((f95 + 2*f98)*fvu1u2u7*tci12)/3 + 
      fvu1u1u4*(((-f95 + f98)*fvu1u1u8)/3 + (f98*tci12)/3)) + 
    fvu1u1u2*((11*f3 - f4 - f22 - 8*f34 + f36 + f38 - 8*f48 + 
        f50 + 19*f75 - 2*f76 - f81 - 10*f83 - 8*f85 + 8*f86 + 
        f87 - 8*f89 - 8*f91 + 2*f106 + 8*f109 - f110 + f121 + 
        8*f122 - f123 - f125 + 8*f147 - f148 + 8*f153 - f155 - 
        8*f160 + f161 + 8*f173 - f174 + 2*f175 + 8*f178 - 
        f179 - 8*f180 + f181)/9 + (f98*fvu1u1u3^2)/6 - 
      (2*(2*f95 - f98)*fvu1u1u4^2)/9 + 
      ((-2*f95 + 3*f98)*fvu1u1u5^2)/9 + (f98*fvu1u1u7^2)/6 - 
      (f95*fvu1u1u8^2)/9 + ((-5*f95 + 2*f98)*fvu1u1u9^2)/9 + 
      (2*(3*f95 - f98)*fvu2u1u3)/9 + (2*(f95 - 3*f98)*fvu2u1u4)/
       9 + (2*(5*f95 - f98)*fvu2u1u11)/9 - 
      (2*(f95 - 3*f98)*tci11^2)/27 + 
      ((6*f61 - f63 + 6*f101)*tci12)/9 + 
      (2*(3*f95 - f98)*fvu1u2u6*tci12)/9 + 
      fvu1u1u5*(-f103/9 - (2*f95*tci12)/9) + 
      fvu1u1u8*((-6*f61 + f63 - 6*f101)/9 - (2*f95*fvu1u1u9)/9 + 
        (2*f95*tci12)/9) + fvu1u1u4*((2*f95*fvu1u1u5)/9 - 
        (2*f95*fvu1u1u8)/9 - (2*(2*f95 + f98)*fvu1u1u9)/9 + 
        (2*(f95 - f98)*tci12)/9) + fvu1u1u3*
       (-(f98*fvu1u1u7)/3 - (f98*tci12)/3) + 
      fvu1u1u7*((6*f94 - f96 - 6*f98)/9 + (f98*tci12)/3) + 
      fvu1u1u9*(-f103/9 + (2*(2*f95 + f98)*tci12)/9)) + 
    fvu1u1u5*((8*f48 - f50 - 8*f86 - 8*f109 + f110 - 8*f147 + 
        f148)/9 - (f98*fvu1u1u6^2)/6 + ((f95 - 2*f98)*fvu1u1u7^2)/
       6 + ((f95 - 3*f98)*fvu1u1u8^2)/18 + 
      ((f95 - f98)*fvu1u1u9^2)/6 + (2*(f95 - 3*f98)*fvu2u1u4)/9 + 
      ((-f95 - 2*f98)*fvu2u1u5)/3 - (2*(f95 - 3*f98)*fvu2u1u7)/
       9 + ((13*f95 - 54*f98)*tci11^2)/108 + 
      ((-6*f20 + f51 - 6*f77)*tci12)/9 + 
      ((f95 + 2*f98)*fvu1u2u7*tci12)/3 + 
      fvu1u1u7*((-6*f94 + f96 + 6*f98)/9 - (f95*fvu1u1u8)/9 + 
        (f95*tci12)/9) + fvu1u1u9*(f103/9 + 
        ((f95 - f98)*tci12)/3) + fvu1u1u6*((f98*fvu1u1u8)/3 - 
        (f98*tci12)/3) + fvu1u1u8*((6*f20 - f51 + 6*f77)/9 + 
        ((-f95 + 3*f98)*tci12)/9)) + 
    fvu1u1u4*(((f95 - 3*f98)*fvu1u1u5^2)/18 + 
      ((4*f95 - f98)*fvu1u1u8^2)/18 + ((7*f95 - f98)*fvu1u1u9^2)/
       18 + (2*(f95 - f98)*fvu2u1u2)/9 + 
      (2*(3*f95 - f98)*fvu2u1u3)/9 + (4*f95*fvu2u1u11)/9 - 
      (f95*tci11^2)/27 + ((-7*f95 + f98)*fvu1u1u9*tci12)/9 + 
      (2*(3*f95 - f98)*fvu1u2u6*tci12)/9 + 
      fvu1u1u8*((2*f95*fvu1u1u9)/9 + ((-4*f95 + f98)*tci12)/
         9) + fvu1u1u5*(((-f95 + f98)*fvu1u1u9)/3 + 
        ((-f95 + 3*f98)*tci12)/9)) + 
    ((29*f95 - 8*f98)*tci12*tcr11^2)/18 + 
    ((-9*f95 + 4*f98)*tcr11^3)/18 + 
    ((3*f77 + 16*f95 + 4*f98 - 3*f101)*tci11^2*tcr12)/18 + 
    (2*(13*f95 - 10*f98)*tci12*tcr21)/9 + 
    tcr11*((-4*(f95 + 2*f98)*tci11^2)/27 - 
      (2*f95*tci12*tcr12)/3 - (10*(f95 - f98)*tcr21)/9) + 
    ((9*f77 - 26*f95 + 12*f98 - 9*f101 - 12*f102)*tcr33)/36;
L ieu1ueu11 = (-10*f3 + f4 - f35 - f75 + 10*f83 - 
      f106 - f121)/9 + ((f98 + f101 + 2*f102)*fvu1u1u2^2)/6 + 
    ((-f77 - f98)*fvu1u1u5^2)/6 + ((f77 - f95)*fvu1u1u7^2)/6 + 
    ((f95 + f101)*fvu1u1u8^2)/6 + 
    fvu1u1u8*(f89/3 - (f95*fvu1u1u9)/3) + 
    ((f98 + f102)*fvu2u1u4)/3 + ((-f77 - f98)*fvu2u1u7)/3 + 
    ((f77 - f95)*fvu2u1u10)/3 + ((f101 + f102)*fvu2u1u11)/3 + 
    ((f95 + f101)*fvu2u1u12)/3 + ((4*f95 - f98 + f101 + f102)*
      tci11^2)/18 - (f89*tci12)/3 + 
    ((f77 - f95)*fvu1u2u8*tci12)/3 + 
    ((-f95 - f101)*fvu1u2u9*tci12)/3 + 
    fvu1u1u5*(-f86/3 + (f98*fvu1u1u7)/3 + (f77*fvu1u1u8)/3 + 
      (f102*fvu1u1u9)/3 - (f77*tci12)/3) + 
    fvu1u1u9*(f79/3 + (f95*tci12)/3) + 
    fvu1u1u7*(f91/3 - (f77*fvu1u1u8)/3 + (f95*fvu1u1u9)/3 + 
      (f95*tci12)/3) + fvu1u1u2*
     ((-f79 - f83 + f86 - f89 - f91)/3 - (f102*fvu1u1u5)/3 - 
      (f98*fvu1u1u7)/3 - (f101*fvu1u1u8)/3 - (f102*fvu1u1u9)/3 + 
      (f101*tci12)/3);
L ieu0ueu11 = f83/3;
L ieum1ueu11 = 0;
L ieum2ueu11 = 0;
L ieu2uou11 = -2*f93*fvu4u28 - 2*f93*fvu4u39 - 
    (2*f93*fvu4u51)/3 - 2*f93*fvu4u80 - 2*f93*fvu4u83 - 
    2*f93*fvu4u91 - 2*f93*fvu4u93 + (2*f93*fvu4u100)/3 - 
    (2*f93*fvu4u102)/3 + 2*f93*fvu4u139 - (2*f93*fvu4u148)/3 - 
    (3*f93*fvu4u171)/4 + (11*f93*fvu4u174)/4 + (11*f93*fvu4u182)/4 - 
    (3*f93*fvu4u184)/4 + (7*f93*fvu4u192)/6 - (f93*fvu4u199)/4 - 
    (f93*fvu4u201)/2 - f93*fvu4u202 - 4*f93*fvu4u213 + 
    2*f93*fvu4u215 - 2*f93*fvu4u221 + (2*f93*fvu4u225)/3 - 
    f93*fvu4u244 + (3*f93*fvu4u252)/2 - (f93*fvu4u255)/6 + 
    (11*f93*fvu4u271)/4 + (9*f93*fvu4u273)/4 - (f93*fvu4u277)/4 + 
    (3*f93*fvu4u279)/4 - (2*f93*fvu4u282)/3 + (f93*fvu4u289)/4 + 
    (f93*fvu4u290)/2 + f93*fvu4u291 + 2*f93*fvu4u295 + 
    2*f93*fvu4u325 - 2*f93*fvu4u330 + 
    fvu3u25*(f93*fvu1u1u1 - f93*fvu1u1u3 + f93*fvu1u1u6 - 
      f93*fvu1u1u7) + fvu3u71*(f93*fvu1u1u3 - f93*fvu1u1u6 + 
      f93*fvu1u1u7) + fvu3u81*(f93*fvu1u1u2 + f93*fvu1u1u5 + 
      f93*fvu1u1u8) + fvu3u45*(-(f93*fvu1u1u2) - f93*fvu1u1u3 + 
      f93*fvu1u1u9) + fvu3u43*(-(f93*fvu1u1u2) - f93*fvu1u1u6 + 
      f93*fvu1u1u7 + f93*fvu1u1u9) + 
    fvu3u82*(-(f93*fvu1u1u5) - f93*fvu1u1u6 + f93*fvu1u1u7 - 
      f93*fvu1u1u8 + f93*fvu1u1u9) + 
    fvu3u63*(-(f93*fvu1u1u6) - f93*fvu1u1u7 - f93*fvu1u1u9 + 
      2*f93*fvu1u1u10) - (151*f93*tci11^3*tci12)/135 + 
    fvu3u78*(-(f93*fvu1u1u2) + 2*f93*fvu1u1u3 - f93*fvu1u1u5 + 
      f93*fvu1u1u8 - 2*f93*tci12) + 
    fvu3u80*(f93*fvu1u1u3 - f93*fvu1u1u5 + (f93*fvu1u1u6)/2 - 
      (f93*fvu1u1u7)/2 + (f93*fvu1u1u8)/2 - (3*f93*fvu1u1u9)/2 - 
      (3*f93*tci12)/2) - (2*f93*tci11^2*tci21)/9 + 
    fvu1u1u1*(f93*fvu3u45 - f93*fvu3u71 - f93*fvu3u78 - 
      f93*fvu3u81 + f93*fvu3u82 + (8*f93*tci11^3)/27 - 
      8*f93*tci12*tci21) + fvu1u1u3*((-4*f93*tci11^3)/27 + 
      4*f93*tci12*tci21) + fvu1u1u5*((-4*f93*tci11^3)/27 + 
      4*f93*tci12*tci21) + fvu1u1u8*((-5*f93*tci11^3)/27 + 
      5*f93*tci12*tci21) + tci12*((-48*f93*tci31)/5 - 
      16*f93*tci32) - (4*f93*tci41)/3 + 
    ((-4*f93*tci11^3)/9 + 8*f93*tci12*tci21)*tcr11 + 
    (f93*tci11*tci12*tcr11^2)/5 + 
    fvu1u1u10*((11*f93*tci11^3)/135 + (4*f93*tci12*tci21)/
       3 + (48*f93*tci31)/5 + 4*f93*tci21*tcr11 - 
      (f93*tci11*tcr11^2)/5) + 
    fvu1u1u2*(f93*fvu3u63 + f93*fvu3u80 + (31*f93*tci11^3)/
       270 - (4*f93*tci12*tci21)/3 + (24*f93*tci31)/5 + 
      2*f93*tci21*tcr11 - (f93*tci11*tcr11^2)/10) + 
    fvu1u1u6*(-(f93*tci11^3)/270 - (5*f93*tci12*tci21)/3 - 
      (24*f93*tci31)/5 - 2*f93*tci21*tcr11 + 
      (f93*tci11*tcr11^2)/10) + 
    fvu1u1u7*((-7*f93*tci11^3)/90 + (f93*tci12*tci21)/3 - 
      (24*f93*tci31)/5 - 2*f93*tci21*tcr11 + 
      (f93*tci11*tcr11^2)/10) + 
    fvu1u1u9*((-7*f93*tci11^3)/90 + (f93*tci12*tci21)/3 - 
      (24*f93*tci31)/5 - 2*f93*tci21*tcr11 + 
      (f93*tci11*tcr11^2)/10) + 
    (40*f93*tci12*tci21*tcr12)/3 + 4*f93*tci11*tci12*
     tcr12^2;
L ieu1uou11 = 2*f93*fvu3u81 - 
    (4*f93*tci11^3)/27 + 4*f93*tci12*tci21;
L ieu0uou11 = 0;
L ieum1uou11 = 0;
L ieum2uou11 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou11+w^2*ieu1uou11+w^3*ieu0uou11+w^4*ieum1uou11+w^5*ieum2uou11;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou11a = K[w^1];
L ieu1uou11a = K[w^2];
L ieu0uou11a = K[w^3];
L ieum1uou11a = K[w^4];
L ieum2uou11a = K[w^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_odd.c> "%O"
#write <e11_odd.c> "return Eps5o2<T>("
#write <e11_odd.c> "%E", ieu2uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu0uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum2uou11a
#write <e11_odd.c> ");\n}"
L H=+u^1*ieu2ueu11+u^2*ieu1ueu11+u^3*ieu0ueu11+u^4*ieum1ueu11+u^5*ieum2ueu11;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu11a = H[u^1];
L ieu1ueu11a = H[u^2];
L ieu0ueu11a = H[u^3];
L ieum1ueu11a = H[u^4];
L ieum2ueu11a = H[u^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_even.c> "%O"
#write <e11_even.c> "return Eps5o2<T>("
#write <e11_even.c> "%E", ieu2ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu0ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum2ueu11a
#write <e11_even.c> ");\n}"
.end
