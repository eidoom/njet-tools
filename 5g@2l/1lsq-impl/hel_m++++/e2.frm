#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f67;
S f65;
S f63;
S f61;
S f69;
S f71;
S f121;
S f72;
S f19;
S f3;
S f1;
S f83;
S f4;
S f22;
S f38;
S f48;
S f31;
S f46;
S f34;
S f36;
S f43;
S f59;
S f53;
S f50;
S f106;
S f57;
S f55;
S fvu1u2u9;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu2u1u9;
S fvu3u6;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u279;
S fvu4u313;
S fvu4u91;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u41;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu2 = (99*f3 - 11*f4 + 11*f22 - 63*f38 - 11*f83 + 
      11*f106 + 11*f121)/27 - (f59*fvu3u1)/3 + (f59*fvu3u2)/6 + 
    (f59*fvu3u3)/9 - (f59*fvu3u4)/9 + ((-3*f31 + f59)*fvu3u5)/18 - 
    (2*f59*fvu3u6)/9 + (f61*fvu3u7)/6 + (f69*fvu3u10)/6 - 
    (2*f59*fvu3u11)/9 + ((-f59 + 3*f65)*fvu3u12)/18 - 
    (f59*fvu3u13)/9 - (f59*fvu3u14)/9 - (f59*fvu3u15)/6 + 
    (f59*fvu3u17)/6 - (f59*fvu3u18)/6 - (f59*fvu3u19)/9 + 
    (f59*fvu3u22)/3 - (7*f59*fvu1u1u1^3)/18 + (f59*fvu1u1u2^3)/3 + 
    (f59*fvu1u1u3^3)/18 - (f59*fvu1u1u5^3)/9 + 
    ((3*f34 - 3*f38 + 3*f43 + 3*f48 + 3*f53 + f63 + f71)*
      fvu1u1u8^2)/18 + fvu1u1u4^2*(-f48/6 + (f59*fvu1u1u5)/9 - 
      (f59*fvu1u1u8)/18 + (f59*fvu1u1u9)/18) + 
    ((-6*f1 + f19 + 6*f59 + f67)*fvu2u1u2)/9 + 
    ((-6*f1 + f19 + 6*f59 - f72)*fvu2u1u3)/9 + 
    ((f63 - f72)*fvu2u1u11)/9 + ((f63 + f71)*fvu2u1u12)/9 - 
    (f59*fvu1u2u7*tci11^2)/3 + 
    ((9*f3 - f4 + f22 - 9*f38 + f46 + f55 - f83 + f106 + 
       f121)*tci12)/9 + ((-6*f1 + f19 + 6*f59 - f72)*fvu1u2u6*
      tci12)/9 + ((-f63 - f71)*fvu1u2u9*tci12)/9 + 
    (8*f59*fvu2u1u1*tci12)/9 + (f59*fvu2u1u5*tci12)/3 + 
    fvu1u1u8*((-9*f3 + f4 - f22 - f36 + 9*f38 - f46 - f50 - 
        f55 + f83 - f106 - f121)/9 - (f71*fvu1u1u9)/9 - 
      (2*f59*fvu2u1u9)/9 - (f59*tci11^2)/54 + 
      ((-f34 + f38 - f43 - f48 - f53)*tci12)/3) + 
    fvu1u1u3^2*(-(f59*fvu1u1u4)/9 - (f59*fvu1u1u8)/6 - 
      (f59*tci12)/2) + fvu1u1u5^2*((f59*fvu1u1u9)/6 - 
      (f59*tci12)/9) + fvu1u1u9^2*(-f43/6 - (f59*tci12)/18) + 
    fvu1u1u2^2*((-6*f1 + f19 - 3*f53 + 6*f59 + f63 - 2*f72)/
       18 - (f59*fvu1u1u4)/6 - (f59*fvu1u1u5)/18 - 
      (f59*fvu1u1u9)/9 + (f59*tci12)/6) + 
    fvu2u1u9*((f67 - f71)/9 + (4*f59*tci12)/9) + 
    fvu1u1u1^2*((-6*f1 + f19 - 3*f34 + 6*f59 + 2*f67 - f71)/
       18 - (f59*fvu1u1u2)/6 + (f59*fvu1u1u3)/18 + 
      (f59*fvu1u1u4)/6 + (f59*fvu1u1u8)/9 + (5*f59*tci12)/6) + 
    tci11^2*((-12*f1 + 2*f19 - 21*f38 + 18*f43 + 18*f53 + 
        12*f59 + 2*f63 + 2*f67)/108 + 
      ((f31 + 4*f59 + f61 + 2*f65 - 3*f69)*tci12)/36) + 
    fvu1u1u9*(f46/9 + (2*f59*fvu2u1u11)/9 + (2*f59*tci11^2)/
       27 + (f72*tci12)/9) + fvu1u1u5*((f59*fvu1u1u9^2)/6 + 
      (4*f59*fvu2u1u4)/9 - (f59*fvu2u1u5)/3 + 
      (13*f59*tci11^2)/108 + (f59*fvu1u1u9*tci12)/3 + 
      (f59*fvu1u2u7*tci12)/3) + 
    fvu1u1u4*(f50/9 + (f59*fvu1u1u5^2)/9 - (f59*fvu1u1u8^2)/18 + 
      (f59*fvu1u1u9^2)/18 + (f48*tci12)/3 + 
      fvu1u1u5*(-(f59*fvu1u1u9)/3 - (2*f59*tci12)/9) + 
      fvu1u1u9*(-f72/9 - (f59*tci12)/9) + 
      fvu1u1u8*(f67/9 + (f59*tci12)/9)) + 
    fvu1u1u3*(-(f59*fvu1u1u4^2)/9 + (f59*fvu1u1u5^2)/6 - 
      (f59*fvu1u1u8^2)/6 - (4*f59*fvu2u1u1)/9 - 
      (f59*fvu2u1u5)/3 + (17*f59*tci11^2)/27 - 
      (f59*fvu1u1u5*tci12)/3 + (f59*fvu1u1u8*tci12)/3 + 
      (f59*fvu1u2u7*tci12)/3 + fvu1u1u4*((f59*fvu1u1u8)/3 + 
        (2*f59*tci12)/9)) + fvu1u1u1*
     (f36/9 + (5*f59*fvu1u1u3^2)/18 + (f59*fvu1u1u4^2)/6 - 
      (f59*fvu1u1u5^2)/6 + (2*f59*fvu1u1u8^2)/9 + 
      (f71*fvu1u1u9)/9 - (4*f59*fvu2u1u1)/9 - 
      (2*f59*fvu2u1u9)/9 - (f59*tci11^2)/3 + 
      ((6*f1 - f19 + 3*f34 - 6*f59 + f71)*tci12)/9 + 
      fvu1u1u3*(-(f59*fvu1u1u4)/9 - (5*f59*tci12)/9) + 
      fvu1u1u8*(-f67/9 - (4*f59*tci12)/9) + 
      fvu1u1u4*(-f67/9 - (2*f59*fvu1u1u8)/9 - (f59*tci12)/3) + 
      fvu1u1u2*((6*f1 - f19 - 6*f59)/9 + (f59*tci12)/3)) + 
    fvu1u1u2*(f55/9 + (f59*fvu1u1u3^2)/6 - (f59*fvu1u1u4^2)/6 - 
      (5*f59*fvu1u1u5^2)/18 - (f63*fvu1u1u8)/9 - 
      (2*f59*fvu1u1u9^2)/9 + (4*f59*fvu2u1u4)/9 + 
      (2*f59*fvu2u1u11)/9 - (f59*tci11^2)/9 + (f63*tci12)/9 - 
      (f59*fvu1u1u3*tci12)/3 - (f59*fvu1u1u5*tci12)/9 + 
      fvu1u1u9*(f72/9 - (2*f59*tci12)/9) + 
      fvu1u1u4*(f72/9 + (f59*fvu1u1u5)/9 + (2*f59*fvu1u1u9)/9 + 
        (f59*tci12)/3)) + ((-2*f59 + 2*f65 - 3*f69)*tci12*
      tcr11^2)/6 + ((6*f59 - 4*f65 + f69)*tcr11^3)/18 + 
    ((-6*f31 + 19*f59 - 6*f61 - 2*f65 - 7*f69)*tci11^2*
      tcr12)/36 + ((-f59 + 2*f65 + f69)*tcr12^3)/9 - 
    (2*(f59 - f65 + f69)*tci12*tcr21)/3 + 
    tcr11*(((5*f59 + 7*f69)*tci11^2)/36 + 
      ((-f59 + f69)*tci12*tcr12)/3 + 
      (2*(f59 - f65)*tcr21)/3) + 
    ((f59 - 2*f65 - f69)*tcr31)/3 + 
    ((f59 - 2*f65 - f69)*tcr32)/12 + 
    ((-18*f31 - 11*f59 - 18*f61 + 74*f65 + 11*f69)*tcr33)/72;
L ieu1ueu2 = (9*f3 - f4 + f22 - 9*f38 - f83 + f106 + 
      f121)/9 + ((f59 + 2*f65 - f69)*fvu1u1u1^2)/6 + 
    ((-2*f31 + f59 + f61)*fvu1u1u2^2)/6 + 
    ((f61 + f69)*fvu1u1u8^2)/6 + 
    fvu1u1u4*(f48/3 + (f65*fvu1u1u8)/3 - (f31*fvu1u1u9)/3) + 
    fvu1u1u8*((-f34 + f38 - f43 - f48 - f53)/3 - 
      (f69*fvu1u1u9)/3) + ((f59 + f65)*fvu2u1u2)/3 + 
    ((-f31 + f59)*fvu2u1u3)/3 + ((f65 - f69)*fvu2u1u9)/3 + 
    ((-f31 + f61)*fvu2u1u11)/3 + ((f61 + f69)*fvu2u1u12)/3 + 
    ((f59 + f61 + f65)*tci11^2)/18 + 
    ((-f38 + f43 + f53)*tci12)/3 + 
    ((-f31 + f59)*fvu1u2u6*tci12)/3 + 
    ((-f61 - f69)*fvu1u2u9*tci12)/3 + 
    fvu1u1u9*(f43/3 + (f31*tci12)/3) + 
    fvu1u1u2*(f53/3 + (f31*fvu1u1u4)/3 - (f61*fvu1u1u8)/3 + 
      (f31*fvu1u1u9)/3 + (f61*tci12)/3) + 
    fvu1u1u1*(f34/3 - (f59*fvu1u1u2)/3 - (f65*fvu1u1u4)/3 - 
      (f65*fvu1u1u8)/3 + (f69*fvu1u1u9)/3 + 
      ((-f59 + f69)*tci12)/3);
L ieu0ueu2 = -f38/3;
L ieum1ueu2 = 0;
L ieum2ueu2 = 0;
L ieu2uou2 = (-4*f57*fvu4u25)/3 + (10*f57*fvu4u28)/3 - 
    (2*f57*fvu4u39)/3 - (4*f57*fvu4u41)/3 + (4*f57*fvu4u49)/9 + 
    (2*f57*fvu4u51)/3 - (2*f57*fvu4u80)/3 - (8*f57*fvu4u83)/3 + 
    2*f57*fvu4u91 - (2*f57*fvu4u93)/3 + (2*f57*fvu4u100)/9 - 
    (4*f57*fvu4u102)/9 - (2*f57*fvu4u139)/3 + (2*f57*fvu4u148)/9 + 
    (17*f57*fvu4u171)/12 - (3*f57*fvu4u174)/4 - (3*f57*fvu4u182)/4 + 
    (17*f57*fvu4u184)/12 - (17*f57*fvu4u192)/18 + (f57*fvu4u199)/4 + 
    (f57*fvu4u201)/2 + f57*fvu4u202 + (2*f57*fvu4u213)/3 - 
    (2*f57*fvu4u215)/3 + (2*f57*fvu4u221)/3 + 5*f57*fvu4u244 + 
    (8*f57*fvu4u246)/3 - 2*f57*fvu4u250 - (13*f57*fvu4u252)/6 - 
    (f57*fvu4u255)/2 + (7*f57*fvu4u271)/12 - (43*f57*fvu4u273)/12 + 
    (43*f57*fvu4u277)/12 + (7*f57*fvu4u279)/12 - 
    (2*f57*fvu4u282)/3 - (f57*fvu4u289)/4 - (f57*fvu4u290)/2 - 
    f57*fvu4u291 - (2*f57*fvu4u293)/3 - (2*f57*fvu4u295)/3 + 
    2*f57*fvu4u313 + fvu3u71*((2*f57*fvu1u1u2)/3 - 
      (2*f57*fvu1u1u5)/3 - (2*f57*fvu1u1u8)/3) + 
    fvu3u23*((2*f57*fvu1u1u2)/3 + (2*f57*fvu1u1u6)/3 - 
      (2*f57*fvu1u1u7)/3 - (2*f57*fvu1u1u9)/3) + 
    fvu3u43*(-(f57*fvu1u1u6)/3 + (f57*fvu1u1u7)/3 + 
      (f57*fvu1u1u9)/3) + fvu3u63*((f57*fvu1u1u6)/3 + 
      (f57*fvu1u1u7)/3 + (f57*fvu1u1u9)/3 - (2*f57*fvu1u1u10)/
       3) + fvu3u45*(-(f57*fvu1u1u3)/3 + (2*f57*fvu1u1u5)/3 - 
      (4*f57*fvu1u1u6)/3 + (5*f57*fvu1u1u7)/3 - f57*fvu1u1u8 - 
      (2*f57*fvu1u1u9)/3 - (f57*fvu1u1u10)/3) + 
    fvu3u70*((f57*fvu1u1u3)/3 + (f57*fvu1u1u6)/3 - 
      (f57*fvu1u1u8)/3 + (f57*fvu1u1u9)/3 - (f57*fvu1u1u10)/3) + 
    fvu3u81*((2*f57*fvu1u1u3)/3 + (f57*fvu1u1u4)/3 + 
      (2*f57*fvu1u1u6)/3 - (2*f57*fvu1u1u7)/3 - 
      (f57*fvu1u1u9)/3 + (f57*fvu1u1u10)/3) - 
    (13*f57*tci11^3*tci12)/15 + 
    fvu3u25*((-2*f57*fvu1u1u1)/3 - (f57*fvu1u1u2)/3 - 
      (f57*fvu1u1u3)/3 + 2*f57*fvu1u1u4 - (2*f57*fvu1u1u5)/3 - 
      f57*fvu1u1u6 + (2*f57*fvu1u1u7)/3 + f57*fvu1u1u8 + 
      f57*fvu1u1u9 + (f57*fvu1u1u10)/3 - 2*f57*tci12) + 
    fvu3u82*((f57*fvu1u1u3)/3 + (f57*fvu1u1u4)/3 - 
      (f57*fvu1u1u7)/3 + (f57*fvu1u1u8)/3 - (f57*fvu1u1u9)/3 - 
      (2*f57*tci12)/3) + fvu3u78*((f57*fvu1u1u2)/3 - 
      (f57*fvu1u1u3)/3 - (f57*fvu1u1u4)/3 + (f57*fvu1u1u6)/3 - 
      (f57*fvu1u1u8)/3 + (2*f57*tci12)/3) + 
    fvu3u80*((f57*fvu1u1u2)/3 - (f57*fvu1u1u3)/3 - 
      (f57*fvu1u1u4)/3 + (2*f57*fvu1u1u5)/3 - 
      (11*f57*fvu1u1u6)/6 + (13*f57*fvu1u1u7)/6 - 
      (5*f57*fvu1u1u8)/2 + (13*f57*fvu1u1u9)/6 + 
      (7*f57*tci12)/2) + (10*f57*tci11^2*tci21)/27 + 
    fvu1u1u10*((-4*f57*tci11^3)/81 + (4*f57*tci12*tci21)/
       3) + tci12*((-16*f57*tci31)/5 - 16*f57*tci32) + 
    (2072*f57*tci41)/135 + (64*f57*tci42)/5 + 
    (16*f57*tci43)/3 + ((-217*f57*tci11^3)/405 - 
      (4*f57*tci12*tci21)/3 - 16*f57*tci32)*tcr11 - 
    (13*f57*tci11*tcr11^3)/45 + 
    fvu1u1u9*((83*f57*tci11^3)/270 - 3*f57*tci12*tci21 + 
      (72*f57*tci31)/5 + 6*f57*tci21*tcr11 - 
      (3*f57*tci11*tcr11^2)/10) + 
    fvu1u1u4*((11*f57*tci11^3)/135 + (4*f57*tci12*tci21)/
       3 + (48*f57*tci31)/5 + 4*f57*tci21*tcr11 - 
      (f57*tci11*tcr11^2)/5) + 
    fvu1u1u8*((-4*f57*tci11^3)/135 + (13*f57*tci12*tci21)/
       3 + (48*f57*tci31)/5 + 4*f57*tci21*tcr11 - 
      (f57*tci11*tcr11^2)/5) + 
    fvu1u1u7*((161*f57*tci11^3)/810 - (43*f57*tci12*tci21)/
       9 + (8*f57*tci31)/5 + (2*f57*tci21*tcr11)/3 - 
      (f57*tci11*tcr11^2)/30) + 
    fvu1u1u6*((-161*f57*tci11^3)/810 + (43*f57*tci12*tci21)/
       9 - (8*f57*tci31)/5 - (2*f57*tci21*tcr11)/3 + 
      (f57*tci11*tcr11^2)/30) + 
    fvu1u1u5*((2*f57*tci11^3)/45 - (32*f57*tci12*tci21)/9 - 
      (32*f57*tci31)/5 - (8*f57*tci21*tcr11)/3 + 
      (2*f57*tci11*tcr11^2)/15) + 
    fvu1u1u2*(-(f57*fvu3u43)/3 - (f57*fvu3u63)/3 - 
      (f57*fvu3u70)/3 - (23*f57*tci11^3)/162 + 
      (8*f57*tci12*tci21)/9 - 8*f57*tci31 - 
      (10*f57*tci21*tcr11)/3 + (f57*tci11*tcr11^2)/6) + 
    fvu1u1u1*((4*f57*fvu3u45)/3 + (2*f57*fvu3u71)/3 + 
      (f57*fvu3u78)/3 - (f57*fvu3u80)/3 - f57*fvu3u81 - 
      (f57*fvu3u82)/3 - (53*f57*tci11^3)/405 - 
      (48*f57*tci31)/5 - 4*f57*tci21*tcr11 + 
      (f57*tci11*tcr11^2)/5) + 
    ((218*f57*tci11^3)/243 + (40*f57*tci12*tci21)/3)*
     tcr12 + (4*f57*tci11*tci12 + (40*f57*tci21)/3)*
     tcr12^2 + tcr11^2*((f57*tci11*tci12)/15 + 
      2*f57*tci21 + 2*f57*tci11*tcr12) - 
    (130*f57*tci11*tcr33)/27;
L ieu1uou2 = 
   2*f57*fvu3u25 + (11*f57*tci11^3)/135 + 
    (4*f57*tci12*tci21)/3 + (48*f57*tci31)/5 + 
    4*f57*tci21*tcr11 - (f57*tci11*tcr11^2)/5;
L ieu0uou2 = 0;
L ieum1uou2 = 0;
L ieum2uou2 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou2+w^2*ieu1uou2+w^3*ieu0uou2+w^4*ieum1uou2+w^5*ieum2uou2;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou2a = K[w^1];
L ieu1uou2a = K[w^2];
L ieu0uou2a = K[w^3];
L ieum1uou2a = K[w^4];
L ieum2uou2a = K[w^5];
.sort
#write <e2.tmp> "`optimmaxvar_'"
#write <e2_odd.c> "%O"
#write <e2_odd.c> "return Eps5o2<T>("
#write <e2_odd.c> "%E", ieu2uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieu1uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieu0uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieum1uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieum2uou2a
#write <e2_odd.c> ");\n}"
L H=+u^1*ieu2ueu2+u^2*ieu1ueu2+u^3*ieu0ueu2+u^4*ieum1ueu2+u^5*ieum2ueu2;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu2a = H[u^1];
L ieu1ueu2a = H[u^2];
L ieu0ueu2a = H[u^3];
L ieum1ueu2a = H[u^4];
L ieum2ueu2a = H[u^5];
.sort
#write <e2.tmp> "`optimmaxvar_'"
#write <e2_even.c> "%O"
#write <e2_even.c> "return Eps5o2<T>("
#write <e2_even.c> "%E", ieu2ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieu1ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieu0ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieum1ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieum2ueu2a
#write <e2_even.c> ");\n}"
.end
