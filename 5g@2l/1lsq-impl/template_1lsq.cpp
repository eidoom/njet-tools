// SSSSS
template <typename T>
std::array<LoopResult<Eps5o2<T>>, 12> Amp0q5g_a2l<T>::hAgHHe2() {
  hAgHHe2coeffs();
  const std::array<int, 5> o{0, 1, 2, 3, 4};
  const TreeValue phase{-i_ * hA0HH(o.data()) / hA0HHX()};

  std::array<Eps5o2<T>, 12> amps{
      hAgHHe2p1e() + hAgHHe2p1o(),   hAgHHe2p2e() + hAgHHe2p2o(),
      hAgHHe2p3e() + hAgHHe2p3o(),   hAgHHe2p4e() + hAgHHe2p4o(),
      hAgHHe2p5e() + hAgHHe2p5o(),   hAgHHe2p6e() + hAgHHe2p6o(),
      hAgHHe2p7e() + hAgHHe2p7o(),   hAgHHe2p8e() + hAgHHe2p8o(),
      hAgHHe2p9e() + hAgHHe2p9o(),   hAgHHe2p10e() + hAgHHe2p10o(),
      hAgHHe2p11e() + hAgHHe2p11o(), hAgHHe2p12e() + hAgHHe2p12o(),
  };

  for (Eps5o2<T> &amp : amps) {
    amp = phase * correction1Lsq(amp);
  }

  for (int i{0}; i < FFF; ++i) {
    f[i] = std::conj(f[i]);
  }

  std::array<Eps5o2<T>, 12> camps{
      hAgHHe2p1e() - hAgHHe2p1o(),   hAgHHe2p2e() - hAgHHe2p2o(),
      hAgHHe2p3e() - hAgHHe2p3o(),   hAgHHe2p4e() - hAgHHe2p4o(),
      hAgHHe2p5e() - hAgHHe2p5o(),   hAgHHe2p6e() - hAgHHe2p6o(),
      hAgHHe2p7e() - hAgHHe2p7o(),   hAgHHe2p8e() - hAgHHe2p8o(),
      hAgHHe2p9e() - hAgHHe2p9o(),   hAgHHe2p10e() - hAgHHe2p10o(),
      hAgHHe2p11e() - hAgHHe2p11o(), hAgHHe2p12e() - hAgHHe2p12o(),
  };

  const TreeValue cphase{std::conj(phase)};

  for (Eps5o2<T> &camp : camps) {
    camp = cphase * correction1Lsq(camp);
  }

  std::array<LoopResult<Eps5o2<T>>, 12> resvec;

  for (int i{0}; i < 12; ++i) {
    resvec[i] = {amps[i], camps[i]};
  }

  return resvec;
}
