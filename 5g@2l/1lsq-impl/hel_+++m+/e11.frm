#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f67;
S f66;
S f65;
S f64;
S f63;
S f132;
S f62;
S f131;
S f61;
S f130;
S f60;
S f124;
S f74;
S f125;
S f75;
S f127;
S f120;
S f70;
S f71;
S f72;
S f123;
S f73;
S f78;
S f129;
S f151;
S f150;
S f152;
S f159;
S f89;
S f18;
S f19;
S f81;
S f3;
S f80;
S f17;
S f83;
S f15;
S f84;
S f97;
S f179;
S f29;
S f178;
S f92;
S f21;
S f20;
S f27;
S f177;
S f176;
S f26;
S f175;
S f25;
S f49;
S f118;
S f117;
S f47;
S f164;
S f166;
S f58;
S f59;
S f102;
S f100;
S f101;
S f104;
S f105;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu4u279;
S fvu4u91;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u330;
S fvu4u100;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u325;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu11 = (-11*f17 + 11*f20 + 99*f59 - 11*f61 - 
      63*f70 + 11*f150 + 11*f177)/27 + ((-2*f92 - f101)*fvu3u1)/
     3 + ((2*f92 + f101)*fvu3u2)/6 + (2*f101*fvu3u3)/9 - 
    (f92*fvu3u4)/6 + ((-f92 + 11*f101)*fvu3u5)/18 + 
    ((-2*f92 - f101)*fvu3u6)/9 + ((f97 + 2*f101)*fvu3u7)/6 + 
    (16*f101*fvu3u8)/9 - (f101*fvu3u10)/6 + 
    (2*(f92 + f101)*fvu3u11)/9 + ((-f92 + 4*f101)*fvu3u12)/18 + 
    ((3*f92 - f101)*fvu3u13)/18 + (f101*fvu3u14)/3 + 
    (f92*fvu3u15)/6 + ((f62 - f92 + f101)*fvu3u17)/6 + 
    ((-f92 + f101)*fvu3u18)/6 + ((-2*f92 - f101)*fvu3u19)/9 - 
    (2*f101*fvu3u24)/9 - (f101*fvu3u31)/9 - (f92*fvu3u32)/18 + 
    (2*f92*fvu3u36)/9 + ((3*f92 - 3*f100 - f101)*fvu3u37)/18 + 
    ((f92 - f101)*fvu3u38)/6 - (f92*fvu3u40)/6 + 
    (f101*fvu3u42)/3 + ((-4*f92 + 9*f101)*fvu1u1u1^3)/18 + 
    ((-15*f92 + 11*f101)*fvu1u1u2^3)/27 + 
    ((-6*f92 + f101)*fvu1u1u3^3)/18 + ((f92 - 2*f101)*fvu1u1u5^3)/
     9 + (f92*fvu1u1u6^3)/9 + ((-f92 + f101)*fvu1u1u7^3)/9 + 
    (4*f101*fvu1u1u9^3)/27 + fvu1u1u8^2*
     ((-6*f47 + f49 + 3*f65 - 3*f70 + 3*f73 + 3*f78 - 3*f84 + 
        6*f97 + 6*f101 - 6*f164 + f166)/18 + (f101*fvu1u1u9)/9) + 
    fvu1u1u4^2*(((-3*f92 + f101)*fvu1u1u5)/18 + 
      ((-f92 + 4*f101)*fvu1u1u8)/18 + ((-f92 + 7*f101)*fvu1u1u9)/
       18) + ((-6*f80 + f83 + 6*f92 + f102)*fvu2u1u4)/9 + 
    ((6*f19 - f21 + 6*f80 - f83 - 6*f92 - 6*f100)*fvu2u1u7)/9 + 
    ((-6*f47 + f49 + 6*f97 + 6*f101 - 6*f164 + f166)*fvu2u1u12)/
     9 + (2*(f92 - 3*f101)*fvu1u2u6*tci11^2)/9 + 
    ((-2*f92 - f101)*fvu1u2u7*tci11^2)/3 + 
    ((-f3 - f17 + 8*f26 - f29 - 18*f58 + 7*f59 + 2*f60 - 
       f61 + 8*f63 - 8*f64 + 8*f65 - f66 + f67 - 9*f70 - 
       8*f71 + 8*f72 + 8*f73 + f74 - f75 + 8*f78 - 8*f84 - 
       8*f117 + f118 - 2*f120 + 8*f123 - f124 + 8*f125 - f127 + 
       8*f129 + 8*f130 - f131 - f132 - f150 + f159)*tci12)/
     9 + ((6*f47 - f49 - 6*f97 - 6*f101 + 6*f164 - f166)*
      fvu1u2u9*tci12)/9 + (4*f92*fvu2u1u1*tci12)/3 + 
    (4*(f92 - f101)*fvu2u1u2*tci12)/9 + 
    (2*(f92 - 3*f101)*fvu2u1u3*tci12)/9 + 
    ((2*f92 + f101)*fvu2u1u5*tci12)/3 + 
    (4*(f92 - 4*f101)*fvu2u1u9*tci12)/9 + 
    fvu1u1u6^2*((f92*fvu1u1u7)/9 - (f92*fvu1u1u8)/18 + 
      (f92*tci12)/18) + fvu1u1u1^2*(-(f101*fvu1u1u2)/9 - 
      (f101*fvu1u1u3)/6 + ((f92 - f101)*fvu1u1u4)/9 - 
      (f101*fvu1u1u7)/3 + ((2*f92 + f101)*fvu1u1u8)/18 - 
      (f101*fvu1u1u9)/18 + ((8*f92 - 23*f101)*tci12)/18) + 
    fvu1u1u9^2*(-f73/6 + ((f92 - 10*f101)*tci12)/18) + 
    fvu1u1u2^2*((-6*f47 + f49 - 6*f80 + f83 + 3*f84 + 6*f92 + 
        6*f97 + 2*f102)/18 + (f92*fvu1u1u3)/6 + 
      ((f92 + f101)*fvu1u1u4)/9 - (f101*fvu1u1u5)/9 + 
      (f92*fvu1u1u7)/6 + (f101*fvu1u1u8)/9 + 
      ((f92 + 2*f101)*fvu1u1u9)/9 + ((-f92 - 8*f101)*tci12)/
       18) + fvu1u1u3^2*(-(f92*fvu1u1u4)/6 + 
      ((f92 + f101)*fvu1u1u7)/6 + ((-f92 + f101)*fvu1u1u8)/6 + 
      ((2*f92 - 3*f101)*tci12)/6) + 
    fvu1u1u7^2*((-6*f19 + f21 - 3*f65 + 6*f100 - 6*f101 + 
        6*f164 - f166)/18 + ((2*f92 + f101)*fvu1u1u8)/18 - 
      (f101*fvu1u1u9)/6 + ((-f92 - 2*f101)*tci12)/18) + 
    fvu2u1u10*((-6*f19 + f21 + 6*f100 - 6*f101 + 6*f164 - f166)/
       9 + (2*(f92 - f101)*tci12)/9) + 
    fvu1u1u5^2*((6*f19 - f21 - 3*f78 + 6*f80 - f83 - 6*f92 - 
        6*f100)/18 - (f92*fvu1u1u6)/6 + (f101*fvu1u1u7)/18 + 
      ((-3*f92 + f101)*fvu1u1u8)/18 + ((-f92 + f101)*fvu1u1u9)/
       6 + ((3*f92 - f101)*tci12)/9) + 
    fvu2u1u11*((-6*f47 + f49 + 6*f97 + f102)/9 - 
      (4*f101*tci12)/9) + tci11^2*
     ((-12*f47 + 2*f49 + 18*f65 - 21*f70 + 18*f73 + 18*f78 + 
        12*f80 - 2*f83 - 18*f84 - 12*f92 + 12*f97 + 48*f101 + 
        2*f102 - 48*f164 + 8*f166)/108 + 
      ((10*f92 + 3*f97 - 3*f100 + 7*f101)*tci12)/108) + 
    fvu1u2u8*((2*(f92 - f101)*tci11^2)/9 + 
      ((-6*f19 + f21 + 6*f100 - 6*f101 + 6*f164 - f166)*
        tci12)/9) + fvu1u1u9*
     ((-3*f3 + 16*f15 - 2*f18 + 24*f25 + 8*f26 - 3*f27 - f29 - 
        16*f58 + 16*f59 + 2*f60 - 2*f61 - 16*f71 + 16*f72 + 
        16*f73 + 2*f74 - 2*f75 + 8*f78 - f81 - 8*f104 + f105 + 
        16*f123 - 2*f124 + 8*f125 - f127 + 8*f130 - f132 - 
        8*f151 + f152 + 3*f159 + 16*f175 - 2*f176 - 24*f178 + 
        3*f179)/18 - (2*(f92 - 7*f101)*fvu2u1u11)/9 - 
      (2*(f92 - 6*f101)*tci11^2)/27 + 
      ((6*f101 - 6*f164 + f166)*tci12)/9) + 
    fvu1u1u6*(-(f92*fvu1u1u8^2)/18 + (2*f92*fvu2u1u8)/9 - 
      (2*f92*tci11^2)/27 + (f92*fvu1u1u8*tci12)/9 + 
      fvu1u1u7*((-2*f92*fvu1u1u8)/9 + (2*f92*tci12)/9)) + 
    fvu1u1u3*(-(f92*fvu1u1u4^2)/6 + ((2*f92 + f101)*fvu1u1u5^2)/
       6 + (f92*fvu1u1u7^2)/6 + ((-f92 + f101)*fvu1u1u8^2)/6 - 
      (2*f92*fvu2u1u1)/3 + ((-2*f92 - f101)*fvu2u1u5)/3 - 
      (2*(f92 - 3*f101)*tci11^2)/9 + 
      ((-2*f92 - f101)*fvu1u1u5*tci12)/3 + 
      ((-f92 - f101)*fvu1u1u7*tci12)/3 + 
      ((f92 - f101)*fvu1u1u8*tci12)/3 + 
      ((2*f92 + f101)*fvu1u2u7*tci12)/3 + 
      fvu1u1u4*(((f92 - f101)*fvu1u1u8)/3 + (f92*tci12)/3)) + 
    fvu1u1u4*(((-3*f92 + f101)*fvu1u1u5^2)/18 + 
      ((-f92 + 4*f101)*fvu1u1u8^2)/18 + 
      ((-f92 + 7*f101)*fvu1u1u9^2)/18 - (2*(f92 - f101)*fvu2u1u2)/
       9 - (2*(f92 - 3*f101)*fvu2u1u3)/9 + (4*f101*fvu2u1u11)/9 - 
      (f101*tci11^2)/27 + ((f92 - 7*f101)*fvu1u1u9*tci12)/9 - 
      (2*(f92 - 3*f101)*fvu1u2u6*tci12)/9 + 
      fvu1u1u8*((2*f101*fvu1u1u9)/9 + ((f92 - 4*f101)*tci12)/
         9) + fvu1u1u5*(((f92 - f101)*fvu1u1u9)/3 + 
        ((3*f92 - f101)*tci12)/9)) + 
    fvu1u1u7*((-8*f25 + f27 + 8*f65 + 8*f104 - f105 - 8*f117 + 
        f118 + 8*f151 - f152 + 8*f178 - f179)/9 + 
      ((4*f92 - f101)*fvu1u1u8^2)/18 - (f101*fvu1u1u9^2)/6 + 
      (2*(3*f92 - f101)*fvu2u1u7)/9 + (2*f92*fvu2u1u8)/9 - 
      (2*(f92 - f101)*fvu2u1u10)/9 + ((2*f92 - f101)*tci11^2)/
       9 + ((6*f101 - 6*f164 + f166)*tci12)/9 - 
      (2*(f92 - f101)*fvu1u2u8*tci12)/9 + 
      fvu1u1u8*((6*f19 - f21 - 6*f100)/9 + 
        ((-2*f92 - f101)*tci12)/9) + 
      fvu1u1u9*((6*f101 - 6*f164 + f166)/9 - (f101*tci12)/3)) + 
    fvu1u1u8*((f3 + f17 - 8*f26 + f29 + 18*f58 - 7*f59 - 
        2*f60 + f61 - 8*f63 + 8*f64 - 8*f65 + f66 - f67 + 
        9*f70 + 8*f71 - 8*f72 - 8*f73 - f74 + f75 - 8*f78 + 
        8*f84 + 8*f117 - f118 + 2*f120 - 8*f123 + f124 - 
        8*f125 + f127 - 8*f129 - 8*f130 + f131 + f132 + f150 - 
        f159)/9 + (f101*fvu1u1u9^2)/9 - 
      (2*(f92 - 4*f101)*fvu2u1u9)/9 - (2*(f92 - f101)*fvu2u1u10)/
       9 + (4*f101*fvu2u1u11)/9 + (f101*tci11^2)/18 + 
      ((-f65 + f70 - f73 - f78 + f84)*tci12)/3 - 
      (2*(f92 - f101)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*((-6*f101 + 6*f164 - f166)/9 - (2*f101*tci12)/
         9)) + fvu1u1u5*(f81/9 - (f92*fvu1u1u6^2)/6 + 
      ((-2*f92 + f101)*fvu1u1u7^2)/6 + 
      ((-3*f92 + f101)*fvu1u1u8^2)/18 + 
      ((-f92 + f101)*fvu1u1u9^2)/6 - (2*(3*f92 - f101)*fvu2u1u4)/
       9 + ((-2*f92 - f101)*fvu2u1u5)/3 + 
      (2*(3*f92 - f101)*fvu2u1u7)/9 + 
      ((-54*f92 + 13*f101)*tci11^2)/108 + 
      ((6*f19 - f21 - 6*f100)*tci12)/9 + 
      ((2*f92 + f101)*fvu1u2u7*tci12)/3 + 
      fvu1u1u6*((f92*fvu1u1u8)/3 - (f92*tci12)/3) + 
      fvu1u1u8*((-6*f19 + f21 + 6*f100)/9 + 
        ((3*f92 - f101)*tci12)/9) + fvu1u1u7*
       ((-6*f80 + f83 + 6*f92)/9 - (f101*fvu1u1u8)/9 + 
        (f101*tci12)/9) + fvu1u1u9*(f102/9 + 
        ((-f92 + f101)*tci12)/3)) + 
    fvu1u1u2*((f3 - 16*f15 + 2*f18 - 2*f20 - 8*f25 + 8*f26 + 
        f27 - f29 - 20*f58 - 20*f59 + 2*f60 + 2*f61 + 16*f63 - 
        16*f64 - 2*f66 + 2*f67 + 8*f78 - f81 - 16*f84 - 
        8*f104 + f105 - 4*f120 + 8*f125 - f127 + 16*f129 + 
        8*f130 - 2*f131 - f132 - 4*f150 - 8*f151 + f152 - 
        f159 - 16*f175 + 2*f176 - 2*f177 + 8*f178 - f179)/18 + 
      (f92*fvu1u1u3^2)/6 + (2*(f92 - 2*f101)*fvu1u1u4^2)/9 + 
      ((3*f92 - 2*f101)*fvu1u1u5^2)/9 + (f92*fvu1u1u7^2)/6 - 
      (f101*fvu1u1u8^2)/9 + ((2*f92 - 5*f101)*fvu1u1u9^2)/9 - 
      (2*(f92 - 3*f101)*fvu2u1u3)/9 - (2*(3*f92 - f101)*fvu2u1u4)/
       9 - (2*(f92 - 5*f101)*fvu2u1u11)/9 + 
      (2*(3*f92 - f101)*tci11^2)/27 + 
      ((-6*f47 + f49 + 6*f97)*tci12)/9 - 
      (2*(f92 - 3*f101)*fvu1u2u6*tci12)/9 + 
      fvu1u1u3*(-(f92*fvu1u1u7)/3 - (f92*tci12)/3) + 
      fvu1u1u7*((6*f80 - f83 - 6*f92)/9 + (f92*tci12)/3) + 
      fvu1u1u4*((2*f101*fvu1u1u5)/9 - (2*f101*fvu1u1u8)/9 - 
        (2*(f92 + 2*f101)*fvu1u1u9)/9 - (2*(f92 - f101)*tci12)/
         9) + fvu1u1u5*(-f102/9 - (2*f101*tci12)/9) + 
      fvu1u1u8*((6*f47 - f49 - 6*f97)/9 - (2*f101*fvu1u1u9)/9 + 
        (2*f101*tci12)/9) + fvu1u1u9*(-f102/9 + 
        (2*(f92 + 2*f101)*tci12)/9)) + 
    fvu1u1u1*((-4*f101*fvu1u1u2^2)/9 + 
      ((2*f92 - f101)*fvu1u1u3^2)/6 + (2*(f92 - f101)*fvu1u1u4^2)/
       9 + ((-2*f92 - f101)*fvu1u1u5^2)/6 - (f101*fvu1u1u7^2)/6 + 
      ((4*f92 - 7*f101)*fvu1u1u8^2)/18 + (f101*fvu1u1u9^2)/18 - 
      (2*f92*fvu2u1u1)/3 - (2*(f92 - f101)*fvu2u1u2)/9 - 
      (2*(f92 - 4*f101)*fvu2u1u9)/9 - (4*f101*fvu2u1u11)/9 + 
      (2*(4*f92 + 5*f101)*tci11^2)/27 + 
      (5*f101*fvu1u1u9*tci12)/9 + fvu1u1u4*
       (((-2*f92 - f101)*fvu1u1u8)/9 - (2*f101*fvu1u1u9)/9 - 
        (4*(f92 - f101)*tci12)/9) + fvu1u1u2*
       ((2*f101*fvu1u1u4)/9 + (2*f101*fvu1u1u8)/9 + 
        (2*f101*fvu1u1u9)/9 - (2*f101*tci12)/9) + 
      fvu1u1u7*((f101*fvu1u1u9)/3 + (2*f101*tci12)/3) + 
      fvu1u1u3*((f101*fvu1u1u4)/3 + ((-2*f92 + f101)*tci12)/
         3) + fvu1u1u8*((-2*f101*fvu1u1u9)/9 + 
        ((-4*f92 + 7*f101)*tci12)/9)) + 
    ((-8*f92 + 29*f101)*tci12*tcr11^2)/18 + 
    ((4*f92 - 9*f101)*tcr11^3)/18 + 
    ((4*f92 - 3*f97 + 3*f100 + 16*f101)*tci11^2*tcr12)/18 - 
    (2*(10*f92 - 13*f101)*tci12*tcr21)/9 + 
    tcr11*((-4*(2*f92 + f101)*tci11^2)/27 - 
      (2*f101*tci12*tcr12)/3 + (10*(f92 - f101)*tcr21)/9) + 
    ((-12*f62 + 12*f92 - 9*f97 + 9*f100 - 26*f101)*tcr33)/36;
L ieu1ueu11 = (-f17 + f20 + 9*f59 - f61 - 9*f70 + 
      f150 + f177)/9 + ((2*f62 + f92 + f97)*fvu1u1u2^2)/6 + 
    ((-f92 - f100)*fvu1u1u5^2)/6 + ((f100 - f101)*fvu1u1u7^2)/6 + 
    ((f97 + f101)*fvu1u1u8^2)/6 + 
    fvu1u1u8*((-f65 + f70 - f73 - f78 + f84)/3 - 
      (f101*fvu1u1u9)/3) + ((f62 + f92)*fvu2u1u4)/3 + 
    ((-f92 - f100)*fvu2u1u7)/3 + ((f100 - f101)*fvu2u1u10)/3 + 
    ((f62 + f97)*fvu2u1u11)/3 + ((f97 + f101)*fvu2u1u12)/3 + 
    ((f62 - f92 + f97 + 4*f101)*tci11^2)/18 + 
    ((f65 - f70 + f73 + f78 - f84)*tci12)/3 + 
    ((f100 - f101)*fvu1u2u8*tci12)/3 + 
    ((-f97 - f101)*fvu1u2u9*tci12)/3 + 
    fvu1u1u2*(-f84/3 - (f62*fvu1u1u5)/3 - (f92*fvu1u1u7)/3 - 
      (f97*fvu1u1u8)/3 - (f62*fvu1u1u9)/3 + (f97*tci12)/3) + 
    fvu1u1u5*(f78/3 + (f92*fvu1u1u7)/3 + (f100*fvu1u1u8)/3 + 
      (f62*fvu1u1u9)/3 - (f100*tci12)/3) + 
    fvu1u1u9*(f73/3 + (f101*tci12)/3) + 
    fvu1u1u7*(f65/3 - (f100*fvu1u1u8)/3 + (f101*fvu1u1u9)/3 + 
      (f101*tci12)/3);
L ieu0ueu11 = -f70/3;
L ieum1ueu11 = 0;
L ieum2ueu11 = 0;
L ieu2uou11 = -2*f89*fvu4u28 - 2*f89*fvu4u39 - 
    (2*f89*fvu4u51)/3 - 2*f89*fvu4u80 - 2*f89*fvu4u83 - 
    2*f89*fvu4u91 - 2*f89*fvu4u93 + (2*f89*fvu4u100)/3 - 
    (2*f89*fvu4u102)/3 + 2*f89*fvu4u139 - (2*f89*fvu4u148)/3 - 
    (3*f89*fvu4u171)/4 + (11*f89*fvu4u174)/4 + (11*f89*fvu4u182)/4 - 
    (3*f89*fvu4u184)/4 + (7*f89*fvu4u192)/6 - (f89*fvu4u199)/4 - 
    (f89*fvu4u201)/2 - f89*fvu4u202 - 4*f89*fvu4u213 + 
    2*f89*fvu4u215 - 2*f89*fvu4u221 + (2*f89*fvu4u225)/3 - 
    f89*fvu4u244 + (3*f89*fvu4u252)/2 - (f89*fvu4u255)/6 + 
    (11*f89*fvu4u271)/4 + (9*f89*fvu4u273)/4 - (f89*fvu4u277)/4 + 
    (3*f89*fvu4u279)/4 - (2*f89*fvu4u282)/3 + (f89*fvu4u289)/4 + 
    (f89*fvu4u290)/2 + f89*fvu4u291 + 2*f89*fvu4u295 + 
    2*f89*fvu4u325 - 2*f89*fvu4u330 + 
    fvu3u25*(f89*fvu1u1u1 - f89*fvu1u1u3 + f89*fvu1u1u6 - 
      f89*fvu1u1u7) + fvu3u71*(f89*fvu1u1u3 - f89*fvu1u1u6 + 
      f89*fvu1u1u7) + fvu3u81*(f89*fvu1u1u2 + f89*fvu1u1u5 + 
      f89*fvu1u1u8) + fvu3u45*(-(f89*fvu1u1u2) - f89*fvu1u1u3 + 
      f89*fvu1u1u9) + fvu3u43*(-(f89*fvu1u1u2) - f89*fvu1u1u6 + 
      f89*fvu1u1u7 + f89*fvu1u1u9) + 
    fvu3u82*(-(f89*fvu1u1u5) - f89*fvu1u1u6 + f89*fvu1u1u7 - 
      f89*fvu1u1u8 + f89*fvu1u1u9) + 
    fvu3u63*(-(f89*fvu1u1u6) - f89*fvu1u1u7 - f89*fvu1u1u9 + 
      2*f89*fvu1u1u10) - (151*f89*tci11^3*tci12)/135 + 
    fvu3u78*(-(f89*fvu1u1u2) + 2*f89*fvu1u1u3 - f89*fvu1u1u5 + 
      f89*fvu1u1u8 - 2*f89*tci12) + 
    fvu3u80*(f89*fvu1u1u3 - f89*fvu1u1u5 + (f89*fvu1u1u6)/2 - 
      (f89*fvu1u1u7)/2 + (f89*fvu1u1u8)/2 - (3*f89*fvu1u1u9)/2 - 
      (3*f89*tci12)/2) - (2*f89*tci11^2*tci21)/9 + 
    fvu1u1u1*(f89*fvu3u45 - f89*fvu3u71 - f89*fvu3u78 - 
      f89*fvu3u81 + f89*fvu3u82 + (8*f89*tci11^3)/27 - 
      8*f89*tci12*tci21) + fvu1u1u3*((-4*f89*tci11^3)/27 + 
      4*f89*tci12*tci21) + fvu1u1u5*((-4*f89*tci11^3)/27 + 
      4*f89*tci12*tci21) + fvu1u1u8*((-5*f89*tci11^3)/27 + 
      5*f89*tci12*tci21) + tci12*((-48*f89*tci31)/5 - 
      16*f89*tci32) - (4*f89*tci41)/3 + 
    ((-4*f89*tci11^3)/9 + 8*f89*tci12*tci21)*tcr11 + 
    (f89*tci11*tci12*tcr11^2)/5 + 
    fvu1u1u10*((11*f89*tci11^3)/135 + (4*f89*tci12*tci21)/
       3 + (48*f89*tci31)/5 + 4*f89*tci21*tcr11 - 
      (f89*tci11*tcr11^2)/5) + 
    fvu1u1u2*(f89*fvu3u63 + f89*fvu3u80 + (31*f89*tci11^3)/
       270 - (4*f89*tci12*tci21)/3 + (24*f89*tci31)/5 + 
      2*f89*tci21*tcr11 - (f89*tci11*tcr11^2)/10) + 
    fvu1u1u6*(-(f89*tci11^3)/270 - (5*f89*tci12*tci21)/3 - 
      (24*f89*tci31)/5 - 2*f89*tci21*tcr11 + 
      (f89*tci11*tcr11^2)/10) + 
    fvu1u1u7*((-7*f89*tci11^3)/90 + (f89*tci12*tci21)/3 - 
      (24*f89*tci31)/5 - 2*f89*tci21*tcr11 + 
      (f89*tci11*tcr11^2)/10) + 
    fvu1u1u9*((-7*f89*tci11^3)/90 + (f89*tci12*tci21)/3 - 
      (24*f89*tci31)/5 - 2*f89*tci21*tcr11 + 
      (f89*tci11*tcr11^2)/10) + 
    (40*f89*tci12*tci21*tcr12)/3 + 4*f89*tci11*tci12*
     tcr12^2;
L ieu1uou11 = 2*f89*fvu3u81 - 
    (4*f89*tci11^3)/27 + 4*f89*tci12*tci21;
L ieu0uou11 = 0;
L ieum1uou11 = 0;
L ieum2uou11 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou11+w^2*ieu1uou11+w^3*ieu0uou11+w^4*ieum1uou11+w^5*ieum2uou11;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou11a = K[w^1];
L ieu1uou11a = K[w^2];
L ieu0uou11a = K[w^3];
L ieum1uou11a = K[w^4];
L ieum2uou11a = K[w^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_odd.c> "%O"
#write <e11_odd.c> "return Eps5o2<T>("
#write <e11_odd.c> "%E", ieu2uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu0uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum2uou11a
#write <e11_odd.c> ");\n}"
L H=+u^1*ieu2ueu11+u^2*ieu1ueu11+u^3*ieu0ueu11+u^4*ieum1ueu11+u^5*ieum2ueu11;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu11a = H[u^1];
L ieu1ueu11a = H[u^2];
L ieu0ueu11a = H[u^3];
L ieum1ueu11a = H[u^4];
L ieum2ueu11a = H[u^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_even.c> "%O"
#write <e11_even.c> "return Eps5o2<T>("
#write <e11_even.c> "%E", ieu2ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu0ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum2ueu11a
#write <e11_even.c> ");\n}"
.end
