#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f137;
S f136;
S f134;
S f132;
S f61;
S f130;
S f60;
S f139;
S f126;
S f120;
S f121;
S f122;
S f128;
S f159;
S f146;
S f3;
S f147;
S f17;
S f144;
S f145;
S f142;
S f140;
S f95;
S f177;
S f98;
S f119;
S f41;
S f43;
S f58;
S f59;
S fvu1u2u9;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu4u282;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u3;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu4u313;
S fvu3u51;
S fvu4u91;
S fvu3u53;
S fvu4u309;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu6 = (-63*f3 - 63*f17 - 666*f58 + 556*f59 + 
      74*f60 - 63*f61 - 11*f120 + 74*f159 - 11*f177)/27 - 
    (f144*fvu3u3)/9 + ((f144 - 2*f146)*fvu3u4)/12 - 
    (11*f144*fvu3u5)/36 + (f144*fvu3u6)/9 - (f144*fvu3u7)/6 - 
    (8*f144*fvu3u8)/9 - (f144*fvu3u11)/9 + (f144*fvu3u12)/36 + 
    (f144*fvu3u13)/36 - (f144*fvu3u17)/12 + (f144*fvu3u18)/12 + 
    (f144*fvu3u24)/9 + (f142*fvu3u35)/6 + (f144*fvu3u51)/9 + 
    ((3*f139 + f144)*fvu3u52)/18 - (f119*fvu3u53)/6 - 
    (2*f144*fvu3u55)/9 - (f144*fvu3u56)/9 - (f144*fvu3u57)/6 + 
    (f144*fvu3u59)/6 + (f144*fvu3u61)/3 + (f144*fvu1u1u1^3)/18 - 
    (11*f144*fvu1u1u2^3)/54 - (f144*fvu1u1u6^3)/9 + 
    (f144*fvu1u1u8^3)/18 - (8*f144*fvu1u1u9^3)/27 + 
    fvu1u1u4^2*((-6*f41 + f43 + 3*f126 + 6*f142 - f147)/18 - 
      (f144*fvu1u1u5)/36 + (f144*fvu1u1u8)/36 - 
      (7*f144*fvu1u1u9)/36) + fvu1u1u3^2*
     ((3*f121 - f140 - f145)/18 + (f144*fvu1u1u4)/12 + 
      (f144*fvu1u1u6)/18 + (f144*fvu1u1u8)/12 + 
      (f144*fvu1u1u9)/9 - (f144*fvu1u1u10)/6) + 
    ((-6*f41 + f43 + 6*f142 - f147)*fvu2u1u6)/9 + 
    ((-f140 - f145)*fvu2u1u13)/9 + ((-f140 + f147)*fvu2u1u14)/9 + 
    (f144*fvu1u2u6*tci11^2)/3 - (f144*fvu1u2u9*tci11^2)/3 + 
    ((-9*f3 - 9*f17 - 90*f58 + 80*f59 + 10*f60 - 9*f61 - f120 - 
       f132 - f136 + 10*f159 - f177)*tci12)/9 + 
    ((f140 + f145)*fvu1u2u2*tci12)/9 + 
    ((6*f41 - f43 - 6*f142 + f147)*fvu1u2u3*tci12)/9 + 
    (f144*fvu2u1u3*tci12)/3 - (2*f144*fvu2u1u9*tci12)/9 + 
    (2*f144*fvu2u1u11*tci12)/9 + (f144*fvu2u1u12*tci12)/3 + 
    fvu2u1u1*((6*f95 - f98 + f145 - 6*f146)/9 - 
      (2*f144*tci12)/3) + fvu1u1u8^2*(-(f144*fvu1u1u9)/18 + 
      (f144*fvu1u1u10)/6 - (f144*tci12)/2) + 
    fvu2u1u2*((6*f41 - f43 + 6*f95 - f98 - 6*f142 - 6*f146)/9 - 
      (2*f144*tci12)/9) + fvu1u1u1^2*
     ((-3*f3 - 3*f17 + 6*f41 - f43 - 27*f58 + 27*f59 + 3*f60 - 
        3*f61 + 12*f95 - 2*f98 - 3*f121 - 3*f126 - 3*f130 - 
        3*f134 - 6*f142 + f145 - 12*f146 + 3*f159)/18 + 
      (f144*fvu1u1u2)/18 - (f144*fvu1u1u4)/18 - 
      (f144*fvu1u1u8)/18 - (f144*fvu1u1u9)/18 - 
      (f144*fvu1u1u10)/6 - (f144*tci12)/18) + 
    fvu1u1u6^2*((3*f134 - f140 + f147)/18 + (f144*fvu1u1u9)/6 - 
      (f144*fvu1u1u10)/9 - (f144*tci12)/18) + 
    fvu1u1u5^2*(-(f144*fvu1u1u9)/12 + (f144*tci12)/36) + 
    fvu1u1u9^2*(-(f144*fvu1u1u10)/18 + (f144*tci12)/12) + 
    fvu1u1u10^2*(f130/6 + (f144*tci12)/6) + 
    fvu1u1u2^2*(-(f144*fvu1u1u4)/18 + (f144*fvu1u1u5)/18 - 
      (f144*fvu1u1u8)/18 - (f144*fvu1u1u9)/9 + 
      (2*f144*tci12)/9) + tci11^2*
     ((-21*f3 - 21*f17 - 189*f58 + 189*f59 + 21*f60 - 21*f61 + 
        12*f95 - 2*f98 - 18*f130 - 18*f134 - 2*f140 - 12*f146 - 
        2*f147 + 21*f159)/108 + 
      ((-f119 - f139 - 3*f142 + 5*f144 - 2*f146)*tci12)/36) + 
    fvu1u1u10*(-f132/9 - (2*f144*fvu2u1u14)/9 - 
      (4*f144*fvu2u1u15)/9 - (f144*tci11^2)/9 - 
      (f147*tci12)/9) + fvu1u1u5*(-(f144*fvu1u1u9^2)/12 - 
      (f144*fvu2u1u4)/9 - (f144*tci11^2)/27 - 
      (f144*fvu1u1u9*tci12)/6) + 
    fvu1u1u8*((f144*fvu1u1u9^2)/9 + (f144*fvu2u1u9)/9 - 
      (2*f144*fvu2u1u11)/9 - (f144*fvu2u1u12)/3 + 
      (73*f144*tci11^2)/108 - (2*f144*fvu1u1u9*tci12)/9 - 
      (f144*fvu1u1u10*tci12)/3 + (f144*fvu1u2u9*tci12)/3) + 
    fvu1u1u9*((f144*fvu1u1u10^2)/6 - (7*f144*fvu2u1u11)/9 - 
      (f144*fvu2u1u12)/3 - (4*f144*fvu2u1u15)/9 - 
      (7*f144*tci11^2)/54 - (f144*fvu1u1u10*tci12)/9 + 
      (f144*fvu1u2u9*tci12)/3) + 
    fvu1u1u6*(-f136/9 + (f144*fvu1u1u9^2)/6 - 
      (2*f144*fvu2u1u14)/9 + (2*f144*tci11^2)/27 - 
      (f140*tci12)/9 + (f144*fvu1u1u9*tci12)/3 + 
      fvu1u1u10*(-f147/9 - (2*f144*tci12)/9)) + 
    fvu1u1u2*((2*f144*fvu1u1u4^2)/9 + (f144*fvu1u1u5^2)/9 + 
      (f144*fvu1u1u8^2)/18 + (5*f144*fvu1u1u9^2)/18 - 
      (f144*fvu2u1u3)/3 - (f144*fvu2u1u4)/9 - 
      (5*f144*fvu2u1u11)/9 + (f144*tci11^2)/27 + 
      (f144*fvu1u1u5*tci12)/9 - (2*f144*fvu1u1u9*tci12)/9 - 
      (f144*fvu1u2u6*tci12)/3 + fvu1u1u8*((f144*fvu1u1u9)/9 - 
        (f144*tci12)/9) + fvu1u1u4*(-(f144*fvu1u1u5)/9 + 
        (f144*fvu1u1u8)/9 + (2*f144*fvu1u1u9)/9 - 
        (f144*tci12)/9)) + fvu1u1u4*(-f128/9 - 
      (f144*fvu1u1u5^2)/36 + ((6*f41 - f43 - 6*f142)*fvu1u1u6)/
       9 + (f144*fvu1u1u8^2)/36 - (7*f144*fvu1u1u9^2)/36 + 
      (f147*fvu1u1u10)/9 + (f144*fvu2u1u2)/9 - 
      (f144*fvu2u1u3)/3 - (2*f144*fvu2u1u11)/9 + 
      (f144*tci11^2)/27 - (f126*tci12)/3 + 
      (7*f144*fvu1u1u9*tci12)/18 - (f144*fvu1u2u6*tci12)/3 + 
      fvu1u1u8*(-(f144*fvu1u1u9)/9 - (f144*tci12)/18) + 
      fvu1u1u5*((f144*fvu1u1u9)/6 + (f144*tci12)/18)) + 
    fvu1u1u3*(-f122/9 + (f144*fvu1u1u4^2)/12 + 
      (f144*fvu1u1u6^2)/18 + (f144*fvu1u1u8^2)/12 + 
      (f144*fvu1u1u9^2)/9 - (f144*fvu1u1u10^2)/6 + 
      (f144*fvu2u1u1)/3 + (f144*tci11^2)/36 - 
      (f121*tci12)/3 - (f144*fvu1u1u8*tci12)/6 + 
      fvu1u1u9*((f144*fvu1u1u10)/9 - (2*f144*tci12)/9) + 
      fvu1u1u4*((6*f95 - f98 - 6*f146)/9 - (f144*fvu1u1u8)/6 - 
        (f144*tci12)/6) + fvu1u1u6*(f140/9 - (f144*fvu1u1u9)/
         3 + (2*f144*fvu1u1u10)/9 - (f144*tci12)/9) + 
      fvu1u1u10*(f145/9 + (f144*tci12)/3)) + 
    fvu1u1u1*((9*f3 + 9*f17 + 90*f58 - 80*f59 - 10*f60 + 
        9*f61 + f120 + f122 + f128 + f132 + f136 - 10*f159 + 
        f177)/9 + (2*f144*fvu1u1u2^2)/9 - (f144*fvu1u1u3^2)/6 - 
      (f144*fvu1u1u4^2)/9 + ((-6*f41 + f43 + 6*f142)*fvu1u1u6)/
       9 - (f144*fvu1u1u8^2)/9 - (f144*fvu1u1u9^2)/9 + 
      (f144*fvu2u1u1)/3 + (f144*fvu2u1u2)/9 + (f144*fvu2u1u9)/9 + 
      (2*f144*fvu2u1u11)/9 - (23*f144*tci11^2)/54 + 
      ((3*f3 + 3*f17 - 6*f41 + f43 + 27*f58 - 27*f59 - 3*f60 + 
         3*f61 + 3*f121 + 3*f126 + 3*f130 + 3*f134 + 6*f142 - 
         f145 - 3*f159)*tci12)/9 - (f144*fvu1u1u9*tci12)/9 + 
      fvu1u1u2*(-(f144*fvu1u1u4)/9 - (f144*fvu1u1u8)/9 - 
        (f144*fvu1u1u9)/9 + (f144*tci12)/9) + 
      fvu1u1u8*((f144*fvu1u1u9)/9 + (2*f144*tci12)/9) + 
      fvu1u1u4*((-6*f95 + f98 + 6*f146)/9 + (f144*fvu1u1u8)/9 + 
        (f144*fvu1u1u9)/9 + (2*f144*tci12)/9) + 
      fvu1u1u10*(-f145/9 + (f144*tci12)/3) + 
      fvu1u1u3*((-6*f95 + f98 + 6*f146)/9 + (f144*tci12)/3)) + 
    ((-9*f142 + 10*f144 - 6*f146)*tci12*tcr11^2)/18 + 
    ((f142 - 2*f144 + 4*f146)*tcr11^3)/18 + 
    ((6*f119 + 6*f139 - 7*f142 + 7*f144 + 2*f146)*tci11^2*
      tcr12)/36 + ((f142 - f144 - 2*f146)*tcr12^3)/9 - 
    (2*(3*f142 - 8*f144 + 3*f146)*tci12*tcr21)/9 + 
    tcr11*(((21*f142 + 25*f144)*tci11^2)/108 + 
      ((f142 - f144)*tci12*tcr12)/3 + 
      ((-5*f144 + 6*f146)*tcr21)/9) + 
    ((-f142 + f144 + 2*f146)*tcr31)/3 + 
    ((-f142 + f144 + 2*f146)*tcr32)/12 + 
    ((18*f119 + 18*f139 + 11*f142 - 11*f144 - 74*f146)*tcr33)/
     72;
L ieu1ueu6 = (-9*f3 - 9*f17 - 90*f58 + 80*f59 + 
      10*f60 - 9*f61 - f120 + 10*f159 - f177)/9 + 
    ((-f142 + f144 - 2*f146)*fvu1u1u1^2)/6 + 
    ((-f139 - f144)*fvu1u1u3^2)/6 + ((-f119 + f142)*fvu1u1u4^2)/
     6 + ((f119 - f139)*fvu1u1u6^2)/6 + 
    fvu1u1u4*(-f126/3 - (f142*fvu1u1u6)/3 + (f119*fvu1u1u10)/3) + 
    fvu1u1u3*(-f121/3 - (f146*fvu1u1u4)/3 + (f139*fvu1u1u6)/3 + 
      (f144*fvu1u1u10)/3) + ((f144 - f146)*fvu2u1u1)/3 + 
    ((-f142 - f146)*fvu2u1u2)/3 + ((-f119 + f142)*fvu2u1u6)/3 + 
    ((-f139 - f144)*fvu2u1u13)/3 + ((f119 - f139)*fvu2u1u14)/3 + 
    ((-f119 - f139 - f146)*tci11^2)/18 + 
    ((-f3 - f17 - 9*f58 + 9*f59 + f60 - f61 - f130 - f134 + 
       f159)*tci12)/3 + ((f139 + f144)*fvu1u2u2*tci12)/3 + 
    ((f119 - f142)*fvu1u2u3*tci12)/3 + 
    fvu1u1u10*(-f130/3 - (f119*tci12)/3) + 
    fvu1u1u6*(-f134/3 - (f119*fvu1u1u10)/3 - (f139*tci12)/3) + 
    fvu1u1u1*((f3 + f17 + 9*f58 - 9*f59 - f60 + f61 + f121 + 
        f126 + f130 + f134 - f159)/3 + (f146*fvu1u1u3)/3 + 
      (f146*fvu1u1u4)/3 + (f142*fvu1u1u6)/3 - 
      (f144*fvu1u1u10)/3 + ((f142 - f144)*tci12)/3);
L ieu0ueu6 = (-f3 - f17 - 9*f58 + 9*f59 + f60 - f61 + 
     f159)/3;
L ieum1ueu6 = 0;
L ieum2ueu6 = 0;
L ieu2uou6 = -(f137*fvu4u25)/3 - (f137*fvu4u28)/3 - 
    (7*f137*fvu4u39)/3 - (f137*fvu4u41)/3 + (f137*fvu4u49)/3 - 
    (5*f137*fvu4u51)/9 + (5*f137*fvu4u80)/12 - 
    (37*f137*fvu4u83)/12 - (7*f137*fvu4u91)/4 + 
    (5*f137*fvu4u93)/12 + (f137*fvu4u100)/3 - 
    (23*f137*fvu4u102)/18 + (f137*fvu4u111)/4 + (f137*fvu4u113)/2 + 
    f137*fvu4u114 - (8*f137*fvu4u129)/3 + (f137*fvu4u132)/3 - 
    (f137*fvu4u139)/6 - (8*f137*fvu4u141)/3 + (4*f137*fvu4u146)/9 - 
    (11*f137*fvu4u148)/18 + (5*f137*fvu4u171)/12 + 
    (23*f137*fvu4u174)/12 - (f137*fvu4u182)/12 + 
    (5*f137*fvu4u184)/12 + (f137*fvu4u190)/9 - (f137*fvu4u192)/18 + 
    (f137*fvu4u199)/4 + (f137*fvu4u201)/2 + f137*fvu4u202 + 
    (7*f137*fvu4u213)/12 - (f137*fvu4u215)/4 + (9*f137*fvu4u219)/4 + 
    (7*f137*fvu4u221)/12 - (4*f137*fvu4u225)/9 - (f137*fvu4u233)/4 - 
    (f137*fvu4u234)/2 - f137*fvu4u235 + (17*f137*fvu4u244)/3 + 
    (4*f137*fvu4u246)/3 - (7*f137*fvu4u252)/2 - 
    (17*f137*fvu4u255)/18 - (5*f137*fvu4u271)/12 - 
    (43*f137*fvu4u273)/12 + (67*f137*fvu4u277)/12 - 
    (5*f137*fvu4u279)/12 - (f137*fvu4u282)/3 - (f137*fvu4u289)/4 - 
    (f137*fvu4u290)/2 - f137*fvu4u291 - (2*f137*fvu4u293)/3 - 
    (2*f137*fvu4u295)/3 - 2*f137*fvu4u309 + 2*f137*fvu4u313 + 
    fvu3u78*((2*f137*fvu1u1u2)/3 - (2*f137*fvu1u1u3)/3 - 
      (2*f137*fvu1u1u9)/3) + fvu3u70*((f137*fvu1u1u2)/6 - 
      (f137*fvu1u1u3)/6 - (f137*fvu1u1u9)/6) + 
    fvu3u45*(-(f137*fvu1u1u2)/2 - (7*f137*fvu1u1u3)/6 - 
      (2*f137*fvu1u1u6)/3 + (2*f137*fvu1u1u7)/3 + 
      (f137*fvu1u1u9)/2) + fvu3u23*((f137*fvu1u1u1)/3 + 
      (f137*fvu1u1u2)/6 + (f137*fvu1u1u6)/2 - (f137*fvu1u1u7)/6 - 
      (f137*fvu1u1u8)/3 - (f137*fvu1u1u9)/6 - 
      (f137*fvu1u1u10)/3) + fvu3u43*(-(f137*fvu1u1u2)/6 - 
      (f137*fvu1u1u4)/3 - (f137*fvu1u1u6)/6 + (f137*fvu1u1u7)/6 + 
      (f137*fvu1u1u9)/2 - (f137*fvu1u1u10)/3) + 
    fvu3u25*(-(f137*fvu1u1u2)/3 - (7*f137*fvu1u1u3)/6 + 
      f137*fvu1u1u4 - (2*f137*fvu1u1u5)/3 + (5*f137*fvu1u1u6)/6 - 
      (f137*fvu1u1u7)/6 + (f137*fvu1u1u10)/3) + 
    fvu3u81*((f137*fvu1u1u3)/6 + (f137*fvu1u1u4)/3 + 
      (f137*fvu1u1u6)/6 - (f137*fvu1u1u7)/6 - (f137*fvu1u1u9)/3 + 
      (f137*fvu1u1u10)/3) + fvu3u62*((-4*f137*fvu1u1u2)/3 + 
      (2*f137*fvu1u1u4)/3 + (2*f137*fvu1u1u5)/3 - 
      (4*f137*fvu1u1u6)/3 + (2*f137*fvu1u1u7)/3 + 
      (4*f137*fvu1u1u10)/3) + (13*f137*tci11^3*tci12)/15 + 
    fvu3u71*(-(f137*fvu1u1u2)/3 + (4*f137*fvu1u1u3)/3 + 
      f137*fvu1u1u4 - (2*f137*fvu1u1u5)/3 - (4*f137*fvu1u1u6)/3 + 
      (f137*fvu1u1u7)/3 + (f137*fvu1u1u8)/3 - 2*f137*tci12) + 
    fvu3u82*((f137*fvu1u1u3)/3 + (f137*fvu1u1u4)/3 - 
      (f137*fvu1u1u7)/3 + (f137*fvu1u1u8)/3 - (f137*fvu1u1u9)/3 - 
      (2*f137*tci12)/3) + fvu3u63*((2*f137*fvu1u1u2)/3 + 
      (f137*fvu1u1u3)/2 - f137*fvu1u1u4 + (2*f137*fvu1u1u5)/3 - 
      (f137*fvu1u1u6)/2 - (f137*fvu1u1u7)/3 - (f137*fvu1u1u8)/3 - 
      (f137*fvu1u1u9)/3 - (f137*fvu1u1u10)/6 + 2*f137*tci12) + 
    fvu3u80*((2*f137*fvu1u1u2)/3 + f137*fvu1u1u3 - 
      (19*f137*fvu1u1u6)/6 + (19*f137*fvu1u1u7)/6 - 
      (17*f137*fvu1u1u8)/6 + (13*f137*fvu1u1u9)/6 + 
      (17*f137*tci12)/6) - (10*f137*tci11^2*tci21)/27 + 
    fvu1u1u8*((-17*f137*tci11^3)/81 + (17*f137*tci12*tci21)/
       3) + tci12*((16*f137*tci31)/5 + 16*f137*tci32) - 
    (2072*f137*tci41)/135 - (64*f137*tci42)/5 - 
    (16*f137*tci43)/3 + ((1259*f137*tci11^3)/1620 + 
      f137*tci12*tci21 + (84*f137*tci31)/5 + 
      16*f137*tci32)*tcr11 - (11*f137*tci11*tcr11^3)/
     180 + fvu1u1u2*((181*f137*tci11^3)/1620 + 
      (2*f137*tci12*tci21)/9 + (44*f137*tci31)/5 + 
      (11*f137*tci21*tcr11)/3 - (11*f137*tci11*tcr11^2)/
       60) + fvu1u1u6*((-13*f137*tci11^3)/162 + 
      (46*f137*tci12*tci21)/9 + 8*f137*tci31 + 
      (10*f137*tci21*tcr11)/3 - (f137*tci11*tcr11^2)/6) + 
    fvu1u1u3*((73*f137*tci11^3)/1620 - (f137*tci12*tci21)/
       3 + (12*f137*tci31)/5 + f137*tci21*tcr11 - 
      (f137*tci11*tcr11^2)/20) + 
    fvu1u1u9*((103*f137*tci11^3)/540 - 
      (49*f137*tci12*tci21)/9 - (4*f137*tci31)/5 - 
      (f137*tci21*tcr11)/3 + (f137*tci11*tcr11^2)/60) + 
    fvu1u1u5*((f137*tci11^3)/45 - (16*f137*tci12*tci21)/9 - 
      (16*f137*tci31)/5 - (4*f137*tci21*tcr11)/3 + 
      (f137*tci11*tcr11^2)/15) + 
    fvu1u1u4*((-31*f137*tci11^3)/270 + (4*f137*tci12*tci21)/
       3 - (24*f137*tci31)/5 - 2*f137*tci21*tcr11 + 
      (f137*tci11*tcr11^2)/10) + 
    fvu1u1u10*((-13*f137*tci11^3)/180 - (f137*tci12*tci21)/
       9 - (28*f137*tci31)/5 - (7*f137*tci21*tcr11)/3 + 
      (7*f137*tci11*tcr11^2)/60) + 
    fvu1u1u7*((221*f137*tci11^3)/1620 - 
      (19*f137*tci12*tci21)/3 - (36*f137*tci31)/5 - 
      3*f137*tci21*tcr11 + (3*f137*tci11*tcr11^2)/20) + 
    fvu1u1u1*((f137*fvu3u25)/6 + (f137*fvu3u43)/3 + 
      (7*f137*fvu3u45)/6 - (2*f137*fvu3u62)/3 - (7*f137*fvu3u63)/6 + 
      (f137*fvu3u70)/6 - (2*f137*fvu3u71)/3 + (2*f137*fvu3u78)/3 - 
      f137*fvu3u80 - (f137*fvu3u81)/2 - (f137*fvu3u82)/3 - 
      (179*f137*tci11^3)/1620 + (f137*tci12*tci21)/3 - 
      (36*f137*tci31)/5 - 3*f137*tci21*tcr11 + 
      (3*f137*tci11*tcr11^2)/20) + 
    ((-218*f137*tci11^3)/243 - (40*f137*tci12*tci21)/3)*
     tcr12 + (-4*f137*tci11*tci12 - (40*f137*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f137*tci11*tci12)/15 + 
      5*f137*tci21 - 2*f137*tci11*tcr12) + 
    (130*f137*tci11*tcr33)/27;
L ieu1uou6 = 
   -2*f137*fvu3u63 - (11*f137*tci11^3)/135 - 
    (4*f137*tci12*tci21)/3 - (48*f137*tci31)/5 - 
    4*f137*tci21*tcr11 + (f137*tci11*tcr11^2)/5;
L ieu0uou6 = 0;
L ieum1uou6 = 0;
L ieum2uou6 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou6+w^2*ieu1uou6+w^3*ieu0uou6+w^4*ieum1uou6+w^5*ieum2uou6;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou6a = K[w^1];
L ieu1uou6a = K[w^2];
L ieu0uou6a = K[w^3];
L ieum1uou6a = K[w^4];
L ieum2uou6a = K[w^5];
.sort
#write <e6.tmp> "`optimmaxvar_'"
#write <e6_odd.c> "%O"
#write <e6_odd.c> "return Eps5o2<T>("
#write <e6_odd.c> "%E", ieu2uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieu1uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieu0uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieum1uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieum2uou6a
#write <e6_odd.c> ");\n}"
L H=+u^1*ieu2ueu6+u^2*ieu1ueu6+u^3*ieu0ueu6+u^4*ieum1ueu6+u^5*ieum2ueu6;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu6a = H[u^1];
L ieu1ueu6a = H[u^2];
L ieu0ueu6a = H[u^3];
L ieum1ueu6a = H[u^4];
L ieum2ueu6a = H[u^5];
.sort
#write <e6.tmp> "`optimmaxvar_'"
#write <e6_even.c> "%O"
#write <e6_even.c> "return Eps5o2<T>("
#write <e6_even.c> "%E", ieu2ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieu1ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieu0ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieum1ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieum2ueu6a
#write <e6_even.c> ");\n}"
.end
