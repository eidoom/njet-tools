#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f67;
S f66;
S f64;
S f63;
S f61;
S f69;
S f124;
S f74;
S f75;
S f126;
S f77;
S f120;
S f70;
S f121;
S f71;
S f122;
S f72;
S f123;
S f128;
S f78;
S f159;
S f88;
S f8;
S f81;
S f3;
S f17;
S f80;
S f14;
S f83;
S f7;
S f11;
S f86;
S f96;
S f29;
S f179;
S f94;
S f178;
S f91;
S f22;
S f20;
S f177;
S f27;
S f176;
S f26;
S f175;
S f25;
S f24;
S f99;
S f48;
S f34;
S f36;
S f58;
S f59;
S f50;
S f57;
S f104;
S f54;
S f105;
S f55;
S fvu1u2u8;
S fvu3u78;
S fvu4u25;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu1u1u9;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu4u39;
S fvu4u290;
S fvu4u291;
S fvu4u293;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u279;
S fvu4u91;
S fvu4u309;
S fvu4u93;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u330;
S fvu4u100;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u325;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu7 = (-36*f59 + 11*f61)/27 + 
    ((2*f80 + f86)*fvu3u1)/3 + ((-2*f80 - f86)*fvu3u2)/6 + 
    (f86*fvu3u3)/9 + ((f80 + f86)*fvu3u4)/6 + (f80*fvu3u5)/18 + 
    (2*(f80 + 2*f86)*fvu3u6)/9 - (2*f80*fvu3u11)/9 + 
    ((f80 + 2*f86)*fvu3u12)/18 + ((3*f55 - 3*f80 + f86)*fvu3u13)/
     18 + (f86*fvu3u14)/3 + ((-f80 + f86)*fvu3u15)/6 + 
    (f80*fvu3u17)/6 + ((f80 + 2*f86)*fvu3u18)/6 + 
    ((2*f80 + f86)*fvu3u19)/9 - (f86*fvu3u22)/3 - 
    (2*f86*fvu3u31)/9 + ((f80 - 11*f86)*fvu3u32)/18 + 
    ((-2*f86 - f96)*fvu3u33)/6 - (16*f86*fvu3u34)/9 + 
    (f86*fvu3u35)/6 - (2*(f80 + f86)*fvu3u36)/9 + 
    ((-3*f80 + f86)*fvu3u37)/18 - (f80*fvu3u38)/6 + 
    ((f80 - f86 + f91)*fvu3u40)/6 + (2*f86*fvu3u44)/9 + 
    ((4*f80 + 9*f86)*fvu1u1u1^3)/18 + 
    ((5*f80 - 2*f86)*fvu1u1u2^3)/9 + ((6*f80 - f86)*fvu1u1u3^3)/
     18 + ((-f80 + 2*f86)*fvu1u1u5^3)/9 + 
    ((-3*f80 + 17*f86)*fvu1u1u6^3)/27 + 
    ((3*f80 + 7*f86)*fvu1u1u7^3)/27 + 
    fvu1u1u4^2*((6*f48 - f50 + 3*f64 - f88 - 6*f96)/18 + 
      ((3*f80 - f86)*fvu1u1u5)/18 - (f86*fvu1u1u6)/9 + 
      (f86*fvu1u1u7)/9 + ((f80 + 2*f86)*fvu1u1u8)/18 + 
      (f80*fvu1u1u9)/18) + ((6*f54 - 6*f55 - f83 - f99)*
      fvu2u1u4)/9 + ((6*f48 - f50 - f88 - 6*f96)*fvu2u1u6)/9 + 
    ((f83 - f94)*fvu2u1u7)/9 + ((2*f80 + f86)*fvu1u2u7*
      tci11^2)/3 - (2*(f80 - 3*f86)*fvu1u2u8*tci11^2)/9 + 
    fvu1u1u9*((2*f80*fvu2u1u11)/9 + (2*f80*tci11^2)/27) + 
    (f67*tci12)/9 - (f80*fvu1u1u9^2*tci12)/18 + 
    ((-6*f48 + f50 + f88 + 6*f96)*fvu1u2u3*tci12)/9 - 
    (4*(f80 + f86)*fvu2u1u1*tci12)/3 - 
    (4*(f80 - f86)*fvu2u1u2*tci12)/9 + 
    ((-2*f80 - f86)*fvu2u1u5*tci12)/3 - 
    (4*(f80 + 2*f86)*fvu2u1u9*tci12)/9 - 
    (2*(f80 - 3*f86)*fvu2u1u10*tci12)/9 + 
    fvu1u1u1^2*((f86*fvu1u1u2)/3 - (f86*fvu1u1u3)/6 + 
      ((-2*f80 - 7*f86)*fvu1u1u4)/18 + (f86*fvu1u1u6)/18 + 
      (f86*fvu1u1u7)/9 + ((-f80 - 2*f86)*fvu1u1u8)/9 + 
      ((-8*f80 - 13*f86)*tci12)/18) + 
    fvu2u1u3*((6*f54 - 6*f55 + f88 - f99)/9 - 
      (2*(f80 - f86)*tci12)/9) + 
    fvu2u1u8*((-6*f48 + f50 - f94 + 6*f96)/9 - 
      (4*f86*tci12)/9) + fvu1u1u5^2*((-3*f69 + f83 - f94)/18 + 
      ((f80 - f86)*fvu1u1u6)/6 + (f86*fvu1u1u7)/9 + 
      ((3*f80 - f86)*fvu1u1u8)/18 + (f80*fvu1u1u9)/6 + 
      ((-3*f80 + f86)*tci12)/9) + fvu1u1u2^2*
     ((12*f54 - 12*f55 - 3*f72 - f83 + f88 - 2*f99)/18 - 
      (f80*fvu1u1u3)/6 + ((-2*f80 - f86)*fvu1u1u4)/18 - 
      (f86*fvu1u1u5)/18 + (f86*fvu1u1u6)/6 - (f80*fvu1u1u7)/6 - 
      (f80*fvu1u1u9)/9 + ((f80 + 2*f86)*tci12)/18) + 
    fvu1u1u3^2*(((f80 + f86)*fvu1u1u4)/6 - (f80*fvu1u1u7)/6 + 
      ((f80 + 2*f86)*fvu1u1u8)/6 + ((-2*f80 + 3*f86)*tci12)/
       6) + fvu1u1u7^2*(f57/6 + ((-f80 + f86)*fvu1u1u8)/9 + 
      ((f80 + 4*f86)*tci12)/18) + fvu1u1u6^2*
     ((-6*f48 + f50 - 3*f57 + 3*f59 - 3*f64 + 3*f69 + 3*f72 - 
        f94 + 6*f96)/18 + ((-f80 - 4*f86)*fvu1u1u7)/9 + 
      ((f80 - 3*f86)*fvu1u1u8)/18 + ((-f80 + 6*f86)*tci12)/
       18) + tci11^2*((12*f48 - 2*f50 + 3*f59 + 18*f64 + 
        2*f83 - 8*f88 + 2*f94 - 12*f96)/108 + 
      ((3*f55 - 10*f80 - 7*f86 - 3*f96)*tci12)/108) + 
    fvu1u2u6*((-2*(f80 - f86)*tci11^2)/9 + 
      ((6*f54 - 6*f55 + f88 - f99)*tci12)/9) + 
    fvu1u1u8*((4*f86*fvu2u1u8)/9 + (2*(f80 + 2*f86)*fvu2u1u9)/9 + 
      (2*(f80 - 3*f86)*fvu2u1u10)/9 + (5*f86*tci11^2)/54 + 
      (2*(f80 - 3*f86)*fvu1u2u8*tci12)/9) + 
    fvu1u1u7*((2*f3 + 8*f7 - f8 - 16*f11 + 2*f14 + f17 - 
        2*f20 - 16*f22 + 2*f24 - 16*f25 + 2*f27 - 8*f34 + f36 - 
        16*f57 - 2*f58 - 19*f59 + 2*f61 - 16*f63 - 16*f64 + 
        2*f66 + 2*f67 - 2*f70 + 8*f71 - f74 - 8*f78 + f81 + 
        16*f104 - 2*f105 - 2*f120 - 16*f123 + 2*f124 + 16*f126 - 
        2*f128 - 2*f159 - 2*f177 + 16*f178 - 2*f179)/18 - 
      (2*(f80 - 2*f86)*fvu1u1u8^2)/9 - (2*(3*f80 - f86)*fvu2u1u7)/
       9 - (2*(f80 - 5*f86)*fvu2u1u8)/9 + 
      (2*(f80 - 3*f86)*fvu2u1u10)/9 - (2*(3*f80 - f86)*tci11^2)/
       27 + ((6*f48 - f50 - 6*f96)*tci12)/9 + 
      (2*(f80 - f86)*fvu1u1u8*tci12)/9 + 
      (2*(f80 - 3*f86)*fvu1u2u8*tci12)/9) + 
    fvu1u1u6*((-2*f3 - 8*f7 + f8 + 16*f11 - 2*f14 - f17 + 
        2*f20 + 16*f22 - 2*f24 + 16*f25 + 16*f26 - 2*f27 - 
        2*f29 + 8*f34 - f36 + 16*f57 + 2*f58 + 19*f59 - 4*f61 + 
        16*f63 + 16*f64 - 2*f66 - 16*f69 + 2*f70 - 8*f71 + 
        f74 - 2*f75 + 8*f78 - f81 - 16*f104 + 2*f105 + 2*f120 - 
        16*f121 + 2*f122 + 16*f123 - 2*f124 - 16*f126 + 2*f128 + 
        2*f159 + 16*f175 - 2*f176 + 2*f177 - 16*f178 + 2*f179)/
       18 - (5*f86*fvu1u1u7^2)/9 + ((f80 - 7*f86)*fvu1u1u8^2)/18 - 
      (2*(f80 - 7*f86)*fvu2u1u8)/9 + (2*(f80 - 6*f86)*tci11^2)/
       27 - (f88*tci12)/9 + ((-f80 + 7*f86)*fvu1u1u8*tci12)/
       9 + fvu1u1u7*((6*f48 - f50 - 6*f96)/9 + (2*f80*fvu1u1u8)/
         9 - (2*f80*tci12)/9)) + 
    fvu1u1u3*(((f80 + f86)*fvu1u1u4^2)/6 + 
      ((-2*f80 - f86)*fvu1u1u5^2)/6 - (f80*fvu1u1u7^2)/6 + 
      ((f80 + 2*f86)*fvu1u1u8^2)/6 + (2*(f80 + f86)*fvu2u1u1)/3 + 
      ((2*f80 + f86)*fvu2u1u5)/3 + ((4*f80 - 11*f86)*tci11^2)/
       18 + ((2*f80 + f86)*fvu1u1u5*tci12)/3 + 
      (f80*fvu1u1u7*tci12)/3 + ((-f80 - 2*f86)*fvu1u1u8*
        tci12)/3 + ((-2*f80 - f86)*fvu1u2u7*tci12)/3 + 
      fvu1u1u4*(((-f80 - 2*f86)*fvu1u1u8)/3 + 
        ((-f80 - f86)*tci12)/3)) + 
    fvu1u1u5*((-8*f26 + f29 + 8*f69 + 8*f121 - f122 - 8*f175 + 
        f176)/9 + ((f80 - f86)*fvu1u1u6^2)/6 + 
      (f80*fvu1u1u7^2)/3 + ((3*f80 - f86)*fvu1u1u8^2)/18 + 
      (f80*fvu1u1u9^2)/6 + (2*(3*f80 - f86)*fvu2u1u4)/9 + 
      ((2*f80 + f86)*fvu2u1u5)/3 - (2*(3*f80 - f86)*fvu2u1u7)/9 + 
      ((54*f80 - 13*f86)*tci11^2)/108 + 
      ((-6*f54 + 6*f55 + f99)*tci12)/9 + 
      ((-3*f80 + f86)*fvu1u1u8*tci12)/9 + 
      (f80*fvu1u1u9*tci12)/3 + ((-2*f80 - f86)*fvu1u2u7*
        tci12)/3 + fvu1u1u6*(f94/9 + ((-f80 + f86)*fvu1u1u8)/
         3 + ((f80 - f86)*tci12)/3) + 
      fvu1u1u7*(-f83/9 - (2*f86*fvu1u1u8)/9 + (2*f86*tci12)/
         9)) + fvu1u1u4*(-f67/9 + ((3*f80 - f86)*fvu1u1u5^2)/18 + 
      (f86*fvu1u1u6^2)/9 + (f86*fvu1u1u7^2)/9 + 
      ((f80 + 2*f86)*fvu1u1u8^2)/18 + (f80*fvu1u1u9^2)/18 + 
      (2*(f80 - f86)*fvu2u1u2)/9 + (2*(f80 - f86)*fvu2u1u3)/9 + 
      (4*f86*fvu2u1u8)/9 - (f64*tci12)/3 + 
      ((-f80 - 2*f86)*fvu1u1u8*tci12)/9 - 
      (f80*fvu1u1u9*tci12)/9 + (2*(f80 - f86)*fvu1u2u6*
        tci12)/9 + fvu1u1u7*((-6*f48 + f50 + 6*f96)/9 + 
        (2*f86*fvu1u1u8)/9 - (2*f86*tci12)/9) + 
      fvu1u1u6*(f88/9 - (2*f86*fvu1u1u7)/9 - (2*f86*fvu1u1u8)/9 + 
        (2*f86*tci12)/9) + fvu1u1u5*((6*f54 - 6*f55 - f99)/9 - 
        (f80*fvu1u1u9)/3 + ((-3*f80 + f86)*tci12)/9)) + 
    fvu1u1u2*(f75/9 + ((-f80 - f86)*fvu1u1u3^2)/6 + 
      ((-4*f80 + f86)*fvu1u1u4^2)/18 + 
      ((-6*f80 + f86)*fvu1u1u5^2)/18 + (f86*fvu1u1u6^2)/6 - 
      (f80*fvu1u1u7^2)/6 - (2*f80*fvu1u1u9^2)/9 + 
      (2*(f80 - f86)*fvu2u1u3)/9 + (2*(3*f80 - f86)*fvu2u1u4)/9 + 
      (2*f80*fvu2u1u11)/9 + ((-2*f80 + f86)*tci11^2)/9 - 
      (f88*tci12)/9 - (2*f80*fvu1u1u9*tci12)/9 + 
      (2*(f80 - f86)*fvu1u2u6*tci12)/9 + 
      fvu1u1u7*(f83/9 - (f80*tci12)/3) + 
      fvu1u1u5*((-6*f54 + 6*f55 + f99)/9 - (f86*tci12)/9) + 
      fvu1u1u6*(-f88/9 + (f86*tci12)/3) + 
      fvu1u1u3*((f80*fvu1u1u7)/3 + ((f80 + f86)*tci12)/3) + 
      fvu1u1u4*((-6*f54 + 6*f55 + f99)/9 + (f86*fvu1u1u5)/9 + 
        (2*f80*fvu1u1u9)/9 + ((2*f80 + f86)*tci12)/9)) + 
    fvu1u1u1*((f86*fvu1u1u2^2)/6 + ((-2*f80 - 3*f86)*fvu1u1u3^2)/
       6 + ((-4*f80 - 5*f86)*fvu1u1u4^2)/18 + 
      ((2*f80 + f86)*fvu1u1u5^2)/6 - (5*f86*fvu1u1u6^2)/18 + 
      (2*f86*fvu1u1u7^2)/9 - (2*(f80 + 2*f86)*fvu1u1u8^2)/9 + 
      (2*(f80 + f86)*fvu2u1u1)/3 + (2*(f80 - f86)*fvu2u1u2)/9 - 
      (4*f86*fvu2u1u8)/9 + (2*(f80 + 2*f86)*fvu2u1u9)/9 + 
      ((-16*f80 - 11*f86)*tci11^2)/54 + 
      (4*(f80 + 2*f86)*fvu1u1u8*tci12)/9 + 
      fvu1u1u2*(-(f86*fvu1u1u6)/3 - (2*f86*tci12)/3) + 
      fvu1u1u6*((2*f86*fvu1u1u7)/9 + (2*f86*fvu1u1u8)/9 - 
        (5*f86*tci12)/9) + fvu1u1u7*((-2*f86*fvu1u1u8)/9 + 
        (2*f86*tci12)/9) + fvu1u1u3*((f86*fvu1u1u4)/3 + 
        ((2*f80 + 3*f86)*tci12)/3) + 
      fvu1u1u4*((2*f86*fvu1u1u6)/9 - (2*f86*fvu1u1u7)/9 + 
        (2*(f80 + 2*f86)*fvu1u1u8)/9 + ((4*f80 + 5*f86)*tci12)/
         9)) + ((8*f80 + 7*f86)*tci12*tcr11^2)/18 + 
    ((-4*f80 - 9*f86)*tcr11^3)/18 + 
    ((-3*f55 - 4*f80 - 16*f86 + 3*f96)*tci11^2*tcr12)/18 + 
    (10*(2*f80 + f86)*tci12*tcr21)/9 + 
    tcr11*(((16*f80 - f86)*tci11^2)/54 + 
      (2*f86*tci12*tcr12)/3 - (2*(5*f80 + 4*f86)*tcr21)/
       9) + ((-9*f55 - 12*f80 + 26*f86 - 12*f91 + 9*f96)*tcr33)/
     36;
L ieu1ueu7 = f61/9 + 
    ((-2*f55 - f80 + f86)*fvu1u1u2^2)/6 + 
    ((-f86 - f96)*fvu1u1u4^2)/6 + ((f80 - f91)*fvu1u1u5^2)/6 + 
    ((-f91 + f96)*fvu1u1u6^2)/6 + 
    fvu1u1u4*(-f64/3 - (f55*fvu1u1u5)/3 + (f86*fvu1u1u6)/3 + 
      (f96*fvu1u1u7)/3) + ((-f55 + f86)*fvu2u1u3)/3 + 
    ((-f55 - f80)*fvu2u1u4)/3 + ((-f86 - f96)*fvu2u1u6)/3 + 
    ((f80 - f91)*fvu2u1u7)/3 + ((-f91 + f96)*fvu2u1u8)/3 + 
    ((f80 - 4*f86 + f91 - f96)*tci11^2)/18 + 
    (f64*tci12)/3 + ((f86 + f96)*fvu1u2u3*tci12)/3 + 
    ((-f55 + f86)*fvu1u2u6*tci12)/3 + 
    fvu1u1u5*(f69/3 + (f91*fvu1u1u6)/3 - (f80*fvu1u1u7)/3 + 
      (f55*tci12)/3) + fvu1u1u2*(f72/3 + (f55*fvu1u1u4)/3 + 
      (f55*fvu1u1u5)/3 - (f86*fvu1u1u6)/3 + (f80*fvu1u1u7)/3 - 
      (f86*tci12)/3) + fvu1u1u6*
     ((f57 - f59 + f64 - f69 - f72)/3 - (f96*fvu1u1u7)/3 - 
      (f86*tci12)/3) + fvu1u1u7*(-f57/3 - (f96*tci12)/3);
L ieu0ueu7 = f59/3;
L ieum1ueu7 = 0;
L ieum2ueu7 = 0;
L ieu2uou7 = -(f77*fvu4u25) - f77*fvu4u28 - f77*fvu4u39 - 
    f77*fvu4u41 + (f77*fvu4u49)/3 - (f77*fvu4u51)/3 - 
    f77*fvu4u80 - f77*fvu4u83 - f77*fvu4u91 - f77*fvu4u93 + 
    (f77*fvu4u100)/3 - (f77*fvu4u102)/3 + (7*f77*fvu4u171)/4 + 
    (5*f77*fvu4u174)/4 - (3*f77*fvu4u182)/4 - (f77*fvu4u184)/4 - 
    (f77*fvu4u190)/3 - (f77*fvu4u192)/2 + (f77*fvu4u199)/4 + 
    (f77*fvu4u201)/2 + f77*fvu4u202 - 2*f77*fvu4u213 + 
    (2*f77*fvu4u225)/3 + 7*f77*fvu4u244 - (3*f77*fvu4u252)/2 - 
    (7*f77*fvu4u255)/6 + (f77*fvu4u271)/4 - (17*f77*fvu4u273)/4 + 
    (17*f77*fvu4u277)/4 + (f77*fvu4u279)/4 - (f77*fvu4u282)/3 - 
    (f77*fvu4u289)/4 - (f77*fvu4u290)/2 - f77*fvu4u291 - 
    2*f77*fvu4u293 - 2*f77*fvu4u309 + 2*f77*fvu4u325 - 
    2*f77*fvu4u330 + ((f77*fvu3u45)/2 - (f77*fvu3u70)/2 - 
      (f77*fvu3u81)/2)*fvu1u1u1 + fvu3u78*(-(f77*fvu1u1u4) - 
      f77*fvu1u1u5 + f77*fvu1u1u6) + 
    fvu3u25*((f77*fvu1u1u1)/2 - (f77*fvu1u1u3)/2 + 
      (f77*fvu1u1u6)/2 - (f77*fvu1u1u7)/2) + 
    fvu3u81*((f77*fvu1u1u3)/2 + (f77*fvu1u1u6)/2 - 
      (f77*fvu1u1u7)/2) + fvu3u23*((f77*fvu1u1u2)/2 + 
      (f77*fvu1u1u6)/2 - (f77*fvu1u1u7)/2 - (f77*fvu1u1u9)/2) + 
    fvu3u70*((f77*fvu1u1u2)/2 - (f77*fvu1u1u3)/2 + 
      f77*fvu1u1u4 + f77*fvu1u1u5 + f77*fvu1u1u7 - 
      (f77*fvu1u1u9)/2) + fvu3u45*(-(f77*fvu1u1u2)/2 - 
      (f77*fvu1u1u3)/2 + (f77*fvu1u1u9)/2) + 
    fvu3u43*(-(f77*fvu1u1u6)/2 + (f77*fvu1u1u7)/2 + 
      (f77*fvu1u1u9)/2) + (151*f77*tci11^3*tci12)/135 + 
    fvu3u71*(f77*fvu1u1u4 - f77*fvu1u1u5 - f77*fvu1u1u6 - 
      2*f77*tci12) + fvu3u82*(f77*fvu1u1u4 - f77*fvu1u1u5 - 
      f77*fvu1u1u6 - 2*f77*tci12) + 
    fvu3u80*((-5*f77*fvu1u1u6)/2 + (5*f77*fvu1u1u7)/2 - 
      (7*f77*fvu1u1u8)/2 + (5*f77*fvu1u1u9)/2 + 
      (7*f77*tci12)/2) + (2*f77*tci11^2*tci21)/9 + 
    fvu1u1u4*((4*f77*tci11^3)/27 - 4*f77*tci12*tci21) + 
    fvu1u1u5*((4*f77*tci11^3)/27 - 4*f77*tci12*tci21) + 
    fvu1u1u3*(f77*fvu3u71 + f77*fvu3u78 + f77*fvu3u82 - 
      (4*f77*tci11^3)/27 + 4*f77*tci12*tci21) + 
    fvu1u1u8*((-7*f77*tci11^3)/27 + 7*f77*tci12*tci21) + 
    tci12*((48*f77*tci31)/5 + 16*f77*tci32) + 
    (4*f77*tci41)/3 + (4*f77*tci11^3*tcr11)/27 - 
    (f77*tci11*tci12*tcr11^2)/5 + 
    fvu1u1u7*((19*f77*tci11^3)/60 - (23*f77*tci12*tci21)/
       3 + (12*f77*tci31)/5 + f77*tci21*tcr11 - 
      (f77*tci11*tcr11^2)/20) + 
    fvu1u1u9*((91*f77*tci11^3)/540 - (11*f77*tci12*tci21)/
       3 + (12*f77*tci31)/5 + f77*tci21*tcr11 - 
      (f77*tci11*tcr11^2)/20) + 
    fvu1u1u2*(-(f77*fvu3u43)/2 + f77*fvu3u80 + 
      (49*f77*tci11^3)/540 - (10*f77*tci12*tci21)/3 - 
      (12*f77*tci31)/5 - f77*tci21*tcr11 + 
      (f77*tci11*tcr11^2)/20) + 
    fvu1u1u6*((-19*f77*tci11^3)/60 + (23*f77*tci12*tci21)/
       3 - (12*f77*tci31)/5 - f77*tci21*tcr11 + 
      (f77*tci11*tcr11^2)/20) - 
    (40*f77*tci12*tci21*tcr12)/3 - 4*f77*tci11*tci12*
     tcr12^2;
L ieu1uou7 = 2*f77*fvu3u70 + 
    (4*f77*tci11^3)/27 - 4*f77*tci12*tci21;
L ieu0uou7 = 0;
L ieum1uou7 = 0;
L ieum2uou7 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou7+w^2*ieu1uou7+w^3*ieu0uou7+w^4*ieum1uou7+w^5*ieum2uou7;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou7a = K[w^1];
L ieu1uou7a = K[w^2];
L ieu0uou7a = K[w^3];
L ieum1uou7a = K[w^4];
L ieum2uou7a = K[w^5];
.sort
#write <e7.tmp> "`optimmaxvar_'"
#write <e7_odd.c> "%O"
#write <e7_odd.c> "return Eps5o2<T>("
#write <e7_odd.c> "%E", ieu2uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieu1uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieu0uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieum1uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieum2uou7a
#write <e7_odd.c> ");\n}"
L H=+u^1*ieu2ueu7+u^2*ieu1ueu7+u^3*ieu0ueu7+u^4*ieum1ueu7+u^5*ieum2ueu7;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu7a = H[u^1];
L ieu1ueu7a = H[u^2];
L ieu0ueu7a = H[u^3];
L ieum1ueu7a = H[u^4];
L ieum2ueu7a = H[u^5];
.sort
#write <e7.tmp> "`optimmaxvar_'"
#write <e7_even.c> "%O"
#write <e7_even.c> "return Eps5o2<T>("
#write <e7_even.c> "%E", ieu2ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieu1ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieu0ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieum1ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieum2ueu7a
#write <e7_even.c> ");\n}"
.end
