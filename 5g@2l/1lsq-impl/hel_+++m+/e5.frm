#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f136;
S f66;
S f134;
S f63;
S f131;
S f61;
S f60;
S f124;
S f74;
S f125;
S f127;
S f120;
S f70;
S f71;
S f123;
S f129;
S f151;
S f150;
S f152;
S f159;
S f16;
S f17;
S f144;
S f145;
S f13;
S f5;
S f4;
S f179;
S f178;
S f20;
S f177;
S f115;
S f114;
S f111;
S f40;
S f110;
S f113;
S f42;
S f112;
S f108;
S f58;
S f109;
S f59;
S f103;
S f106;
S f107;
S f104;
S f105;
S fvu1u2u9;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u139;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u3;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu3u51;
S fvu4u91;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu5 = (-11*f17 - 74*f20 - 729*f58 - 641*f59 + 
      74*f60 + 63*f61 - 63*f70 - 74*f120 - 74*f150 + 11*f159 - 
      63*f177)/27 - (f113*fvu3u3)/9 + (f113*fvu3u4)/12 - 
    (11*f113*fvu3u5)/36 + (f113*fvu3u6)/9 - (f113*fvu3u7)/6 - 
    (8*f113*fvu3u8)/9 + (f103*fvu3u10)/6 - (f113*fvu3u11)/9 + 
    (f113*fvu3u12)/36 + (f113*fvu3u13)/36 - (f113*fvu3u17)/12 + 
    ((-2*f110 + f113)*fvu3u18)/12 + (f113*fvu3u24)/9 + 
    (f113*fvu3u51)/9 + (f113*fvu3u52)/18 - (2*f113*fvu3u55)/9 + 
    ((-2*f113 - 3*f114)*fvu3u56)/18 + ((f111 - f113)*fvu3u57)/6 + 
    (f113*fvu3u59)/6 + (f113*fvu3u61)/3 + (f113*fvu1u1u1^3)/18 - 
    (11*f113*fvu1u1u2^3)/54 - (f113*fvu1u1u6^3)/9 + 
    (f113*fvu1u1u8^3)/18 - (8*f113*fvu1u1u9^3)/27 + 
    fvu1u1u4^2*(-(f113*fvu1u1u5)/36 + (f113*fvu1u1u8)/36 - 
      (7*f113*fvu1u1u9)/36) + fvu1u1u3^2*
     ((-3*f104 - 6*f113 - f115 + 6*f144 - f145)/18 + 
      (f113*fvu1u1u4)/12 + (f113*fvu1u1u6)/18 + 
      (f113*fvu1u1u8)/12 + (f113*fvu1u1u9)/9 - 
      (f113*fvu1u1u10)/6) + ((-6*f113 - f115 + 6*f144 - f145)*
      fvu2u1u13)/9 + ((f112 - f115)*fvu2u1u15)/9 + 
    (f113*fvu1u2u6*tci11^2)/3 + 
    ((-8*f63 + f66 + 8*f71 - f74 - f105 + 8*f107 + 8*f108 - 
       8*f123 + f124 - 8*f125 + f127 - 8*f129 + f131 + 8*f134 - 
       f136 + 8*f151 - f152 + 8*f178 - f179)*tci12)/9 + 
    ((6*f113 + f115 - 6*f144 + f145)*fvu1u2u2*tci12)/9 - 
    (2*f113*fvu2u1u2*tci12)/9 + (f113*fvu2u1u3*tci12)/3 + 
    (2*f113*fvu2u1u11*tci12)/9 + 
    fvu1u1u10*((8*f4 - f5 + f17 + 10*f20 + 99*f58 + 91*f59 - 
        10*f60 - 9*f61 - 8*f63 + f66 + 9*f70 - f105 - 8*f106 + 
        8*f107 + 8*f108 + 10*f120 - 8*f123 + f124 - 8*f125 + 
        f127 - 8*f129 + f131 + 10*f150 + 8*f151 - f152 - f159 + 
        9*f177 + 8*f178 - f179)/9 - (2*f113*fvu2u1u14)/9 - 
      (4*f113*fvu2u1u15)/9 - (f113*tci11^2)/9 - 
      (f112*tci12)/9) + fvu1u2u9*(-(f113*tci11^2)/3 + 
      ((6*f40 - f42 - 6*f103 + f112)*tci12)/9) + 
    fvu2u1u1*((6*f13 - f16 - 6*f110 + 6*f113 - 6*f144 + f145)/
       9 - (2*f113*tci12)/3) + fvu1u1u8^2*
     ((-6*f40 + f42 + 6*f103 + 3*f108 - f112)/18 - 
      (f113*fvu1u1u9)/18 + (f113*fvu1u1u10)/6 - 
      (f113*tci12)/2) + fvu2u1u9*
     ((6*f13 - f16 + 6*f40 - f42 - 6*f103 - 6*f110)/9 - 
      (2*f113*tci12)/9) + fvu1u1u1^2*
     ((12*f13 - 2*f16 + 6*f40 - f42 - 6*f103 + 3*f107 - 
        12*f110 + 6*f113 - 6*f144 + f145)/18 + 
      (f113*fvu1u1u2)/18 - (f113*fvu1u1u4)/18 - 
      (f113*fvu1u1u8)/18 - (f113*fvu1u1u9)/18 - 
      (f113*fvu1u1u10)/6 - (f113*tci12)/18) + 
    fvu1u1u6^2*((f113*fvu1u1u9)/6 - (f113*fvu1u1u10)/9 - 
      (f113*tci12)/18) + fvu1u1u5^2*(-(f113*fvu1u1u9)/12 + 
      (f113*tci12)/36) + fvu1u1u9^2*
     ((-3*f106 + f112 - f115)/18 - (f113*fvu1u1u10)/18 + 
      (f113*tci12)/12) + fvu1u1u10^2*
     ((-f20 - 10*f58 - 10*f59 + f60 + f61 - f70 + f104 + 
        f106 - f107 - f108 - f120 - f150 - f177)/6 + 
      (f113*tci12)/6) + fvu1u1u2^2*(-(f113*fvu1u1u4)/18 + 
      (f113*fvu1u1u5)/18 - (f113*fvu1u1u8)/18 - 
      (f113*fvu1u1u9)/9 + (2*f113*tci12)/9) + 
    fvu2u1u12*((-6*f40 + f42 + 6*f103 - f112)/9 + 
      (f113*tci12)/3) + tci11^2*
     ((12*f13 - 2*f16 - 3*f20 - 30*f58 - 30*f59 + 3*f60 + 
        3*f61 - 3*f70 - 18*f104 + 18*f107 + 18*f108 - 12*f110 - 
        2*f112 - 2*f115 - 3*f120 - 3*f150 - 3*f177)/108 + 
      ((-3*f103 - 2*f110 - f111 + 5*f113 - f114)*tci12)/36) + 
    fvu1u1u5*(-(f113*fvu1u1u9^2)/12 - (f113*fvu2u1u4)/9 - 
      (f113*tci11^2)/27 - (f113*fvu1u1u9*tci12)/6) + 
    fvu1u1u6*((f113*fvu1u1u9^2)/6 - (2*f113*fvu2u1u14)/9 + 
      (2*f113*tci11^2)/27 + (f113*fvu1u1u9*tci12)/3 - 
      (2*f113*fvu1u1u10*tci12)/9) + 
    fvu1u1u8*((8*f63 - f66 - 8*f71 + f74 - 8*f108 + 8*f123 - 
        f124 + 8*f129 - f131 - 8*f134 + f136)/9 + 
      (f113*fvu1u1u9^2)/9 + (f113*fvu2u1u9)/9 - 
      (2*f113*fvu2u1u11)/9 - (f113*fvu2u1u12)/3 + 
      (73*f113*tci11^2)/108 - (f108*tci12)/3 + 
      (f113*fvu1u2u9*tci12)/3 + fvu1u1u10*
       (f112/9 - (f113*tci12)/3) + fvu1u1u9*
       ((6*f40 - f42 - 6*f103)/9 - (2*f113*tci12)/9)) + 
    fvu1u1u9*((-8*f4 + f5 + 8*f71 - f74 + 8*f106 + 8*f134 - 
        f136)/9 + (f113*fvu1u1u10^2)/6 - (7*f113*fvu2u1u11)/9 - 
      (f113*fvu2u1u12)/3 - (4*f113*fvu2u1u15)/9 - 
      (7*f113*tci11^2)/54 - (f115*tci12)/9 + 
      (f113*fvu1u2u9*tci12)/3 + fvu1u1u10*
       (-f112/9 - (f113*tci12)/9)) + 
    fvu1u1u2*((2*f113*fvu1u1u4^2)/9 + (f113*fvu1u1u5^2)/9 + 
      (f113*fvu1u1u8^2)/18 + (5*f113*fvu1u1u9^2)/18 - 
      (f113*fvu2u1u3)/3 - (f113*fvu2u1u4)/9 - 
      (5*f113*fvu2u1u11)/9 + (f113*tci11^2)/27 + 
      (f113*fvu1u1u5*tci12)/9 - (2*f113*fvu1u1u9*tci12)/9 - 
      (f113*fvu1u2u6*tci12)/3 + fvu1u1u8*((f113*fvu1u1u9)/9 - 
        (f113*tci12)/9) + fvu1u1u4*(-(f113*fvu1u1u5)/9 + 
        (f113*fvu1u1u8)/9 + (2*f113*fvu1u1u9)/9 - 
        (f113*tci12)/9)) + fvu1u1u4*(-(f113*fvu1u1u5^2)/36 + 
      (f113*fvu1u1u8^2)/36 - (7*f113*fvu1u1u9^2)/36 + 
      (f113*fvu2u1u2)/9 - (f113*fvu2u1u3)/3 - 
      (2*f113*fvu2u1u11)/9 + (f113*tci11^2)/27 + 
      (7*f113*fvu1u1u9*tci12)/18 - (f113*fvu1u2u6*tci12)/3 + 
      fvu1u1u8*(-(f113*fvu1u1u9)/9 - (f113*tci12)/18) + 
      fvu1u1u5*((f113*fvu1u1u9)/6 + (f113*tci12)/18)) + 
    fvu1u1u1*((-8*f107 + 8*f125 - f127 - 8*f151 + f152 - 
        8*f178 + f179)/9 + (2*f113*fvu1u1u2^2)/9 - 
      (f113*fvu1u1u3^2)/6 - (f113*fvu1u1u4^2)/9 - 
      (f113*fvu1u1u8^2)/9 - (f113*fvu1u1u9^2)/9 + 
      (f113*fvu2u1u1)/3 + (f113*fvu2u1u2)/9 + (f113*fvu2u1u9)/9 + 
      (2*f113*fvu2u1u11)/9 - (23*f113*tci11^2)/54 + 
      ((-6*f40 + f42 + 6*f103 - 3*f107 - 6*f113 + 6*f144 - 
         f145)*tci12)/9 + fvu1u1u9*((-6*f40 + f42 + 6*f103)/9 - 
        (f113*tci12)/9) + fvu1u1u2*(-(f113*fvu1u1u4)/9 - 
        (f113*fvu1u1u8)/9 - (f113*fvu1u1u9)/9 + 
        (f113*tci12)/9) + fvu1u1u8*((-6*f13 + f16 + 6*f110)/9 + 
        (f113*fvu1u1u9)/9 + (2*f113*tci12)/9) + 
      fvu1u1u4*((f113*fvu1u1u8)/9 + (f113*fvu1u1u9)/9 + 
        (2*f113*tci12)/9) + fvu1u1u3*((-6*f13 + f16 + 6*f110)/
         9 + (f113*tci12)/3) + fvu1u1u10*
       ((-6*f113 + 6*f144 - f145)/9 + (f113*tci12)/3)) + 
    fvu1u1u3*(f105/9 + (f113*fvu1u1u4^2)/12 + 
      (f113*fvu1u1u6^2)/18 + (f113*fvu1u1u8^2)/12 + 
      (f113*fvu1u1u9^2)/9 - (f113*fvu1u1u10^2)/6 + 
      (f113*fvu2u1u1)/3 + (f113*tci11^2)/36 + 
      (f104*tci12)/3 + fvu1u1u9*(f115/9 + (f113*fvu1u1u10)/9 - 
        (2*f113*tci12)/9) + fvu1u1u8*
       ((6*f13 - f16 - 6*f110)/9 - (f113*tci12)/6) + 
      fvu1u1u4*(-(f113*fvu1u1u8)/6 - (f113*tci12)/6) + 
      fvu1u1u6*(-(f113*fvu1u1u9)/3 + (2*f113*fvu1u1u10)/9 - 
        (f113*tci12)/9) + fvu1u1u10*((6*f113 - 6*f144 + f145)/
         9 + (f113*tci12)/3)) + ((-9*f103 - 6*f110 + 10*f113)*
      tci12*tcr11^2)/18 + ((f103 + 4*f110 - 2*f113)*
      tcr11^3)/18 + ((-7*f103 + 2*f110 + 6*f111 + 7*f113 + 
       6*f114)*tci11^2*tcr12)/36 + 
    ((f103 - 2*f110 - f113)*tcr12^3)/9 - 
    (2*(3*f103 + 3*f110 - 8*f113)*tci12*tcr21)/9 + 
    tcr11*(((21*f103 + 25*f113)*tci11^2)/108 + 
      ((f103 - f113)*tci12*tcr12)/3 + 
      ((6*f110 - 5*f113)*tcr21)/9) + 
    ((-f103 + 2*f110 + f113)*tcr31)/3 + 
    ((-f103 + 2*f110 + f113)*tcr32)/12 + 
    ((11*f103 - 74*f110 + 18*f111 - 11*f113 + 18*f114)*tcr33)/
     72;
L ieu1ueu5 = (-f17 - 10*f20 - 99*f58 - 91*f59 + 
      10*f60 + 9*f61 - 9*f70 - 10*f120 - 10*f150 + f159 - 
      9*f177)/9 + ((-f103 - 2*f110 + f113)*fvu1u1u1^2)/6 + 
    ((-f113 - f114)*fvu1u1u3^2)/6 + ((f103 - f111)*fvu1u1u8^2)/
     6 + ((f111 - f114)*fvu1u1u9^2)/6 + 
    fvu1u1u8*(-f108/3 - (f103*fvu1u1u9)/3 + (f111*fvu1u1u10)/3) + 
    fvu1u1u3*(f104/3 - (f110*fvu1u1u8)/3 + (f114*fvu1u1u9)/3 + 
      (f113*fvu1u1u10)/3) + ((-f110 + f113)*fvu2u1u1)/3 + 
    ((-f103 - f110)*fvu2u1u9)/3 + ((f103 - f111)*fvu2u1u12)/3 + 
    ((-f113 - f114)*fvu2u1u13)/3 + ((f111 - f114)*fvu2u1u15)/3 + 
    ((-f110 - f111 - f114)*tci11^2)/18 + 
    ((-f104 + f107 + f108)*tci12)/3 + 
    ((f113 + f114)*fvu1u2u2*tci12)/3 + 
    ((-f103 + f111)*fvu1u2u9*tci12)/3 + 
    fvu1u1u10*((f20 + 10*f58 + 10*f59 - f60 - f61 + f70 - 
        f104 - f106 + f107 + f108 + f120 + f150 + f177)/3 - 
      (f111*tci12)/3) + fvu1u1u1*(-f107/3 + (f110*fvu1u1u3)/3 + 
      (f110*fvu1u1u8)/3 + (f103*fvu1u1u9)/3 - 
      (f113*fvu1u1u10)/3 + ((f103 - f113)*tci12)/3) + 
    fvu1u1u9*(f106/3 - (f111*fvu1u1u10)/3 - (f114*tci12)/3);
L ieu0ueu5 = (-f20 - 10*f58 - 10*f59 + f60 + f61 - 
     f70 - f120 - f150 - f177)/3;
L ieum1ueu5 = 0;
L ieum2ueu5 = 0;
L ieu2uou5 = (-4*f109*fvu4u28)/3 - (4*f109*fvu4u39)/3 - 
    (2*f109*fvu4u49)/9 - (4*f109*fvu4u51)/9 - (f109*fvu4u80)/12 - 
    (7*f109*fvu4u83)/12 + (3*f109*fvu4u91)/4 - (f109*fvu4u93)/12 - 
    (4*f109*fvu4u100)/9 + (f109*fvu4u102)/2 - (f109*fvu4u111)/4 - 
    (f109*fvu4u113)/2 - f109*fvu4u114 + (2*f109*fvu4u129)/3 - 
    (13*f109*fvu4u132)/3 + (7*f109*fvu4u139)/2 - 
    (4*f109*fvu4u141)/3 - (4*f109*fvu4u146)/9 - 
    (7*f109*fvu4u148)/6 + (4*f109*fvu4u174)/3 + 
    (4*f109*fvu4u182)/3 + (2*f109*fvu4u190)/9 + 
    (4*f109*fvu4u192)/9 + (f109*fvu4u213)/12 + 
    (67*f109*fvu4u215)/12 - (9*f109*fvu4u219)/4 + 
    (f109*fvu4u221)/12 + (4*f109*fvu4u225)/9 + (f109*fvu4u233)/4 + 
    (f109*fvu4u234)/2 + f109*fvu4u235 + (10*f109*fvu4u244)/3 - 
    (2*f109*fvu4u246)/3 - 2*f109*fvu4u250 - (2*f109*fvu4u252)/3 - 
    (2*f109*fvu4u255)/9 - 2*f109*fvu4u273 + 2*f109*fvu4u277 + 
    fvu3u25*((-2*f109*fvu1u1u3)/3 + (2*f109*fvu1u1u6)/3 - 
      (2*f109*fvu1u1u7)/3) + fvu3u71*((2*f109*fvu1u1u3)/3 - 
      (2*f109*fvu1u1u6)/3 + (2*f109*fvu1u1u7)/3) + 
    fvu3u70*((f109*fvu1u1u6)/3 - (f109*fvu1u1u8)/3 - 
      (f109*fvu1u1u10)/3) + fvu3u45*(-(f109*fvu1u1u2) + 
      (f109*fvu1u1u3)/3 + (2*f109*fvu1u1u5)/3 + 
      (f109*fvu1u1u6)/3 - f109*fvu1u1u8 + (f109*fvu1u1u9)/3 - 
      (f109*fvu1u1u10)/3) + fvu3u23*(-(f109*fvu1u1u1)/3 - 
      (f109*fvu1u1u6)/3 + (f109*fvu1u1u8)/3 + 
      (f109*fvu1u1u10)/3) + fvu3u43*((f109*fvu1u1u2)/3 + 
      (f109*fvu1u1u4)/3 + (f109*fvu1u1u6)/3 - (f109*fvu1u1u7)/3 - 
      (2*f109*fvu1u1u9)/3 + (f109*fvu1u1u10)/3) + 
    fvu3u63*(f109*fvu1u1u2 - (3*f109*fvu1u1u3)/2 - 
      (2*f109*fvu1u1u5)/3 - (19*f109*fvu1u1u6)/6 - 
      (f109*fvu1u1u7)/3 - (2*f109*fvu1u1u8)/3 - 
      (f109*fvu1u1u9)/3 + (7*f109*fvu1u1u10)/2) + 
    (13*f109*tci11^3*tci12)/15 + 
    fvu3u62*((-2*f109*fvu1u1u2)/3 + f109*fvu1u1u3 + 
      (f109*fvu1u1u4)/3 - (2*f109*fvu1u1u5)/3 - 
      (2*f109*fvu1u1u6)/3 + (f109*fvu1u1u7)/3 + f109*fvu1u1u8 + 
      f109*fvu1u1u9 + (2*f109*fvu1u1u10)/3 - 2*f109*tci12) + 
    fvu3u78*(-(f109*fvu1u1u2)/3 + (f109*fvu1u1u3)/3 - 
      (f109*fvu1u1u4)/3 + (f109*fvu1u1u6)/3 - (f109*fvu1u1u8)/3 + 
      (2*f109*fvu1u1u9)/3 + (2*f109*tci12)/3) + 
    fvu3u80*((2*f109*fvu1u1u2)/3 - (f109*fvu1u1u3)/3 - 
      (f109*fvu1u1u4)/3 + (2*f109*fvu1u1u5)/3 - 
      (2*f109*fvu1u1u6)/3 + f109*fvu1u1u7 - (5*f109*fvu1u1u8)/3 + 
      f109*fvu1u1u9 + (8*f109*tci12)/3) - 
    (10*f109*tci11^2*tci21)/27 + 
    tci12*((16*f109*tci31)/5 + 16*f109*tci32) - 
    (2072*f109*tci41)/135 - (64*f109*tci42)/5 - 
    (16*f109*tci43)/3 + ((53*f109*tci11^3)/108 + 
      (5*f109*tci12*tci21)/3 - (12*f109*tci31)/5 + 
      16*f109*tci32)*tcr11 + (61*f109*tci11*tcr11^3)/
     180 + fvu1u1u10*((169*f109*tci11^3)/1620 + 
      (25*f109*tci12*tci21)/9 + (76*f109*tci31)/5 + 
      (19*f109*tci21*tcr11)/3 - (19*f109*tci11*tcr11^2)/
       60) + fvu1u1u2*((53*f109*tci11^3)/270 + 
      (72*f109*tci31)/5 + 6*f109*tci21*tcr11 - 
      (3*f109*tci11*tcr11^2)/10) + 
    fvu1u1u1*((2*f109*fvu3u25)/3 - (f109*fvu3u43)/3 + 
      (2*f109*fvu3u45)/3 - (f109*fvu3u62)/3 + (13*f109*fvu3u63)/6 + 
      (f109*fvu3u70)/3 - (2*f109*fvu3u71)/3 - (f109*fvu3u78)/3 - 
      (f109*fvu3u80)/3 + (19*f109*tci11^3)/108 - 
      (f109*tci12*tci21)/3 + 12*f109*tci31 + 
      5*f109*tci21*tcr11 - (f109*tci11*tcr11^2)/4) + 
    fvu1u1u5*((f109*tci11^3)/45 - (16*f109*tci12*tci21)/9 - 
      (16*f109*tci31)/5 - (4*f109*tci21*tcr11)/3 + 
      (f109*tci11*tcr11^2)/15) + 
    fvu1u1u8*((-133*f109*tci11^3)/810 + 
      (8*f109*tci12*tci21)/3 - (24*f109*tci31)/5 - 
      2*f109*tci21*tcr11 + (f109*tci11*tcr11^2)/10) + 
    fvu1u1u6*((-11*f109*tci11^3)/180 - f109*tci12*tci21 - 
      (36*f109*tci31)/5 - 3*f109*tci21*tcr11 + 
      (3*f109*tci11*tcr11^2)/20) + 
    fvu1u1u7*((-7*f109*tci11^3)/162 - (16*f109*tci12*tci21)/
       9 - 8*f109*tci31 - (10*f109*tci21*tcr11)/3 + 
      (f109*tci11*tcr11^2)/6) + 
    fvu1u1u9*((-19*f109*tci11^3)/270 - 
      (20*f109*tci12*tci21)/9 - (56*f109*tci31)/5 - 
      (14*f109*tci21*tcr11)/3 + (7*f109*tci11*tcr11^2)/
       30) + fvu1u1u3*((-391*f109*tci11^3)/1620 + 
      (f109*tci12*tci21)/3 - (84*f109*tci31)/5 - 
      7*f109*tci21*tcr11 + (7*f109*tci11*tcr11^2)/20) + 
    ((-218*f109*tci11^3)/243 - (40*f109*tci12*tci21)/3)*
     tcr12 + (-4*f109*tci11*tci12 - (40*f109*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f109*tci11*tci12)/15 - 
      3*f109*tci21 - 2*f109*tci11*tcr12) + 
    (130*f109*tci11*tcr33)/27;
L ieu1uou5 = 
   2*f109*fvu3u62 - (11*f109*tci11^3)/135 - 
    (4*f109*tci12*tci21)/3 - (48*f109*tci31)/5 - 
    4*f109*tci21*tcr11 + (f109*tci11*tcr11^2)/5;
L ieu0uou5 = 0;
L ieum1uou5 = 0;
L ieum2uou5 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou5+w^2*ieu1uou5+w^3*ieu0uou5+w^4*ieum1uou5+w^5*ieum2uou5;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou5a = K[w^1];
L ieu1uou5a = K[w^2];
L ieu0uou5a = K[w^3];
L ieum1uou5a = K[w^4];
L ieum2uou5a = K[w^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_odd.c> "%O"
#write <e5_odd.c> "return Eps5o2<T>("
#write <e5_odd.c> "%E", ieu2uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu0uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum2uou5a
#write <e5_odd.c> ");\n}"
L H=+u^1*ieu2ueu5+u^2*ieu1ueu5+u^3*ieu0ueu5+u^4*ieum1ueu5+u^5*ieum2ueu5;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu5a = H[u^1];
L ieu1ueu5a = H[u^2];
L ieu0ueu5a = H[u^3];
L ieum1ueu5a = H[u^4];
L ieum2ueu5a = H[u^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_even.c> "%O"
#write <e5_even.c> "return Eps5o2<T>("
#write <e5_even.c> "%E", ieu2ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu0ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum2ueu5a
#write <e5_even.c> ");\n}"
.end
