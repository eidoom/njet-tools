#!/usr/bin/env bash

OUT=header.cpp
rm -f ${OUT}
touch ${OUT}

l=1
dir=new_exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_epsexp_}
    h=${g%_lr*.m}
    d=hel_${h}

    j=0
    for i in {0..5}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(( $j+2**$i ))
        fi
    done

    echo $h $j

    perl -p -e "s|HH|$j|g;s|SSSSS|$h|g;" template_header.cpp >> $OUT
done

