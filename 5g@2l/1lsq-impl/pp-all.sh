#!/usr/bin/env bash

l=1
dir=new_exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_epsexp_}
    h=${g%_lr*.m}
    d=hel_${h}
    cd $d
    ../pp-form.sh ys.c
    ../pp-form.sh fs.c
    for i in {1..12}; do
        ../pp-form.sh e${i}_even.c
        ../pp-form.sh e${i}_odd.c
    done
    cd ..
done
