#include <array>
#include <cmath>
#include <iostream>
#include <numeric>

template <int mul>
struct Helicity : std::array<int, mul> {
    Helicity(const int e0,const int e1,const int e2,const int e3,const int e4)
: std::array<int, mul>({e0,e1,e2,e3,e4})
{
}

    int sign() const
    {
        return std::accumulate(this->cbegin(), this->cend(), 0);
    }

    int order() const
    {
        return std::abs(this->sign());
    }
};

template <int mul>
std::ostream& operator<<(std::ostream& out, const Helicity<mul>& hel)
{
    for (int h : hel) {
        out << (h == 1 ? '+' : '-');
    }
    return out;
}
