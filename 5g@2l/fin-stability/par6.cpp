#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "finrem/0q5g/0q5g-2l.h"
#include "chsums/NJetAccuracy.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

void run(const int start, const int end)
{
    std::cout << "Initialising amplitude classes..." << '\n';

    using TP = std::chrono::time_point<std::chrono::high_resolution_clock>;

    TP t0;
    TP t1;

    long d_specDD { 0 };
    long d_specQD { 0 };
    long d_specQQ { 0 };

    long d_elseDD { 0 };
    long d_elseQD { 0 };
    long d_elseQQ { 0 };

    const std::vector<double> scales2 { { 0 } };
    const double sqrtS { M_PI };
    const double mur { sqrtS / 2. };
    const int Nc { 3 };
    const int Nf { 0 };
    const double cutoff_errD { 1e-3 };
    const dd_real cutoff_errQ { static_cast<dd_real>(cutoff_errD) };

    NJetAccuracy<double>* const ampDD { NJetAccuracy<double>::template create<Amp0q5g_a2l<double, double>>() };
    ampDD->setMuR2(mur * mur);
    ampDD->setNf(Nf);
    ampDD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQD { NJetAccuracy<dd_real>::template create<Amp0q5g_a2l<dd_real, double>>() };
    ampQD->setMuR2(mur * mur);
    ampQD->setNf(Nf);
    ampQD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQQ { NJetAccuracy<dd_real>::template create<Amp0q5g_a2l<dd_real, dd_real>>() };
    ampQQ->setMuR2(mur * mur);
    ampQQ->setNf(Nf);
    ampQQ->setNc(Nc);

    // rseed = p
    for (int p { start }; p < end; ++p) {
        std::cout
            << "Evaluating point " << p << " ..." << '\n'
            << "  try coeff(double)+specialFn(double)..." << '\n';

        std::vector<MOM<double>> psD { njet_random_point<double>(p, sqrtS) };
        refineM(psD, psD, scales2);
        ampDD->setMomenta(psD);
        t0 = std::chrono::high_resolution_clock::now();
        ampDD->setSpecFuncs();
        t1 = std::chrono::high_resolution_clock::now();
        d_specDD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

        t0 = std::chrono::high_resolution_clock::now();
        ampDD->initFinRem();
        const double born_valD { ampDD->c0lx0l_fin() };
        const double virt_valD { ampDD->c1lx0l_fin() };
        const double virtsq_valD { ampDD->c1lx1l_fin() };
        const double dblvirt_valD { ampDD->c2lx0l_fin() };
        t1 = std::chrono::high_resolution_clock::now();
        d_elseDD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

        const double born_errD { std::abs(ampDD->c0lx0l_fin_error() / ampDD->c0lx0l_fin_value()) };
        const double virt_errD { std::abs(ampDD->c1lx0l_fin_error() / ampDD->c1lx0l_fin_value()) };
        const double virtsq_errD { std::abs(ampDD->c1lx1l_fin_error() / ampDD->c1lx1l_fin_value()) };
        const double dblvirt_errD { std::abs(ampDD->c2lx0l_fin_error() / ampDD->c2lx0l_fin_value()) };

        {
            std::ofstream o("result.DD." + std::to_string(start), std::ios::app);
            o.setf(std::ios_base::scientific);
            o.precision(16);
            o
                << p << ' '
                << born_valD << ' '
                << born_errD << ' '
                << virt_valD << ' '
                << virt_errD << ' '
                << virtsq_valD << ' '
                << virtsq_errD << ' '
                << dblvirt_valD << ' '
                << dblvirt_errD << ' '
                << d_specDD << ' '
                << d_elseDD << ' '
                << '\n';
        }

        if (dblvirt_errD < cutoff_errD) {
            std::cout
                << "    stability test passed" << '\n';
        } else {
            std::cout
                << "    stability test failed" << '\n'
                << "  try coeff(quad)+specialFn(double)..." << '\n';

            dd_real born_valQ;
            dd_real virt_valQ;
            dd_real virtsq_valQ;
            dd_real dblvirt_valQ;

            dd_real born_errQ;
            dd_real virt_errQ;
            dd_real virtsq_errQ;
            dd_real dblvirt_errQ;

            std::vector<MOM<dd_real>> psQ { njet_random_point<dd_real>(p, sqrtS) };
            refineM(psQ, psQ, scales2);

            ampQD->setMomenta(psQ);
            t0 = std::chrono::high_resolution_clock::now();
            ampQD->copySpecFuncs(ampDD);
            t1 = std::chrono::high_resolution_clock::now();
            d_specQD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

            t0 = std::chrono::high_resolution_clock::now();
            ampQD->initFinRem();
            born_valQ = ampQD->c0lx0l_fin();
            virt_valQ = ampQD->c1lx0l_fin();
            virtsq_valQ = ampQD->c1lx1l_fin();
            dblvirt_valQ = ampQD->c2lx0l_fin();
            t1 = std::chrono::high_resolution_clock::now();
            d_elseQD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

            born_errQ = abs(ampQD->c0lx0l_fin_error() / ampQD->c0lx0l_fin_value());
            virt_errQ = abs(ampQD->c1lx0l_fin_error() / ampQD->c1lx0l_fin_value());
            virtsq_errQ = abs(ampQD->c1lx1l_fin_error() / ampQD->c1lx1l_fin_value());
            dblvirt_errQ = abs(ampQD->c2lx0l_fin_error() / ampQD->c2lx0l_fin_value());

            {
                std::ofstream o("result.QD." + std::to_string(start), std::ios::app);
                o.setf(std::ios_base::scientific);
                o.precision(16);
                o
                    << p << ' '
                    << born_valQ << ' '
                    << born_errQ << ' '
                    << virt_valQ << ' '
                    << virt_errQ << ' '
                    << virtsq_valQ << ' '
                    << virtsq_errQ << ' '
                    << dblvirt_valQ << ' '
                    << dblvirt_errQ << ' '
                    << d_specQD << ' '
                    << d_elseQD << ' '
                    << '\n';
            }

            if (dblvirt_errQ < cutoff_errQ) {
                std::cout
                    << "    stability test passed" << '\n';

            } else {
                std::cout
                    << "    stability test failed" << '\n'
                    << "  try coeff(quad)+specialFn(quad)..." << '\n';

                ampQQ->setMomenta(psQ);
                t0 = std::chrono::high_resolution_clock::now();
                ampQQ->setSpecFuncs();
                t1 = std::chrono::high_resolution_clock::now();
                d_specQQ = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

                t0 = std::chrono::high_resolution_clock::now();
                ampQQ->initFinRem();
                born_valQ = ampQQ->c0lx0l_fin();
                virt_valQ = ampQQ->c1lx0l_fin();
                virtsq_valQ = ampQQ->c1lx1l_fin();
                dblvirt_valQ = ampQQ->c2lx0l_fin();
                t1 = std::chrono::high_resolution_clock::now();
                d_elseQQ = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

                born_errQ = abs(ampQQ->c0lx0l_fin_error() / ampQQ->c0lx0l_fin_value());
                virt_errQ = abs(ampQQ->c1lx0l_fin_error() / ampQQ->c1lx0l_fin_value());
                virtsq_errQ = abs(ampQQ->c1lx1l_fin_error() / ampQQ->c1lx1l_fin_value());
                dblvirt_errQ = abs(ampQQ->c2lx0l_fin_error() / ampQQ->c2lx0l_fin_value());

                {
                    std::ofstream o("result.QQ." + std::to_string(start), std::ios::app);
                    o.setf(std::ios_base::scientific);
                    o.precision(16);
                    o
                        << p << ' '
                        << born_valQ << ' '
                        << born_errQ << ' '
                        << virt_valQ << ' '
                        << virt_errQ << ' '
                        << virtsq_valQ << ' '
                        << virtsq_errQ << ' '
                        << dblvirt_valQ << ' '
                        << dblvirt_errQ << ' '
                        << d_specQQ << ' '
                        << d_elseQQ << ' '
                        << '\n';
                }

                if (dblvirt_errQ < cutoff_errQ) {
                    std::cout
                        << "    stability test passed" << '\n';
                } else {
                    std::cout
                        << "    stability test failed (END)" << '\n';
                }
            }
        }
    }
}

void run_from_file(const std::string& filename, const int start, const int end)
{
    std::cout << "Reading points from " << filename << "..." << '\n';
    std::ifstream data(filename);
    std::string line;
    std::vector<int> points;
    while (std::getline(data, line)) {
        points.push_back(std::stod(line));
    }

    std::cout << "Initialising amplitude classes..." << '\n';

    using TP = std::chrono::time_point<std::chrono::high_resolution_clock>;

    TP t0;
    TP t1;

    long d_specDD { 0 };
    long d_specQD { 0 };
    long d_specQQ { 0 };

    long d_elseDD { 0 };
    long d_elseQD { 0 };
    long d_elseQQ { 0 };

    const double sqrtS { 1. };
    const double mur { sqrtS / 2. };
    const int Nc { 3 };
    const int Nf { 0 };
    const double cutoff_errD { 1e-3 };
    const dd_real cutoff_errQ { static_cast<dd_real>(cutoff_errD) };

    NJetAccuracy<double>* ampDD { NJetAccuracy<double>::template create<Amp0q5g_a2l<double, double>>() };
    ampDD->setMuR2(mur * mur);
    ampDD->setNf(Nf);
    ampDD->setNc(Nc);

    NJetAccuracy<dd_real>* ampQD { NJetAccuracy<dd_real>::template create<Amp0q5g_a2l<dd_real, double>>() };
    ampQD->setMuR2(mur * mur);
    ampQD->setNf(Nf);
    ampQD->setNc(Nc);

    NJetAccuracy<dd_real>* ampQQ { NJetAccuracy<dd_real>::template create<Amp0q5g_a2l<dd_real, dd_real>>() };
    ampQQ->setMuR2(mur * mur);
    ampQQ->setNf(Nf);
    ampQQ->setNc(Nc);

    // rseed = p
    for (int i { start }; i < end; ++i) {
        const int p { points[i] };
        std::cout
            << "Evaluating point " << p << " ..." << '\n'
            << "  try coeff(double)+specialFn(double)..." << '\n';

        const std::vector<MOM<double>> psD { njet_random_point<double>(p, sqrtS) };
        ampDD->setMomenta(psD);
        t0 = std::chrono::high_resolution_clock::now();
        ampDD->setSpecFuncs();
        t1 = std::chrono::high_resolution_clock::now();
        d_specDD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

        t0 = std::chrono::high_resolution_clock::now();
        ampDD->initFinRem();
        const double born_valD { ampDD->c0lx0l_fin() };
        const double virt_valD { ampDD->c1lx0l_fin() };
        const double virtsq_valD { ampDD->c1lx1l_fin() };
        const double dblvirt_valD { ampDD->c2lx0l_fin() };
        t1 = std::chrono::high_resolution_clock::now();
        d_elseDD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

        const double born_errD { std::abs(ampDD->c0lx0l_fin_error() / ampDD->c0lx0l_fin_value()) };
        const double virt_errD { std::abs(ampDD->c1lx0l_fin_error() / ampDD->c1lx0l_fin_value()) };
        const double virtsq_errD { std::abs(ampDD->c1lx1l_fin_error() / ampDD->c1lx1l_fin_value()) };
        const double dblvirt_errD { std::abs(ampDD->c2lx0l_fin_error() / ampDD->c2lx0l_fin_value()) };

        if (dblvirt_errD < cutoff_errD) {
            std::cout
                << "    stability test passed" << '\n';

            std::ofstream o("result" + std::to_string(start), std::ios::app);
            o.setf(std::ios_base::scientific);
            o.precision(16);

            o
                << p << ' '
                << born_valD << ' '
                << born_errD << ' '
                << virt_valD << ' '
                << virt_errD << ' '
                << virtsq_valD << ' '
                << virtsq_errD << ' '
                << dblvirt_valD << ' '
                << dblvirt_errD << ' '
                << d_specDD << ' '
                << d_elseDD << ' '
                << '\n';
        } else {
            std::cout
                << "    stability test failed" << '\n'
                << "  try coeff(quad)+specialFn(double)..." << '\n';

            dd_real born_valQ;
            dd_real virt_valQ;
            dd_real virtsq_valQ;
            dd_real dblvirt_valQ;

            dd_real born_errQ;
            dd_real virt_errQ;
            dd_real virtsq_errQ;
            dd_real dblvirt_errQ;

            const std::vector<MOM<dd_real>> psQ { njet_random_point<dd_real>(p, sqrtS) };

            ampQD->setMomenta(psQ);
            t0 = std::chrono::high_resolution_clock::now();
            ampQD->copySpecFuncs(ampDD);
            t1 = std::chrono::high_resolution_clock::now();
            d_specQD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

            t0 = std::chrono::high_resolution_clock::now();
            ampQD->initFinRem();
            born_valQ = ampQD->c0lx0l_fin();
            virt_valQ = ampQD->c1lx0l_fin();
            virtsq_valQ = ampQD->c1lx1l_fin();
            dblvirt_valQ = ampQD->c2lx0l_fin();
            t1 = std::chrono::high_resolution_clock::now();
            d_elseQD = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

            born_errQ = abs(ampQD->c0lx0l_fin_error() / ampQD->c0lx0l_fin_value());
            virt_errQ = abs(ampQD->c1lx0l_fin_error() / ampQD->c1lx0l_fin_value());
            virtsq_errQ = abs(ampQD->c1lx1l_fin_error() / ampQD->c1lx1l_fin_value());
            dblvirt_errQ = abs(ampQD->c2lx0l_fin_error() / ampQD->c2lx0l_fin_value());

            if (dblvirt_errQ < cutoff_errQ) {
                std::cout
                    << "    stability test passed" << '\n';

                std::ofstream o("result" + std::to_string(start), std::ios::app);
                o.setf(std::ios_base::scientific);
                o.precision(16);

                o
                    << p << ' '
                    << born_valQ << ' '
                    << born_errQ << ' '
                    << virt_valQ << ' '
                    << virt_errQ << ' '
                    << virtsq_valQ << ' '
                    << virtsq_errQ << ' '
                    << dblvirt_valQ << ' '
                    << dblvirt_errQ << ' '
                    << d_specQD << ' '
                    << d_elseQD << ' '
                    << '\n';

            } else {
                std::cout
                    << "    stability test failed" << '\n'
                    << "  try coeff(quad)+specialFn(quad)..." << '\n';

                ampQQ->setMomenta(psQ);
                t0 = std::chrono::high_resolution_clock::now();
                ampQQ->setSpecFuncs();
                t1 = std::chrono::high_resolution_clock::now();
                d_specQQ = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

                t0 = std::chrono::high_resolution_clock::now();
                ampQQ->initFinRem();
                born_valQ = ampQQ->c0lx0l_fin();
                virt_valQ = ampQQ->c1lx0l_fin();
                virtsq_valQ = ampQQ->c1lx1l_fin();
                dblvirt_valQ = ampQQ->c2lx0l_fin();
                t1 = std::chrono::high_resolution_clock::now();
                d_elseQQ = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

                born_errQ = abs(ampQQ->c0lx0l_fin_error() / ampQQ->c0lx0l_fin_value());
                virt_errQ = abs(ampQQ->c1lx0l_fin_error() / ampQQ->c1lx0l_fin_value());
                virtsq_errQ = abs(ampQQ->c1lx1l_fin_error() / ampQQ->c1lx1l_fin_value());
                dblvirt_errQ = abs(ampQQ->c2lx0l_fin_error() / ampQQ->c2lx0l_fin_value());

                std::ofstream o("result" + std::to_string(start), std::ios::app);
                o.setf(std::ios_base::scientific);
                o.precision(16);

                o
                    << p << ' '
                    << born_valQ << ' '
                    << born_errQ << ' '
                    << virt_valQ << ' '
                    << virt_errQ << ' '
                    << virtsq_valQ << ' '
                    << virtsq_errQ << ' '
                    << dblvirt_valQ << ' '
                    << dblvirt_errQ << ' '
                    << d_specQQ << ' '
                    << d_elseQQ << ' '
                    << '\n';

                if (dblvirt_errQ < cutoff_errQ) {
                    std::cout
                        << "    stability test passed" << '\n';
                } else {
                    std::cout
                        << "    stability test failed (END)" << '\n';
                }
            }
        }
    }
}

int main(int argc, char* argv[])
{
    std::cout << '\n';

    if (argc == 3) {
        const int start { std::atoi(argv[1]) };
        const int end { std::atoi(argv[2]) };
        run(start, end);
    } else if (argc == 4) {
        const std::string filename { argv[1] };
        const int start { std::atoi(argv[2]) };
        const int end { std::atoi(argv[3]) };
        run_from_file(filename, start, end);
    } else {
        std::cout << "Provide arguments" << '\n';
    }

    std::cout << '\n';
}
