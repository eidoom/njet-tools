#!/usr/bin/env bash

bad_points=$1

strt=0
points=`wc -l < $bad_points`
cores=$2
clo=$((cores-1))

points_per_core=$((points/clo))
maj=$((points_per_core*clo))
rem=$((points-maj))

echo Requested points from file $bad_points \($points points\) over $cores cores
echo So get $points_per_core points per core for first $clo cores \($maj points\) plus $rem points on last core

n=0
for (( i=$strt; i<$(($maj)); i+=$points_per_core )); do
    n=$((n+1))
    ./par6_f ${bad_points} ${i} $((${i} + ${points_per_core})) >run.par6.$n.log 2>&1 &
done
n=$((n+1))
./par6_f ${bad_points} ${maj} $((${maj} + ${rem})) >run.par6.$n.log 2>&1 &
