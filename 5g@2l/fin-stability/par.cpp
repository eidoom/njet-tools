#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "analytic/0q5g-2l-analytic.h"
#include "chsums/NJetAccuracy.h"
#include "ngluon2/Mom.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

// rseed = p
template <typename T>
void run(const int start, const int end)
{
    std::cout << "Initialising amplitude class..." << '\n';
    const int legs { 5 };
    const double sqrtS { 1. };
    const double mur { sqrtS / 2. };
    const int Nc { 3 };
    const int Nf { 0 };

    NJetAccuracy<T>* amp { NJetAccuracy<T>::template create<Amp0q5g_a2l<T, T>>() };
    amp->setMuR2(mur * mur);
    amp->setNf(Nf);
    amp->setNc(Nc);

    for (int p { start }; p < end; ++p) {
        std::cout << "Evaluating point " << p << " ..." << '\n';

        const std::vector<MOM<T>> momenta { njet_random_point<T>(p) };

        amp->setMomenta(momenta);
        amp->setSpecFuncVals();
        amp->setSpecFuncMons();

        amp->born_fin();
        amp->virt_fin();
        amp->virtsq_fin();
        amp->dblvirt_fin();

        std::ofstream o("result" + std::to_string(start), std::ios::app);
        o.setf(std::ios_base::scientific);
        o.precision(16);

        o
            << p << ' '
            << amp->born_fin_value() << ' '
            << amp->born_fin_error() / amp->born_fin_value() << ' '
            << amp->virt_fin_value() << ' '
            << amp->virt_fin_error() / amp->virt_fin_value() << ' '
            << amp->virtsq_fin_value() << ' '
            << amp->virtsq_fin_error() / amp->virtsq_fin_value() << ' '
            << amp->dblvirt_fin_value() << ' '
            << amp->dblvirt_fin_error() / amp->dblvirt_fin_value() << ' '
            << '\n';
    }
}

int main(int argc, char* argv[])
{
    assert(argc == 3);
    const int start { std::atoi(argv[1]) };
    const int end { std::atoi(argv[2]) };

    run<double>(start, end);
}
