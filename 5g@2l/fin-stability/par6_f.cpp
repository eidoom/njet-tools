#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "analytic/0q5g-2l-analytic.h"
#include "chsums/NJetAccuracy.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

template <typename T, typename P>
void run_list(const std::vector<int>& points, const int start)
{
    std::cout << "Initialising amplitude class..." << '\n';

    using TP = std::chrono::time_point<std::chrono::high_resolution_clock>;

    TP t0;
    TP t1;

    long d_spec { 0 };
    long d_else { 0 };

    const double sqrtS { 1. };
    const double mur { sqrtS / 2. };
    const int Nc { 3 };
    const int Nf { 0 };

    NJetAccuracy<T>* amp { NJetAccuracy<T>::template create<Amp0q5g_a2l<T, P>>() };
    amp->setMuR2(mur * mur);
    amp->setNf(Nf);
    amp->setNc(Nc);

    // rseed = p
    for (int p : points) {
        std::cout
            << "Evaluating point " << p << " ..." << '\n';

        const std::vector<MOM<T>> ps { njet_random_point<T>(p, sqrtS) };
        amp->setMomenta(ps);
        t0 = std::chrono::high_resolution_clock::now();
        amp->setSpecFuncVals();
        t1 = std::chrono::high_resolution_clock::now();
        d_spec = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();
        t0 = std::chrono::high_resolution_clock::now();
        amp->setSpecFuncMons();

        const T born_val { amp->born_fin() };
        const T virt_val { amp->virt_fin() };
        const T virtsq_val { amp->virtsq_fin() };
        const T dblvirt_val { amp->dblvirt_fin() };
        t1 = std::chrono::high_resolution_clock::now();
        d_else = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

        const T born_err { abs(amp->born_fin_error() / amp->born_fin_value()) };
        const T virt_err { abs(amp->virt_fin_error() / amp->virt_fin_value()) };
        const T virtsq_err { abs(amp->virtsq_fin_error() / amp->virtsq_fin_value()) };
        const T dblvirt_err { abs(amp->dblvirt_fin_error() / amp->dblvirt_fin_value()) };

        std::ofstream o("result" + std::to_string(start), std::ios::app);
        o.setf(std::ios_base::scientific);
        o.precision(16);

        o
            << p << ' '
            << born_val << ' '
            << born_err << ' '
            << virt_val << ' '
            << virt_err << ' '
            << virtsq_val << ' '
            << virtsq_err << ' '
            << dblvirt_val << ' '
            << dblvirt_err << ' '
            << d_spec << ' '
            << d_else << ' '
            << '\n';
    }
}

int main(int argc, char* argv[])
{
    std::cout << '\n';
    assert(argc == 4);

    const std::string filename { argv[1] };
    std::cout << "Reading points from " << filename << "..." << '\n';
    std::ifstream data(filename);
    std::string line;
    std::vector<int> points;
    while (std::getline(data, line)) {
        points.push_back(std::stod(line));
    }
    const int start { std::atoi(argv[2]) };
    const int end { std::atoi(argv[3]) };
    run_list<dd_real, dd_real>(std::vector<int>(points.cbegin() + start, points.cbegin() + end), start);

    std::cout << '\n';
}
