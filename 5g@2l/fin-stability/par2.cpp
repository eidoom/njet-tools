#include <array>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <random>
#include <vector>

#include "analytic/0q5g-2l-analytic.h"
#include "chsums/NJetAccuracy.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

template <typename T>
void run()
{
    const int legs { 5 };
    const double sqrtS { 1. };
    const double mur { sqrtS / 2. };

    NJetAccuracy<T>* amp { NJetAccuracy<T>::template create<Amp0q5g_a2l<T, T>>() };
    amp->setMuR2(mur * mur);
    amp->setNc(3);
    amp->setNf(0);

    // random point
    std::random_device dev;
    std::mt19937_64 rng(dev());
    const T boundary { 0.01 };
    std::uniform_real_distribution<T> dist_y1(boundary, 1. - boundary);
    std::uniform_real_distribution<T> dist_a(boundary, M_PI - boundary);

    const T y1 { dist_y1(rng) };
    std::uniform_real_distribution<T> dist_y2(boundary, 1. - y1 - boundary);
    const T y2 { dist_y2(rng) };
    const T theta { dist_a(rng) };
    const T alpha { dist_a(rng) };

    const std::array<MOM<T>, legs> moms { phase_space_point(y1, y2, theta, alpha, sqrtS) };

    amp->setMomenta(moms.data());
    amp->setSpecFuncVals();
    amp->setSpecFuncMons();

    amp->born_fin();
    amp->virt_fin();
    amp->virtsq_fin();
    amp->dblvirt_fin();

    std::ofstream o("result_" + std::to_string(y1) + "_" + std::to_string(y2) + "_" + std::to_string(theta) + "_" + std::to_string(alpha), std::ios::app);
    o.setf(std::ios_base::scientific);
    o.precision(16);

    o
        << amp->born_fin_value() << ' '
        << amp->born_fin_error() / amp->born_fin_value() << ' '
        << amp->virt_fin_value() << ' '
        << amp->virt_fin_error() / amp->virt_fin_value() << ' '
        << amp->virtsq_fin_value() << ' '
        << amp->virtsq_fin_error() / amp->virtsq_fin_value() << ' '
        << amp->dblvirt_fin_value() << ' '
        << amp->dblvirt_fin_error() / amp->dblvirt_fin_value() << ' '
        << '\n';
}

int main(int argc, char* argv[])
{
    assert(argc == 1);
    run<double>();
}
