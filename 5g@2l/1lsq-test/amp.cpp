#include <array>
#include <cassert>
#include <chrono>
#include <complex>
#include <iostream>
#include <random>

#include "analytic/0q5g-2l-analytic.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

const int N { 5 };

template <typename T>
Eps5<T> run(const std::array<MOM<T>, N> mom, const int Nc, const T mur)
{
    const T scale { 1. };
    Amp0q5g_a2l<T> amp(scale);
    amp.setNf(0);
    amp.setNc(Nc);
    amp.setMuR2(mur * mur);
    amp.setMomenta(mom.data());
    // amp.setpenta1l();
    amp.setpenta2l();

    // T born { amp.born() };
    // std::cout << "B:  " << born << '\n';

    //Eps3<T> virt2 { amp.virt_part() };
    //std::cout << "V: part  " << virt2 << '\n';

    // Eps5<T> virtsq2 { amp.virtsq_part() };
    // std::cout << "V^2: part  " << virtsq2 << "\n";

    Eps5<T> dblloop { amp.dblvirt() };
    std::cout << "2V: " << dblloop << '\n';

    Eps5<T> virtsqe2 { amp.virtsqe2() };
    std::cout << "V^2: penta_e2  " << virtsqe2 << "\n";
    Eps5<T> virtsq2 { amp.virtsq_penta() };
    std::cout << "V^2: penta " << virtsq2 << '\n';
    Eps5<T> virtsq { amp.virtsq_part() };
    std::cout << "V^2: part  " << virtsq << "\n";

    Eps3<T> virt { amp.virt_part() };
    std::cout << "V: part  " << virt << '\n';
    Eps3<T> virt2 { amp.virt_penta() };
    std::cout << "V: penta " << virt2 << '\n';
    Eps3<T> virt3 { amp.virt_prim() };
    std::cout << "V: prim " << virt3 << '\n';

    T born { amp.born() };
    std::cout << "B:  " << born << '\n';

    // return hamp_virt;
    return std::complex<T>(1.);
}

template <typename T>
void loop_matrix(const int num_ps = 10, const int num_nc = 1, const int num_mur = 1)
{
    std::random_device dev;
    std::mt19937_64 rng(dev());
    const T boundary { 0.01 };
    std::uniform_real_distribution<T> dist_y1(boundary, 1. - boundary);
    std::uniform_real_distribution<T> dist_a(boundary, M_PI - boundary);

    std::cout << '\n';
    // std::cout << '{' << '\n';
    for (int i { 0 }; i < num_ps; ++i) {

        T y1 { dist_y1(rng) };
        std::uniform_real_distribution<T> dist_y2(boundary, 1. - y1 - boundary);
        T y2 { dist_y2(rng) };
        T theta { dist_a(rng) };
        T alpha { dist_a(rng) };
        // std::cout << y1 << " "
        //           << y2 << " "
        //           << theta << " "
        //           << alpha << " "
        //           << '\n';
        y1 = 2.5466037534799052e-01;
        y2 = 3.9999592899158604e-01;
        theta = 1.2398476487204491e+00;
        alpha = 4.3891163982516607e-01;
        const std::array<MOM<T>, N> moms { phase_space_point(y1, y2, theta, alpha) };

        for (int Nc { 3 }; Nc < 3 * std::pow(10, num_nc); Nc *= 10) {
            for (T mur { 1. }; mur < std::pow(10., num_mur); mur *= 10.) {
                // std::cout << "{" << '\n';

                const Eps5<T> hamp_virt { run<T>(moms, Nc, mur) };

                // std::cout << "{" << '\n';
                // for (int j { 0 }; j < N - 1; ++j) {
                //     std::cout << moms[j] << ',' << '\n';
                // }
                // std::cout << moms[N - 1] << "\n},\n";

                // std::cout << mur << "," << '\n';

                // std::cout << Nc << "," << '\n';

                // std::cout << hamp_virt << '\n';

                // std::cout << "}," << '\n';
            }
        }
    }
    // std::cout << '}' << '\n';
    std::cout << '\n';
}

int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);

    loop_matrix<double>(1, 1, 1);
}
