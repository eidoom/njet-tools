#!/usr/bin/env bash

perl -0777p -e "s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com \*\)||g; \
            s|x\[(\d)\]|x\1|g; \
            s|s\[(\d), (\d)\]|s\1\2|g; \
            s|s32|s23|g; \
            s|pow\[(.*?), (-?\d)\]|pow(\1, \2)|g; \
            s|pow\[(.*?), (-?\d)\]|pow(\1, \2)|g; \
            s|log\[(.*?)\]|clog(\1)|g; \
            s|([\s-])(\d)([/*])|\1T(\2.)\3|g; \
            # s|\*pow\[(.*?), \-1\]|/ \1|g; \
            " $1 >$2
