#-

S x1,x2,x3,x4,x5;

L rat1 = 64 * x1^5 * x2^2 * x3^2 * x5^2 / (x1^3 * x2^2 * x3);

Format O4;
.sort

Format C;

ExtraSymbols,array,Z1;
#optimize rat1
#write <optimize_ppppp.c> "std::array<TreeValue,`optimmaxvar_'> Z1;"
#write <optimize_ppppp.c> "%O"
#write <optimize_ppppp.c> "const TreeValue rat1{%E};\n", rat1

.end
