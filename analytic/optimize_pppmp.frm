#-

S x1,x2,x3,x4,x5;

L ratN=i_*(64*x1*x2*((1 + x3)*x4 + x2*x3*(-1 + x5))*x5*(x2 - x4 + x5 + x3*x5 + 
   x2*x3*x5));

L ratD=((1 + x3)*(1 + x3 + x2*x3)*(x4 + x2*(-1 + x5))^2);

Format O4;
.sort

Format C;

ExtraSymbols,array,Z;

#optimize ratN
#write <optimize_pppmp.c> "%O"
#write <optimize_pppmp.c> "const TreeValue ratN{%E};\n", ratN

#optimize ratD
#write <optimize_pppmp.c> "%O"
#write <optimize_pppmp.c> "const TreeValue ratD{%E};\n", ratD

#write <optimize_pppmp.c> "const LoopValue amp{ratN/ratD};\n"

.end
