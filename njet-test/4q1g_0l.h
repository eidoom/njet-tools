#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

#include "finrem/4q1g/uUdDg/4q1g-uUdDg.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

template <typename T> void run() {
  std::cout << std::scientific << std::setprecision(16);

  PhaseSpace<T> ps(5, 1, 1.);
  std::vector<MOM<T>> momenta{ps.getPSpoint()};
  const std::vector<double> scales2{{0.}};
  refineM(momenta, momenta, scales2);

  Amp4q1g_uUdDg_a2l<T, T> amp;

  amp.setMuR2(std::pow(91.188, 2));
  amp.setNf(5);
  amp.setNc(3);

  amp.setMomenta(momenta);
  amp.initFinRem();

  std::cout << "born:    " << amp.c0lx0l_fin() << '\n' << '\n';
}
