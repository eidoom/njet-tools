#include <cassert>
#include <iostream>

#include "interface.h"

constexpr int me = 0;
constexpr int cc = 10;
constexpr int sc = 20;
constexpr int cs = 30;

enum p {
    GGAA = me,
    GGGAA,
    GGGGAA,
    GGAAcc01 = cc,
    GGGAAcc01,
    GGGAAcc02,
    GGGAAcc12,
    GGAAsc01 = sc,
    GGGAAsc01,
    GGGAAsc02,
    GGGAAsc12,
    GGAAcs0 = cs,
    GGAAcs1,
    GGGAAcs0,
    GGGAAcs1,
    GGGAAcs2,
};

void NJetSherpaInterface::initNJet()
{
    std::cout.setf(std::ios_base::scientific);
    std::cout.precision(16);
    std::cout.sync_with_stdio(false);
    std::cout << "~ NJet/Sherpa ng2A interface initialised ~\n";
}

int NJetSherpaInterface::getProcessLO(
    const std::string& particles,
    const int alphaS,
    const int alpha)
{
    if (particles == "21 21 -> 22 22" && alphaS == 2 && alpha == 2) {
        return p::GGAA;
    } else if (particles == "21 21 -> 21 22 22" && alphaS == 3 && alpha == 2) {
        return p::GGGAA;
    } else if (particles == "21 21 -> 21 21 22 22" && alphaS == 4 && alpha == 2) {
        return p::GGGGAA;
    } else {
        std::cout << "Unrecognised process!\n";
        std::exit(1);
    }
}

int NJetSherpaInterface::getProcessCC(
    const std::string& particles,
    const int alphaS,
    const int alpha,
    const int i,
    const int k)
{
    if (particles == "21 21 -> 22 22" && alphaS == 2 && alpha == 2 && ((i == 0 && k == 1) || (i == 1 && k == 0))) {
        return p::GGAAcc01;
    } else if (particles == "21 21 -> 21 22 22" && alphaS == 3 && alpha == 2) {
        if ((i == 0 && k == 1) || (i == 1 && k == 0)) {
            return p::GGGAAcc01;
        } else if ((i == 0 && k == 2) || (i == 2 && k == 0)) {
            return p::GGGAAcc02;
        } else if ((i == 1 && k == 2) || (i == 2 && k == 1)) {
            return p::GGGAAcc12;
        }
    }
    std::cout << "Unrecognised process!\n";
    std::exit(1);
}

int NJetSherpaInterface::getProcessSC(
    const std::string& particles,
    const int alphaS,
    const int alpha,
    const int i,
    const int k)
{
    if (particles == "21 21 -> 22 22" && alphaS == 2 && alpha == 2 && ((i == 0 && k == 1) || (i == 1 && k == 0))) {
        return p::GGAAsc01;
    } else if (particles == "21 21 -> 21 22 22" && alphaS == 3 && alpha == 2) {
        if ((i == 0 && k == 1) || (i == 1 && k == 0)) {
            return p::GGGAAsc01;
        } else if ((i == 0 && k == 2) || (i == 2 && k == 0)) {
            return p::GGGAAsc02;
        } else if ((i == 1 && k == 2) || (i == 2 && k == 1)) {
            return p::GGGAAsc12;
        }
    }
    std::cout << "Unrecognised process!\n";
    std::exit(1);
}

int NJetSherpaInterface::getProcessCS(
    const std::string& particles,
    const int alphaS,
    const int alpha,
    const int i)
{
    if (particles == "21 21 -> 22 22" && alphaS == 2 && alpha == 2) {
        switch (i) {
        case 0:
            return p::GGAAcs0;
        case 1:
            return p::GGAAcs1;
        }
    } else if (particles == "21 21 -> 21 22 22" && alphaS == 3 && alpha == 2) {
        switch (i) {
        case 0:
            return p::GGGAAcs0;
        case 1:
            return p::GGGAAcs1;
        case 2:
            return p::GGGAAcs2;
        }
    }
    std::cout << "Unrecognised process!\n";
    std::exit(1);
}

std::complex<double> NJetSherpaInterface::evalProcess(
    const int id,
    const std::vector<std::array<double, 4>>& mom,
    const double MuR,
    const double alphaS,
    const double alpha)
{
    std::string proc;
    int ii { 0 };
    int jj { 0 };
    switch (id) {

    case p::GGAA:
        proc = "2g2A";
        break;
    case p::GGGAA:
        proc = "3g2A";
        break;
    case p::GGGGAA:
        proc = "4g2A";
        break;

    case p::GGAAcc01:
        proc = "2g2Acc";
        ii = 0;
        jj = 1;
        break;
    case p::GGGAAcc01:
        proc = "3g2Acc";
        ii = 0;
        jj = 1;
        break;
    case p::GGGAAcc02:
        proc = "3g2Acc";
        ii = 0;
        jj = 2;
        break;
    case p::GGGAAcc12:
        proc = "3g2Acc";
        ii = 1;
        jj = 2;
        break;

    case p::GGAAsc01:
        proc = "2g2Asc";
        ii = 0;
        jj = 1;
        break;
    case p::GGGAAsc01:
        proc = "3g2Asc";
        ii = 0;
        jj = 1;
        break;
    case p::GGGAAsc02:
        proc = "3g2Asc";
        ii = 0;
        jj = 2;
        break;
    case p::GGGAAsc12:
        proc = "3g2Asc";
        ii = 1;
        jj = 2;
        break;

    case p::GGAAcs0:
        proc = "2g2Acs";
        ii = 0;
        break;
    case p::GGAAcs1:
        proc = "2g2Acs";
        ii = 1;
        break;
    case p::GGGAAcs0:
        proc = "3g2Acs";
        ii = 0;
        break;
    case p::GGGAAcs1:
        proc = "3g2Acs";
        ii = 1;
        break;
    case p::GGGAAcs2:
        proc = "3g2Acs";
        ii = 2;
        break;

    default:
        std::cout << "Unrecognised process!\n";
        std::exit(1);
    }
    std::cout << "# " << proc << ((id < cc) ? "" : std::to_string(ii) + ((id < cs) ? std::to_string(jj) : "")) << "\n";
    std::string contract { "OLE_contract_" + proc + ".lh" };

    int rStatus;
    OLP_Start(contract.c_str(), &rStatus);
    assert(rStatus);

    const int mul = mom.size();
    const int n { 5 };
    const int d { 4 };
    double LHMomenta[mul * n];
    for (int p { 0 }; p < mul; ++p) {
        for (int mu { 0 }; mu < d; ++mu) {
            LHMomenta[mu + p * n] = mom[p][mu];
        }
        // Set masses
        LHMomenta[d + p * n] = 0.;
    }

    const int channel { 1 };
    // 5 for e^-4, e^-3, e^-2, e^-1, finite
    const int epslen { 5 };
    const int nn = 2 * epslen + ((id < cc) ? 1 : 0);
    double out[nn] {};
    std::complex<double> outc[2 * epslen] {};
    std::complex<double> outci[2 * epslen * 16] {};
    double acc { 0. };

    const double zero { 0. };

    OLP_SetParameter("alphas", &alphaS, &zero, &rStatus);
    assert(rStatus == 1);

    OLP_SetParameter("alpha", &alpha, &zero, &rStatus);
    assert(rStatus == 1);

    if (id >= cc) {
        const double iid { static_cast<double>(ii) };
        const double jjd { static_cast<double>(jj) };
        OLP_SetParameter("#ij", &iid, &jjd, &rStatus);
        assert(rStatus == 1);
    }

    if (id >= cs) {
        OLP_EvalSubProcess2(&channel, LHMomenta, &MuR, reinterpret_cast<double*>(&outci[0]), &acc);
    } else if (id >= sc) {
        OLP_EvalSubProcess2(&channel, LHMomenta, &MuR, reinterpret_cast<double*>(&outc[0]), &acc);
    } else {
        OLP_EvalSubProcess2(&channel, LHMomenta, &MuR, out, &acc);
    }

#ifdef DEBUG
    if (id >= cs) {
        for (int i { 0 }; i < 4; ++i) {
            for (int j { 0 }; j < 4; ++j) {
                for (int a { 0 }; a < epslen; ++a) {
                    std::cout << "CS^" << ii << "_" << i << "," << j << "(e^-" << epslen - 1 - a << ") = "
                              << outci[epslen * (i + n * j) + a] << " +- "
                              << 100 * outci[16 * epslen + epslen * (i + n * j) + a].real() << " %\n";
                }
            }
        }
    } else if (id >= sc) {
        for (int a { 0 }; a < epslen; ++a) {
            std::cout << "SC(" << ii << "," << jj << ")(e^-" << epslen - 1 - a << ") = "
                      << (outc[a].real() < 0 ? "" : " ") << outc[a].real()
                      << " " << (outc[a].imag() < 0 ? "-" : "+") << " " << std::abs(outc[a].imag()) << "i"
                      << " +- " << 100 * outc[epslen + a].real() << " %\n";
        }
    } else if (id >= cc && id < sc) {
        for (int a { 0 }; a < epslen; ++a) {
            std::cout << "CC(" << ii << "," << jj << ")(e^-" << epslen - 1 - a << ") = " << (out[a] < 0 ? "" : " ") << out[a]
                      << " +- " << 100 * out[epslen + a] << " %\n";
        }
    } else {
        for (int a { 0 }; a < epslen; ++a) {
            std::cout << "Loop(-" << epslen - 1 - a << ") = " << (out[a] < 0 ? "" : " ") << out[a]
                      << " +- " << 100 * out[epslen + 1 + a] << " %\n";
        }
    }
#endif

    if (id >= cs) {
        std::cout << "Warning: No return value currently configured for CS. Returning zero.\n";
        return std::complex<double>();
    } else if (id >= sc) {
        return outc[epslen - 1];
    } else {
        return out[epslen - 1];
    }
}

MOM<double> NJetSherpaInterface::convertMom(const std::array<double, 4>& mom)
{
    return MOM<double>(mom[0], mom[1], mom[2], mom[3]);
}
