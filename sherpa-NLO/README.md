# sherpa-NLO

* NLO real corrections to loop-induced diphoton+gluon process from NJet, interfaced to Sherpa for integration.

## Notes

* Colourless legs must come at the end in the order file

## TODO

* Use single order file with channels - make channels order match enum
