(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[4], f[5], f[3], f[6], f[2], f[1]}, {f[9], f[7], f[8], f[15], f[16], 
   f[11], f[12], f[13], f[10], f[14], f[17], f[6], f[18]}}, 
 {{1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 
   23, 24, 25, 26, 27, 28, 29, 31}, {1, 2, 3, 32, 33, 34, 35, 36, 38, 39, 40, 
   41, 42, 10, 11, 43, 44, 45, 46, 22, 48, 24, 25, 26, 49, 50, 28, 52}}, 
 {{MySparseMatrix[{6, 28}, {m[1, 1] -> 1/18, m[2, 2] -> 1/6, m[2, 9] -> -1/6, 
     m[3, 2] -> 2, m[3, 9] -> -2, m[4, 3] -> 1/2, m[4, 13] -> -1/2, 
     m[4, 15] -> -1, m[4, 18] -> 1, m[4, 20] -> 1, m[4, 21] -> -1, 
     m[4, 22] -> -1/6, m[5, 1] -> -2/3, m[5, 2] -> -2/3, m[5, 3] -> -1/2, 
     m[5, 4] -> -5/3, m[5, 5] -> 1, m[5, 6] -> -3/2, m[5, 7] -> -1, 
     m[5, 8] -> 1, m[5, 10] -> 1, m[5, 11] -> -1, m[5, 12] -> -1, 
     m[5, 13] -> 1/2, m[5, 14] -> -5/3, m[5, 16] -> -1, m[5, 17] -> 1, 
     m[5, 20] -> -1, m[5, 21] -> 1, m[5, 22] -> -2/3, m[5, 23] -> 5/3, 
     m[5, 24] -> -1, m[5, 25] -> -1, m[5, 26] -> 1, m[5, 27] -> 1, 
     m[6, 1] -> 2/3, m[6, 2] -> 2/3, m[6, 3] -> -1, m[6, 4] -> 5/3, 
     m[6, 6] -> 3/2, m[6, 7] -> 1, m[6, 8] -> -1, m[6, 12] -> 1, 
     m[6, 14] -> 5/3, m[6, 15] -> 1, m[6, 16] -> 1, m[6, 17] -> -1, 
     m[6, 18] -> -1, m[6, 19] -> -1, m[6, 20] -> -1, m[6, 22] -> 5/6, 
     m[6, 23] -> -5/3, m[6, 24] -> 1, m[6, 25] -> 1, m[6, 27] -> -1, 
     m[6, 28] -> -1}], MySparseMatrix[{6, 28}, {}]}, 
  {MySparseMatrix[{13, 28}, {m[1, 1] -> -2/3, m[2, 2] -> 2/3, 
     m[2, 14] -> -2/3, m[3, 4] -> -1/2, m[3, 14] -> 1/2, m[4, 3] -> -1/2, 
     m[4, 5] -> 1/2, m[4, 15] -> 1/2, m[4, 16] -> -1/2, m[4, 19] -> -1/2, 
     m[4, 20] -> -1/2, m[4, 22] -> -1/12, m[5, 3] -> 1/2, m[5, 5] -> -1/2, 
     m[5, 15] -> -1/2, m[5, 16] -> 1/2, m[5, 19] -> 1/2, m[5, 20] -> 1/2, 
     m[5, 22] -> 1/12, m[6, 2] -> -1/2, m[6, 9] -> 1/2, m[6, 23] -> -1/2, 
     m[7, 4] -> -1/2, m[7, 9] -> 1/2, m[7, 23] -> -1/2, m[8, 9] -> 1/2, 
     m[8, 14] -> -1/2, m[8, 23] -> -1/2, m[9, 9] -> -1/2, m[9, 14] -> 1/2, 
     m[9, 23] -> 1/2, m[10, 9] -> -1/2, m[10, 14] -> 1/2, m[10, 23] -> 1/2, 
     m[11, 3] -> 1/4, m[11, 10] -> -1/2, m[11, 13] -> 1/4, m[11, 20] -> 1/2, 
     m[11, 21] -> 1/2, m[11, 22] -> 1/12, m[11, 24] -> 1/2, 
     m[11, 28] -> -1/2, m[12, 1] -> 2/3, m[12, 3] -> 1/2, m[12, 4] -> 5/3, 
     m[12, 5] -> -1, m[12, 6] -> 3/2, m[12, 7] -> 1, m[12, 8] -> -1, 
     m[12, 9] -> 5/3, m[12, 11] -> 1, m[12, 12] -> -1, m[12, 13] -> 1/2, 
     m[12, 14] -> 2/3, m[12, 15] -> -1, m[12, 16] -> 1, m[12, 17] -> 1, 
     m[12, 18] -> -1, m[12, 20] -> 1, m[12, 21] -> 1, m[12, 22] -> 1, 
     m[12, 23] -> -5/3, m[12, 25] -> -1, m[12, 26] -> 1, m[12, 27] -> 1, 
     m[12, 28] -> -1, m[13, 3] -> -1/4, m[13, 10] -> 1/2, m[13, 13] -> -1/4, 
     m[13, 20] -> -1/2, m[13, 21] -> -1/2, m[13, 22] -> -1/12, 
     m[13, 24] -> -1/2, m[13, 28] -> 1/2}], MySparseMatrix[{13, 28}, {}]}}, 
 {1, pflip}, {f[3] -> (y[1]^2*y[2]*y[3])/y[4] + (y[1]^2*y[2]*y[3]*y[5]*y[6])/
     (4*y[4]*y[7]), f[1] -> y[1]^2*y[2]*y[3], 
  f[2] -> (y[1]^2*y[2]*y[3])/y[4], f[5] -> (y[1]^2*y[2]*y[3])/y[4] - 
    (4*y[1]^2*y[2]*y[3]*y[6]^3*y[8]^2)/y[7]^3 + 
    (3*y[1]^2*y[2]*y[3]*y[6]^2*y[8]*y[9])/(y[4]*y[7]^2) - 
    (3*y[1]^2*y[2]*y[3]*y[6]*y[8]*y[10])/(y[4]*y[7]), 
  f[4] -> (y[1]^2*y[2]*y[3]^2*y[11])/y[4] + 
    (12*y[1]^2*y[2]*y[3]^2*y[6]^3*y[8]^2)/(y[7]^2*y[12]) - 
    (3*y[1]^2*y[2]^3*y[3]^3*y[14])/(y[12]^2*y[13]) - 
    (3*y[1]^2*y[2]*y[3]^2*y[6]^2*y[8]^2*y[16])/(y[4]*y[12]^2*y[15]) - 
    (3*y[1]^2*y[2]*y[3]^2*y[6]^2*y[8]*y[17])/(y[4]*y[7]*y[12]^2), 
  f[6] -> (y[1]^2*y[2]*y[3])/y[8], 
  f[11] -> (y[1]^2*y[2]*y[3]*y[5]*y[6])/(y[4]*y[7]) + 
    (y[1]^2*y[2]*y[3]*y[19])/(y[4]*y[18]) - (y[1]^2*y[2]*y[3]^2*y[6]*y[19])/
     (y[18]*y[20]), f[10] -> (y[1]^2*y[2]*y[3]*y[5]*y[6])/(y[4]*y[7]) + 
    (y[1]^2*y[2]*y[3]*y[21])/(y[4]*y[8]*y[18]^2) + 
    (y[1]^2*y[2]^4*y[3]^2*y[19])/(y[4]*y[8]*y[18]^2*y[22]), 
  f[13] -> (y[1]^2*y[2]*y[3]*y[24])/(y[8]*y[23]) + 
    (y[1]^2*y[2]*y[3]*y[26])/(y[8]*y[25]) - (y[1]^2*y[2]*y[3]*y[27])/
     (y[8]*y[28]) - (y[1]^2*y[2]^3*y[3]^2*y[29])/(y[8]*y[23]*y[30]), 
  f[12] -> (y[1]^2*y[2]*y[3]^2*y[6]*y[19])/(y[18]*y[20]) + 
    (y[1]^2*y[2]^2*y[3]^2)/(y[18]*y[23]) - (y[1]^2*y[2]^2*y[3]^2)/
     (y[8]*y[25]) + (y[1]^2*y[2]^3*y[3]^2*y[29])/(y[8]*y[23]*y[30]), 
  f[16] -> (y[1]^2*y[2]*y[3]*y[31])/y[18], 
  f[17] -> (y[1]^2*y[2]*y[3]*y[32])/(y[4]^2*y[18]), 
  f[7] -> (y[1]^2*y[2]*y[3]*y[6]^3*y[8]^2)/y[7]^3 + 
    (3*y[1]^2*y[2]*y[3]^3*y[6]^2)/(4*y[20]^2) + (3*y[1]^2*y[2]*y[3]*y[33])/
     (4*y[4]^2*y[18]) - (3*y[1]^2*y[2]*y[3]^2*y[6]*y[34])/(4*y[18]*y[20]) + 
    (3*y[1]^2*y[2]*y[3]*y[6]^2*y[8]*y[35])/(4*y[4]*y[7]^2) + 
    (3*y[1]^2*y[2]*y[3]^2*y[6]*y[36])/(4*y[4]^2*y[7]), 
  f[9] -> (y[1]^2*y[2]*y[3]^2*y[6]^3*y[8]^2)/(y[7]^2*y[12]) + 
    (y[1]^2*y[2]*y[3]*y[37])/(12*y[4]*y[8]*y[18]) - 
    (3*y[1]^2*y[2]*y[3]^2*y[6]^2*y[8])/(4*y[20]*y[38]) - 
    (y[1]^2*y[2]^3*y[3]^2*y[6]*y[39])/(4*y[12]^2*y[13]*y[38]) + 
    (3*y[1]^2*y[2]^5*y[3]^2)/(4*y[4]*y[8]*y[18]*y[22]*y[40]) - 
    (y[1]^2*y[2]*y[3]^2*y[6]^2*y[8]*y[41]*y[42])/(4*y[12]^2*y[15]*y[40]) + 
    (y[1]^2*y[2]*y[3]^2*y[6]^2*y[8]*y[43])/(4*y[4]*y[7]*y[12]^2), 
  f[8] -> (y[1]^2*y[2]*y[3]^3*y[6]^2)/y[20]^2 - (y[1]^2*y[2]^2*y[3]^2)/
     (y[18]*y[23]) + (y[1]^2*y[2]^2*y[3]^2)/(y[8]*y[25]) - 
    (y[1]^2*y[2]^3*y[3]^2*y[29])/(y[8]*y[23]*y[30]) - 
    (y[1]^2*y[2]*y[3]^2*y[6]*y[34])/(y[18]*y[20]), 
  f[14] -> (y[1]^2*y[2]^6*y[3]^2)/(y[4]*y[8]*y[18]^2*y[22]^2) - 
    (y[1]^2*y[2]*y[3]*y[27])/(y[8]*y[28]) - (y[1]^2*y[2]*y[3]^2*y[44])/
     (y[4]^2*y[18]) - (y[1]^2*y[2]^4*y[3]^2*y[45])/(y[4]^2*y[8]*y[18]^2*
      y[22]) - (y[1]^2*y[2]*y[3]*y[46])/y[25], 
  f[18] -> (y[1]^2*y[2]^3*y[3]^2*y[47])/(y[4]^3*y[18]), 
  f[15] -> (y[1]^2*y[2]*y[3]^2*y[34])/y[18]}, {y[1] -> ex[1], y[2] -> ex[2], 
  y[3] -> ex[3], y[4] -> 1 + ex[3] + ex[2]*ex[3], 
  y[5] -> -4 - ex[2]*ex[3] + 4*ex[3]^2 + 3*ex[2]*ex[3]^2, y[6] -> ex[5], 
  y[7] -> ex[2]*ex[3] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], y[8] -> 1 + ex[3], 
  y[9] -> 3 + 4*ex[3] + 2*ex[2]*ex[3] + ex[3]^2 + ex[2]*ex[3]^2, 
  y[10] -> 2 + 2*ex[3] + ex[2]*ex[3], 
  y[11] -> -29 - 38*ex[2] + 9*ex[3] + 9*ex[2]*ex[3], 
  y[12] -> ex[2]*ex[3] + ex[5] + ex[3]*ex[5], y[13] -> ex[4], 
  y[14] -> 3*ex[2]*ex[3] + ex[5] + 3*ex[3]*ex[5], 
  y[15] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[16] -> -2*ex[2] - 2*ex[2]*ex[3] + ex[2]^2*ex[3] + 2*ex[5] + 
    3*ex[2]*ex[5] + 4*ex[3]*ex[5] + 5*ex[2]*ex[3]*ex[5] + 2*ex[3]^2*ex[5] + 
    2*ex[2]*ex[3]^2*ex[5], y[17] -> 9*ex[2]*ex[3] + 12*ex[2]*ex[3]^2 + 
    6*ex[2]^2*ex[3]^2 + 3*ex[2]*ex[3]^3 + 3*ex[2]^2*ex[3]^3 + 5*ex[5] + 
    11*ex[3]*ex[5] + 2*ex[2]*ex[3]*ex[5] + 7*ex[3]^2*ex[5] + 
    3*ex[2]*ex[3]^2*ex[5] + ex[3]^3*ex[5] + ex[2]*ex[3]^3*ex[5], 
  y[18] -> 1 + ex[2], y[19] -> 4 + 3*ex[2], 
  y[20] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + ex[3]*ex[5], 
  y[21] -> 4 + 8*ex[2] + 4*ex[2]^2 + 4*ex[3] + 12*ex[2]*ex[3] + 
    8*ex[2]^2*ex[3] + ex[2]^3*ex[3], 
  y[22] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[2]*ex[4] - 
    ex[3]*ex[4] - ex[2]*ex[3]*ex[4], y[23] -> 1 + ex[2] + ex[3], 
  y[24] -> ex[2] - ex[3] - ex[3]^2, y[25] -> 1 + ex[4] - ex[5], 
  y[26] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - ex[3]*ex[5], 
  y[27] -> -1 + ex[5] + ex[3]*ex[5], 
  y[28] -> -1 + ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[29] -> ex[2] + ex[5] + ex[3]*ex[5], 
  y[30] -> -(ex[2]*ex[3]) + ex[4] + ex[2]*ex[4] + ex[3]*ex[4] + 
    ex[2]*ex[3]*ex[5], y[31] -> -2 - 2*ex[2] + 4*ex[3] + 3*ex[2]*ex[3], 
  y[32] -> -2 - 2*ex[2] - 2*ex[3] + ex[2]^2*ex[3], 
  y[33] -> 1 + ex[3] + 3*ex[2]*ex[3] + 2*ex[2]^2*ex[3], 
  y[34] -> 2 + ex[2] + 2*ex[3] + 2*ex[2]*ex[3], 
  y[35] -> -1 - 2*ex[2]*ex[3] + ex[3]^2 + ex[2]*ex[3]^2, 
  y[36] -> 2 - ex[2] + 6*ex[3] + 2*ex[2]*ex[3] + ex[2]^2*ex[3] + 6*ex[3]^2 + 
    7*ex[2]*ex[3]^2 + ex[2]^2*ex[3]^2 + 2*ex[3]^3 + 4*ex[2]*ex[3]^3 + 
    2*ex[2]^2*ex[3]^3, y[37] -> 38 + 38*ex[2] + 38*ex[3] + 67*ex[2]*ex[3] + 
    38*ex[2]^2*ex[3], y[38] -> ex[2] + ex[5], 
  y[39] -> ex[2]*ex[3] + 3*ex[5] + ex[3]*ex[5], 
  y[40] -> -ex[2] + ex[5] + ex[2]*ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[41] -> ex[2] - ex[5] - ex[3]*ex[5], 
  y[42] -> 2*ex[2] + 2*ex[2]*ex[3] + 3*ex[2]^2*ex[3] - 2*ex[5] + 
    ex[2]*ex[5] - 4*ex[3]*ex[5] - ex[2]*ex[3]*ex[5] - 2*ex[3]^2*ex[5] - 
    2*ex[2]*ex[3]^2*ex[5], y[43] -> -3*ex[2]*ex[3] - 6*ex[2]^2*ex[3]^2 + 
    3*ex[2]*ex[3]^3 + 3*ex[2]^2*ex[3]^3 + ex[5] + 7*ex[3]*ex[5] - 
    2*ex[2]*ex[3]*ex[5] + 11*ex[3]^2*ex[5] + 3*ex[2]*ex[3]^2*ex[5] + 
    5*ex[3]^3*ex[5] + 5*ex[2]*ex[3]^3*ex[5], 
  y[44] -> 1 - ex[2] + ex[3] + 2*ex[2]*ex[3] + ex[2]^2*ex[3], 
  y[45] -> -2 - ex[2] - 2*ex[3] - ex[2]*ex[3] + ex[2]^2*ex[3], 
  y[46] -> -1 + ex[5], y[47] -> -1 + ex[3] + ex[2]*ex[3]}}
