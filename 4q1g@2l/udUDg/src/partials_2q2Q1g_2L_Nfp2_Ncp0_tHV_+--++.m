(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[4], f[5], f[3], f[2], f[1]}, {f[5], f[4], f[6]}}, 
 {{1, 2, 3, 4, 6, 7, 8, 9, 10, 15, 16, 17, 18, 1332, 24, 25, 26, 27, 2112, 
   29}, {1, 34, 36, 37, 39, 40, 2, 42, 43, 3, 4, 45, 46, 6, 24, 25, 3605, 51, 
   26, 27}}, {{MySparseMatrix[{5, 20}, {m[1, 1] -> -200/81, m[1, 2] -> 20/27, 
     m[1, 4] -> 20/27, m[1, 6] -> 5/27, m[1, 10] -> 5/27, m[1, 16] -> -5/27, 
     m[2, 1] -> 200/81, m[2, 2] -> -20/27, m[2, 4] -> -20/27, 
     m[2, 6] -> -5/27, m[2, 10] -> -5/27, m[2, 16] -> 5/27, 
     m[3, 2] -> -40/27, m[3, 3] -> 4/9, m[3, 4] -> 40/27, m[3, 5] -> -4/9, 
     m[3, 7] -> 1/9, m[3, 8] -> -1/9, m[3, 11] -> 1/9, m[3, 12] -> -1/9, 
     m[3, 17] -> -1/9, m[3, 18] -> 1/9, m[4, 1] -> -50/81, m[4, 2] -> 20/27, 
     m[4, 3] -> -2/9, m[4, 6] -> 5/27, m[4, 7] -> -1/18, m[4, 9] -> -5/144, 
     m[4, 10] -> 5/27, m[4, 11] -> -1/18, m[4, 13] -> -1/72, 
     m[4, 14] -> -5/144, m[4, 15] -> -13/432, m[4, 16] -> -5/27, 
     m[4, 17] -> 1/18, m[4, 19] -> 5/72, m[4, 20] -> 1/72, m[5, 1] -> -50/81, 
     m[5, 2] -> 20/27, m[5, 3] -> -2/9, m[5, 6] -> 5/27, m[5, 7] -> -1/18, 
     m[5, 9] -> -5/144, m[5, 10] -> 5/27, m[5, 11] -> -1/18, 
     m[5, 13] -> -1/72, m[5, 14] -> -5/144, m[5, 15] -> -13/432, 
     m[5, 16] -> -5/27, m[5, 17] -> 1/18, m[5, 19] -> 5/72, 
     m[5, 20] -> 1/72}], MySparseMatrix[{5, 20}, {}]}, 
  {MySparseMatrix[{3, 20}, {m[1, 1] -> -200/81, m[1, 2] -> 5/27, 
     m[1, 4] -> 5/27, m[1, 7] -> 20/27, m[1, 11] -> 20/27, m[1, 16] -> -5/27, 
     m[2, 1] -> 100/81, m[2, 2] -> 5/27, m[2, 3] -> -5/72, m[2, 4] -> 5/27, 
     m[2, 5] -> -1/36, m[2, 6] -> -5/72, m[2, 7] -> -20/27, 
     m[2, 11] -> 20/27, m[2, 12] -> -1/9, m[2, 13] -> -1/9, m[2, 14] -> -4/9, 
     m[2, 15] -> -13/216, m[2, 16] -> -5/27, m[2, 17] -> 5/36, 
     m[2, 18] -> 1/36, m[2, 20] -> 1/9, m[3, 7] -> 40/27, m[3, 8] -> -1/9, 
     m[3, 9] -> -1/9, m[3, 10] -> -4/9, m[3, 11] -> -40/27, m[3, 12] -> 1/9, 
     m[3, 13] -> 1/9, m[3, 14] -> 4/9, m[3, 19] -> 1/9, m[3, 20] -> -1/9}], 
   MySparseMatrix[{3, 20}, {}]}}, {1, pflip}, 
 {f[3] -> (y[1]^3*y[3]^2)/(y[2]*y[4]*y[5]) + 
    (y[1]^3*y[2]^2*y[3]^2*y[4]^2*y[6]^3)/(y[5]*y[7]^3) + 
    (3*y[1]^3*y[2]*y[3]^2*y[4]*y[6]^2)/(2*y[5]*y[7]^2) + 
    (3*y[1]^3*y[3]^2*y[6])/(2*y[5]*y[7]), f[1] -> y[8]/(y[2]*y[4]), 
  f[2] -> y[9]/(y[2]*y[5]), f[6] -> (y[1]^2*y[3]^2)/y[5] + 
    (y[1]^3*y[2]^2*y[3]^2*y[4]^2*y[6]^3)/(y[5]*y[7]^3) + 
    (3*y[1]^3*y[2]*y[3]^2*y[4]*y[6]^2)/(2*y[5]*y[7]^2) + 
    (3*y[1]^3*y[3]^2*y[6])/(2*y[5]*y[7]), f[4] -> (y[1]^2*y[3])/y[2], 
  f[5] -> (y[1]^2*y[3])/y[2] + (3*y[1]^2*y[3]^2*y[5]*y[6])/
     (10*y[10]*y[11]^2) + (3*y[1]^2*y[2]*y[3]^2*y[4]^2*y[6]^3)/
     (5*y[7]^2*y[11]) - (3*y[1]*y[3]^2*y[4]^2*y[6]^2*y[13])/
     (10*y[11]^2*y[12]) + (3*y[1]*y[3]^2*y[4]*y[6]^2*y[14])/
     (10*y[7]*y[11]^2), f[7] -> (-10*y[1]^2*y[3])/(y[2]*y[5]) + 
    (22*y[1]^3*y[2]^2*y[3]^2*y[4]^2*y[6]^3)/(y[5]*y[7]^3) - 
    (3*y[1]^2*y[3]^2*y[5]*y[6])/(y[10]*y[11]^2) + 
    (3*y[1]^2*y[2]*y[3]^2*y[4]*y[6]^2*y[15])/(y[5]*y[7]^2*y[11]) + 
    (3*y[1]^2*y[3]^2*y[6]*y[16])/(y[5]*y[7]*y[11]^2)}, 
 {y[1] -> ex[2], y[2] -> 1 + ex[2], y[3] -> ex[3], y[4] -> 1 + ex[3], 
  y[5] -> 1 + ex[3] + ex[2]*ex[3], y[6] -> ex[5], 
  y[7] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4] - ex[2]*ex[5], 
  y[8] -> 1 - 3*ex[2] + ex[3] - 3*ex[2]*ex[3] + 2*ex[2]^2*ex[3], 
  y[9] -> -1 + 3*ex[2] - ex[3] + 2*ex[2]*ex[3] + ex[2]^2*ex[3], 
  y[10] -> 1 + ex[4] - ex[5], y[11] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - 
    ex[3]*ex[5], y[12] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4], 
  y[13] -> ex[2] + ex[5] + ex[3]*ex[5], 
  y[14] -> 3*ex[2] + 3*ex[2]*ex[3] + 3*ex[2]^2*ex[3] + ex[5] - ex[2]*ex[5] + 
    2*ex[3]*ex[5] + ex[3]^2*ex[5] + ex[2]*ex[3]^2*ex[5], 
  y[15] -> 11*ex[2] + 11*ex[2]*ex[3] + 11*ex[2]^2*ex[3] - ex[5] - 
    11*ex[2]*ex[5] - 2*ex[3]*ex[5] - 12*ex[2]*ex[3]*ex[5] - ex[3]^2*ex[5] - 
    ex[2]*ex[3]^2*ex[5], y[16] -> 7*ex[2] + 14*ex[2]*ex[3] + 
    14*ex[2]^2*ex[3] + 7*ex[2]*ex[3]^2 + 14*ex[2]^2*ex[3]^2 + 
    7*ex[2]^3*ex[3]^2 - 2*ex[5] - 14*ex[2]*ex[5] - 6*ex[3]*ex[5] - 
    32*ex[2]*ex[3]*ex[5] - 14*ex[2]^2*ex[3]*ex[5] - 6*ex[3]^2*ex[5] - 
    22*ex[2]*ex[3]^2*ex[5] - 16*ex[2]^2*ex[3]^2*ex[5] - 2*ex[3]^3*ex[5] - 
    4*ex[2]*ex[3]^3*ex[5] - 2*ex[2]^2*ex[3]^3*ex[5] + ex[5]^2 + 
    7*ex[2]*ex[5]^2 + 3*ex[3]*ex[5]^2 + 15*ex[2]*ex[3]*ex[5]^2 + 
    3*ex[3]^2*ex[5]^2 + 9*ex[2]*ex[3]^2*ex[5]^2 + ex[3]^3*ex[5]^2 + 
    ex[2]*ex[3]^3*ex[5]^2}}
