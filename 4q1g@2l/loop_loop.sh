#! /usr/bin/env bash

cores=$(nproc)
channels=6
each=$((cores / channels))

source ~/env-form

for f in *; do
	if [[ $f =~ ^[udUDg]{5}$ ]]; then
		echo -e "\n$f\n"
		cd $f
		../proc/loop.sh
		cd build
		make -j$each &
		cd ../..
	fi
done
