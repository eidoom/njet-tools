(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[3], f[4], f[2], f[1]}, {f[4], f[3], f[5]}}, 
 {{1, 2, 3, 4, 6, 10, 11, 12, 851, 14, 15, 16, 18, 1486, 22, 23, 2273, 26, 
   27}, {1, 30, 31, 32, 33, 34, 2, 35, 36, 3, 4, 41, 42, 6, 22, 23, 46, 47, 
   2273, 24}}, {{MySparseMatrix[{4, 19}, {m[1, 1] -> -200/81, 
     m[1, 2] -> 20/27, m[1, 4] -> 20/27, m[1, 6] -> 5/27, m[1, 10] -> 5/27, 
     m[1, 16] -> -20/27, m[2, 1] -> 40/81, m[2, 2] -> -4/27, 
     m[2, 4] -> -4/27, m[2, 6] -> -1/27, m[2, 10] -> -1/27, m[2, 16] -> 4/27, 
     m[3, 2] -> 20/27, m[3, 3] -> -2/9, m[3, 4] -> -20/27, m[3, 5] -> 2/9, 
     m[3, 7] -> -1/18, m[3, 8] -> 1/18, m[3, 11] -> -1/18, m[3, 12] -> 1/18, 
     m[3, 15] -> -2/9, m[3, 16] -> -20/27, m[3, 17] -> 4/9, m[3, 18] -> 1/18, 
     m[3, 19] -> 1/18, m[4, 1] -> 100/81, m[4, 2] -> -40/27, m[4, 3] -> 4/9, 
     m[4, 6] -> -10/27, m[4, 7] -> 1/9, m[4, 9] -> 5/72, m[4, 10] -> -10/27, 
     m[4, 11] -> 1/9, m[4, 13] -> 1/36, m[4, 14] -> 5/72, m[4, 15] -> 47/108, 
     m[4, 16] -> 40/27, m[4, 17] -> -8/9, m[4, 18] -> -1/9, 
     m[4, 19] -> -1/9}], MySparseMatrix[{4, 19}, {}]}, 
  {MySparseMatrix[{3, 20}, {m[1, 1] -> -40/81, m[1, 2] -> 1/27, 
     m[1, 4] -> 1/27, m[1, 7] -> 4/27, m[1, 11] -> 4/27, m[1, 16] -> -5/27, 
     m[2, 1] -> 100/81, m[2, 2] -> 5/27, m[2, 3] -> -5/72, m[2, 4] -> 5/27, 
     m[2, 5] -> -1/36, m[2, 6] -> -5/72, m[2, 7] -> 20/27, m[2, 8] -> -1/9, 
     m[2, 9] -> -1/9, m[2, 10] -> -4/9, m[2, 11] -> -20/27, 
     m[2, 15] -> -133/216, m[2, 16] -> -25/27, m[2, 17] -> 1/4, 
     m[2, 18] -> 5/36, m[2, 19] -> 1, m[3, 7] -> -20/27, m[3, 8] -> 1/18, 
     m[3, 9] -> 1/18, m[3, 10] -> 2/9, m[3, 11] -> 20/27, m[3, 12] -> -1/18, 
     m[3, 13] -> -1/18, m[3, 14] -> -2/9, m[3, 15] -> 5/18, 
     m[3, 16] -> 20/27, m[3, 17] -> -1/18, m[3, 18] -> -1/18, 
     m[3, 19] -> -1/2, m[3, 20] -> 1/18}], MySparseMatrix[{3, 20}, {}]}}, 
 {1, pflip}, {f[1] -> (y[1]^2*y[2]^2)/(y[3]*y[4]), 
  f[2] -> (y[1]^2*y[2]^2*y[5])/(y[3]*y[4]^3) - (2*y[1]^2*y[2]^7*y[6]^3)/
     (y[4]^3*y[7]^3) - (3*y[1]^2*y[2]^5*y[6]^2*y[8])/(y[4]^3*y[7]^2) - 
    (3*y[1]^2*y[2]^3*y[6]*y[9])/(y[4]^3*y[7]), 
  f[5] -> (-2*y[1]^2*y[2]^7*y[6]^3)/(y[4]^3*y[7]^3) - 
    (3*y[1]^2*y[2]^5*y[6]^2*y[8])/(y[4]^3*y[7]^2) - 
    (3*y[1]^2*y[2]^3*y[6]*y[9])/(y[4]^3*y[7]) + (y[1]^2*y[2]*y[10]*y[11])/
     y[4]^3, f[3] -> y[1]^2*y[2], f[4] -> (y[1]^2*y[2]*y[12])/y[4]^2 - 
    (3*y[1]^2*y[2]^2*y[6]*y[13]^2*y[15])/(2*y[14]*y[16]^2) - 
    (3*y[1]^2*y[2]^6*y[6]^2)/(y[4]^2*y[7]^2*y[16]) - 
    (3*y[1]^2*y[2]^3*y[6]*y[10]*y[13]^2*y[17])/(2*y[16]^2*y[18]) - 
    (3*y[1]^2*y[2]^4*y[6]*y[8]*y[19])/(2*y[4]^2*y[7]*y[16]^2), 
  f[6] -> (22*y[1]^2*y[2]^7*y[6]^3)/(y[4]^3*y[7]^3) - 
    (3*y[1]^2*y[2]^2*y[6]*y[13]^2*y[15])/(y[14]*y[16]^2) + 
    (2*y[1]^2*y[2]^2*y[6]*y[20])/y[4]^3 + (3*y[1]^2*y[2]^5*y[6]^2*y[21])/
     (y[4]^3*y[7]^2*y[16]) + (3*y[1]^2*y[2]^3*y[6]*y[22])/
     (y[4]^3*y[7]*y[16]^2)}, {y[1] -> ex[1], y[2] -> ex[2], 
  y[3] -> 1 + ex[2], y[4] -> 1 + ex[3] + ex[2]*ex[3], 
  y[5] -> 2 + 3*ex[3] + 3*ex[2]*ex[3] + 3*ex[2]*ex[3]^2 + 3*ex[2]^2*ex[3]^2 - 
    ex[3]^3 + ex[2]^2*ex[3]^3, y[6] -> ex[3], 
  y[7] -> ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4], y[8] -> -1 - ex[3] + ex[2]*ex[3], 
  y[9] -> 1 + 2*ex[3] + ex[3]^2 + ex[2]^2*ex[3]^2, y[10] -> 1 + ex[3], 
  y[11] -> 2 + 4*ex[3] + 3*ex[2]*ex[3] + 2*ex[3]^2 + 3*ex[2]*ex[3]^2 + 
    3*ex[2]^2*ex[3]^2, y[12] -> 5 + 10*ex[3] + 13*ex[2]*ex[3] + 5*ex[3]^2 + 
    13*ex[2]*ex[3]^2 + 5*ex[2]^2*ex[3]^2, y[13] -> ex[5], 
  y[14] -> ex[2] - ex[4] + ex[5], y[15] -> ex[2] + ex[5] + ex[3]*ex[5], 
  y[16] -> ex[2] + ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[17] -> 1 + ex[3]*ex[5], y[18] -> -(ex[2]*ex[3]) + ex[4] + ex[3]*ex[4] + 
    ex[2]*ex[3]*ex[5], y[19] -> 2*ex[2] + 3*ex[5] + 3*ex[3]*ex[5] + 
    3*ex[2]*ex[3]*ex[5], y[20] -> 12 + 24*ex[3] + 6*ex[2]*ex[3] + 
    12*ex[3]^2 + 6*ex[2]*ex[3]^2 + 5*ex[2]^2*ex[3]^2, 
  y[21] -> -12*ex[2] - 12*ex[2]*ex[3] + 10*ex[2]^2*ex[3] - 11*ex[5] - 
    22*ex[3]*ex[5] - 11*ex[3]^2*ex[5] + 11*ex[2]^2*ex[3]^2*ex[5], 
  y[22] -> 8*ex[2]^2 + 16*ex[2]^2*ex[3] - 8*ex[2]^3*ex[3] + 
    8*ex[2]^2*ex[3]^2 - 8*ex[2]^3*ex[3]^2 + 6*ex[2]^4*ex[3]^2 + 
    15*ex[2]*ex[5] + 45*ex[2]*ex[3]*ex[5] - 2*ex[2]^2*ex[3]*ex[5] + 
    45*ex[2]*ex[3]^2*ex[5] - 4*ex[2]^2*ex[3]^2*ex[5] - 
    5*ex[2]^3*ex[3]^2*ex[5] + 15*ex[2]*ex[3]^3*ex[5] - 
    2*ex[2]^2*ex[3]^3*ex[5] - 5*ex[2]^3*ex[3]^3*ex[5] + 
    12*ex[2]^4*ex[3]^3*ex[5] + 7*ex[5]^2 + 28*ex[3]*ex[5]^2 + 
    6*ex[2]*ex[3]*ex[5]^2 + 42*ex[3]^2*ex[5]^2 + 18*ex[2]*ex[3]^2*ex[5]^2 - 
    2*ex[2]^2*ex[3]^2*ex[5]^2 + 28*ex[3]^3*ex[5]^2 + 
    18*ex[2]*ex[3]^3*ex[5]^2 - 4*ex[2]^2*ex[3]^3*ex[5]^2 + 
    6*ex[2]^3*ex[3]^3*ex[5]^2 + 7*ex[3]^4*ex[5]^2 + 6*ex[2]*ex[3]^4*ex[5]^2 - 
    2*ex[2]^2*ex[3]^4*ex[5]^2 + 6*ex[2]^3*ex[3]^4*ex[5]^2 + 
    7*ex[2]^4*ex[3]^4*ex[5]^2}}
