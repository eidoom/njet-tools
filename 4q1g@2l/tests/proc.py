#!/usr/bin/env python3

from decimal import Decimal
import re


def proc(filename, cut=True):
    no = r"\d+\.\d+"
    with open(filename, "r") as f:
        r = f.read()
    # print(r)
    r = re.sub(r"\\\n", "", r)
    r = re.sub(r"`" + no, "", r)
    r = re.sub(r"\s*[+-]\s*" + no + "(?:|\*\^-?\d+)\s*\*\s*I", r"", r, flags=re.DOTALL)
    r = re.sub(r"\*\^-?(\d+)", r"e\1", r)
    if cut:
        r = re.sub(r"([\d\.]{65})\d+", r"\1", r)
    r = re.sub(r"\s+", " ", r, flags=re.DOTALL)
    return r


# def proc_nnlo(r):
#     no = r"\d+\.\d+"
#     number = r"(-?\d+\.\d+)`\d+\.\d+(?:|\*\^(\d+))"
#     r = re.sub(r"\s+", " ", r, flags=re.DOTALL)
#     r = re.sub(r"\((" + number + r")\s*[-+]\s*0``\d+\.\d+\*I\)", r"\1", r, flags=re.DOTALL)
#     r = re.sub(r"`" + no, "", r)
#     return r


def stringify_nnlo(data):
    s = "{"
    for i, psp in enumerate(data):
        s += "{"
        for j, hel in enumerate(psp):
            s += f"{hel}"
            if j != len(psp) - 1:
                s += ","
        s += "}"
        if i != len(data) - 1:
            s += ","
    s += "},"
    return s


if __name__ == "__main__":
    c2q2Q1g = ("udUDg", "uDdUg", "ugUdD", "uUdDg", "udDUg", "ugDdU")
    c4q1g = ("ugUuU", "uuUUg", "uUuUg")
    fix_factors_of_two = False  # actually, never use this!
    # res = {}

    with open("tmp_4q1g.cpp", "w") as o:

        # for c in c2q2Q1g:
        for c in c4q1g:
            # res[c] = {
            #     "0Lx0L": {},
            #     "1Lx0L": {},
            #     "2Lx0L": {},
            #     "1Lx1L": {},
            # }

            o.write(
                f"template <typename T>\nconst std::vector<std::vector<int>> MmaVals<T>::hels_{c}{{\n"
            )

            h = f"NJcompare_{c}_helicity_list.m"
            with open(h, "r") as f:
                hh = f.read()
            hs = (
                hh.replace('"', "")
                .replace(" \n", "")
                .replace("{", "{\n{")
                .replace("+", "+1,")
                .replace("-", "-1,")
                .replace(",,", "},")
                .replace(",}", "},\n}")
                .replace(" ", "\n{")
            )
            o.write(hs)
            o.write("};\n\n")
            # hl = hh.strip().strip('{}"').split('", "')

            o.write(
                f"template <typename T>\nconst std::vector<std::vector<std::vector<T>>> MmaVals<T>::vals_{c}{{{{\n"
            )

            r = proc(f"NJcompare_{c}_LO.m")
            o.write(r)
            o.write(",\n")

            for nf in range(2):
                r = proc(f"NJcompare_{c}_NLO_nf{nf}.m", cut=False)
                if fix_factors_of_two:
                    r = [
                        [2 * Decimal(b) for b in a.split(", ")]
                        for a in r.strip("{} ").split("}, {")
                    ]
                    o.write(stringify_nnlo(r))
                else:
                    o.write(r)
                    o.write(",\n")

            # for nf in range(3):
            #     with open(f"NJcompare_{c}_NNLO_nf{nf}_split.m", "r") as f:
            #         raw = f.read()
            #     r = proc_nnlo(raw)
            #     print(r)

            nfs_2Lx0L = []
            nfs_1Lx1L = []

            number = r"(-?\d+\.\d+)`\d+\.\d+(?:|\*\^(\d+))"

            for nf in range(3):
                with open(f"NJcompare_{c}_NNLO_nf{nf}_split.m", "r") as f:
                    r = f.read()

                psps_2Lx0L = []
                psps_1Lx1L = []

                for psp in re.split(r"},\s*{", r.strip().strip("}{"), flags=re.DOTALL):
                    hels_2Lx0L = []
                    hels_1Lx1L = []

                    rhels = re.findall(
                        r"(-?\d+)\s*\*\s*\("
                        + number
                        + r'\s*\*\s*flag\["0Lx2L"\]\s*([-+])\s*\('
                        + number
                        + r'\s*[-+]\s*0``\d+\.\d+\*I\)\s*\*\s*flag\["1L\^2"\]\)',
                        psp,
                        flags=re.DOTALL,
                    )

                    for rhel in rhels:
                        c2Lx0L = Decimal(rhel[0]) * Decimal(rhel[1])
                        if rhel[2]:
                            c2Lx0L *= 10 ** int(rhel[2])
                        if fix_factors_of_two:
                            c2Lx0L *= 4
                        hels_2Lx0L.append(c2Lx0L)

                        c1Lx1L = Decimal(rhel[0]) * Decimal(rhel[3] + rhel[4])
                        if rhel[5]:
                            c1Lx1L *= 10 ** int(rhel[5])
                        if fix_factors_of_two:
                            c1Lx1L *= 4
                        hels_1Lx1L.append(c1Lx1L)

                    psps_2Lx0L.append(hels_2Lx0L)
                    psps_1Lx1L.append(hels_1Lx1L)

                nfs_2Lx0L.append(psps_2Lx0L)
                nfs_1Lx1L.append(psps_1Lx1L)

            for nf in range(3):
                o.write(stringify_nnlo(nfs_2Lx0L[nf]))
                o.write("\n")
                o.write(stringify_nnlo(nfs_1Lx1L[nf]))
                o.write("\n")

            o.write("}};\n\n")
