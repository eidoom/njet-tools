// #include <algorithm>
#include <array>
#include <chrono>
// #include <cstddef>
#include <string>
// #include <type_traits>
#include <vector>

#include "analytic/0q3g2A-analytic.h"
#include "chsums/0q3gA.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

template <typename T>
class Test {
private:
    using TP = const std::chrono::high_resolution_clock::time_point;
    static constexpr int mul { 5 };
    const double MuR2 { std::pow(91.118, 2) };

    const std::vector<double> scales {{0}};

    long int total_speedup { 0 };
    std::chrono::nanoseconds total_time_analytic { 0 };
    std::chrono::nanoseconds total_time_numeric { 0 };

    Amp0q3gAA<T> num;
    Amp0q3g2A_a<T> ana;

    const std::vector<MOM<T>> genMom(int rseed, double sqrtS = 1.) const
    {
        // args: multiplicity, flavour array, rseed, sqrts
        PhaseSpace<T> psp(mul, rseed, sqrtS);
        std::vector<MOM<T>> mom { psp.getPSpoint() };

#ifdef DEBUG
        psp.showPSpoint();
#endif // DEBUG

        refineM(mom, mom, scales);

        return mom;
    }

public:
    const long int get_total_speedup() const
    {
        return this->total_speedup;
    }

    const std::chrono::nanoseconds get_total_time_analytic() const
    {
        return this->total_time_analytic;
    }

    const std::chrono::nanoseconds get_total_time_numeric() const
    {
        return this->total_time_numeric;
    }

    const std::string genHelStr(const std::array<int, mul>& helInt) const
    {
        std::string helStr;
        for (int hel : helInt) {
            helStr.append(hel == 1 ? "+" : "-");
        }
        return helStr;
    }

    constexpr int get_mul() const
    {
        return this->mul;
    }

    void compare(std::array<int, mul> hel, const bool v = true)
    {
        unsigned int num { 0 };
        for (int i { 0 }; i < mul; ++i) {
            num += hel[i] == 1 ? 1 << i : 0;
        }

        std::cout << num << " " << this->genHelStr(hel) << '\n';

        TP t0s { std::chrono::high_resolution_clock::now() };
        const T numsol { this->num.virtsq(hel.data()).get0().real() };
        TP t0e { std::chrono::high_resolution_clock::now() };
        std::chrono::nanoseconds d0 { std::chrono::duration_cast<std::chrono::nanoseconds>(t0e - t0s).count() };
        this->total_time_numeric += d0;
        if (v) {
            std::cout << "|amp_n|^2 " << numsol << '\n';
            std::cout << "t_n       " << std::chrono::duration_cast<std::chrono::milliseconds>(d0).count() << "ms\n";
        }

        TP t1s { std::chrono::high_resolution_clock::now() };
        const T anasol { this->ana.virtsq(hel.data()).get0().real() };
        TP t1e { std::chrono::high_resolution_clock::now() };
        std::chrono::nanoseconds d1 { std::chrono::duration_cast<std::chrono::nanoseconds>(t1e - t1s).count() };
        this->total_time_analytic += d1;
        if (v) {
            std::cout << "|amp_a|^2 " << anasol << '\n';
            std::cout << "t_a       " << std::chrono::duration_cast<std::chrono::microseconds>(d1).count() << "\u03BCs\n";
        }

        const long int speedup { d0 / d1 };
        this->total_speedup += speedup;
        std::cout << "ratio     " << anasol / numsol << '\n';
        std::cout << "speedup   " << speedup << '\n';
    }

    void primary_helicities()
    {
        // +++++
        std::array<int, this->mul> hel3 { 1, 1, 1, 1, 1 };
        this->compare(hel3);

        // -++++
        std::array<int, this->mul> hel4 { -1, 1, 1, 1, 1 };
        this->compare(hel4);

        // --+++
        std::array<int, this->mul> hel1 { -1, -1, 1, 1, 1 };
        this->compare(hel1);

        // +++--
        std::array<int, this->mul> hel2 { 1, 1, 1, -1, -1 };
        this->compare(hel2);

        // +++-+
        std::array<int, this->mul> hel5 { 1, 1, 1, -1, 1 };
        this->compare(hel5);

        // +--+-
        std::array<int, this->mul> hel6 { 1, -1, -1, 1, -1 };
        this->compare(hel6);
    };

    void all_helicities(const bool v = true)
    {
        for (int i0 { -1 }; i0 < 2; i0 += 2) {
            for (int i1 { -1 }; i1 < 2; i1 += 2) {
                for (int i2 { -1 }; i2 < 2; i2 += 2) {
                    for (int i3 { -1 }; i3 < 2; i3 += 2) {
                        for (int i4 { -1 }; i4 < 2; i4 += 2) {
                            const std::array<int, mul> hel { { i4, i3, i2, i1, i0 } };
                            this->compare(hel, v);
                        }
                    }
                }
            }
        }
    }

    Test(int rseed)
        : num(StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()), 1.)
        , ana(StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()), 1.)
    {
        const std::vector<MOM<T>> mom { genMom(rseed) };

        this->num.setMomenta(mom);
        this->num.setMuR2(this->MuR2);

        this->ana.setMomenta(mom);
        this->ana.setMuR2(this->MuR2);
    }
};

int main(int argc, char* argv[])
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);

    if (argc != 1) {
        std::cout << "Error: " << argv[0] << " doesn't accept command line arguments.\n";
    }

    const int numPSPs { 1 };
    const int rseed1 { 1 };
    // std::chrono::nanoseconds total_time_a { 0 };
    // long int total_speedup { 0 };
    for (int i { rseed1 }; i < numPSPs + rseed1; ++i) {
        Test<dd_real> test(i);
        // total += test.primary_helicities();
        test.all_helicities(false);

        // std::array<int, test.get_mul()> hel { -1, -1, 1, 1, 1 };
        // test.compare(hel, true);

        // total_speedup += test.get_total_speedup();
        // total_time_a += test.get_total_time_analytic();
    }
    // std::cout << "<t_a>     " << std::chrono::duration_cast<std::chrono::microseconds>(total_time_a / numPSPs).count() << "\u03BCs\n";
    // std::cout << "<speedup> " << total_speedup / numPSPs << "x\n";
}
