#include <algorithm>
#include <array>
#include <cstddef>
#include <string>
#include <vector>

#include "chsums/0q3gA.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

constexpr int mul { 5 };

template <std::size_t m>
std::string genHelStr(const std::array<int, m>& helInt)
{
    std::string helStr;
    for (int hel : helInt) {
        helStr.append(hel == 1 ? "+" : "-");
    }
    return helStr;
}

// only supports type double
template <typename T>
std::vector<MOM<T>> genMom(int rseed)
{
    std::vector<double> scales2{{0.}};
    // args: multiplicity, rseed, sqrtS
    PhaseSpace<T> psp(mul, rseed, 1.);
    std::vector<MOM<T>> mom { psp.getPSpoint() };
    refineM(mom, mom, scales2);

#ifdef DEBUG
    psp.showPSpoint();
#endif // DEBUG

    return mom;
}

// change order of legs in amplitude (so swap helicities and momenta)
template <typename T>
T reorderAmp(const std::vector<MOM<T>>& momenta, const std::array<int, mul>& helicities, const std::array<int, mul>& reorder)
{
    // args: photon, scale
    Amp0q3gAA<T> amp(StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()), 1.);

    std::vector<MOM<T>> mom(mul);
    std::generate(mom.begin(), mom.end(), [&momenta, &reorder, n = 0]() mutable { return momenta[reorder[n++]]; });

#ifdef DEBUG
    for (MOM<T> m : mom) {
        std::cout << m << '\n';
    }
    std::cout << '\n';
#endif // DEBUG

    amp.setMomenta(mom);

    std::array<int, mul> hel;
    std::generate(hel.begin(), hel.end(), [&helicities, &reorder, n = 0]() mutable { return helicities[reorder[n++]]; });

#ifdef DEBUG
    for (int h : hel) {
        std::cout << h << ' ';
    }
    std::cout << '\n';
#endif // DEBUG

    return amp.virtsq(hel.data()).get0().real();
}

template <int n, typename T>
void reorderResults(const std::vector<MOM<T>>& momenta, const std::array<int, mul>& helicities, const std::array<std::array<int, mul>, n>& orders)
{
    std::cout << genHelStr(helicities) << '\n';
    for (std::array<int, mul> order : orders) {
        for (int o : order) {
            std::cout << o;
        }
        std::cout << ' ' << reorderAmp<T>(momenta, helicities, order) << '\n';
    }
    std::cout << '\n';
}

template <typename T>
void loopHels(int num)
{
    // args: photon, scale
    Amp0q3gAA<T> amp(StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()), 1.);

    for (int rseed { 1 }; rseed < num + 1; ++rseed) {
        const std::vector<MOM<T>> baseMom { genMom<T>(rseed) };
        for (int i0 { -1 }; i0 < 2; i0 += 2) {
            for (int i1 { -1 }; i1 < 2; i1 += 2) {
                for (int i2 { -1 }; i2 < 2; i2 += 2) {
                    for (int i3 { -1 }; i3 < 2; i3 += 2) {
                        for (int i4 { -1 }; i4 < 2; i4 += 2) {
                            const std::array<int, mul> baseHel { { i0, i1, i2, i3, i4 } };

                            // gluon cyclicity
                            const std::array<std::array<int, mul>, 3> orders0 { {
                                { 0, 1, 2, 3, 4 },
                                { 1, 2, 0, 3, 4 },
                                { 2, 0, 1, 3, 4 },
                            } };
                            reorderResults<3, T>(baseMom, baseHel, orders0);

                            // photon swap
                            const std::array<std::array<int, mul>, 2> orders1 { {
                                { 0, 1, 2, 3, 4 },
                                { 0, 1, 2, 4, 3 },
                            } };
                            reorderResults<2, T>(baseMom, baseHel, orders1);

                            // gluon reversal (comes with a minus sign in the amplitude)
                            const std::array<std::array<int, mul>, 2> orders2 { {
                                { 0, 1, 2, 3, 4 },
                                { 2, 1, 0, 3, 4 },
                            } };
                            reorderResults<2, T>(baseMom, baseHel, orders2);
                        }
                    }
                }
            }
        }
    }
}

int main(int argc, char* argv[])
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    if (argc != 1) {
        std::cout << "Error: " << argv[0] << " doesn't accept command line arguments.\n";
    }

    loopHels<dd_real>(1);
}
