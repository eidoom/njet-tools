#include <iostream>
#include <vector>

#include "qd/dd_real.h"

#include "analytic/0q3g2A-analytic.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

template <typename T> void run() {
  const int legs{5};
  const int Nc{3};
  const int Nf{5};
  const double mur{91.188};
  const double scaling{1.};
  const std::vector<double> scales2{{0}};
  const Flavour<double> Ax{
      StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C())};
  std::vector<int> hel{{-1, -1, +1, +1, +1}};

  PhaseSpace<T> ps(legs, 1);
  std::vector<MOM<T>> momenta{ps.getPSpoint()};
  refineM(momenta, momenta, scales2);

  Amp0q3g2A_a<T> old_amp(Ax, scaling);
  old_amp.setNf(Nf);
  old_amp.setNc(Nc);
  old_amp.setMuR2(mur * mur);
  old_amp.setMomenta(momenta);

  std::cout << old_amp.virtsq(hel.data()) << '\n';
}

int main(int argc, char *argv[]) {
  std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  std::cout.precision(16);

  std::cout << '\n';

  run<dd_real>();

  std::cout << '\n';
}
