#!/usr/bin/env bash

start=$1
num=$2
end=$1+$2
echo Starting $((11 * $num)) jobs

for channel in {0..10}; do
	for ((rseed = $start; rseed < $end; rseed++)); do
		./calc_otf $channel $rseed &
	done
done
