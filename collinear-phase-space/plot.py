#!/usr/bin/env python3

import numpy, matplotlib.pyplot

import reader, plotter


def plot(xs, ys, es, labels, ylabel, name, logy=True):
    fig, ax = matplotlib.pyplot.subplots()

    for x, y, e, label in zip(xs, ys, es, labels):
        ax.fill_between(
            x,
            y - e,
            y + e,
            alpha=0.2,
        )
        ax.plot(x, y, label=label)

    ax.set_xscale("log", base=10)
    if logy:
        ax.set_yscale("log", base=10)

    ax.set_title(r"$i||j$")
    ax.set_xlabel(r"$s_{ij}/s_{12}$")
    ax.set_ylabel(ylabel)
    if any(labels):
        ax.legend()

    fig.savefig(f"{name}.pdf", bbox_inches="tight")


def proc(x, ds, prec, k):
    ys = x[numpy.array(ds[prec]["ggggg"][:, 0], dtype=int)]
    val = ds[prec]["ggggg"][:, 7]
    if k:
        val /= ds[prec]["ggggg"][:, 1]
    err = val * ds[prec]["ggggg"][:, 8]  # assume L errors dominate
    return numpy.array([ys, val, err])


def proc_all(x, ds, k):
    ys1, val1, err1 = proc(x, ds, "f64f64", k)
    ys2, val2, err2 = proc(x, ds, "f128f64", k)
    ys3, val3, err3 = proc(x, ds, "f128f128", k)
    return [[ys1, ys2, ys3], [val1, val2, val3], [err1, err2, err3]]


if __name__ == "__main__":
    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")

    ylabel_a = r"$\mathcal{A}^{(2)} \cdot \mathcal{A}^{(0)}$"
    ylabel_k = ylabel_a + r"$/\left|\mathcal{A}^{(0)}\right|^2$"
    labels = numpy.array([*plotter.PREC.values()])

    ds = reader.read_proc("data", "res")
    ys = numpy.loadtxt("data/test.sijs.csv")

    ays, avs, aes = proc_all(ys, ds, False)
    ys, vs, es = proc_all(ys, ds, True)

    for name, logy in (("all", True), ("lin", False)):
        plot(
            ys,
            vs,
            es,
            labels,
            ylabel_k,
            name,
            logy=logy,
        )

    for name, logy in (("amp_all", True), ("amp_lin", False)):
        plot(
            ays,
            avs,
            aes,
            labels,
            ylabel_a,
            name,
            logy=logy,
        )

    plot(ys[2:], vs[2:], es[2:], labels[2:], ylabel_k, "f128")
    plot(ays[2:], avs[2:], aes[2:], labels[2:], ylabel_a, "amp_f128")

    plot(
        [ys[0], ys[2]],
        [ds["f64f64"]["ggggg"][:, 9], ds["f128f128"]["ggggg"][:, 9]],
        [[0 for _ in range(len(ys))] for ys in [ys[0], ys[2]]],
        [r"$\texttt{f64}$", r"$\texttt{f128}$"],
        "t ($\mu$s)",
        "time_sf",
    )
