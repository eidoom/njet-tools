#include <algorithm>
#include <complex>
#include <numeric>
#include <vector>

#include "ngluon2/Mom.h"

// for QD compatibility
using std::abs;

// declarations
// ~~~~~~~~~~~~

namespace cps {

template <typename T> bool test(const std::vector<MOM<T>> &moms);

template <typename T> T pp(const MOM<T> &p);

template <typename T> T pm(const MOM<T> &p);

template <typename T> std::complex<T> pt(const MOM<T> &p);

template <typename T> std::complex<T> ptb(const MOM<T> &p);

template <typename T>
MOM<std::complex<T>> spba(const std::vector<MOM<T>> &psr, int i, int j);

template <typename T> MOM<T> real(const MOM<std::complex<T>> &m);

template <typename T> MOM<T> imag(const MOM<std::complex<T>> &m);

template <typename T>
std::vector<MOM<T>> collinear(const std::vector<MOM<T>> &psr, T z, T d, int i);

template <typename T>
std::vector<T> invariants(const std::vector<MOM<T>> &ps);

} // namespace cps

// implementations
// ~~~~~~~~~~~~~~~

template <typename T> bool cps::test(const std::vector<MOM<T>> &moms) {
  T zero{1e-12};

  for (const MOM<T> &mom : moms) {
    if (abs(mom.mass()) > zero) {
      return false;
    }
  }

  MOM<T> sum{std::accumulate(moms.cbegin(), moms.cend(), MOM<T>())};

  for (int i{0}; i < 4; ++i) {
    if (sum.x(i) > zero) {
      return false;
    }
  }

  return true;
}

template <typename T> T cps::pp(const MOM<T> &p) { return p.x0 + p.x3; }

template <typename T> T cps::pm(const MOM<T> &p) { return p.x0 - p.x3; }

template <typename T> std::complex<T> cps::pt(const MOM<T> &p) {
  return std::complex<T>(p.x1, p.x2);
}

template <typename T> std::complex<T> cps::ptb(const MOM<T> &p) {
  return std::conj(cps::pt(p));
}

template <typename T>
MOM<std::complex<T>> cps::spba(const std::vector<MOM<T>> &psr, const int i,
                               const int j) {
  const T pip{cps::pp(psr[i])}, pjp{cps::pp(psr[j])};
  const std::complex<T> pitb{cps::ptb(psr[i])}, pjt{cps::pt(psr[j])},
      n{1. / std::sqrt(std::complex<T>(pip * pjp))};
  return n *
         MOM<std::complex<T>>(pip * pjp + pitb * pjt, pip * pjt + pitb * pjp,
                              std::complex(0., 1.) * (-pip * pjt + pitb * pjp),
                              pip * pjp - pitb * pjt);
}

template <typename T> MOM<T> cps::real(const MOM<std::complex<T>> &m) {
  return MOM<T>(m.x0.real(), m.x1.real(), m.x2.real(), m.x3.real());
}

template <typename T> MOM<T> cps::imag(const MOM<std::complex<T>> &m) {
  return MOM<T>(m.x0.imag(), m.x1.imag(), m.x2.imag(), m.x3.imag());
}

template <typename T>
std::vector<MOM<T>> cps::collinear(const std::vector<MOM<T>> &psr, const T z,
                                   const T d, const int i) {
  const int m = psr.size(), mm{m + 1}, j{(i + 1) % m};
  const MOM<T> k(d * cps::imag(cps::spba(psr, i, j) - cps::spba(psr, j, i)));
  const T sij{2. * dot(psr[i], psr[j])}, k2{dot(k, k)};
  std::vector<MOM<T>> psf(mm);
  std::copy_n(psr.cbegin(), i, psf.begin());
  psf[i] = z * psr[i] + k - k2 * psr[j] / (z * sij);
  psf[(i + 1) % mm] = (1. - z) * psr[i] - k - k2 * psr[j] / ((1. - z) * sij);
  psf[(i + 2) % mm] = (1. + k2 / (sij * z * (1. - z))) * psr[j];
  std::copy(psr.cbegin() + i + 2, psr.cend(), psf.begin() + i + 3);
  return psf;
}

template <typename T>
std::vector<T> cps::invariants(const std::vector<MOM<T>> &ps) {
  const int m = ps.size();
  std::vector<T> v(m);
  for (int i{0}; i < m; ++i) {
    const int j{(i + 1) % m};
    v[i] = dot(ps[i], ps[j]);
  }
  return v;
}
