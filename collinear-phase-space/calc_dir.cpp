#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

#include "finrem/0q5g/ggggg.h"
// #include "finrem/2q3g/ggqbqg.h"
// #include "finrem/2q3g/qbgqgg.h"
// #include "finrem/2q3g/qbqggg.h"
// #include "finrem/4q1g/uDdUg.h"
// #include "finrem/4q1g/uUdDg.h"
// #include "finrem/4q1g/uUuUg.h"
// #include "finrem/4q1g/udUDg.h"
// #include "finrem/4q1g/ugUdD.h"
// #include "finrem/4q1g/ugUuU.h"
// #include "finrem/4q1g/uuUUg.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

#include "cps.hpp"

template <template <typename, typename> class AMP> void run() {
  const double sqrtS{1.}, mur{91.188}, z{0.5}, base(1.1);
  const int legs{5}, rseed{1}, Nc{3}, Nf{5}, i{2}, start{49}, num{100};
  const std::vector<double> scales2{{0.}};

  PhaseSpace<double> ps4(legs - 1, rseed, sqrtS);
  const std::vector<MOM<double>> moms4{ps4.getPSpoint()};

  AMP<double, double> amp;
  amp.setNf(Nf);
  amp.setNc(Nc);
  amp.setMuR2(std::pow(mur, 2));

  for (int a{0}; a < num; ++a) {
    const int p{start + a};
    const double d{std::pow(base, -p)};

    std::vector<MOM<double>> moms5{cps::collinear(moms4, z, d, i)};
    refineM(moms5, moms5, scales2);

    std::vector<double> v{cps::invariants(moms5)};

    // std::cout << '\n';
    // for (const MOM<double> &mom : moms5) {
    //   std::cout << mom << '\n';
    // }
    // std::cout << '\n';

    amp.setMomenta(moms5);
    amp.initFinRem();
    amp.setSpecFuncs();

    double born{amp.c0lx0l_fin()};
    double virt{amp.c1lx0l_fin()};
    double virtsq{amp.c1lx1l_fin()};
    double dblvirt{amp.c2lx0l_fin()};

    std::cout << std::setw(3) << a << " | y = " << v[i] / v[0]
              << " | VV/B: " << (dblvirt + virtsq) / born << '\n';
  }
}

int main() {
  std::cout << std::scientific;

  run<Amp0q5g_ggggg_a2l>();
}
