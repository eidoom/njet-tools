#!/usr/bin/env python3

import random, math, operator, sys

import pprint

D = 4
ZERO = 1e2 * sys.float_info.epsilon


def random_unit():
    return random.uniform(ZERO, 1 - ZERO)


def random_momentum():
    tmp = [random_unit() for _ in range(D - 1)]
    return Momentum([math.sqrt(sum(map(lambda a: a**2, tmp))), *tmp])


def empty_momentum():
    return Momentum([None] * D)


def zero_momentum():
    return Momentum([0.0] * D)


def sum_momenta(momenta):
    return sum(momenta, start=zero_momentum())


class Momentum(list):
    def __init__(self, init):
        super().__init__(init)

    def __neg__(self):
        return Momentum(map(operator.neg, self))

    def __add__(self, other):
        return Momentum(map(operator.add, self, other))

    def __sub__(self, other):
        return Momentum(map(operator.sub, self, other))

    def __mul__(self, other):
        return self[0] * other[0] - sum(map(operator.mul, self[1:], other[1:]))

    def mass(self):
        return self * self

    def massless(self):
        return self.mass() < ZERO


def random_point(n):
    """
    App. A of https://arxiv.org/pdf/1910.11355.pdf
    https://github.com/GDeLaurentis/lips/blob/master/lips/particles.py#L111
    """
    init = [random_momentum() for _ in range(n - 2)]
    tmp = sum_momenta(init)
    aa = -tmp.mass() / (2.0 * (tmp[0] - tmp[3]))
    a = Momentum([aa, 0, 0, aa])
    b = -tmp - a
    p = Point([*init, a, b])
    return p if p.check() else random_point(n)


class Point(list):
    def __init__(self, init):
        super().__init__(init)

    def momentum_conservation(self):
        return all([abs(comp) < ZERO for comp in sum_momenta(self)])

    def massless(self):
        return all([m.massless() for m in self])

    def check(self):
        return self.momentum_conservation() and self.massless()


if __name__ == "__main__":
    pt = random_point(4)
    pprint.pprint(pt)
