#!/usr/bin/env python3

import argparse, math, itertools

import numpy, matplotlib

import reader_numpy, plotter

from plot_paper import PREC, CHAN, proc, C2LX0L_V


def histo_single(
    ax,
    x,
    ys,
    es,
    labels,
    channel,
    xlabel=True,
    ylabel=True,
    tick_left=True,
    tick_bottom=True,
    tick_right=False,
    tick_top=False,
    m=0.05,
    ha="right",
    va="top",
):
    ax.set_xscale("log", base=10)
    ax.set_yscale("log", base=10)

    for y, e, label in zip(ys, es, labels):
        ax.fill_between(
            x,
            y - e,
            y + e,
            alpha=0.2,
        )
        ax.plot(x, y, label=label)

    if xlabel:
        ax.set_xlabel(r"$s_{34}/s_{12}$")

    if ylabel:
        ax.set_ylabel(
            r"$\mathcal{A}^{(2)} \cdot \mathcal{A}^{(0)}/\left|\mathcal{A}^{(0)}\right|^2$"
        )

    ax.set_ylim(9.2e3, 4.3e4)

    if tick_right:
        ax.yaxis.set_label_position("right")

    ax.tick_params(
        axis="both",
        direction="in",
        which="both",
        top=tick_top,
        bottom=tick_bottom,
        left=tick_left,
        right=tick_right,
        # labeltop=False,
        # labelbottom=True,
        # labelleft=tick_left,
        # labelright=tick_right,
    )

    ax.text(
        x=1 - m if ha == "right" else m,
        y=1 - m if va == "top" else m,
        s=channel,
        transform=ax.transAxes,
        ha=ha,
        va=va,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser("analyse collinear limit of 5p 2L")
    parser.add_argument(
        "-r", "--refresh-data", action="store_true", help="Don't use cached data"
    )
    args = parser.parse_args()

    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")

    labels = numpy.array([*PREC.values()])
    ch_ns = numpy.array([*CHAN.values()])

    ds = reader_numpy.read_proc("data_paper", "res", not args.refresh_data)
    ys = numpy.loadtxt("data_paper/sijs.csv")

    vs, es = proc(ds, C2LX0L_V)

    w = 4.8
    r = 2 / (1 + math.sqrt(5))

    fig, axs = matplotlib.pyplot.subplots(
        nrows=2,
        ncols=2,
        figsize=(2 * w, 2 * w * r),
        sharex="all",
        sharey="all",
    )

    fig.subplots_adjust(
        wspace=0,
        hspace=0,
    )

    for i, (ax, channel, (va, ha)) in enumerate(
        zip(
            itertools.chain.from_iterable(axs),
            (0, 3, 7, 9),
            (
                ("top", "left"),
                ("top", "left"),
                ("top", "left"),
                ("top", "left"),
            ),
        )
    ):
        left = i % 2 == 0
        bottom = i in (2, 3)

        histo_single(
            ax,
            ys,
            numpy.mean(vs[channel], axis=0),
            numpy.mean(es[channel], axis=0),
            labels,
            ch_ns[channel],
            xlabel=bottom,
            ylabel=left,
            tick_left=left,
            tick_bottom=bottom,
            tick_top=not bottom,
            tick_right=not left,
            ha=ha,
            va=va,
        )

    axs[0, 1].legend(loc="upper right", frameon=False)

    plotter.write(fig, "collinear", vector=True)
