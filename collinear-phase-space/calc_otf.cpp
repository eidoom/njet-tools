#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "finrem/0q5g/ggggg.h"
#include "finrem/2q3g/ggqbqg.h"
#include "finrem/2q3g/qbgqgg.h"
#include "finrem/2q3g/qbqggg.h"
#include "finrem/4q1g/uDdUg.h"
#include "finrem/4q1g/uUdDg.h"
#include "finrem/4q1g/uUuUg.h"
#include "finrem/4q1g/udUDg.h"
#include "finrem/4q1g/ugUdD.h"
#include "finrem/4q1g/ugUuU.h"
#include "finrem/4q1g/uuUUg.h"
#include "finrem/common/rescue.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

#include "cps.hpp"

template <template <typename, typename> class AMP>
void run(const std::vector<MOM<double>> &moms4, const double cutoff,
         const std::string &channel, const std::string &name) {
  const double mur{91.188}, z{0.5}, base{1.1};
  const int Nc{3}, Nf{5}, i{2}, start{49}, num{100};

  Rescue2LNNLO<AMP> amp(cutoff);
  amp.setNf(Nf);
  amp.setNc(Nc);
  amp.setMuR2(std::pow(mur, 2));

  for (int a{0}; a < num; ++a) {
    std::cout << a << " / " << num << '\n';

    const int p{start + a};

    const double d{std::pow(base, -p)};

    std::vector<MOM<double>> moms5{cps::collinear(moms4, z, d, i)};

    const std::vector v{cps::invariants(moms5)};

    {
      std::ofstream o("sijs." + name, std::ios::app);
      o << std::scientific << std::setprecision(16) << v[i] / v[0] << '\n';
    }

    amp.setMomenta(moms5);

    amp.eval_timed(a, channel, name);
  }
}

int main(int argc, char *argv[]) {
  if (argc != 4) {
    std::cerr << "Wrong number of command lines arguments!" << '\n'
              << "Use as: ./calc_otf <channel #> <rseed #> <cutoff>" << '\n';
    std::exit(EXIT_FAILURE);
  }

  const int legs{5}, channel{std::stoi(argv[1])}, rseed{std::stoi(argv[2])};
  assert(rseed > 0);
  const double sqrtS{1.}, cutoff{std::stod(argv[3])};
  const std::string name{std::to_string(channel) + "." + std::to_string(rseed) +
                         ".csv"};

  PhaseSpace<double> ps4(legs - 1, rseed, sqrtS);
  std::vector<MOM<double>> moms4{ps4.getPSpoint()};

  switch (channel) {
  case 0:
    // 5g

    // ggggg
    run<Amp0q5g_ggggg_a2l>(moms4, cutoff, std::to_string(channel), name);
    break;

    // 2q3g

    // ggqbqg
  case 1:
    run<Amp2q3g_ggqbqg_a2l>(moms4, cutoff, std::to_string(channel), name);
    break;

    // qbgqgg
  case 2:
    run<Amp2q3g_qbgqgg_a2l>(moms4, cutoff, std::to_string(channel), name);
    break;

    // qbqggg
  case 3:
    run<Amp2q3g_qbqggg_a2l>(moms4, cutoff, std::to_string(channel), name);
    break;

    // 2q2Q1g

    // uDdUg qQbQqbg
  case 4:
    run<Amp4q1g_uDdUg_a2l>(moms4, cutoff, std::to_string(channel), name);
    break;

    // udUDg qQqbQbg
  case 5:
    run<Amp4q1g_udUDg_a2l>(moms4, cutoff, std::to_string(channel), name);
    break;

    // ugUdD qgqbQQb
  case 6:
    run<Amp4q1g_ugUdD_a2l>(moms4, cutoff, std::to_string(channel), name);
    break;

    // uUdDg qqbQQbg
  case 7:
    run<Amp4q1g_uUdDg_a2l>(moms4, cutoff, std::to_string(channel), name);
    break;

    // 4q1g

    // ugUuU qgqbqqb
  case 8:
    run<Amp4q1g_ugUuU_a2l>(moms4, cutoff, std::to_string(channel), name);
    break;

    // uUuUg qqbqqbg
  case 9:
    run<Amp4q1g_uUuUg_a2l>(moms4, cutoff, std::to_string(channel), name);
    break;

    // uuUUg qqqbqbg
  case 10:
    run<Amp4q1g_uuUUg_a2l>(moms4, cutoff, std::to_string(channel), name);
    break;

  default:
    std::cerr << channel << " is an unsupported channel" << '\n';
    std::exit(EXIT_FAILURE);
  }
}
