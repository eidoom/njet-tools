#!/usr/bin/env python3

import math, cmath, random

import numpy

from phase_space_numpy import SIGMA, D, check, dot, random_unit

X = [0, 1, 2, 3]


# I did this on paper
def point(n, rseed=0):
    random.seed(rseed)

    ps = numpy.empty(n, dtype=numpy.ndarray)

    e1, e2 = (random_unit() for _ in range(2))
    ps[0] = numpy.array([-e1, 0, 0, e1])
    ps[1] = numpy.array([-e2, 0, 0, -e2])

    for i in range(2, n - 1):
        ps[i] = numpy.empty(D)
        for j in range(1, D):
            ps[i][j] = random_unit()
        ps[i][0] = math.sqrt(sum(ps[i][1:] ** 2))

    q = sum(ps[:-2])

    x = -dot(q, q) / (2 * dot(q, ps[-2]))

    ps[-2] *= x

    ps[-1] = -q - ps[-2]

    return ps


# these are the spinors that NJet uses


def pp(p):
    return p[X[0]] + p[X[3]]


def pm(p):
    return p[X[0]] - p[X[3]]


def pt(p):
    return p[X[1]] + 1j * p[X[2]]


def ptb(p):
    return pt(p).conjugate()


def spna(p):
    return cmath.sqrt(pp(p))


def spnb(p):
    return ptb(p) / spna(p)


def spal(p):
    return numpy.array([pm(p), -ptb(p)]) / spnb(p)


def spar(p):
    return numpy.array([[1], [pm(p) / ptb(p)]]) * spna(p)


def spbl(p):
    return numpy.array([pp(p), ptb(p)]) / spna(p)


def spbr(p):
    return numpy.array([[1], [-pp(p) / ptb(p)]]) * spnb(p)


# def spba(p1, p2, p3):
#     return (
#         spna(p3)
#         / (spna(p1) * ptb(p3))
#         * (
#             pm(p3) * (pp(p2) * ptb(p1) - pp(p1) * ptb(p2))
#             + ptb(p3) * (pm(p2) * pp(p1) - pt(p2) * ptb(p1))
#         )
#     )


def spba(ps, i1, i2):
    p1 = ps[i1]
    p2 = ps[i2]
    return numpy.array([(spbl(p1) @ SIGMA[X[mu]] @ spar(p2))[0] for mu in range(D)])


def collinear(ps, z, d, i):
    m = len(ps)
    j = (i + 1) % m

    p = ps[i]
    n = ps[j]
    spn = 2 * dot(p, n)

    k = d * (spba(ps, i, j) + spba(ps, j, i)).real

    k2 = dot(k, k)

    p1 = z * p + k - k2 * n / (z * spn)

    p2 = (1 - z) * p - k - k2 * n / ((1 - z) * spn)

    p3 = (1 + k2 / (spn * z * (1 - z))) * n

    new = numpy.array(
        [
            *ps[1 if j == 0 else 0 : i],
            p1.real,
            p2.real,
            p3.real,
            *ps[i + 2 :],
        ]
    )

    return new


if __name__ == "__main__":
    ps4 = point(4)
    print(ps4)
    print("ps4", check(ps4))

    ps5 = collinear(ps4, 0.5, 1e-1, 2)
    print(ps5)
    print("ps5", check(ps5))
