#!/usr/bin/env python3

import random, math, sys, itertools, cmath

import numpy
from pprint import pprint

from phase_space import D, ZERO, random_unit

SIGMA = [
    numpy.array(x)
    for x in (
        [[1, 0], [0, 1]],
        [[0, 1], [1, 0]],
        [[0, -1j], [1j, 0]],
        [[1, 0], [0, -1]],
    )
]

SIGMA_BAR = [(-1 if i > 0 else 1) * pauli for i, pauli in enumerate(SIGMA)]


def dot(a, b):
    return a[0] * b[0] - a[1:].dot(b[1:])


def mom():
    tmp = numpy.array([random_unit() for _ in range(D - 1)])
    return numpy.array([math.sqrt(sum(tmp**2)), *tmp])


def check(ps, v=False):
    if v:
        print("mom cons", sum(ps))
        print("on-shell", [dot(q, q) for q in ps])
    return all(
        [abs(x) < ZERO for x in itertools.chain(sum(ps), (dot(q, q) for q in ps))]
    )


def point(n):
    init = [mom() for _ in range(n - 2)]
    tmp = sum(init)
    aa = -dot(tmp, tmp) / (2 * (tmp[0] - tmp[3]))
    a = numpy.array([aa, 0, 0, aa])
    b = -tmp - a
    return numpy.array([*init, a, b])


def spinors(p):
    t0 = cmath.sqrt(p[0] + p[3])
    t1 = p[1] + 1j * p[2]
    t2 = t1 / t0

    ar = [[t0], [t2]]
    al = [t2, -t0]

    t3 = t1.conjugate() / t0

    sr = [[t3], [-t0]]
    sl = [t0, t3]

    return [numpy.array(x) for x in (ar, al, sr, sl)]


def kinematics(ps):
    kin = {"p": ps}
    for a in ("al", "sl"):
        kin[a] = numpy.empty((4, 2), dtype=complex)
    for a in ("ar", "sr"):
        kin[a] = numpy.empty((4, 2, 1), dtype=complex)
    for i, p in enumerate(ps):
        kin["ar"][i], kin["al"][i], kin["sr"][i], kin["sl"][i] = spinors(p)
    return kin


def triple_vec(kin, a, i, j, bar=False):
    b = "a" if a == "s" else "s"
    s = SIGMA_BAR if bar else SIGMA
    return numpy.array(
        [(kin[a + "l"][i] @ s[mu] @ kin[b + "r"][j])[0] for mu in range(D)]
    )


# < i sigma^mu j ]
def left(kin, i, j, bar=False):
    return triple_vec(kin, "a", i, j, bar)


# [ i sigma^mu j >
def right(kin, i, j, bar=False):
    return triple_vec(kin, "s", i, j, bar)


def check_spinors(kin):
    n = len(kin["p"])
    checks = []

    def inner(a, i, j):
        return (kin[a + "l"][i] @ kin[a + "r"][j])[0]

    def mom_cons(j, k):
        return sum(
            inner("s", j, i) * inner("a", i, k) for i in range(n) if i not in (j, k)
        )

    def schouten(a, i, j, k, l):
        return (
            inner(a, i, k) * inner(a, j, l)
            + inner(a, i, l) * inner(a, k, j)
            - inner(a, i, j) * inner(a, k, l)
        )

    # because energy not always positive, spinors not related by complex conjugation!
    # print(inner("a", 0, 1)+inner("s", 0, 1).conjugate())

    # pprint(
    #     {
    #         (i, j): (inner("a", i, j) - inner("s", i, j).conjugate())
    #         # < ZERO
    #         for i in range(n)
    #         for j in range(i + 1, n)
    #     }
    # )

    for a in ("a", "s"):
        # anti-symmetry
        checks += [inner(a, i, i) < ZERO for i in range(n)]
        checks += [
            inner(a, i, j) + inner(a, j, i) < ZERO
            for j in range(n)
            for i in range(j + 1, n)
        ]
        # schouten
        checks += [
            schouten(a, i, j, k, l) < ZERO
            for i in range(n)
            for j in range(n)
            for k in range(n)
            for l in range(n)
        ]

    # mom cons
    checks += [mom_cons(i, j) < ZERO for j in range(n) for i in range(n)]

    # gordon
    checks += [all((right(kin, i, i) - 2 * kin["p"][i]) < ZERO) for i in range(n)]

    # fierz
    checks += [
        (
            dot(right(kin, i, j), right(kin, k, l))
            - 2 * inner("s", i, k) * inner("a", l, j)
        )
        < ZERO
        and (
            dot(left(kin, i, j), left(kin, k, l))
            - 2 * inner("a", i, k) * inner("s", l, j)
        )
        < ZERO
        for i in range(n)
        for j in range(n)
        for k in range(n)
        for l in range(n)
    ]

    # charge conj
    checks += [
        all((right(kin, i, j) - left(kin, j, i, bar=True)) < ZERO)
        and all((right(kin, i, j, bar=True) - left(kin, j, i)) < ZERO)
        for i in range(n)
        for j in range(n)
    ]

    # sij
    checks += [
        inner("a", i, j) * inner("s", j, i) - 2 * dot(kin["p"][i], kin["p"][j]) < ZERO
        for j in range(n)
        for i in range(j + 1, n)
    ]

    return all(checks)


def collinear(kin, z, d, i):
    m = len(kin["p"])
    j = (i + 1) % m

    p = kin["p"][i]
    n = kin["p"][j]
    spn = 2 * dot(p, n)

    k = d * (right(kin, i, j) + right(kin, j, i)).real

    k2 = dot(k, k)

    p1 = z * p + k - k2 * n / (z * spn)

    p2 = (1 - z) * p - k - k2 * n / ((1 - z) * spn)

    p3 = (1 + k2 / (spn * z * (1 - z))) * kin["p"][j]

    new = numpy.array(
        [
            *kin["p"][1 if j == 0 else 0 : i],
            p1.real,
            p2.real,
            p3.real,
            *kin["p"][i + 2 :],
        ]
    )

    return new


def sij(ps, i, j):
    return dot(ps[i], ps[j])


def sijs(ps):
    n = len(ps)
    return {(i, j): sij(ps, i, j) for i in range(n) for j in range(i + 1, n)}


def gen(n, z, d, i):
    psr = point(n - 1)
    kin = kinematics(psr)
    return collinear(kin, z, d, i)


if __name__ == "__main__":
    print("0 =", ZERO)

    ps4 = point(4)
    print("ps4", check(ps4))
    print(ps4)

    kin = kinematics(ps4)
    print("spinors", check_spinors(kin))

    ps5 = collinear(kin, 0.4, 1e-1, 0)
    print("ps5", check(ps5))
    print(ps5)
    pprint(sijs(ps5))
