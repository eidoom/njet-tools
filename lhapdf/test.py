#!/usr/bin/env python3

import math
import numpy, matplotlib.pyplot, lhapdf

if __name__ == "__main__":
    p = lhapdf.mkPDF("NNPDF31_nlo_as_0118")

    # xs = numpy.linspace(p.xMin, p.xMax, 100)
    xs = numpy.logspace(math.log(p.xMin), math.log(p.xMax), 100)

    fs = numpy.array([p.xfxQ(21, x, 91.188) for x in xs])

    fig, ax = matplotlib.pyplot.subplots()

    # ax.plot(xs, fs)
    # ax.semilogx(xs, fs)
    # ax.semilogy(xs, fs)
    ax.loglog(xs, fs)

    ax.set_xlabel("x")
    ax.set_ylabel("f")

    fig.savefig("pdf.png")
