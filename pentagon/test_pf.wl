(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/pentagon/math2njet"];

<<PentagonFunctions`

evaluator = StartEvaluatorProcess[F@@#&/@{
{1,1,1},
{1,2,3},
{2,1,4}
}];

<< "InitTwoLoopToolsFF.m"
<< "setupfiles/Setup_5g.m"

invariantsvalues = {
100.00000000000000,
-44.627966432493800,
29.602455883989265,
17.852178035369874,
-22.592147032542179
};

values = EvaluateFunctions[evaluator, invariantsvalues];

values



