e.g. dsm2p0
## w/o FORM
`proc.wl` then `../conv-*.sh` to make `irpoles.m` and `irrules.m`
## w/ FORM
```sh
cd dsm2p...
../conv-irrules-form.sh ..._PSanalyticcoeffrules.m
form irrules.frm
../pp-form.sh irrules.c
vi irrules.cpp
```
Or use [`tform`](https://www.nikhef.nl/~form/maindir/publications/tform.pdf)?

## Updates
* Perform simultaneous optimisation over all expressions as described in 4.2 of [optim.pdf](https://www.nikhef.nl/~form/maindir/publications/optim.pdf)
