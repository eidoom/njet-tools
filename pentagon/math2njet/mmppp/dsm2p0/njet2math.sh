#!/usr/bin/env bash

echo "{" > njet-irpoles.m

perl \
    -0777p \
    -e  "
        s|\(([\d.e+\-]+),([\d.e+\-]+)\)|(\1+I*(\2))|g;
        s|$|,|gm;
        s|e([+-]\d+)|*10^(\1)|g;
        s|e|eps|g;
        " \
    $1 >> njet-irpoles.m

echo "}" >> njet-irpoles.m
