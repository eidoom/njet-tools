#!/usr/bin/env bash

# Use directly on `*.m` file from Simon

FRM=irrules.frm
C=irrules.c

echo -e "#-\nS u,x1,x2,x3,x4,x5;" > $FRM
echo "ExtraSymbols,array,Z;" >> $FRM

perl \
    -0777p \
    -e  "
        s|f\[(\d+), (\d+)\]|L gs\1s\2|g;
        s|f\[(\d+), (\d+), (\d+)\]|L gs\1s\2s\3|g;
        s|ex\[(\d)\]|x\1|g;
        s|->|=|g;
        s|,[ \n]*(L gs)|;\n\1|g;
        s|{||g;
        s|}|;|g;
        " \
    $1 >> $FRM

echo -e "\n.sort\nFormat O4;\nFormat C;" >> $FRM

h="L H="
i=1
gs=$(rg -o "g[^ ]+" $FRM)

for g in ${gs[@]}; do
    h+="+u^$((i++))*$g"
done

echo "$h;" >> $FRM

echo -e "B u;\n.sort\n#optimize H\nB u;\n.sort" >> $FRM

i=1

for g in ${gs[@]}; do
    echo "L ${g}u = H[u^$((i++))];" >> $FRM
done

echo -e ".sort" >> $FRM

echo "#write <$C> \"std::array<TreeValue, \`optimmaxvar_'> Z;\"" >> $FRM
echo "#write <$C> \"%O\"" >> $FRM

for g in ${gs[@]}; do
    echo "#write <$C> \"const TreeValue $g{%E};\n\", ${g}u" >> $FRM
done

echo ".end" >> $FRM
