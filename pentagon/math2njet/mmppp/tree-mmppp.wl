(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/pentagon/math2njet/mmppp"];

<< "InitTwoLoopToolsFF.m"
<< "setupfiles/Setup_5g.m"


treeX0 = Power[ex[1], 3] * Power[ex[2], 2] * ex[3];


treeS[o_]:=spA[p[o[[1]]],p[o[[2]]]]^3/(spA[p[o[[2]]],p[o[[3]]]]*spA[p[o[[3]]],p[o[[4]]]]*spA[p[o[[4]]],p[o[[5]]]]*spA[p[o[[5]]],p[o[[1]]]])


treeS[{1,2,3,4,5}]


treeX[o_]:=GetMomentumTwistorExpression[treeS[o],PSanalytic]


treeX[{1,2,3,4,5}]


os={        { 0, 1, 2, 3, 4 },
        { 0, 3, 4, 2, 1 },
        { 0, 4, 2, 3, 1 },
        { 0, 1, 3, 4, 2 },
        { 0, 1, 4, 2, 3 },
        { 0, 2, 3, 4, 1 },
        { 0, 2, 1, 3, 4 },
        { 0, 3, 4, 1, 2 },
        { 0, 2, 3, 1, 4 },
        { 0, 3, 1, 4, 2 },
        { 0, 4, 2, 1, 3 },
        { 0, 4, 1, 2, 3 }
        }+1


xtrees=treeX/@os//.{Power[a_,b_]->pow[a,b]}


Export["trees.m",xtrees];
