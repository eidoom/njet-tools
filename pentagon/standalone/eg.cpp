#include "FunctionID.h"

#include <complex>
#include <iostream>
#include <vector>

int main()
{
    std::cout << '\n';

    using namespace PentagonFunctions;

    // in this example we use double precision
    // if you compiled with high-precision support, you can try switching to dd_real or qd_real
    using T = double;

    /**
     * Construction and initialization phase.
     * Can be done only once.
     */

    // a vector of FunctionID, where we put indices of all pentagon funcitons that we are interested in.
    // { w, i, j } for g^{(w)}_{i, j} (w=1,2)
    // or { w, i } for g^{(w)}_{i} (w=3,4)
    const std::vector<FunctionID> needed_functions {
        { 1, 1, 1 },
        { 1, 3, 1 },
        { 1, 2, 10 },
        { 2, 1, 3 },
        { 2, 2, 1 },
        { 3, 3 },
        { 3, 17 },
        { 3, 111 },
        { 4, 17 },
        { 4, 122 },
        { 4, 436 },
        { 4, 466 },
        { 4, 472 },
    };

    // a vector which will store evaluator function objects (which are callable)
    std::vector<FunctionObjectType<T>> function_evaluators;

    // construct evaluator objects

    std::cout << "Construction: " << '\n';
    for (FunctionID f : needed_functions) {
        function_evaluators.push_back(f.get_evaluator<T>());
        std::cout << f << '\n';
    }
    std::cout << '\n';

    /**
     * Evaluation phase.
     * Evaluation can be repeated any number of times using the evaluator objects function_evaluators.
     */

    // // a kinematical point is given by Mandelstam invariants
    const std::array<T, 5> sij {
        169.00000000000000000000000000000000000000000000000000000000000024,
        -127.03554269209390121978365556633554285695735563298040778575769793,
        33.796369695873051618118516726995190978444086393722898206354653227,
        34.135578250263955969284362230088631173450982210677708512608176643,
        -112.93867208272743252147419350392861691229747936905042006487121976,
    };
    const Kin<T> k(sij);
    // // or:
    // const Kin<T> k(
    //     169.00000000000000000000000000000000000000000000000000000000000024,
    //     -127.03554269209390121978365556633554285695735563298040778575769793,
    //     33.796369695873051618118516726995190978444086393722898206354653227,
    //     34.135578250263955969284362230088631173450982210677708512608176643,
    //     -112.93867208272743252147419350392861691229747936905042006487121976);

    // evaluation is done by invoking the function objects

    std::cout << "Evaluation: " << '\n';

    for (std::function<std::complex<T>(const PentagonFunctions::Kin<T>&)> f : function_evaluators) {
        std::cout << f(k) << '\n';
    }
    std::cout << '\n';

    std::cout << "Alternative: " << '\n';
    for (size_t i { 0 }; i < function_evaluators.size(); ++i) {
        std::complex<T> ri { function_evaluators.at(i)(k) };
        std::cout << needed_functions.at(i) << " = " << ri << '\n';
    }

    std::cout << '\n';

    FunctionObjectType<T> f { FunctionID( 1, 1, 1 ).get_evaluator<T>() };
    std::cout << f(k) << '\n';

    std::cout << '\n';
}
