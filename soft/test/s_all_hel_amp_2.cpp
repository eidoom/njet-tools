#include <array>
#include <cmath>
#include <complex>
#include <iostream>
#include <numeric>

#include "analytic/0q4g-analytic.h"
#include "analytic/0q5g-analytic.h"
#include "ir/soft_gg2g-analytic.h"
#include "ngluon2/Mom.h"

#include "hel.hpp"

// int main(int argc, char* argv[])
int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);

    // p[3] -> 0
    const std::array<MOM<double>, 5> full_mom { {
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, -5.0000000000000000e-01 },
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, 5.0000000000000000e-01 },
        { 4.9999949999999999e-01, 4.3301226887951738e-01, 0.0000000000000000e+00, 2.4999975000000005e-01 },
        { 4.9999999999772449e-06, -2.2505887718767004e-06, 1.7633487034433740e-06, -4.1018839000804920e-06 },
        { 4.9999550000000009e-01, -4.3301001829074548e-01, -1.7633487034433740e-06, -2.4999564811609998e-01 },
    } };

    std::array<MOM<double>, 4> reduced_mom;
    std::copy_n(full_mom.cbegin(), 3, reduced_mom.begin());
    reduced_mom[3] = full_mom[4];

    const double scale { 1. };
    Amp0q5g_a<double> full_amp(scale);
    Amp0q4g_a<double> reduced_amp(scale);
    Softgg2g_a<double> soft_amp(scale);

    full_amp.setMomenta(full_mom.data());
    reduced_amp.setMomenta(reduced_mom.data());

    std::cout
        << '\n'
        << "zero indexed" << '\n'
        << "p[3]^0            = " << full_mom[3].x0 << '\n'
        << '\n';

    double all_full { 0. };
    double all_lim { 0. };

    for (int h1 { -1 }; h1 < 2; h1 += 2) {
        for (int h2 { -1 }; h2 < 2; h2 += 2) {
            for (int h3 { -1 }; h3 < 2; h3 += 2) {
                for (int h4 { -1 }; h4 < 2; h4 += 2) {
                    for (int h5 { -1 }; h5 < 2; h5 += 2) {
                        Helicity<5> full_hels { h1, h2, h3, h4, h5 };
                        Helicity<4> reduced_hels { h1, h2, h3, h5 };

                        if (
                            (std::abs(std::accumulate(full_hels.cbegin(), full_hels.cend(), 0)) == 1) && (std::abs(std::accumulate(reduced_hels.cbegin(), reduced_hels.cend(), 0)) == 0)) {
                            Helicity<3> soft_hels { -h3, h4, -h5 };

                            // helicity amplitude modulus squared - includes colour
                            const double amp_val { full_amp.born(full_hels.data()) };

                            double lim { 0. };
                            int ref_index { 0 };
                            for (int i { 0 }; i < 4; ++i) {
                                for (int j { i + 1 }; j < 4; ++j) {
                                    while ((ref_index == i) || (ref_index == j)) {
                                        ref_index = (ref_index + 1) % 4;
                                    }
                                    // the second argument is a reference momentum
                                    soft_amp.setMomenta({ reduced_mom[i], full_mom[3], reduced_mom[j] }, reduced_mom[ref_index]);
                                    const double soft_val { soft_amp.born(soft_hels.data()) };
                                    const double cc_val { reduced_amp.born_ccij(reduced_hels.data(), i, j) };
                                    lim += cc_val * soft_val;
                                }
                            }
                            lim *= 2.;

                            all_full += amp_val;
                            all_lim += lim;

                            std::cout
                                << full_hels << " > " << reduced_hels << " * " << soft_hels << '\n'
                                // << "full amplitude    = " << amp_val << '\n'
                                // << "reduced amplitude = " << red_amp_val << '\n'
                                // << "soft amplitude    = " << soft_val << '\n'
                                // << "limit             = " << lim << '\n'
                                << "lim2/amp2           = " << lim / amp_val << '\n'
                                << '\n';
                        }
                    }
                }
            }
        }
    }
    std::cout
        << "total amp = " << all_full << '\n'
        << "total lim = " << all_lim << '\n'
        << '\n';
}
