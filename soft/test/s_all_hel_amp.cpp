#include <array>
#include <cmath>
#include <complex>
#include <iostream>
#include <numeric>

#include "analytic/0q4g-analytic.h"
#include "analytic/0q5g-analytic.h"
#include "ir/soft_gg2g-analytic.h"
#include "ngluon2/Mom.h"

#include "hel.hpp"

// int main(int argc, char* argv[])
int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);

    // p[3] -> 0
    std::array<MOM<double>, 5> full_mom { {
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, -5.0000000000000000e-01 },
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, 5.0000000000000000e-01 },
        { 4.9999949999999999e-01, 4.3301226887951738e-01, 0.0000000000000000e+00, 2.4999975000000005e-01 },
        { 5.0000000001437783e-07, 4.3301270190467088e-07, 0.0000000000000000e+00, 2.5000000000718897e-07 },
        { 5.0000000000000000e-01, -4.3301270189221930e-01, -0.0000000000000000e+00, -2.5000000000000006e-01 },
    } };

    std::array<MOM<double>, 4> reduced_mom { {
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, -5.0000000000000000e-01 },
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, 5.0000000000000000e-01 },
        { 4.9999949999999999e-01, 4.3301226887951738e-01, 0.0000000000000000e+00, 2.4999975000000005e-01 },
        { 5.0000000000000000e-01, -4.3301270189221930e-01, -0.0000000000000000e+00, -2.5000000000000006e-01 },
    } };

    std::array<MOM<double>, 3> soft_mom { {
        { 4.9999949999999999e-01, 4.3301226887951738e-01, 0.0000000000000000e+00, 2.4999975000000005e-01 },
        { 5.0000000001437783e-07, 4.3301270190467088e-07, 0.0000000000000000e+00, 2.5000000000718897e-07 },
        { 5.0000000000000000e-01, -4.3301270189221930e-01, -0.0000000000000000e+00, -2.5000000000000006e-01 },
    } };

    const double scale { 1. };
    Amp0q5g_a<double> full_amp(scale);
    Amp0q4g_a<double> reduced_amp(scale);
    Softgg2g_a<double> soft_amp(scale);

    full_amp.setMomenta(full_mom.data());
    reduced_amp.setMomenta(reduced_mom.data());
    // the second argument is a reference momentum
    soft_amp.setMomenta(soft_mom.data(), reduced_mom[0]);

    std::cout
        << '\n'
        << "zero indexed" << '\n'
        << "p[3]^0            = " << full_mom[3].x0 << '\n'
        << '\n';

    for (int h1 { -1 }; h1 < 2; h1 += 2) {
        for (int h2 { -1 }; h2 < 2; h2 += 2) {
            for (int h3 { -1 }; h3 < 2; h3 += 2) {
                for (int h4 { -1 }; h4 < 2; h4 += 2) {
                    for (int h5 { -1 }; h5 < 2; h5 += 2) {
                        Helicity<5> full_hels { h1, h2, h3, h4, h5 };
                        Helicity<4> reduced_hels { h1, h2, h3, h5 };

                        if (
                            (std::abs(std::accumulate(full_hels.cbegin(), full_hels.cend(), 0)) == 1) && (std::abs(std::accumulate(reduced_hels.cbegin(), reduced_hels.cend(), 0)) == 0)) {
                            Helicity<3> soft_hels { -h3, h4, -h5 };

                            full_amp.setHelicity(full_hels.data());
                            reduced_amp.setHelicity(reduced_hels.data());
                            soft_amp.setHelicity(soft_hels.data());

                            // (kinematic) partial(/colour ordered) amplitude
                            const std::complex<double> amp_val { full_amp.A0() };
                            const std::complex<double> red_amp_val { reduced_amp.A0() };
                            const std::complex<double> soft_val { soft_amp.A0() };
                            const std::complex<double> lim { red_amp_val * soft_val };

                            std::cout
                                << full_hels << " > " << reduced_hels << " * " << soft_hels << '\n'
                                // << "full amplitude    = " << amp_val << '\n'
                                // << "reduced amplitude = " << red_amp_val << '\n'
                                // << "soft amplitude    = " << soft_val << '\n'
                                // << "limit             = " << lim << '\n'
                                << "lim/amp           = " << lim / amp_val << '\n'
                                << '\n';
                        }
                    }
                }
            }
        }
    }
}
