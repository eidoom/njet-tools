#include <array>
#include <iostream>

#include "mom4.hpp"

mom4::mom4(const double p0, const double p1, const double p2, const double p3)
    : std::array<double, 4>({ p0, p1, p2, p3 }) {};

mom4::mom4(const double px)
    : std::array<double, 4>({ px, px, px, px }) {};

mom4::mom4() {};

mom4 mom4::operator-() const
{
    return mom4(-(*this)[0], -(*this)[1], -(*this)[2], -(*this)[3]);
}

mom4& mom4::operator*=(const double& c)
{
    for (int i { 0 }; i < 4; ++i) {
        (*this)[i] *= c;
    }
    return *this;
}

mom4& mom4::operator+=(const double& c)
{
    for (int i { 0 }; i < 4; ++i) {
        (*this)[i] += c;
    }
    return *this;
}

mom4& mom4::operator+=(const mom4& o)
{
    for (int i { 0 }; i < 4; ++i) {
        (*this)[i] += o[i];
    }
    return *this;
}

mom4& mom4::operator-=(const mom4& o)
{
    for (int i { 0 }; i < 4; ++i) {
        (*this)[i] -= o[i];
    }
    return *this;
}

double mom4::mass2() const
{
    return (*this) * (*this);
}

mom4 operator*(mom4 m, const double& c)
{
    m *= c;
    return m;
}

mom4 operator*(const double& c, mom4 m) { return m * c; }

double operator*(const mom4& m, const mom4& o)
{
    double sol { m[0] * o[0] };
    for (int i { 1 }; i < 4; ++i) {
        sol -= m[i] * o[i];
    }
    return sol;
}

mom4 operator+(mom4 m, const double& c)
{
    m += c;
    return m;
}

mom4 operator+(const double& c, mom4 m) { return m + c; }

mom4 operator+(mom4 m, const mom4& o)
{
    m += o;
    return m;
}

mom4 operator-(mom4 m, const mom4& o)
{
    m -= o;
    return m;
}

std::ostream& operator<<(std::ostream& out, const mom4& m)
{
    out << "{" << m[0] << ", " << m[1] << ", " << m[2] << ", " << m[3] << "}";
    return out;
}
