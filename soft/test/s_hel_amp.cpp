#include <algorithm>
#include <array>
#include <complex>
#include <iostream>

#include "analytic/0q4g-analytic.h"
#include "analytic/0q5g-analytic.h"
#include "ir/soft_gg2g-analytic.h"
#include "ngluon2/Mom.h"

#include "hel.hpp"

// int main(int argc, char* argv[])
int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);

    // p[3] -> 0 (j=3)
    const std::array<MOM<double>, 5> full_mom { {
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, -5.0000000000000000e-01 },
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, 5.0000000000000000e-01 },
        { 4.9999949999999999e-01, 4.3301226887951738e-01, 0.0000000000000000e+00, 2.4999975000000005e-01 },
        { 5.0000000001437783e-07, 4.3301270190467088e-07, 0.0000000000000000e+00, 2.5000000000718897e-07 },
        { 5.0000000000000000e-01, -4.3301270189221930e-01, -0.0000000000000000e+00, -2.5000000000000006e-01 },
    } };

    std::array<MOM<double>, 4> reduced_mom;
    std::copy_n(full_mom.cbegin(), 3, reduced_mom.begin());
    std::copy_n(full_mom.cbegin() + 4, 1, reduced_mom.begin() + 3);

    const double scale { 1. };
    Amp0q5g_a<double> full_amp(scale);
    Amp0q4g_a<double> reduced_amp(scale);
    Softgg2g_a<double> soft_amp(scale);

    full_amp.setMomenta(full_mom.data());
    reduced_amp.setMomenta(reduced_mom.data());

    // all in [0,4] (inclusive interval)
    const int i { 2 % 5 }; // fixed as (j-1)
    const int j { 3 % 5 }; // fixed by PS
    const int k { 4 % 5 }; // can change freely

    // choose ref mom different to other mom
    int r { 0 };
    while ((r == i) || (r == j) || (r == k)) {
        r = (r + 1) % 5;
    }

    // the second argument is a reference momentum
    soft_amp.setMomenta({ full_mom[i], full_mom[j], full_mom[k] }, full_mom[r]);

    const Helicity<5> full_hels { +1, +1, -1, -1, -1 };
    Helicity<4> reduced_hels;
    std::copy_n(full_hels.cbegin(), 3, reduced_hels.begin());
    std::copy_n(full_hels.cbegin() + 4, 1, reduced_hels.begin() + 3);
    // only the soft helicity matters
    Helicity<3> soft_hels { +1, full_hels[j], +1 };

    full_amp.setHelicity(full_hels.data());
    reduced_amp.setHelicity(reduced_hels.data());
    soft_amp.setHelicity(soft_hels.data());

    // (kinematic) partial(/colour ordered) amplitude
    const std::complex<double> amp_val { full_amp.A0() };
    const std::complex<double> red_amp_val { reduced_amp.A0() };
    const std::complex<double> soft_val { soft_amp.A0() };
    const std::complex<double> lim { red_amp_val * soft_val };

    std::cout
        << '\n'
        << "zero indexed" << '\n'
        << "p[3]^0            = " << full_mom[3].x0 << '\n'
        << full_hels << " > " << reduced_hels << " * " << soft_hels << '\n'
        // << "full amplitude    = " << amp_val << '\n'
        // << "reduced amplitude = " << red_amp_val << '\n'
        // << "soft amplitude    = " << soft_val << '\n'
        // << "limit             = " << lim << '\n'
        << "lim/amp           = " << lim / amp_val << '\n';

    std::cout << '\n';
}
