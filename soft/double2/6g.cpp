#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <complex>
#include <iostream>
#include <numbers>
#include <numeric>
#include <vector>

#include "analytic/0q4g-analytic.h"
#include "chsums/0q6g.h"
#include "ngluon2/Mom.h"

#include "hel.hpp"

template <typename T>
class Col {
private:
    const T Nc;

    const T Nc2 { Nc * Nc };
    const T Nc4 { Nc2 * Nc2 };
    const T V { Nc2 - 1. };

    const std::array<std::array<int, 6>, 136> cc2arr { {
        { 0, 1, 0, 1, 1, 0 },
        { 2, 3, 2, 4, 4, 5 },
        { 0, 1, 0, 1, 1, 0 },
        { 5, 4, 2, 4, 3, 2 },
        { 6, 7, 8, 9, 7, 6 },
        { 0, 1, 0, 1, 1, 0 },
        { 2, 4, 5, 3, 4, 2 },
        { 8, 7, 6, 7, 9, 6 },
        { 6, 9, 6, 7, 7, 8 },
        { 0, 1, 0, 1, 1, 0 },
        { 6, 9, 6, 7, 7, 8 },
        { 5, 5, 5, 3, 3, 10 },
        { 11, 12, 13, 14, 12, 11 },
        { 13, 12, 11, 12, 14, 11 },
        { 0, 1, 0, 1, 1, 0 },
        { 5, 5, 5, 3, 3, 10 },
        { 6, 9, 6, 7, 7, 8 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 12, 13, 14, 12, 11 },
        { 2, 3, 2, 4, 4, 5 },
        { 0, 1, 0, 1, 1, 0 },
        { 11, 12, 13, 14, 12, 11 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 9, 6, 7, 7, 8 },
        { 15, 16, 15, 17, 17, 16 },
        { 8, 7, 6, 7, 9, 6 },
        { 2, 4, 5, 3, 4, 2 },
        { 0, 1, 0, 1, 1, 0 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 12, 13, 14, 12, 11 },
        { 15, 16, 15, 17, 17, 16 },
        { 6, 9, 6, 7, 7, 8 },
        { 6, 7, 8, 9, 7, 6 },
        { 5, 4, 2, 4, 3, 2 },
        { 6, 9, 6, 7, 7, 8 },
        { 0, 1, 0, 1, 1, 0 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 12, 13, 14, 12, 11 },
        { 10, 3, 5, 3, 5, 5 },
        { 11, 14, 11, 12, 12, 13 },
        { 6, 7, 8, 9, 7, 6 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 14, 11, 12, 12, 13 },
        { 15, 17, 16, 16, 17, 15 },
        { 0, 1, 0, 1, 1, 0 },
        { 11, 12, 13, 14, 12, 11 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 14, 11, 12, 12, 13 },
        { 16, 17, 15, 17, 16, 15 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 7, 8, 9, 7, 6 },
        { 5, 3, 10, 5, 3, 5 },
        { 11, 14, 11, 12, 12, 13 },
        { 6, 9, 6, 7, 7, 8 },
        { 0, 1, 0, 1, 1, 0 },
        { 10, 3, 5, 3, 5, 5 },
        { 11, 14, 11, 12, 12, 13 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 12, 13, 14, 12, 11 },
        { 11, 14, 11, 12, 12, 13 },
        { 5, 3, 10, 5, 3, 5 },
        { 6, 7, 8, 9, 7, 6 },
        { 13, 12, 11, 12, 14, 11 },
        { 5, 4, 2, 4, 3, 2 },
        { 2, 4, 5, 3, 4, 2 },
        { 0, 1, 0, 1, 1, 0 },
        { 11, 14, 11, 12, 12, 13 },
        { 16, 17, 15, 17, 16, 15 },
        { 11, 12, 13, 14, 12, 11 },
        { 8, 7, 6, 7, 9, 6 },
        { 15, 17, 16, 16, 17, 15 },
        { 11, 14, 11, 12, 12, 13 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 7, 8, 9, 7, 6 },
        { 6, 7, 8, 9, 7, 6 },
        { 8, 7, 6, 7, 9, 6 },
        { 2, 3, 2, 4, 4, 5 },
        { 0, 1, 0, 1, 1, 0 },
        { 6, 7, 8, 9, 7, 6 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 14, 11, 12, 12, 13 },
        { 5, 3, 10, 5, 3, 5 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 12, 13, 14, 12, 11 },
        { 16, 17, 15, 17, 16, 15 },
        { 11, 14, 11, 12, 12, 13 },
        { 6, 9, 6, 7, 7, 8 },
        { 15, 16, 15, 17, 17, 16 },
        { 11, 12, 13, 14, 12, 11 },
        { 13, 12, 11, 12, 14, 11 },
        { 0, 1, 0, 1, 1, 0 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 7, 8, 9, 7, 6 },
        { 15, 17, 16, 16, 17, 15 },
        { 11, 14, 11, 12, 12, 13 },
        { 11, 12, 13, 14, 12, 11 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 14, 11, 12, 12, 13 },
        { 10, 3, 5, 3, 5, 5 },
        { 15, 16, 15, 17, 17, 16 },
        { 6, 9, 6, 7, 7, 8 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 12, 13, 14, 12, 11 },
        { 6, 9, 6, 7, 7, 8 },
        { 0, 1, 0, 1, 1, 0 },
        { 11, 14, 11, 12, 12, 13 },
        { 15, 17, 16, 16, 17, 15 },
        { 6, 7, 8, 9, 7, 6 },
        { 13, 12, 11, 12, 14, 11 },
        { 16, 17, 15, 17, 16, 15 },
        { 11, 14, 11, 12, 12, 13 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 12, 13, 14, 12, 11 },
        { 11, 12, 13, 14, 12, 11 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 9, 6, 7, 7, 8 },
        { 5, 5, 5, 3, 3, 10 },
        { 8, 7, 6, 7, 9, 6 },
        { 6, 7, 8, 9, 7, 6 },
        { 0, 1, 0, 1, 1, 0 },
        { 5, 3, 10, 5, 3, 5 },
        { 11, 14, 11, 12, 12, 13 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 7, 8, 9, 7, 6 },
        { 11, 14, 11, 12, 12, 13 },
        { 10, 3, 5, 3, 5, 5 },
        { 11, 12, 13, 14, 12, 11 },
        { 8, 7, 6, 7, 9, 6 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 12, 13, 14, 12, 11 },
        { 5, 5, 5, 3, 3, 10 },
        { 6, 9, 6, 7, 7, 8 },
        { 2, 4, 5, 3, 4, 2 },
        { 5, 4, 2, 4, 3, 2 },
        { 2, 3, 2, 4, 4, 5 },
        { 0, 1, 0, 1, 1, 0 },
    } };

    const T norma { V };

    const std::array<T, 18> vals {
        4 * (6 - 2 * Nc2 + Nc4),
        -8 * (-3 + Nc2),
        -4 - Nc4,
        -4,
        2 * (-2 + Nc2),
        4 * V,
        -2 * (4 + Nc4),
        4 * (-2 + Nc2),
        8 * V,
        -8,
        -4 + 8 * Nc2,
        4 - 6 * Nc2,
        4 - 2 * Nc2,
        4 + 2 * Nc2 + Nc4,
        4,
        Nc2*(4 + Nc2),
        4 * Nc2,
        -2 * Nc2,
    };

    // index symmetric matrix (including diagonal) in compact form
    int is(int i, int j) const
    {
        return i <= j ? i + j * (j + 1) / 2 : j + i * (i + 1) / 2;
    }

public:
    const T CA { Nc };

    Col(T nc = 3.)
        : Nc(nc)
    {
    }

    T getNc() const
    {
        return this->Nc;
    }

    T cc2(int i, int j, int k, int l, int a, int b) const
    {
        int ii { 4 * i + j };
        int jj { 4 * k + l };

        return this->norma * this->vals[this->cc2arr[this->is(ii, jj)][this->is(a, b)]];
    }
};

template <typename T>
T s1(const MOM<T> pi, const MOM<T> pj, const MOM<T> q)
{
    return dot(pi, pj) / (dot(pi, q) * dot(pj, q));
}

template <typename T>
T s2(const MOM<T> pi, const MOM<T> pj, const MOM<T> q1, const MOM<T> q2)
{
    return (2 * dot(pi, q1) * dot(pi, q2) * dot(pj, q1) * dot(pj, q2) * (dot(pi, q2) * dot(pj, q1) + dot(pi, q1) * dot(pj, q2)) + dot(pi, pj) * (pow(dot(pi, q1), 2) * dot(pj, q2) * (2 * dot(pj, q1) + dot(pj, q2)) + pow(dot(pi, q2), 2) * dot(pj, q1) * (dot(pj, q1) + 2 * dot(pj, q2)) + 2 * dot(pi, q1) * dot(pi, q2) * (pow(dot(pj, q1), 2) - dot(pj, q1) * dot(pj, q2) + pow(dot(pj, q2), 2))) * dot(q1, q2) - pow(dot(pi, pj), 2) * (dot(pi, q1) * (2 * dot(pj, q1) + dot(pj, q2)) + dot(pi, q2) * (dot(pj, q1) + 2 * dot(pj, q2))) * pow(dot(q1, q2), 2)) / (2. * dot(pi, q1) * dot(pi, q2) * (dot(pi, q1) + dot(pi, q2)) * dot(pj, q1) * dot(pj, q2) * (dot(pj, q1) + dot(pj, q2)) * pow(dot(q1, q2), 2));
}

template <typename T>
T s2so(const MOM<T> pi, const MOM<T> pj, const MOM<T> q1, const MOM<T> q2)
{
    return (dot(pi, pj) * (dot(pi, q2) * dot(pj, q1) + dot(pi, q1) * dot(pj, q2) - dot(pi, pj) * dot(q1, q2))) / (dot(pi, q1) * dot(pi, q2) * dot(pj, q1) * dot(pj, q2) * dot(q1, q2));
}

template <typename T, std::size_t mul>
void test_massless(const std::array<MOM<T>, mul>& momenta)
{
    T zero { 1e-10 };
    std::for_each(momenta.cbegin(), momenta.cend(), [zero](const MOM<T>& m) { assert(abs(m.mass()) < zero); });
}

template <typename T, std::size_t mul>
void test_mom_cons(const std::array<MOM<T>, mul>& momenta)
{
    T zero { 1e-10 };
    MOM<T> sum { std::accumulate(momenta.cbegin(), momenta.cend(), MOM<T>(0., 0., 0., 0.)) };
    assert(sum.x0 < zero);
    assert(sum.x1 < zero);
    assert(sum.x2 < zero);
    assert(sum.x3 < zero);
}

template <typename T>
std::array<MOM<T>, 6> p6_s2(const T x1, const T x2, const T alpha, const T beta, const T gamma, const T sqrtS, int r)
{
    assert(r >= 0);

    r %= 5;

    const T ca { cos(alpha) };
    const T cb { cos(beta) };
    const T cg { cos(gamma) };

    const T sa { sin(alpha) };
    const T sb { sin(beta) };
    const T sg { sin(gamma) };

    // x3 fixes masslessness of p5
    const T x3 {
        (4 - 4 * x1 + x1 * x1 - ca * ca * x1 * x1 - sa * sa * x1 * x1 - 4 * x2 + 2 * x1 * x2 - 2 * ca * ca * cb * x1 * x2 - 2 * sa * sa * x1 * x2 + x2 * x2 - ca * ca * cb * cb * x2 * x2 - sa * sa * x2 * x2 - ca * ca * sb * sb * x2 * x2) / (2 * (2 - x1 + ca * ca * cb * x1 + cg * sa * sa * x1 - ca * sa * sb * sg * x1 - x2 + ca * ca * cb * cb * x2 + cg * sa * sa * x2 + ca * ca * cg * sb * sb * x2))
    };

    // generate momenta recursively
    // incoming momenta are back-to-back
    std::array<MOM<T>, 6> momenta { {
        -sqrtS / 2. * MOM<T>(1., 0., 0., 1.),
        -sqrtS / 2. * MOM<T>(1., 0., 0., -1.),
        // p2 = rotate p1 by alpha about x
        x1 * sqrtS / 2. * MOM<T>(1., 0., -sa, ca),
        // p3 = rotate p2 by beta about y
        x2 * sqrtS / 2. * MOM<T>(1., ca * sb, -sa, ca * cb),
        // p4 = rotate p3 by gamma about z
        x3 * sqrtS / 2. * MOM<T>(1., ca * cg * sb + sa * sg, -(cg * sa) + ca * sb * sg, ca * cb),
        MOM<T>(),
    } };
    // p5 given by momentum conservation
    momenta[5] = -std::accumulate(momenta.begin(), momenta.end() - 1, MOM<T>());

    std::rotate(momenta.begin(), momenta.begin() + r, momenta.end());

    test_massless(momenta);
    test_mom_cons(momenta);

    return momenta;
}

template <typename T>
T run(const T nc, const T small)
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);

    const int N { 6 };     // full amplitude multiplicity
    const int o { 2 };     // how many soft gluons
    const int n { N - o }; // reduced amplitude multiplicity

    // take p2 & p3 soft (zero index)
    const std::array<MOM<T>, N> full_mom {
        p6_s2(
            small,
            small,
            static_cast<T>(std::numbers::pi / 3.),
            static_cast<T>(std::numbers::pi / 5.),
            static_cast<T>(std::numbers::pi / 7.),
            static_cast<T>(1.),
            0)
    };

    // for (int i { 0 }; i < N; ++i) {
    //     for (int j { i + 1 }; j < N; ++j) {
    //         const T s { 2. * dot(moms[i], moms[j]) };
    //         std::cout << "s" << i << j << "=" << ((s >= 0) ? " " : "") << s << '\n';
    //     }
    // }
    // std::cout << '\n';

    // // T soft: p[2], p[3] -> 0
    // const std::array<MOM<T>, N> full_mom { {
    //     { -5.0000000000000000e-01, -0.0000000000000000e+00, -0.0000000000000000e+00, -5.0000000000000000e-01 },
    //     { -5.0000000000000000e-01, -0.0000000000000000e+00, -0.0000000000000000e+00, 5.0000000000000000e-01 },
    //     { 4.9999999999999998e-07, 0.0000000000000000e+00, -4.3301270189221928e-07, 2.5000000000000004e-07 },
    //     { 4.9999999999999998e-07, 1.4694631307311831e-07, -4.3301270189221928e-07, 2.0225424859373691e-07 },
    //     { 4.9999907881923744e-01, 3.2027063345893469e-01, -3.2637274674779743e-01, 2.0225387596829103e-01 },
    //     { 4.9999992118076264e-01, -3.2027078040524776e-01, 3.2637361277320120e-01, -2.0225432822253964e-01 },
    // } };

    // strong-ordering soft limit q2<<q1<<1
    // const std::array<MOM<T>, 6> full_mom { {
    //     { -5.0000000000000000e-01, -0.0000000000000000e+00, -0.0000000000000000e+00, -5.0000000000000000e-01 },
    //     { -5.0000000000000000e-01, -0.0000000000000000e+00, -0.0000000000000000e+00, 5.0000000000000000e-01 },
    //     { 4.9999999999999998e-07, 0.0000000000000000e+00, -4.3301270189221928e-07, 2.5000000000000004e-07 },
    //     { 4.9999999999999999e-13, 1.4694631307311831e-13, -4.3301270189221931e-13, 2.0225424859373692e-13 },
    //     { 4.9999955811210195e-01, 3.2027094046635896e-01, -3.2637305960463120e-01, 2.0225406984632735e-01 },
    //     { 4.9999994188739805e-01, -3.2027094046650589e-01, 3.2637349261776610e-01, -2.0225431984652961e-01 },
    // } };

    std::array<MOM<T>, n> reduced_mom;
    std::copy_n(full_mom.cbegin(), 2, reduced_mom.begin());
    std::copy_n(full_mom.cbegin() + 4, 2, reduced_mom.begin() + 2);

    std::array<MOM<T>, o> soft_mom;
    std::copy_n(full_mom.cbegin() + 2, 2, soft_mom.begin());

    const T scale { 1. };
    Amp0q6g<T> full_amp(scale);
    Amp0q4g_a<T> reduced_amp(scale);

    full_amp.setMomenta(full_mom.data());
    reduced_amp.setMomenta(reduced_mom.data());

    Helicity<N> full_hels { +1, -1, +1, -1, +1, -1 };
    Helicity<n> reduced_hels;
    std::copy_n(full_hels.cbegin(), 2, reduced_hels.begin());
    std::copy_n(full_hels.cbegin() + 4, 2, reduced_hels.begin() + 2);
    // Helicity<3> soft_hels; // doesn't matter
    reduced_amp.setHelicity(reduced_hels.data());

    Col<T> col(nc);

    full_amp.setNc(col.getNc());
    reduced_amp.setNc(col.getNc());

    std::cout
        << '\n'
        << "zero indexed" << '\n'
        << "p[2]^0                   = " << soft_mom[0].x0 << '\n'
        << "p[3]^0                   = " << soft_mom[1].x0 << '\n'
        << full_hels << " > " << reduced_hels << " * "
        << "S" << '\n'
        << '\n';

    // T lim { 0. };
    // int ref_index { 0 };
    // for (int i { 0 }; i < 4; ++i) {
    //     for (int j { i + 1 }; j < 4; ++j) {
    //         while ((ref_index == i) || (ref_index == j)) {
    //             ++ref_index;
    //         }
    //         // the second argument is a reference momentum
    //         soft_amp.setMomenta({ reduced_mom[i], full_mom[3], reduced_mom[j] }, reduced_mom[ref_index]);
    //         const T soft_val { soft_amp.born(soft_hels.data()) };
    //         const T cc_val { reduced_amp.born_ccij(reduced_hels.data(), i, j) };
    //         lim += cc_val * soft_val;
    //     }
    // }
    // lim *= 2.;

    // helicity amplitude modulus squared - includes colour
    const T amp_val { full_amp.born(full_hels.data()) };

    const int cbl { 3 }; // length of colour basis

    // for (int i { 0 }; i < 4; ++i) {
    //     std::cout
    //         << i << "312"
    //         << '\n';
    //     for (int a { 0 }; a < cbl; ++a) {
    //         for (int b { 0 }; b < cbl; ++b) {
    //             std::cout
    //                 << cc2(i, 3, 1, 2, a, b)
    //                 << " ";
    //         }
    //         std::cout
    //             << '\n';
    //     }
    //     std::cout
    //         << '\n';
    // }

    // for (int a { 0 }; a < cbl; ++a) {
    //     for (int b { 0 }; b < cbl; ++b) {
    //         std::cout
    //             << cc2(2, 3, 1, 2, a, b)
    //             << " ";
    //     }
    //     std::cout
    //         << '\n';
    // }
    // std::cout
    //     << '\n';

    std::array<std::complex<T>, cbl> vec {
        // A0(...) tmp moved to public for this
        reduced_amp.A0(0, 1, 2, 3),
        reduced_amp.A0(0, 1, 3, 2),
        reduced_amp.A0(0, 3, 1, 2),
    };

    // for (std::complex<T> v : vec) {
    //     std::cout << v << '\n';
    // }

    // std::cout << reduced_amp.A0(0,1,2,3) << '\n';
    // std::cout << reduced_amp.A0(3,2,1,0) << '\n';

    // std::cout << reduced_amp.A0(0, 3, 1, 2) << '\n';
    // std::cout << -reduced_amp.A0(0, 1, 2, 3) - reduced_amp.A0(0, 1, 3, 2) << '\n';

    T term_one { 0. };

    for (int i { 0 }; i < n; ++i) {
        for (int j { 0 }; j < n; ++j) {
            for (int k { 0 }; k < n; ++k) {
                for (int l { 0 }; l < n; ++l) {

                    std::complex<T> col_sum_2 { std::complex<T>() };

                    for (int a { 0 }; a < cbl; ++a) {

                        std::complex<T> col_i10e { std::complex<T>() };

                        for (int b { 0 }; b < cbl; ++b) {
                            col_i10e += static_cast<T>(col.cc2(i, j, k, l, a, b)) * vec[b];
                        }

                        col_sum_2 += std::conj(vec[a]) * col_i10e;
                    }

                    // std::cout
                    //     << col_sum_2
                    //     << '\n';

                    term_one += s1(reduced_mom[i], reduced_mom[j], soft_mom[0])
                        * s1(reduced_mom[k], reduced_mom[l], soft_mom[1])
                        * col_sum_2.real();
                }
            }
        }
    }

    term_one *= 0.5;

    std::cout << "colour-connected term    = " << term_one << '\n';

    T term_two { 0. };

    for (int i { 0 }; i < n; ++i) {
        for (int j { 0 }; j < n; ++j) {
            term_two += s2(reduced_mom[i], reduced_mom[j], soft_mom[0], soft_mom[1])
                // term_two += s2so(reduced_mom[i], reduced_mom[j], soft_mom[0], soft_mom[1])
                * reduced_amp.born_ccij(reduced_hels.data(), i, j);
        }
    }

    term_two *= -col.CA;

    std::cout << "un-colour-connected term = " << term_two << '\n';

    T lim { term_one + term_two };

    T rati { lim / amp_val };

    std::cout
        << '\n'
        << "full amplitude 2         = " << amp_val << '\n'
        << "limit 2                  = " << lim << '\n'
        << "lim2/amp2                = " << rati << '\n';

    std::cout << '\n';

    return rati;
}

template <typename T>
void loop(const T nc)
{
    std::vector<T> res;
    for (int e { 1 }; e < 17; ++e) {
        res.push_back(run<T>(nc, pow(10., -e)));
    }

    for (T r : res) {
        std::cout << r << '\n';
    }

    std::cout << '\n';
}

// int main(int argc, char* argv[])
int main()
{
    run<double>(3, pow(10., -3));
    // loop<qd_real>(1e6);
}
