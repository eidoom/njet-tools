# Soft limits

## double [deprecated]
* See double2
## double2 
* Testing double soft limit
## overlap
* Testing behaviour of |A|^2 in planes of PS
* In particular, looking at overlapping regions of limits
## test
* Testing single soft limit
