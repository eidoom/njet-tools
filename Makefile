include Makefile.inc

.PHONY: clean r all

all: *.pdf

r:
	./md2pdf -r

clean:
	rm -f *.pdf
