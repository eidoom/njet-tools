# 3g2a-2l-ir-perf

## `calc_otf.cpp`

* calculate amplitude (rescue system) generating points on the fly (using cps.hpp)
* does a 100 point slice into IR limit
* execution is sequential
* runtime is long (~24h)
* args: `<rseed (unity-indexed)> <cutoff>`

## `cps.hpp`

* (symlink) generates IRPS from NJet LIPS

## `run.sh`

* run `calc_otf` in parallel over sequential rseeds
* args: `<rseed start> <rseed number>`
