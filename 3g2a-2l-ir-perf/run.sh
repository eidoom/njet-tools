#!/usr/bin/env bash

start=$1
num=$2
end=$1+$2
echo "Starting $num jobs"

for ((rseed = $start; rseed < $end; rseed++)); do
	./calc_otf $rseed 1e-3 &
done
