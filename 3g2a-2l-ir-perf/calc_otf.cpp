#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "finrem/common/rescue.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

#include "cps.hpp"

template <template <typename, typename> class AMP>
void run(const std::vector<MOM<double>> &moms4, const double cutoff,
         const std::string &suffix) {
  const double mur{91.188}, z{0.5}, base{1.1};
  const int Nc{3}, Nf{5}, i{2}, start{49}, num{100};

  Rescue2LNLO<AMP> amp(
      StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()), cutoff);
  amp.setNf(Nf);
  amp.setNc(Nc);
  amp.setMuR2(std::pow(mur, 2));

  for (int a{0}; a < num; ++a) {
    std::cout << a << " / " << num - 1 << '\n';

    const int p{start + a};

    const double d{std::pow(base, -p)};

    std::vector<MOM<double>> moms5{cps::collinear(moms4, z, d, i)};

    const std::vector v{cps::invariants(moms5)};

    {
      std::ofstream o("sijs." + suffix, std::ios::app);
      o << std::scientific << std::setprecision(16) << v[i] / v[0] << '\n';
    }

    amp.setMomenta(moms5);

    amp.eval_timed(a, "0", suffix);
  }
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cerr << "Wrong number of command lines arguments!" << '\n'
              << "Use as: ./calc_otf <rseed (unity-indexed)> <cutoff>" << '\n';
    std::exit(EXIT_FAILURE);
  }

  const int legs{5}, rseed{std::stoi(argv[1])};
  assert(rseed > 0);
  const double sqrtS{1.}, cutoff{std::stod(argv[2])};
  const std::string suffix{std::to_string(rseed) + ".csv"};

  PhaseSpace<double> ps4(legs - 1, rseed, sqrtS);
  std::vector<MOM<double>> moms4{ps4.getPSpoint()};

  run<Amp0q3g2A_a2l>(moms4, cutoff, suffix);
}
