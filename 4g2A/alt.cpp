#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

#include "chsums/0q4gA.h"
#include "ngluon2/EpsQuintuplet.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

const int W { 7 };

const Flavour<double> Au {
    StandardModel::Au(StandardModel::u(), StandardModel::ubar())
};

const Flavour<double> Ax {
    StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C())
};

const int Nc { 3 };
const int Nf { 5 };
const double mur2 { 1000. };

template <typename T>
void test0q4g2a(const std::vector<MOM<T>>& momenta)
{

    Amp0q4gAA<T> amp(Ax);
    amp.setNf(Nf);
    amp.setNc(Nc);
    amp.setMuR2(mur2);

    amp.setMomenta(momenta);

    Eps5<T> loop2 { amp.virtsq() };

    T res = 0.;
    for (auto h : Amp0q4gAAStatic::HSarr) {

        T amph { amp.virtsq(h).get0().real() };

        res = res + amph;
    }

    T ratio = loop2.get0().real() / res;

    std::cout << std::setw(W) << loop2.get0().real() << " " << res << " " << ratio << '\n';
}

template <typename T>
void run0q4g2A()
{

    const std::vector<double> scales2 { { 0 } };
    const int rseed { 1 };

    PhaseSpace<T> ps6(6, rseed);

    int ii;
    for (ii = 0; ii < 10; ii++) {

        std::vector<MOM<T>> momenta61 { ps6.getPSpoint() };
        refineM(momenta61, momenta61, scales2);

        test0q4g2a<T>(momenta61);
    }
}

int main()
{
    std::cout << std::scientific << std::setprecision(16);
    run0q4g2A<double>();
}
